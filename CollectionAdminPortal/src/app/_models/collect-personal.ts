export class CollectPersonal {
    CPNAME: string;
    EmailId: string;
    ContactNo: string;
    CompanyId: number;
    Address: string;
    City: string;
    Zone: string;
    StateId: string;
    CountryId: number;
    ZipCode: string;
    Password: string;
    CPassword: string;
}

export class CountryList {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}

export class StateList {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}
export class CompanyList {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}

export class CollectPersonList{
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}

export class CPStatus{
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}

export class CPUpdate{
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}

export class CPdelete{
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}