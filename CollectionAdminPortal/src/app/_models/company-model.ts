export class CompanyModel {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}

export class CompanyDetailsModel {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}
export class UpdateCompany {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}

export class DelateCompany {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}

export class ChangeCompStatus {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}