import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollectPersonDetailsComponent } from './collect-person-details.component';

describe('CollectPersonDetailsComponent', () => {
  let component: CollectPersonDetailsComponent;
  let fixture: ComponentFixture<CollectPersonDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollectPersonDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectPersonDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
