import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Response, Headers, ResponseContentType } from '@angular/http';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment';
//import { AuthService } from './auth.Service';
import { ApiResponse } from '../_models/api-response';
import { from } from 'rxjs';
import { Message } from '../_models/message'

@Injectable()
export class HttpCommonService<T> {
  router: any;
  constructor(public http: HttpClient) { }
  
  protected convertDateAsUTC(date: Date) {
    if (date instanceof Date)
      return new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds()));
    else
      return date;
  }

  protected getAccessToken(actionUrl: string, userId: string, password: string) {
    var data = "username=" + userId + "&password=" + password + "&grant_type=password";
    var reqHeader = new HttpHeaders({ 'Content-Type': 'application/x-www-urlencoded', 'No-Auth': 'True' });
    return this.http.post(environment.apiTokenUrl, data, { headers: reqHeader })
      .map(this.handleGetResponseMessage)
      .catch(this.handleError);
  }

  // protected getModelList(actionUrl: string, queryStringParams?: HttpParams, authRequired?: boolean): Observable<T[]> {
  //   return this.http.get<T[]>(environment.apiBaseUrl + actionUrl, this.createHttpRequestOptions(authRequired, queryStringParams))
  //     .map(this.handleGetResponseMessage)
  //     .catch(this.handleError);
  // }


  // protected filterModelList(actionUrl: string, model: T): Observable<ApiResponse> {
  //   var modelList: T[] = []
  //   modelList.push(model);
  //   return this.http.post<T>(environment.apiBaseUrl + actionUrl, model, this.createHttpRequestOptions(true, null))
  //     .map(this.handleSetResponseMessage)
  //     .catch(this.handleError);
  // }

  protected Login(actionUrl: string, model: T): Observable<ApiResponse> {
    return this.http.post<T>(environment.apiBaseUrl + actionUrl, model, this.createHttpRequestOptions(false, null))
      .map(this.handleGetResponseMessage)
      .catch(this.handleError);
  }

  // protected addModel(actionUrl: string, model: T): Observable<ApiResponse> {    
  //   return this.http.post<T>(environment.apiBaseUrl + actionUrl, model, this.createHttpRequestOptions(false, null))
  //     .map(this.handleSetResponseMessage)     
  //     .catch(this.handleError);
  // }

  // protected addModelList(actionUrl: string, model: T[]): Observable<ApiResponse> {    
  //   return this.http.post<T>(environment.apiBaseUrl + actionUrl, model, this.createHttpRequestOptions(true, null))
  //     .map(this.handleSetResponseMessage)
  //     .catch(this.handleError);
  // }

  // protected uploadLogo(actionUrl: string, model: Logo[]): Observable<ApiResponse> {   
  //   return this.http.post<Logo>(environment.apiBaseUrl + actionUrl, model, this.createHttpRequestOptions(true, null))
  //     .map(this.handleSetResponseMessage)
  //     .catch(this.handleError);
  // }


  protected createHttpRequestOptions(authRequired?: boolean, queryStringParams?: HttpParams): any {
    var options: any; var headers: any;
    if (authRequired) {
      headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + environment.accessToken //this.authService.AuthorizationToken
      });
    }
    options = {
      headers: headers,
      params: queryStringParams
    };
    return options;
  }

  protected handleGetResponseMessage(response: Response | any) {
    var customizedResponse = response;
    if (response == null) {
      customizedResponse = new Message();
      customizedResponse.type = "Information";
      customizedResponse.summary = "Sorry, No Record Found";
      customizedResponse.detail = "Sorry, No Record Found";
    }
    return customizedResponse;
  }

  protected handleError(response: Response | any) {
    let message = new Message();
    message.type = "Error";
    message.summary = 'Server error in performing the operation !!!';
    if (response.status === 401) {
      //this.authService.handleUnauthorizeRequest();
      message.summary = response.error.Message;
    }
    else {
      try {
        message.detail = response.message;
      }
      catch (err) {
      }
    }
    return Observable.throw(message);
    //return message;
  }

  protected handleSetResponseMessage(response: Response | any) {
    let apiResponse = new ApiResponse();
    let message = new Message();
    message.type = "Success";
    // message.summary = "Operation Completed Successfully";
    message.detail = response; //"Operation Completed Successfully";
    apiResponse.Models = response.models;
    apiResponse.Message = message;
    return apiResponse;
  }

  // protected updateModel(actionUrl: string, model: T): Observable<ApiResponse> {
  //   var modelList: T[] = [];
  //   modelList.push(model);

  //   return this.http.put<T>(environment.apiBaseUrl + actionUrl, model, this.createHttpRequestOptions(true, null))
  //     .map(this.handleSetResponseMessage)
  //     .catch(this.handleError);
  // }

  // protected deleteModel(actionUrl: string, model: T): Observable<ApiResponse> {
  //   var modelList: T[] = [];
  //   modelList.push(model);

  //   let headers = new HttpHeaders({
  //     'Content-Type': 'application/json',
  //     'Authorization': 'Bearer ' + this.authService.AuthorizationToken
  //   });

  //   let options = {
  //     headers: headers,
  //     body: modelList
  //   };

  //   return this.http.post(environment.apiBaseUrl + actionUrl, model)
  //     .map(this.handleSetResponseMessage)
  //     .catch(this.handleError);
  // }
}
