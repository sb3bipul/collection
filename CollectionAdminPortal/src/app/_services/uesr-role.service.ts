// import { Injectable } from '@angular/core';

// @Injectable({
//   providedIn: 'root'
// })
// export class UesrRoleService {

//   constructor() { }
// }
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core'
import { UserRole } from '../_models/user';
import {HttpCommonService} from '../_services/http-common.service';
import { AuthService } from './auth.Service';

@Injectable()
export class UserRoleService extends HttpCommonService<UserRole> {

 
constructor(http: HttpClient, authService: AuthService) {
  super(http, authService);
}

  public add(model: UserRole): any {
    return this.addModel("Roles/SetUserRole", model);
  }
  public get(model:HttpParams): any {
    return this.getModelList("Roles/GetUserRoles", model, true);
  }
  public delete(model: UserRole): any {
    return this.deleteModel("Roles/DeleteRole", model);
  }
}
