import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response, RequestMethod } from '@angular/http';
import { CollectPersonal, CountryList, StateList, CompanyList, CollectPersonList, CPStatus, CPdelete, CPUpdate } from '../_models/collect-personal';
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import { throwError } from 'rxjs';
import { HttpClientModule } from '@angular/common/http';
import { appConfig } from '../app.config';

@Injectable({
  providedIn: 'root'
})
export class CollectPersonService {
  private baseurl: string = appConfig.apiUrl;
  constructor(private http: Http) { }

  private handleError(error: any) {
    var applicationError = error.headers.get('Application-Error');
    var serverError = error.json();
    var modelStateErrors: string = '';
    if (!serverError.type) {
      console.log(serverError);
      for (var key in serverError) {
        if (serverError[key])
          modelStateErrors += serverError[key] + '\n';
      }
    }
    modelStateErrors = modelStateErrors = '' ? null : modelStateErrors;
    return Observable.throw(applicationError || modelStateErrors || 'Server error');
  }

  // getCountryList(): Observable<CountryList> {
  //   debugger;
  //   //let headers = new Headers({ 'Content-Type': 'application/json' });
  //   let options = new RequestOptions({ method: RequestMethod.Get});
  //   //let body = JSON.stringify(obj);//this.serializeObj(obj);
  //   //console.log('Invoice status log :api/Payment/getPaymentreceiptImage:' + JSON.stringify());
  //   return this.http.get(this.baseurl + 'api/CollectPersonal/getCountryList', options)
  //     .map((res: Response) => {
  //       return res.json();
  //     })
  //     .catch(this.handleError);
  // }

  getCountryList() {
    console.log('Calling URL Get API :  ' + this.baseurl + 'api/CollectPersonal/getCountryList');
    return this.http.get(this.baseurl + 'api/CollectPersonal/getCountryList').map((response: Response) => response.json());
  }

  getStateListByCountryId(CountryCode: number): Observable<StateList> {
    var obj = { CountryCode: CountryCode }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);//this.serializeObj(obj);
    console.log('Invoice status log :api/CollectPersonal/GetStateListByCountryId:' + JSON.stringify(obj));
    return this.http.post(this.baseurl + 'api/CollectPersonal/GetStateListByCountryId', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getCompanyList(CompanyId: number): Observable<CompanyList> {
    var obj = { CompanyId: CompanyId }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);//this.serializeObj(obj);
    console.log('Invoice status log :api/CollectPersonal/GetCompanylist:' + JSON.stringify(obj));
    return this.http.post(this.baseurl + 'api/CollectPersonal/GetCompanylist', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }


  insertCollectPersondata(_CPname: string, _EmailId: String, _ContactNo: String, _CompanyId: number, _Address: string, _City: string, _Zone: string, _StateId: string, _CountryId: number, _ZipCode: string, _Password: string, _CPassword: string,_CPImage:string, _ImageBase64:string): Observable<CollectPersonal> {
    var obj = { CPNAME: _CPname, EmailId: _EmailId, ContactNo: _ContactNo, CompanyId: _CompanyId, Address: _Address, City: _City, Zone: _Zone, StateId: _StateId, CountryId: _CountryId, ZipCode: _ZipCode, Password: _Password, CPassword: _CPassword, CPImage:_CPImage, ImageBase64String:_ImageBase64 }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);//this.serializeObj(obj);
    console.log('Insert Collect Personal Data : api/CollectPersonal/InsertCollectPersonData :' + JSON.stringify(obj));
    return this.http.post(this.baseurl + 'api/CollectPersonal/InsertCollectPersonData', body, options)
      .map((res: Response) => {
        console.log('Insert collect personal return data: ' + JSON.stringify(res));
        return res.json();
      })
      .catch(this.handleError);
  }

  getCollectPersonList(_fromdate: Date, _ToDate: Date, _CPName: string, _EmailId: string): Observable<CollectPersonList> {
    var obj = { FromDate: _fromdate, ToDate: _ToDate, CPName: _CPName, EmailId: _EmailId }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);//this.serializeObj(obj);
    console.log('Collect Personal Data : api/CollectPersonal/GetCollectPersonData :' + JSON.stringify(obj));
    return this.http.post(this.baseurl + 'api/CollectPersonal/GetCollectPersonData', body, options)
      .map((res: Response) => {
        console.log('Collect personal return data: ' + JSON.stringify(res));
        return res.json();
      })
      .catch(this.handleError);
  }

  private serializeObj(obj: any) {
    var result = [];
    for (var property in obj)
      result.push(encodeURIComponent(property) + "=" + encodeURIComponent(obj[property]));
    return result.join("&");
  }

  UpdateCPstatue(_Status: boolean, _CompanyId: number, _CPCode: string): Observable<CPStatus> {
    var obj = { Status: _Status, CompanyId: _CompanyId, CPCode: _CPCode }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);
    return this.http.post(this.baseurl + 'api/CollectPersonal/ChangeCPStatus', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }

  DeleteCollectPerson(_CompanyId: number, _CPCode: string): Observable<CPdelete> {
    var obj = { CompanyId: _CompanyId, CPCode: _CPCode }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);
    return this.http.post(this.baseurl + 'api/CollectPersonal/DeleteCPData', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }

  UpdateCollectPerson(_CPName: string, _EmailId: string, _ContactNo: string, _Address: string, _City: string, _Zone: string, _StateId: string, _CountryId: number, _Zip: string, _CPCode: string): Observable<CPUpdate> {
    var obj = { CPName: _CPName, EmailId: _EmailId, ContactNo: _ContactNo, Address: _Address, City: _City, Zone: _Zone, StateId: _StateId, CountryId: _CountryId, Zip: _Zip, CPCode: _CPCode }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);
    return this.http.post(this.baseurl + 'api/CollectPersonal/updateCollectPersonData', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }
}
