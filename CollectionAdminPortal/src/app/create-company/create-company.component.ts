import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CollectPersonService } from '../_services/collect-person.service';
import { CompanyServiceService } from '../_services/company-service.service';

@Component({
  selector: 'app-create-company',
  templateUrl: './create-company.component.html',
  styleUrls: ['./create-company.component.css']
})
export class CreateCompanyComponent implements OnInit {
  CompBanner: string; ComLogo: string; ComURL: string; Zip: string; Street: string; City: string;
  StateId: string; CountryId: string; ContactP: string; Fax: string;
  Email: string; ContactNo: string; CompanyName: string; ContactPEmail: string;
  countries: {};
  states: {};
  Base64ImageString: any;
  BaseImageString: File;
  Base64ImageStringBanner: any;
  BaseImageStringBanner: File;
  constructor(private formbulider: FormBuilder, private http: HttpClient, private router: Router, private collect: CollectPersonService, private toastr: ToastrService, private company: CompanyServiceService) { }

  ngOnInit() {
    this.collect.getCountryList().subscribe((data) => {
      console.log(data);
      this.countries = JSON.parse(JSON.stringify(data)).Payload;
    });
  }

  onChangeCountry(CountryCode: number) {
    this.collect.getStateListByCountryId(CountryCode).subscribe(
      _stateListData => {
        console.log('Load state list : ' + JSON.stringify(_stateListData.Payload));
        this.states = JSON.parse(JSON.stringify(_stateListData)).Payload;
      },
      error => {
        console.log('Failed while trying to load state list. ' + error);
      });
  }

  SaveCompanyData() {
    var _email = '';
    var _fax = ''; var _contactPersonEmail = '';
    var _companyURL = ''; var _compLogo = '';
    var _compBanner = ''; var _logoBase64 = ''; var _bannerBase64 = '';
    if (this.Email == undefined || this.Email == null) {
      _email = null;
    }
    else {
      _email = this.Email;
    }
    if (this.Fax == undefined || this.Fax == null) {
      _fax = null;
    }
    else {
      _fax = this.Fax;
    }
    if (this.ContactPEmail == undefined || this.ContactPEmail == null) {
      _contactPersonEmail = null;
    }
    else {
      _contactPersonEmail = this.ContactPEmail;
    }
    if (this.ComURL == undefined || this.ComURL == null) {
      _companyURL = null;
    } else {
      _companyURL = this.ComURL;
    }
    if (this.ComLogo == undefined || this.ComLogo == null) {
      _compLogo = null;
      _logoBase64 = null;
    }
    else {
      _compLogo = this.ComLogo;
      _logoBase64 = this.Base64ImageString;
    }
    if (this.CompBanner == undefined || this.CompBanner == null) {
      _compBanner = null;
      _bannerBase64 = null;
    } else { _compBanner = this.CompBanner; _bannerBase64 = this.Base64ImageStringBanner; }

    if ((this.CompanyName == undefined || this.CompanyName == null) && (this.ContactNo == undefined || this.ContactNo == null) && (this.ContactP == undefined || this.ContactP == null) && (this.CountryId == undefined || this.CountryId == null) && (this.StateId == undefined || this.StateId == null) && (this.City == undefined || this.City == null) && (this.Street == undefined || this.Street == null) && (this.Zip == undefined || this.Zip == null)) {
      this.toastr.warning("All (*) marked fields are mandatory", "Warning");
    }
    else {
      this.company.InsertCompanyMasterData(this.CompanyName, this.ContactNo, _email, _fax, this.ContactP, _contactPersonEmail, this.Street, this.City, this.StateId, this.CountryId, this.Zip, _companyURL, _compLogo, _compBanner, _logoBase64, _bannerBase64).subscribe(
        _savecompany => {
          this.toastr.success("Company data successfully saved", "Success");
          this.reset();
        },
        error => {
          console.log("Unable to save the company data" + error);
          this.toastr.error("Unable to save company data", "Error");
        }
      );
    }
  }

  cancel() {
    this.router.navigate(['./myQueue']);
  }

  handleInputChangeLogo(files) {
    var file = files;
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      this.toastr.error("Invalid image format", "Error");
      this.ComLogo = null;
      this.Base64ImageString = null;
      return;
    }
    reader.onloadend = this._handleReaderLoadedLogo.bind(this);
    reader.readAsDataURL(file);
  }

  _handleReaderLoadedLogo(e) {
    let reader = e.target;
    var base64result = reader.result.substr(reader.result.indexOf(',') + 1);
    this.Base64ImageString = base64result;
  }

  public pickedLogo(event) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file: File = fileList[0];
      this.BaseImageString = file;
      this.handleInputChangeLogo(file); //turn into base64
    }
    else {
      this.toastr.warning("No file selected", "Warning");
    }
  }

  handleInputChangeBanner(files) {
    var file = files;
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      this.toastr.error("Invalid image format", "Error");
      this.CompBanner = null;
      this.Base64ImageStringBanner = null;
      return;
    }
    reader.onloadend = this._handleReaderLoadedBanner.bind(this);
    reader.readAsDataURL(file);
  }

  _handleReaderLoadedBanner(e) {
    let reader = e.target;
    var base64resultBanner = reader.result.substr(reader.result.indexOf(',') + 1);
    this.Base64ImageStringBanner = base64resultBanner;
  }

  public pickedBanner(event) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file: File = fileList[0];
      this.BaseImageStringBanner = file;
      this.handleInputChangeBanner(file); //turn into base64
    }
    else {
      this.toastr.warning("No file selected", "Warning");
    }
  }

  reset() {
    this.CompBanner = null; this.ComLogo = null; this.ComURL = null; this.Zip = null; this.Street = null; this.City = null;
    this.StateId = null; this.CountryId = null; this.ContactP = null; this.Fax = null;
    this.Email = null; this.ContactNo = null; this.CompanyName = null; this.ContactPEmail = null;
    this.Base64ImageString = null;
    this.Base64ImageStringBanner = null;
  }
}
