import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import { AssignCustomerService } from '../_services/assign-customer.service';
import { ToastrService } from 'ngx-toastr';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
// import { DomSanitizer } from '@angular/platform-browser';
// import { ngxLoadingAnimationTypes, NgxLoadingComponent } from 'ngx-loading';

// const PrimaryWhite = '#ffffff';
// const SecondaryGrey = '#ccc';
// const PrimaryRed = '#dd0031';
// const SecondaryBlue = '#006ddd';

@Component({
  selector: 'app-assign-customer',
  templateUrl: './assign-customer.component.html',
  styleUrls: ['./assign-customer.component.css']
})

export class AssignCustomerComponent implements OnInit {
  CustomerCode: string; CustomerName: string; Zone: number; PersonName: string;
  dataSource: any;
  selection: any; displayedColumns: any;
  CollectPersonList: {};
  ColpanyList: {};
  ZoneList: {};
  BrokerList: {};
  CustomerList: {}; //select:string; selected:string;
  //rows: PeriodicElement[] = [];
  SelectedCust: {};
  BrokerId: string;
  CPBrokerId: string;
  flag: any;
  CPName: string; CustCode: string; CustName: string; CPZone: number;
  x: string[];
  y: string[];
  // @ViewChild('ngxLoading', { static: false }) ngxLoadingComponent: NgxLoadingComponent;
  // @ViewChild('customLoadingTemplate', { static: false }) customLoadingTemplate: TemplateRef<any>;

  //public ngxLoadingAnimationTypes = ngxLoadingAnimationTypes;
  public loading = true;
  //public primaryColour = PrimaryWhite;
  //public secondaryColour = SecondaryGrey;
  //public coloursEnabled = false;
  //public loadingTemplate: TemplateRef<any>;
  //public config = { animationType: ngxLoadingAnimationTypes.none, primaryColour: this.primaryColour, secondaryColour: this.secondaryColour, tertiaryColour: this.primaryColour, backdropBorderRadius: '3px' };
  todo: [];
  done: [];
  constructor(private formbulider: FormBuilder, private http: HttpClient, private router: Router, private assignCust: AssignCustomerService, private toastr: ToastrService) { }

  ngOnInit() {
    this.LoadCollectPersonList();
    //this.LoadCustomerList();
    this.LoadAllZone();
    this.LoadBrokerList();
    this.flag = 0;
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  SearchInvoice() {
    this.LoadCustomerList();
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  private LoadCustomerList() {
    var _CustomerCode = '';
    var _CustomerName = '';
    var _Zone = 0;
    var _brokerId = '';
    if (this.BrokerId == undefined || this.BrokerId == '' || this.BrokerId == null) {
      this.toastr.warning("Please select broker", "Warning");
    }
    else {
      if (this.CustomerCode == undefined || this.CustomerCode == '') {
        _CustomerCode = null;
      } else {
        _CustomerCode = this.CustomerCode;
      }
      if (this.CustomerName == undefined || this.CustomerName == '')
        _CustomerName = null;
      else
        _CustomerName = this.CustomerName;

      if (this.Zone == undefined || this.Zone == 0)
        _Zone = null;
      else
        _Zone = Number(this.Zone);

      let CompanyId = Number(sessionStorage.getItem("CompanyId"));
      this.assignCust.getCustomerList(CompanyId, _CustomerCode, _CustomerName, _Zone, this.BrokerId).subscribe(
        _CustomerList => {
          console.log('Customer List : ' + JSON.stringify(_CustomerList.Payload));
          this.dataSource = new MatTableDataSource(JSON.parse(JSON.stringify(_CustomerList)).Payload);
          this.dataSource = this.dataSource;
          this.displayedColumns = ['select', 'UserID', 'Name', 'ContactNo', 'EmailID', 'CustAddress', 'Zone', 'BALANCE'];
          this.selection = new SelectionModel(true, [])
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        },
        error => {
          console.log('Failed while trying to load customer list data. ' + error);
        }
      );
    }
    this.CustomerCode = null; this.CustomerName = null;
    this.Zone = null;
  }

  private LoadCollectPersonList() {
    let CompanyId = Number(sessionStorage.getItem("CompanyId"));
    this.assignCust.getCollectPersonList(CompanyId).subscribe(
      _CollectPersonList => {
        console.log('Load collect person list : ' + JSON.stringify(_CollectPersonList.Payload));
        this.CollectPersonList = JSON.parse(JSON.stringify(_CollectPersonList)).Payload;
      },
      error => {
        console.log('Failed while trying to load collect person list. ' + error);
      });
  }

  private LoadAllZone() {
    let CompanyId =Number(sessionStorage.getItem("CompanyId"));
    this.assignCust.getAllZone(CompanyId).subscribe(
      _Zone => {
        console.log('Load zone list : ' + JSON.stringify(_Zone.Payload));
        this.ZoneList = JSON.parse(JSON.stringify(_Zone)).Payload;
      },
      error => {
        console.log('Failed while trying to load zone list. ' + error);
      });
  }

  AssignCustomerToCP() {
    let length = this.selection.selected.length;
    if (this.PersonName == undefined || this.PersonName == null && length == 0) {
      this.toastr.info("Please Select Collect Person and Customer", "Info")
    }
    else {
      var _CPCode = '';
      if (this.PersonName == undefined || this.PersonName == null)
        _CPCode = null;
      else
        _CPCode = this.PersonName;
      let CompanyId = Number(sessionStorage.getItem("CompanyId"));

      if (length > 0) {
        for (let obj of this.selection.selected) {
          console.log("UserId:" + obj.UserID);
          this.assignCust.insertCollectPersonCustomerRelation(this.PersonName, obj.UserID, CompanyId).subscribe(
            _SaveAssignCust => {
              //this.toastr.success("Data inserted successfully.", "Success");
            },
            error => {
              console.log("Error occured while trying to assign customer", error);
              this.toastr.error("Error occured while trying to assign customer.", "Erroe");
            }
          )
        }
        this.toastr.success("Data inserted successfully.", "Success");
        this.selection.clear();
        this.PersonName = null;
      }
      else {
        this.toastr.info("Please Select Customer", "Info")
      }
    }
  }

  selectRow($event, dataSource) {
    if ($event.checked) {
      console.log(dataSource.UserID);
    }
  }

  private LoadBrokerList() {
    let CompanyId = sessionStorage.getItem("CompanyId");
    this.assignCust.GetBrokerList(CompanyId).subscribe(
      _BrokerList => {
        console.log('Load broker list : ' + JSON.stringify(_BrokerList.Payload));
        this.BrokerList = JSON.parse(JSON.stringify(_BrokerList)).Payload;
      },
      error => {
        console.log('Failed while trying to load broker list. ' + error);
      });
  }

  OnChangeCustomer() {
    this.LoadCustomerList();
  }

  // bind done list box by cp selection
  BindCustomer() {
    var CPCode = '';
    if (this.CPName == undefined || this.CPName == null) {
      this.toastr.warning("Please select collect person", "Warning");
    }
    else {
      let CompanyId = sessionStorage.getItem("CompanyId");
      CPCode = this.CPName;
      this.assignCust.getCPCustomerListByCPCode(CPCode, CompanyId).subscribe(
        _AssigedCust => {
          this.done = JSON.parse(JSON.stringify(_AssigedCust)).Payload;
        },
        error => {
          console.log("Failed to load CP Customer list by cpcode" + error);
          this.toastr.error("Failed to load CP customer list by CP Code", "Error");
        }
      );
    }
  }

  SearchCustomer() {
    this.LoadCustomerListForUpdateCP();
  }

  private LoadCustomerListForUpdateCP() {
    var _CustomerCode = '';
    var _CustomerName = '';
    var _Zone = 0;
    if (this.CPBrokerId == undefined || this.CPBrokerId == '' || this.CPBrokerId == null) {
      this.toastr.warning("Please select broker", "Warning");
    }
    else {
      if (this.CustCode == undefined || this.CustCode == '') {
        _CustomerCode = null;
      } else {
        _CustomerCode = this.CustCode;
      }
      if (this.CustName == undefined || this.CustName == '')
        _CustomerName = null;
      else
        _CustomerName = this.CustName;

      let CompanyId = Number(sessionStorage.getItem("CompanyId"));

      this.assignCust.getCustByBrokerId(CompanyId, _CustomerCode, _CustomerName, this.CPBrokerId).subscribe(
        _CPCustomerList => {
          console.log('Customer List : ' + JSON.stringify(_CPCustomerList.Payload));
          this.todo = JSON.parse(JSON.stringify(_CPCustomerList)).Payload;
        },
        error => {
          console.log('Failed while trying to load customer list data. ' + error);
        }
      );
    }
    this.CustCode = null; this.CustName = null;
    this.CPZone = null;
  }


  // bind todo list by broker change  
  OnCPChangeCustomer() {
    this.LoadCustomerListForUpdateCP();
  }


  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      console.log('dropped Event', `> dropped '${event.item.data}' into '${event.container.id}'`);
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      if (this.done == undefined || this.done == null) {
        this.toastr.warning("Please select collect person", "Warning");
      }
      else {
        console.log('dropped Event', `> dropped '${event.item.data}' into '${event.container.id}'`);
        transferArrayItem(
          event.previousContainer.data,
          event.container.data,
          event.previousIndex,
          event.currentIndex
        );
      }
    }
    this.x = this.todo;
    this.y = this.done;
  }

  UpdateCustomerToCP() {
    var CPName = '';
    let CompanyId = Number(sessionStorage.getItem("CompanyId"));
    this.assignCust.DeleteCPCustRelation(this.CPName, sessionStorage.getItem("CompanyId")).subscribe(
      _DeleteCPCustomer => {
      },
      error => {
        console.log("Unable to remove CP customer", error);
      }
    );
    if (this.CPName == undefined || this.CPName == null) {
      this.toastr.warning("Please select collect person", "Warning");
      CPName = null;
    }
    else {
      CPName = this.CPName;
      for (var i = 0; i < this.y.length; i++) {
        console.log("container CP - " + this.y[i]["UserID"]); // output
        this.assignCust.UpdateCPCustomer(CPName, this.y[i]["UserID"], CompanyId).subscribe(
          _UpdateCPCust => {
          },
          error => {
            console.log("Failed to update CP Customer" + error);
            this.toastr.error("Failed to update CP customer", "Error");
          }
        );
      }
      this.toastr.success("CP Customer Updated Successfully", "Success");
      this.todo = null;
      this.done = null;
      this.y = null; this.x = null;
    }
  }


}
