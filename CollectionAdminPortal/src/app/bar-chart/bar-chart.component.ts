import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.css']
})
export class BarChartComponent implements OnInit {


  barChartOptions: ChartOptions = {
    responsive: true,
  };
  barChartLabels: Label[] = ['B1', 'B2', 'B3', 'B4', 'B5', 'B6'];
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartPlugins = [];

  barChartData: ChartDataSets[] = [
    { data: [45, 37, 60, 70, 46, 33], label: 'This Month' },
    {data: [50, 40,30,67,100,50], label: 'Last Month'}
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
