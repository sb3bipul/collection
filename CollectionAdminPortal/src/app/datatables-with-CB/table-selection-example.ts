import {SelectionModel} from '@angular/cdk/collections';
import {Component} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { Input } from '@angular/core';


export interface PeriodicElement {
  position: number;
  Amount: string;
  InvoiceId: string;
  Date: string;
  Vendor: string;
  TimeInQueue: string;
  Comments: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1,InvoiceId: 'INV-00956', Amount: '$459.30', Date: '12-08-19', Vendor: 'Triveni IT', TimeInQueue: '10', Comments: 'With Accounts'},
  {position: 2,InvoiceId: 'INV-00349', Amount: '$125.30', Date: '12-08-19', Vendor: 'Infor', TimeInQueue: '3', Comments: 'Pending Approval'},
  {position: 3,InvoiceId: 'INV-00853', Amount: '$10.503', Date: '12-08-19', Vendor: 'Tibco', TimeInQueue: '5', Comments: 'With Accounts'},
  {position: 4,InvoiceId: 'INV-00452', Amount: '$90', Date: '12-08-19', Vendor: 'Alphabet', TimeInQueue: '90', Comments: 'Approved'},
  {position: 5,InvoiceId: 'INV-00123', Amount: '$15,900', Date: '12-08-19', Vendor: 'National Sales', TimeInQueue: '12', Comments: 'Pending Approval'},
  {position: 6,InvoiceId: 'INV-00853', Amount: '$115.06', Date: '12-08-19', Vendor: 'AT&T', TimeInQueue: '45', Comments: 'Pending Approval'},
  {position: 7,InvoiceId: 'INV-00153', Amount: '$1,090', Date: '12-08-19', Vendor: 'Verizon', TimeInQueue: '54', Comments: 'Approved'},
  {position: 8,InvoiceId: 'INV-00788', Amount: '$555.50', Date: '12-08-19', Vendor: 'Source Consulting', TimeInQueue: '5', Comments: 'Payment Initiated'},
  {position: 9,InvoiceId: 'INV-00326', Amount: '$8,563', Date: '12-08-19', Vendor: 'United', TimeInQueue: '7', Comments: 'Pending Approval'},
  {position: 10,InvoiceId: 'INV-00759', Amount: '$10,960.20', Date: '12-08-19', Vendor: 'IBM', TimeInQueue: '10', Comments: 'With Accounts'},
  {position: 11,InvoiceId: 'INV-00999', Amount: '$886.90', Date: '12-08-19', Vendor: 'Apple', TimeInQueue: '13', Comments: 'Payment Initiated'},
  {position: 12,InvoiceId: 'INV-00223', Amount: '$459.30', Date: '12-08-19', Vendor: 'Microsoft', TimeInQueue: '14', Comments: 'With Admin'}
];

/**
 * @title Table with selection
 */
@Component({
  selector: 'table-selection-example',
  styleUrls: ['table-selection-example.css'],
  templateUrl: 'table-selection-example.html',
})
export class TableSelectionExample {

  // checked = false;
  // indeterminate = false;
  // labelPosition: 'before' | 'after' = 'after';
  // disabled = false;

  displayedColumns: string[] = ['select', 'InvoiceId', 'Amount', 'Vendor', 'Date', 'TimeInQueue', 'Comments', 'Action'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  selection = new SelectionModel<PeriodicElement>(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: PeriodicElement): string {
    debugger;
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

}


/**  Copyright 2019 Google LLC. All Rights Reserved.
    Use of this source code is governed by an MIT-style license that
    can be found in the LICENSE file at http://angular.io/license */