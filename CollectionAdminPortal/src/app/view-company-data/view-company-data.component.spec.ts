import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewCompanyDataComponent } from './view-company-data.component';

describe('ViewCompanyDataComponent', () => {
  let component: ViewCompanyDataComponent;
  let fixture: ComponentFixture<ViewCompanyDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewCompanyDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewCompanyDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
