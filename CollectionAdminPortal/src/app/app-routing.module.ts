﻿import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyQueueComponent } from './Invoice/myQueue.component';
import { LoginComponent } from './login/login.component';
import { from } from 'rxjs';
import { HomeLayoutComponent } from './home-layout/home-layout.component';
import { LoginLayoutComponent } from './login-layout/login-layout.component';
import { AddCollectPersonComponent } from './add-collect-person/add-collect-person.component';
import { CollectPersonDetailsComponent } from './collect-person-details/collect-person-details.component';
import { AssignCustomerComponent } from './assign-customer/assign-customer.component';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { DataTableComponent } from './data-table/data-table.component';
import { EmailConfigComponent } from './email-config/email-config.component';
import { CreateCompanyComponent } from './create-company/create-company.component';
import { ViewCompanyDataComponent } from './view-company-data/view-company-data.component';
import { UICustomizationComponent } from './uicustomization/uicustomization.component';
import { CustomUXComponent } from './custom-ux/custom-ux.component'
const routes: Routes = [
  {
    path: '',
    component: LoginLayoutComponent,
    children: [
      { path: '', component: LoginComponent, pathMatch: 'full' },
      { path: 'login', component: LoginComponent, pathMatch: 'full' }
    ]
  },
  {
    path: '',
    component: HomeLayoutComponent,
    children: [
      //{ path: '', redirectTo: 'myQueue', pathMatch: 'full' },
      { path: 'myQueue', component: MyQueueComponent },
      { path: 'addPerson', component: AddCollectPersonComponent },
      { path: 'viewPerson', component: CollectPersonDetailsComponent },
      { path: 'assignCustomer', component: AssignCustomerComponent },
      { path: 'allCustomer', component: CustomerListComponent },
      { path: 'emailConfig', component: EmailConfigComponent },
      // { path: 'createCom', component: CreateCompanyComponent },
      // { path: 'viewCompany', component: ViewCompanyDataComponent },
      // { path: 'uiCustom', component: UICustomizationComponent },
      // { path: 'customUX', component: CustomUXComponent }
    ]
  },
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {
  static forRoot(arg0: any): any {
    throw new Error("Method not implemented.");
  }
}
