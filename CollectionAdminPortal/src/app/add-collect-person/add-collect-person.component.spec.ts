import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCollectPersonComponent } from './add-collect-person.component';

describe('AddCollectPersonComponent', () => {
  let component: AddCollectPersonComponent;
  let fixture: ComponentFixture<AddCollectPersonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCollectPersonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCollectPersonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
