import { Component, OnInit, ViewChild, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoginService } from '../_services/login.service';
import { ILogin, UserLogin, Login } from '../_models/login';
import 'rxjs/add/operator/map';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  private loggedIn = new BehaviorSubject<boolean>(false);
  public loginForm: FormGroup;
  public LoginResponse: Login[];
  UserId: string;
  Password: string;
  CompanyId: string;
  storage: any;
  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }

  constructor(private loginServices: LoginService, private http: HttpClient, private router: Router, private location: Location, private toastr: ToastrService) {
    // this.user = new UserLogin();
    // this.strPassword = "password";
    // this.loginModel = new ILogin();
  }

  ngOnInit() {
    this.CompanyId = "2";
  }


  UserLogin() {
    let CompanyId = this.CompanyId;
    if (this.isValid()) {
      this.loginServices.getUserToken(this.UserId, this.Password, CompanyId).subscribe(
        _Login => {
          this.LoginResponse = JSON.parse(JSON.stringify(_Login)).Payload;
          for (let key of this.LoginResponse) {
            if (key.role == "CollectionAdmin") {
              if (key.Authorization == "Success") {
                this.loggedIn.next(true);
                //if (key.role == "CollectionAdmin") {
                this.router.navigate(['./myQueue']);
                // }
                // else {
                //   this.router.navigate(['./createCom']);
                // }
                sessionStorage.setItem("UserRole", key.role);
                sessionStorage.setItem("CompanyId", this.CompanyId);
                //alert(sessionStorage.getItem("UserRole"));              
              }
              else {
                this.toastr.error("Incorrect userid or password", "Error");
              }
            }
            else{
              this.toastr.error("Access denied for this user", "Error");
              this.logout();
            }
          }
        },
        error => {
          console.log("Login failed!" + error);
          this.toastr.error("Login failed for this user", "Error");
        }
      );
    }
  }

  isValid(): boolean {
    if (this.UserId == undefined) {
      this.toastr.error("UserId is Required !", "Error");
      return false;
    }
    if (this.Password == undefined) {
      this.toastr.error("Password is required !", "Error");
      return false;
    }
    else {
      return true;
    }
  }

  // cancel(): void {
  //   this.canClick = false;
  //   this.user.UserId = undefined;
  //   this.user.Password = undefined;
  //   this.user.CompanyId = undefined;
  //   this.loginModel.Authorization = undefined;
  //   this.userId = "";
  //   this.password = "";
  //   this.CompanyId = "";
  // }

  public hasError = (controlName: string, errorName: string) => {
    return this.loginForm.controls[controlName].hasError(errorName);
  }

  public onCancel = () => {
    this.location.back();
  }

  logout() {
    this.loggedIn.next(false);
    this.router.navigate(['/login']);
    sessionStorage.clear();
  }

}
