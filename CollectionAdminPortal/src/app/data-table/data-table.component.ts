﻿import { AfterViewInit, Component, OnInit, ViewChild, enableProdMode, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, NgForm } from "@angular/forms";
import { MatPaginator } from '@angular/material/paginator';
import { DatePipe } from '@angular/common';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { UserService } from '../_services/index';
import { IInvoice, IInvoicePdfStatus, InvoiceStatusLog, DailyCollection, MonthlyCollection, TotalCollection, TotalCP, TotalAssignedCust } from '../_models/index';
import { HttpClient } from '@angular/common/http';
import { ModalService } from '../_modal';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-data-table',
    templateUrl: './data-table.component.html',
    styleUrls: ['./data-table.component.css']
})
export class DataTableComponent {
    public displayedColumns = [];
    public invoices: IInvoice[]; public invoicesHistory: InvoiceStatusLog[]; public invoicesLog = [];
    form: FormGroup; InvNumberEdit: string; ReceiptNo: string; CustomerId: string;
    CustomerCode: string; startdate: Date; todate: Date; CustName: string; PaymentDate: Date;
    CollectionMode: string; Amount: number; bankname: string; Accountno: string; branchname: string;
    DigitalMode: string; checkNo: string; Ftrstname: string; Lastname: string;
    CardType: string; InvoiceNo: string; Comment: string;
    public dailyCollect: DailyCollection[];
    public monthlyCollect: MonthlyCollection[];
    public YearlyCollect: TotalCollection[];
    TotalCollect: string; TodayCollect: string; CurrentMonth: string;
    public totalCP: TotalCP[]; public TotalAssignCust: TotalAssignedCust[];
    CPTotal: string; AssignCustTotal: string;
    dataSource: any; dataSource1: any;


    constructor(private httpClient: HttpClient, private userService: UserService, private datePipe: DatePipe, private modalService: ModalService, public fb: FormBuilder, private toastr: ToastrService) {
        var date = new Date();// first date of month
        var fmDt = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('01').slice(-2);
        this.startdate = new Date(fmDt);
        var toDt = new Date();
        // let to_date = this.datePipe.transform(toDt, 'yyyy-MM-dd');
        this.todate = new Date(toDt);
        // this.form = this.fb.group({
        //     name: [''],
        //     avatar: [null]
        // })

        //startdate=
    }

    ngOnInit() {
        this.LoadSearchInvoice();
        this.LoadDashborddata();
    }

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;

    ngAfterViewInit() {
        
    }

    SearchInvoice() {
        this.LoadSearchInvoice();
    }

    private LoadSearchInvoice() {
        var _receiptNo = '';
        var _CustomerId = '';

        let frDt = this.datePipe.transform(this.startdate, 'yyyy-MM-dd');
        let toDt = this.datePipe.transform(this.todate, 'yyyy-MM-dd');
        console.log('dri : ' + this.ReceiptNo);

        if (this.ReceiptNo == undefined || this.ReceiptNo == '')
            _receiptNo = null;
        else
            _receiptNo = this.ReceiptNo;
        if (this.CustomerCode == undefined || this.CustomerCode == '')
            _CustomerId = null;
        else
            _CustomerId = this.CustomerCode;

        this.userService.searchPaymentData(frDt, toDt, _receiptNo, _CustomerId).subscribe(
            _invoiceDetial => {
                console.log('Returned search data : ' + JSON.stringify(_invoiceDetial.Payload));
                this.dataSource = new MatTableDataSource(JSON.parse(JSON.stringify(_invoiceDetial)).Payload);
                this.dataSource = this.dataSource;
                this.displayedColumns = ["CustomerId", "PaymentReceipt", "CollectedBy", "CustName", "PaymentDate", "CollectionMode", "PaymentStatus", "Amount", "collection"];
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
                //setTimeout(() => this.dataSource.paginator = this.paginator);
            },
            error => {
                console.log('Failed while trying to search the payment receipt. ' + error);
            });
    }

    // openModal(id: string, invoiceID: string, InvoiceNo: string) {
    //     this.modalService.open(id);
    //     console.log('Rejecting ' + invoiceID);        
    // }

    closeModal(id: string) {
        this.modalService.close(id);
    }

    openPaymentDetails(id: string, ReceiptNo: string, CustomerId: string, Customername: string, Amount: number, PaymentMode: string, BankName: string, AccountNo: string, BranchName: string, DigitalMode: string, PaymentDate: Date, CheckNo: string, FirstName: string, LastName: string, CardType: string, InvoiceNo: string, Comment: string) {
        this.modalService.open(id);
        this.userService.getPaymentReceiptImage(ReceiptNo).subscribe(
            _invoiceStatus => {
                console.log('Returned search data : ' + JSON.stringify(_invoiceStatus.Payload));
                this.dataSource1 = new MatTableDataSource(JSON.parse(JSON.stringify(_invoiceStatus)).Payload);
                this.dataSource1 = this.dataSource1;
                this.invoicesLog = ["Paymentimage"];
                //this.dataSource.paginator = this.paginator;
                //this.dataSource.sort = this.sort;                
            },
            error => {
                console.log('Failed while trying to search the payment receipt. ' + error);
            });

        this.InvNumberEdit = ReceiptNo;
        this.CustomerId = CustomerId;
        this.CustName = Customername;
        this.PaymentDate = PaymentDate;
        this.CollectionMode = PaymentMode;
        this.Amount = Amount;
        this.bankname = BankName;
        this.Accountno = AccountNo;
        this.branchname = BranchName;
        this.DigitalMode = DigitalMode;
        this.checkNo = CheckNo;
        this.Ftrstname = FirstName;
        this.Lastname = LastName;
        this.CardType = CardType;
        this.InvoiceNo = InvoiceNo;
        this.Comment = Comment;
        this.dataSource1 = null;
    }

    private LoadDashborddata() {
        let CompanyId = Number(sessionStorage.getItem("CompanyId"));
        this.userService.getDashbordData(CompanyId).subscribe(
            _dashBordData => {
                console.log('Load daily data : ' + JSON.stringify(_dashBordData.DailyCollection));
                console.log('Load molthly data : ' + JSON.stringify(_dashBordData.MonthlyCollection));
                console.log('Load total data : ' + JSON.stringify(_dashBordData.TotalCollection));

                this.dailyCollect = JSON.parse(JSON.stringify(_dashBordData)).DailyCollection;
                this.monthlyCollect = JSON.parse(JSON.stringify(_dashBordData)).MonthlyCollection;
                this.YearlyCollect = JSON.parse(JSON.stringify(_dashBordData)).TotalCollection;
                this.totalCP = JSON.parse(JSON.stringify(_dashBordData)).TotalCP;
                this.TotalAssignCust = JSON.parse(JSON.stringify(_dashBordData)).TotalAssignedCust;

                this.TotalCollect = this.YearlyCollect[0].TotalCollection;
                this.TodayCollect = this.dailyCollect[0].DailyCollection;
                this.CurrentMonth = this.monthlyCollect[0].MonthlyCollection;
                this.CPTotal = this.totalCP[0].TotalCP;
                this.AssignCustTotal = this.TotalAssignCust[0].TotalAssignedCust;

            },
            error => {
                console.log('Failed while trying to load dashboard data. ' + error);
            });
    }

    DeleteFailedAndPendingPayment(ReceiptNo: string) {
        let CompanyId = sessionStorage.getItem("CompanyId");
        this.userService.DeletePendingPayment(ReceiptNo, CompanyId).subscribe(
            _RmvPendingPayment => {
                this.toastr.success("Payment successfully removed", "Success");
                this.LoadSearchInvoice();
            }, error => {
                console.log("Failed to remove pending payment" + error);
                this.toastr.error("Unable to remove payment", "Error");
            }
        );
    }
}
