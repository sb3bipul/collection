import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ModalService } from '../_modal';
import { UiConfigService } from '../_services/ui-config.service';

@Component({
  selector: 'app-uicustomization',
  templateUrl: './uicustomization.component.html',
  styleUrls: ['./uicustomization.component.css']
})

export class UICustomizationComponent implements OnInit {
  client: string;
  CustomerData: string;
  CustConfigDt: any;
  Company: {};
  paymentmethod: string;

  PayMethod = [
    { id: 'venmo', name: 'Venmo', isChecked: false },
    { id: 'zelle', name: 'Zelle', isChecked: false },
    { id: 'paypal', name: 'PayPal', isChecked: false },
    { id: 'banktransfer', name: 'Bank Transfer ACH', isChecked: false },
    { id: 'creditcard', name: 'Credit Card', isChecked: false },
    { id: 'Paytm', name: 'Paytm', isChecked: false },
  ];
  constructor(private configservice: UiConfigService, private toastr: ToastrService, private modalService: ModalService, private router: Router) { }

  ngOnInit() {
    this.CustomerData = "IsERP";

    this.configservice.getCompanyList().subscribe((data) => {
      console.log(data);
      this.Company = JSON.parse(JSON.stringify(data)).Payload;
    },
      error => {
        this.toastr.error("Unable to load company data", "Error");
      }
    );
  }

  Save() {
    var _IsERP: boolean;
    var _IsOther: boolean;
    var _Createdby = "";
    if (this.client == undefined || this.client == null) {
      this.toastr.warning("Please select client", "Warning");
    }
    else {
      if (this.CustomerData == "IsERP" && (this.CustomerData != undefined || this.CustomerData != null)) {
        _IsERP = true;
        _IsOther = false;
      }
      else {
        _IsERP = false;
        _IsOther = true;
      }
      this.configservice.InsertCustomerDataConfig(this.client, this.client, _IsERP, _IsOther, _Createdby).subscribe(
        _CustCongig => {
          this.toastr.success("Customer data config saved successfully", "Success");
          this.savePaymentConfig(this.client);
        },
        _error => {
          this.toastr.error("Unable to save config data", "Error");
          console.log("Unable to save cust config data" + _error);
        }
      );
    }

  }

  cancel() {
    this.router.navigate(['./myQueue']);
  }

  savePaymentConfig(ClientName) {
    if (this.paymentmethod == undefined || this.paymentmethod == null) {
      this.toastr.warning("Please select atleast one payment mathod", "Warning");
    }
    else {
      var i;
      for (i = 0; i < this.paymentmethod.length; i++) {
        this.configservice.insertPaymentMethodConfigData(ClientName, this.paymentmethod[i]).subscribe(
          _PMConfigDt => {
            //this.toastr.success("Payment method config successfully saved", "Success");
          },
          _error => {
            this.toastr.error("Unable to save payment config data", "Error");
          }
        )
      }
    }
    //this.client = null;
  }

  changeSuit() {
    this.configservice.getCustConfigdataByClientName(this.client).subscribe(
      _CustConfigData => {
        this.CustConfigDt = JSON.parse(JSON.stringify(_CustConfigData)).Payload;
        if (this.CustConfigDt[0]["ClientName"] != undefined || this.CustConfigDt[0]["ClientName"] != null || this.CustConfigDt[0]["ClientName"] != "-") {
          if (this.CustConfigDt[0]["IsERP"] == "True") {
            this.CustomerData = "IsERP";
          }
          else {
            this.CustomerData = "IsOthers";
          }
        }
        else {
          this.CustomerData = "IsERP";
        }
      }
    );
  }

  demo() {
    alert(this.paymentmethod);
  }
}
