import { Component } from "@angular/core";

@Component({
    selector: 'app-route',
    templateUrl: './payment.component.html',
    styles: [
        `.pageTitle{
            margin: 1.575rem 10px .46rem !important;
        }
        .card{
            margin: 0px !important;
        }
        `
    ]
})

export class PaymentComponent{
    
}