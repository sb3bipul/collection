import { AfterViewInit, Component, OnInit, ViewChild, enableProdMode } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { DatePipe } from '@angular/common';
import { PaymentDataTableDataSource, PaymentDataTableItem } from './payment-data-table-datasource';
import { UserService } from '../_services/index';
import {  IInvoice } from '../_models/index';
import { Observable } from 'rxjs/Observable';
import { throwError } from 'rxjs';
enableProdMode();
@Component({
  selector: 'app-payment-data-table',
  templateUrl: './payment-data-table.component.html',
  styleUrls: ['./payment-data-table.component.css']
})
export class PaymentDataTableComponent implements AfterViewInit, OnInit {
  title = 'app';
  public invoices:IInvoice[];
  invFullLoadData;
  constructor(private userService: UserService, private datePipe: DatePipe){}
    rowData: any;
    columnDefs = [
        {headerName: 'UserID', field: 'UserID' },
        {headerName: 'UserType', field: 'UserType' },
        {headerName: 'SalesmanName', field: 'SalesmanName'}
    ];

    
  // @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ViewChild(MatSort) sort: MatSort;
  // @ViewChild(MatTable) table: MatTable<PaymentDataTableItem>;
  // dataSource: PaymentDataTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  //displayedColumns = ['id', 'username', 'name', 'lastActivity', 'verified', 'role', 'status', 'Action'];
//https://www.ag-grid.com/angular-grid/?gclid=Cj0KCQjw_ez2BRCyARIsAJfg-kvOOwh8T9U0mOZKZbz2hJfYGNUYCAcdyFLAsN29Zq8tqJIKkJ9YLWgaAp3GEALw_wcB
  ngOnInit() {
    this.LoadUser();
  //   this.rowData = [
  //     { UserID: 'Toyota', UserType: '12345', SalesmanName: 'Smith' },
  //     { UserID: 'Ford', UserType: '54321', SalesmanName: 'John' },
  //     { UserID: 'Porsche', UserType: '22334', SalesmanName: 'Steve' }
  // ];
    //this.dataSource = new PaymentDataTableDataSource();
  }
  private LoadUser() {
    this.userService.getUser().subscribe(userDetial => 
      {
        this.rowData = userDetial;
          console.log('User data : ' + JSON.stringify( userDetial));
      });
    
  }

  ngAfterViewInit() {
    // this.dataSource.sort = this.sort;
    // this.dataSource.paginator = this.paginator;
    // this.table.dataSource = this.dataSource;
  }
}
