import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';

// TODO: Replace this with your own data model type
export interface PaymentDataTableItem {
  name: string;
  username: string;
  id: number;
  lastActivity: string;
  verified: string;
  role: string;
  status: string;
}

// TODO: replace this with real data from your application
const EXAMPLE_DATA: PaymentDataTableItem[] = [
  {id: 301, username: 'dean3004', name: 'Dean Stanley', lastActivity: '30/08/2019', verified: 'Yes', role: 'User', status: 'Banned'},
  {id: 302, username: 'zena0604', name: 'Zena Buckley', lastActivity: '20/09/2019', verified: 'Yes', role: 'Staff', status: 'Active'},
  {id: 303, username: 'delilah0301', name: 'Delilah Moon', lastActivity: '30/08/2019', verified: 'No', role: 'Staff', status: 'Banned'},
  {id: 304, username: 'hillary1807', name: 'Hillary Rasmussen', lastActivity: '30/08/2019', verified: 'Yes', role: 'User', status: 'Active'},
  {id: 305, username: 'herman2003', name: 'Herman Tate', lastActivity: '20/09/2019', verified: 'No', role: 'User', status: 'Banned'},
  {id: 306, username: 'kuame3008', name: 'Kuame Ford', lastActivity: '30/08/2019', verified: 'No', role: 'Staff', status: 'Active'},
  {id: 307, username: 'fulton2009', name: 'Fulton Stafford', lastActivity: '20/09/2019', verified: 'No', role: 'Staff', status: 'Active'},
  {id: 308, username: 'piper0508', name: 'Piper Jordan', lastActivity: '05/08/2020', verified: 'Yes', role: 'User', status: 'Active'},
  {id: 309, username: 'neil1002', name: 'Neil Sosa', lastActivity: '10/02/2019', verified: 'Yes', role: 'User', status: 'Banned'},
  {id: 400, username: 'caldwell2402', name: 'Caldwell Chapman', lastActivity: '24/02/2020', verified: 'No', role: 'Use', status: 'Banned'},
  {id: 401, username: 'wesley0508', name: 'Wesley Oneil', lastActivity: '20/09/2019', verified: 'No', role: 'Staff', status: 'Active'},
  {id: 402, username: 'tallulah2009', name: 'Tallulah Fleming', lastActivity: '10/02/2019', verified: 'Yes', role: 'Staff', status: 'Banned'},
  {id: 403, username: 'iris2505', name: 'Iris Maddox', lastActivity: '10/02/2019', verified: 'Yes', role: 'User', status: 'Banned'},
  {id: 404, username: 'caleb1504', name: 'Caleb Bradley', lastActivity: '20/09/2019', verified: 'No', role: 'Staff', status: 'Active'},
  {id: 405, username: 'illiana0410', name: 'Illiana Grimes', lastActivity: '10/02/2019', verified: 'No', role: 'Staff', status: 'Active'}
];

/**
 * Data source for the PaymentDataTable view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class PaymentDataTableDataSource extends DataSource<PaymentDataTableItem> {
  data: PaymentDataTableItem[] = EXAMPLE_DATA;
  paginator: MatPaginator;
  sort: MatSort;

  constructor() {
    super();
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<PaymentDataTableItem[]> {
    // Combine everything that affects the rendered data into one update
    // stream for the data-table to consume.
    const dataMutations = [
      observableOf(this.data),
      this.paginator.page,
      this.sort.sortChange
    ];

    return merge(...dataMutations).pipe(map(() => {
      return this.getPagedData(this.getSortedData([...this.data]));
    }));
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {}

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: PaymentDataTableItem[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: PaymentDataTableItem[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'name': return compare(a.name, b.name, isAsc);
        case 'id': return compare(+a.id, +b.id, isAsc);
        default: return 0;
      }
    });
  }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a: string | number, b: string | number, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
