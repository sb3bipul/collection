import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import { UiConfigService } from '../_services/ui-config.service';
import { ToastrService } from 'ngx-toastr';
import { ModalService } from '../_modal';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-email-config',
  templateUrl: './email-config.component.html',
  styleUrls: ['./email-config.component.css']
})
export class EmailConfigComponent implements OnInit {
  Customer: boolean; Company: boolean; Broker: boolean; CollectP: boolean;
  userrole: string;
  EmailConfig: any;
  constructor(private configservice: UiConfigService, private toastr: ToastrService, private modalService: ModalService, private router: Router) { }

  ngOnInit() {

    this.userrole = 'CPAdmin';

    this.configservice.getConfigdata(2).subscribe(
      _ConfigDt => {
        this.EmailConfig = JSON.parse(JSON.stringify(_ConfigDt)).Payload;
        if (this.EmailConfig[0]["IsEmailToCompany"] == undefined || this.EmailConfig[0]["IsEmailToCompany"] == null || this.EmailConfig[0]["IsEmailToCompany"] == '-') {
          this.Company = true;
          this.Customer = true;
        }
        else {
          if (this.EmailConfig[0]["IsEmailToCompany"] == "True") {
            this.Company = true;
          } else {
            this.Company = false;
          }
          if (this.EmailConfig[0]["IsEmailToCustomer"] == "True") {
            this.Customer = true
          } else {
            this.Customer = false;
          }
          if (this.EmailConfig[0]["IsEmailToBroker"] == "True") {
            this.Broker = true;
          } else {
            this.Broker = false;
          }
          if (this.EmailConfig[0]["IsEmailToCP"] == "True") {
            this.CollectP = true;
          } else {
            this.CollectP = false;
          }
        }
        console.log("data ------------" + this.EmailConfig[0]["IsEmailToCompany"]);
      },
      _error => {
        console.log("Unable to fatch email config data" + _error);
      }
    );
  }
  Save() {
    var _Company: boolean;
    var _Customer: boolean;
    var _Broker: boolean;
    var _CP: boolean;
    if (this.Company == undefined || this.Company == null) {
      _Company = false;
    }
    else {
      _Company = this.Company;
    }
    if (this.Customer == undefined || this.Customer == null) {
      _Customer = false;
    }
    else {
      _Customer = this.Customer;
    }
    if (this.Broker == undefined || this.Broker == null) {
      _Broker = false;
    }
    else {
      _Broker = this.Broker;
    }
    if (this.CollectP == undefined || this.CollectP == null) {
      _CP = false;
    }
    else {
      _CP = this.CollectP;
    }

    this.configservice.InsertEmailConfigdata(_Company, _Broker, _CP, _Customer, this.userrole).subscribe(
      _saveemailconfig => {
        this.toastr.success("Data saved successfully", "Success");
      },
      error => {
        console.log("Unable to saved data" + error);
        this.toastr.error("Unable to saved data", "Error");
      }
    )
  }
  cancel() {
    this.router.navigate(['./myQueue']);
  }


}
