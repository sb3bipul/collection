export class AssignCustomer {
    CPCode: string;
    CustomerCode: string;
    CompanyId: number;
}

export class CustomerList {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}

export class CollectPersonList {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}

export class ZoneList {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}

export class CustCollectPerson {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}

export class BrokerList {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}

export class DeleteCP {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}

export class CustomerListCP {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}
export class CPCustomerListByCP {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}

export class CustByBroker {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}

export class UpdateCPCust {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}

export class CPCustRelation {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}