enum MessageType {
  Success = "Success",
  Error = "Error",
  Warning = "Warning",
  Information = "Information",
}

export class Message{
  type: string //this will hold Success, Error, Warning, Information
  summary: string //summary message
  detail: string; // detail message
}
