﻿import { routing } from './app.routing';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
// import { DataTablesModule } from 'angular-datatables';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MyQueueComponent } from './Invoice/myQueue.component';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { DataTableComponent } from './data-table/data-table.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { UserService } from './_services/index';
import { AuthGuard } from './_guards/index';
import { A11yModule } from '@angular/cdk/a11y';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { PortalModule } from '@angular/cdk/portal';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { CdkTableModule } from '@angular/cdk/table';
import { CdkTreeModule } from '@angular/cdk/tree';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatBadgeModule } from '@angular/material/badge';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatStepperModule } from '@angular/material/stepper';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
// import {MatSortModule} from '@angular/material/sort';
// import {MatTableModule} from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTreeModule } from '@angular/material/tree';
import { ApprovalDataTableComponent } from './approval-data-table/approval-data-table.component';
import { TableSelectionExample } from './datatables-with-CB/table-selection-example';
import { PaymentDataTableComponent } from './payment-data-table/payment-data-table.component';
import { PaymentComponent } from './Account/payment.component';
import { LineChartComponent } from './line-chart/line-chart.component';
import { ChartsModule } from 'ng2-charts';
//import { DashboardComponent } from './dashboard.component';
import { BarChartComponent } from './bar-chart/bar-chart.component';
import { LoginComponent } from './login/login.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { HeaderComponent } from './header/header.component';
import { LoginLayoutComponent } from './login-layout/login-layout.component';
import { HomeLayoutComponent } from './home-layout/home-layout.component';
//import { ReportsComponent } from './reports.component';
import { HttpModule } from '@angular/http';
import { DatePipe, HashLocationStrategy, LocationStrategy } from '@angular/common';
import { AgGridModule } from 'ag-grid-angular';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from './_modal';
import { AddCollectPersonComponent } from './add-collect-person/add-collect-person.component';
import { CollectPersonDetailsComponent } from './collect-person-details/collect-person-details.component';
import { AssignCustomerComponent } from './assign-customer/assign-customer.component';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { HttpClient } from '@angular/common/http';
import { NgxLoadingModule } from 'ngx-loading';
import { OverlayModule } from '@angular/cdk/overlay';
import { EmailConfigComponent } from './email-config/email-config.component';
import { CreateCompanyComponent } from './create-company/create-company.component';
import { ViewCompanyDataComponent } from './view-company-data/view-company-data.component';
import { UICustomizationComponent } from './uicustomization/uicustomization.component';
import { CustomUXComponent } from './custom-ux/custom-ux.component';


@NgModule({
  declarations: [
    AppComponent,
    MyQueueComponent,
    PaymentComponent,
    DataTableComponent,
    ApprovalDataTableComponent,
    PaymentDataTableComponent,
    TableSelectionExample,
    LineChartComponent,
    //DashboardComponent,
    BarChartComponent,
    LoginComponent,
    SidenavComponent,
    HeaderComponent,
    LoginLayoutComponent,
    HomeLayoutComponent,
    AddCollectPersonComponent,
    CollectPersonDetailsComponent,
    AssignCustomerComponent,
    CustomerListComponent,
    EmailConfigComponent,
    ViewCompanyDataComponent,
    CreateCompanyComponent,
    UICustomizationComponent,
    CustomUXComponent],
  imports: [
    routing,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatCheckboxModule,
    A11yModule,
    ClipboardModule,
    CdkStepperModule,
    CdkTableModule,
    CdkTreeModule,
    DragDropModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    // MatSortModule,
    // MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    PortalModule,
    ScrollingModule,
    ChartsModule,
    HttpModule,
    AgGridModule.withComponents([]),
    HttpClientModule,
    ModalModule,
    ToastrModule.forRoot(),
    NgxLoadingModule.forRoot({}),
    OverlayModule
    //HttpClient
  ],
  //exports: [],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [AuthGuard, DatePipe, UserService, { provide: LocationStrategy, useClass: HashLocationStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule { }
