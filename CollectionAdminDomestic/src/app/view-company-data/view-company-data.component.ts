import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ToastrService } from 'ngx-toastr';
import { ZipOperator } from 'rxjs/internal/observable/zip';
import { ModalService } from '../_modal';
import { CollectPersonService } from '../_services/collect-person.service';
import { CompanyServiceService } from '../_services/company-service.service';

@Component({
  selector: 'app-view-company-data',
  templateUrl: './view-company-data.component.html',
  styleUrls: ['./view-company-data.component.css']
})
export class ViewCompanyDataComponent implements OnInit {
  startdate: Date;
  todate: Date;
  CompName: string;
  dataSource: any;
  displayedColumns: any;

  // Edit Model control
  CompanyName: string;
  Contact: string;
  EdEmailId: string;
  ContactPName: string;
  CPEmail: string;
  EdStreet: string;
  EdCity: string;
  Edcountry: string;
  EDState: string;
  EdZip: string; EdFax: string; EdURL: string;
  countries: {}; states: {};
  EdCompanyId: number;
  constructor(private collection: CollectPersonService, private toastr: ToastrService, private modalService: ModalService, private ComService: CompanyServiceService) { }

  ngOnInit() {
    this.LoadCompanydata();
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  Search() {
    this.LoadCompanydata();
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }

  private LoadCompanydata() {
    var _Fromdate = null;
    var _ToDt = null;
    var _CompanyName = '';

    if (this.startdate == undefined || this.startdate == null)
      _Fromdate = null;
    else
      _Fromdate = this.startdate;

    if (this.todate == undefined || this.todate == null)
      _ToDt = null;
    else
      _ToDt = this.todate;

    if (this.CompName == undefined || this.CompName == null) {
      _CompanyName = null
    }
    else {
      _CompanyName = this.CompName;
    }

    this.ComService.getCompanyList(_CompanyName, _Fromdate, _ToDt).subscribe(
      _CollectList => {
        this.dataSource = new MatTableDataSource(JSON.parse(JSON.stringify(_CollectList)).Payload);
        this.dataSource = this.dataSource;
        this.displayedColumns = ['CompanyName', 'ContactNo', 'EmailId', 'ContactPerson', 'ContactPersonEmail', 'CompanyAddress', 'CreatedOn', 'IsActive', 'Action'];
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => {
        console.log("Failed to load company details", error);
        this.toastr.error("Unable to load company details", "Error");
      }
    );
  }

  editCompanydata(id: string, CompanyName: string, ContactNo: string, EmailId: string, Fax: string, ContactPerson: string, ContactPersonEmail: string, CompanyURL: string, Street: string, City: string, StateId: string, CountryId: string, Zip: string, CompanyId: number) {
    this.modalService.open(id);
    this.CompanyName = CompanyName;
    this.Contact = ContactNo;
    this.EdEmailId = EmailId;
    this.EdFax = Fax;
    this.ContactPName = ContactPerson;
    this.CPEmail = ContactPersonEmail;
    this.EdURL = CompanyURL;
    this.EdStreet = Street;
    this.EdCity = City;
    this.EdZip = Zip;
    this.EdCompanyId = CompanyId;
    this.collection.getCountryList().subscribe((data) => {
      this.countries = JSON.parse(JSON.stringify(data)).Payload;
    });
    this.Edcountry = CountryId;
    this.collection.getStateListByCountryId(Number(this.Edcountry)).subscribe(
      _stateListData => {
        this.states = JSON.parse(JSON.stringify(_stateListData)).Payload;
        this.EDState = StateId;
      },
      error => {
        console.log('Failed while trying to load state list. ' + error);
      });
  }

  onChangeCountry(CountryCode: number) {
    this.collection.getStateListByCountryId(CountryCode).subscribe(
      _stateListData => {
        this.states = JSON.parse(JSON.stringify(_stateListData)).Payload;
      },
      error => {
        console.log('Failed while trying to load state list. ' + error);
      });
  }

  deleteCompanydata(CompanyId: number) {
    if (CompanyId == undefined || CompanyId == null) {
      this.toastr.warning("Data not available or not selected", "Warning");
    }
    else {
      this.ComService.DelateCompanyByCompanyId(CompanyId).subscribe(
        _updateStatus => {
          this.toastr.success("Company details successfully deleted", "Success");
          this.LoadCompanydata();
        }, error => {
          this.toastr.error("Failed to delete the company details", "Error");
          this.LoadCompanydata();
        }
      );
    }
  }

  UpdateStatus(CompanyId: number, event) {
    var Status = event.target.checked;

    if (CompanyId == undefined || CompanyId == null) {
      this.toastr.warning("Data not available or not selected", "Warning");
    }
    else {
      this.ComService.CnahgeCompanyStatus(CompanyId, Status).subscribe(
        _updateStatus => {
          this.toastr.success("Status successfully changed", "Success");
          this.LoadCompanydata();
        }, error => {
          this.toastr.error("Failed to change the company status", "Error");
          this.LoadCompanydata();
        }
      );
    }
  }

  UpdateCompanyDetails() {
    alert(this.EdCompanyId);
    var _CompanyName = '';
    var _ContactNo = '';
    var _Email = ''; var _Fax = '';
    var _CP = '';
    var _CPEmail = '';
    var _Url = '';
    var _street = ''; var _city = ''; var _Zip = '';
    var _Country = ''; var _state = '';

    if (this.CompanyName == undefined || this.CompanyName == null) {
      _CompanyName = null;
    } else {
      _CompanyName = this.CompanyName;
    }
    if (this.Contact == undefined || this.Contact == null) {
      _ContactNo = null;
    } else {
      _ContactNo = this.Contact;
    }

    if (this.EdEmailId == undefined || this.EdEmailId == null) {
      _Email = null;
    } else {
      _Email = this.EdEmailId;
    }
    if (this.EdFax == undefined || this.EdFax == null) {
      _Fax = null;
    } else {
      _Fax = this.EdFax;
    }

    if (this.ContactPName == undefined || this.ContactPName == null) {
      _CP = null;
    }
    else {
      _CP = this.ContactPName;
    }

    if (this.CPEmail == undefined || this.CPEmail == null) {
      _CPEmail = null;
    } else {
      _CPEmail = this.CPEmail;
    }
    if (this.EdURL == undefined || this.EdURL == null) {
      _Url = null;
    } else {
      _Url = this.EdURL;
    }
    if (this.EdStreet == undefined || this.EdStreet == null) {
      _street = null;
    } else {
      _street = this.EdStreet;
    }

    if (this.EdCity == undefined || this.EdCity == null) {
      _city = null;
    }
    else {
      _city = this.EdCity;
    }

    if (this.EdZip == undefined || this.EdZip == null) {
      _Zip = null;
    }
    else {
      _Zip = this.EdZip;
    }

    if (this.Edcountry == undefined || this.Edcountry == null) {
      _Country = null;
    }
    else {
      _Country = this.Edcountry;
    }

    if (this.EDState == undefined || this.EDState == null) {
      _state = null;
    }
    else {
      _state = this.EDState;
    }
    this.ComService.UpdateCompanyDetails(_CompanyName, _ContactNo, _Email, _Fax, _CP, _CPEmail, _Url, _street, _city, _state, _Country, _Zip, this.EdCompanyId).subscribe(
      _UpdateCompDetails => {
        this.toastr.success("Company details successfully updated", "Success");
        this.closeModal('custom-modal-1');
        this.LoadCompanydata();
      },
      error => {
        console.log("Unable to undate company details" + error);
        this.toastr.error("Unable to update company details", "Error");
        this.LoadCompanydata();
      }
    )
  }
}
