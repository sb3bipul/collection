import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import { AssignCustomerService } from '../_services/assign-customer.service';
import { ToastrService } from 'ngx-toastr';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ModalService } from '../_modal';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {
  CPName: string; CustomerCode: string; CustomerName: string;
  dataSource: any;
  displayedColumns: any;
  constructor(private assignCust: AssignCustomerService, private toastr: ToastrService, private modalService: ModalService) { }

  ngOnInit() {
    this.LoadCustCollectorData();
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  SearchInvoice() {
    this.LoadCustCollectorData();
  }

  private LoadCustCollectorData() {
    var _CustCode = '';
    var _CustName = '';
    var _CPName = '';
    if (this.CustomerCode == undefined || this.CustomerCode == null)
      _CustCode = null;
    else
      _CustCode = this.CustomerCode;

    if (this.CustomerName == undefined || this.CustomerName == null)
      _CustName = null;
    else
      _CustName = this.CustomerName;

    if (this.CPName == undefined || this.CPName == null)
      _CPName = null;
    else
      _CPName = this.CPName;

    this.assignCust.getCustCollectPersondata(_CustCode, _CPName, _CustName).subscribe(
      _CollectCustData => {
        console.log('Customer List : ' + JSON.stringify(_CollectCustData.Payload));
        this.dataSource = new MatTableDataSource(JSON.parse(JSON.stringify(_CollectCustData)).Payload);
        this.dataSource = this.dataSource;
        this.displayedColumns = ['CPCODE', 'CPNAME', 'CUSTOMERCODE', 'NAME', 'ContactNo', 'EmailID', 'CUSTADDRESS', 'Action'];
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => {
        console.log("Failed to load customer collect person data", error);
      }
    );
  }

  DeleteCPCustomerByCPCode(_CPCode: string, _custCode: string) {
    let CompanyId = sessionStorage.getItem("CompanyId");
    var CPCode = _CPCode;
    var CustCode = _custCode;

    if (CPCode == undefined || CPCode == null || CPCode == '-') {
      this.toastr.warning("Data not available or not selected", "Warning");
    }
    else {
      this.assignCust.DeleteCPCustomer(CPCode, CustCode, CompanyId).subscribe(
        _DeleteCPCustomer => {
          this.toastr.success("Customer successfully removed", "Success");
          this.LoadCustCollectorData();
        },
        error => {
          console.log("Unable to remove CP customer", error);
          this.toastr.error("Unable to remove customer", "Error");
        }
      );
    }
  }

  OpenEditModel(id: string) {
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }
}
