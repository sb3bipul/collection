import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { ApprovalDataTableDataSource, ApprovalDataTableItem } from './approval-data-table-datasource';
import {SelectionModel} from '@angular/cdk/collections';

export interface ApprovalDataTableItem1 {
  Position: number;
  Amount: string;
  InvoiceId: string;
  Date: string;
  Vendor: string;
  TimeInQueue: string;
  Comments: string;
  isChecked: boolean;
}

// TODO: replace this with real data from your application
const EXAMPLE_DATA: ApprovalDataTableItem1[] = [
  {Position: 1,InvoiceId: 'INV-00956', Amount: '$459.30', Date: '12-08-19', Vendor: 'Triveni IT', TimeInQueue: '10', Comments: 'With Accounts', isChecked: false},
  {Position: 2,InvoiceId: 'INV-00349', Amount: '$125.30', Date: '12-08-19', Vendor: 'Infor', TimeInQueue: '3', Comments: 'Pending Approval', isChecked: false},
  {Position: 3,InvoiceId: 'INV-00853', Amount: '$10.503', Date: '12-08-19', Vendor: 'Tibco', TimeInQueue: '5', Comments: 'With Accounts', isChecked: false},
  {Position: 4,InvoiceId: 'INV-00452', Amount: '$90', Date: '12-08-19', Vendor: 'Alphabet', TimeInQueue: '90', Comments: 'Approved', isChecked: false},
  {Position: 5,InvoiceId: 'INV-00123', Amount: '$15,900', Date: '12-08-19', Vendor: 'National Sales', TimeInQueue: '12', Comments: 'Pending Approval', isChecked: false},
  {Position: 6,InvoiceId: 'INV-00853', Amount: '$115.06', Date: '12-08-19', Vendor: 'AT&T', TimeInQueue: '45', Comments: 'Pending Approval', isChecked: false},
  {Position: 7,InvoiceId: 'INV-00153', Amount: '$1,090', Date: '12-08-19', Vendor: 'Verizon', TimeInQueue: '54', Comments: 'Approved', isChecked: false},
  {Position: 8,InvoiceId: 'INV-00788', Amount: '$555.50', Date: '12-08-19', Vendor: 'Source Consulting', TimeInQueue: '5', Comments: 'Payment Initiated', isChecked: false},
  {Position: 9,InvoiceId: 'INV-00326', Amount: '$8,563', Date: '12-08-19', Vendor: 'United', TimeInQueue: '7', Comments: 'Pending Approval', isChecked: false},
  {Position: 10,InvoiceId: 'INV-00759', Amount: '$10,960.20', Date: '12-08-19', Vendor: 'IBM', TimeInQueue: '10', Comments: 'With Accounts', isChecked: false},
  {Position: 11,InvoiceId: 'INV-00999', Amount: '$886.90', Date: '12-08-19', Vendor: 'Apple', TimeInQueue: '13', Comments: 'Payment Initiated', isChecked: false},
  {Position: 12,InvoiceId: 'INV-00223', Amount: '$459.30', Date: '12-08-19', Vendor: 'Microsoft', TimeInQueue: '14', Comments: 'With Admin', isChecked: false}
];

@Component({
  selector: 'app-approval-data-table',
  templateUrl: './approval-data-table.component.html',
  styleUrls: ['./approval-data-table.component.css']
})
export class ApprovalDataTableComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<ApprovalDataTableItem>;
  dataSource: ApprovalDataTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['select', 'InvoiceId', 'Amount', 'Vendor', 'Date', 'TimeInQueue', 'Comments', 'Action'];

  ngOnInit() {
    this.dataSource = new ApprovalDataTableDataSource();
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }

  selection = new SelectionModel<ApprovalDataTableItem1>(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    // this.isAllSelected() ?
    //     this.selection.clear() :
    //     this.dataSource.data.forEach(row => this.selection.select(row));
    this.isAllSelected() ?
        (
            this.selection.selected.length = 0,
            this.dataSource.data.forEach(row => this.selection.deselect(row))
        ) :
        this.dataSource.data.forEach(
            row => {
                this.selection.select(row);
                this.selection.selected.push(row);
            }
    );
  }
}
