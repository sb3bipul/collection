import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';

// TODO: Replace this with your own data model type
export interface ApprovalDataTableItem {
  Position: number;
  Amount: string;
  InvoiceId: string;
  Date: string;
  Vendor: string;
  TimeInQueue: string;
  Comments: string;
  isChecked: boolean;
}

// TODO: replace this with real data from your application
const EXAMPLE_DATA: ApprovalDataTableItem[] = [
  {Position: 1,InvoiceId: 'INV-00956', Amount: '$459.30', Date: '12-08-19', Vendor: 'Triveni IT', TimeInQueue: '10', Comments: 'With Accounts', isChecked: false},
  {Position: 2,InvoiceId: 'INV-00349', Amount: '$125.30', Date: '12-08-19', Vendor: 'Infor', TimeInQueue: '3', Comments: 'Pending Approval', isChecked: false},
  {Position: 3,InvoiceId: 'INV-00853', Amount: '$10.503', Date: '12-08-19', Vendor: 'Tibco', TimeInQueue: '5', Comments: 'With Accounts', isChecked: false},
  {Position: 4,InvoiceId: 'INV-00452', Amount: '$90', Date: '12-08-19', Vendor: 'Alphabet', TimeInQueue: '90', Comments: 'Approved', isChecked: false},
  {Position: 5,InvoiceId: 'INV-00123', Amount: '$15,900', Date: '12-08-19', Vendor: 'National Sales', TimeInQueue: '12', Comments: 'Pending Approval', isChecked: false},
  {Position: 6,InvoiceId: 'INV-00853', Amount: '$115.06', Date: '12-08-19', Vendor: 'AT&T', TimeInQueue: '45', Comments: 'Pending Approval', isChecked: false},
  {Position: 7,InvoiceId: 'INV-00153', Amount: '$1,090', Date: '12-08-19', Vendor: 'Verizon', TimeInQueue: '54', Comments: 'Approved', isChecked: false},
  {Position: 8,InvoiceId: 'INV-00788', Amount: '$555.50', Date: '12-08-19', Vendor: 'Source Consulting', TimeInQueue: '5', Comments: 'Payment Initiated', isChecked: false},
  {Position: 9,InvoiceId: 'INV-00326', Amount: '$8,563', Date: '12-08-19', Vendor: 'United', TimeInQueue: '7', Comments: 'Pending Approval', isChecked: false},
  {Position: 10,InvoiceId: 'INV-00759', Amount: '$10,960.20', Date: '12-08-19', Vendor: 'IBM', TimeInQueue: '10', Comments: 'With Accounts', isChecked: false},
  {Position: 11,InvoiceId: 'INV-00999', Amount: '$886.90', Date: '12-08-19', Vendor: 'Apple', TimeInQueue: '13', Comments: 'Payment Initiated', isChecked: false},
  {Position: 12,InvoiceId: 'INV-00223', Amount: '$459.30', Date: '12-08-19', Vendor: 'Microsoft', TimeInQueue: '14', Comments: 'With Admin', isChecked: false}
];

/**
 * Data source for the ApprovalDataTable view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class ApprovalDataTableDataSource extends DataSource<ApprovalDataTableItem> {
  data: ApprovalDataTableItem[] = EXAMPLE_DATA;
  paginator: MatPaginator;
  sort: MatSort;

  constructor() {
    super();
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<ApprovalDataTableItem[]> {
    // Combine everything that affects the rendered data into one update
    // stream for the data-table to consume.
    const dataMutations = [
      observableOf(this.data),
      this.paginator.page,
      this.sort.sortChange
    ];

    return merge(...dataMutations).pipe(map(() => {
      return this.getPagedData(this.getSortedData([...this.data]));
    }));
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {}

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: ApprovalDataTableItem[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: ApprovalDataTableItem[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'Vendor': return compare(a.Vendor, b.Vendor, isAsc);
        case 'InvoiceId': return compare(+a.InvoiceId, +b.InvoiceId, isAsc);
        case 'Date': return compare(+a.Date, +b.Date, isAsc);
        case 'Amount': return compare(+a.Amount, +b.Amount, isAsc);
        case 'Comments': return compare(+a.Comments, +b.Comments, isAsc);
        default: return 0;
      }
    });
  }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a: string | number, b: string | number, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
