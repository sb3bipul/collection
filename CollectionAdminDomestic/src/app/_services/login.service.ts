import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import { throwError } from 'rxjs';
import { HttpClientModule } from '@angular/common/http';
import { appConfig } from '../app.config';
import { ILogin, UserLogin } from '../_models/login';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private baseurl: string = appConfig.apiUrl;

  constructor(private http: Http) { }

  private handleError(error: any) {
    var applicationError = error.headers.get('Application-Error');
    var serverError = error.json();
    var modelStateErrors: string = '';
    if (!serverError.type) {
      console.log(serverError);
      for (var key in serverError) {
        if (serverError[key])
          modelStateErrors += serverError[key] + '\n';
      }
    }
    modelStateErrors = modelStateErrors = '' ? null : modelStateErrors;
    return Observable.throw(applicationError || modelStateErrors || 'Server error');
  }

  getUserToken(_UserId: string, _Password: string, _CompanyID: string): Observable<ILogin> {
    
    var obj = { UserId: _UserId, Password: _Password, CompanyID: _CompanyID }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);
    return this.http.post(this.baseurl + 'api/User', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }
}
