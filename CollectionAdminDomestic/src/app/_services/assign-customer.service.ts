import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response, RequestMethod } from '@angular/http';
import {CPCustRelation, AssignCustomer, CustomerList, CollectPersonList, ZoneList, CustCollectPerson, UpdateCPCust, BrokerList, DeleteCP, CustByBroker, CustomerListCP, CPCustomerListByCP } from '../_models/assign-customer';
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import { throwError } from 'rxjs';
import { HttpClientModule } from '@angular/common/http';
import { appConfig } from '../app.config';


@Injectable({
  providedIn: 'root'
})
export class AssignCustomerService {
  private baseurl: string = appConfig.apiUrl;

  constructor(private http: Http) { }

  private handleError(error: any) {
    var applicationError = error.headers.get('Application-Error');
    var serverError = error.json();
    var modelStateErrors: string = '';
    if (!serverError.type) {
      console.log(serverError);
      for (var key in serverError) {
        if (serverError[key])
          modelStateErrors += serverError[key] + '\n';
      }
    }
    modelStateErrors = modelStateErrors = '' ? null : modelStateErrors;
    return Observable.throw(applicationError || modelStateErrors || 'Server error');
  }

  getCustomerList(_CompanyId: number, _CustomerCode: string, _CustomerName: string, _Zone: number, _BrokerId: string): Observable<CustomerList> {
    var obj = { CompanyId: _CompanyId, CustCode: _CustomerCode, CustName: _CustomerName, Zone: _Zone, BrokerId: _BrokerId }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);//this.serializeObj(obj);
    console.log('Customer List Log :api/CollectPersonal/GetCustomerlist:' + JSON.stringify(obj));
    return this.http.post(this.baseurl + 'api/CollectPersonal/GetCustomerlist', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }


  getCollectPersonList(_CompanyId: number): Observable<CollectPersonList> {
    var obj = { CompanyId: _CompanyId }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);//this.serializeObj(obj);
    console.log('Collect Person List Log :api/CollectPersonal/GetAllCollectPersonList:' + JSON.stringify(obj));
    return this.http.post(this.baseurl + 'api/CollectPersonal/GetAllCollectPersonList', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }


  getAllZone(_CompanyId: number): Observable<ZoneList> {
    var obj = { CompanyId: _CompanyId }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);//this.serializeObj(obj);
    console.log('Zone List Log :api/CollectPersonal/getAllZone:' + JSON.stringify(obj));
    return this.http.post(this.baseurl + 'api/CollectPersonal/getAllZone', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }

  insertCollectPersonCustomerRelation(_CPCode: string, _CustomerCode: String, _CompanyId: number): Observable<AssignCustomer> {
    var obj = { CPCode: _CPCode, CustCode: _CustomerCode, CompanyId: _CompanyId }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);//this.serializeObj(obj);
    console.log('Insert collect personal and customer relation Data : api/CollectPersonal/SaveAssignCustomerdata :' + JSON.stringify(obj));
    return this.http.post(this.baseurl + 'api/CollectPersonal/SaveAssignCustomerdata', body, options)
      .map((res: Response) => {
        console.log('Insert collect personal and customer relation return data: ' + JSON.stringify(res));
        return res.json();
      })
      .catch(this.handleError);
  }


  private serializeObj(obj: any) {
    var result = [];
    for (var property in obj)
      result.push(encodeURIComponent(property) + "=" + encodeURIComponent(obj[property]));
    return result.join("&");
  }

  getCustCollectPersondata(_CustCode: string, _CPName: string, _CustName: string): Observable<CustCollectPerson> {
    var obj = { CustCode: _CustCode, CPName: _CPName, CustName: _CustName }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);//this.serializeObj(obj);
    console.log('Customer collect person list Log :api/CollectPersonal/getCustCollectPersonRelation:' + JSON.stringify(obj));
    return this.http.post(this.baseurl + 'api/CollectPersonal/getCustCollectPersonRelation', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }

  GetBrokerList(_CompanyId: string): Observable<BrokerList> {
    var obj = { CompanyId: _CompanyId }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);
    console.log('Broker List api/CollectPersonal/GetBrokerListByCompanyId:' + JSON.stringify(obj));
    return this.http.post(this.baseurl + 'api/CollectPersonal/GetBrokerListByCompanyId', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }

  DeleteCPCustomer(_CPCode: string, _CustCode: string, _CompanyId): Observable<DeleteCP> {
    var obj = { CPCode: _CPCode, CustCode: _CustCode, CompanyId: _CompanyId }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);
    console.log('Delete CP Custpomer by CPCode and Customer code : api/CollectPersonal/DeleteCPCustomer:' + JSON.stringify(obj));
    return this.http.post(this.baseurl + 'api/CollectPersonal/DeleteCPCustomer', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getCustomerListForCPUpdate(_CompanyId: number, _CustomerCode: string, _CustomerName: string, _Zone: number): Observable<CustomerListCP> {
    var obj = { CompanyId: _CompanyId, CustCode: _CustomerCode, CustName: _CustomerName, Zone: _Zone }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);//this.serializeObj(obj);
    console.log('Customer List Log :api/CollectPersonal/GetCustomerlistForCPUpdate:' + JSON.stringify(obj));
    return this.http.post(this.baseurl + 'api/CollectPersonal/GetCustomerlistForCPUpdate', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getCPCustomerListByCPCode(_CPCode: string, _CompanyId: String): Observable<CPCustomerListByCP> {
    var obj = { CPCode: _CPCode, CompantId: _CompanyId }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);//this.serializeObj(obj);
    console.log('Assigned Customer List Log :api/CollectPersonal/getCPCustomerListByCPCode:' + JSON.stringify(obj));
    return this.http.post(this.baseurl + 'api/CollectPersonal/getCPCustomerListByCPCode', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getCustByBrokerId(_companyId: number, _CustCode: string, _CustName: string, _BrokerId: string): Observable<CustByBroker> {
    var obj = { CompanyId: _companyId, CustCode: _CustCode, CustName: _CustName, BrokerId: _BrokerId }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);//this.serializeObj(obj);
    console.log('Customer List Log :api/CollectPersonal/GetCustomerlistByBrokerId:' + JSON.stringify(obj));
    return this.http.post(this.baseurl + 'api/CollectPersonal/GetCustomerlistByBrokerId', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }

  UpdateCPCustomer(_CPCode: string, _customercode: string, _CompanyId: number): Observable<UpdateCPCust> {
    debugger;
    var obj = { CPCode: _CPCode, CustomerCode: _customercode, CompanyId: _CompanyId }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);//this.serializeObj(obj);
    return this.http.post(this.baseurl + 'api/CollectPersonal/UpdateCPCutomer', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }

  DeleteCPCustRelation(_CPCode: string, _CompanyId: string): Observable<CPCustRelation> {
    var obj = { CPCode: _CPCode, CompanyId: _CompanyId }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);//this.serializeObj(obj);
    return this.http.post(this.baseurl + 'api/CollectPersonal/DeleteCPRelation', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }
}
