import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { appConfig } from '../app.config';
import { CollectPersonal } from '../_models/collect-personal';
import { Http } from '@angular/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CollectPersonService } from '../_services/collect-person.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-collect-person',
  templateUrl: './add-collect-person.component.html',
  styleUrls: ['./add-collect-person.component.css']
})
export class AddCollectPersonComponent implements OnInit {
  public createCollectionPerson: FormGroup;
  countries: {};
  states: {};
  companyList: {};
  infoEd: string; errorPass: string; errorCPass: string; NotMatch: string;
  PersonName: string; Zip: string; City: string; Address: string; Zone: string; CPassword: string; Password: string;
  Mobile: string; EmailId: string; CompanyId: number; StateId: string; CountryId: number;
  collectForm: any;
  imageSrc;
  CPImage: string;
  BaseImageString: any;
  //base64s
  Base64ImageString: string;

  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  constructor(private formbulider: FormBuilder, private http: HttpClient, private router: Router, private collect: CollectPersonService, private toastr: ToastrService) {
    this.createCollectionPerson = new FormGroup(
      {
        EmailId: new FormControl('', [Validators.required, Validators.pattern(this.emailPattern)]),
        Mobile: new FormControl('', [Validators.required, Validators.pattern("^\d{3}-\d{3}-\d{4}$")]),
        Zip: new FormControl('', [Validators.required, Validators.pattern("^[0-9]{5}(?:-[0-9]{4})?$")]),
        Password: new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(12)]),
        CPassword: new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(12)])
      }
    );
  }
  ngOnInit() {
    this.LoadCompanyList();

    this.collect.getCountryList().subscribe((data) => {
      console.log(data);
      this.countries = JSON.parse(JSON.stringify(data)).Payload;
    });
  }
  onChangeCountry(CountryCode: number) {
    //let CountryCode = CountryCode;
    this.collect.getStateListByCountryId(CountryCode).subscribe(
      _stateListData => {
        console.log('Load state list : ' + JSON.stringify(_stateListData.Payload));
        this.states = JSON.parse(JSON.stringify(_stateListData)).Payload;
      },
      error => {
        console.log('Failed while trying to load state list. ' + error);
      });
  }

  private LoadCompanyList() {
    let CompanyId = Number(sessionStorage.getItem("CompanyId"));    
    this.collect.getCompanyList(CompanyId).subscribe(
      _CompanyList => {
        console.log('Load Company list : ' + JSON.stringify(_CompanyList.Payload));
        this.companyList = JSON.parse(JSON.stringify(_CompanyList)).Payload;
      },
      error => {
        console.log('Failed while trying to load company list. ' + error);
      });
  }

  SaveCollectPersonalData() {
    var _CPImage = '';
    var _BaseImageString = '';
    if (this.CPImage == undefined || this.CPImage == null) {
      _CPImage = null;
    }
    else {
      _CPImage = this.CPImage;
    }
    if (this.Base64ImageString == undefined || this.Base64ImageString == null) {
      _BaseImageString = null;
    }
    else {
      _BaseImageString = this.Base64ImageString;
    }
    if ((this.PersonName == undefined || this.PersonName == '') || (this.EmailId == undefined || this.EmailId == '') || (this.Mobile == undefined || this.Mobile == '') || (this.Zip == undefined || this.Zip == '') || (this.Password == undefined || this.Password == '') || (this.CPassword == undefined || this.CPassword == '')) {
      this.toastr.warning("All (*) markes are mandatory fields", "Warning");
    }
    else {
      if (this.Password != this.CPassword) {
        this.toastr.error("Password and Confirm Password not match.", "Error");
      }
      else {
        this.collect.insertCollectPersondata(this.PersonName, this.EmailId, this.Mobile, this.CompanyId, this.Address, this.City, this.Zone, this.StateId, this.CountryId, this.Zip, this.Password, this.CPassword, _CPImage, _BaseImageString).subscribe(
          _saveCollectPersonal => {
            console.log('Returned data after inserted : ' + JSON.stringify(_saveCollectPersonal));
            // this.LoadSearchInvoice();
            // this.successMsgEd = 'Extra Invoice was edited successfully. ';
            // this.closeModal('custom-modal-3');
            this.toastr.success("Data inserted successfully.", "Success");
            this.reset();
          },
          error => {
            console.log('Failed while trying to save collect personal data. ' + error);
            this.toastr.error("Failed while trying to save collect personal data.", "Erroe");
          });
      }
    }
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.createCollectionPerson.controls[controlName].hasError(errorName);
  }


  reset() {
    this.PersonName = null;
    this.EmailId = null;
    this.Mobile = null;
    this.CompanyId = null;
    this.Password = null; this.CPassword = null;
    this.Address = null;
    this.City = null;
    this.StateId = null; this.CountryId = null; this.Zip = null;
    this.Zone = null;
    this.CPImage = null;
    this.Base64ImageString = null;
  }

  cancel() {
    this.router.navigate(['./myQueue']);
  }

  ValidateEmail(EmailId: string) {

  }

  handleInputChange(files) {
    var file = files;
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      this.toastr.error("Invalid image format", "Error");
      this.CPImage = null;
      this.Base64ImageString = null;
      return;
    }
    reader.onloadend = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }
  _handleReaderLoaded(e) {
    let reader = e.target;
    var base64result = reader.result.substr(reader.result.indexOf(',') + 1);
    this.Base64ImageString = base64result;
  }
  public picked(event) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file: File = fileList[0];
      this.BaseImageString = file;
      this.handleInputChange(file); //turn into base64
    }
    else {
      this.toastr.warning("No file selected", "Warning");
    }
  }

}
