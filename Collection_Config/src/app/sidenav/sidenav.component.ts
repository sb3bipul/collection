import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {
  version: string;
  userRole: string;
  constructor() { }

  ngOnInit() {
    this.version = "V2020.09.25.1.1";
    this.userRole = sessionStorage.getItem("UserRole");
  }

}
