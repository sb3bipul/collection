import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomUXComponent } from './custom-ux.component';

describe('CustomUXComponent', () => {
  let component: CustomUXComponent;
  let fixture: ComponentFixture<CustomUXComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomUXComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomUXComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
