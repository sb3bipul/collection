import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ModalService } from '../_modal';
import { UiConfigService } from '../_services/ui-config.service';

@Component({
  selector: 'app-custom-ux',
  templateUrl: './custom-ux.component.html',
  styleUrls: ['./custom-ux.component.css']
})
export class CustomUXComponent implements OnInit {
  Company: {};
  client: string;
  themecolor: string;
  Currency: string;
  IsDisplayInvoice: boolean;
  dataSource: any;
  displayedColumns: any;
  AppUiConfigDt: any;
  constructor(private configservice: UiConfigService, private toastr: ToastrService, private modalService: ModalService, private router: Router) { }

  ngOnInit() {
    this.themecolor = "#23347e";
    this.Currency = "USD";

    this.configservice.getCompanyList().subscribe((data) => {
      console.log(data);
      this.Company = JSON.parse(JSON.stringify(data)).Payload;
    },
      error => {
        this.toastr.error("Unable to load company data", "Error");
      }
    );

    this.LoadAppUIConfigData();
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  cancel() {
    this.router.navigate(['./myQueue']);
  }

  Save() {
    var _colorCode = '';
    var _currency = '';
    if (this.client == undefined || this.client == null) {
      this.toastr.warning("Please select a client", "Warning");
    }
    else {
      if (this.themecolor == undefined || this.themecolor == null) {
        _colorCode = "#23347e";
      }
      else {
        _colorCode = this.themecolor;
      }
      if (this.Currency == undefined || this.Currency == null) {
        _currency = "USD";
      }
      else {
        _currency = this.Currency;
      }

      this.configservice.insertAppUIConfigData(this.client, _colorCode, _currency, this.IsDisplayInvoice).subscribe(
        _AppUIConfig => {
          this.toastr.success("App UI config data saved sucessfully", "Success");
          this.LoadAppUIConfigData();
        },
        _error => {
          this.toastr.error("Unable to save App UI Config data", "Error");
          console.log("Unable to save App UI config data" + _error);
        }
      )
    }
  }

  changeSuit() {
    this.configservice.GetAppUIConfigData(this.client).subscribe(
      _DtCobfig => {
        this.AppUiConfigDt = JSON.parse(JSON.stringify(_DtCobfig)).Payload;
        if (this.AppUiConfigDt[0]["ID"] != '-') {
          this.Currency = this.AppUiConfigDt[0]["Currency"];
          if (this.AppUiConfigDt[0]["IsDisplayInvoice"] == "True" || this.AppUiConfigDt[0]["IsDisplayInvoice"] == "true") {
            this.IsDisplayInvoice = true;
          }
          else {
            this.IsDisplayInvoice = false;
          }
          this.themecolor = this.AppUiConfigDt[0]["ThemeColor"];
        }
        else {
          this.Currency = "";
          this.themecolor = "#23347e";
          this.IsDisplayInvoice = null;
        }
      }
    )
  }

  private LoadAppUIConfigData() {
    var _clientname = null;
    this.configservice.GetAppUIConfigData(_clientname).subscribe(
      _UiConfig => {
        this.dataSource = new MatTableDataSource(JSON.parse(JSON.stringify(_UiConfig)).Payload);
        this.dataSource = this.dataSource;
        this.displayedColumns = ['ClientName', 'ThemeColor', 'Currency', 'CreatedDate', 'IsDisplayInvoice', 'IsActive'];
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => {
        console.log("Failed to load app UI config data", error);
      }
    );
  }
}
