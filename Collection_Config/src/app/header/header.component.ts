import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  private loggedIn = new BehaviorSubject<boolean>(false);
  constructor(private router: Router) { }

  ngOnInit(): void {
  }


  logout(){
    this.loggedIn.next(false);
    this.router.navigate(['/login']);
    sessionStorage.clear();
  }
}
