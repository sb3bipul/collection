import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response, RequestMethod } from '@angular/http';
import { CompanyModel, CompanyDetailsModel, UpdateCompany, DelateCompany, ChangeCompStatus } from '../_models/company-model';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import { appConfig } from '../app.config';

@Injectable({
  providedIn: 'root'
})
export class CompanyServiceService {
  private baseurl: string = appConfig.apiUrl;

  constructor(private http: Http) { }

  private handleError(error: any) {
    var applicationError = error.headers.get('Application-Error');
    var serverError = error.json();
    var modelStateErrors: string = '';
    if (!serverError.type) {
      console.log(serverError);
      for (var key in serverError) {
        if (serverError[key])
          modelStateErrors += serverError[key] + '\n';
      }
    }
    modelStateErrors = modelStateErrors = '' ? null : modelStateErrors;
    return Observable.throw(applicationError || modelStateErrors || 'Server error');
  }

  private serializeObj(obj: any) {
    var result = [];
    for (var property in obj)
      result.push(encodeURIComponent(property) + "=" + encodeURIComponent(obj[property]));
    return result.join("&");
  }

  InsertCompanyMasterData(_CompanyName: string, _ContactNo: string, _EmailId: string, _Fax: string, _ContactPerson: string, _ContactPersonEmail: string, _Street: string, _City: string, _StateId: string, _CountryId: string, _Zip: string, _CompanyURL: string, _CompanyLogo: string, _CompanyBanner: string, _Base64logo: string, _Base64Banner: string): Observable<CompanyModel> {
    var obj = { CompanyName: _CompanyName, ContactNo: _ContactNo, EmailId: _EmailId, Fax: _Fax, ContactPerson: _ContactPerson, ContactPersonEmail: _ContactPersonEmail, Street: _Street, City: _City, StateId: _StateId, CountryId: _CountryId, Zip: _Zip, CompanyURL: _CompanyURL, CompanyLogo: _CompanyLogo, CompanyBanner: _CompanyBanner, Base64logo: _Base64logo, Base64Banner: _Base64Banner }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);
    return this.http.post(this.baseurl + 'api/UIConfig/InsertCompanyData', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getCompanyList(_CompanyName: string, _fromdate: Date, _ToDate: Date): Observable<CompanyDetailsModel> {
    var obj = { CompanyName: _CompanyName, FromDate: _fromdate, ToDate: _ToDate }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);
    return this.http.post(this.baseurl + 'api/UIConfig/GetCompanyDetails', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }

  UpdateCompanyDetails(_CompanyName: string, _ContactNo: string, _Emailid: string, _Fax: string, _ContactPerson: string, _ContactPersonEmail: string, _CompanyURL: string, _Street: string, _City: string, _StateId: string, _CountryId: string, _Zip: string, _CompanyId: number): Observable<UpdateCompany> {
    var obj = { CompanyName: _CompanyName, ContactNo: _ContactNo, Emailid: _Emailid, Fax: _Fax, ContactPerson: _ContactPerson, ContactPersonEmail: _ContactPersonEmail, CompanyURL: _CompanyURL, Street: _Street, City: _City, StateId: _StateId, CountryId: _CountryId, Zip: _Zip, CompanyId: _CompanyId }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);
    return this.http.post(this.baseurl + 'api/UIConfig/UpdateCompanyDetails', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }

  DelateCompanyByCompanyId(_CompanyId: number): Observable<DelateCompany> {
    var obj = { CompanyId: _CompanyId }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);
    return this.http.post(this.baseurl + 'api/UIConfig/DeleteCompanyDetails', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }

  CnahgeCompanyStatus(_CompanyId: number, _Status: boolean): Observable<ChangeCompStatus> {
    var obj = { CompanyId: _CompanyId, Status: _Status }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);
    return this.http.post(this.baseurl + 'api/UIConfig/ChangeCompanyStatus', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }

}
