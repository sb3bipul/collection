﻿import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response, RequestMethod } from '@angular/http';
import { IInvoice, IInvoiceApproval, IInvoiceAddExtra, PImage, PDashbordData,RmvPayment } from '../_models/index';
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import { appConfig } from '../app.config';

@Injectable({
    providedIn: 'root'
})

//@Injectable()
export class UserService {
    private baseurl: string = appConfig.apiUrl;
    private pdfUrl: string = appConfig.pdfUrl;
    constructor(private http: Http) { }

    private handleError(error: any) {
        var applicationError = error.headers.get('Application-Error');
        var serverError = error.json();
        var modelStateErrors: string = '';
        if (!serverError.type) {
            console.log(serverError);
            for (var key in serverError) {
                if (serverError[key])
                    modelStateErrors += serverError[key] + '\n';
            }
        }
        modelStateErrors = modelStateErrors = '' ? null : modelStateErrors;
        return Observable.throw(applicationError || modelStateErrors || 'Server error');
    }

    getUser() {
        return this.http.get('http://107.21.119.157/OMSapi/api/checkUser/015937/sugar1').map((response: Response) => response.json());
    }

    searchPaymentData(frDt: string, toDt: string, _receiptNo: string, _CustomerId: string): Observable<IInvoiceApproval> {
        var obj = { FromDate: frDt, ToDate: toDt, ReceiptNo: _receiptNo, Customerid: _CustomerId }
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
        let body = JSON.stringify(obj);//this.serializeObj(obj);
        console.log('Collection Data Fatch :  api/Payment/GetDataforCollectionAdmin : ' + JSON.stringify(obj));
        return this.http.post(this.baseurl + 'api/Payment/GetDataforCollectionAdmin', body, options)
            .map((res: Response) => {
                return res.json();
            })
            .catch(this.handleError);
    }

    getPaymentReceiptImage(ReceiptNo: string): Observable<PImage> {
        var obj = { ReceiptNo: ReceiptNo }
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
        let body = JSON.stringify(obj);//this.serializeObj(obj);
        console.log('Invoice status log :api/Payment/getPaymentreceiptImage:' + JSON.stringify(obj));
        return this.http.post(this.baseurl + 'api/Payment/getPaymentreceiptImage', body, options)
            .map((res: Response) => {
                return res.json();
            })
            .catch(this.handleError);
    }

    getDashbordData(CompanyId: number): Observable<PDashbordData> {
        var obj = { CompanyId: CompanyId }
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
        let body = JSON.stringify(obj);//this.serializeObj(obj);
        console.log('Invoice status log :api/Payment/getCollectionDashbordData:' + JSON.stringify(obj));
        return this.http.post(this.baseurl + 'api/Payment/getCollectionDashbordData', body, options)
            .map((res: Response) => {
                return res.json();
            })
            .catch(this.handleError);
    }

    DeletePendingPayment(_ReceiptNo: string, _companyId: string): Observable<RmvPayment> {
        var obj = { ReceiptNo: _ReceiptNo, CompanyId: _companyId }
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
        let body = JSON.stringify(obj);
        console.log('Invoice status log :api/Payment/DeletePendingPayment:' + JSON.stringify(obj));
        return this.http.post(this.baseurl + 'api/Payment/DeletePendingPayment', body, options)
            .map((res: Response) => {
                return res.json();
            })
            .catch(this.handleError);
    }

    private serializeObj(obj: any) {
        var result = [];
        for (var property in obj)
            result.push(encodeURIComponent(property) + "=" + encodeURIComponent(obj[property]));
        return result.join("&");
    }
}