import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response, RequestMethod } from '@angular/http';
import { Config, ConfigDt, CustConfigDt, CustConfigdata, AppUIConfigdata, UIConfigdata, PMConfigdata, PayConfigdata,DelConfigdata } from '../_models/config';
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import { throwError } from 'rxjs';
import { HttpClientModule } from '@angular/common/http';
import { appConfig } from '../app.config';

@Injectable({
  providedIn: 'root'
})
export class UiConfigService {
  private baseurl: string = appConfig.apiUrl;
  constructor(private http: Http) { }

  private handleError(error: any) {
    var applicationError = error.headers.get('Application-Error');
    var serverError = error.json();
    var modelStateErrors: string = '';
    if (!serverError.type) {
      console.log(serverError);
      for (var key in serverError) {
        if (serverError[key])
          modelStateErrors += serverError[key] + '\n';
      }
    }
    modelStateErrors = modelStateErrors = '' ? null : modelStateErrors;
    return Observable.throw(applicationError || modelStateErrors || 'Server error');
  }

  private serializeObj(obj: any) {
    var result = [];
    for (var property in obj)
      result.push(encodeURIComponent(property) + "=" + encodeURIComponent(obj[property]));
    return result.join("&");
  }

  InsertEmailConfigdata(_IsEmailToCompany: boolean, _IsEmailToBroker: boolean, _IsEmailToCP: boolean, _IsEmailToCustomer: boolean, _CreatedBy: string): Observable<Config> {
    var obj = { IsEmailCompany: _IsEmailToCompany, IsEmailBroker: _IsEmailToBroker, IsEmailCP: _IsEmailToCP, IsEmailCustomer: _IsEmailToCustomer, CreatedBy: _CreatedBy }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);
    return this.http.post(this.baseurl + 'api/UIConfig/InsertEmailConfigData', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }


  getConfigdata(_CompanyId: number): Observable<ConfigDt> {
    var obj = { CompanyId: _CompanyId }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);
    return this.http.post(this.baseurl + 'api/UIConfig/getEmailConfigDatabyCompanyId', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }

  InsertCustomerDataConfig(_ClientName: string, _ClientID: string, _IsERP: boolean, _IsOthers: boolean, _CreatedBy: string): Observable<CustConfigDt> {
    var obj = { ClientName: _ClientName, ClientID: _ClientID, IsERP: _IsERP, IsOthers: _IsOthers, CreatedBy: _CreatedBy }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);
    return this.http.post(this.baseurl + 'api/UIConfig/InsertUiConfigData', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getCustConfigdataByClientName(_ClientName: string): Observable<CustConfigdata> {
    debugger;
    var obj = { ClientName: _ClientName }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);
    return this.http.post(this.baseurl + 'api/UIConfig/GetCustConfigDataByClientName', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getCompanyList() {
    return this.http.get(this.baseurl + 'api/UIConfig/GetCompanyListForUIConfig').map((response: Response) => response.json());
  }

  insertAppUIConfigData(_ClientName: string, _ThemeColor: string, _Currency: string, _IsDisplayInvoice: boolean): Observable<AppUIConfigdata> {
    var obj = { ClientName: _ClientName, Themecolor: _ThemeColor, Currency: _Currency, IsDisplayInvoice: _IsDisplayInvoice }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);
    return this.http.post(this.baseurl + 'api/UIConfig/InsertAppUIConfigData', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }

  GetAppUIConfigData(_ClientName: string): Observable<UIConfigdata> {
    var obj = { ClientName: _ClientName }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);
    return this.http.post(this.baseurl + 'api/UIConfig/GetAppUIConfigDataByClientName', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }

  insertPaymentMethodConfigData(_ClientName: string, _PayMethod: string, _Name: string): Observable<PMConfigdata> {
    var obj = { ClientName: _ClientName, PaymentMethod: _PayMethod, Name: _Name }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);
    return this.http.post(this.baseurl + 'api/UIConfig/InsertPaymentmethodconfig', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getPaymentConfigData(_ClientName: string): Observable<PayConfigdata> {
    var obj = { ClientName: _ClientName }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);
    return this.http.post(this.baseurl + 'api/UIConfig/GetPaymentMethodByClientName', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }

  DeletePaymentConfigData(_ClientName: string): Observable<DelConfigdata> {
    var obj = { ClientName: _ClientName }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);
    return this.http.post(this.baseurl + 'api/UIConfig/deletepaymentmethodbyclientname', body, options)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }
}
