export class Config {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}
export class ConfigDt {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}

export class CustConfigDt {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}

export class CustConfigdata {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    CustConfig: string;
    PaymentMethodConfig:string;
}

export class AppUIConfigdata {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}

export class UIConfigdata {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}

export class PMConfigdata {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}

export class PayConfigdata {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}

export class DelConfigdata {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}