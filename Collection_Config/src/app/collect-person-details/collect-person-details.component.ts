import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import 'rxjs/add/operator/map';
import { CollectPersonService } from '../_services/collect-person.service';
import { ToastrService } from 'ngx-toastr';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ModalService } from '../_modal';


@Component({
  selector: 'app-collect-person-details',
  templateUrl: './collect-person-details.component.html',
  styleUrls: ['./collect-person-details.component.css']
})
export class CollectPersonDetailsComponent implements OnInit {
  startdate: Date;
  todate: Date;
  CPName: string;
  EmailId: string;
  dataSource: any;
  displayedColumns: any;
  date: string; CPStatus: boolean;
  PersonName: string; Mobile: string; Address: string; City: string; Zone: string; CPCode: string;
  countries: {}; states: {}; edEmailId: string;
  StateId: string; CountryId: number; Zip: string;
  constructor(private collection: CollectPersonService, private toastr: ToastrService, private modalService: ModalService) { }

  ngOnInit() {
    //this.startdate=new Date();
    this.LoadCollectPersonList();
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  SearchInvoice() {
    this.LoadCollectPersonList();
  }

  private LoadCollectPersonList() {
    var _Fromdate = null;
    var _ToDt = null;
    var _CPName = '';
    var _EmailId = '';

    if (this.startdate == undefined || this.startdate == null)
      _Fromdate = null;
    else
      _Fromdate = this.startdate;

    if (this.todate == undefined || this.todate == null)
      _ToDt = null;
    else
      _ToDt = this.todate;

    if (this.CPName == undefined || this.CPName == null)
      _CPName = null;
    else
      _CPName = this.CPName;

    if (this.EmailId == undefined || this.EmailId == null)
      _EmailId = null;
    else
      _EmailId = this.EmailId;

    this.collection.getCollectPersonList(_Fromdate, _ToDt, _CPName, _EmailId).subscribe(
      _CollectList => {
        console.log('Collect Person List : ' + JSON.stringify(_CollectList.Payload));
        this.dataSource = new MatTableDataSource(JSON.parse(JSON.stringify(_CollectList)).Payload);
        this.dataSource = this.dataSource;
        this.displayedColumns = ['CPCODE', 'CPNAME', 'EmailId', 'ContactNo', 'CPAddress', 'CreatedDate', 'IsActive', 'action'];
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => {
        console.log("Failed to load collect person data", error);
      }
    );
  }

  editCollectPersondata(id: string, CPName: string, address: string, ContactNo: string, Zone: string, CPCODE: string, City: string, EmailId: string, CountryId: number, StateCode: string, Zip: string) {
    this.modalService.open(id);
    this.PersonName = CPName;
    this.CPCode = CPCODE;
    this.Address = address;
    this.City = City;
    this.edEmailId = EmailId;
    this.Zone = Zone;
    this.Mobile = ContactNo;
    this.Zip = Zip;
    this.collection.getCountryList().subscribe((data) => {
      this.countries = JSON.parse(JSON.stringify(data)).Payload;
    });
    this.CountryId = CountryId;
    this.collection.getStateListByCountryId(this.CountryId).subscribe(
      _stateListData => {
        this.states = JSON.parse(JSON.stringify(_stateListData)).Payload;
        this.StateId = StateCode;
      },
      error => {
        console.log('Failed while trying to load state list. ' + error);
      });

  }

  onChangeCountry(CountryCode: number) {
    this.collection.getStateListByCountryId(CountryCode).subscribe(
      _stateListData => {
        this.states = JSON.parse(JSON.stringify(_stateListData)).Payload;
      },
      error => {
        console.log('Failed while trying to load state list. ' + error);
      });
  }



  deleteCollectPersondata(CPCode: string) {
    let CompanyId = Number(sessionStorage.getItem("CompanyId"));
    var CPCode = CPCode;

    if (CPCode == undefined || CPCode == null || CPCode == '-') {
      this.toastr.warning("Data not available or not selected", "Warning");
    }
    else {
      this.collection.DeleteCollectPerson(CompanyId, CPCode).subscribe(
        _updateStatus => {
          this.toastr.success("Collect person successfully deleted ", "Success");
          this.LoadCollectPersonList();
        }, error => {
          this.toastr.error("Failed to delete collect person", "Error");
          this.LoadCollectPersonList();
        }
      );
    }
  }

  updateCPStatus(CPCode: string, event) {
    var Status = event.target.checked;
    let CompanyId = Number(sessionStorage.getItem("CompanyId"));
    var CPCode = CPCode;

    if (CPCode == undefined || CPCode == null || CPCode == '-') {
      this.toastr.warning("Data not available or not selected", "Warning");
    }
    else {
      this.collection.UpdateCPstatue(Status, CompanyId, CPCode).subscribe(
        _updateStatus => {
          this.toastr.success("Status successfully updated", "Success");
          this.LoadCollectPersonList();
        }, error => {
          this.toastr.error("Failed to update CP Status", "Error");
          this.LoadCollectPersonList();
        }
      );
    }
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }

  UpdateCollectPersonalData() {
    var CPName = '';
    var edEmailId = '';
    var ContactNo = '';
    var Address = '';
    var City = '';
    var Zone = '';
    var state = '';
    var country = 0;
    var zipcode = '';

    if (this.PersonName == undefined || this.PersonName == '') {
      CPName = null;
    } else {
      CPName = this.PersonName;
    }
    if (this.edEmailId == undefined || this.edEmailId == '') {
      edEmailId = null;
    }
    else {
      edEmailId = this.edEmailId;
    }
    if (this.Mobile == undefined || this.Mobile == '') {
      ContactNo = null;
    }
    else {
      ContactNo = this.Mobile;
    }
    if (this.Address == undefined || this.Address == '') {
      Address = null;
    }
    else {
      Address = this.Address;
    }

    if (this.City == undefined || this.City == '') {
      City = null;
    }
    else {
      City = this.City;
    }

    if (this.Zone == undefined || this.Zone == '') {
      Zone = null;
    }
    else {
      Zone = this.Zone;
    }

    if (this.StateId == undefined || this.StateId == '') {
      state = null;
    }
    else {
      state = this.StateId;
    }

    if (this.CountryId == undefined || this.CountryId == 0 || this.CountryId == null) {
      country = 0;
    }
    else {
      country = this.CountryId;
    }

    if (this.Zip == undefined || this.Zip == '') {
      zipcode = null;
    }
    else {
      zipcode = this.Zip;
    }
    this.collection.UpdateCollectPerson(CPName, edEmailId, ContactNo, Address, City, Zone, state, country, zipcode, this.CPCode).subscribe(
      _UpdateCP => {
        this.toastr.success("Collect person data updated successfully", "Success");
        this.closeModal('custom-modal-1');
        this.LoadCollectPersonList();
      }, error => {
        console.log("CP Data Updation failed" + error);
        this.toastr.error("Unable to update collect person data", "Error");
        this.LoadCollectPersonList();
      }
    );
  }
}
