import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UICustomizationComponent } from './uicustomization.component';

describe('UICustomizationComponent', () => {
  let component: UICustomizationComponent;
  let fixture: ComponentFixture<UICustomizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UICustomizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UICustomizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
