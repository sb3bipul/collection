// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  dev: true,
  uat: false,
  apiBaseUrl: 'http://localhost:50313/',//'http://3.221.58.176:60/',
  apiTokenUrl: 'http://localhost:50313/api/User',//'http://3.221.58.176:60/api/User',
  apiUrl: 'http://localhost:50313/',//'http://3.221.58.176:60/',
	pdfUrl: 'http://107.21.119.157/GoyaInvoice',
  //apiBaseUrl: 'http://localhost:55760/Api/',
  //apiTokenUrl: 'http://localhost:55760/token',
  companyId: 1,
  userId: '',
  accessToken: '',
  userName: '', 
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
