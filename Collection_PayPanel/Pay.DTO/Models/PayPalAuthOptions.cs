﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pay.DTO.Models
{
    public class PayPalAuthOptions
    {
        public string PayPalClientId { get; set; }
        public string PayPalClientSecret { get; set; }
    }
}
