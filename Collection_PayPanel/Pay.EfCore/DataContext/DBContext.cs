﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Pay.EfCore.DataContext
{
    public partial class DBContext:DbContext
    {
        public DBContext()
        {
        }
        public DBContext(DbContextOptions<DBContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=SB3LAP-07/SQLEXPRESS;Initial Catalog=Collection_SB3;Persist Security Info=True;User ID=sa;pwd=sb3soft");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<tblt_UserPostData>(entity =>
            //{
            //    entity.ToTable("tblt_UserPostData");
            //    entity.Property(e => e.POSTID).HasColumnName("POSTID");

            //    entity.Property(e => e.POSTNO).HasMaxLength(100).IsUnicode(false);
            //    entity.Property(e => e.CREATEDBY).HasMaxLength(500).IsUnicode(false);
            //    entity.Property(e => e.CREATEDON).HasColumnType("datetime");
            //    entity.Property(e => e.PRIVACYTYPEID).HasMaxLength(10).IsUnicode(false);
            //    entity.Property(e => e.POSTTYPEID).HasMaxLength(10).IsUnicode(false);
            //    entity.Property(e => e.COMMENTS).HasMaxLength(500).IsUnicode(false);
            //    entity.Property(e => e.FROMDATE).HasColumnType("datetime");
            //    entity.Property(e => e.TODATE).HasColumnType("datetime");
            //    entity.Property(e => e.UPDATEDBY).HasMaxLength(500).IsUnicode(false);
            //    entity.Property(e => e.UPDATEDON).HasColumnType("datetime");
            //    entity.Property(e => e.Header).HasMaxLength(250).IsUnicode(false);
            //    entity.Property(e => e.CompanyId).HasMaxLength(10).IsUnicode(false);
            //    entity.Property(e => e.DivisionId).HasMaxLength(100).IsUnicode(false);
            //    entity.Property(e => e.GroupId).HasMaxLength(100).IsUnicode(false);
            //});

            OnModelCreatingPartial(modelBuilder);
            //modelBuilder.Entity<PostTypeDto>().HasNoKey();
            //modelBuilder.Entity<PrivacyTypeDto>().HasNoKey();
            //modelBuilder.Entity<UserPostDto>().HasNoKey();
            //modelBuilder.Entity<UserDto>().HasNoKey();
            //modelBuilder.Entity<PostDataDto>().HasNoKey();
            //modelBuilder.Entity<CompanyDto>().HasNoKey();
            //modelBuilder.Entity<BrokerGroupDto>().HasNoKey();
            //modelBuilder.Entity<DivisionCompanyDto>().HasNoKey();
            //modelBuilder.Entity<ZoneDto>().HasNoKey();
            //modelBuilder.Entity<PostLocationDto>().HasNoKey();
            //modelBuilder.Entity<PostTagDto>().HasNoKey();

        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
