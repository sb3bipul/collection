﻿using PayPal.Api;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pay.Services.Interfaces
{
    public interface IPaypalServices
    {
        Payment CreatePayment(decimal amount, string returnUrl, string cancelUrl, string intent);
        Payment ExecutePayment(string paymentId, string payerId);
    }
}
