﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMSWebApi.Models
{
    public class SearchLoad
    {
        public string ProductId { get; set; }
        public string ProductImage { get; set; }
        public string ProductName { get; set; }
        public string WareHouseName { get; set; }
        public string CustomerId { get; set; }
        public string SearchText { get; set; }

    }
}