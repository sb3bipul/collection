﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMSWebApi.Models
{
    public class PriceList
    {
        public string CategoryId { get; set; }
        public string SubCategoryId { get; set; }
        public string CategoryCode { get; set; }
        public string SubCategoryCode { get; set; }
        public string CompanyId { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string CustomerCode { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string BrokerId { get; set; }
        public int SalesCategoryId { get; set; }
        public int SalesSubCategoryId { get; set; }
        public string SalesCategoryCode { get; set; }
        public string SalesSubCategoryCode { get; set; }
        public string TagName { get; set; }
        public int Days { get; set; }
        public string BillingCO { get; set; }
    }
}