﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMSWebApi.Models
{
    public class GuidedSearch
    {
        public string vendorId { get; set; }
        public string categoryId { get; set; }
        public string customerId { get; set; }
        public string warehouseId { get; set; }
        public string groupId { get; set; }
        public string searchText { get; set; }
        public string mfgUpc { get; set; }
        public string searchFor { get; set; }
        public string salesmanId { get; set; }
        public string companyId { get; set; }
        public string productId { get; set; }
        public string UPC1 { get; set; }
        public int lineNo { get; set; }
        public int OrdQty { get; set; }
        public decimal ListPrice { get; set; }
        public string promoCode { get; set; }
        public bool isRestricted { get; set; }
        public decimal retailPrice { get; set; }
        public decimal promoAmount { get; set; }
        public bool isPromoItem { get; set; }
        public int promoMinQuantity { get; set; }
        public string poNumber { get; set; }
        public string clientPONumber { get; set; }
        public string comments { get; set; }
        public DateTime shippingDate { get; set; }
        public DateTime orderDate { get; set; }
        public DateTime cancelDate { get; set; }
        public string orderMode { get; set; }
        public string CartItems { get; set; }
        public string message { get; set; }
        public bool SBTag { get; set; }
        public string fileName { get; set; }
        public string fileData { get; set; }
        public string fileType { get; set; }
        public DateTime sentDate { get; set; }
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
        public string status { get; set; }
        public string subCategoryId { get; set; }
        


    }
}