﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMSWebApi.Models
{
    public class CustomerDetail
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Region { get; set; }
        public int? CompanyId { get; set; }
        public string WHCompany { get; set; }
        public string CompanyName { get; set; }
        public string WHName { get; set; }
        public string ContactNo { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string CustomerId { get; set; }
        public string StreetAddress1 { get; set; }
        public string StreetAddress2 { get; set; }
        public string Phone { get; set; }
        public string SalesmanId { get; set; }
        

    }

    public class CustomerList
    {
        public string UserID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Region { get; set; }
        public string CompanyID { get; set; }
        public string WHCompany { get; set; }
        public string CompanyName { get; set; }
        public string WHName { get; set; }
        public string ContactNo { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Status { get; set; }
        public string CreditStatus { get; set; }
        public string CustomerBalance { get; set; }
        public string Zone { get; set; }
        
    }
    public class CustomerLst1
    {
        public string CustomerId { get; set; }
        public string Customer_Name { get; set; }
        public string CompanyID { get; set; }
        public string BillingCo { get; set; }
        public string BrokerID { get; set; }
        public string ADDRESS { get; set; }
        public string STREET { get; set; }
        public string CITY { get; set; }
        public string STATE { get; set; }
        public string ZIP { get; set; }
        public string COUNTRY { get; set; }
        public string CONTACTNO { get; set; }
        public string EMAILID { get; set; }
        public string STATUS { get; set; }
        public string WHSE_CODE { get; set; }
        public string CREDITSTATUS { get; set; }
        public string BALANCE { get; set; }
        public string CUSTOMERBALANCE { get; set; }
        public string CUSTOMERBALANCEOVER30 { get; set; }
        public string PromoAmount { get; set; }
        public string TaxGroup { get; set; }
        public string ContractNo { get; set; }
        public string CreditDescription { get; set; }
        public string CreditLimit { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string OrderQtyLength { get; set; }
    }

    public class CustomerFullDetail
    {
        public string Msg { get; set; }
        public string CustomerID { get; set; }
        public string CustomerFullAddress { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string ContactNo { get; set; }
        public string EmailID { get; set; }
        public string Organization { get; set; }
        public string Website { get; set; }
        public decimal CustomerBalance { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string Location { get; set; }
        public string BillingAddress { get; set; }
        public string ShippingAddress { get; set; }
        public string CustomerImage { get; set; }
        public string CustomerImagePath { get; set; }

    }

    public class CustomerStoreHours
    {
        public string Msg { get; set; }
        public string CustomerID { get; set; }
        public string CompanyID { get; set; }
        public Int16 DayHoursID { get; set; }
        public string WeekDay { get; set; }
        public DateTime CreatedDate { get; set; }
        public string TimeFrom { get; set; }
        public string TimeTo { get; set; }
        public string SrlNo { get; set; }
    }

    public class CustomerReceivingeHours
    {
        public string Msg { get; set; }
        public string CustomerID { get; set; }
        public string CompanyID { get; set; }
        public Int16 DayHoursID { get; set; }
        public string WeekDay { get; set; }
        public DateTime CreatedDate { get; set; }
        public string TimeFrom { get; set; }
        public string TimeTo { get; set; }
        public string SrlNo { get; set; }
    }
    public class CustomerPayment
    {
        public string Msg { get; set; }
        public decimal CustomerBalance { get; set; }
    }
    public class CustomerLocation
    {
        public string Msg { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string Location { get; set; }
        public string BillingAddress { get; set; }
        public string ShippingAddress { get; set; }
    }

}