﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMSWebApi.Models
{
    public class Order
    {
        public string BrokerId { get; set; }
        public string CustomerId { get; set; }
        public string OrderNumber { get; set; }
        public string Status { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int CompanyId { get; set; }
        public string GoyaCompanyId { get; set; }
        public bool isPickUp { get; set; }
        public string  BasketId { get; set; }
        public int  Message { get; set; } // 0 - Fail,1 - Success

        public string Day { get; set; } // Day for delivery
        public string NotDay { get; set; } // Day - Not for delivery
        public string Department { get; set; }
        public string ItemCode { get; set; }
        public int CaseQuantity { get; set; }
        public int UnitQuantity { get; set; }
        public DateTime DeliveryDate { get; set; }
        public int CatalogId { get; set; }
        public string WarehouseId { get; set; }
        public string Comments { get; set; }
        public string Result { get; set; }
        public string EOR { get; set; }
        public string CurrentEOR { get; set; } // current EOR for template
        public string ClientPONumber { get; set; }
        public string PromoCode { get; set; }
        public string LanguageId { get; set; }
        public string Action { get; set; } // ITEM CODE or UPC
        public string Dept { get; set; }
        public decimal AmountCollected { get; set; }
        public bool isActive { get; set; }
        public int totalCaseQuantity { get; set; }
        public decimal totalAmount { get; set; }
        public string productType{ get; set; }
        public string templateName{ get; set; }
        public int totalUnitQuantity{ get; set; }
        public string pageTitle { get; set; }
        public bool isRestricted { get; set; }
        public int QOH { get; set; }
        public decimal CasePrice {get;set;}
        public decimal RetailPrice {get;set;}
        public int UnitForCasePrice { get; set; }
        public decimal PromoAmount {get;set;}
        public bool IsPromoItem {get;set;}
        public int PromoMinQuantity {get;set;}
        public decimal OriginalCasePrice { get; set; }
        public string OrderMode { get; set; }
        public string BillingCO { get; set; }

        // Storing Location while submitting order
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string  Location { get; set; }

        // Email Order
        public string EmailTo { get; set; }
        public string EmailBody { get; set; }
        public string AttachmentPath { get; set; }
        public string FileType { get; set; }

        // Cart Items in one Object for Offline
        public string CartItems { get; set; }
        public string PaymentTerm { get; set; }
        public string PONumber { get; set; }
        //public DateTime OrderDate { get; set; } 
        //public DateTime CancelDate { get; set; }
        //public string CustomerName { get; set; } 
    }
    public class OrderParam
    {
        public string BrokerId { get; set; }
        public string CompanyID { get; set; }
        public string BillingCO { get; set; }
    }
    public class OrderFiles
    {
        public string BrokerId { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string PONumber { get; set; }
        public decimal totalAmount { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime DeliveryDate { get; set; }
        public DateTime CancelDate { get; set; }
        public int CompanyId { get; set; }
        public string WarehouseId { get; set; }
        public string BasketId { get; set; }
        public string ItemCode { get; set; }
        public int CaseQuantity { get; set; }
        public int totalCaseQuantity { get; set; }
        public decimal CasePrice { get; set; }
        public int UnitForCasePrice { get; set; }

    }
    public class OrderItemDetailsViewModel
    {
        public string SalesmanId { get; set; }
        public OrderHistory[] itemDetails { get; set; }
    }
    public class savedOrder
    {
        public string SalesmanId  { get; set; }
        public ICollection<OrderHistory> Details { get; set; }
    }
    public class OrderHistory
    {
        public string CustomerID { get; set; }
        public string SalesmanID { get; set; }
        public int CompanyID { get; set; }
        public string ClientPONumber { get; set; }
        public DateTime OrderDate { get; set; }

        public string OrderQuantity { get; set; }
        public bool Pickup { get; set; }
        public decimal AmountCollected { get; set; }
        public decimal TotalAmount { get; set; }
        public string CustomerName { get; set; }
        public int ItemCount { get; set; }
        public string PONumber { get; set; }
    }

    /// Save Order By Chat Text
    public class OrderViewModel
    {
        public IEnumerable<PayLoadItem> payLoadItems { get; set; }
        //public OrderItems[] orderitemDetails { get; set; }
    }

    public class PayLoadItem
    {
        public string CustomerID { get; set; }
        public string SalesmanID { get; set; }
        public string CompanyID { get; set; }
        public string OnOffline { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public Int32 Qty { get; set; }
        public decimal ItemPrice { get; set; }
        public string ItemStatus { get; set; }
        public string OrderVerify { get; set; }
        public string OrderId { get; set; }
        public string OrderCreateFlag { get; set; }
        public string OrderDate { get; set; }
        //public Int32 IsConfirmByError { get; set; }
    }

    public class NotificationModel
    {
        public string CompanyID { get; set; }
        public IEnumerable<PayLoadNotification> payLoadNotifications { get; set; }
    }

    public class PayLoadNotification
    {
        public String OrderNo { get; set; }
    }

    public class ActivityLogViewModel
    {
        public IEnumerable<ActivityLogItem> activityLogItems { get; set; }
    }

    public class ActivityLogItem
    {
        public string UserLoginID { get; set; }
        public string ActivityDate { get; set; }
        public string ActivityDescription { get; set; }
        public string OrderId { get; set; }
        public string CustomerID { get; set; }
        public string CompanyID { get; set; }
        public string ActivityLogType { get; set; }
    }

}