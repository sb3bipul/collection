﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMSWebApi.Models
{
    public class UserProfile
    {
        public string UserName { get; set; }
        public string UserFullName { get; set; }
        public string CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string Role { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Domain { get; set; }
        public string UserType { get; set; }
        public string BrokerPicName { get; set; }
        public IEnumerable<UserProfile> UserProfileList { get; set; }
        public bool SuccessStatus { get; set; }
        public string Password { get; set; }
        public string CreateBy { get; set; }
        public string UpdateBy { get; set; }
        public bool IsNewReleaseFlag { get; set; }
        public string OMSRole { get; set; }
        public string RPTRole { get; set; }
        public string LoginCompanyId { get; set; }
    }
    public class UserLoginByEmail
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string UserPassword { get; set; }
        public string CompanyId { get; set; }
        public string EmailID { get; set; }
    }

}