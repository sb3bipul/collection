﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMSWebApi.Models
{
    public class BackRoomInventory
    {
        public string BrokerId { get; set; }
        public string CustomerId { get; set; }
        public string OrderNumber { get; set; }
        public string Status { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int CompanyId { get; set; }
        public string GoyaCompanyId { get; set; }
        public bool isPickUp { get; set; }
        public string BasketId { get; set; }
        public int Message { get; set; } // 0 - Fail,1 - Success

        public string Day { get; set; } // Day for delivery
        public string NotDay { get; set; } // Day - Not for delivery
        public string Department { get; set; }
        public string ItemCode { get; set; }
        public int CaseQuantity { get; set; }
        public int UnitQuantity { get; set; }
        public DateTime DeliveryDate { get; set; }
        public int CatalogId { get; set; }
        public string WarehouseId { get; set; }
        public string Comments { get; set; }
        public string Result { get; set; }
        public string EOR { get; set; }
        public string CurrentEOR { get; set; } // current EOR for template
        public string ClientPONumber { get; set; }
        public string PromoCode { get; set; }
        public string LanguageId { get; set; }
        public string Action { get; set; } // ITEM CODE or UPC
        public string Dept { get; set; }
        public decimal AmountCollected { get; set; }
        public bool isActive { get; set; }
        public int totalCaseQuantity { get; set; }
        public decimal totalAmount { get; set; }
        public string productType { get; set; }
        public string templateName { get; set; }
        public int totalUnitQuantity { get; set; }
        public string pageTitle { get; set; }
        public bool isRestricted { get; set; }
        public int QOH { get; set; }
        public decimal CasePrice { get; set; }
        public decimal RetailPrice { get; set; }
        public int UnitForCasePrice { get; set; }
        public decimal PromoAmount { get; set; }
        public bool IsPromoItem { get; set; }
        public int PromoMinQuantity { get; set; }
        public decimal OriginalCasePrice { get; set; }

        // Storing Location while submitting order
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string Location { get; set; }
    }
}