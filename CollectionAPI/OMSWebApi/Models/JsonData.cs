﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMSWebApi.Models
{
    public class JsonData
    {
        public string domain { get; set; }
        public string events { get; set; }
        public string payload { get; set; }
        public string clientData { get; set; }
        public string status { get; set; }
    }
}