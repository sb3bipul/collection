﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMSWebApi.Models
{
    public class CountryModel
    {
        public string CountryName { get; set; }
        public int CountryCode { get; set; }
    }
    
}