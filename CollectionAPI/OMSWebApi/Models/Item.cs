﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMSWebApi.Models
{
    public class Item
    {
        public int CompanyId { get; set; }
        public string LanguageId { get; set; }
        public string SearchText { get; set; }
        public string CatalogId { get; set; }
        public string WarehouseId { get; set; }
        public DateTime FromDate {get;set;}
        public DateTime ToDate { get; set; }
        public string GoyaCompanyId { get; set; }
        public string Zone { get; set; }
        public string BillingCompanyId { get; set; }
        public string CustomerId { get; set; }
        public int Limit { get; set; }
        public int Offset { get; set; }
        public string IsPromo {get;set;}
        public string ProductCode { get; set; }
        public string SalesmanId { get; set; }
        public string MenuLabel { get; set; }
       
    }

    public class ItemModel
    {
        public int ItemId { get; set; }
        public int CompanyId { get; set; }
        public int VendorId { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string UPCNumber { get; set; }
        public int ItemCategoryId { get; set; }
        public string SecondaryItemNumber { get; set; }
        public int ZoneId { get; set; }
        public bool? IsActive { get; set; }
    }

}