﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMSWebApi.Models
{
    public class PaymentModel
    {
        //public string PaymentReceipt { get; set; }
        public string CustomerId { get; set; }
        public string CollectionMode { get; set; }
        public decimal Amount { get; set; }
        public string CheckNo { get; set; }
        public string AccountNo { get; set; }
        public string BankName { get; set; }
        public string CardType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DigitalMode { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string CollectedBy { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime PaymentDate { get; set; }
        public string TempReceiptNo { get; set; }
        public string OnOffLine { get; set; }
        public int CompanyId { get; set; }
        public string BranchName { get; set; }
        public string TransectionId { get; set; }
        public string PaymentStatus { get; set; }
        public string InvoiceNo { get; set; }

        public string SendToMobile { get; set; }
        public string SendToEmail { get; set; }
        public string Comment { get; set; }
        public Boolean IsDisplayReceipt { get; set; }
        public decimal PaymentConvenienceFee { get; set; }
        //public IEnumerable<PayLoadItem> payLoadItems { get; set; }

        public IEnumerable<PayLoadImage> payLoadImage { get; set; }
    }

    //public class PayLoadItem
    //{
    //    public string ImageName { get; set; }
    //    public string ImagePath { get; set; }
    //}
    public class PayLoadImage
    {
        public string ImageName { get; set; }
        public string ImageBase64String { get; set; }
    }


    public class PrefixModel
    {
        public int Id { get; set; }
        public int StartNo { get; set; }
        public int LastNo { get; set; }
        public string Prefix { get; set; }
        public string ModuleName { get; set; }
        public int UserId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CompanyId { get; set; }
    }
    public class PaymentHistory
    {
        public string ReceiptNo { get; set; }
        public string Customerid { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string CollectedBy { get; set; }
    }

    public class PaymentReceipt
    {
        public string ReceiptNo { get; set; }
        public string PDFName { get; set; }
        public string PdfBase64string { get; set; }
    }

    public class Search
    {
        public int CompanyId { get; set; }
    }

    public class UpdatePayment
    {
        public string order_id { get; set; }
        public string status { get; set; }

    }

    public class RmvPendingPayment
    {
        public string ReceiptNo { get; set; }
        public string CompanyId { get; set; }
    }

    public class SMSService
    {
        public string send_to { get; set; }
        public string message { get; set; }
    }

    public class ResendPaymentReceipt
    {
        public string ReceiptNo { get; set; }
        public string CompanyId { get; set; }
        //public string CustomerId { get; set; }
    }
}