﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMSWebApi.Models
{
    public class ServerData
    {
    }

    public class subCategory
    {
        public string CompanyId { get; set; }
        public string LanguageId { get; set; }
        public string CategoryID { get; set; }
        public string CategoryName { get; set; }
        public string SubCategoryID { get; set; }
        public string SubCategoryName { get; set; }
    }

    public class products
    {
        public string CompanyId { get; set; }
        public string LanguageId { get; set; }
        public string BillingId { get; set; }
        public string WHID { get; set; }
        public string ProductID { get; set; }
        public string DisplayName { get; set; }
        public string ProductDesc { get; set; }
        public string MFGCode { get; set; }
        public string UPC { get; set; }
        public string Brand { get; set; }
        public string PackType { get; set; }
        public string PackSize { get; set; }
        public string HEIGHT { get; set; }
        public string UNIT_WEIGHT { get; set; }
        public string WIDTH { get; set; }
        public string VENDOR { get; set; }
        public string FreeQty { get; set; }
        public string Cat1_ID { get; set; }
        public string Cat2_ID { get; set; }
        public string Image_Path { get; set; }
        public string BImageFileName { get; set; }
        public string ImageFileName { get; set; }
        public string TaxId { get; set; }
        public string TaxableBy { get; set; }
        public string DiscountPercentage { get; set; }
        public string DiscuntAmount { get; set; }
    }

    public class prices
    {
        public string CompanyID { get; set; }
        public string SalesmanId { get; set; }
        public string ProductID { get; set; }
        public string CasePrice { get; set; }
        public string SuggRetl { get; set; }
        public string UnitPrice { get; set; }
        public string UnitCost { get; set; }
        public string By_Qty { get; set; }
        public string PromoMinQty { get; set; }
        public string IsPromo { get; set; }
        public string CSBZN { get; set; }
        public string PRSUNT { get; set; }
        public string PromoPrice { get; set; }
    }

    public class warehouses
    {
        public string BILLING_CO { get; set; }
        public string Whse_Code { get; set; }
        public string Whse_Name { get; set; }
        public string Cust_Whse_Class { get; set; }
        public string CompanyID { get; set; }
    }

    public class stocks
    {
        public string CompanyID { get; set; }
        public string BILLCO { get; set; }
        public string ITEM { get; set; }
        public string CATEGORY_ID { get; set; }
        public string SUBCATEGORY_ID { get; set; }
        public string WHSE_CODE { get; set; }
        public string QOH { get; set; }
    }

    public class brokers
    {
        public string BRROUT { get; set; }
        public string USERNAME { get; set; }
        public string BrokerDivisionCode { get; set; }
        public string COMPANY { get; set; }
        public string BrokerGroupCode { get; set; }
    }


}