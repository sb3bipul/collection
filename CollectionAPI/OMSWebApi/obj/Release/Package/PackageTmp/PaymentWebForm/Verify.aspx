﻿<%@ Page Language="C#" MasterPageFile="~/PaymentWebForm/PayPanel.Master" AutoEventWireup="true" CodeBehind="Verify.aspx.cs" Inherits="OMSWebApi.PaymentWebForm.Verify" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .divider-text {
            position: relative;
            text-align: center;
            margin-top: 15px;
            margin-bottom: 15px;
        }

            .divider-text span {
                padding: 7px;
                font-size: 12px;
                position: relative;
                z-index: 2;
            }

            .divider-text:after {
                content: "";
                position: absolute;
                width: 100%;
                border-bottom: 1px solid #ddd;
                top: 55%;
                left: 0;
                z-index: 1;
            }

        .btn-facebook {
            background-color: #405D9D;
            color: #fff;
        }

        .btn-twitter {
            background-color: #42AEEC;
            color: #fff;
        }
    </style>

    <script>
        function display() {
            var verifyDiv = document.getElementById("divVerify");
            verifyDiv.style.display = 'block';
        }
        window.onload = display;
    </script>

    <script src="https://js.stripe.com/v3/"></script>
    <script>
        var el = document.getElementById("verifyAccount");
        if (el) {
            el.addEventListener("click", VerifyAC);
        }
        function VerifyAC() {
            debugger;
            var stripe = Stripe("pk_test_51ITpT7HoTn5ARElrJHr9ihINaWBotTmpUHZ0YHSRIk0TXJVl1tBX3Esf4DEaHU16kVb6bKkOzS2TYHYeqMhaZvoc00uJAfIsCG");
            var AccountNo = document.getElementById("accountNo").value;
            var HolderName = document.getElementById("holderName").value;
            var AcType = document.getElementById("accounttype").value;
            var elements = stripe.elements();
            stripe
                .createToken('bank_account', {
                    country: 'US',
                    currency: 'USD',
                    routing_number: '110000000',
                    account_number: '000123456789',
                    account_holder_name: 'Bipul Pan',
                    account_holder_type: 'indivitual',
                })
                .then(function (result) {
                    debugger;
                    // Handle result.error or result.token
                    console.log(result.token.id);
                    console.log(result.token.bank_account.id);
                    document.getElementById("bankToken").value = result.token.id;
                    document.getElementById("bankId").value = result.token.bank_account.id;
                    //alert(result.token);                
                });
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="divVerify" class="row" style="display: none;">
        <div class="col-lg-6 col-md-8 m-auto">
            <div class="payment-contnt">
                <div class="payment-contnt-1">
                    <img src="../Content/images/logo.png" style="width: 80%; height: auto; margin-bottom: 20px;" />
                    <h5>Verify Bank Details</h5>
                </div>
                <div class="content-box">
                    <div class="payment-contnt-4">
                        <form id="form1" runat="server">
                            <div class="form-group input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-user"></i></span>
                                </div>
                                <input id="accountNo" class="form-control" placeholder="Account Number" type="text" />
                            </div>
                            <div class="form-group input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                </div>
                                <input id="holderName" class="form-control" placeholder="Account Holder Name" type="text" />
                            </div>
                            <div class="form-group input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-building"></i></span>
                                </div>
                                <select class="form-control" id="accounttype">
                                    <option selected="">Select Account Type</option>
                                    <option value="company">Company</option>
                                    <option value="individual">Individual</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <button id="verifyAccount" type="submit" class="btn btn-primary btn-block">Verify Account</button>
                            </div>

                            <asp:HiddenField ID="bankToken" runat="server" />
                            <asp:HiddenField ID="bankId" runat="server" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
