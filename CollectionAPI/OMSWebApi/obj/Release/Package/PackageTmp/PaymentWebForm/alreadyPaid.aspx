﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaymentWebForm/PayPanel.Master" AutoEventWireup="true" CodeBehind="alreadyPaid.aspx.cs" Inherits="OMSWebApi.PaymentWebForm.alreadyPaid" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-lg-6 col-md-8 m-auto">
            <div class="payment-contnt">
                <div class="payment-contnt-1">
                    <img src="../Content/images/logo.png" style="width: 80%; height: auto; margin-bottom: 20px;">
                </div>
                <div class="alert alert-info">
                    This invoice is already Paid. !!               
                </div>
            </div>
        </div>
    </div>
</asp:Content>
