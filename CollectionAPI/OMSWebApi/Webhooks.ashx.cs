﻿using OMSWebApi.Models;
using OMSWebApi.Repository;
using Stripe;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;

namespace OMSWebApi
{
    /// <summary>
    /// Summary description for Webhooks
    /// </summary>
    public class Webhooks : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            SMSService sms = new SMSService();
            var endpointSecret = "whsec_y8mBN2psCSSr4mB3ncakQdDzlCzy7Kcc";
            var json = new StreamReader(context.Request.InputStream).ReadToEnd();

            try
            {
                var stripeEvent = EventUtility.ConstructEvent(
                    json,
                    context.Request.Headers["Stripe-Signature"],
                    endpointSecret
                );

                switch (stripeEvent.Type)
                {
                    case Events.PaymentIntentSucceeded:
                        // look up the payment in the database and update it's state
                        // fulfill order
                        // send a customer email
                        
                        var paymentIntent = stripeEvent.Data.Object as PaymentIntent;

                        //sms.send_to = Convert.ToString("+918777399822");
                        //sms.message = Convert.ToString("Your Payment Done"+ paymentIntent.Id);

                        //CallSMSServiceAPI(sms);
                        UpdatePaymentData(Convert.ToString(paymentIntent.Id));

                        Console.WriteLine($"Payment Succeeded {paymentIntent.Id} for ${paymentIntent.Amount}");
                        break;
                    default:
                        Console.WriteLine($"Got event {stripeEvent.Type}");
                        break;
                }
            }
            catch (StripeException e)
            {
                Console.WriteLine(e);
                throw e;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public string CallSMSServiceAPI(SMSService SMS)
        {
            string data = string.Empty;
            string SmsAPI = Convert.ToString(ConfigurationSettings.AppSettings["SingleSMSUrl"].ToString());
            string token = Convert.ToString(ConfigurationSettings.AppSettings["Token"].ToString());
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(SmsAPI);
                request.Timeout = 1000000;
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("Authorization", "AppToken " + token);
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (var sw = new StreamWriter(request.GetRequestStream()))
                {
                    string json = serializer.Serialize(SMS);
                    sw.Write(json);
                    sw.Flush();
                }
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    data = reader.ReadToEnd();
                }
                return data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        /// <summary>
        /// Function to update ispaid flag in stripe webhook
        /// </summary>
        /// <param name="TnxId"></param>
        /// <returns></returns>
        public string UpdatePaymentData(string TnxId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            DBConnection dbConnectionObj = null;
            SqlCommand SqlCmd = null;
            DataToJson dtToJson = new DataToJson();
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.updatePayPanelPaymentStatus, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@TnxId", TnxId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                jsonString = dtToJson.convertDataTableToJson(dt, "UpdatePaymentData", true);
                return jsonString;
            }
            catch (Exception ex)
            {

            }
            return null;
        }
    }
}