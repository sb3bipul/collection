﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaymentWebForm/PayPanel.Master" AutoEventWireup="true" CodeBehind="CreditCard.aspx.cs" Inherits="OMSWebApi.PaymentWebForm.CreditCard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="https://js.stripe.com/v3/"></script>
    <script src="https://www.paypal.com/sdk/js?client-id=test&currency=USD"></script>
    <script type="text/javascript">
        function onChange() {
            var paymenttype = document.getElementsByName("customRadio");
            var cardfn = document.getElementById("card");
            var bankfn = document.getElementById("bank");
            var payPal = document.getElementById("payPal");

            for (var i = 0, length = paymenttype.length; i < length; i++) {
                if (paymenttype[i].checked) {
                    if (paymenttype[i].value === "CreditCard") {
                        cardfn.style.display = 'block'; // Display
                        bankfn.style.display = 'none'; // Hide
                        payPal.style.display = 'none'; // Hide
                    }
                    else if (paymenttype[i].value === "BankTransfer") {
                        cardfn.style.display = 'none'; // Hide
                        bankfn.style.display = 'block'; // Display
                        payPal.style.display = 'none'; // Hide
                    }
                    else {
                        cardfn.style.display = 'none'; // Hide
                        bankfn.style.display = 'none'; // Hide
                        payPal.style.display = 'block'; // Display
                    }
                    break;
                }
            }

        };

        document.addEventListener("DOMContentLoaded", function () {
            var success =<%=isPaid%>;
            console.log(success);
            if (success == true) {
                window.location.href = "alreadyPaid.aspx";
            }
            else {
                var stripe = Stripe("pk_test_51ITpT7HoTn5ARElrJHr9ihINaWBotTmpUHZ0YHSRIk0TXJVl1tBX3Esf4DEaHU16kVb6bKkOzS2TYHYeqMhaZvoc00uJAfIsCG");
                var elements = stripe.elements();
                var cardElement = elements.create('card');
                cardElement.mount('#card-element');

                var form = document.getElementById("paymentForm");
                form.addEventListener("submit", function (e) {
                    e.preventDefault();
                    stripe.confirmCardPayment(
                    "<%= clientSecret %>",
                        {
                            payment_method: {
                                card: cardElement,
                                billing_details: {
                                    name: "<%= CustName %>"
                                }
                            }
                        }
                    ).then(function (result) {
                        if (result.error) {
                            alert(result.error.message);
                        }
                        else {
                            debugger;
                            form.submit();
                            var paymentID = result.paymentIntent.payment_method;
                            var PaymentType = result.paymentIntent.payment_method_types[0];

                            localStorage.setItem("PaymentID", paymentID);
                            localStorage.setItem("PaymentType", PaymentType);
                            localStorage.setItem("Amount", result.paymentIntent.amount);
                            //alert("Payment Successfully done!");

                            window.location.href = "success.aspx";
                            cardElement.unmount();
                            cardElement.clear();
                            cardElement.destroy();
                        }
                    })
                });
            }
        });

    </script>

    <script type="text/javascript">
        paypal.Buttons(
            {
                createOrder: function (data, actions) {
                    // This function sets up the details of the transaction, including the amount and line item details.
                    return actions.order.create({
                        purchase_units: [{
                            amount: {
                                value: '1'
                            },
                            currency: {
                                value: 'USD'
                            }
                        }]
                    });
                },

                onApprove: function (data, actions) {
                    return actions.order.capture().then(function (details) {
                        if (details.status === "COMPLETED") {
                            localStorage.setItem("PayPalPaymentDate", details.create_time);
                            localStorage.setItem("PayPalTnxID", details.id);
                            localStorage.setItem("PayPalCurrency", details.purchase_units[0].amount.currency_code);
                            localStorage.setItem("PayPalPayAmount", details.purchase_units[0].amount.value);

                            window.location.href = "paypal_paid.aspx";
                        }
                    });
                }
            }
        ).render('#paypal-button-container');
        // This function displays Smart Payment Buttons on your web page.
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-lg-6 col-md-8 m-auto">
            <div class="payment-contnt">
                <div class="payment-contnt-1">
                    <img src="../Content/images/logo.png" style="width: 80%; height: auto; margin-bottom: 20px;" />
                    <h5>Invoice from Goya Foods Inc</h5>
                    <p>Billed to <%= CustName %></p>
                </div>
                <div class="content-box">
                    <div class="payment-contnt-4">
                        <h5>$ <%= Amount %> due on <%= DueDate %></h5>
                        <p>Choose how would you like to pay</p>

                        <div class="radio-container mt-4">
                            <div class="custom-control custom-radio mb-2">
                                <input type="radio" id="customRadio1" name="customRadio" value="CreditCard" class="custom-control-input" onchange="onChange();" />
                                <label class="custom-control-label" for="customRadio1">
                                    <span class="img-box">
                                        <img src="../Content/images/credit-card.png" style="width: 25px;" /></span> Credit Card</label>
                            </div>

                            <div class="custom-control custom-radio mb-2">
                                <input type="radio" id="customRadio2" name="customRadio" value="BankTransfer" class="custom-control-input" onchange="onChange();" />
                                <label class="custom-control-label" for="customRadio2">
                                    <span class="img-box">
                                        <img src="../Content/images/bank.png" style="width: 20px;" /></span> Bank Transfer</label>
                            </div>

                            <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio3" name="customRadio" value="Paypal" class="custom-control-input" onchange="onChange();" />
                                <label class="custom-control-label" for="customRadio3">
                                    <span class="img-box">
                                        <img src="../Content/images/paypal.png" style="width: 25px;" /></span> Paypal</label>
                            </div>
                        </div>

                        <div id="card" style="display: none;">
                            <%--<span><%= clientSecret %></span>--%>
                            <form id="paymentForm" runat="server" method="post">
                                <div id="card-element"></div>
                                <button type="submit" class="btn btn-block btn-info mt-3">Pay</button>
                            </form>
                        </div>

                        <div id="bank" style="display: none;">
                            <button type="submit" class="btn btn-block btn-info mt-3" onclick="location.href = 'Verify.aspx';">Pay</button>
                        </div>

                        <div id="payPal" style="display: none;">
                            <div id="paypal-button-container"></div>
                        </div>


                        <%--<table class="card-details mt-5">
                                    <tr>
                                        <td>
                                            <div id="card-element"></div>
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <input type="text" class="form-control border-0" placeholder="MM" />
                                                <input type="text" class="form-control border-0" placeholder="YY" />
                                                <input type="text" class="form-control border-0" placeholder="CVC" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
