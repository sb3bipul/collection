﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaymentWebForm/PayPanel.Master" AutoEventWireup="true" CodeBehind="success.aspx.cs" Inherits="OMSWebApi.PaymentWebForm.success" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function load() {
            d = new Date();
            utc = d.getTime() + (d.getTimezoneOffset() * 60000);
            nd = new Date(utc + (3600000 * -5));
            //return nd.toLocaleString();

            var paymentTnx = localStorage.getItem("PaymentID");
            var Payamount = localStorage.getItem("Amount");
            document.getElementById("lblTnxId").innerHTML = "Transaction No: " + paymentTnx;
            document.getElementById("lblPaymentM").innerHTML = "Payment Method: " + localStorage.getItem("PaymentType");
            document.getElementById("lblAmt").innerHTML = "Amount: "+ Payamount;
            document.getElementById("lblDtTime").innerHTML = "Date & Time: " + nd.toLocaleString();
        }
        window.onload = load;
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-lg-6 col-md-8 m-auto">
            <div class="payment-contnt">
                <div class="payment-contnt-1">
                    <img src="../Content/images/logo.png" style="width: 80%; height: auto; margin-bottom: 20px;" />
                </div>
                <div class="alert alert-info">
                    Your payment has been completed successfully.               
                </div>

                <p>
                    <label id="lblPaymentM"></label>
                </p>
                <p>
                    <label id="lblTnxId"></label>
                </p>
                <p>
                    <label id="lblAmt"></label>
                </p>
                <p>
                    <label id="lblDtTime"></label>
                </p>
            </div>
        </div>

    </div>
</asp:Content>
