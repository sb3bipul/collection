﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BankTransfer.aspx.cs" Inherits="OMSWebApi.PaymentWebForm.BankTransfer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="https://js.stripe.com/v3/"></script>

    <script>
        document.addEventListener("DOMContentLoaded", function () {
            debugger;
            var stripe = Stripe("pk_test_51ITpT7HoTn5ARElrJHr9ihINaWBotTmpUHZ0YHSRIk0TXJVl1tBX3Esf4DEaHU16kVb6bKkOzS2TYHYeqMhaZvoc00uJAfIsCG");
            var elements = stripe.elements();
            var AccountNo = document.getElementById("accountNo").value;
            var HolderName = document.getElementById("holderName").value;
            var AcType = document.getElementById("accounttype").value;

            stripe
                .createToken('bank_account', {
                    country: 'US',
                    currency: 'USD',
                    routing_number: '110000000',
                    account_number: AccountNo,
                    account_holder_name: 'Jenny Rosen',
                    account_holder_type: 'individual',
                })
                .then(function (result) {
                    debugger;
                    // Handle result.error or result.token
                    console.log(result.token.id);
                    console.log(result.token.bank_account.id);
                    document.getElementById("bankToken").value = result.token.id;
                    document.getElementById("bankId").value = result.token.bank_account.id;
                    //alert(result.token);
                });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <button id="link-button">Link Account</button>
        <div class="form-group input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-user"></i></span>
            </div>
            <input id="accountNo" class="form-control" placeholder="Account Number" type="text" />
        </div>
        <div class="form-group input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-envelope"></i></span>
            </div>
            <input id="holderName" class="form-control" placeholder="Account Holder Name" type="text" />
        </div>
        <div class="form-group input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-building"></i></span>
            </div>
            <select class="form-control" id="accounttype">
                <option selected="">Select Account Type</option>
                <option value="company">Company</option>
                <option value="individual">Individual</option>
            </select>
        </div>
        <div class="form-group">
            <button id="verifyAccount" type="submit" class="btn btn-primary btn-block">Verify Account</button>
        </div>
        <asp:HiddenField ID="bankToken" runat="server" />
        <asp:HiddenField ID="bankId" runat="server" />
    </form>
</body>
</html>


