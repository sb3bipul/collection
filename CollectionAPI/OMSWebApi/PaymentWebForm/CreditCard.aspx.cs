﻿using OMSWebApi.Repository;
using Stripe;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace OMSWebApi.PaymentWebForm
{
    public partial class CreditCard : System.Web.UI.Page
    {
        public string stripePublishableKey = WebConfigurationManager.AppSettings["StripePublishableKey"];
        public string clientSecret = "";
        public string CustName = "";
        public string Amount = "";
        public string DueDate = "";
        public string isPaid = "";
        public int PaybleAmount = 0;
        DBConnection dbConnectionObj = null;
        SqlCommand SqlCmd = null;
        DataToJson dtToJson = new DataToJson();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string guid = Request.QueryString["id"];
                byte[] b;
                string decrypted;
                b = Convert.FromBase64String(guid);
                decrypted = System.Text.ASCIIEncoding.ASCII.GetString(b);
                Session["guid"] = decrypted;
                PaybleAmount = getPaymentAmount(decrypted);
                //this.BindControls(guid);
                var option = new PaymentIntentCreateOptions
                {
                    Amount = PaybleAmount,
                    Currency = "USD",
                    Description = "Payment for foods service",
                    Shipping = new ChargeShippingOptions
                    {
                        Name = "Goya Foods Inc.",
                        Address = new AddressOptions
                        {
                            Line1 = "510 Townsend St",
                            PostalCode = "98140",
                            City = "San Francisco",
                            State = "CA",
                            Country = "US",
                        },
                    },

                    PaymentMethodTypes = new List<string>
                    {
                        "card",
                    },
                };
                var service = new PaymentIntentService();
                var paymentIntent = service.Create(option);
                clientSecret = paymentIntent.ClientSecret;
                //service.Get(paymentIntent.Id);

                isPaid = BindControls(decrypted, Convert.ToString(paymentIntent.Id));
            }
            else
            {
                //Console.Write("jdgshjdyu");
            }
        }

        public string BindControls(string guid,string TnxId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            DateTime date = DateTime.Now;
            string Paid = string.Empty;
            dbConnectionObj = new DBConnection();
            SqlCmd = new SqlCommand(ApiConstant.SProc_Get_PayPanelData, dbConnectionObj.ApiConnection);
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@ID", guid);
            SqlCmd.Parameters.AddWithValue("@TnxId", string.IsNullOrEmpty(TnxId) ? DBNull.Value : (object)TnxId);
            da = new SqlDataAdapter(SqlCmd);
            dt = new DataTable();
            dbConnectionObj.ConnectionOpen();
            da.Fill(dt);
            dbConnectionObj.ConnectionClose();
            if (dt.Rows.Count > 0)
            {
                if (Convert.ToBoolean(dt.Rows[0]["IsPaid"]) == true)
                {
                    Paid = "true";
                }
                else
                {
                    Paid = "false";
                }
            }
            CustName = Convert.ToString(dt.Rows[0]["CustomerName"]);
            Amount = Convert.ToString(dt.Rows[0]["Amount"]);
            DueDate = String.Format("{0:ddd, MMM d, yyyy}", date);    // "Sun, Mar 9, 2008"
            return Paid;
        }

        public int getPaymentAmount(string guid)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            int amount = 0;
            dbConnectionObj = new DBConnection();
            SqlCmd = new SqlCommand(ApiConstant.PaybleAmountById, dbConnectionObj.ApiConnection);
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@ID", guid);
            da = new SqlDataAdapter(SqlCmd);
            dt = new DataTable();
            dbConnectionObj.ConnectionOpen();
            da.Fill(dt);
            dbConnectionObj.ConnectionClose();
            if (dt.Rows.Count > 0)
            {
                amount = Convert.ToInt32(dt.Rows[0]["Amount"]);
            }
            return amount;
        }
    }
}