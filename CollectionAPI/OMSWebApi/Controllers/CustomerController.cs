﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Filters;
using OMSWebApi.Models;
using OMSWebApi.Repository;
using System.Data;

namespace OMSWebApi.Controllers
{
    [AllowCrossSiteJson]
    public class CustomerController : ApiController
    {
        //Here is the once-per-class call to initialize the log object 
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        int x = Globals.GlobalInt;
        Int32 compId = Globals.SetGlobalInt(2);
        CustomerRepository custRepository = null;
        String ClientName = ApiConstant.getClientName.ToString();

        /// <summary>
        /// Get Customer Data by BrokerID (Post Method)
        /// </summary>
        /// <param name="objOrder"></param>
        /// <returns></returns>
        //  [Authorize (Roles="Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Customer/CustomersDetailListPost")]
        public HttpResponseMessage CustomersDetailListPost(Order objOrder)
        {
            try
            {
                //switch (ClientName)
                //{
                //    case "Goya":
                //        {
                //            if (objOrder.GoyaCompanyId == null || objOrder.GoyaCompanyId == "")
                //            {
                //                objOrder.GoyaCompanyId = objOrder.BrokerId.Substring(0, 2); ;
                //            }
                //            log.Debug(" Method: CustomersDetailListPost(Order objOrder) [api/Customer/CustomersDetailListPost] -> Token Validation Success");
                //            custRepository = new CustomerRepository();
                //            StringContent sc = new StringContent(custRepository.GetCustomersDetailListDB2(objOrder.BrokerId, objOrder.GoyaCompanyId));
                //            sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                //            HttpResponseMessage data = new HttpResponseMessage();
                //            data.Content = sc;
                //            return data;
                //        }
                //    default:
                //        {
                log.Debug(" Method: CustomersDetailListPost(Order objOrder) [api/Customer/CustomersDetailListPost] -> Token Validation Success");

                custRepository = new CustomerRepository();
                StringContent sc = new StringContent(custRepository.GetCustomersListBySalesmanID(objOrder.BrokerId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;

                //        }
                //}
            }
            catch (Exception ex)
            {
                log.Error(" Method: CustomersDetailListPost(Order objOrder) [api/Customer/CustomersDetailListPost] " + ex);
                throw ex;
            }
            finally
            {
                custRepository = null;
            }
            //  return null;
        }


        /// <summary>
        /// Get Customer Deatil by Customer ID and Company ID
        /// </summary>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        //  [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Customer/CustomersDetailByCustomerIdPost")]
        public HttpResponseMessage CustomersDetailByCustomerIdPost(Order objOrderstring)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                string userId = string.Empty;
                var request = Request;
                var headers = request.Headers;

                custRepository = new CustomerRepository();
                StringContent sc = new StringContent(custRepository.GetCustomersDetailByCustomerId(objOrderstring.CustomerId, objOrderstring.CompanyId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;           
             
            }
            catch (Exception ex)
            {
                log.Error("Method: CustomersDetailByCustomerIdPost(Order objOrderstring) [api/Customer/CustomersDetailByCustomerIdPost]", ex);
            }
            finally
            {
                custRepository = null;
            }
            return null;
        }

        /// <summary>
        /// Validat Customer by Customer ID and Company ID
        /// </summary>
        /// <param name="objOrderstring"></param>
        /// <returns></returns>
        //   [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Customer/ValidateCustomerByID")]
        public HttpResponseMessage ValidateCustomerByID(Order objOrderstring)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                string userId = string.Empty;
                var request = Request;
                var headers = request.Headers;

                // Check if the client request have header with Authorization key
                //if (headers.Contains("Authorization"))
                //{
                //    // Read token received from client's request in header
                //    token = headers.GetValues("Authorization").First();
                //    userId = "013030";//headers.GetValues("UserId").First();
                //}

                // Check if token received from client is valid, if validated then send customer list otherwise unauthorized note.
                //        if (userController.IsValidToken(token, userId))
                //        {
                custRepository = new CustomerRepository();
                StringContent sc = new StringContent(custRepository.ValidateCustomerByID(objOrderstring.CustomerId, objOrderstring.CompanyId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
                //}
                //else
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");
            }
            catch (Exception ex)
            {
                log.Error("-> api/Customer/ValidateCustomerByID ", ex);
            }
            finally
            {
                custRepository = null;
            }
            return null;
        }


        //  [Authorize(Roles = "Broker")]
        //[Authorize(Roles = CustomRoles.BrkRole)]
        //[HttpPost]
        //[Route("api/Customer/CustomersDetailList1")]
        //public HttpResponseMessage CustomersDetailList1(Order objOrder)
        //{
        //    // UserController declaration for Token Varification
        //    UserController userController = null;

        //    try
        //    {
        //        userController = new UserController();
        //        string token = string.Empty;
        //        var request = Request;
        //        var headers = request.Headers;
        //        string userId = string.Empty;

        //        ////Check if the client request have header with Authorization key
        //        //// Check if token received from client is valid, if validated then send customer list otherwise unauthorized note.
        //        ////  if (userController.IsValidToken(token, userId))
        //        ////  {

        //        custRepository = new CustomerRepository();
        //        StringContent sc = new StringContent(custRepository.GetCustomersDetailList1(objOrder.BrokerId));
        //        sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
        //        HttpResponseMessage data = new HttpResponseMessage();
        //        data.Content = sc;
        //        return data;

        //        ////  }
        //        ////else
        //        ////  return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("-> api/Customer/CustomersDetailList1 ", ex);
        //        throw ex;
        //    }
        //    finally
        //    {
        //        custRepository = null;
        //    }
        //    ////   return null;
        //}

        // To get the customer list for master data
        //[Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Customer/CustomersDetailList2")]
        public HttpResponseMessage CustomersDetailList1(Order objOrder)
        {
            try
            {
                custRepository = new CustomerRepository();
                StringContent sc = new StringContent(custRepository.GetCustomersDetailListBySalesmanId(objOrder.BrokerId, objOrder.CompanyId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/Customer/CustomersDetailList2 ", ex);
                throw ex;
            }
            finally
            {
                custRepository = null;
            }
        }

        //[Authorize(Roles = CustomRoles.BrkRole)]
        //[HttpPost]
        //[Route("api/Customer/CustomersDetailList1")]
        //public IEnumerable<CustomerLst1> CustomersDetailList1(Order objOrder)
        //{
        //    try
        //    {
        //        custRepository = new CustomerRepository();
        //        return custRepository.GetCustomersDetailListBySalesmanId(objOrder.BrokerId, objOrder.CompanyId).AsEnumerable();
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("-> api/Customer/CustomersDetailList1 ", ex);
        //        throw ex;
        //    }
        //    finally
        //    {
        //        custRepository = null;
        //    }
        //    ////   return null;
        //}

        /// <summary>
        /// Get Brokers list By Adming Broker ID ending with 01
        /// </summary>
        /// <param name="objOrder"></param>
        /// <returns></returns>
        //   [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        //[Authorize]
        [HttpPost]
        [Route("api/Customer/GetBrokerListByAdminBroker")]
        public HttpResponseMessage GetBrokerListByAdminBroker(Order objOrder)
        {
            // UserController declaration for Token Varification
            UserController userController = null;

            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                //Check if the client request have header with Authorization key


                // Check if token received from client is valid, if validated then send customer list otherwise unauthorized note.
                //  if (userController.IsValidToken(token, userId))
                //  {
                custRepository = new CustomerRepository();
                StringContent sc = new StringContent(custRepository.GetBrokerListByAdminBroker(objOrder.BrokerId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
                //  }
                //else
                //  return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");
            }
            catch (Exception ex)
            {
                log.Error("-> api/Customer/CustomersDetailList1 ", ex);
                throw ex;
            }
            finally
            {
                custRepository = null;
            }
            // return null;
        }

        /// <summary>
        /// Get Customer Detail by Customer ID
        /// </summary>
        /// <param name="objOrder"></param>
        /// <returns></returns>
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Customer/GetCustomerDetailByCustomerID")]
        public HttpResponseMessage GetCustomerDetailByCustomerID(Order objOrder)
        {
            // UserController declaration for Token Varification
            UserController userController = null;

            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                //Check if the client request have header with Authorization key


                // Check if token received from client is valid, if validated then send customer list otherwise unauthorized note.
                //  if (userController.IsValidToken(token, userId))
                //  {
                custRepository = new CustomerRepository();
                StringContent sc = new StringContent(custRepository.GetCustomerDetailByCustomerID(objOrder.GoyaCompanyId, objOrder.CustomerId, objOrder.BrokerId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
                //  }
                //else
                //  return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");
            }
            catch (Exception ex)
            {
                log.Error("-> api/Customer/GetCustomerDetailByCustomerID ", ex);
                throw ex;
            }
            finally
            {
                custRepository = null;
            }
            // return null;
        }

        /// <summary>
        /// Get Broker Detail by Broker ID
        /// </summary>
        /// <param name="objOrder"></param>
        /// <returns></returns>
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Customer/GetBrokerDetailByBrokerID")]
        public HttpResponseMessage GetBrokerDetailByBrokerID(Order objOrder)
        {
            // UserController declaration for Token Varification
            UserController userController = null;

            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                //Check if the client request have header with Authorization key


                // Check if token received from client is valid, if validated then send customer list otherwise unauthorized note.
                //  if (userController.IsValidToken(token, userId))
                //  {
                custRepository = new CustomerRepository();
                StringContent sc = new StringContent(custRepository.GetBrokerDetailByBrokerID(objOrder.GoyaCompanyId, objOrder.BrokerId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
                //  }
                //else
                //  return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");
            }
            catch (Exception ex)
            {
                log.Error("-> api/Customer/GetBrokerDetailByBrokerID ", ex);
                throw ex;
            }
            finally
            {
                custRepository = null;
            }
            // return null;
        }


        // Address Change Request 
        //   [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Customer/AddressChangeRequest")]
        public HttpResponseMessage AddressChangeRequest(CustomerDetail objCustomer)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                string userId = string.Empty;
                var request = Request;
                var headers = request.Headers;
 
                custRepository = new CustomerRepository();
                StringContent sc = new StringContent(custRepository.AddressChangeRequest(objCustomer.CustomerId, objCustomer.UserId, objCustomer.StreetAddress1, objCustomer.StreetAddress2, objCustomer.City, objCustomer.State, objCustomer.Zip, objCustomer.Phone));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
             
            }
            catch (Exception ex)
            {
                log.Error("-> api/Customer/AddressChangeRequest ", ex);
            }
            finally
            {
                custRepository = null;
            }
            return null;
        }

        /// <summary>
        ///Get Price List file detail by Customer Id
        /// </summary>
        /// <param name="objCustomer"></param>
        /// <returns></returns>
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Customer/GetPriceListFileByCustomerId")]
        public HttpResponseMessage GetPriceListFileByCustomerId(CustomerDetail objCustomer)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                string userId = string.Empty;
                var request = Request;
                var headers = request.Headers;

                custRepository = new CustomerRepository();
                StringContent sc = new StringContent(custRepository.getPriceListFileByCustomerId(objCustomer));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/Customer/GetPriceListFileByCustomerId ", ex);
            }
            finally
            {
                custRepository = null;
            }
            return null;
        }

        /// <summary>
        ///Update Customer address by Customer Id
        /// </summary>
        /// <param name="objCustomer"></param>
        /// <returns></returns>
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Customer/UpdateCustomerAddressByCustomerId")]
        public HttpResponseMessage UpdateCustomerAddressByCustomerId(CustomerDetail objCustomer)
        {
            try
            {
                custRepository = new CustomerRepository();
                StringContent sc = new StringContent(custRepository.updateCustomerAddressDataByCustomerId(objCustomer));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/Customer/UpdateCustomerAddressByCustomerId ", ex);
            }
            finally
            {
                custRepository = null;
            }
            return null;
        }

        //[HttpGet]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Customer/GetCustomerProfile")]
        public HttpResponseMessage GetCustomerProfile(CustomerLst1 cstDet)
        {
            CustomerRepository repositoryObj = new CustomerRepository();
            DataTable dtFullDetails = new DataTable();
            dtFullDetails = repositoryObj.GetCustomerFullDetails(cstDet.CompanyID, cstDet.CustomerId); //CustomerFullDetail
            DataSet dsData = new DataSet();
            dsData.Tables.Add(dtFullDetails);

            DataTable dtStoreHr = new DataTable();
            dtStoreHr = repositoryObj.GetCustomerStoreHours(cstDet.CompanyID, cstDet.CustomerId); //Customer Store Hours
            dsData.Tables.Add(dtStoreHr);

            DataTable dtReceivingHr = new DataTable();
            dtReceivingHr = repositoryObj.GetCustomerReceivingHours(cstDet.CompanyID, cstDet.CustomerId); //Customer Receiving Hours
            dsData.Tables.Add(dtReceivingHr);

            DataTable dtPayment = new DataTable();
            dtPayment = repositoryObj.GetCustomerPayment(Convert.ToString(dtFullDetails.Rows[0]["CustomerBalance"])); //Customer Payment
            dsData.Tables.Add(dtPayment);

            DataTable dtLocation = new DataTable();
            dtLocation = repositoryObj.GetCustomerLocation(Convert.ToString(dtFullDetails.Rows[0]["Latitude"]), Convert.ToString(dtFullDetails.Rows[0]["Longitude"]), Convert.ToString(dtFullDetails.Rows[0]["Location"]), Convert.ToString(dtFullDetails.Rows[0]["BillingAddress"]), Convert.ToString(dtFullDetails.Rows[0]["ShippingAddress"])); //Customer Location
            dsData.Tables.Add(dtLocation);
            return Request.CreateResponse(HttpStatusCode.OK, dsData);
        }

        public class AddCustomHeaderActionFilterAttribute : ActionFilterAttribute
        {
            public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
            {
                base.OnActionExecuted(actionExecutedContext);
             
            }
        }

        public class AllowCrossSiteJsonAttribute : ActionFilterAttribute
        {
            public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
            {
                base.OnActionExecuted(actionExecutedContext);
            }
        }


        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Customer/UpdateCustomerAddress")]
        public HttpResponseMessage UpdateCustomerAddress(CustomerDetail objCustomer)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                string userId = string.Empty;
                var request = Request;
                var headers = request.Headers;

                custRepository = new CustomerRepository();
                StringContent sc = new StringContent(custRepository.updateCustomerAddressData(objCustomer));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/Customer/UpdateCustomerAddress ", ex);
            }
            finally
            {
                custRepository = null;
            }
            return null;
        }

    }


    public static class Globals
    {
        // parameterless constructor required for static class
        static Globals() { GlobalInt = 0; } // default value

        // public get, and private set for strict access control
        public static int GlobalInt { get; private set; }

        // GlobalInt can be changed only via this method
        public static int SetGlobalInt(int newInt)
        {
            GlobalInt = newInt;
            return GlobalInt;
        }
    }

}
