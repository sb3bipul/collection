﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using OMSWebApi.Models;
using OMSWebApi.Repository;

namespace OMSWebApi.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ApplicationAccessMapController : ApiController
    {

        ApplicationAccessMapRepository repositoryObj = null;

        [HttpGet]
        [Route("api/ApplicationAccessMap/GetAppAccessMapViewList/{appId}")]
        public ApplicationAccessMapping GetAppAccessMapViewList(int appId)
        {
            ApplicationAccessMapping listObj = new ApplicationAccessMapping();
            repositoryObj = new ApplicationAccessMapRepository();
            listObj = (repositoryObj.GetAppAccessMapListFromDb(appId));
            return listObj;
        }


        [HttpPost]
        [Route("api/ApplicationAccessMap/UpdateMenuById")]
        public bool UpdateRoleById(ApplicationAccessMapping pObj)
        {
            repositoryObj = new ApplicationAccessMapRepository();
            return repositoryObj.UpdateMenuToDb(pObj);
        }

        [HttpPost]
        [Route("api/ApplicationAccessMap/AddNewMenu")]
        public bool AddNewMenu(ApplicationAccessMapping pObj)
        {
            repositoryObj = new ApplicationAccessMapRepository();
            return repositoryObj.AddNewMenuToDb(pObj);
        }

        [HttpPost]
        [Route("api/ApplicationAccessMap/DeleteMenu")]
        public bool DeleteApplication(ApplicationAccessMapping appObj)
        {
            repositoryObj = new ApplicationAccessMapRepository();
            return repositoryObj.DeleteMenuFromDb(appObj);
        }
    }
}
