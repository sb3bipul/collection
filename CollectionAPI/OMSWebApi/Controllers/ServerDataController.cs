﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Filters;
using OMSWebApi.Models;
using OMSWebApi.Repository;
using System.Data;

namespace OMSWebApi.Controllers
{
    public class ServerDataController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        ServerDataRepository srvrRepository = null;
        int x = Globals.GlobalInt;
        Int32 compId = Globals.SetGlobalInt(0);
        String ClientName = ApiConstant.getClientName.ToString();

        [HttpPost]
        [Route("api/ServerData/getCategoryMaster")]
        public List<subCategory> getCategoryMaster(subCategory objMstr)
        {
            try
            {
                ServerDataRepository srvrRepository = new ServerDataRepository();
                List<subCategory> mstrLst = new List<subCategory>();
                //DataTable dtFullDetails = new DataTable();
                mstrLst = srvrRepository.getCategoryMasterList(objMstr.CompanyId, objMstr.LanguageId);
                //DataSet dsData = new DataSet()mstrLst
                //dsData.Tables.Add(dtFullDetails);
                //return Request.CreateResponse(HttpStatusCode.OK, dsData);
                return mstrLst;
            }
            catch (Exception ex)
            {
                log.Error("-> api/ServerData/getCategoryMaster", ex);
            }
            finally
            {
                srvrRepository = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/ServerData/getSubCategoryMaster")]
        public List<subCategory> getSubCategoryMaster(subCategory objMstr)
        {
            try
            {
                ServerDataRepository srvrRepository = new ServerDataRepository();
                List<subCategory> mstrLst = new List<subCategory>();
                mstrLst = srvrRepository.getSubCategoryMasterList(objMstr.CompanyId, objMstr.LanguageId);
                return mstrLst;
            }
            catch (Exception ex)
            {
                log.Error("-> api/ServerData/getSubCategoryMaster", ex);
            }
            finally
            {
                srvrRepository = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/ServerData/getProductMaster")]
        public List<products> getProductMaster(products objProd)
        {
            try
            {
                ServerDataRepository srvrRepository = new ServerDataRepository();
                List<products> prodLst = new List<products>();
                prodLst = srvrRepository.getItemMasterList(objProd.CompanyId, objProd.LanguageId, objProd.BillingId);
                return prodLst;
            }
            catch (Exception ex)
            {
                log.Error("-> api/ServerData/getProductMaster", ex);
            }
            finally
            {
                srvrRepository = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/ServerData/getCustomer")]
        public List<CustomerLst1> getCustomer(CustomerLst1 objCust)
        {
            try
            {
                ServerDataRepository srvrRepository = new ServerDataRepository();
                List<CustomerLst1> prodLst = new List<CustomerLst1>();
                prodLst = srvrRepository.getCustomerList(objCust.CompanyID, objCust.BrokerID);
                return prodLst;
            }
            catch (Exception ex)
            {
                log.Error("-> api/ServerData/getCustomer", ex);
            }
            finally
            {
                srvrRepository = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/ServerData/getPriceMaster")]
        public List<prices> getPriceMaster(prices objPrice)
        {
            try
            {
                ServerDataRepository srvrRepository = new ServerDataRepository();
                List<prices> priceLst = new List<prices>();
                priceLst = srvrRepository.getPriceMasterData(objPrice.CompanyID, objPrice.SalesmanId);
                return priceLst;
            }
            catch (Exception ex)
            {
                log.Error("-> api/ServerData/getPriceMaster", ex);
            }
            finally
            {
                srvrRepository = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/ServerData/getWarehouseMaster")]
        public List<warehouses> getWarehouseMaster(warehouses objWhse)
        {
            try
            { 
                ServerDataRepository srvrRepository = new ServerDataRepository();
                List<warehouses> whseLst = new List<warehouses>();
                whseLst = srvrRepository.getWarehouseMasterList(objWhse.CompanyID);
                return whseLst;
            }
            catch (Exception ex)
            {
                log.Error("-> api/ServerData/getWarehouseMaster", ex);
            }
            finally
            {
                srvrRepository = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/ServerData/getStockMaster")]
        public List<stocks> getStockMaster(stocks objStk)
        {
            try
            {
                ServerDataRepository srvrRepository = new ServerDataRepository();
                List<stocks> stkLst = new List<stocks>();
                stkLst = srvrRepository.getStockMasterList(objStk.CompanyID);
                return stkLst;
            }
            catch (Exception ex)
            {
                log.Error("-> api/ServerData/getStockMaster", ex);
            }
            finally
            {
                srvrRepository = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/ServerData/getBrokerMaster")]
        public List<brokers> getBrokerMaster(brokers objBrkr)
        {
            try
            {
                ServerDataRepository srvrRepository = new ServerDataRepository();
                List<brokers> brkrLst = new List<brokers>();
                brkrLst = srvrRepository.getBrokerMasterList(objBrkr.COMPANY);
                return brkrLst;
            }
            catch (Exception ex)
            {
                log.Error("-> api/ServerData/getBrokerMaster", ex);
            }
            finally
            {
                srvrRepository = null;
            }
            return null;
        }
    }
}