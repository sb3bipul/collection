﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using OMSWebApi.Models;
using OMSWebApi.Repository;

namespace OMSWebApi.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ApplicationRoleMapController : ApiController
    {
        ApplicationRoleMapRepository repositoryObj = null;

        [HttpGet]
        [Route("api/ApplicationRoleMapController/GetAppRoleMapViewList/{AppId}")]
        public RoleApplicationAccessMapping GetAppRoleMapViewList(int AppId)
        {
            RoleApplicationAccessMapping listObj = new RoleApplicationAccessMapping();
            repositoryObj = new ApplicationRoleMapRepository();
            listObj = (repositoryObj.GetAppRoleMapListFromDb(AppId));
            return listObj;
        }

        [HttpGet]
        [Route("api/ApplicationRoleMapController/GetRoleDropDownListForAppId/{AppId}")]
        public RoleApplicationAccessMapping GetRoleDropDownListForAppId(int AppId)
        {
            RoleApplicationAccessMapping listObj = new RoleApplicationAccessMapping();
            repositoryObj = new ApplicationRoleMapRepository();
            listObj = (repositoryObj.GetRoleDropDownListFromDb(AppId));
            return listObj;
        }

        [HttpPost]
        [Route("api/ApplicationRoleMapController/UpdateAppRoleList")]
        public bool UpdateAppRoleList(List<RoleApplicationAccessMapping> pObj)
        {
            repositoryObj = new ApplicationRoleMapRepository();
            return repositoryObj.UpdateAppRoleMapToDb(pObj);
        }
    }
}
