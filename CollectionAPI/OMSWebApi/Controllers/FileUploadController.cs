﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using System.Threading.Tasks;
using System.Web;
//using OMSWebApi.UploadRepo;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OMSWebApi.Models;
using OMSWebApi.Repository;
using System.Net.Http.Headers;
using System.Data;

namespace OMSWebApi.Controllers
{
    public class FileUploadController : ApiController
    {
        [Mime]
        public async Task<FileUploadDetails> Post()
        {
            // file path
            var fileuploadPath = HttpContext.Current.Server.MapPath("~/UploadedFiles");
            // 
            var multiFormDataStreamProvider = new MultiFileUploadProvider(fileuploadPath);

            // Read the MIME multipart asynchronously 
            await Request.Content.ReadAsMultipartAsync(multiFormDataStreamProvider);

            String uploadingFileName = multiFormDataStreamProvider
                .FileData.Select(x => x.LocalFileName).FirstOrDefault();

            //var provider = new MultipartFormDataStreamProvider(fileuploadPath);
            //foreach (MultipartFileData forms in multiFormDataStreamProvider.FormData.)
            //{
            //    string str1 = forms.Headers.ContentDisposition.Name;//get FileName
            //    string str2 = forms.LocalFileName;//get File Path
            //}

            String uploadingFormData = multiFormDataStreamProvider.FormData.GetValues(0).FirstOrDefault();//.FirstOrDefault();
            uploadingFormData = uploadingFormData.Replace("\\","");

            //dynamic stuff = JsonConvert.DeserializeObject("{ 'Name': 'Jon Smith', 'Address': { 'City': 'New York', 'State': 'NY' }, 'Age': 42 }");
            //string name = stuff.Name;            //string address = stuff.Address.City;
            dynamic frmData = JsonConvert.DeserializeObject(uploadingFormData);
            String CustId, CompId, SecImgName, OutsideImgName, InImgName;
            //int SecImgSrl, OutsideImgSrl, InsideImgSrl, i=0;
            //String[] SectionSrl = new String[3];
            //String[] SectionNm = new String[3];
            //String[] OutsideSrl = new String[3];
            //String[] OutsideNm = new String[3];
            //String[] InsideSrl = new String[3];
            //String[] InsideNm = new String[3];//String[] InsideNm = new String[] { };
            //List<string> lst = new List<string>(InsideNm);
            CustId = frmData.CustomerID;
            CompId = frmData.CompanyID;
            //SecImgSrl = frmData.sectionDet[0].SectionSrl;
            //foreach (var sec in frmData.sectionDet)
            //{
            //    SectionSrl[i] = sec.SectionSrl;
            //    SectionNm[i] = sec.SectionImgName;
            //    i++;
            //}
            //i = 0;
            //foreach( var ot in frmData.outsideDet)
            //{
            //    OutsideSrl[i] = ot.OutsideSrl;
            //    OutsideNm[i] = ot.OutsideImgName;
            //    i++;
            //}
            //i = 0;
            //foreach (var ins in frmData.insideDet)
            //{
            //    InsideSrl[i] = ins.InsideSrl;
            //    InsideNm[i] = ins.InsideImgName;
            //    i++;
            //}
            CustProfileRepository custProfile = null;
            custProfile = new CustProfileRepository();
            DataTable dtl = new DataTable();
            dtl = custProfile.SaveImageListAll(CustId, CompId, frmData);//SectionSrl, SectionNm, OutsideSrl, OutsideNm, InsideSrl, InsideNm);

            //ImageViewModel imageVwModel = new ImageViewModel();            //var jData = JsonObject.Parse(uploadingFormData); //var formdata = new FormData();
            //Newtonsoft.Json.Linq.JArray a = Newtonsoft.Json.Linq.JArray.Parse(uploadingFormData);
            //{            //    foreach (Newtonsoft.Json.Linq.JObject o in a.Children<Newtonsoft.Json.Linq.JObject>())
            //    {            //        foreach (JProperty p in o.Properties())
            //        {            //            string name = p.Name;            //            imageVwModel.sectionImgs.sec
            //            list_skupin.Add(name);            //            string value = (string)p.Value;            //            Console.WriteLine(name);            //        }            //    }            //}

            // Create response
            return new FileUploadDetails
            {

                FilePath = uploadingFileName,

                FileName = "Message: " + Convert.ToString(dtl.Rows[0]["Msg"]) + ", CustomerId: " + Convert.ToString(dtl.Rows[0]["CustomerId"]) + "",

                FileLength = new FileInfo(uploadingFileName).Length,

                FileCreatedTime = DateTime.Now.ToLongDateString()
            };
        }
    }
}
