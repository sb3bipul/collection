﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;
using OMSWebApi.Models;
using OMSWebApi.Repository;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Data;

namespace OMSWebApi.Controllers
{
    public class OrderController : ApiController
    {
        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        String ClientName = ApiConstant.getClientName.ToString();
        Int32 compId = Globals.SetGlobalInt(2);
        CreateOrderRepository orderRepository = null;
        /// <summary>
        /// Get All Promo Codes 
        /// </summary>
        /// <param name="brokerId"></param>
        /// <returns></returns>
       ///  [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Order/getPromoCodePost")]
        public HttpResponseMessage getPromoCodePost()
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                switch (ClientName)
                {
                    case "Goya":
                        {
                            userController = new UserController();
                            string token = string.Empty;
                            var request = Request;
                            var headers = request.Headers;
                            string userId = string.Empty;

                            //// Check if the client request have header with Authorization key
                            //if (headers.Contains("Authorization"))
                            //{
                            //    // Read token received from client's request in header
                            //    token = headers.GetValues("Authorization").First();
                            //    userId = "013030";//headers.GetValues("UserId").First();
                            //}

                            //// Check if token received from client is valid, if validated then send customer list otherwise unauthorized note.
                            //if (userController.IsValidToken(token, userId))
                            //{
                            orderRepository = new CreateOrderRepository();
                            StringContent sc = new StringContent(orderRepository.GetPromoCode());
                            sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                            HttpResponseMessage data = new HttpResponseMessage();
                            data.Content = sc;

                            return data;
                            //}
                            //else
                            //    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");
                        }
                    default:
                        {
                            orderRepository = new CreateOrderRepository();
                            StringContent sc = new StringContent(orderRepository.GetPromoCodeList());
                            sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                            HttpResponseMessage data = new HttpResponseMessage();
                            data.Content = sc;

                            return data;
                        }
                }
            }
            catch (Exception ex)
            {
                log.Error("-> api/Order/getPromoCodePost", ex);
            }
            finally
            {
                orderRepository = null;
            }
            return null;
        }

        /// <summary>
        /// Get all Departments
        /// </summary>
        /// <returns></returns>
     //   [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Order/getDepartmentsPost")]
        public HttpResponseMessage getDepartmentsPost()
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                //// Check if the client request have header with Authorization key
                //if (headers.Contains("Authorization"))
                //{
                //    // Read token received from client's request in header
                //    token = headers.GetValues("Authorization").First();
                //    userId = "013030";//headers.GetValues("UserId").First();
                //}

                //// Check if token received from client is valid, if validated then send customer list otherwise unauthorized note.
                //if (userController.IsValidToken(token, userId))
                //{
                    orderRepository = new CreateOrderRepository();
                    StringContent sc = new StringContent(orderRepository.GetDepartments());
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;

                    return data;
                //}
                //else
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");

            }
            catch (Exception ex)
            {
                log.Error("-> api/Order/getDepartmentsPost", ex);
            }
            finally
            {
                orderRepository = null;
            }
            return null;
        }

        /// <summary>
        /// Load template
        /// </summary>
        /// <returns></returns>
    //    [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Order/loadTemplate")]
        public HttpResponseMessage loadTemplate(Order objOrderTemplate)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                    userController = new UserController();
                    string token = string.Empty;
                    var request = Request;
                    var headers = request.Headers;
                    string userId = string.Empty;

                    //// Check if the client request have header with Authorization key
                    //if (headers.Contains("Authorization"))
                    //{
                    //    // Read token received from client's request in header
                    //    token = headers.GetValues("Authorization").First();
                    //    userId = "013030";//headers.GetValues("UserId").First();
                    //}

                    //// Check if token received from client is valid, if validated then send customer list otherwise unauthorized note.
                    //if (userController.IsValidToken(token, userId))
                    //{
                    orderRepository = new CreateOrderRepository();
                    StringContent sc = new StringContent(orderRepository.getTemplatesByBrokerId(objOrderTemplate.CustomerId, objOrderTemplate.CompanyId, objOrderTemplate.BrokerId, objOrderTemplate.isPickUp));
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;

                    return data;
                    //}
                    //else
                    //    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");
                        
            }
            catch (Exception ex)
            {
                log.Error("-> api/Order/loadTemplate", ex);
            }
            finally
            {
                orderRepository = null;
            }
            return null;
        }

        /// <summary>
        /// Get Order Item detail, Get all the item from Order
        /// </summary>
        /// <param name="objOrderTemplate"></param>
        /// <returns></returns>
    //    [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Order/getOrderItemList")]
        public HttpResponseMessage getOrderItemList(Order objOrderTemplate)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                    orderRepository = new CreateOrderRepository();
                    StringContent sc = new StringContent(orderRepository.getOrderItemList(objOrderTemplate.CompanyId, objOrderTemplate.Dept, objOrderTemplate.ItemCode, objOrderTemplate.CaseQuantity, 
                              objOrderTemplate.UnitQuantity, objOrderTemplate.CatalogId, objOrderTemplate.CustomerId, objOrderTemplate.BrokerId, 
                                objOrderTemplate.LanguageId, objOrderTemplate.WarehouseId, objOrderTemplate.isPickUp, objOrderTemplate.Comments, 
                               objOrderTemplate.Action,objOrderTemplate.EOR, objOrderTemplate.Result, objOrderTemplate.DeliveryDate,
                                objOrderTemplate.ClientPONumber, objOrderTemplate.PromoCode, objOrderTemplate.Day, objOrderTemplate.NotDay,
                                objOrderTemplate.AmountCollected,objOrderTemplate.GoyaCompanyId,objOrderTemplate.OrderMode)); 
                                           
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;

                    return data;
              
            }
            catch (Exception ex)
            {
                log.Error("-> api/Order/getOrderItemList", ex);
            }
            finally
            {
                orderRepository = null;
            }
            return null;
        }

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Order/addOrderItem")]
        public HttpResponseMessage addOrderItem(Order objOrderTmpBasket)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                DataTable dt = null;
                DataToJson dtToJson = null;

                string userId = string.Empty;
                orderRepository = new CreateOrderRepository();

                dt = orderRepository.getDtl_ItemQOH(Convert.ToString(objOrderTmpBasket.CompanyId), objOrderTmpBasket.ItemCode, objOrderTmpBasket.CustomerId, objOrderTmpBasket.CaseQuantity);

                if (dt.Rows.Count > 0)
                {
                    if (Convert.ToString(dt.Rows[0]["ItemStatus"]) == "Ok")
                    {
                        StringContent sc = new StringContent(orderRepository.addOrderItem(objOrderTmpBasket.CompanyId, objOrderTmpBasket.ItemCode, objOrderTmpBasket.CaseQuantity,
                                   objOrderTmpBasket.CustomerId, objOrderTmpBasket.BrokerId, objOrderTmpBasket.DeliveryDate, objOrderTmpBasket.PromoCode,
                                    objOrderTmpBasket.AmountCollected, objOrderTmpBasket.EOR, objOrderTmpBasket.OrderMode));

                        sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage data = new HttpResponseMessage();
                        data.Content = sc;

                        return data;
                    }
                    else
                    {
                        dt = new DataTable();
                        dt.Columns.Add("SuccessMessage", typeof(string));
                        dt.Columns.Add("Message", typeof(string));
                        dt.Rows.Add("Item Code Is Not Available, Please Enter Valid Item Code.", "");
                        String jsonString = null;
                        dtToJson = new DataToJson();
                        // Convert Datatable to JSON string
                        //jsonString = JObject.Parse(dtToJson.convertDataTableToJson(dt, "getOrderItemList", false));
                        jsonString = dtToJson.convertDataTableToJson(dt, "getOrderItemList", false);
                        StringContent sc = new StringContent(jsonString);
                        sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage data = new HttpResponseMessage();
                        data.Content = sc;

                        return data;
                    }
                }

            }
            catch (Exception ex)
            {
                log.Error("-> api/Order/addOrderItem", ex);
            }
            finally
            {
                orderRepository = null;
            }
            return null;
        }

    //    [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Order/submitOrder")]
        public HttpResponseMessage submitOrder(Order objOrder)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                    orderRepository = new CreateOrderRepository();
                    StringContent sc = new StringContent(orderRepository.submitOrder(objOrder.CustomerId,objOrder.CompanyId,objOrder.BrokerId,
                        objOrder.CatalogId,objOrder.Day,objOrder.NotDay,objOrder.Comments,objOrder.isPickUp,objOrder.EOR,objOrder.AmountCollected,
                        objOrder.isActive,objOrder.totalCaseQuantity,objOrder.totalAmount,objOrder.DeliveryDate,objOrder.ClientPONumber,
                        objOrder.productType,objOrder.templateName,objOrder.totalUnitQuantity,objOrder.ClientPONumber,objOrder.Latitude,objOrder.Longitude,objOrder.Location));
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;
                    
                    return data;
             
            }
            catch (Exception ex)
            {
                log.Error("-> api/Order/submitOrder", ex);
            }
            finally
            {
                orderRepository = null;
            }
            return null;
        }

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Order/submitQuickOrder")]
        public HttpResponseMessage submitQuickOrder(Order objOrder)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                orderRepository = new CreateOrderRepository();
                StringContent sc = new StringContent(orderRepository.submitQuickOrders(objOrder.CustomerId, objOrder.CompanyId, objOrder.BrokerId,
                    objOrder.EOR, objOrder.AmountCollected, objOrder.totalCaseQuantity, objOrder.totalAmount, objOrder.DeliveryDate, objOrder.ClientPONumber,
                    objOrder.Latitude, objOrder.Longitude, objOrder.Location));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/Order/submitQuickOrder", ex);
            }
            finally
            {
                orderRepository = null;
            }
            return null;
        }

    //    [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Order/deleteOrderItem")]
        public HttpResponseMessage deleteOrderItem(Order objOrderTemplate)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

 
            orderRepository = new CreateOrderRepository();
            StringContent sc = new StringContent(orderRepository.deleteOrderItem(objOrderTemplate.ItemCode, objOrderTemplate.CustomerId,objOrderTemplate.EOR));
            sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage data = new HttpResponseMessage();
            data.Content = sc;

                    return data;
            
            }
            catch (Exception ex)
            {
                log.Error("-> api/Order/deleteOrderItem", ex);
            }
            finally
            {
                orderRepository = null;
            }
            return null;
        }


        /// <summary>
        /// Delete All Items from Order
        /// </summary>
        /// <param name="objOrderTemplate"></param>
        /// <returns></returns>
     //   [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Order/deleteAllOrderItems")]
        public HttpResponseMessage DeleteAllOrderItems(Order objOrderTemplate)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                //// Check if the client request have header with Authorization key
                //if (headers.Contains("Authorization"))
                //{
                //    // Read token received from client's request in header
                //    token = headers.GetValues("Authorization").First();
                //    userId = "013030";//headers.GetValues("UserId").First();
                //}

                //// Check if token received from client is valid, if validated then send customer list otherwise unauthorized note.
                //if (userController.IsValidToken(token, userId))
                //{
                    orderRepository = new CreateOrderRepository();
                    StringContent sc = new StringContent(orderRepository.deleteAllOrderItems(objOrderTemplate.CustomerId, objOrderTemplate.EOR));
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;

                    return data;
                //}
                //else
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");

            }
            catch (Exception ex)
            {
                log.Error("-> api/Order/deleteAllOrderItems", ex);
            }
            finally
            {
                orderRepository = null;
            }
            return null;
        }

   //     [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Order/updateItemCaseQuantity")]
        public HttpResponseMessage UpdateItemCaseQuantity(Order objOrderTemplate)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                // Check if the client request have header with Authorization key
                if (headers.Contains("Authorization"))
                {
                    // Read token received from client's request in header
                    token = headers.GetValues("Authorization").First();
                    //userId = "013030";//headers.GetValues("UserId").First();
                }

                // Check if token received from client is valid, if validated then send customer list otherwise unauthorized note.
                if (userController.IsValidToken(token, userId))
                {
                    orderRepository = new CreateOrderRepository();
                    StringContent sc = new StringContent(orderRepository.updateItemCaseQuantity(objOrderTemplate.CaseQuantity, objOrderTemplate.ItemCode,objOrderTemplate.CustomerId,objOrderTemplate.EOR));
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;

                    return data;
                }
                else
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");

            }
            catch (Exception ex)
            {
                log.Error("-> api/Order/updateItemCaseQuantity", ex);
            }
            finally
            {
                orderRepository = null;
            }
            return null;
        }

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Order/updatePOPaymentTerm")]
        public HttpResponseMessage updatePOPaymentTerm(Order objOrderTemplate)
        {
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                orderRepository = new CreateOrderRepository();
                StringContent sc = new StringContent(orderRepository.updatePaymentTermCustomerPO(objOrderTemplate.PaymentTerm, objOrderTemplate.AmountCollected, objOrderTemplate.ClientPONumber, objOrderTemplate.EOR, objOrderTemplate.CustomerId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/Order/updatePOPaymentTerm", ex);
            }
            finally
            {
                orderRepository = null;
            }
            return null;
        }

    //    [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Order/updateItemUnitQuantity")]
        public HttpResponseMessage UpdateItemUnitQuantity(Order objOrderTemplate)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                //// Check if the client request have header with Authorization key
                //if (headers.Contains("Authorization"))
                //{
                //    // Read token received from client's request in header
                //    token = headers.GetValues("Authorization").First();
                //    userId = "013030";//headers.GetValues("UserId").First();
                //}

                //// Check if token received from client is valid, if validated then send customer list otherwise unauthorized note.
                //if (userController.IsValidToken(token, userId))
                //{
                    orderRepository = new CreateOrderRepository();
                    StringContent sc = new StringContent(orderRepository.updateItemUnitQuantity(objOrderTemplate.UnitQuantity, objOrderTemplate.ItemCode, objOrderTemplate.CustomerId, objOrderTemplate.EOR));
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;

                    return data;
                //}
                //else
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");

            }
            catch (Exception ex)
            {
                log.Error("-> api/Order/updateItemUnitQuantity", ex);
            }
            finally
            {
                orderRepository = null;
            }
            return null;
        }


    //    [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Order/createOrderFromTemplate")]
        public HttpResponseMessage createOrderFromTemplate(Order objOrderTemplate)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                //// Check if the client request have header with Authorization key
                //if (headers.Contains("Authorization"))
                //{
                //    // Read token received from client's request in header
                //    token = headers.GetValues("Authorization").First();
                //    userId = "013030";//headers.GetValues("UserId").First();
                //}

                //// Check if token received from client is valid, if validated then send customer list otherwise unauthorized note.
                //if (userController.IsValidToken(token, userId))
                //{
                    orderRepository = new CreateOrderRepository();
                    StringContent sc = new StringContent(orderRepository.createOrderFromTemplate(objOrderTemplate.BasketId, objOrderTemplate.CompanyId, objOrderTemplate.CustomerId, objOrderTemplate.BrokerId, objOrderTemplate.LanguageId,objOrderTemplate.CurrentEOR,objOrderTemplate.EOR));
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;

                    return data;
                //}
                //else
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");

            }
            catch (Exception ex)
            {
                log.Error("-> api/Order/createOrderFromTemplate", ex);
            }
            finally
            {
                orderRepository = null;
            }
            return null;
        }

        /// <summary>
        /// Get Order Header Details
        /// </summary>
        /// <param name="objOrderTemplate"></param>
        /// <returns></returns>
    //    [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Order/getOrderHeaderDetails")]
        public HttpResponseMessage getOrderHeaderDetails(Order objOrderTemplate)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                //// Check if the client request have header with Authorization key
                //if (headers.Contains("Authorization"))
                //{
                //    // Read token received from client's request in header
                //    token = headers.GetValues("Authorization").First();
                //    userId = "013030";//headers.GetValues("UserId").First();
                //}

                //// Check if token received from client is valid, if validated then send customer list otherwise unauthorized note.
                //if (userController.IsValidToken(token, userId))
                //{
                    orderRepository = new CreateOrderRepository();
                    StringContent sc = new StringContent(orderRepository.getOrderHeaderDetails(objOrderTemplate.EOR, objOrderTemplate.CustomerId));
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;

                    return data;
                //}
                //else
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");

            }
            catch (Exception ex)
            {
                log.Error("-> api/Order/getOrderHeaderDetails", ex);
            }
            finally
            {
                orderRepository = null;
            }
            return null;
        }

        /// <summary>
        /// Get Pending Order Item Details
        /// </summary>
        /// <param name="objOrderTemplate"></param>
        /// <returns></returns>
        //    [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Order/getPendingOrderItems")]
        public HttpResponseMessage getPendingOrderItems(Order objOrderTemplate)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;
                DataTable dt = null;
                DataToJson dtToJson = null;
                orderRepository = new CreateOrderRepository();
                //switch (ClientName)
                //{
                //    case "Goya":
                //        {
                            dt = orderRepository.checkPendingOrderItem(objOrderTemplate.BrokerId, objOrderTemplate.CustomerId, objOrderTemplate.CompanyId);
                            if (dt.Rows.Count > 0)
                            {

                                StringContent sc = new StringContent(orderRepository.getPendingOrderItemDetails(objOrderTemplate.BrokerId, objOrderTemplate.CustomerId, objOrderTemplate.CompanyId));
                                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                                HttpResponseMessage data = new HttpResponseMessage();
                                data.Content = sc;

                                return data;
                            }
                            else
                            {
                                dt = new DataTable();
                                dt.Columns.Add("SuccessMessage", typeof(string));
                                dt.Columns.Add("Message", typeof(string));
                                dt.Rows.Add("Add To Cart Item Does Not Exists", "");
                                String jsonString = null;
                                dtToJson = new DataToJson();
                                // Convert Datatable to JSON string
                                //jsonString = JObject.Parse(dtToJson.convertDataTableToJson(dt, "getOrderItemList", false));
                                jsonString = dtToJson.convertDataTableToJson(dt, "getOrderItemList", false);
                                StringContent sc = new StringContent(jsonString);
                                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                                HttpResponseMessage data = new HttpResponseMessage();
                                data.Content = sc;

                                return data;
                            }
                //        }
                //    default:
                //        {
                //            dt = orderRepository.checkPendingOrderItemLst(objOrderTemplate.BrokerId, objOrderTemplate.CustomerId, objOrderTemplate.CompanyId);
                //            if (dt.Rows.Count > 0)
                //            {
                //                StringContent sc = new StringContent(orderRepository.getPendingOrderItems(objOrderTemplate.BrokerId, objOrderTemplate.CustomerId, objOrderTemplate.CompanyId));
                //                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                //                HttpResponseMessage data = new HttpResponseMessage();
                //                data.Content = sc;

                //                return data;
                //            }
                //            else
                //            {
                //                dt = new DataTable();
                //                dt.Columns.Add("SuccessMessage", typeof(string));
                //                dt.Columns.Add("Message", typeof(string));
                //                dt.Rows.Add("Add To Cart Item Does Not Exists", "");
                //                String jsonString = null;
                //                dtToJson = new DataToJson();
                //                // Convert Datatable to JSON string
                //                jsonString = dtToJson.convertDataTableToJson(dt, "getOrderItemList", false);
                //                StringContent sc = new StringContent(jsonString);
                //                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                //                HttpResponseMessage data = new HttpResponseMessage();
                //                data.Content = sc;

                //                return data;
                //            }
                //        }
                //}

            }
            catch (Exception ex)
            {
                log.Error("-> api/Order/getPendingOrderItems", ex);
            }
            finally
            {
                orderRepository = null;
            }
            return null;
        }

        // Get Pending Order Count by CustomerId
     //   [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Order/getPendingOrderCountByCustomerId")]
        public HttpResponseMessage GetPendingOrderCountByCustomerId(Order objOrderTemplate)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                //// Check if the client request have header with Authorization key
                //if (headers.Contains("Authorization"))
                //{
                //    // Read token received from client's request in header
                //    token = headers.GetValues("Authorization").First();
                //    userId = "013030";//headers.GetValues("UserId").First();
                //}

                //// Check if token received from client is valid, if validated then send customer list otherwise unauthorized note.
                //if (userController.IsValidToken(token, userId))
                //{
                    orderRepository = new CreateOrderRepository();
                    StringContent sc = new StringContent(orderRepository.getPendingOrderCountByCustomerId(objOrderTemplate.CustomerId));
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;

                    return data;
                //}
                //else
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");

            }
            catch (Exception ex)
            {
                log.Error("-> api/Order/getPendingOrderCountByCustomerId", ex);
            }
            finally
            {
                orderRepository = null;
            }
            return null;
        }


    //    [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Order/getLastEOR")]
        public HttpResponseMessage getLastEOR(Order objOrderTemplate)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                //// Check if the client request have header with Authorization key
                //if (headers.Contains("Authorization"))
                //{
                //    // Read token received from client's request in header
                //    token = headers.GetValues("Authorization").First();
                //    userId = "013030";//headers.GetValues("UserId").First();
                //}

                //// Check if token received from client is valid, if validated then send customer list otherwise unauthorized note.
                //if (userController.IsValidToken(token, userId))
                //{
                    orderRepository = new CreateOrderRepository();
                    StringContent sc = new StringContent(orderRepository.getLastEOR(objOrderTemplate.CompanyId, objOrderTemplate.pageTitle, objOrderTemplate.BrokerId));
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;

                    return data;
                //}
                //else
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");

            }
            catch (Exception ex)
            {
                log.Error("-> api/Order/getLastEOR", ex);
            }
            finally
            {
                orderRepository = null;
            }
            return null;
        }


        /// <summary>
        /// Get Quanitity on Hand using IBM DB2
        /// </summary>
        /// <param name="objOrderTemplate"></param>
        /// <returns></returns>
     //   [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Order/getItemQOH")]
        public HttpResponseMessage getItemQOH(Order objOrderTemplate)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                //// Check if the client request have header with Authorization key
                //if (headers.Contains("Authorization"))
                //{
                //    // Read token received from client's request in header
                //    token = headers.GetValues("Authorization").First();
                //    userId = "013030";//headers.GetValues("UserId").First();
                //}

                //// Check if token received from client is valid, if validated then send customer list otherwise unauthorized note.
                //if (userController.IsValidToken(token, userId))
                //{
                
                //      Goya and Other company is in getItemQOH () method
                orderRepository = new CreateOrderRepository();
                StringContent sc = new StringContent(orderRepository.getItemQOH(objOrderTemplate.GoyaCompanyId, objOrderTemplate.ItemCode, objOrderTemplate.CustomerId, objOrderTemplate.CaseQuantity));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
                //}
                //else
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");
            }
            catch (Exception ex)
            {

            }
            finally
            {
                orderRepository = null;
            }
            return null;
        }

        // Sync data from Indexed DB when User comes online
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Order/syncOrderData")]
        public HttpResponseMessage insertItems(Order objOrderTemplate)
        {
          
            HttpResponseMessage data = new HttpResponseMessage();
            var CartResult = JsonConvert.DeserializeObject<List<Order>>(objOrderTemplate.CartItems);

            try
            {
             
                orderRepository = new CreateOrderRepository();  

                // Parse User Order Data to Store in DB
                if (CartResult != null)
                {
                    for (int i = 0; i < CartResult.Count; i++)
                    {
                        StringContent sc = new StringContent(orderRepository.getOrderItemList(CartResult[i].CompanyId, CartResult[i].Dept, 
                            CartResult[i].ItemCode, CartResult[i].CaseQuantity,CartResult[i].UnitQuantity, CartResult[i].CatalogId, CartResult[i].CustomerId,
                            CartResult[i].BrokerId, CartResult[i].LanguageId, CartResult[i].WarehouseId, CartResult[i].isPickUp, CartResult[i].Comments,
                            CartResult[i].Action, CartResult[i].EOR, CartResult[i].Result, CartResult[i].DeliveryDate,CartResult[i].ClientPONumber,
                            CartResult[i].PromoCode, CartResult[i].Day, CartResult[i].NotDay,CartResult[i].AmountCollected, CartResult[i].GoyaCompanyId, 
                            CartResult[i].OrderMode));
                        sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        data.Content = sc;
                    }
                }
                return data;
  
            }
            catch (Exception ex)
            {
                log.Error("-> api/Order/syncOrderData", ex);
            }
            finally
            {
                orderRepository = null;
            }
            return null;
        }

    }
}
