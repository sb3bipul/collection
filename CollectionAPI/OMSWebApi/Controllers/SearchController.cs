﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Net.Http.Headers;
using System.Web.Http.Filters;
using OMSWebApi.Models;
using OMSWebApi.Repository;


namespace OMSWebApi.Controllers
{
    public class SearchController : ApiController
    {
        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        SearchRepository searchrepository = null;

        /// <param name="CustomerId">CustomerId is CustomerId in DB</param>
        /// <returns></returns>
     //   [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Search/SearchLoad")]
        public HttpResponseMessage SearchLoad(SearchLoad objItem)
        {
            UserController userController = null;            
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                //// Check if the client request have header with Authorization key
                //if (headers.Contains("Authorization"))
                //{
                //    //Read token received from client's request in header
                //    token = headers.GetValues("Authorization").First();
                //    userId = "013030";//headers.GetValues("UserId").First();
                //}
                
                //if (userController.IsValidToken(token, userId))
                //{
                    searchrepository = new SearchRepository();
                    StringContent sc = new StringContent(searchrepository.LoadProduct(objItem.CustomerId));
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;
                    return data;
                //}
                //else
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");
            }
            catch (Exception ex)
            {
                log.Error("-> api/Search/SearchLoad", ex);
            }
            finally
            {
                searchrepository = null;
            }
            return null;

        }

   //     [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Search/FreeTextSearch")]
        public HttpResponseMessage FreeTextSearch(SearchLoad objItem)
        {
            UserController userController = null;

            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                //// Check if the client request have header with Authorization key
                //if (headers.Contains("Authorization"))
                //{
                //    //Read token received from client's request in header
                //    token = headers.GetValues("Authorization").First();
                //    userId = "013030";//headers.GetValues("UserId").First();
                //}
                
                //if (userController.IsValidToken(token, userId))
                //{
                    searchrepository = new SearchRepository();
                    StringContent sc = new StringContent(searchrepository.FreeTextSearch(objItem.CustomerId, objItem.SearchText));
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;
                    return data;
                //}
                //else
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");
            }
            catch (Exception ex)
            {
                log.Error("-> api/Search/FreeTextSearch", ex);
            }
            finally
            {
                searchrepository = null;
            }
            return null;

        }

        /// <summary>
        /// Get Out of Stock Item List by Date Range
        /// </summary>
        /// <param name="objItem"></param>
        /// <returns></returns>
     //   [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Search/getOutOfStockItemList")]
        public HttpResponseMessage getOutOfStockItemList(Item objItem)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                //// Check if the client request have header with Authorization key
                //if (headers.Contains("Authorization"))
                //{
                //    // Read token received from client's request in header
                //    token = headers.GetValues("Authorization").First();
                //    userId = "013030";//headers.GetValues("UserId").First();
                //}

                //// Check if token received from client is valid, if validated then send customer list otherwise unauthorized note.
                //if (userController.IsValidToken(token, userId))
                //{
                    searchrepository = new SearchRepository();
                    StringContent sc = new StringContent(searchrepository.SProcGetOutOfStockItemList(objItem.CompanyId, objItem.FromDate, objItem.ToDate));
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;
                    return data;
                //}
                //else
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");

            }
            catch (Exception ex)
            {
                log.Error("-> api/Item/getItemList", ex);
            }
            finally
            {
                searchrepository = null;
            }
            return null;
        }



        /// <summary>
        /// Get Item List
        /// </summary>
        /// <param name="objOrderTemplate"></param>
        /// <returns></returns>
      //  [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Search/getItemListDB2")]
        public HttpResponseMessage getItemListDB2(Item objItem)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                searchrepository = new SearchRepository();
                StringContent sc = new StringContent(searchrepository.GetItemListDB2(objItem.GoyaCompanyId, objItem.BillingCompanyId, objItem.Zone, objItem.CustomerId,objItem.Limit,objItem.Offset,objItem.SearchText,objItem.IsPromo));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/Item/getItemListDB2", ex);
            }
            finally
            {
                searchrepository = null;
            }
            return null;
        }
        
    }
}

