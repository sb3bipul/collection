﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;
using OMSWebApi.Models;
using OMSWebApi.Repository;
using System.Data.Entity;
using System.Data;
using System.IO;

namespace OMSWebApi.Controllers
{
    public class PaymentController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        PaymentRepository paymentRepo = null;
        /// <summary>
        /// insert payment information
        /// </summary>
        [HttpPost]
        [Route("api/Payment/InsertPaymentInformation")]
        public HttpResponseMessage InsertPaymentInformation(PaymentModel Payment)
        {
            try
            {
                paymentRepo = new PaymentRepository();
                StringContent sc = new StringContent(paymentRepo.InsertPaymentInfo(Payment));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/Payment/InsertPaymentInformation", ex);
            }
            finally
            {
                paymentRepo = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/Payment/GetPaymentHistory")]
        public HttpResponseMessage GetPaymentHistory(PaymentHistory PaymentHist)
        {
            try
            {
                paymentRepo = new PaymentRepository();
                StringContent sc = new StringContent(paymentRepo.GetPaymentHistory(PaymentHist));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/Payment/GetPaymentHistory", ex);
            }
            finally
            {
                paymentRepo = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/Payment/getPaymentreceiptImage")]
        public HttpResponseMessage getPaymentreceiptImage(PaymentHistory PaymentHist)
        {
            try
            {
                paymentRepo = new PaymentRepository();
                StringContent sc = new StringContent(paymentRepo.getPaymentreceiptImage(PaymentHist));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/Payment/getPaymentreceiptImage", ex);
            }
            finally
            {
                paymentRepo = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/Payment/SavePaymentReceiptPDF")]
        public HttpResponseMessage SavePaymentReceiptPDF(PaymentReceipt Paymentreceipt)
        {
            try
            {
                paymentRepo = new PaymentRepository();
                StringContent sc = new StringContent(paymentRepo.SavePaymentReceiptPDF(Paymentreceipt));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/Payment/SavePaymentReceiptPDF", ex);
            }
            finally
            {
                paymentRepo = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/Payment/GetDataforCollectionAdmin")]
        public HttpResponseMessage GetDataforCollectionAdmin(PaymentHistory PaymentHist)
        {
            try
            {
                paymentRepo = new PaymentRepository();
                StringContent sc = new StringContent(paymentRepo.GetDataforCollectionAdmin(PaymentHist));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/Payment/GetDataforCollectionAdmin", ex);
            }
            finally
            {
                paymentRepo = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/Payment/getCollectionDashbordData")]
        public HttpResponseMessage getCollectionDashbordData(Search search)
        {
            try
            {
                paymentRepo = new PaymentRepository();
                StringContent sc = new StringContent(paymentRepo.getCollectionDashbordData(search));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/Payment/getCollectionDashbordData", ex);
            }
            finally
            {
                paymentRepo = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/Payment/GetAllPendingFailurePaymentDetails")]
        public HttpResponseMessage GetAllPendingFailurePaymentDetails(PaymentHistory PaymentHist)
        {
            try
            {
                paymentRepo = new PaymentRepository();
                StringContent sc = new StringContent(paymentRepo.GetAllPendingFailurePaymentDetails(PaymentHist));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/Payment/GetAllPendingFailurePaymentDetails", ex);
            }
            finally
            {
                paymentRepo = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/Payment/GetPaymentStatusByReceiptNo")]
        public HttpResponseMessage GetPaymentStatusByReceiptNo(PaymentHistory paymenthist)
        {
            try
            {
                paymentRepo = new PaymentRepository();
                StringContent sc = new StringContent(paymentRepo.GetPaymentStatusByReceiptNo(paymenthist));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/Payment/GetPaymentStatusByReceiptNo", ex);
            }
            finally
            {
                paymentRepo = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/Payment/UpdatePaymentStatus")]
        public HttpResponseMessage UpdatePaymentStatus(UpdatePayment updatept)
        {
            try
            {
                paymentRepo = new PaymentRepository();
                StringContent sc = new StringContent(paymentRepo.UpdatePaymentStatus(updatept));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/Payment/UpdatePaymentStatus", ex);
            }
            finally
            {
                paymentRepo = null;
            }
            return null;
        }

        /// <summary>
        /// Delete Pending Payment
        /// </summary>
        /// <param name="RmvPayment"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Payment/DeletePendingPayment")]
        public HttpResponseMessage DeletePendingPayment(RmvPendingPayment RmvPayment)
        {
            try
            {
                paymentRepo = new PaymentRepository();
                StringContent sc = new StringContent(paymentRepo.DeletePendingPayment(RmvPayment));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/Payment/DeletePendingPayment", ex);
            }
            finally
            {
                paymentRepo = null;
            }
            return null;
        }

        /// <summary>
        /// Resend Payment Receipt Number
        /// </summary>
        /// <param name="resend"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Payment/ResendPaymentReceipt")]
        public HttpResponseMessage ResendPaymentReceiptNumber(ResendPaymentReceipt resend)
        {
            try
            {
                paymentRepo = new PaymentRepository();
                StringContent sc = new StringContent(paymentRepo.ResendPaymentReceiptNumber(resend));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/Payment/ResendPaymentReceipt", ex);
            }
            finally
            {
                paymentRepo = null;
            }
            return null;
        }
    }
}
