﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Filters;
using OMSWebApi.Models;
using OMSWebApi.Repository;
using System.IO;

namespace OMSWebApi.Controllers
{
    public class StateController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        StateRepository stateRepository = null;

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/State/GetStateListByCountry")]
        public HttpResponseMessage GetStateListByCountry(CustProfile objState)
        {
            try
            {
                stateRepository = new StateRepository();
                //return stateRepository.GetStateListFromDb(objState.CountryID).AsEnumerable();

                StringContent sc = new StringContent(stateRepository.GetStateListFromDb(objState.CountryID));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/State/GetStateListByCountry", ex);
            }
            finally
            {
                stateRepository = null;
            }
            return null;
        }
        
    }
}