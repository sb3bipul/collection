﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;
using OMSWebApi.Models;
using OMSWebApi.Repository;
using System.Data.Entity;
using System.Data;
using System.IO;

namespace OMSWebApi.Controllers
{
    public class UIConfigController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        UIConfigRepository configRepo = null;

        /// <summary>
        /// Insert email config Data
        /// </summary>
        [HttpPost]
        [Route("api/UIConfig/InsertEmailConfigData")]
        public HttpResponseMessage InsertCollectPersonData(UIConfigModel UiConfig)
        {
            try
            {
                configRepo = new UIConfigRepository();
                StringContent sc = new StringContent(configRepo.InsertEmailConfigData(UiConfig));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/UIConfig/InsertEmailConfigData", ex);
            }
            finally
            {
                configRepo = null;
            }
            return null;
        }

        /// <summary>
        /// Insert email config Data
        /// </summary>
        [HttpPost]
        [Route("api/UIConfig/getEmailConfigDatabyCompanyId")]
        public HttpResponseMessage getEmailConfigDatabyCompanyId(SearchCinfigDt SearchConfig)
        {
            try
            {
                configRepo = new UIConfigRepository();
                StringContent sc = new StringContent(configRepo.getEmailConfigDatabyCompanyId(SearchConfig));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/UIConfig/getEmailConfigDatabyCompanyId", ex);
            }
            finally
            {
                configRepo = null;
            }
            return null;
        }

        /// <summary>
        /// Insert company data
        /// </summary>
        [HttpPost]
        [Route("api/UIConfig/InsertCompanyData")]
        public HttpResponseMessage InsertCompanyData(CompanyMaster Company)
        {
            try
            {
                configRepo = new UIConfigRepository();
                StringContent sc = new StringContent(configRepo.InsertCompanyData(Company));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/UIConfig/InsertCompanyData", ex);
            }
            finally
            {
                configRepo = null;
            }
            return null;
        }

        /// <summary>
        /// get company details list
        /// </summary>
        [HttpPost]
        [Route("api/UIConfig/GetCompanyDetails")]
        public HttpResponseMessage GetCompanyDetails(SearchCompanyList search)
        {
            try
            {
                configRepo = new UIConfigRepository();
                StringContent sc = new StringContent(configRepo.GetCompanyDetails(search));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/UIConfig/GetCompanyDetails", ex);
            }
            finally
            {
                configRepo = null;
            }
            return null;
        }

        /// <summary>
        /// Insert Ui Config Data
        /// </summary>
        [HttpPost]
        [Route("api/UIConfig/InsertUiConfigData")]
        public HttpResponseMessage InsertUiConfigData(UIConfig UiCobfig)
        {
            try
            {
                configRepo = new UIConfigRepository();
                StringContent sc = new StringContent(configRepo.InsertUiConfigData(UiCobfig));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/UIConfig/InsertUiConfigData", ex);
            }
            finally
            {
                configRepo = null;
            }
            return null;
        }

        /// <summary>
        /// Insert Collection Temp Cust
        /// </summary>
        [HttpPost]
        [Route("api/UIConfig/InsertCollectionTempCust")]
        public HttpResponseMessage InsertCollectionTempCust(CollectionTempCust tempCust)
        {
            try
            {
                configRepo = new UIConfigRepository();
                StringContent sc = new StringContent(configRepo.InsertCollectionTempCust(tempCust));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/UIConfig/InsertCollectionTempCust", ex);
            }
            finally
            {
                configRepo = null;
            }
            return null;
        }

        /// <summary>
        /// get cust config data by client name
        /// </summary>
        [HttpPost]
        [Route("api/UIConfig/GetCustConfigDataByClientName")]
        public HttpResponseMessage GetCustConfigDataByClientName(SearchCustConfig searchCustClient)
        {
            try
            {
                configRepo = new UIConfigRepository();
                StringContent sc = new StringContent(configRepo.GetCustConfigDataByClientName(searchCustClient));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/UIConfig/GetCustConfigDataByClientName", ex);
            }
            finally
            {
                configRepo = null;
            }
            return null;
        }

        /// <summary>
        /// Update Company Details
        /// </summary>
        /// <param name="updateComp"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UIConfig/UpdateCompanyDetails")]
        public HttpResponseMessage UpdateCompanyDetails(UpdateCompany updateComp)
        {
            try
            {
                configRepo = new UIConfigRepository();
                StringContent sc = new StringContent(configRepo.UpdateCompanyDetails(updateComp));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/UIConfig/UpdateCompanyDetails", ex);
            }
            finally
            {
                configRepo = null;
            }
            return null;
        }

        /// <summary>
        /// Change Company Status
        /// </summary>
        /// <param name="CompanyStatus"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UIConfig/ChangeCompanyStatus")]
        public HttpResponseMessage ChangeCompanyStatus(ChangeCompanyStatus CompanyStatus)
        {
            try
            {
                configRepo = new UIConfigRepository();
                StringContent sc = new StringContent(configRepo.ChangeCompanyStatus(CompanyStatus));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/UIConfig/ChangeCompanyStatus", ex);
            }
            finally
            {
                configRepo = null;
            }
            return null;
        }

        /// <summary>
        /// Delete Company Details
        /// </summary>
        /// <param name="DelCompany"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UIConfig/DeleteCompanyDetails")]
        public HttpResponseMessage DeleteCompanyDetails(DeleteCompany DelCompany)
        {
            try
            {
                configRepo = new UIConfigRepository();
                StringContent sc = new StringContent(configRepo.DeleteCompanyDetails(DelCompany));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/UIConfig/DeleteCompanyDetails", ex);
            }
            finally
            {
                configRepo = null;
            }
            return null;
        }

        /// <summary>
        /// Get Company List For UI Config
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/UIConfig/GetCompanyListForUIConfig")]
        public HttpResponseMessage GetCompanyListForUIConfig()
        {
            try
            {
                configRepo = new UIConfigRepository();
                StringContent sc = new StringContent(configRepo.GetCompanyListForUIConfig());
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/UIConfig/GetCompanyListForUIConfig", ex);
            }
            finally
            {
                configRepo = null;
            }
            return null;
        }

        /// <summary>
        /// Insert App UI Config Data
        /// </summary>
        /// <param name="AppUiCobfig"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UIConfig/InsertAppUIConfigData")]
        public HttpResponseMessage InsertAppUIConfigData(AppUIConfig AppUiCobfig)
        {
            try
            {
                configRepo = new UIConfigRepository();
                StringContent sc = new StringContent(configRepo.InsertAppUIConfigData(AppUiCobfig));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/UIConfig/InsertAppUIConfigData", ex);
            }
            finally
            {
                configRepo = null;
            }
            return null;
        }

        /// <summary>
        /// Get App UI Config Data By Client Name
        /// </summary>
        /// <param name="AppUiCobfig"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UIConfig/GetAppUIConfigDataByClientName")]
        public HttpResponseMessage GetAppUIConfigDataByClientName(AppUIConfig AppUiCobfig)
        {
            try
            {
                configRepo = new UIConfigRepository();
                StringContent sc = new StringContent(configRepo.GetAppUIConfigDataByClientName(AppUiCobfig));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/UIConfig/GetAppUIConfigDataByClientName", ex);
            }
            finally
            {
                configRepo = null;
            }
            return null;
        }

        /// <summary>
        /// Insert Payment method config
        /// </summary>
        /// <param name="PaymentConfig"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UIConfig/InsertPaymentmethodconfig")]
        public HttpResponseMessage InsertPaymentmethodconfig(PaymentMethodConfig PaymentConfig)
        {
            try
            {
                configRepo = new UIConfigRepository();
                StringContent sc = new StringContent(configRepo.InsertPaymentmethodconfig(PaymentConfig));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/UIConfig/InsertPaymentmethodconfig", ex);
            }
            finally
            {
                configRepo = null;
            }
            return null;
        }

        /// <summary>
        /// Get Payment Method By ClientName for collection config
        /// </summary>
        /// <param name="PmConfig"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UIConfig/GetPaymentMethodByClientName")]
        public HttpResponseMessage GetPaymentMethodByClientName(PaymentMethodConfig PmConfig)
        {
            try
            {
                configRepo = new UIConfigRepository();
                StringContent sc = new StringContent(configRepo.GetPaymentMethodByClientName(PmConfig));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/UIConfig/GetPaymentMethodByClientName", ex);
            }
            finally
            {
                configRepo = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/UIConfig/deletepaymentmethodbyclientname")]
        public HttpResponseMessage deletepaymentmethodbyclientname(PaymentMethodConfig PmConfig)
        {
            try
            {
                configRepo = new UIConfigRepository();
                StringContent sc = new StringContent(configRepo.deletepaymentmethodbyclientname(PmConfig));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/UIConfig/deletepaymentmethodbyclientname", ex);
            }
            finally
            {
                configRepo = null;
            }
            return null;
        }
    }
}
