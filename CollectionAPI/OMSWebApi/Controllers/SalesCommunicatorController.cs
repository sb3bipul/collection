﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;
using OMSWebApi.Models;
using OMSWebApi.Repository;

namespace OMSWebApi.Controllers
{
    public class SalesCommunicatorController : ApiController
    {
        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        SalesCommunicatorRepository salesCommRepository = null;
        /// <summary>
        /// Get Sales Communicator Data 
        /// </summary>
      //  [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Communicator/getSalesCommunicatorData")]
        public HttpResponseMessage getSalesCommunicatorData(SalesCommunicator objSalesCommunicator)
        {
           
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                //// Check if the client request have header with Authorization key
                //if (headers.Contains("Authorization"))
                //{
                //    // Read token received from client's request in header
                //    token = headers.GetValues("Authorization").First();
                //    userId = "013030";//headers.GetValues("UserId").First();
                //}

                //// Check if token received from client is valid, if validated then send customer list otherwise unauthorized note.
                //if (userController.IsValidToken(token, userId))
                //{
                    salesCommRepository = new SalesCommunicatorRepository();
                    StringContent sc = new StringContent(salesCommRepository.getSalesCommunicatorData(objSalesCommunicator.WeekNo));
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;

                    return data;
                //}
                //else
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");

            }
            catch (Exception ex)
            {
                log.Error("-> api/Communicator/getSalesCommunicatorData", ex);
            }
            finally
            {
                salesCommRepository = null;
            }
            return null;
        }


        /// <summary>
        /// Insert Sales Communicator file/data
        /// </summary>
        /// <param name="objSalesCommunicator"></param>
        /// <returns></returns>
      //  [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Communicator/insertSalesCommunicatorData")]
        public HttpResponseMessage insertSalesCommunicatorData(SalesCommunicator objSalesCommunicator)
        {

            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                //// Check if the client request have header with Authorization key
                //if (headers.Contains("Authorization"))
                //{
                //    // Read token received from client's request in header
                //    token = headers.GetValues("Authorization").First();
                //    userId = "013030";//headers.GetValues("UserId").First();
                //}

                //// Check if token received from client is valid, if validated then send customer list otherwise unauthorized note.
                //if (userController.IsValidToken(token, userId))
                //{
                    salesCommRepository = new SalesCommunicatorRepository();
                    StringContent sc = new StringContent(salesCommRepository.insertSalesCommunicatorData(objSalesCommunicator.WeekNo,objSalesCommunicator.Year,objSalesCommunicator.StartDate,objSalesCommunicator.EndDate,objSalesCommunicator.PdfFilePath,objSalesCommunicator.FileName,objSalesCommunicator.UserID,objSalesCommunicator.IsActive,objSalesCommunicator.Comments));
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;

                    return data;
                //}
                //else
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");

            }
            catch (Exception ex)
            {
                log.Error("-> api/Communicator/getSalesCommunicatorData", ex);
            }
            finally
            {
                salesCommRepository = null;
            }
            return null;
        }
     
        /// <summary>
        /// Send File Data to Create PDF
        /// </summary>
        /// <param name="objSalesCommunicator"></param>
        /// <returns></returns>
     //   [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Communicator/sendFileData")]
        public HttpResponseMessage sendFileData(SalesCommunicator objSalesCommunicator)
        {

            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                    salesCommRepository = new SalesCommunicatorRepository();
                    StringContent sc = new StringContent(salesCommRepository.sendFileData(objSalesCommunicator.FileName, objSalesCommunicator.FileData, objSalesCommunicator.FileType, objSalesCommunicator.SentDate));
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;

                    return data;
             
            }
            catch (Exception ex)
            {
                log.Error("-> api/Communicator/getSalesCommunicatorData", ex);
            }
            finally
            {
                salesCommRepository = null;
            }
            return null;
        }

        /// <summary>
        /// Edit Sales Communicator Data
        /// </summary>
        /// <param name="objSalesCommunicator"></param>
        /// <returns></returns>
     //   [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Communicator/editSalesCommunicatorData")]
        public HttpResponseMessage editSalesCommunicatorData(SalesCommunicator objSalesCommunicator)
        {

            // UserController declaration for Token Varification
            UserController userController = null;
            try 
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                //// Check if the client request have header with Authorization key
                //if (headers.Contains("Authorization"))
                //{
                //    // Read token received from client's request in header
                //    token = headers.GetValues("Authorization").First();
                //    userId = "013030";//headers.GetValues("UserId").First();
                //}

                //// Check if token received from client is valid, if validated then send customer list otherwise unauthorized note.
                //if (userController.IsValidToken(token, userId))
                //{
                    salesCommRepository = new SalesCommunicatorRepository();
                    StringContent sc = new StringContent(salesCommRepository.editSalesCommunicatorData(objSalesCommunicator.WeekNo, objSalesCommunicator.Year, objSalesCommunicator.StartDate, objSalesCommunicator.EndDate, objSalesCommunicator.UserID, objSalesCommunicator.IsActive, objSalesCommunicator.Comments, objSalesCommunicator.Srl));
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;

                    return data;
                //}
                //else
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");

            }
            catch (Exception ex)
            {
                log.Error("-> api/Communicator/getSalesCommunicatorData", ex);
            }
            finally
            {
                salesCommRepository = null;
            }
            return null;
        }

        /// <summary>
        /// Delete Sales Communicator (Soft Delete)
        /// </summary>
        /// <param name="objSalesCommunicator"></param>
        /// <returns></returns>
    //    [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Communicator/deleteSalesCommunicatorData")]
        public HttpResponseMessage deleteSalesCommunicatorData(SalesCommunicator objSalesCommunicator)
        {

            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                //// Check if the client request have header with Authorization key
                //if (headers.Contains("Authorization"))
                //{
                //    // Read token received from client's request in header
                //    token = headers.GetValues("Authorization").First();
                //    userId = "013030";//headers.GetValues("UserId").First();
                //}

                //// Check if token received from client is valid, if validated then send customer list otherwise unauthorized note.
                //if (userController.IsValidToken(token, userId))
                //{
                    salesCommRepository = new SalesCommunicatorRepository();
                    StringContent sc = new StringContent(salesCommRepository.deleteSalesCommunicatorData(objSalesCommunicator.Srl,objSalesCommunicator.UserID));
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;

                    return data;
                //}
                //else
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");

            }
            catch (Exception ex)
            {
                log.Error("-> api/Communicator/getSalesCommunicatorData", ex);
            }
            finally
            {
                salesCommRepository = null;
            }
            return null;
        }

    }
}
