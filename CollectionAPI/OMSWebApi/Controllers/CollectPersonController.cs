﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;
using OMSWebApi.Models;
using OMSWebApi.Repository;
using System.Data.Entity;
using System.Data;
using System.IO;

namespace OMSWebApi.Controllers
{
    public class CollectPersonController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        CollectPersonRepository collectPerson = null;

        /// <summary>
        /// Insert Collect Person Data
        /// </summary>
        [HttpPost]
        [Route("api/CollectPersonal/InsertCollectPersonData")]
        public HttpResponseMessage InsertCollectPersonData(CollectPersonModel CollectPersonal)
        {
            try
            {
                collectPerson = new CollectPersonRepository();
                StringContent sc = new StringContent(collectPerson.InsertCollectPersonData(CollectPersonal));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CollectPersonal/InsertCollectPersonData", ex);
            }
            finally
            {
                collectPerson = null;
            }
            return null;
        }

        /// <summary>
        /// Get Collect Person Data
        /// </summary>
        [HttpPost]
        [Route("api/CollectPersonal/GetCollectPersonData")]
        public HttpResponseMessage GetCollectPersonData(CollectPersonList CPList)
        {
            try
            {
                collectPerson = new CollectPersonRepository();
                StringContent sc = new StringContent(collectPerson.GetCollectPersonData(CPList));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CollectPersonal/GetCollectPersonData", ex);
            }
            finally
            {
                collectPerson = null;
            }
            return null;
        }

        [HttpGet]
        [Route("api/CollectPersonal/getCountryList")]
        public HttpResponseMessage getCountryList()
        {
            try
            {
                collectPerson = new CollectPersonRepository();
                StringContent sc = new StringContent(collectPerson.getCountryList());
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CollectPersonal/getCountryList", ex);
            }
            finally
            {
                collectPerson = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/CollectPersonal/GetStateListByCountryId")]
        public HttpResponseMessage GetStateListByCountryId(StateList state)
        {
            try
            {
                collectPerson = new CollectPersonRepository();
                StringContent sc = new StringContent(collectPerson.GetStateListByCountryId(state));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CollectPersonal/GetStateListByCountryId", ex);
            }
            finally
            {
                collectPerson = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/CollectPersonal/GetCompanylist")]
        public HttpResponseMessage GetCompanylist(CompanyList Company)
        {
            try
            {
                collectPerson = new CollectPersonRepository();
                StringContent sc = new StringContent(collectPerson.GetCompanylist(Company));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CollectPersonal/GetCompanylist", ex);
            }
            finally
            {
                collectPerson = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/CollectPersonal/GetCustomerlist")]
        public HttpResponseMessage GetCustomerlist(CustomerModel Customer)
        {
            try
            {
                collectPerson = new CollectPersonRepository();
                StringContent sc = new StringContent(collectPerson.GetCustomerlist(Customer));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CollectPersonal/GetCustomerlist", ex);
            }
            finally
            {
                collectPerson = null;
            }
            return null;
        }


        [HttpPost]
        [Route("api/CollectPersonal/getAllZone")]
        public HttpResponseMessage getAllZone(CustomerModel Customer)
        {
            try
            {
                collectPerson = new CollectPersonRepository();
                StringContent sc = new StringContent(collectPerson.getAllZone(Customer));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CollectPersonal/getAllZone", ex);
            }
            finally
            {
                collectPerson = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/CollectPersonal/GetAllCollectPersonList")]
        public HttpResponseMessage GetAllCollectPersonList(CustomerModel Customer)
        {
            try
            {
                collectPerson = new CollectPersonRepository();
                StringContent sc = new StringContent(collectPerson.GetAllCollectPersonList(Customer));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CollectPersonal/GetAllCollectPersonList", ex);
            }
            finally
            {
                collectPerson = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/CollectPersonal/SaveAssignCustomerdata")]
        public HttpResponseMessage SaveAssignCustomerdata(AssignCustomer AssignCust)
        {
            try
            {
                collectPerson = new CollectPersonRepository();
                StringContent sc = new StringContent(collectPerson.SaveAssignCustomerdata(AssignCust));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CollectPersonal/SaveAssignCustomerdata", ex);
            }
            finally
            {
                collectPerson = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/CollectPersonal/getCustCollectPersonRelation")]
        public HttpResponseMessage getCustCollectPersonRelation(SearchAssignCust getAssin)
        {
            try
            {
                collectPerson = new CollectPersonRepository();
                StringContent sc = new StringContent(collectPerson.getCustCollectPersonRelation(getAssin));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CollectPersonal/getCustCollectPersonRelation", ex);
            }
            finally
            {
                collectPerson = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/CollectPersonal/User")]
        public HttpResponseMessage getUserProfileforLogin(Userlogin user)
        {
            try
            {
                collectPerson = new CollectPersonRepository();
                StringContent sc = new StringContent(collectPerson.getUserProfileforLogin(user));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CollectPersonal/getUserProfileforLogin", ex);
            }
            finally
            {
                collectPerson = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/CollectPersonal/CustomersDetailList2")]
        public HttpResponseMessage getCustomerListByCP(CustomerListCP CpCust)
        {
            try
            {
                collectPerson = new CollectPersonRepository();
                StringContent sc = new StringContent(collectPerson.getCustomerListByCP(CpCust));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CollectPersonal/getCustomerListByCP", ex);
            }
            finally
            {
                collectPerson = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/CollectPersonal/updateCollectPersonData")]
        public HttpResponseMessage updateCollectPersonData(UpdateCp CPUpdate)
        {
            try
            {
                collectPerson = new CollectPersonRepository();
                StringContent sc = new StringContent(collectPerson.updateCollectPersonData(CPUpdate));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CollectPersonal/updateCollectPersonData", ex);
            }
            finally
            {
                collectPerson = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/CollectPersonal/ChangeCPStatus")]
        public HttpResponseMessage ChangeCPStatus(CPStatus CPstatus)
        {
            try
            {
                collectPerson = new CollectPersonRepository();
                StringContent sc = new StringContent(collectPerson.ChangeCPStatus(CPstatus));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CollectPersonal/ChangeCPStatus", ex);
            }
            finally
            {
                collectPerson = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/CollectPersonal/DeleteCPData")]
        public HttpResponseMessage DeleteCPData(CPStatus CPstatus)
        {
            try
            {
                collectPerson = new CollectPersonRepository();
                StringContent sc = new StringContent(collectPerson.DeleteCPData(CPstatus));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CollectPersonal/DeleteCPData", ex);
            }
            finally
            {
                collectPerson = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/CollectPersonal/GetBrokerListByCompanyId")]
        public HttpResponseMessage GetBrokerListByCompanyId(BrokerList broker)
        {
            try
            {
                collectPerson = new CollectPersonRepository();
                StringContent sc = new StringContent(collectPerson.GetBrokerListByCompanyId(broker));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CollectPersonal/GetBrokerListByCompanyId", ex);
            }
            finally
            {
                collectPerson = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/CollectPersonal/DeleteCPCustomer")]
        public HttpResponseMessage DeleteCPCustomer(DelCPCustomer DelCPCust)
        {
            try
            {
                collectPerson = new CollectPersonRepository();
                StringContent sc = new StringContent(collectPerson.DeleteCPCustomer(DelCPCust));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CollectPersonal/DeleteCPCustomer", ex);
            }
            finally
            {
                collectPerson = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/CollectPersonal/GetCustomerlistForCPUpdate")]
        public HttpResponseMessage GetCustomerlistForCPUpdate(CustomerModel Customer)
        {
            try
            {
                collectPerson = new CollectPersonRepository();
                StringContent sc = new StringContent(collectPerson.GetCustomerlistForCPUpdate(Customer));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CollectPersonal/GetCustomerlistForCPUpdate", ex);
            }
            finally
            {
                collectPerson = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/CollectPersonal/getCPCustomerListByCPCode")]
        public HttpResponseMessage getCPCustomerListByCPCode(CPCustomer CPCustomer)
        {
            try
            {
                collectPerson = new CollectPersonRepository();
                StringContent sc = new StringContent(collectPerson.getCPCustomerListByCPCode(CPCustomer));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CollectPersonal/getCPCustomerListByCPCode", ex);
            }
            finally
            {
                collectPerson = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/CollectPersonal/GetCustomerlistByBrokerId")]
        public HttpResponseMessage GetCustomerlistByBrokerId(CustomerModel Customer)
        {
            try
            {
                collectPerson = new CollectPersonRepository();
                StringContent sc = new StringContent(collectPerson.GetCustomerlistByBrokerId(Customer));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CollectPersonal/GetCustomerlistForCPUpdate", ex);
            }
            finally
            {
                collectPerson = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/CollectPersonal/UpdateCPCutomer")]
        public HttpResponseMessage UpdateCPCutomer(UPdateCPCustomer UpdateCP)
        {
            try
            {
                collectPerson = new CollectPersonRepository();
                StringContent sc = new StringContent(collectPerson.UpdateCPCutomer(UpdateCP));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CollectPersonal/UpdateCPCutomer", ex);
            }
            finally
            {
                collectPerson = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/CollectPersonal/DeleteCPRelation")]
        public HttpResponseMessage DeleteCPRelation(DelCPCustomer DelCPCust)
        {
            try
            {
                collectPerson = new CollectPersonRepository();
                StringContent sc = new StringContent(collectPerson.DeleteCPRelation(DelCPCust));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CollectPersonal/DeleteCPRelation", ex);
            }
            finally
            {
                collectPerson = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/CollectPersonal/GetLastEmailAndContact")]
        public HttpResponseMessage GetLastEmailAndContact(string CustomerId)
        {
            try
            {
                collectPerson = new CollectPersonRepository();
                StringContent sc = new StringContent(collectPerson.GetLastEmailAndContact(CustomerId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CollectPersonal/GetLastEmailAndContact", ex);
            }
            finally
            {
                collectPerson = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/CollectPersonal/GetOutSideERPCustomerlist")]
        public HttpResponseMessage GetOutSideERPCustomerlist(CustomerListCP outSideCustomer)
        {
            try
            {
                collectPerson = new CollectPersonRepository();
                StringContent sc = new StringContent(collectPerson.GetOutSideERPCustomerlist(outSideCustomer));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CollectPersonal/GetOutSideERPCustomerlist", ex);
            }
            finally
            {
                collectPerson = null;
            }
            return null;
        }
    }
}
