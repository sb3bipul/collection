﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using OMSWebApi.Models;
using OMSWebApi.Repository;

namespace OMSWebApi.Controllers
{
    [Authorize(Roles = "Admin")]
    public class RoleController : ApiController
    {
        RoleRepository repositoryObj = null;

        [HttpGet]
        [Route("api/Role/GetAllRoleList")]
        public IEnumerable<Roles> GetAllRoleList()
        {
            List<Roles> roleObjLst = new List<Roles>();
            repositoryObj = new RoleRepository();
            roleObjLst = (repositoryObj.GetRoleListFromDb());
            return roleObjLst;
        }

        [HttpPost]
        [Route("api/Role/UpdateRoleById")]
        public bool UpdateRoleById(Roles pObj)
        {
            repositoryObj = new RoleRepository();
            return repositoryObj.UpdateRoleToDb(pObj);
        }

        [HttpPost]
        [Route("api/Role/AddNewRole")]
        public bool AddNewRole(Roles pObj)
        {
            repositoryObj = new RoleRepository();
            return repositoryObj.AddNewRoleToDb(pObj);
        }

        [HttpPost]
        [Route("api/Role/DeleteRole")]
        public bool DeleteRole(Roles pObj)
        {
            repositoryObj = new RoleRepository();
            return repositoryObj.DeleteRoleFromDb(pObj);
        }
    }
}
