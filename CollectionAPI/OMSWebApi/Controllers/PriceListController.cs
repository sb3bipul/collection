﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http.Cors;
using System.Web.Http.Filters;
using OMSWebApi.Models;
using OMSWebApi.Repository;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using System.Configuration;

namespace OMSWebApi.Controllers
{
    public class PriceListController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        PriceListRepository priceListRepository = null;

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/PriceList/GetItemCategory")]
        public HttpResponseMessage GetItemCategory(PriceList objPriceList)
        {
            try
            {
                priceListRepository = new PriceListRepository();
                StringContent sc = new StringContent(priceListRepository.GetItemCategory(objPriceList));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/PriceList/GetItemCategory" + "param:" + objPriceList.CompanyId, ex);
                throw ex;
            }
            finally
            {
                priceListRepository = null;
            }
            //   return null;
        }

        /// <summary>
        /// Get Items List by Categories OK
        /// </summary>
        /// <param name="objPriceList"></param>
        /// <returns></returns>
        //  [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/PriceList/GetItemsListByCategories")]
        public HttpResponseMessage GetItemsListByCategories(PriceList objPriceList)
        {
            try
            {
                priceListRepository = new PriceListRepository();
                StringContent sc = new StringContent(priceListRepository.GetItemsListByCategories(objPriceList));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/PriceList/GetItemsListByCategories" + "param:" + objPriceList.CompanyId, ex);
                throw ex;
            }
            finally
            {
                priceListRepository = null;
            }
            //   return null;
        }

        /// <summary>
        /// Get Items List by Sales Category 
        /// </summary>
        /// <param name="objPriceList"></param>
        /// <returns></returns>
        //[Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/PriceList/GetItemsListBySalesCategories")]
        public HttpResponseMessage GetItemsListBySalesCategories(PriceList objPriceList)
        {
            try
            {
                priceListRepository = new PriceListRepository();
                StringContent sc = new StringContent(priceListRepository.GetItemsListBySalesCategories(objPriceList));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/PriceList/GetItemsListBySalesCategories" + "param:" + objPriceList.CompanyId, ex);
                throw ex;
            }
            finally
            {
                priceListRepository = null;
            }
            //   return null;
        }

        /// <summary>
        /// Get Items List by Tag Name
        /// </summary>
        /// <param name="objPriceList"></param>
        /// <returns></returns>
        //  [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/PriceList/GetItemsListByTagName")]
        public HttpResponseMessage GetItemsListByTagName(PriceList objPriceList)
        {
            try
            {
                priceListRepository = new PriceListRepository();
                StringContent sc = new StringContent(priceListRepository.GetItemsListByTagName(objPriceList));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/PriceList/GetItemsListByTagName" + "param:" + objPriceList.CompanyId, ex);
                throw ex;
            }
            finally
            {
                priceListRepository = null;
            }
            //   return null;
        }

        /// <summary>
        /// Get Sub Category for Item Price List OK
        /// </summary>
        /// <param name="objPriceList"></param>
        /// <returns></returns>
        //  [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/PriceList/GetItemSubCategory")]
        public HttpResponseMessage GetItemSubCategory(PriceList objPriceList)
        {
            try
            {
                priceListRepository = new PriceListRepository();
                StringContent sc = new StringContent(priceListRepository.GetItemSubCategory(objPriceList));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/PriceList/GetItemCategory", ex);
                throw ex;
            }
            finally
            {
                priceListRepository = null;
            }
            //   return null;
        }


        /// <summary>
        /// Get Sub Sub Category for Item Price List
        /// </summary>
        /// <param name="objPriceList"></param>
        /// <returns></returns>
        //  [Authorize(Roles = "Broker")]
        //[Authorize(Roles = CustomRoles.BrkRole)]
        //[HttpPost]
        //[Route("api/PriceList/GetItemSubCategory")]
        //public HttpResponseMessage GetItemSubCategory2(PriceList objPriceList)
        //{
        //    try
        //    {

        //        priceListRepository = new PriceListRepository();
        //        StringContent sc = new StringContent(priceListRepository.GetItemSubCategory2(objPriceList));
        //        sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
        //        HttpResponseMessage data = new HttpResponseMessage();
        //        data.Content = sc;

        //        return data;

        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("-> api/PriceList/GetItemCategory2", ex);
        //        throw ex;
        //    }
        //    finally
        //    {
        //        priceListRepository = null;
        //    }
        //    //   return null;
        //}

        /// <summary>
        /// 
        // Send Email with attached price list to Customer
        /// </summary>
        /// <param name="objPriceList"></param>
        /// <returns></returns>
        //  [Authorize(Roles = "Broker")]
        //[Authorize(Roles = CustomRoles.BrkRole)]
        //[HttpPost]
        //[Route("api/PriceList/EmailPriceList")]
        //public HttpResponseMessage EmailPriceList(PriceList objPriceList)
        //{
        //    try
        //    {

        //        priceListRepository = new PriceListRepository();
        //        StringContent sc = new StringContent(priceListRepository.EmailPriceList(objPriceList));
        //        sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
        //        HttpResponseMessage data = new HttpResponseMessage();
        //        data.Content = sc;

        //        return data;

        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("-> api/PriceList/EmailPriceList" + "param:" + objPriceList.CompanyId, ex);
        //        throw ex;
        //    }
        //    finally
        //    {
        //        priceListRepository = null;
        //    }
        //    //   return null;
        //}

        /// <summary>
        /// Get Item Sales Category OK
        /// </summary>

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/PriceList/GetItemSalesCategory")]
        public HttpResponseMessage GetItemSalesCategory(PriceList objPriceList)
        {
            try
            {
                priceListRepository = new PriceListRepository();
                StringContent sc = new StringContent(priceListRepository.GetItemSalesCategory(objPriceList));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/PriceList/GetItemSalesCategory" + "param:" + objPriceList.CompanyId, ex);
                throw ex;
            }
            finally
            {
                priceListRepository = null;
            }
            //   return null;
        }

        /// <summary>
        /// Get Item Sales Sub Category OK
        /// </summary>

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/PriceList/GetItemSalesSubCategory")]
        public HttpResponseMessage GetItemSalesSubCategory(PriceList objPriceList)
        {
            try
            {

                priceListRepository = new PriceListRepository();
                StringContent sc = new StringContent(priceListRepository.GetItemSalesSubCategory(objPriceList));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/PriceList/GetItemSalesSubCategory", ex);
                throw ex;
            }
            finally
            {
                priceListRepository = null;
            }
            //   return null;
        }


        // Get Items for Price List
        //[Authorize(Roles = CustomRoles.BrkRole)]
        //[HttpPost]
        //[Route("api/PriceList/getItemListDB2")]
        //public HttpResponseMessage getItemListDB2(Item objItem)
        //{
        //    try
        //    {

        //        priceListRepository = new PriceListRepository();
        //        StringContent sc = new StringContent(priceListRepository.GetItemListDB2(objItem.GoyaCompanyId, objItem.BillingCompanyId, objItem.Zone, objItem.CustomerId, objItem.Limit, objItem.Offset, objItem.SearchText, objItem.IsPromo));
        //        sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
        //        HttpResponseMessage data = new HttpResponseMessage();
        //        data.Content = sc;

        //        return data;

        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("-> api/PriceList/getItemListDB2", ex);
        //    }
        //    finally
        //    {
        //        priceListRepository = null;
        //    }
        //    return null;
        //}


        // Get Top 50 Items by Customer
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/PriceList/getTop50ItemsByCustomer")]
        public HttpResponseMessage getTop50ItemsByCustomer(Item objItem)
        {
            try
            {
                priceListRepository = new PriceListRepository();
                StringContent sc = new StringContent(priceListRepository.getTop50ItemsByCustomer(objItem));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/PriceList/getTop50ItemsByCustomer", ex);
            }
            finally
            {
                priceListRepository = null;
            }
            return null;
        }

         //Get Item List by Customer History
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/PriceList/getItemListByCustomerHistory")]
        public HttpResponseMessage getItemListByCustomerHistory(Item objItem)
        {
            try
            {
                priceListRepository = new PriceListRepository();
                StringContent sc = new StringContent(priceListRepository.getItemListByCustomerHistory(objItem));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/PriceList/getItemListByCustomerHistory", ex);
            }
            finally
            {
                priceListRepository = null;
            }
            return null;
        }

        // Get Item List by Customer Last Order
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/PriceList/getItemListByOrderLabel")]
        public HttpResponseMessage getItemListByOrderLabel(Item objItem)
        {
            try
            {
                priceListRepository = new PriceListRepository();
                if (objItem.MenuLabel == "Last Order")
                    objItem.Limit = 1;
                else if (objItem.MenuLabel == "Last 4 Order")
                    objItem.Limit = 4;

                if (objItem.MenuLabel == "Not Ordered Last Month")
                {
                    StringContent sc = new StringContent(priceListRepository.getItemListNotOrderedLastMonth(objItem));
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;
                    return data;
                }
                else
                {
                    StringContent sc = new StringContent(priceListRepository.getItemListByCustomerLastOrder(objItem));
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;
                    return data;
                }
            }
            catch (Exception ex)
            {
                log.Error("-> api/PriceList/getItemListByOrderLabel", ex);
            }
            finally
            {
                priceListRepository = null;
            }
            return null;
        }



        // Get Price Catalog PDF
        ////[Authorize(Roles = CustomRoles.BrkRole)]
        //[HttpPost]
        //[Route("api/PriceList/getPriceCatalogPDF")]
        //public HttpResponseMessage getPriceCatalogPDF(Item objItem)
        //{
        //    SqlDataAdapter da = null;
        //    DataTable dt = null;
        //    string jsonString;

        //    try
        //    {
        //        //   String retValue = objConn.insertSmartOrderCatalog("SProc_SmartOrderCatalogInsert", "@CompanyId", smrtCatalogOrd.CompanyID, "@CatalogId", Convert.ToString(smrtCatalogOrd.CatalogId), "@Week", smrtCatalogOrd.Week, "@Zone", smrtCatalogOrd.Zone, "@ItemNos", smrtCatalogOrd.ItemNos);
        //        String smartOrderID = System.DateTime.Now.Year.ToString() + System.DateTime.Now.Month.ToString().PadLeft(2, '0') + System.DateTime.Now.Day.ToString().PadLeft(2, '0') + "_" +
        //      System.DateTime.Now.Hour.ToString().PadLeft(2, '0') + System.DateTime.Now.Minute.ToString().PadLeft(2, '0') +
        //      System.DateTime.Now.Second.ToString().PadLeft(2, '0') + System.DateTime.Now.Millisecond.ToString().PadLeft(3, '0');
        //        DBConnection dbConnectionObj = null;
        //        SqlCommand SqlCmd = null;
        //        DataToJson dtToJson = new DataToJson();

        //        dbConnectionObj = new DBConnection();
        //        SqlCmd = new SqlCommand(ApiConstant.SProcGetPriceCatalogPDF, dbConnectionObj.ApiConnectionPriceList);
        //        SqlCmd.CommandType = CommandType.StoredProcedure;


        //        SqlCmd.Parameters.AddWithValue("@CompanyId", objItem.GoyaCompanyId);
        //        // SqlCmd.Parameters.AddWithValue("@CatalogId", "1");
        //        SqlCmd.Parameters.AddWithValue("@Week", objItem.Week);
        //        SqlCmd.Parameters.AddWithValue("@Zone", objItem.Zone);
        //        SqlCmd.Parameters.AddWithValue("@ItemNos", objItem.ItemNos);
        //        SqlCmd.Parameters.AddWithValue("@SmartOrderID", smartOrderID);

        //        da = new SqlDataAdapter(SqlCmd);
        //        dt = new DataTable();
        //        dbConnectionObj.ConnectionOpen();
        //        da.Fill(dt);
        //        dbConnectionObj.ConnectionClose();

        //        var strDtTm = DateTime.Now.ToString(@"yyyy/MM/dd hh:mm:ss", new CultureInfo("en-US"));
        //        strDtTm = strDtTm.Replace(":", "_");
        //        strDtTm = strDtTm.Replace("/", "_");

        //        var PDF_path = Convert.ToString(ConfigurationManager.AppSettings["PDF_downloadPath"]);
        //        var ReportURL = Convert.ToString(ConfigurationManager.AppSettings["ReportUrl"]);
        //        var filename = Convert.ToString(ConfigurationManager.AppSettings["FileName"]) + smartOrderID + ".PDF";


        //        string paramVal = "CompanyID=" + Convert.ToString(objItem.GoyaCompanyId) + "&CatalogId=1&SmartOrderID=" + smartOrderID; ;
        //        var RptURL = ReportURL + "/Pages/ReportViewer.aspx?%2fSales+Report+UAT%2frptSmartOrderCatalogDetails&rs:Command=Render&" + paramVal + "&rs:ClearSession=true&rs:Format=PDF";
        //        WebClient Client = new WebClient();
        //        Client.UseDefaultCredentials = true;

        //        byte[] myDataBuffer;

        //        try
        //        {
        //            myDataBuffer = Client.DownloadData(RptURL);
        //        }
        //        catch (Exception ex1)
        //        {
        //            log.Debug("REPORT URL ::::" + RptURL);
        //            throw ex1;
        //        }

        //        var fileLocation = PDF_path + "\\" + filename;

        //        dt.Columns.Add("FilePath");

        //        HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

        //        if (System.IO.File.Exists(fileLocation) == true)
        //        {
        //            //lblMessage.Text = "PDF File already exists!";
        //        }
        //        else
        //        {
        //            //SAVE FILE HERE
        //            System.IO.File.WriteAllBytes(fileLocation, myDataBuffer);

        //            // Send PDF images
        //            MemoryStream ms = new MemoryStream(myDataBuffer, 0, myDataBuffer.Length);
        //            ms.Write(myDataBuffer, 0, myDataBuffer.Length);


        //            result.Content = new ByteArrayContent(ms.ToArray());
        //            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");

        //            return result;

        //        }

        //        //if (dt.Rows.Count > 0)
        //        //    dt.Rows[0]["FilePath"] = fileLocation;

        //        //// Convert Datatable to JSON string
        //        //jsonString = dtToJson.convertDataTableToJson(dt, "SProcGetRouteOrderSummary", true);
        //        //return jsonString;



        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("-> api/PriceList/getPriceCatalogPDF", ex);
        //    }
        //    return null;
        //}


        // Get Customer Order Item Avegrage Count by Last 30 days, Last 4 Orders
        //[Authorize(Roles = CustomRoles.BrkRole)]
        //[HttpPost]
        //[Route("api/PriceList/GetCustomerOrderItemsAvegrage")]
        //public HttpResponseMessage getCustomerOrderItemsAvegrage(PriceList objPriceList)
        //{

        //    try
        //    {
        //        priceListRepository = new PriceListRepository();
        //        StringContent sc = new StringContent(priceListRepository.getCustomerOrderItemsAvegrage(objPriceList));
        //        sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
        //        HttpResponseMessage data = new HttpResponseMessage();
        //        data.Content = sc;

        //        return data;

        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("-> api/PriceList/getCustomerOrderItemsAvegrage", ex);
        //    }
        //    finally
        //    {
        //        priceListRepository = null;
        //    }
        //    return null;
        //}

        [HttpGet]
        [Route("api/Company/GetSmartOrderMenu/{CompanyId}")]
        public HttpResponseMessage GetSmartOrderMenu(string CompanyId)
        {
            PriceListRepository repositoryObj = new PriceListRepository();
            DataTable dtTable = new DataTable();

            dtTable = repositoryObj.GetCustomerMenuFromDb("Customer", CompanyId);       //    Customer

            DataSet dsData = new DataSet();

            dsData.Tables.Add(dtTable);

            DataTable dtTable2 = new DataTable();
            dtTable2 = repositoryObj.GetOrderMenuFromDb("Order", CompanyId);            //Order
            dsData.Tables.Add(dtTable2);

            DataTable dtTable3 = new DataTable();
            dtTable3 = repositoryObj.GetDemographicMenuFromDb("Demographic", CompanyId); //Demographic
            dsData.Tables.Add(dtTable3);

            DataTable dtTable4 = new DataTable();
            dtTable4 = repositoryObj.GetTagMenuFromDb("Tag", CompanyId);         //Tag
            dsData.Tables.Add(dtTable4);

            return Request.CreateResponse(HttpStatusCode.OK, dsData);
        }

        /// <summary>
        /// Get Items List by Promo OK
        /// </summary>
        /// <param name="objPriceList"></param>
        /// <returns></returns>
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/PriceList/GetPromoItemsList")]
        public HttpResponseMessage GetPromoItemsList(PriceList objPriceList)
        {
            try
            {
                priceListRepository = new PriceListRepository();
                StringContent sc = new StringContent(priceListRepository.GetPromoItemsList(objPriceList));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/PriceList/GetPromoItemsList" + "param:" + objPriceList.CompanyId, ex);
                throw ex;
            }
            finally
            {
                priceListRepository = null;
            }
            //   return null;
        }

        //[Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/PriceList/getItemListByCustomerHistoryLabel")]
        public HttpResponseMessage getItemListByCustomerHistoryLabel(Item objItem)
        {
            try
            {
                if (objItem.MenuLabel == "This Week")
                {
                    int days = DateTime.Now.DayOfWeek - DayOfWeek.Sunday;
                    DateTime pastDate = DateTime.Now.AddDays(-(days) + 1);
                    objItem.FromDate = Convert.ToDateTime(pastDate.ToString("MM-dd-yyyy"));
                    objItem.ToDate = Convert.ToDateTime(Convert.ToDateTime(System.DateTime.Today.Date).ToString("MM-dd-yyyy"));
                }
                else if (objItem.MenuLabel=="Last Week")
                {
                    int days = DateTime.Now.DayOfWeek - DayOfWeek.Sunday;
                    DateTime pastDate = DateTime.Now.AddDays(-(days + 7));
                    DateTime lastWekBeginDate = pastDate.AddDays(1);
                    int lastWeekdays = DateTime.Now.DayOfWeek - DayOfWeek.Sunday;
                    DateTime lastWeekEndDate = DateTime.Now.AddDays(-(lastWeekdays));
                    objItem.FromDate = Convert.ToDateTime(lastWekBeginDate.ToString("MM-dd-yyyy"));
                    objItem.ToDate = Convert.ToDateTime(lastWeekEndDate.ToString("MM-dd-yyyy"));
                }
                else if (objItem.MenuLabel == "Last Month")
                {
                    DateTime firstDay = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                    DateTime firstDate = firstDay.AddMonths(-1);
                    DateTime lastDay = firstDay.AddMonths(0).AddDays(-1);
                    objItem.FromDate = Convert.ToDateTime(firstDate.ToString("MM/dd/yyyy"));
                    objItem.ToDate = Convert.ToDateTime(lastDay.ToString("MM/dd/yyyy"));
                }
                else if (objItem.MenuLabel == "This Week Last Year")
                {
                    int thisWeekdays = DateTime.Now.DayOfWeek - DayOfWeek.Sunday;
                    DateTime thisWeekBeginDate = DateTime.Now.AddDays(-(thisWeekdays) + 1);
                    objItem.FromDate = Convert.ToDateTime(thisWeekBeginDate.ToString("MM/dd/yyyy"));
                    objItem.ToDate = Convert.ToDateTime(Convert.ToDateTime(DateTime.Today.Date).ToString("MM/dd/yyyy"));
                }
                else if (objItem.MenuLabel == "This Year")
                {
                    int year = DateTime.Now.Year;
                    DateTime firstDay = new DateTime(year, 1, 1);
                    objItem.FromDate = Convert.ToDateTime(firstDay.ToString("MM/dd/yyyy"));
                    objItem.ToDate = Convert.ToDateTime(DateTime.Today.Date.ToString("MM/dd/yyyy"));
                }
                else if (objItem.MenuLabel == "Last Year")
                {
                    int year = DateTime.Now.Year;
                    DateTime firstDay = new DateTime(year, 1, 1);
                    DateTime lastYearEndDate = (firstDay.AddDays(-1));
                    DateTime firstDayLastYear = new DateTime(year - 1, 1, 1);
                    objItem.FromDate = Convert.ToDateTime(firstDayLastYear.ToString("MM/dd/yyyy"));
                    objItem.ToDate = Convert.ToDateTime(lastYearEndDate.ToString("MM/dd/yyyy"));
                }
                else if (objItem.MenuLabel == "Last 45 Days")
                {
                    DateTime today = DateTime.Today;
                    objItem.FromDate = Convert.ToDateTime(DateTime.Now.AddDays(-45).ToString("MM/dd/yyyy"));
                    objItem.ToDate = Convert.ToDateTime(today.ToString("MM/dd/yyyy"));
                }
                
                priceListRepository = new PriceListRepository();
                
                if (objItem.MenuLabel == "Top 50 Items")
                {
                    StringContent sc = new StringContent(priceListRepository.getTop50ItemsByCustomer(objItem));
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;
                    return data;
                }
                else
                {
                    StringContent sc = new StringContent(priceListRepository.getItemListByCustomerHistory(objItem));
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;
                    return data;
                }
                
            }
            catch (Exception ex)
            {
                log.Error("-> api/PriceList/getItemListByCustomerHistoryLabel" + "param:" + objItem.CompanyId, ex);
                throw ex;
            }
            finally
            {
                priceListRepository = null;
            }
            //   return null;
        }

        [HttpGet]
        [Route("api/Company/GetOrderImagesAll/{companyId}")]
        public HttpResponseMessage GetOrderImagesAll(string companyId)
        {
            PriceListRepository repositoryObj = new PriceListRepository();
            //DataTable dtCategory = new DataTable();
            //dtCategory = repositoryObj.GetCategoryImagesFromDb(companyId); //Category Images Top

            DataSet dsData = new DataSet();
            //dsData.Tables.Add(dtCategory);

            DataTable dtBanner = new DataTable();
            dtBanner = repositoryObj.GetBannerImagesFromDb(companyId); //Banner Images Middle
            dsData.Tables.Add(dtBanner);

            //DataTable dtSalesCat = new DataTable();
            //dtSalesCat = repositoryObj.GetSalesCategoryImagesFromDb(companyId); //Sales Category Images Bottom
            //dsData.Tables.Add(dtSalesCat);

            DataTable dtItemDefImage = new DataTable();
            dtItemDefImage = repositoryObj.GetItemDefaultImageFromDb(companyId); //Item Default Images Middle
            dsData.Tables.Add(dtItemDefImage);

            return Request.CreateResponse(HttpStatusCode.OK, dsData);
        }
    }
}
