﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Filters;
using OMSWebApi.Models;
using OMSWebApi.Repository;
using System.IO;
using System.Data;

namespace OMSWebApi.Controllers
{
    //[AllowCrossSiteJson]
    public class CompanyController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        CompanyRepository compRepository = null;
        String ClientName = ApiConstant.getClientName.ToString();
        //CompanyModels repositoryObj = null;

        [HttpGet]
        [Route("api/Company/GetScreenDetails/{ScreenName}/{companyId}")]
        public IEnumerable<ScreenData> GetScreenDetails(string ScreenName, int companyId)
        {
            CompanyModels repositoryObj = new CompanyModels();
            return repositoryObj.GetLoginImgListFromDb(ScreenName, companyId).AsEnumerable();
        }


        [HttpGet]
        [Route("api/Company/GetCompanyImgDetails")]
        public IEnumerable<CompanyImgData> GetCompanyImgDetails()
        {
           compRepository = new CompanyRepository();
           return compRepository.GetCompanyImgListFromDb().AsEnumerable();
        }

        [HttpPost]
        [Route("api/Company/AddCompanyRegistration")]
        public HttpResponseMessage AddCompanyRegistration(CompanyModelsProperty objComp)
        {
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;
                string[] CompNm = objComp.CompanyName.Split(' ');
                String CompName = CompNm[0];
                compRepository = new CompanyRepository();
                StringContent sc = new StringContent(compRepository.insertCompanyRegistration(objComp.CompanyName, objComp.Street, objComp.City,
                    objComp.Country, objComp.State, objComp.ZipCode, objComp.ContactNo, objComp.EmailId, objComp.CompanyUrl, objComp.ContactPerson,
                    CompName + objComp.CompanyLogo, CompName + objComp.CompanyIcon, objComp.Comments));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
               
                try
                {   // Company Icon
                    String ModelData = "Name: " + objComp.CompanyName + ", Street: " + objComp.Street + ", City: " + objComp.City + ", Country: " + objComp.Country + ", State: " + objComp.State + ", Zip: " + objComp.ZipCode + ", Contact: " + objComp.ContactNo + ", " + objComp.EmailId + ", " + objComp.CompanyUrl + ", Contact Person: " + objComp.ContactPerson + ", Company Logo: " + objComp.CompanyLogo + ", Company Icon: " + objComp.CompanyIcon + ", Comments: " + objComp.Comments;
                    log.Info("-> api/Company/AddCompanyRegistration : Company Data: " + Convert.ToString(ModelData));
                    
                    System.Drawing.Image img;
                    String path = "E:\\OMSmobile_API\\OMSWebAPI\\OMSWebApi\\CompanyImg\\";
                    //String ImgPath = HttpContext.Current.Request.PhysicalApplicationPath + "CompanyImg\\icon\\";
                    String ImgPath = path + "icon\\";
                    if (!String.IsNullOrEmpty(objComp.IconBinaryData) && !string.IsNullOrEmpty(objComp.CompanyIcon) && objComp.CompanyIcon != "~" && objComp.IconBinaryData != "~")
                    {
                        string extension = Path.GetExtension(ImgPath + objComp.CompanyIcon);
                        if (extension == ".png" || extension == ".gif")
                            img = Base64ToImage(objComp.IconBinaryData.Substring(22));
                        else
                            img = Base64ToImage(objComp.IconBinaryData.Substring(23));

                        img.Save(ImgPath + CompName + objComp.CompanyIcon, System.Drawing.Imaging.ImageFormat.Png);
                    }
                    // Company Logo
                    System.Drawing.Image imgLogo;
                    //String ImgLogoPath = HttpContext.Current.Request.PhysicalApplicationPath + "CompanyImg\\logo\\";
                    String ImgLogoPath = path + "logo\\";
                    if (!String.IsNullOrEmpty(objComp.LogoBinarydData) && !string.IsNullOrEmpty(objComp.CompanyLogo) && objComp.CompanyLogo != "~" && objComp.LogoBinarydData != "~")
                    {
                        string extension = Path.GetExtension(ImgLogoPath + objComp.CompanyLogo);
                        if (extension == ".png" || extension == ".gif")
                            imgLogo = Base64ToImage(objComp.LogoBinarydData.Substring(22));
                        else
                            imgLogo = Base64ToImage(objComp.LogoBinarydData.Substring(23));
                        imgLogo.Save(ImgLogoPath + CompName + objComp.CompanyLogo, System.Drawing.Imaging.ImageFormat.Png);

                    }
                }
                catch (Exception ex)
                { }

                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/Company/AddCompanyRegistration", ex);
            }
            finally
            {
                compRepository = null;
            }
            return null;
        }

        public System.Drawing.Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
            return image;
        }

        public class AllowCrossSiteJsonAttribute : ActionFilterAttribute
        {
            public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
            {
                base.OnActionExecuted(actionExecutedContext);
            }
        }

        [HttpGet]
        [Route("api/Company/GetMenuFooter/{companyId}")]
        public HttpResponseMessage GetMenuFooter(int companyId)
        {
            CompanyModels repositoryObj = new CompanyModels();
            DataTable dtTable = new DataTable();
            dtTable =  repositoryObj.GetMenuFromDb("Menu", companyId); //Menu 

            DataSet dsData = new DataSet();

            dsData.Tables.Add(dtTable);

            DataTable dtTable2 = new DataTable();
            dtTable2 = repositoryObj.GetFooterFromDb("Footer", companyId); // Footer 
            dsData.Tables.Add(dtTable2);

            return Request.CreateResponse(HttpStatusCode.OK, dsData);
        }


        [HttpGet]
        [Route("api/Company/GetDashboardAllData/{companyId}/{SalesmanId}")]
        public HttpResponseMessage GetDashboardAllData(int companyId, String SalesmanId)
        {
            CompanyModels repositoryObj = new CompanyModels();
            DataTable dtTable = new DataTable();

            dtTable = repositoryObj.GetDashboardTopListFromDb(companyId); //Dashboard top

            DataSet dsData = new DataSet();

            dsData.Tables.Add(dtTable);

            DataTable dtTable2 = new DataTable();
            dtTable2 = repositoryObj.GetDashboardMiddleListFromDb(companyId, SalesmanId); //Dashboard Middle
            dsData.Tables.Add(dtTable2);

            DataTable dtTable3 = new DataTable();
            dtTable3 = repositoryObj.GetDashboardBottomListFromDb(companyId); //Dashboard Bottom
            dsData.Tables.Add(dtTable3);

            return Request.CreateResponse(HttpStatusCode.OK, dsData);
        }


    }
}