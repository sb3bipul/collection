﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Filters;
using OMSWebApi.Models;
using OMSWebApi.Repository;
using System.IO;
using System.Data;

namespace OMSWebApi.Controllers
{
    public class CountryController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        CountryRepository countryRepository = null;

        [HttpGet]
        [Route("api/Country/GetCountryList")]
        public IEnumerable<CountryModel> GetCountryList()
        {
            countryRepository = new CountryRepository();
            return countryRepository.GetCountryListFromDb().AsEnumerable();
        }

        [HttpGet]
        [Route("api/Country/GetData")]
        public HttpResponseMessage GetData()
        {
            DataSet dsData = new DataSet();
            DataTable dtTable = new DataTable();
            dtTable.Columns.Add(new DataColumn("MenuList"));
            dtTable.Rows.Add("Customer List");
            dtTable.Rows.Add("Quick Order ");
            dtTable.Rows.Add("Promotion");

            dsData.Tables.Add(dtTable);

            DataTable dtTable2 = new DataTable();
            dtTable2.Columns.Add(new DataColumn("FooterList"));
            dtTable2.Rows.Add("Customer");
            dtTable2.Rows.Add("Order");
            dtTable2.Rows.Add("Promo");
            dtTable2.Rows.Add("Activities");
            dtTable2.Rows.Add("Reports");
            dsData.Tables.Add(dtTable2);

            return Request.CreateResponse(HttpStatusCode.OK, dsData);
        }
        

        

        
    }
}