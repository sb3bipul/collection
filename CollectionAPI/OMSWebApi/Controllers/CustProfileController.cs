﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;
using OMSWebApi.Models;
using OMSWebApi.Repository;
using System.Data.Entity;
using System.Data;
using System.IO;

namespace OMSWebApi.Controllers
{
    public class CustProfileController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        CustProfileRepository custProfile = null;

        /// <summary>
        /// Get Show Customer list
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="customerId"></param>
        /// <param name="orderNumber"></param>
        /// <param name="status"></param>
        //[Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/CustProfile/GetCustomerProfileList")]
        public HttpResponseMessage GetCustomerProfileList(CustProfile objCustProfile)
        {
            try
            {
                DataTable dtTable = new DataTable();
                CustProfileRepository custProfile = new CustProfileRepository();
                dtTable = custProfile.GetCustProfileData(objCustProfile.SalesmanId, objCustProfile.SearchTxt, objCustProfile.CompanyID);
                DataSet dsData = new DataSet();
                dsData.Tables.Add(dtTable); // All Customers 

                DataTable dtTable2 = new DataTable();
                dtTable2 = custProfile.GetSectionImgList(); //Section Image
                dsData.Tables.Add(dtTable2);

                DataTable dtTable3 = new DataTable();
                dtTable3 = custProfile.GetShopOutsideImgList(); //Shop Outside Image
                dsData.Tables.Add(dtTable3);

                DataTable dtTable4 = new DataTable();
                dtTable4 = custProfile.GetShopInsideImgList(); //Shop Inside Image
                dsData.Tables.Add(dtTable4);

                DataTable dtTable5 = new DataTable();
                dtTable5 = custProfile.GetCustomerDayHoursList(objCustProfile.CompanyID); //Customer Day Hours
                dsData.Tables.Add(dtTable5);

                DataTable dtTable5_1 = new DataTable();
                dtTable5_1 = custProfile.GetCustomerReceivingHoursList(objCustProfile.CompanyID); //Customer Receiving Hours
                dsData.Tables.Add(dtTable5_1);
                //custProfile = new CustProfileRepository();
                //StringContent sc = new StringContent(custProfile.GetCustProfileData(objCustProfile.SalesmanId, objCustProfile.SearchTxt));
                //sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                //HttpResponseMessage data = new HttpResponseMessage();
                //data.Content = sc;
                //return data;
                return Request.CreateResponse(HttpStatusCode.OK, dsData);
            }
            catch (Exception ex)
            {
                log.Error("-> api/CustProfile/GetCustomerProfileList", ex);
            }
            finally
            {
                custProfile = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/CustProfile/SendEmailToCustomer")]
        public HttpResponseMessage SendEmailToCustomer(SendEmail objSendMail)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                custProfile = new CustProfileRepository();
                StringContent sc = new StringContent(custProfile.SendEmail(objSendMail.CustomerId, objSendMail.CompanyId, objSendMail.CustomerEmail, objSendMail.Action, objSendMail.Comments));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CustProfile/SendEmailToCustomer", ex);
            }
            finally
            {
                custProfile = null;
            }
            return null;
        }

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/CustProfile/SaveCustomerProfile")]
        public HttpResponseMessage SaveCustomerProfile(CustProfileViewModel objCustProfileVM)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            DataTable dtCust = new DataTable();
            DataSet dsSaveCust = new DataSet();
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                custProfile = new CustProfileRepository();
                //StringContent sc = new StringContent(custProfile.SaveCustomerProfileData(objCustProfileVM));
                //sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                //HttpResponseMessage data = new HttpResponseMessage();
                //data.Content = sc;
                //return data;

                dtCust = custProfile.SaveCustomerProfileData(objCustProfileVM);
                if (dtCust != null)
                {   
                    if (dtCust.Rows.Count > 0)
                    {
                        if (dtCust.Rows[0]["Result"].ToString() == "Ok")
                        {
                            try
                            {   // Image Icon
                                String ModelData = "Customer: " + objCustProfileVM.CustomerID + ", Store Logo Image Name: " + objCustProfileVM.StoreLogoImgName;
                                log.Info("-> api/CustProfile/SaveCustomerProfile : Image Data: " + Convert.ToString(ModelData));

                                System.Drawing.Image img;
                                String StoreImgPath = ApiConstant.StorePhotoPath;                                //String ImgPath = HttpContext.Current.Request.PhysicalApplicationPath + "ImgName\\icon\\";
                                String ImgPath = StoreImgPath + "\\";
                                if (!String.IsNullOrEmpty(objCustProfileVM.StoreLogoImgBinaryData) && !string.IsNullOrEmpty(objCustProfileVM.StoreLogoImgName) && objCustProfileVM.StoreLogoImgName != "~" && objCustProfileVM.StoreLogoImgBinaryData != "~")
                                {
                                    string extension = Path.GetExtension(ImgPath + objCustProfileVM.StoreLogoImgName);
                                    img = Base64ToImage(objCustProfileVM.StoreLogoImgBinaryData);
                                    //if (extension == ".png" || extension == ".gif")
                                    //    img = Base64ToImage(objImg.ImageBinaryData.Substring(22));
                                    //else
                                    //    img = Base64ToImage(objImg.ImageBinaryData.Substring(23));

                                    img.Save(ImgPath + objCustProfileVM.StoreLogoImgName, System.Drawing.Imaging.ImageFormat.Png);
                                }

                            }
                            catch (Exception ex)
                            { }

                            dsSaveCust.Tables.Add(dtCust); // Getting result data after saving the customer
                            String CustID = Convert.ToString(dtCust.Rows[0]["CustomerID"]);
                            //Day hours
                            DataTable dtDayHr = new DataTable();
                            dtDayHr = custProfile.GetDayHoursByCustomerId(CustID, objCustProfileVM.CompanyID); //Customer Day Hours
                            dsSaveCust.Tables.Add(dtDayHr);
                            //Receive hours
                            DataTable dtReceiveHr = new DataTable();
                            dtReceiveHr = custProfile.GetReceiveHoursByCustomerId(CustID, objCustProfileVM.CompanyID); //Customer Receive Hours
                            dsSaveCust.Tables.Add(dtReceiveHr);

                            return Request.CreateResponse(HttpStatusCode.OK, dsSaveCust);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("-> api/CustProfile/GetCustomerProfileList", ex);
            }
            finally
            {
                custProfile = null;
            }
            return null;
        }

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/CustProfile/GetStateList")]
        public HttpResponseMessage GetStateList(CustProfile objCustProf)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                custProfile = new CustProfileRepository();
                StringContent sc = new StringContent(custProfile.GetStateListData(objCustProf.CompanyID));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CustProfile/GetStateList", ex);
            }
            finally
            {
                custProfile = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/CustProfile/GetTicketDetails")]
        public HttpResponseMessage GetTicketDetails(TicketDet tcktObj)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                custProfile = new CustProfileRepository();
                StringContent sc = new StringContent(custProfile.GetTicketListData(tcktObj.CustomerID, tcktObj.UserEmpId, tcktObj.CompanyID, tcktObj.Status, tcktObj.FromDate, tcktObj.ToDate));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CustProfile/GetTicketDetails", ex);
            }
            finally
            {
                custProfile = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/CustProfile/AddSectionOutsideInsideImage")]
        public HttpResponseMessage AddSectionOutsideInsideImage(ImageData objImg)
        {
            UserController userController = null;
            try
            {
                if (objImg != null)
                {
                    userController = new UserController();
                    string token = string.Empty;
                    var request = Request;
                    var headers = request.Headers;
                    string userId = string.Empty;
                    //string[] CompNm = objComp.CompanyName.Split(' ');
                    //String CompName = CompNm[0];
                    custProfile = new CustProfileRepository();
                    //StringContent sc = new StringContent(custProfile.insertCompanyRegistration(objComp.CompanyName, objComp.Street, objComp.City,
                    //    objComp.Country, objComp.State, objComp.ZipCode, objComp.ContactNo, objComp.EmailId, objComp.CompanyUrl, objComp.ContactPerson,
                    //    CompName + objComp.CompanyLogo, CompName + objComp.CompanyIcon, objComp.Comments));
                    //sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    //HttpResponseMessage data = new HttpResponseMessage();
                    //data.Content = sc;

                    DataTable dtl = new DataTable();
                    dtl = custProfile.SaveImageList(objImg.CustomerId, objImg.CompanyId, objImg.ImageName, objImg.Action, objImg.LocalCustomerID);

                    try
                    {   // Image Icon
                        String ModelData = "Name: " + objImg.CustomerId + ", ImageName: " + objImg.ImageName + ", Srl: " + objImg.Srl + ", Action: " + objImg.Action;
                        log.Info("-> api/CustProfile/AddSectionOutsideInsideImage : Image Data: " + Convert.ToString(ModelData));

                        System.Drawing.Image img;
                        String path = ApiConstant.SectionOutsideInside_ImagePath;
                        //String ImgPath = HttpContext.Current.Request.PhysicalApplicationPath + "ImgName\\icon\\";
                        String ImgPath = path + "\\";
                        if (!String.IsNullOrEmpty(objImg.ImageBinaryData) && !string.IsNullOrEmpty(objImg.ImageName) && objImg.ImageName != "~" && objImg.ImageBinaryData != "~")
                        {
                            string extension = Path.GetExtension(ImgPath + objImg.ImageName);
                            img = Base64ToImage(objImg.ImageBinaryData);
                            //if (extension == ".png" || extension == ".gif")
                            //    img = Base64ToImage(objImg.ImageBinaryData.Substring(22));
                            //else
                            //    img = Base64ToImage(objImg.ImageBinaryData.Substring(23));

                            img.Save(ImgPath + objImg.ImageName, System.Drawing.Imaging.ImageFormat.Png);
                        }

                    }
                    catch (Exception ex)
                    { }

                    DataSet dsData = new DataSet();
                    dsData.Tables.Add(dtl);
                    return Request.CreateResponse(HttpStatusCode.OK, dsData);
                }
                //return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CustProfile/AddSectionOutsideInsideImage", ex);
            }
            finally
            {
                custProfile = null;
            }
            return null;
        }

        public System.Drawing.Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
            return image;
        }

        [HttpPost]
        [Route("api/CustProfile/SaveTicketToGenerate")]
        public HttpResponseMessage SaveTicketToGenerate(SaveTicket ocjTckt)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                custProfile = new CustProfileRepository();
                StringContent sc = new StringContent(custProfile.SaveTicketData(ocjTckt.CustomerId, ocjTckt.UserEmpId, ocjTckt.CompanyId, ocjTckt.CustomerEmail, ocjTckt.OtherReason, ocjTckt.TicketReason, ocjTckt.fileName, ocjTckt.FilePath, ocjTckt.TicketId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CustProfile/SaveTicketToGenerate", ex);
            }
            finally
            {
                custProfile = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/CustProfile/UpdateTicketStatus")]
        public HttpResponseMessage UpdateTicketStatus(UpdateTicket ocjTckt)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                custProfile = new CustProfileRepository();
                StringContent sc = new StringContent(custProfile.UpdateTicketData(ocjTckt.CompanyId, ocjTckt.TicketID, ocjTckt.Status, ocjTckt.UserId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CustProfile/UpdateTicketStatus", ex);
            }
            finally
            {
                custProfile = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/CustProfile/SaveContactUsData")]
        public HttpResponseMessage SaveContactUsData(SaveContactUs ocjContUs)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                custProfile = new CustProfileRepository();
                StringContent sc = new StringContent(custProfile.SaveContactUs(ocjContUs.CompanyID, ocjContUs.ContactName, ocjContUs.ContactEmail, ocjContUs.ContactSubject, ocjContUs.ContactMessage, ocjContUs.ContactPhone, ocjContUs.ContactCompany, ocjContUs.ContactPerson));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CustProfile/SaveContactUsData", ex);
            }
            finally
            {
                custProfile = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/CustProfile/GetOpenTicket")]
        public HttpResponseMessage GetOpenTicket(TicketDet tcktObj)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                custProfile = new CustProfileRepository();
                StringContent sc = new StringContent(custProfile.GetOpenTicketData(tcktObj.CustomerID, tcktObj.UserEmpId, tcktObj.CompanyID));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CustProfile/GetOpenTicket", ex);
            }
            finally
            {
                custProfile = null;
            }
            return null;
        }

        

        [HttpGet]
        [Route("api/CustProfile/GetGeneratedCustomerID/{CompanyId}/{BillingCO}")]
        public HttpResponseMessage GetGeneratedCustomerID(string CompanyId, String BillingCO)
        {
            CustProfileRepository repositoryObj = new CustProfileRepository();
            DataTable dtTable = new DataTable();
            dtTable = repositoryObj.GetGeneratedCustomeIDFromDB(CompanyId, BillingCO); //CustomerID 

            DataSet dsData = new DataSet();

            dsData.Tables.Add(dtTable);

            return Request.CreateResponse(HttpStatusCode.OK, dsData);
        }

        [HttpGet]
        [Route("api/CustProfile/DeleteOpenTicket/{CompanyId}/{TicketId}")]
        public HttpResponseMessage DeleteOpenTicket(string CompanyId, String TicketId)
        {
            CustProfileRepository repositoryObj = new CustProfileRepository();
            DataTable dtTable = new DataTable();
            dtTable = repositoryObj.DeleteOpenTicketFromDB(CompanyId, TicketId); 

            DataSet dsData = new DataSet();

            dsData.Tables.Add(dtTable);

            return Request.CreateResponse(HttpStatusCode.OK, dsData);
        }

        [HttpPost]
        [Route("api/CustProfile/SendEmailToNewUser")]
        public HttpResponseMessage SendEmailToNewUser(SendEmail objSendMail)
        {
            try
            {
                custProfile = new CustProfileRepository();
                StringContent sc = new StringContent(custProfile.EmailToNewUser(objSendMail.CustomerId, objSendMail.UserEmpID, objSendMail.CompanyId, objSendMail.CustomerEmail, objSendMail.Action));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CustProfile/SendEmailToNewUser", ex);
            }
            finally
            {
                custProfile = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/CustProfile/GetZoneList")]
        public HttpResponseMessage GetZoneList(CustProfile custProf)
        {
            try
            {
                custProfile = new CustProfileRepository();
                StringContent sc = new StringContent(custProfile.GetZoneListData(custProf.CompanyID));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/CustProfile/GetZoneList", ex);
            }
            finally
            {
                custProfile = null;
            }
            return null;
        }


        

    }
}
