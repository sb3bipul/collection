﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using OMSWebApi.Models;
using OMSWebApi.Repository;

namespace OMSWebApi.Controllers
{
    [Authorize(Roles = "Admin")]
    public class RoleApplicationAccessMapController : ApiController
    {

        RoleApplicationAccessMapRepository repositoryObj = null;

        [HttpGet]
        [Route("api/RoleApplicationAccessMap/GetRoleAppAccessMapViewList/{AppId}/{RoleId}/{PageFlag}")]
        public RoleApplicationAccessMapping GetAppAccessMapViewList(int AppId, int RoleId, int PageFlag)
        {
            if (PageFlag == 0)
                PageFlag = 1;
            RoleApplicationAccessMapping listObj = new RoleApplicationAccessMapping();
            repositoryObj = new RoleApplicationAccessMapRepository();
            if (PageFlag == 1)
                listObj = (repositoryObj.GetRoleAppAccessMapListFromDb(AppId, RoleId, string.Empty));
            else if (PageFlag == 2)
                listObj = (repositoryObj.GetUserAppRoleAssignedListFromDb(AppId, RoleId));
            return listObj;
        }

        [HttpPost]
        [Route("api/RoleApplicationAccessMap/UpdateRoleAppAccessList")]
        public bool UpdateRoleAppAccessList(List<RoleApplicationAccessMapping> pObj)
        {
            repositoryObj = new RoleApplicationAccessMapRepository();
            return repositoryObj.UpdateRoleAppAccessMapToDb(pObj);
        }

        [HttpPost]
        [Route("api/RoleApplicationAccessMap/UpdateUserAppRoleList")]
        public bool UpdateUserAppRoleList(List<RoleApplicationAccessMapping> pObj)
        {
            repositoryObj = new RoleApplicationAccessMapRepository();
            return repositoryObj.UpdateUserAppRoleMapToDb(pObj);
        }

    }
}
