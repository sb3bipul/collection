﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using OMSWebApi.Repository;

namespace OMSWebApi.Controllers
{
    public class ImportOMSUserController : ApiController
    {
        ImportOMSUserRepository repositoryObj = null;
        /// <summary>
        /// Import tbl_OMS_User into OAuth table AspNetUsers
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ImportOMSUser/ImprotFromOMSUserTbl")]
        public bool ImprotFromOMSUserTbl()
        {
            repositoryObj = new ImportOMSUserRepository();
            return repositoryObj.ImportOMSUserToOAuthDB();
        }
    }
}
