﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using OMSWebApi.Models;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Web.Configuration;
using System.Transactions;
using System.IO;
using System.Reflection;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using System.Configuration;
using System.Globalization;
using System.Web.Http;
using System.Web.Script.Serialization;
using System.Threading.Tasks;

namespace OMSWebApi.Repository
{
    public class PaymentRepository
    {
        DBConnection dbConnectionObj = null;
        SqlCommand SqlCmd = null;
        DataToJson dtToJson = new DataToJson();
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string InsertPaymentInfo(PaymentModel Payment)
        {
            PaymentModel PaymentImage = new PaymentModel();
            SqlDataAdapter da = null;
            DataTable dt = null;
            DataTable dtImage = new DataTable();
            DataSet dsImage = new DataSet();
            string jsonString;
            
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.Sproc_SavePaymentInfo, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CustomerId", string.IsNullOrEmpty(Payment.CustomerId) ? DBNull.Value : (object)Payment.CustomerId);
                SqlCmd.Parameters.AddWithValue("@CollectionMode", string.IsNullOrEmpty(Payment.CollectionMode) ? DBNull.Value : (object)Payment.CollectionMode);
                SqlCmd.Parameters.AddWithValue("@Amount", Convert.ToDecimal(Payment.Amount));
                SqlCmd.Parameters.AddWithValue("@CheckNo", string.IsNullOrEmpty(Payment.CheckNo) ? DBNull.Value : (object)Payment.CheckNo);
                SqlCmd.Parameters.AddWithValue("@AccountNo", string.IsNullOrEmpty(Payment.AccountNo) ? DBNull.Value : (object)Payment.AccountNo);
                SqlCmd.Parameters.AddWithValue("@BankName", string.IsNullOrEmpty(Payment.BankName) ? DBNull.Value : (object)Payment.BankName);
                SqlCmd.Parameters.AddWithValue("@CardType", string.IsNullOrEmpty(Payment.CardType) ? DBNull.Value : (object)Payment.CardType);
                SqlCmd.Parameters.AddWithValue("@FirstName", string.IsNullOrEmpty(Payment.FirstName) ? DBNull.Value : (object)Payment.FirstName);
                SqlCmd.Parameters.AddWithValue("@LastName", string.IsNullOrEmpty(Payment.LastName) ? DBNull.Value : (object)Payment.LastName);
                SqlCmd.Parameters.AddWithValue("@DigitalMode", string.IsNullOrEmpty(Payment.DigitalMode) ? DBNull.Value : (object)Payment.DigitalMode);
                SqlCmd.Parameters.AddWithValue("@UpdatedDate", Payment.UpdatedDate == DateTime.MinValue ? DBNull.Value : (object)Payment.UpdatedDate);
                SqlCmd.Parameters.AddWithValue("@CollectedBy", string.IsNullOrEmpty(Payment.CollectedBy) ? DBNull.Value : (object)Payment.CollectedBy);
                SqlCmd.Parameters.AddWithValue("@PaymentDate", Payment.PaymentDate == DateTime.MinValue ? DBNull.Value : (object)Payment.PaymentDate);
                SqlCmd.Parameters.AddWithValue("@TempReceiptNo", string.IsNullOrEmpty(Payment.TempReceiptNo) ? DBNull.Value : (object)Payment.TempReceiptNo);
                SqlCmd.Parameters.AddWithValue("@OnOffLine", string.IsNullOrEmpty(Payment.OnOffLine) ? DBNull.Value : (object)Payment.OnOffLine);
                SqlCmd.Parameters.AddWithValue("@CompanyId", Convert.ToInt32(Payment.CompanyId));
                SqlCmd.Parameters.AddWithValue("@BranchName", string.IsNullOrEmpty(Payment.BranchName) ? DBNull.Value : (object)Payment.BranchName);
                SqlCmd.Parameters.AddWithValue("@TransectionId", string.IsNullOrEmpty(Payment.TransectionId) ? DBNull.Value : (object)Payment.TransectionId);
                SqlCmd.Parameters.AddWithValue("@PaymentStatus", string.IsNullOrEmpty(Payment.PaymentStatus) ? DBNull.Value : (object)Payment.PaymentStatus);
                SqlCmd.Parameters.AddWithValue("@InvoiceNo", string.IsNullOrEmpty(Payment.InvoiceNo) ? DBNull.Value : (object)Payment.InvoiceNo);
                SqlCmd.Parameters.AddWithValue("@SendToMobile", string.IsNullOrEmpty(Payment.SendToMobile) ? DBNull.Value : (object)Payment.SendToMobile);
                SqlCmd.Parameters.AddWithValue("@SendToEmail", string.IsNullOrEmpty(Payment.SendToEmail) ? DBNull.Value : (object)Payment.SendToEmail);
                SqlCmd.Parameters.AddWithValue("@Comment", string.IsNullOrEmpty(Payment.Comment) ? DBNull.Value : (object)Payment.Comment);
                SqlCmd.Parameters.AddWithValue("@IsDisplayReceipt", Payment.IsDisplayReceipt);
                SqlCmd.Parameters.AddWithValue("@PaymentConvenienceFee", (Payment.PaymentConvenienceFee <= 0) ? DBNull.Value : (object)Payment.PaymentConvenienceFee);


                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                if (Convert.ToString(dt.Rows[0]["PaymentReceipt"]) != null)
                {
                    var saveImageName = "";
                    string PaymentReceiptNo = Convert.ToString(dt.Rows[0]["PaymentReceipt"]);
                    IEnumerable<PayLoadImage> ImageLst = Payment.payLoadImage;
                    DataTable dtImageList = new DataTable();
                    dtImageList.Columns.Add("ImageName", typeof(string));
                    dtImageList.Columns.Add("ImageBase64String", typeof(string));
                    int count = 1;
                    System.Drawing.Image img;
                    String StoreImgPath = System.Web.Hosting.HostingEnvironment.MapPath("~/PaymentImage");
                    string imageSavePath = string.Empty;
                    foreach (var itms in ImageLst)
                    {
                        //dtImageList.Rows.Add(itms.ImageName, itms.ImageBase64String);

                        var base64Image = itms.ImageBase64String;
                        var imageName = itms.ImageName;
                        if (!String.IsNullOrEmpty(base64Image))
                        {
                            saveImageName = PaymentReceiptNo + count.ToString() + Path.GetExtension(imageName);
                            imageSavePath = StoreImgPath + "\\" + saveImageName;
                            if (!String.IsNullOrEmpty(base64Image))
                            {
                                img = Base64ToImage(Convert.ToString(base64Image));
                                img.Save(imageSavePath);
                            }


                            SqlCmd = new SqlCommand(ApiConstant.SProc_SavePaymentImage, dbConnectionObj.ApiConnection);
                            SqlCmd.CommandType = CommandType.StoredProcedure;

                            SqlCmd.Parameters.AddWithValue("@ReceiptNo", PaymentReceiptNo);
                            SqlCmd.Parameters.AddWithValue("@ImageName", saveImageName);
                            SqlCmd.Parameters.AddWithValue("@ImagePath", ApiConstant.PaymentImage);

                            da = new SqlDataAdapter(SqlCmd);
                            dbConnectionObj.ConnectionOpen();
                            da.Fill(dtImage);
                            dbConnectionObj.ConnectionClose();
                            count++;
                        }
                    }
                }
                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertPaymentInfo", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertPaymentInfo", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
                dtImage = null;
            }
            return null;
        }

        public string GetPaymentHistory(PaymentHistory PaymentHist)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            DataTable dtImage = new DataTable();
            string jsonString;
            //Random rnd = new Random();
            //int ReceiptNo = rnd.Next(10000000, 99999999); 

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_GetPaymentHistory, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@ReceiptNo", string.IsNullOrEmpty(PaymentHist.ReceiptNo) ? DBNull.Value : (object)PaymentHist.ReceiptNo);
                SqlCmd.Parameters.AddWithValue("@CustomerId", string.IsNullOrEmpty(PaymentHist.Customerid) ? DBNull.Value : (object)PaymentHist.Customerid);
                SqlCmd.Parameters.AddWithValue("@Fromdate", (PaymentHist.FromDate) == DateTime.MinValue ? DBNull.Value : (object)PaymentHist.FromDate);
                SqlCmd.Parameters.AddWithValue("@Todate", (PaymentHist.ToDate) == DateTime.MinValue ? DBNull.Value : (object)PaymentHist.ToDate);
                SqlCmd.Parameters.AddWithValue("@CollectedBy", string.IsNullOrEmpty(PaymentHist.CollectedBy) ? DBNull.Value : (object)PaymentHist.CollectedBy);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetPaymentHistory", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetPaymentHistory", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
                dtImage = null;
            }
            return null;
        }

        public string getPaymentreceiptImage(PaymentHistory PaymentHist)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_getPaymentimage, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@ReceiptNo", string.IsNullOrEmpty(PaymentHist.ReceiptNo) ? DBNull.Value : (object)PaymentHist.ReceiptNo);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "getPaymentreceiptImage", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "getPaymentreceiptImage", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string getCollectionDashbordData(Search search)
        {
            SqlDataAdapter da = null;
            DataSet ds = null;
            DataTable dtImage = new DataTable();
            string jsonString;
            //Random rnd = new Random();
            //int ReceiptNo = rnd.Next(10000000, 99999999); 

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_getCollectionDashbordData, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", Convert.ToInt32(search.CompanyId));

                da = new SqlDataAdapter(SqlCmd);
                ds = new DataSet();
                dbConnectionObj.ConnectionOpen();
                da.Fill(ds);
                dbConnectionObj.ConnectionClose();
                ds.Tables[0].TableName = "DailyCollection";
                ds.Tables[1].TableName = "TotalCollection";
                ds.Tables[2].TableName = "MonthlyCollection";
                ds.Tables[3].TableName = "TotalCP";
                if (ds.Tables.Count > 4)
                    ds.Tables[4].TableName = "TotalAssignedCust";

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataSetToJson(ds, "getCollectionDashbordData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataSetToJson(ds, "getCollectionDashbordData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                ds = null;
                dtImage = null;
            }
            return null;
        }
        public string SavePaymentReceiptPDF(PaymentReceipt PaymentPdf)
        {
            PaymentModel PaymentImage = new PaymentModel();
            SqlDataAdapter da = null;
            DataTable dt = null;
            DataTable dtEmail = null;
            DataTable dtBroker = null;
            DataTable dtConfig = null;
            DataTable dtCom = null;
            string jsonString;
            string filePath, from, to, cc = "", subject, body;
            SMSService sms = new SMSService();
            try
            {
                if (PaymentPdf.PdfBase64string != null)
                {
                    var savePdfName = "";
                    System.Drawing.Image img;
                    String StorePdfPath = System.Web.Hosting.HostingEnvironment.MapPath("~/PaymentImage");
                    string PdfSavePath = string.Empty;

                    var base64Pdf = PaymentPdf.PdfBase64string;
                    var PdfName = PaymentPdf.PDFName;
                    if (!String.IsNullOrEmpty(base64Pdf))
                    {
                        savePdfName = PaymentPdf.ReceiptNo + Path.GetExtension(PdfName);
                        PdfSavePath = StorePdfPath + "\\" + savePdfName;
                        if (!String.IsNullOrEmpty(base64Pdf))
                        {
                            byte[] PDFDecoded = Convert.FromBase64String(base64Pdf);
                            FileStream obj = File.Create(PdfSavePath);
                            obj.Write(PDFDecoded, 0, PDFDecoded.Length);
                            obj.Flush();
                            obj.Close();
                        }

                        dbConnectionObj = new DBConnection();
                        SqlCmd = new SqlCommand(ApiConstant.SProc_SavePaymentPDF, dbConnectionObj.ApiConnection);
                        SqlCmd.CommandType = CommandType.StoredProcedure;

                        SqlCmd.Parameters.AddWithValue("@ReceiptNo", string.IsNullOrEmpty(PaymentPdf.ReceiptNo) ? DBNull.Value : (object)PaymentPdf.ReceiptNo);
                        SqlCmd.Parameters.AddWithValue("@PDFName", savePdfName);
                        SqlCmd.Parameters.AddWithValue("@PDFPath", ApiConstant.PaymentImage);

                        da = new SqlDataAdapter(SqlCmd);
                        dt = new DataTable();
                        dbConnectionObj.ConnectionOpen();
                        da.Fill(dt);
                        dbConnectionObj.ConnectionClose();
                    }
                }

                if (dt.Rows.Count > 0)
                {
                    SqlCmd = new SqlCommand(ApiConstant.SProc_GetCustomerContactInfo, dbConnectionObj.ApiConnection);
                    SqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlCmd.Parameters.AddWithValue("@PaymentReceipt", Convert.ToString(PaymentPdf.ReceiptNo));
                    da = new SqlDataAdapter(SqlCmd);
                    dtEmail = new DataTable();
                    dbConnectionObj.ConnectionOpen();
                    da.Fill(dtEmail);
                    SqlConnection Sqlcon = new SqlConnection(ApiConstant.apiConnectionStringConfig);
                    Sqlcon.Open();
                    String Sqlquery = @"SELECT CompanyName,ContactNo, EmailId FROM tbl_collection_CompanyMaster WHERE CompanyId=@CompanyId";
                    SqlCommand Sqlcommand = new SqlCommand(Sqlquery, Sqlcon);
                    Sqlcommand.Parameters.Add("@CompanyId", Convert.ToString(dtEmail.Rows[0]["CompanyId"]));
                    Sqlcommand.ExecuteNonQuery();
                    da = new SqlDataAdapter(Sqlcommand);
                    dtCom = new DataTable();
                    da.Fill(dtCom);
                    Sqlcon.Close();
                    System.Data.DataColumn Column = new System.Data.DataColumn("COMPANYCONTACT", typeof(System.String));
                    System.Data.DataColumn Column1 = new System.Data.DataColumn("CPMPANYEMAIL", typeof(System.String));
                    Column.DefaultValue = Convert.ToString(dtCom.Rows[0]["ContactNo"]);
                    Column1.DefaultValue = Convert.ToString(dtCom.Rows[0]["EmailId"]);
                    dtEmail.Columns.Add(Column);
                    dtEmail.Columns.Add(Column1);

                    dbConnectionObj.ConnectionClose();

                    SqlConnection con = new SqlConnection(ApiConstant.apiConnectionStringOAuth);
                    con.Open();
                    String query = @"SELECT DISTINCT U.USERNAME, USERFULLNAME, U.Email,U.PhoneNumber FROM AspNetUsers U INNER JOIN AspNetUserRoles R ON U.UserName=R.UserName AND U.Id=R.UserId
                                    WHERE CompanyId=2 AND R.RoleId IN (1,7,8) and U.UserName=@BrokerId";
                    SqlCommand command = new SqlCommand(query, con);
                    command.Parameters.Add("@BrokerId", Convert.ToString(dtEmail.Rows[0]["SalesManID"]));
                    command.ExecuteNonQuery();
                    da = new SqlDataAdapter(command);
                    //DataSet ds = new DataSet();
                    dtBroker = new DataTable();
                    da.Fill(dtBroker);
                    con.Close();

                    SqlCmd = new SqlCommand(ApiConstant.SProc_getemailconfigByCompanyId, dbConnectionObj.ApiConnection);
                    SqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlCmd.Parameters.AddWithValue("@CompanyId", 2);
                    da = new SqlDataAdapter(SqlCmd);
                    dtConfig = new DataTable();
                    dbConnectionObj.ConnectionOpen();
                    da.Fill(dtConfig);
                    dbConnectionObj.ConnectionClose();

                    if (dtConfig.Rows.Count > 0)
                    {
                        if (Convert.ToBoolean(dtConfig.Rows[0]["IsEmailToCompany"]) == true)
                        {
                            cc = Convert.ToString(dtEmail.Rows[0]["CPMPANYEMAIL"]) + ";";
                        }
                        if (Convert.ToBoolean(dtConfig.Rows[0]["IsEmailToBroker"]) == true)
                        {
                            cc = Convert.ToString(dtBroker.Rows[0]["Email"]) + ";" + cc;
                        }
                        if (Convert.ToBoolean(dtConfig.Rows[0]["IsEmailToCP"]) == true)
                        {
                            cc = Convert.ToString(dtEmail.Rows[0]["EmailId"]) + ";" + cc;
                        }
                    }

                    filePath = HttpContext.Current.Server.MapPath("~/PaymentImage/") + Convert.ToString(PaymentPdf.ReceiptNo) + ".pdf";

                    from = WebConfigurationManager.AppSettings["FromEmail"];
                    to = Convert.ToString(dtEmail.Rows[0]["SendToEmail"]);
                    cc = WebConfigurationManager.AppSettings["CCedEmail"] + ";" + cc;
                    subject = WebConfigurationManager.AppSettings["PaymentCreateSubject"];
                    body = "Dear " + Convert.ToString(dtEmail.Rows[0]["Name"]) + ",<br/>" + "Your payment is credited successfully on " + dtEmail.Rows[0]["CreatedDate"] + "...!! For more reference, Please check the attachment OR You can download the receipt by below link<br/>." + dt.Rows[0]["Pdflink"] + " Your payment receipt number is "
                        + Convert.ToString(PaymentPdf.ReceiptNo) + "<br/><br/><b>Thanks</b><br/>" + Convert.ToString(dtCom.Rows[0]["CompanyName"]);

                    bool isEmailSent = SendEmail(from, to, subject, body, cc, filePath, Convert.ToString(PaymentPdf.ReceiptNo));


                    sms.send_to = Convert.ToString(dtEmail.Rows[0]["SendToMobile"]);
                    sms.message = Convert.ToString("Dear " + Convert.ToString(dtEmail.Rows[0]["Name"]) + ",\n" + "Your payment is credited successfully on " + dtEmail.Rows[0]["CreatedDate"] + "...!! For more reference, Please download the receipt by below link.\n" + dt.Rows[0]["Pdflink"] + "\nYour payment receipt number is "
                        + Convert.ToString(PaymentPdf.ReceiptNo) + "\n\nThanks\n" + Convert.ToString(dtCom.Rows[0]["CompanyName"]));

                    CallSMSServiceAPI(sms);

                }

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertPaymentReceiptPdf", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertPaymentReceiptPdf", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }


        public string GetDataforCollectionAdmin(PaymentHistory PaymentHist)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_getDataforCollectionAdmin, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@Fromdate", (PaymentHist.FromDate) == DateTime.MinValue ? DBNull.Value : (object)PaymentHist.FromDate);
                SqlCmd.Parameters.AddWithValue("@ToDate", (PaymentHist.ToDate) == DateTime.MinValue ? DBNull.Value : (object)PaymentHist.ToDate);
                SqlCmd.Parameters.AddWithValue("@ReceiptNo", string.IsNullOrEmpty(PaymentHist.ReceiptNo) ? DBNull.Value : (object)PaymentHist.ReceiptNo);
                SqlCmd.Parameters.AddWithValue("@CustomerId", string.IsNullOrEmpty(PaymentHist.Customerid) ? DBNull.Value : (object)PaymentHist.Customerid);
                SqlCmd.Parameters.AddWithValue("@CollectedBy", string.IsNullOrEmpty(PaymentHist.CollectedBy) ? DBNull.Value : (object)PaymentHist.CollectedBy);
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetDataforCollectionAdmin", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetDataforCollectionAdmin", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public System.Drawing.Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
            return image;
        }

        public string GetAllPendingFailurePaymentDetails(PaymentHistory PaymentHist)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_GetAllPendingPaymentDetails, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@ReceiptNo", string.IsNullOrEmpty(PaymentHist.ReceiptNo) ? DBNull.Value : (object)PaymentHist.ReceiptNo);
                SqlCmd.Parameters.AddWithValue("@CustomerId", string.IsNullOrEmpty(PaymentHist.Customerid) ? DBNull.Value : (object)PaymentHist.Customerid);
                SqlCmd.Parameters.AddWithValue("@Fromdate", (PaymentHist.FromDate) == DateTime.MinValue ? DBNull.Value : (object)PaymentHist.FromDate);
                SqlCmd.Parameters.AddWithValue("@Todate", (PaymentHist.ToDate) == DateTime.MinValue ? DBNull.Value : (object)PaymentHist.ToDate);
                SqlCmd.Parameters.AddWithValue("@CollectedBy", string.IsNullOrEmpty(PaymentHist.CollectedBy) ? DBNull.Value : (object)PaymentHist.CollectedBy);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetAllPendingPaymentDetails", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetAllPendingPaymentDetails", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string GetPaymentStatusByReceiptNo(PaymentHistory pamentHistory)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_GetPaymentStatusByReceiptNo, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@ReceiptNo", string.IsNullOrEmpty(pamentHistory.ReceiptNo) ? DBNull.Value : (object)pamentHistory.ReceiptNo);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetPaymentStatusByReceiptNo", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetPaymentStatusByReceiptNo", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string UpdatePaymentStatus(UpdatePayment updatept)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            string Status = string.Empty;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_UpdatePaymentStatus, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@TempReceiptNo", string.IsNullOrEmpty(updatept.order_id) ? DBNull.Value : (object)updatept.order_id);
                if (updatept.status == "success")
                {
                    Status = "Successful";
                }
                else
                {
                    Status = updatept.status;
                }
                SqlCmd.Parameters.AddWithValue("@Status", string.IsNullOrEmpty(Status) ? DBNull.Value : (object)Status);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "UpdatePaymentStatus", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "UpdatePaymentStatus", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string DeletePendingPayment(RmvPendingPayment rmvpmt)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_DeleteFailedPayment, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@ReceiptNo", string.IsNullOrEmpty(rmvpmt.ReceiptNo) ? DBNull.Value : (object)rmvpmt.ReceiptNo);
                SqlCmd.Parameters.AddWithValue("@CompanyId", string.IsNullOrEmpty(rmvpmt.CompanyId) ? DBNull.Value : (object)rmvpmt.CompanyId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "DeletePendingPayment", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "DeletePendingPayment", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }


        [AllowAnonymous]
        public static bool SendEmail(string from, string to, string subject, string body, string cc, string filePath, string InvoiceNo)
        {
            bool isEmailSent = false;

            try
            {
                MailMessage email = new MailMessage(from, to, subject, body);
                System.Net.Mail.Attachment attachment;
                attachment = new System.Net.Mail.Attachment(filePath);
                email.Attachments.Add(attachment);
                if (cc != "")
                {
                    foreach (var address in cc.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        email.CC.Add(address);
                    }
                }
                email.IsBodyHtml = true;

                string server = WebConfigurationManager.AppSettings["SMTPServer"];
                SmtpClient smtp = new SmtpClient(server);

                //smtp.UseDefaultCredentials = true;
                smtp.UseDefaultCredentials = false;
                smtp.Port = 587;
                smtp.EnableSsl = true;
                string userEmail = WebConfigurationManager.AppSettings["SMTPServerEmail"].ToString();
                string password = WebConfigurationManager.AppSettings["SMTPServerPassword"].ToString();

                smtp.Credentials = new System.Net.NetworkCredential(userEmail, password);
                smtp.Send(email);
                isEmailSent = true;
            }
            catch (Exception ex)
            {
                isEmailSent = false;
            }
            return isEmailSent;
        }

        public string CallSMSServiceAPI(SMSService SMS)
        {
            string data = string.Empty;
            string SmsAPI = Convert.ToString(ConfigurationSettings.AppSettings["SingleSMSUrl"].ToString());
            string token = Convert.ToString(ConfigurationSettings.AppSettings["Token"].ToString());
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(SmsAPI);
                request.Timeout = 1000000;
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("Authorization", "AppToken " + token);
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (var sw = new StreamWriter(request.GetRequestStream()))
                {
                    string json = serializer.Serialize(SMS);
                    sw.Write(json);
                    sw.Flush();
                }
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    data = reader.ReadToEnd();
                }
                return data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public string ResendPaymentReceiptNumber(ResendPaymentReceipt resend)
        {
            SqlDataAdapter da = null;
            DataTable dtEmail = null;
            DataTable dtCom = null;
            DataTable dtBroker = null;
            DataTable dtConfig = null;
            string filePath, from, to, cc = "", subject, body;
            string status = string.Empty;
            DataTable dt = null;
            SMSService sms = new SMSService();
            dbConnectionObj = new DBConnection();
            try
            {
                SqlCmd = new SqlCommand(ApiConstant.SProc_GetCustomerContactInfo, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@PaymentReceipt", Convert.ToString(resend.ReceiptNo));
                da = new SqlDataAdapter(SqlCmd);
                dtEmail = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dtEmail);
                SqlConnection Sqlcon = new SqlConnection(ApiConstant.apiConnectionStringConfig);
                Sqlcon.Open();
                String Sqlquery = @"SELECT CompanyName,ContactNo, EmailId FROM tbl_collection_CompanyMaster WHERE CompanyId=@CompanyId";
                SqlCommand Sqlcommand = new SqlCommand(Sqlquery, Sqlcon);
                Sqlcommand.Parameters.Add("@CompanyId", Convert.ToString(dtEmail.Rows[0]["CompanyId"]));
                Sqlcommand.ExecuteNonQuery();
                da = new SqlDataAdapter(Sqlcommand);
                dtCom = new DataTable();
                da.Fill(dtCom);
                Sqlcon.Close();
                System.Data.DataColumn Column = new System.Data.DataColumn("COMPANYCONTACT", typeof(System.String));
                System.Data.DataColumn Column1 = new System.Data.DataColumn("CPMPANYEMAIL", typeof(System.String));
                Column.DefaultValue = Convert.ToString(dtCom.Rows[0]["ContactNo"]);
                Column1.DefaultValue = Convert.ToString(dtCom.Rows[0]["EmailId"]);
                dtEmail.Columns.Add(Column);
                dtEmail.Columns.Add(Column1);

                dbConnectionObj.ConnectionClose();

                SqlConnection con = new SqlConnection(ApiConstant.apiConnectionStringOAuth);
                con.Open();
                String query = @"SELECT DISTINCT U.USERNAME, USERFULLNAME, U.Email,U.PhoneNumber FROM AspNetUsers U INNER JOIN AspNetUserRoles R ON U.UserName=R.UserName AND U.Id=R.UserId
                                    WHERE CompanyId=2 AND R.RoleId IN (1,7,8) and U.UserName=@BrokerId";
                SqlCommand command = new SqlCommand(query, con);
                command.Parameters.Add("@BrokerId", Convert.ToString(dtEmail.Rows[0]["SalesManID"]));
                command.ExecuteNonQuery();
                da = new SqlDataAdapter(command);
                //DataSet ds = new DataSet();
                dtBroker = new DataTable();
                da.Fill(dtBroker);
                con.Close();

                SqlCmd = new SqlCommand(ApiConstant.SProc_getemailconfigByCompanyId, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", resend.CompanyId);
                da = new SqlDataAdapter(SqlCmd);
                dtConfig = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dtConfig);
                dbConnectionObj.ConnectionClose();

                if (dtConfig.Rows.Count > 0)
                {
                    if (Convert.ToBoolean(dtConfig.Rows[0]["IsEmailToCompany"]) == true)
                    {
                        cc = Convert.ToString(dtEmail.Rows[0]["CPMPANYEMAIL"]) + ";";
                    }
                    if (Convert.ToBoolean(dtConfig.Rows[0]["IsEmailToBroker"]) == true)
                    {
                        cc = Convert.ToString(dtBroker.Rows[0]["Email"]) + ";" + cc;
                    }
                    if (Convert.ToBoolean(dtConfig.Rows[0]["IsEmailToCP"]) == true)
                    {
                        cc = Convert.ToString(dtEmail.Rows[0]["EmailId"]) + ";" + cc;
                    }
                }

                filePath = HttpContext.Current.Server.MapPath("~/PaymentImage/") + Convert.ToString(resend.ReceiptNo) + ".pdf";

                from = WebConfigurationManager.AppSettings["FromEmail"];
                to = Convert.ToString(dtEmail.Rows[0]["SendToEmail"]);
                cc = WebConfigurationManager.AppSettings["CCedEmail"] + ";" + cc;
                subject = WebConfigurationManager.AppSettings["PaymentCreateSubject"];
                body = "Dear " + Convert.ToString(dtEmail.Rows[0]["Name"]) + ",<br/>" + "Your payment is credited successfully on " + dtEmail.Rows[0]["CreatedDate"] + "...!! For more reference, Please check the attachment OR You can download the receipt by below link<br/>." + dtEmail.Rows[0]["Pdflink"] + " Your payment receipt number is "
                    + Convert.ToString(resend.ReceiptNo) + "<br/><br/><b>Thanks</b><br/>" + Convert.ToString(dtCom.Rows[0]["CompanyName"]);

                bool isEmailSent = SendEmail(from, to, subject, body, cc, filePath, Convert.ToString(resend.ReceiptNo));

                sms.send_to = Convert.ToString(dtEmail.Rows[0]["SendToMobile"]);
                sms.message = Convert.ToString("Dear " + Convert.ToString(dtEmail.Rows[0]["Name"]) + ",\n" + "Your payment is credited successfully on " + dtEmail.Rows[0]["CreatedDate"] + "...!! For more reference, Please download the receipt by below link.\n" + dtEmail.Rows[0]["Pdflink"] + "\nYour payment receipt number is "
                    + Convert.ToString(resend.ReceiptNo) + "\n\nThanks\n" + Convert.ToString(dtCom.Rows[0]["CompanyName"]));

                CallSMSServiceAPI(sms);

                if (isEmailSent)
                {
                    dt = new DataTable();
                    dt.Columns.Add("Status");
                    DataRow dr = dt.NewRow();
                    dr[0] = "200";
                    dt.Rows.Add(dr);
                    status = dtToJson.convertDataTableToJson(dt, "ResendPaymentReceiptNumber", true);
                }
                return status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dtBroker = null;
                dtCom = null;
                dtConfig = null;
                dtEmail = null;
            }
            return null;
        }
    }
}