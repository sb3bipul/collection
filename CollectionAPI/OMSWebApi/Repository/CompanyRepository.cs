﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using OMSWebApi.Models;
using System.Xml.Serialization;
using Newtonsoft.Json.Linq;
using System.Data.OleDb;
using System.Web.Configuration;
using OMSWebApi.Controllers;
using OMSWebApi.Providers;
using OMSWebApi.Results;
using OMSWebApi.Repository;
using System.Net.Mail;

namespace OMSWebApi.Repository
{
    public class CompanyRepository
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        DBConnection dbConnectionObj = null;
        SqlCommand SqlCmd = null;
        DataToJson dtToJson = new DataToJson();
        SqlCommand com = null;
        SqlDataAdapter da = null;
        DataTable dt = null;

        public string insertCompanyRegistration(string coName, string coStreet, string coCity, string coCountry, string coState, string coZip, string coContactNo, string coEmail, string coUrl, string coContactPerson, string coLogo, string coIcon, string coComments)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcInsertCompany, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;


                SqlCmd.Parameters.AddWithValue("@CompanyName", coName);
                SqlCmd.Parameters.AddWithValue("@Street", coStreet);
                SqlCmd.Parameters.AddWithValue("@City", coCity);
                SqlCmd.Parameters.AddWithValue("@Country", coCountry);
                SqlCmd.Parameters.AddWithValue("@State", coState);
                SqlCmd.Parameters.AddWithValue("@ZipCode", coZip);
                SqlCmd.Parameters.AddWithValue("@ContactNo", coContactNo);
                SqlCmd.Parameters.AddWithValue("@EmailId", coEmail);
                SqlCmd.Parameters.AddWithValue("@CompanyUrl", coUrl);
                SqlCmd.Parameters.AddWithValue("@ContactPerson", coContactPerson);
                SqlCmd.Parameters.AddWithValue("@CompanyLogo", coLogo);
                SqlCmd.Parameters.AddWithValue("@CompanyIcon", coIcon);
                SqlCmd.Parameters.AddWithValue("@Comments", coComments);
                SqlCmd.Parameters.AddWithValue("@MESSAGE", "");

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "getCompanyList", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "getCompanyList", false);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public List<CompanyImgData> GetCompanyImgListFromDb()
        {
            Common common = new Common();
            List<CompanyImgData> CompImgListObj = null;
            try
            {
                CompImgListObj = new List<CompanyImgData>();
                dbConnectionObj = new DBConnection();
                com = new SqlCommand(ApiConstant.SProcGetCompanyImageList, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                ////Bind CompanyImgModel generic list using LINQ 
                CompImgListObj = (from DataRow dr in dt.Rows
                                   select new CompanyImgData()
                                   {
                                       ImageID = Convert.ToInt32(dr["ImageID"]),
                                       ImageFullPath = Convert.ToString(dr["ImageFullPath"]),
                                       CompanyID = Convert.ToInt32(dr["CompanyID"]),
                                       cmBGImageName = Convert.ToString(dr["cmBGImageName"]),
                                       cmLogoName = Convert.ToString(dr["cmLogoName"]),
                                       cmIconName = Convert.ToString(dr["cmIconName"]),
                                       CompanyName = Convert.ToString(dr["CompanyName"]),
                                       HeaderColor = Convert.ToString(dr["HeaderColor"]),
                                       AllPagesBGColor = Convert.ToString(dr["AllPagesBGColor"]),
                                       MiddleSectionBGcolor = Convert.ToString(dr["MiddleSectionBGcolor"]),
                                       FooterBGcolor = Convert.ToString(dr["FooterBGcolor"]),
                                       OrderVerify = Convert.ToString(dr["OrderVerify"])
                                   }).ToList();


            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbConnectionObj = null;
                com = null;
                da = null;
                dt = null;
            }

            return CompImgListObj;
        }

    }
}