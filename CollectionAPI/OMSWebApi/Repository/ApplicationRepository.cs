﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using OMSWebApi.Models;

namespace OMSWebApi.Repository
{
    public class ApplicationRepository
    {
       
        DBConnectionOAuth dbConnectionObj = null;
        SqlCommand com = null;

        /// <summary>
        /// Retunn list of all Aplication
        /// </summary>
        /// <returns></returns>
        public List<Applications> GetApplicationListFromDb()
        {
            List<Applications> AppList = null;
            
            SqlDataAdapter da = null;
            DataTable dt = null;
            try
            {
                AppList = new List<Applications>();
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocGetApplicationsList, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                //Bind ApplicationsModel generic list using LINQ 
                AppList = (from DataRow dr in dt.Rows

                           select new Applications()
                            {
                                ApplicationId = Convert.ToInt32(dr["ApplicationId"]),
                                ApplicationName = Convert.ToString(dr["ApplicationName"]),
                                Description = Convert.ToString(dr["Description"]),
                              //  CurrentVersion = Convert.ToString(dr["CurrentVersion"]),
                                //StartDate = dr["StartDate"] == DBNull.Value ? (DateTime?)null : (DateTime)dr["StartDate"],
                                //EndDate = dr["EndDate"] == DBNull.Value ? (DateTime?)null : (DateTime)dr["EndDate"],
                                //CreateDate = dr["CreateDate"] == DBNull.Value ? (DateTime?)null : (DateTime)dr["CreateDate"],
                                //UpdateDate = dr["UpdateDate"] == DBNull.Value ? (DateTime?)null : (DateTime)dr["UpdateDate"],
                                //  CreateDate = Convert.ToString(dr["CreateDate"]),
                            //    StartDate = Convert.ToString(dr["StartDate"]),
                            //    EndDate = Convert.ToString(dr["EndDate"]), 
                                CreateDate = Convert.ToString(dr["CreateDate"]), 
                                UpdateDate = Convert.ToString(dr["UpdateDate"]),
                                CreatedBy = Convert.ToString(dr["CreatedBy"]),
                                UpdatedBy = Convert.ToString(dr["UpdatedBy"]),
                                IsActive = Convert.ToBoolean(dr["IsActive"])
                            }).ToList();

                return AppList;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                dbConnectionObj = null;
                AppList = null;
                com = null;
                da = null;
                dt = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objApplications"></param>
        /// <returns></returns>
        public bool UpdateApplicationToDb(Applications objApplications)
        {
            bool success = false;
            SqlDataReader reader = null;
            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocUpdateApplicationById, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@ApplicationId", objApplications.ApplicationId);
                com.Parameters.AddWithValue("@ApplicationName", objApplications.ApplicationName);
                com.Parameters.AddWithValue("@Description", objApplications.Description);
               // com.Parameters.AddWithValue("@CurrentVersion", objApplications.CurrentVersion);
             //   com.Parameters.AddWithValue("@StartDate", objApplications.StartDate);
              //  com.Parameters.AddWithValue("@EndDate", objApplications.EndDate);
              //  com.Parameters.AddWithValue("@CreateDate", objApplications.CreateDate);
                com.Parameters.AddWithValue("@UpdateDate", objApplications.UpdateDate);
              //  com.Parameters.AddWithValue("@CreatedBy", objApplications.CreatedBy);
             //   com.Parameters.AddWithValue("@UpdatedBy", objApplications.UpdatedBy);
                com.Parameters.AddWithValue("@IsActive", objApplications.IsActive);

                dbConnectionObj.ConnectionOpen();
                reader = com.ExecuteReader();
                success = true;

                //if (reader.HasRows)
                //    success = true;
                //else
                //    success = false;

                reader.Close();
                dbConnectionObj.ConnectionClose();
                return success; 
            }
            catch(Exception ex)
            {

            }
            finally
            {
                reader = null;
              //  dbConnectionObj = null;
                com = null;
            }

            return success; 
        }

        public bool AddNewApplicationToDb(Applications objApplications)
        {
            bool success = false;
            SqlDataReader reader = null;
            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocInsertApplication, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@ApplicationName", objApplications.ApplicationName);
                com.Parameters.AddWithValue("@Description", objApplications.Description);
                com.Parameters.AddWithValue("@CreateDate", objApplications.CreateDate);
                com.Parameters.AddWithValue("@IsActive", objApplications.IsActive);

                dbConnectionObj.ConnectionOpen();
                reader = com.ExecuteReader();
                success = true;

                reader.Close();
                dbConnectionObj.ConnectionClose();
                return success;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                reader = null;
                //  dbConnectionObj = null;
                com = null;
            }

            return success;
        }


        public bool DeleteApplicationFromDb(Applications objApplications)
        {
            bool success = false;
            SqlDataReader reader = null;
            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocDeleteApplicationById, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@ApplicationId", objApplications.ApplicationId);

                dbConnectionObj.ConnectionOpen();
                reader = com.ExecuteReader();
                success = true;

                reader.Close();
                dbConnectionObj.ConnectionClose();
                return success;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                reader = null;
                //  dbConnectionObj = null;
                com = null;
            }

            return success;
        }
    }
}