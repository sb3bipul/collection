﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace OMSWebApi.Repository
{
    public class UserRepository
    {
        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        DBConnection db2ConnectionObj = null;
        DBConnectionOAuth dbConnectionObj = null;
        SqlCommand SqlCmd = null;
        DataToJson dtToJson = new DataToJson();

        public void GetUserDetails(string brokerId, out int companyId, out string userName)
        {
            companyId = 0;
            userName = string.Empty;

            SqlDataAdapter da = null;
            DataTable dt = null;

            try
            {
               // custList = new List<CustomerDetail>();
                dbConnectionObj = new DBConnectionOAuth();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetUserDetails, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@userId", brokerId);
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                log.Debug("-> Method: GetUserDetails(string brokerId, out int companyId, out string userName) - Connection stablish to OMSWebApi Database successfully");
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

               // companyId = Convert.ToInt32(dt[0][1]);

                foreach(DataRow dr in dt.Rows)
                {
                    companyId = Convert.ToInt32(dr["CompanyID"]);
                    userName = dr["Name"].ToString();
                }


             //   string jsonString;
             //   jsonString = dtToJson.convertDataTableToJson(dt, "GetCustomersDetailList", true);
               // return jsonString;

            }
            catch (Exception ex)
            {
                log.Error("->Method: GetUserDetails(string brokerId, out int companyId, out string userName) " + ex);
            }
            finally
            {
                dbConnectionObj = null;
               // custList = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }

           // return "";
        }

        public string insertUserRegistration(string UserID, Int32 CompanyId, string Name, string Street, string City, string Zip,
           string ContactNo, string Email, string Password, Int32 UserType, string IMEI)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                db2ConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SprocInsertUserRegistration, db2ConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                Guid UserGUID = Guid.NewGuid();
                SqlCmd.Parameters.AddWithValue("@UserID", UserID);
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);
                SqlCmd.Parameters.AddWithValue("@UserGUID", Convert.ToString(UserGUID));
                SqlCmd.Parameters.AddWithValue("@Name", Name);
                SqlCmd.Parameters.AddWithValue("@Street", Street);
                SqlCmd.Parameters.AddWithValue("@City", City);
                SqlCmd.Parameters.AddWithValue("@Zip", Zip);
                SqlCmd.Parameters.AddWithValue("@ContactNo", ContactNo);
                SqlCmd.Parameters.AddWithValue("@EmailID", Email);
                SqlCmd.Parameters.AddWithValue("@Password", Password);
                SqlCmd.Parameters.AddWithValue("@UserType", UserType);
                SqlCmd.Parameters.AddWithValue("@IMEI", IMEI);
                SqlCmd.Parameters.AddWithValue("@Message", "");

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                db2ConnectionObj.ConnectionOpen();
                da.Fill(dt);
                db2ConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "getUserList", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "getUserList", false);
            }
            finally
            {
                db2ConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

    }
}