﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using OMSWebApi.Models;
using System.Xml.Serialization;
using Newtonsoft.Json.Linq;
using System.Data.OleDb;
using System.Web.Configuration;
using OMSWebApi.Controllers;
using OMSWebApi.Providers;
using OMSWebApi.Results;
using OMSWebApi.Repository;
using System.Net.Mail;
using System.Globalization;


namespace OMSWebApi.Repository
{
    public class OrderHistoryRepository
    {
        DBConnection dbConnectionObj = null;
        SqlCommand SqlCmd = null;
        DataToJson dtToJson = new DataToJson();

        /// <summary>
        /// Get Non Pending Order History
        /// </summary>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        public string GetOrderHistory(string brokerId, string customerId, string orderNumber, string status, DateTime fromDate, DateTime toDate, int companyId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcOrderHistory, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@SalesmanID", brokerId);
                SqlCmd.Parameters.AddWithValue("@CustomerID", customerId);
                SqlCmd.Parameters.AddWithValue("@PONumber", orderNumber);
                SqlCmd.Parameters.AddWithValue("@Status", status);
                SqlCmd.Parameters.AddWithValue("@FromDate", fromDate);
                SqlCmd.Parameters.AddWithValue("@ToDate", toDate);
                SqlCmd.Parameters.AddWithValue("@CompanyID", companyId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetOrderHistory", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetOrderHistory", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }


        /// <summary>
        /// Get All Pending Order History By Customer
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="customerId"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public string GetPendingOrderHistoryByCustomer(string brokerId, string customerId, DateTime fromDate, DateTime toDate, int companyId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcPendingOrderHistoryByCustomer, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@SalesmanID", brokerId);
                SqlCmd.Parameters.AddWithValue("@CustomerID", customerId);
                SqlCmd.Parameters.AddWithValue("@FromDate", fromDate);
                SqlCmd.Parameters.AddWithValue("@ToDate", toDate);
                SqlCmd.Parameters.AddWithValue("@CompanyID", companyId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetPendingOrderHistoryByCustomer", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetPendingOrderHistoryByCustomer", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }


        /// <summary>
        /// Get All Order Status
        /// </summary>
        /// <returns></returns>
        public string GetAllOrderStatus()
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetAllOrderStatus, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetAllOrderStatus", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetAllOrderStatus", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }


        /// <summary>
        /// Delete Order 
        /// </summary>
        /// <param name="basketId"></param>
        /// <param name="OrderNumber"></param>
        /// <param name="BrokerId"></param>
        /// <returns></returns>
        public string DeleteOrder(string basketId, string orderNumber, string brokerId, int message)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcDeleteOrders, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@BasketID", basketId);
                SqlCmd.Parameters.AddWithValue("@PONumber", orderNumber);
                SqlCmd.Parameters.AddWithValue("@SalesmanId", brokerId);
                SqlCmd.Parameters.AddWithValue("@MESSAGE", message);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "DeleteOrder", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "DeleteOrder", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }


        // Get Order Preview
        public string GetOrderPreview(string basketId, int companyID, string action)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetOrderPreview, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@BasketID", basketId);
                SqlCmd.Parameters.AddWithValue("@CompanyID", companyID);
                SqlCmd.Parameters.AddWithValue("@Action", action);


                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetOrderPreview", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetOrderPreview", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }



        // Get Order Preview from DB2
        public string GetOrderPreviewDB2(string basketId, string companyID, string action)
        {
            SqlDataAdapter da = null;
            DataTable dt = new DataTable();
            string jsonString;
            try
            {
                OleDbConnection con = new OleDbConnection(ApiConstant.apiConnectionStringDB2);
                OleDbCommand DB2cmd = new OleDbCommand(ApiConstant.SProcGetOrderPreviewDB2.ToString(), con);

                DB2cmd.CommandType = CommandType.StoredProcedure;
                DB2cmd.Connection = con;

                DB2cmd.Parameters.AddWithValue("@ORDERID", basketId);
                DB2cmd.Parameters.AddWithValue("@COMPANYID", companyID);
                //  SqlCmd.Parameters.AddWithValue("@Action", action);


                con.Open();

                var dr = DB2cmd.ExecuteReader(CommandBehavior.CloseConnection);

                dt.Load(dr);
                string newColumnName;

                // Change column name coming from DB2 which have double quotes in all column name
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    newColumnName = dt.Columns[i].ColumnName.ToString();
                    dt.Columns[i].ColumnName = newColumnName.Replace("\"", "");
                    dt.AcceptChanges();
                }

                con.Close();

                jsonString = dtToJson.convertDataTableToJson(dt, "GetOrderPreviewDB2", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetOrderPreview", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }


        // Get order count by BROKER ID
        public string GetOrderCountByBroker(DateTime FromDate, DateTime ToDate, string GoyaCompanyID)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetOrderCountByBroker, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@FromDate", FromDate);
                SqlCmd.Parameters.AddWithValue("@ToDate", ToDate);
                SqlCmd.Parameters.AddWithValue("@CompanyID", GoyaCompanyID);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetOrderPreview", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetOrderPreview", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }


        // Send Order Email
        public string SendOrderByEmail(string BrokerID, string EOR, string EmailTo, string EmailBody, string AttachmentPath, string FileType)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                UserAuthorizationRepository userRepObj = null;
                bool ifEmailExist = false;
                string regEmail = string.Empty;
                var jsonResponseObj = "[{}]";
                string body = string.Empty;

                userRepObj = new UserAuthorizationRepository();
                ifEmailExist = userRepObj.IfUserValidEmailOAuthDB(BrokerID, EmailTo);

                // string from = WebConfigurationManager.AppSettings["SMTPServerEmail"];
                string from = WebConfigurationManager.AppSettings["FromEmail"];
                string to = EmailTo;
                //   string cc = WebConfigurationManager.AppSettings["CCedEmail"];
                string subject = "Order # " + EOR;
                string bodyStr1 = EmailBody;
                //string bodyStr1 = "Please confirm your email address to set your password and redirect to ";

                //  string redirectOMSurl = WebConfigurationManager.AppSettings["RedirectToOMSfromEmailURL"] + model.UserName + "&fogetPwdFlag=true";
                string supportEmail = WebConfigurationManager.AppSettings["SupportEmail"];
                string supportPhone = WebConfigurationManager.AppSettings["SupportPhone"];


                if (true)
                {
                    UserController userControllerObj = new UserController();
                    AccountController accountController = new AccountController();
                    LoginUser objUser = new LoginUser();
                    objUser.UserId = "FORGET_BROKER_TOKEN";
                    //    objUser.Password = "Pwd1@forget_broker_token";
                    //  var resStr = await userControllerObj.GetUserToken("FORGET_BROKER_TOKEN", "Pwd1@forget_broker_token");

                    //  string tokerStr = ((System.Web.Http.Results.OkNegotiatedContentResult<string>)(resStr)).Content;
                    // redirectOMSurl += "&access_token=" + tokerStr;

                    //  body = bodyStr1 + "<a href='" + redirectOMSurl + "'> link </a> </br>";
                    // body += "For assistance please email to " + supportEmail + " OR call to " + supportPhone;

                    bool isEmailSent = accountController.SendEmailWithAttachment(from, to, subject, EmailBody, "", AttachmentPath, EOR);
                }
                else
                {
                    userRepObj = null;
                    userRepObj = new UserAuthorizationRepository();


                    //  body = bodyStr1 + "<a href='" + redirectOMSurl + "'> link </a> </br>";
                    //  body += "For assistance please email to " + supportEmail + " OR call to " + supportPhone;

                    body = "Sorry, your email does not match with registered email, for assistance please email to " + supportEmail + " OR call to " + supportPhone;
                }



                jsonString = dtToJson.convertDataTableToJson(dt, "GetOrderPreview", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetOrderPreview", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        // Get Order History from AS400 DB2
        public string GetOrderHistoryDB2(Order objOrderHistory)
        {
            SqlDataAdapter da = null;
            DataTable dt = new DataTable();
            string jsonString;
            try
            {
                DataTable dtOpenOrdres;
                dtOpenOrdres = getOpenOrders(objOrderHistory);

                OleDbConnection con = new OleDbConnection(ApiConstant.apiConnectionStringDB2);
                OleDbCommand DB2cmd = new OleDbCommand(ApiConstant.SProcGetOrderHistoryDB2.ToString(), con);

                DB2cmd.CommandType = CommandType.StoredProcedure;
                DB2cmd.Connection = con;

                string FromDate = objOrderHistory.FromDate.ToString("yyyy-MM-dd");
                string ToDate = objOrderHistory.ToDate.ToString("yyyy-MM-dd");



                DB2cmd.Parameters.AddWithValue("@SALESMANID", objOrderHistory.BrokerId);
                DB2cmd.Parameters.AddWithValue("@CUSTOMERID", objOrderHistory.CustomerId);
                DB2cmd.Parameters.AddWithValue("@PONUMBER", objOrderHistory.OrderNumber);
                DB2cmd.Parameters.AddWithValue("@STATUS", objOrderHistory.Status);
                DB2cmd.Parameters.AddWithValue("@FROMDATE", FromDate);
                DB2cmd.Parameters.AddWithValue("@TODATE", ToDate);
                DB2cmd.Parameters.AddWithValue("@COMPANYID", objOrderHistory.CompanyId.ToString());
                //  SqlCmd.Parameters.AddWithValue("@Action", action);


                con.Open();

                var dr = DB2cmd.ExecuteReader(CommandBehavior.CloseConnection);

                dt.Load(dr);
                string newColumnName;

                // Change column name coming from DB2 which have double quotes in all column name
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    newColumnName = dt.Columns[i].ColumnName.ToString();
                    dt.Columns[i].ColumnName = newColumnName.Replace("\"", "");
                    dt.AcceptChanges();
                }

                con.Close();
                DataTable dtAllOrders;
                dtAllOrders = dtOpenOrdres.Copy();
                dtAllOrders.Merge(dt,true,MissingSchemaAction.Ignore);

               

                DataView dv = dtAllOrders.DefaultView;
                dv.Sort = "OrderDate desc";
                DataTable sortedDT = dv.ToTable();

                //for (int j = 0; j < sortedDT.Rows.Count; j++)
                //{
                //    DateTime b = Convert.ToDateTime(sortedDT.Rows[j]["OrderDate"]);
                //    string a = b.ToShortDateString();
                //    sortedDT.Rows[j].SetField("OrderDate",a );
                //}

                jsonString = dtToJson.convertDataTableToJson(sortedDT, "GetOrderHistoryDB2", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetOrderHistoryDB2", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        // Get Open Orders from OMS
        public DataTable getOpenOrders(Order objOrderHistory)
        {
            SqlDataAdapter da = null;
            DataTable dt = new DataTable();

            dbConnectionObj = new DBConnection();
            SqlCmd = new SqlCommand(ApiConstant.SProcOrderHistory, dbConnectionObj.ApiConnection);
            SqlCmd.CommandType = CommandType.StoredProcedure;

            SqlCmd.Parameters.AddWithValue("@SALESMANID", objOrderHistory.BrokerId);
            SqlCmd.Parameters.AddWithValue("@CUSTOMERID", objOrderHistory.CustomerId);
            SqlCmd.Parameters.AddWithValue("@PONUMBER", objOrderHistory.OrderNumber);
            SqlCmd.Parameters.AddWithValue("@STATUS", objOrderHistory.Status);
            SqlCmd.Parameters.AddWithValue("@FROMDATE", objOrderHistory.FromDate);
            SqlCmd.Parameters.AddWithValue("@TODATE", objOrderHistory.ToDate);
            SqlCmd.Parameters.AddWithValue("@COMPANYID", objOrderHistory.CompanyId);

            da = new SqlDataAdapter(SqlCmd);
            dt = new DataTable();
            dbConnectionObj.ConnectionOpen();
            da.Fill(dt);
            return dt;

        }

        /// <summary>
        /// Get Submitted Order 
        /// </summary>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        public string GetSubmittedOrderData(string brokerId, int companyId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcSubmittedOrder, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@SALESMANID", brokerId);
                SqlCmd.Parameters.AddWithValue("@COMPANYID", companyId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetSubmittedOrderData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetSubmittedOrderData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// Get Saved Order 
        /// </summary>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        public string GetSavedCartOrderData(string brokerId, int companyId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            { 
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcSavedCartOrder, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@SALESMANID", brokerId);
                SqlCmd.Parameters.AddWithValue("@COMPANYID", companyId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string 
                if (dt.Rows.Count > 0)
                    jsonString = dtToJson.convertDataTableToJson(dt, "GetSavedCartOrderData", true);
                else
                {
                    DataTable dtl = new DataTable();
                    dtl.Columns.Add("SuccessMessage", typeof(string));
                    dtl.Columns.Add("Message", typeof(string));
                    dtl.Rows.Add("Saved Order Item Does Not Exists", "");
                    jsonString = dtToJson.convertDataTableToJson(dtl, "GetSavedCartOrderData", false);
                    dtl = null;
                }
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetSavedCartOrderData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string deleteOpenOrderData(string CustomerId, string POno, string SalesmanId, int CompanyId)
        {
            SqlDataAdapter da = null; 
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcDeleteOpenOrders, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CustomerId", CustomerId);
                SqlCmd.Parameters.AddWithValue("@PONumber", POno);
                SqlCmd.Parameters.AddWithValue("@SalesmanId", SalesmanId);
                SqlCmd.Parameters.AddWithValue("@CompanyId", CompanyId);
                SqlCmd.Parameters.AddWithValue("@MESSAGE", 1);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string 
                jsonString = dtToJson.convertDataTableToJson(dt, "deleteOpenOrderData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "deleteOpenOrderData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string GetSubmittedOrderDetailData(string OrderNo, int companyId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetSubmittedOrdersDetail, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@OrderNumber", OrderNo);
                SqlCmd.Parameters.AddWithValue("@CompanyID", companyId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetSubmittedOrderDetailData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetSubmittedOrderDetailData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }


        public string GetSavedCartOrderDetailData(string PONumber, int companyId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetSavedCartOrdersDetail, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@PONumber", PONumber);
                SqlCmd.Parameters.AddWithValue("@CompanyID", companyId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetSavedCartOrderDetailData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetSavedCartOrderDetailData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// Get Order By status action
        /// </summary>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        public string GetOrderByStatusData(string brokerId, int companyId, String StatusMode)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            DataTable dtl = new DataTable();
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetAllOrderByStatus, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@SALESMANID", brokerId);
                SqlCmd.Parameters.AddWithValue("@COMPANYID", companyId);
                SqlCmd.Parameters.AddWithValue("@ActionID", StatusMode);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();
                // Convert Datatable to JSON string
                if (dt.Rows.Count >= 1)
                    jsonString = dtToJson.convertDataTableToJson(dt, "GetOrderByStatusData", true);
                else
                {
                    //dtl.Columns.Add("Msg", typeof(String));
                    //dtl.Rows.Add("Order Does Not Exist.");
                    //jsonString = dtToJson.convertDataTableToJson(dtl, "GetOrderByStatusData", true);
                    CustProfileRepository objCustProfl = new CustProfileRepository();
                    jsonString = objCustProfl.convertDataTableToJsonWithoutPayload(dtl, "GetOrderByStatusData", false);
                    objCustProfl = null;
                }
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetOrderByStatusData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// Get Order Details By Status
        /// </summary>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        public string GetOrderDetailByStatusData(string OrderNo, int companyId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetOrdersDetailByStatus, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@OrderNumber", OrderNo);
                SqlCmd.Parameters.AddWithValue("@CompanyID", companyId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetOrderDetailByStatusData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetOrderDetailByStatusData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// Get HIGH / LOW credit Customer list 
        /// </summary>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        public string GetCreditCustomerData(string brokerId, string companyId, string action)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetCreditCustomer, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@SALESMANID", brokerId);
                SqlCmd.Parameters.AddWithValue("@COMPANYID", companyId);
                SqlCmd.Parameters.AddWithValue("@Action", action);
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetCreditCustomerData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetCreditCustomerData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// Get Top Selling Customer list 
        /// </summary>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        public string GetTopSellingCustomerData(string BrokerId, string companyId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetTopSellingCustomer, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@SALESMANID", BrokerId);
                SqlCmd.Parameters.AddWithValue("@COMPANYID", companyId);
                
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetTopSellingCustomerData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetTopSellingCustomerData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// Get Submitted Order 
        /// </summary>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        public string GetOrderHeaderDataByStatus(string brokerId, string companyId, String OrderStatus)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetOrderHeaderByStatus, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@SALESMANID", brokerId);
                SqlCmd.Parameters.AddWithValue("@COMPANYID", companyId);
                SqlCmd.Parameters.AddWithValue("@STATUS", OrderStatus);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                if (dt.Rows.Count > 0)
                    jsonString = dtToJson.convertDataTableToJson(dt, "GetOrderHeaderDataByStatus", true);
                else
                {
                    DataTable dtl = new DataTable();
                    //dtl.Columns.Add("SuccessMessage", typeof(string));
                    //dtl.Columns.Add("Message", typeof(string));
                    //dtl.Rows.Add("Order Does Not Exists", "");
                    jsonString = dtToJson.convertDataTableToJson(dtl, "GetOrderHeaderDataByStatus", false);
                    dtl = null;
                }

                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetOrderHeaderDataByStatus", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// Get Submitted Order 
        /// </summary>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        public string GetCustomerToVisitData(string CompanyID, string BrokerId, string BillingCO)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetCustomerToVisit, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", CompanyID);
                SqlCmd.Parameters.AddWithValue("@BillingCO", BillingCO);
                SqlCmd.Parameters.AddWithValue("@SalesmanId", BrokerId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                if (dt.Rows.Count > 0)
                    jsonString = dtToJson.convertDataTableToJson(dt, "GetCustomerToVisitData", true);
                else
                {
                    DataTable dtl = new DataTable();
                    dtl.Columns.Add("SuccessMessage", typeof(string));
                    dtl.Columns.Add("Message", typeof(string));
                    dtl.Rows.Add("Record Does Not Exists", "");
                    jsonString = dtToJson.convertDataTableToJson(dtl, "GetCustomerToVisitData", false);
                    dtl = null;
                }

                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetCustomerToVisitData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// Get Notification 
        /// </summary>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        public string GetNotificationData(string brokerId, int companyId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            String jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetNotification, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@SALESMANID", brokerId);
                SqlCmd.Parameters.AddWithValue("@COMPANYID", companyId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                if (dt.Rows.Count > 0)
                    jsonString = dtToJson.convertDataTableToJson(dt, "GetNotificationData", true);
                else
                {
                    DataTable dtl = new DataTable();
                    dtl.Columns.Add("SuccessMessage", typeof(string));
                    dtl.Columns.Add("Message", typeof(string));
                    dtl.Rows.Add("Record Does Not Exists", "");
                    jsonString = dtToJson.convertDataTableToJson(dtl, "GetNotificationData", false);
                    dtl = null;
                }

                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetNotificationData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string UpdateNotificationGetDetailData(string OrderNo, string companyId)
        {
            //SqlDataAdapter da = null;
            DataTable dt = new DataTable();
            string jsonString;
            try
            {
                //dbConnectionObj = new DBConnection();
                //SqlCmd = new SqlCommand(ApiConstant.SPUpdateNotificationGetDetail, dbConnectionObj.ApiConnection);
                //SqlCmd.CommandType = CommandType.StoredProcedure;

                //SqlCmd.Parameters.AddWithValue("@OrderNumber", OrderNo);
                //SqlCmd.Parameters.AddWithValue("@CompanyID", companyId);

                //da = new SqlDataAdapter(SqlCmd);
                //dt = new DataTable();
                //dbConnectionObj.ConnectionOpen();
                //da.Fill(dt);
                //dbConnectionObj.ConnectionClose();

                AdoHelper adoHelper = new AdoHelper();
                dt = adoHelper.ExecDataTableProc(ApiConstant.SPUpdateNotificationGetDetail, new object[] { "@OrderNumber", OrderNo, "@CompanyID", companyId });
                
                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "UpdateNotificationGetOrderDetail", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "UpdateNotificationGetOrderDetail", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                //da = null;
                dt = null;
            }
            return null;
        }

        public string DeleteNotificationData(NotificationModel objNotifyModel)
        {
            DataTable dt = new DataTable();
            string jsonString;
            try
            {
                IEnumerable<PayLoadNotification> notifyOrderLst = objNotifyModel.payLoadNotifications;
                DataTable dtlOrdNotify = new DataTable();
                dtlOrdNotify.Columns.Add("OrderNo", typeof(string));
                dtlOrdNotify.Columns.Add("CompanyID", typeof(string));

                foreach (var itms in notifyOrderLst)
                {
                    dtlOrdNotify.Rows.Add(itms.OrderNo, objNotifyModel.CompanyID);
                }

                NotificationModel notifyOrderModel = new NotificationModel();
                notifyOrderModel.payLoadNotifications = objNotifyModel.payLoadNotifications;

                DataSet dsNotify = new DataSet();
                dsNotify.Tables.Add(dtlOrdNotify);
                String NotifyOrderList = dsNotify.GetXml();

                AdoHelper adoHelper = new AdoHelper();
                dt = adoHelper.ExecDataTableProc(ApiConstant.SProcOMS_DeleteNotification, new object[] { "@XML_OrderDtl", NotifyOrderList });

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "DeleteNotificationData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "DeleteNotificationData", false);
            }
            finally
            {
                dt = null;
            }
            return null;
        }

        public string GetAllOrdersData(string OrderNo, string companyId)
        {
            DataTable dt = new DataTable();
            string jsonString;
            try
            {
                AdoHelper adoHelper = new AdoHelper();
                dt = adoHelper.ExecDataTableProc(ApiConstant.SProcGetAllOrders, new object[] { "@SALESMANID", OrderNo, "@COMPANYID", companyId });

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetAllOrdersData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetAllOrdersData", false);
            }
            finally
            {
                dt = null;
            }
            return null;
        }

        public string GetErrorOrderData(String BrokerId, String companyId)
        {
            DataTable dt = null;
            string jsonString;
            try
            {
                AdoHelper adoHelper = new AdoHelper();
                dt = adoHelper.ExecDataTableProc(ApiConstant.SProcGetErrorOrder, new object[] { "@SALESMANID", BrokerId, "@COMPANYID", companyId });

                // Convert Datatable to JSON string 
                if (dt.Rows.Count > 0)
                    jsonString = dtToJson.convertDataTableToJson(dt, "GetErrorOrderData", true);
                else
                {
                    DataTable dtl = new DataTable();
                    //dtl.Columns.Add("SuccessMessage", typeof(string));
                    //dtl.Columns.Add("Message", typeof(string));
                    //dtl.Rows.Add("Error Order Does Not Exists", "");
                    jsonString = dtToJson.convertDataTableToJson(dtl, "GetErrorOrderData", false);
                    dtl = null;
                }
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetErrorOrderData", false);
            }
            finally
            {
                dt = null;
            }
            return null;
        }

        public string GetErrorOrderDetails(String companyId, String PONumberTmp)
        {
            DataTable dt = null;
            string jsonString;
            try
            {
                AdoHelper adoHelper = new AdoHelper();
                dt = adoHelper.ExecDataTableProc(ApiConstant.SProcGetErrorOrderDetail, new object[] { "@COMPANYID", companyId, "@PONumberTmp", PONumberTmp });

                // Convert Datatable to JSON string 
                if (dt.Rows.Count > 0)
                    jsonString = dtToJson.convertDataTableToJson(dt, "GetErrorOrderDetails", true);
                else
                {
                    DataTable dtl = new DataTable();
                    dtl.Columns.Add("SuccessMessage", typeof(string));
                    dtl.Columns.Add("Message", typeof(string));
                    dtl.Rows.Add("Error Order Details Does Not Exists", "");
                    jsonString = dtToJson.convertDataTableToJson(dtl, "GetErrorOrderDetails", false);
                    dtl = null;
                }
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetErrorOrderDetails", false);
            }
            finally
            {
                dt = null;
            }
            return null;
        }



    }


}