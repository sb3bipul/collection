﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace OMSWebApi.Repository
{
    public class SalesCommunicatorRepository
    {
        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        DBConnection dbConnectionObj = null;
        SqlCommand SqlCmd = null;
        DataToJson dtToJson = new DataToJson();


        public string getSalesCommunicatorData(int weekNo)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetSalesCommunicatorData, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@WeekNo", weekNo);
               

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "getSalesCommunicatorData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "getSalesCommunicatorData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        // Insert Sales Communicator Data
        public string insertSalesCommunicatorData(int weekNo, string year,DateTime startDate, DateTime endDate, string pdfFilePath, string fileName,string userID, bool isActive, string comments)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcInsertSalesCommunicator, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                string salesCommunicatorData = WebConfigurationManager.AppSettings["SalesCommunicatorData"];

                SqlCmd.Parameters.AddWithValue("@WeekNo", weekNo);
                SqlCmd.Parameters.AddWithValue("@Year", year);
                SqlCmd.Parameters.AddWithValue("@StartDate", startDate);
                SqlCmd.Parameters.AddWithValue("@EndDate", endDate);
                SqlCmd.Parameters.AddWithValue("@PdfFile", pdfFilePath);
                SqlCmd.Parameters.AddWithValue("@FileName", fileName);
                SqlCmd.Parameters.AddWithValue("@UserID", userID);               
                SqlCmd.Parameters.AddWithValue("@Active", isActive);
                SqlCmd.Parameters.AddWithValue("@Comments", comments);
                SqlCmd.Parameters.AddWithValue("@Message", ""); 
    
                // Create PDF file
                string base64BinaryStr = "";

                byte[] bytes = Convert.FromBase64String(base64BinaryStr);

                System.IO.FileStream stream = new FileStream(salesCommunicatorData + fileName, FileMode.CreateNew);
                System.IO.BinaryWriter writer =
                new BinaryWriter(stream);
                writer.Write(bytes, 0, bytes.Length);
                writer.Close();

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "insertSalesCommunicatorData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "insertSalesCommunicatorData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        // Insert Sales Communicator Data
        public string sendFileData(string fileName, string fileData, string fileType, DateTime sentDate)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                //dbConnectionObj = new DBConnection();
                //SqlCmd = new SqlCommand(ApiConstant.SProcInsertSalesCommunicator, dbConnectionObj.ApiConnection);
               // SqlCmd.CommandType = CommandType.StoredProcedure;
                string salesCommunicatorData = WebConfigurationManager.AppSettings["SalesCommunicatorData"];

                string base64BinaryStr = fileData;

                byte[] bytes = Convert.FromBase64String(base64BinaryStr);

                System.IO.FileStream stream = new FileStream(salesCommunicatorData + fileName, FileMode.Append);
                System.IO.BinaryWriter writer = 
                new BinaryWriter(stream);
                writer.Write(bytes, 0, bytes.Length);
                 writer.Close();

               // SqlCmd.Parameters.AddWithValue("@WeekNo", fileName);
              //  SqlCmd.Parameters.AddWithValue("@Year", fileData);
              //  SqlCmd.Parameters.AddWithValue("@StartDate", fileType);
              //  SqlCmd.Parameters.AddWithValue("@EndDate", sentDate);
       
             //   da = new SqlDataAdapter(SqlCmd);
             //   dt = new DataTable();
             //   dbConnectionObj.ConnectionOpen();
             //   da.Fill(dt);
             //   dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
             //   jsonString = dtToJson.convertDataTableToJson(dt, "insertSalesCommunicatorData", true);
              // return jsonString;
                 return "";
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "insertSalesCommunicatorData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

       // Edit Sales Communicator Data
        public string editSalesCommunicatorData(int WeekNo,string Year, DateTime StartDate,DateTime EndDate, String UserID, bool IsActive, string Comments,int Srl)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
               dbConnectionObj = new DBConnection();
               SqlCmd = new SqlCommand(ApiConstant.SProcEditSalesCommunicator, dbConnectionObj.ApiConnection);
               SqlCmd.CommandType = CommandType.StoredProcedure;

               SqlCmd.Parameters.AddWithValue("@WeekNo", WeekNo);
               SqlCmd.Parameters.AddWithValue("@Year", Year);
               SqlCmd.Parameters.AddWithValue("@StartDate", StartDate);
               SqlCmd.Parameters.AddWithValue("@EndDate", EndDate);
               SqlCmd.Parameters.AddWithValue("@UserID", UserID);
               SqlCmd.Parameters.AddWithValue("@Active", IsActive);
               SqlCmd.Parameters.AddWithValue("@Comments", Comments);
               SqlCmd.Parameters.AddWithValue("@Srl", Srl);
               SqlCmd.Parameters.AddWithValue("@Message", "");

                   da = new SqlDataAdapter(SqlCmd);
                   dt = new DataTable();
                   dbConnectionObj.ConnectionOpen();
                   da.Fill(dt);
                   dbConnectionObj.ConnectionClose();

                 //Convert Datatable to JSON string
                   jsonString = dtToJson.convertDataTableToJson(dt, "editSalesCommunicatorData", true);
                 return jsonString;
               
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "editSalesCommunicatorData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        // Delete Sales Communicator (Soft Delete)
        public string deleteSalesCommunicatorData(int Srl,string UserID)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcDeleteSalesCommunicator, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@Srl", Srl);
                SqlCmd.Parameters.AddWithValue("@UserID", UserID);
                SqlCmd.Parameters.AddWithValue("@Message", "");

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                //Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "deleteSalesCommunicatorData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "deleteSalesCommunicatorData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }
    }
}