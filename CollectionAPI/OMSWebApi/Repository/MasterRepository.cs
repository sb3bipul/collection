﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using OMSWebApi.Models;
using System.Xml.Serialization;
using Newtonsoft.Json.Linq;
using System.Data.OleDb;
using OMSWebApi.Repository;
using System.Xml;
using System.Text;
using System.Web.Script.Serialization;
using System.IO;
using Newtonsoft.Json;

namespace OMSWebApi.Repository
{
    public class MasterRepository
    {
        DBConnection dbConnectionObj = null;
        SqlCommand SqlCmd = null;
        DataToJson dtToJson = new DataToJson();
        static DataSet objds = new DataSet();
        public static int FileIndex = 0;
        static string strFileIndex = string.Empty;
        static string strEDIFileType = string.Empty;
        static string strEDIFileName = string.Empty;
        String strPONumber = string.Empty;
        public static double totalAmount = 0;
        public static int noLineItem = 0;
        DBConnectionOAuth dbAuthConnectionObj = null;
        DB2Connection conn = null;
        DB2_Connection con = null;
        SqlCommand DB2cmd = null;


        public string getItemList(string companyId, string languageId, string BillingCO)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetProductData, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", companyId);
                SqlCmd.Parameters.AddWithValue("@LanguageId", languageId);
                SqlCmd.Parameters.AddWithValue("@CatalogId", "0");
                SqlCmd.Parameters.AddWithValue("@WarehouseId", BillingCO);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                //jsonString = dtToJson.convertRegulartDataToJson(dt);
                jsonString = dtToJson.convertDataTableToJson(dt, "SProcGetProductData   ", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertRegulartDataToJson(dt);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }



        public string getItemListGoya(string companyId, string languageId, string BillingCo)
        {
            string jsonString;
            DataTable dtl = new DataTable();
            try
            {
                con = new DB2_Connection();
                var dtReturn = con.ExecDataSetProc(ApiConstant.SProcGoyaGetProductData, "@CompanyId", BillingCo, "@LanguageId", languageId);
                dtl = dtReturn.Tables[0];

                ////OleDbConnection con = new OleDbConnection(ApiConstant.apiConnectionStringDB2);
                ////OleDbCommand DB2cmd = new OleDbCommand(ApiConstant.SProcGetQuantityOnHand.ToString(), con);
                ////DB2cmd.Connection = con;
                //DB2cmd.Parameters.AddWithValue("@CompanyId", companyId);
                //DB2cmd.Parameters.AddWithValue("@LanguageId", languageId);
                //DB2cmd.Parameters.AddWithValue("@WarehouseId", warehouseId);

                ////con.Open();
                ////var dr = DB2cmd.ExecuteReader(CommandBehavior.CloseConnection);
                ////dt1.Load(dr);
                ////con.Close();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dtl, "SProcGoyaGetProductData", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertRegulartDataToJson(dtl);
            }
            finally
            {
                SqlCmd = null;
                con = null;
            }
            return null;
        }

        public string getCategoryList(string companyId, string languageId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetCategory, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", companyId);
                SqlCmd.Parameters.AddWithValue("@LanguageId", languageId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                //jsonString = dtToJson.convertRegulartDataToJson(dt);
                jsonString = dtToJson.convertDataTableToJson(dt, "SProcGetCategory   ", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertRegulartDataToJson(dt);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string getCategoryListGoya(string companyId, string languageId)
        {
            DataTable dtl = null;
            string jsonString;
            try
            {
                con = new DB2_Connection();
                var dtReturn = con.ExecDataSetProc(ApiConstant.SProcGetCategoryGoya);
                dtl = dtReturn.Tables[0];

                ////OleDbConnection con = new OleDbConnection(ApiConstant.apiConnectionStringDB2);
                ////OleDbCommand DB2cmd = new OleDbCommand(ApiConstant.SPocCustomersDetailListDB2.ToString(), con);
                //DB2cmd.CommandType = CommandType.StoredProcedure;
                ////DB2cmd.Connection = con;

                ////con.Open();
                ////var dr = DB2cmd.ExecuteReader(CommandBehavior.CloseConnection);
                ////dt1.Load(dr);
                ////con.Close();

                jsonString = dtToJson.convertDataTableToJson(dtl, "getCategoryListGoya", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertRegulartDataToJson(dtl);
            }
            finally
            {
                con = null;
                dtl = null;
            }
            return null;
        }


        public string getSubCategoryList(string companyId, string languageId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetSubCategory, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", companyId);
                SqlCmd.Parameters.AddWithValue("@LanguageId", languageId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                //jsonString = dtToJson.convertRegulartDataToJson(dt);
                jsonString = dtToJson.convertDataTableToJson(dt, "SProcGetSubCategory   ", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertRegulartDataToJson(dt);
            }
            finally
            {
                con = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string getSubCategoryListGoya(string companyId, string languageId)
        {
            DataTable dtl = null;
            string jsonString;
            //List<CustomerLst> custLst;
            try
            {
                con = new DB2_Connection();
                var dtReturn = con.ExecDataSetProc(ApiConstant.SProcGetSubCategoryGoya);
                dtl = dtReturn.Tables[0];

                ////OleDbConnection con = new OleDbConnection(ApiConstant.apiConnectionStringDB2);
                ////OleDbCommand DB2cmd = new OleDbCommand(ApiConstant.SPocCustomersDetailListDB2.ToString(), con);
                //DB2cmd.CommandType = CommandType.StoredProcedure;
                ////DB2cmd.Connection = con;

                ////con.Open();
                ////var dr = DB2cmd.ExecuteReader(CommandBehavior.CloseConnection);
                ////dt1.Load(dr);
                ////con.Close();

                jsonString = dtToJson.convertDataTableToJson(dtl, "getSubCategoryListGoya", true);
                return jsonString;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                //dbConnectionObj = null;
                con = null;
                dtl = null;
            }
            return null;
        }

        public string getCompanyByIDData(string companyId)
        {
            SqlDataAdapter da = null;
            string jsonString;
            DataTable dtl = new DataTable();
            try
            {
                //////OleDbConnection con = new OleDbConnection(ApiConstant.apiConnectionStringDB2);
                //////OleDbCommand DB2cmd = new OleDbCommand(ApiConstant.SProcGetQuantityOnHand.ToString(), con);

                //////DB2cmd.Connection = con;

                ////DB2cmd.Parameters.AddWithValue("@CompanyId", companyId);

                //////con.Open();
                //////var dr = DB2cmd.ExecuteReader(CommandBehavior.CloseConnection);
                //////dt1.Load(dr);
                //////con.Close();

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetCompanyByID, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", companyId);

                // new by kous 
                da = new SqlDataAdapter(SqlCmd);
                dbConnectionObj.ConnectionOpen();
                da.Fill(dtl);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dtl, "getCompanyData", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertRegulartDataToJson(dtl);
            }
            finally
            {
                dbConnectionObj = null;
                da = null;
                dtl = null;
            }
            return null;
        }

        public String getCompanyName(string CompanyId)
        {
            String ClientName = String.Empty;
            if (CompanyId == "1")
                ClientName = ApiConstant.getClientFolsom.ToString();
            else if (CompanyId == "2")
                ClientName = ApiConstant.getClientName.ToString();
            else
                ClientName = ApiConstant.getClientOther.ToString();
            return ClientName;
        }

        public string GetCustomersDetailList_DB2(String BrokerId)
        {
            DataTable dtl = null;
            string jsonString;
            //List<CustomerLst> custLst;
            try
            {
                String brokerId = BrokerId.Substring(2, 4);
                con = new DB2_Connection();
                var dtReturn = con.ExecDataSetProc(ApiConstant.SPocCustomersListDB2, "@COMPANYID", BrokerId.Substring(0, 2), "@BROKERID", brokerId);
                dtl = dtReturn.Tables[0];

                ////OleDbConnection con = new OleDbConnection(ApiConstant.apiConnectionStringDB2);
                ////OleDbCommand DB2cmd = new OleDbCommand(ApiConstant.SPocCustomersDetailListDB2.ToString(), con);
                ////DB2cmd.Connection = con;
                ////DB2cmd.Parameters.AddWithValue("@COMPANYID", BrokerId.Substring(0, 2));
                ////DB2cmd.Parameters.AddWithValue("@BROKERID", brokerId);

                ////con.Open();
                ////var dr = DB2cmd.ExecuteReader(CommandBehavior.CloseConnection);
                ////dt1.Load(dr);
                ////con.Close();

                //custLst = new List<CustomerLst>();
                //if (dtl != null)
                //{
                //    if (dtl.Rows.Count > 0)
                //    {
                //        for (int i = 0; i < dtl.Rows.Count; i++)
                //        {
                //            custLst.Add(new CustomerLst
                //            {
                //                UserID = Convert.ToString(dtl.Rows[i]["CustomerId"]),
                //                CustomerName = Convert.ToString(dtl.Rows[i]["Customer_Name"]),
                //                Address = Convert.ToString(dtl.Rows[i]["ADDRESS"]),
                //                Street = Convert.ToString(dtl.Rows[i]["STREET"]),
                //                City = Convert.ToString(dtl.Rows[i]["CITY"]),
                //                State = Convert.ToString(dtl.Rows[i]["STATE"]),
                //                Zip = Convert.ToString(dtl.Rows[i]["ZIP"]),
                //                BrokerID = Convert.ToString(dtl.Rows[i]["BrokerID"]),
                //                BillingCo = Convert.ToString(dtl.Rows[i]["BillingCo"]),
                //                Status = Convert.ToString(dtl.Rows[i]["Status"]),
                //                ContactNo = Convert.ToString(dtl.Rows[i]["CONTACTNO"]),
                //                CreditStatus = Convert.ToString(dtl.Rows[i]["CREDITSTATUS"]),
                //                CustomerBalance = Convert.ToString(dtl.Rows[i]["CUSTOMERBALANCE"]),
                //                CUSTOMERBALANCEOVER30 = Convert.ToString(dtl.Rows[i]["CUSTOMERBALANCEOVER30"]),
                //                WarehouseID = Convert.ToString(dtl.Rows[i]["WarehouseID"])
                //            });
                //        }
                //    }
                //}
                //dtl.Dispose();
                //return custLst;

                jsonString = dtToJson.convertDataTableToJson(dtl, "GetCustomersDetailList   ", true);
                return jsonString;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                //DB2cmd = null;
                con = null;
                dtl = null;
            }
            return null;
        }

        public string GetBrokerList_DB2(string CompanyId)
        {
            DataTable dtl = null;
            string jsonString;
            //List<CustomerLst> custLst;
            try
            {
                con = new DB2_Connection();
                var dtReturn = con.ExecDataSetProc(ApiConstant.SProcOMSBrokerList, "@CompanyId", Convert.ToString(CompanyId));
                dtl = dtReturn.Tables[0];

                ////OleDbConnection con = new OleDbConnection(ApiConstant.apiConnectionStringDB2);
                ////OleDbCommand DB2cmd = new OleDbCommand(ApiConstant.SPocCustomersDetailListDB2.ToString(), con);
                ////DB2cmd.Connection = con;
                ////DB2cmd.Parameters.AddWithValue("@CompanyId", Convert.ToString(CompanyId));

                ////con.Open();
                ////var dr = DB2cmd.ExecuteReader(CommandBehavior.CloseConnection);
                ////dt1.Load(dr);
                ////con.Close();

                jsonString = dtToJson.convertDataTableToJson(dtl, "GetBrokerList", true);
                return jsonString;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                //DB2cmd = null;
                con = null;
                dtl = null;
            }
            return null;
        }

        public string getBrokerListByComp(string CompanyId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcOMSBrokerListByComp, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", CompanyId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                jsonString = dtToJson.convertDataTableToJson(dt, "SProcGetBrokerList", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertRegulartDataToJson(dt);
            }
            finally
            {
                con = null;
                da = null;
                dt = null;
            }
            return null;
        }


        public List<CustomerLst> GetCustomersDetailListBySalesmanId(String BrokerId)
        {
            SqlDataAdapter da = null;
            DataTable dtl = null;
            List<CustomerLst> custLst;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetCustomerListBySalesmanId, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@SalesmanId", BrokerId);

                // new by kous 
                da = new SqlDataAdapter(SqlCmd);
                dtl = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dtl);
                dbConnectionObj.ConnectionClose();


                custLst = new List<CustomerLst>();
                if (dtl != null)
                {
                    if (dtl.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtl.Rows.Count; i++)
                        {
                            custLst.Add(new CustomerLst
                            {
                                UserID = Convert.ToString(dtl.Rows[i]["USERID"]),
                                CustomerName = Convert.ToString(dtl.Rows[i]["NAME"]),
                                Address = Convert.ToString(dtl.Rows[i]["ADDRESS"]),
                                //Region = Convert.ToString(dtl.Rows[i]["REGION"]),
                                BillingCo = Convert.ToString(dtl.Rows[i]["COMPANYID"]),
                                //WHCompany = Convert.ToString(dtl.Rows[i]["WHCOMPANY"]),
                                //CompanyName = Convert.ToString(dtl.Rows[i]["COMPANYNAME"]),
                                //WHName = Convert.ToString(dtl.Rows[i]["WHNAME"]),
                                ContactNo = Convert.ToString(dtl.Rows[i]["CONTACTNO"]),
                                Street = Convert.ToString(dtl.Rows[i]["STREET"]),
                                City = Convert.ToString(dtl.Rows[i]["CITY"]),
                                State = Convert.ToString(dtl.Rows[i]["STATE"]),
                                Zip = Convert.ToString(dtl.Rows[i]["ZIP"]),
                                Status = Convert.ToString(dtl.Rows[i]["STATUS"]),
                                CreditStatus = Convert.ToString(dtl.Rows[i]["CREDITSTATUS"]),
                                CustomerBalance = Convert.ToString(dtl.Rows[i]["CUSTOMERBALANCE"])
                            });
                        }
                    }
                }
                dtl.Dispose();
                return custLst;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dtl = null;
            }
            return null;
        }


        public string getCustomerBySalesmanId(string CompanyId, String SalesmanId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcCustomerListByBroker, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);
                SqlCmd.Parameters.AddWithValue("@SalesmanId", SalesmanId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                jsonString = dtToJson.convertDataTableToJson(dt, "SProcGetCustomerList", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertRegulartDataToJson(dt);
            }
            finally
            {
                con = null;
                da = null;
                dt = null;
            }
            return null;
        }


        public string GetWarehouseList_DB2(string CompanyId)
        {
            DataTable dtl = null;
            string jsonString;
            //List<CustomerLst> custLst;
            try
            {
                con = new DB2_Connection();
                var dtReturn = con.ExecDataSetProc(ApiConstant.SProcOMSWarehouseList,"@CompanyId", Convert.ToString(CompanyId));
                dtl = dtReturn.Tables[0];

                ////OleDbConnection con = new OleDbConnection(ApiConstant.apiConnectionStringDB2);
                ////OleDbCommand DB2cmd = new OleDbCommand(ApiConstant.SPocCustomersDetailListDB2.ToString(), con);
                ////DB2cmd.Connection = con;

                ////con.Open();
                ////var dr = DB2cmd.ExecuteReader(CommandBehavior.CloseConnection);
                ////dt1.Load(dr);
                ////con.Close();

                jsonString = dtToJson.convertDataTableToJson(dtl, "GetWarehouseList", true);
                return jsonString;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                //DB2cmd = null;
                con = null;
                dtl = null;
            }
            return null;
        }

        public string getWarehouseByComp(string CompanyId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcOMSWarehouseListByComp, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", CompanyId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                jsonString = dtToJson.convertDataTableToJson(dt, "SProcGetWarehouseList", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertRegulartDataToJson(dt);
            }
            finally
            {
                con = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public DataTable getVendorListData(string CompanyId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            String jsonString;
            List<Brand> brandListObj = null;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcOMSVendorList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", Convert.ToString(CompanyId));

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();
                brandListObj = new List<Brand>();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            brandListObj.Add(new Brand
                            {
                                VendorCode = Convert.ToString(dt.Rows[i]["VendorCode"]),
                                Name = Convert.ToString(dt.Rows[i]["Name"]),
                                ImageName = Convert.ToString(dt.Rows[i]["ImageName"]),
                                CompanyID = Convert.ToString(dt.Rows[i]["CompanyID"])
                            });
                        }
                    }
                    else
                    {
                        brandListObj.Add(new Brand
                        {
                            VendorCode = "",
                            Name = "",
                            ImageName ="",
                            CompanyID = Convert.ToString(CompanyId)
                        });
                    }
                }
                OMSWebApi.Models.CompanyModels.ListtoDataTable lsttodt = new OMSWebApi.Models.CompanyModels.ListtoDataTable();
                dt = lsttodt.ToDataTable(brandListObj);

                // Convert Datatable to JSON string
                //jsonString = dtToJson.convertRegulartDataToJson(dt);
                //jsonString = dtToJson.convertDataTableToJson(dt, "getVendorListData", true);
                //return jsonString;
                return dt;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertRegulartDataToJson(dt);
            }
            finally
            {
                dbConnectionObj = null;
                brandListObj = null;
                SqlCmd = null;
                da = null;
                //dt = null;
            }
            return null;
        }

        public DataTable getVendorGroupListData(string CompanyId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            String jsonString;
            List<VendorGroup> brandGrpObj = null;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_OMS_VendorGroupList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", Convert.ToString(CompanyId));

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();
                brandGrpObj = new List<VendorGroup>();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            brandGrpObj.Add(new VendorGroup
                            {
                                VendorId = Convert.ToString(dt.Rows[i]["VendorId"]),
                                VendorGroupId = Convert.ToString(dt.Rows[i]["VendorGroupId"]),
                                GroupDescription = Convert.ToString(dt.Rows[i]["GroupDescription"]),
                                CompanyID = Convert.ToString(dt.Rows[i]["CompanyID"])
                            });
                        }
                    }
                    else
                    {
                        brandGrpObj.Add(new VendorGroup
                        {
                            VendorId = "",
                            VendorGroupId = "",
                            GroupDescription = "",
                            CompanyID = Convert.ToString(CompanyId)
                        });
                    }
                }
                OMSWebApi.Models.CompanyModels.ListtoDataTable lsttodt = new OMSWebApi.Models.CompanyModels.ListtoDataTable();
                dt = lsttodt.ToDataTable(brandGrpObj);

                return dt;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertRegulartDataToJson(dt);
            }
            finally
            {
                dbConnectionObj = null;
                brandGrpObj = null;
                SqlCmd = null;
                da = null;

            }
            return null;
        }

        public string getCountryData(String CompanyID)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            String jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetCountryList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "getCountryData", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertRegulartDataToJson(dt);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string getBrandData()
        {
            DataTable dtl = null;
            SqlDataAdapter da = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcOMSBrandList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                da = new SqlDataAdapter(SqlCmd);
                dtl = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dtl);
                dbConnectionObj.ConnectionClose();

                ////OleDbConnection con = new OleDbConnection(ApiConstant.apiConnectionStringDB2);
                ////OleDbCommand DB2cmd = new OleDbCommand(ApiConstant.SPocCustomersDetailListDB2.ToString(), con);
                ////DB2cmd.Connection = con;
                //DB2cmd.Parameters.AddWithValue("@CompanyId", Convert.ToString(CompanyId));

                ////con.Open();
                ////var dr = DB2cmd.ExecuteReader(CommandBehavior.CloseConnection);
                ////dt1.Load(dr);
                ////con.Close();

                jsonString = dtToJson.convertDataTableToJson(dtl, "getBrandData", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertRegulartDataToJson(dtl);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dtl = null;
            }
            return null;
        }

        public string getOrderStatusData()
        {
            SqlDataAdapter da = null;
            DataTable dtl = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcOMSOrderStatusist, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                da = new SqlDataAdapter(SqlCmd);
                dtl = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dtl);
                dbConnectionObj.ConnectionClose();

                //con = new DB2_Connection();
                //var dtReturn = con.ExecDataSetProc(ApiConstant.SProcOMSOrderStatusist);
                //dtl = dtReturn.Tables[0];

                ////OleDbConnection con = new OleDbConnection(ApiConstant.apiConnectionStringDB2);
                ////OleDbCommand DB2cmd = new OleDbCommand(ApiConstant.SPocCustomersDetailListDB2.ToString(), con);
                ////DB2cmd.Connection = con;

                //DB2cmd.Parameters.AddWithValue("@CompanyId", Convert.ToString(CompanyId));

                ////con.Open();
                ////var dr = DB2cmd.ExecuteReader(CommandBehavior.CloseConnection);
                ////dt1.Load(dr);
                ////con.Close();

                jsonString = dtToJson.convertDataTableToJson(dtl, "getOrderStatusData", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertRegulartDataToJson(dtl);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dtl = null;
            }
            return null;
        }

        public string getStockData(string CompanyId)
        {
            SqlDataAdapter da = null;
            DataTable dtl = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcOMSProductSrockList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", CompanyId);

                da = new SqlDataAdapter(SqlCmd);
                dtl = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dtl);
                dbConnectionObj.ConnectionClose();

                //con = new DB2_Connection(); 
                //var dtReturn = con.ExecDataSetProc(ApiConstant.SProcOMSProductSrockList, "@CompanyId", CompanyId);
                //dtl = dtReturn.Tables[0];
                ////OleDbConnection con = new OleDbConnection(ApiConstant.apiConnectionStringDB2);
                ////OleDbCommand DB2cmd = new OleDbCommand(ApiConstant.SPocCustomersDetailListDB2.ToString(), con);
                ////DB2cmd.Connection = con;

                //DB2cmd.Parameters.AddWithValue("@CompanyId", Convert.ToString(CompanyId));
                ////con.Open();
                ////var dr = DB2cmd.ExecuteReader(CommandBehavior.CloseConnection);
                ////dt1.Load(dr);
                ////con.Close();

                jsonString = dtToJson.convertDataTableToJson(dtl, "getStockData", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertRegulartDataToJson(dtl);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dtl = null;
            }
            return null;
        }


        public string getStockDataByComp(String CompanyId, String LanguageId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcOMSProductSrockListByComp, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", CompanyId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                jsonString = dtToJson.convertDataTableToJson(dt, "SProcGetStockList", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertRegulartDataToJson(dt);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }


        public string getProductPromoPriceData(string CompanyId, String SalesmanId)
        {
            SqlDataAdapter da = null;
            DataTable dtl = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_OMS_GetProductPromoPriceGoya, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", CompanyId);
                SqlCmd.Parameters.AddWithValue("@BrokerId", SalesmanId);

                da = new SqlDataAdapter(SqlCmd);
                dtl = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dtl);
                dbConnectionObj.ConnectionClose();

                //con = new DB2_Connection();
                //var dtReturn = con.ExecDataSetProc(ApiConstant.SProc_OMS_GetProductPromoPriceGoya, "@CompanyId", CompanyId, "@BrokerId", SalesmanId);
                //dtl = dtReturn.Tables[0];

                ////OleDbConnection con = new OleDbConnection(ApiConstant.apiConnectionStringDB2);
                ////OleDbCommand DB2cmd = new OleDbCommand(ApiConstant.SPocCustomersDetailListDB2.ToString(), con);
                ////DB2cmd.Connection = con;

                //DB2cmd.Parameters.AddWithValue("@CompanyId", Convert.ToString(CompanyId));
                ////con.Open();
                ////var dr = DB2cmd.ExecuteReader(CommandBehavior.CloseConnection);
                ////dt1.Load(dr);
                ////con.Close();

                jsonString = dtToJson.convertDataTableToJson(dtl, "getProductPromoPrice", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertRegulartDataToJson(dtl);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dtl = null;
            }
            return null;
        }

        public string addOrderItemData(OrderViewModel objOrderChat)
        {
            SqlDataAdapter da = null;
            DataTable dt = null, dtlOrder;
            String jsonString;
            String CustID = "", SalesID = String.Empty, CompID = String.Empty, On_Off = String.Empty, OrdVerify = string.Empty, TmpOrderNo = string.Empty, OrderCreateFlag = string.Empty, OrderDate = string.Empty;
            decimal Lat = 0, Long = 0;
            //Int32 IsConfirmByErr = 0;
            CreateOrderRepository crtOrder = new CreateOrderRepository();
            try
            {
                IEnumerable<PayLoadItem> ordItemLst = objOrderChat.payLoadItems;
                DataTable dtlOrd = new DataTable();
                dtlOrd.Columns.Add("ItemCode", typeof(string));
                dtlOrd.Columns.Add("ItemName", typeof(string));
                dtlOrd.Columns.Add("Qty", typeof(Int32));
                dtlOrd.Columns.Add("ItemPrice", typeof(decimal));
                dtlOrd.Columns.Add("ItemStatus", typeof(string));
                //dtlOrd.Columns.Add("ItemSrl", typeof(Int32));

                foreach (var itms in ordItemLst)
                {
                    dtlOrd.Rows.Add(itms.ItemCode, itms.ItemName, itms.Qty, itms.ItemPrice, itms.ItemStatus);//, itms.ItemSrl
                    if (String.IsNullOrWhiteSpace(CustID))
                    {
                        CustID = itms.CustomerID;
                        SalesID = itms.SalesmanID;
                        CompID = itms.CompanyID;
                        On_Off = itms.OnOffline;
                        Lat = itms.Latitude;
                        Long = itms.Longitude;
                        OrdVerify = itms.OrderVerify;
                        TmpOrderNo = itms.OrderId;
                        OrderCreateFlag = itms.OrderCreateFlag;
                        OrderDate = itms.OrderDate;
                        //IsConfirmByErr = itms.IsConfirmByError;
                    }
                }

                OrderViewModel orderModel = new OrderViewModel();
                orderModel.payLoadItems = objOrderChat.payLoadItems;

                DataSet dsOrder = new DataSet();
                dsOrder.Tables.Add(dtlOrd);
                String OrderItms = dsOrder.GetXml();

                //// Saving Text file before creating the order
                //dtlOrder = crtOrder.GetOrderDataByCustomerID(SalesID, CustID, CompID, "", On_Off, OrdVerify, OrderItms);
                crtOrder.CreateTXTFileFromOrderItems(SalesID, CustID, CompID, dtlOrd);

                String success = string.Empty;
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcOMSSubmitQuickOrderByChatMsg, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.CommandTimeout = 0;
                SqlCmd.Parameters.AddWithValue("@CustomerId", CustID);
                SqlCmd.Parameters.AddWithValue("@SalesmanId", SalesID);
                SqlCmd.Parameters.AddWithValue("@CompanyId", CompID);
                SqlCmd.Parameters.AddWithValue("@OnOffline", On_Off);
                SqlCmd.Parameters.AddWithValue("@Latitude", Lat);
                SqlCmd.Parameters.AddWithValue("@Longitude", Long);
                SqlCmd.Parameters.AddWithValue("@OrderVerify", OrdVerify);
                SqlCmd.Parameters.AddWithValue("@TmpOrderNo", TmpOrderNo);
                SqlCmd.Parameters.AddWithValue("@CreationFlag", OrderCreateFlag);
                SqlCmd.Parameters.AddWithValue("@OrderDate", OrderDate);
                //SqlCmd.Parameters.AddWithValue("@IsConfirmByError", IsConfirmByErr);
                SqlCmd.Parameters.AddWithValue("@XML_OrderDtl", OrderItms);
                //SqlCmd.Parameters.AddWithValue("@MESSAGE", 1);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                if (dt.Rows.Count > 0)
                {
                    SqlCmd = null;
                    String POnum = Convert.ToString(dt.Rows[0]["Column1"]);
                    //String POnum = Convert.ToString(dt.Rows[0]["PONumber"]);
                    dbConnectionObj = new DBConnection();
                    SqlCmd = new SqlCommand(ApiConstant.SProcGetQuickOrderDetails, dbConnectionObj.ApiConnection);
                    SqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlCmd.Parameters.AddWithValue("@CompanyID", CompID);
                    SqlCmd.Parameters.AddWithValue("@POnumber", POnum);
                    da = new SqlDataAdapter(SqlCmd);
                    dt = new DataTable();
                    dbConnectionObj.ConnectionOpen();
                    da.Fill(dt);
                    dbConnectionObj.ConnectionClose();

                    try
                    {
                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                //String message = (string)SqlCmd.Parameters["@MESSAGE"].Value;
                                //dbConnectionObj = new DBConnection();
                                //SqlCmd = new SqlCommand(ApiConstant.SPGetOrdersByPO, dbConnectionObj.ApiConnection);
                                //SqlCmd.CommandType = CommandType.StoredProcedure;
                                //SqlCmd.Parameters.AddWithValue("@PONumber", message);
                                //SqlCmd.Parameters.AddWithValue("@CompanyId", CompID);
                                //da = new SqlDataAdapter(SqlCmd); dbConnectionObj.ConnectionOpen();da.Fill(objds);dbConnectionObj.ConnectionClose();
                                String StoredFileType = Convert.ToString(dt.Rows[0]["StoredFileType"]);

                                if (StoredFileType == "EDI")
                                {
                                    Console.WriteLine("Start Creating EDI File For Order : " + strPONumber);
                                    DateTime DeliveryDate = DateTime.Today.AddDays(7);
                                    crtOrder.CreateEDIFileFromPurchaseOrder(strPONumber, "", "", DeliveryDate.ToShortDateString(), CustID);
                                    Console.WriteLine("Finish Creating EDI File For Order : " + strPONumber);
                                }
                                if (StoredFileType == "XML")
                                {
                                    String strFilePath = String.Empty;
                                    strFilePath = crtOrder.CreateXMLFile(strPONumber, Convert.ToString(objds.Tables[0].Rows[0]["CustomerName"]));
                                    if (strFilePath != "")
                                    {
                                        Console.WriteLine("1. Start Writing File Header Details");
                                        //objds.WriteXml(strFilePath);
                                        List<OrderFiles> ordObj = new List<OrderFiles>();
                                        DataTable dtl = new DataTable();
                                        dtl = objds.Tables[0];                            //ordObj = DataTableToList(dtl);
                                        XmlDocument doc = new XmlDocument();
                                        String ords = string.Empty;
                                        ords = "<CustomerId>" + Convert.ToString(dtl.Rows[0]["CustomerID"]) + "</CustomerId><BrokerId>" + Convert.ToString(dtl.Rows[0]["SalesmanID"]) + "</BrokerId><CompanyId>" + Convert.ToString(dtl.Rows[0]["CompanyID"]) + "</CompanyId><OrderNumber>" + Convert.ToString(dtl.Rows[0]["PONumber"]) + "</OrderNumber><OrderDate>" + Convert.ToString(dtl.Rows[0]["OrderDate"]) + "</OrderDate><DeliveryDate>" + Convert.ToString(dtl.Rows[0]["DeliveryDate"]) + "</DeliveryDate><CancelDate>" + Convert.ToString(dtl.Rows[0]["CancelDate"]) + "</CancelDate><CustomerName>" + Convert.ToString(dtl.Rows[0]["CustomerName"]).Trim() + "</CustomerName><TotalValue>" + Convert.ToString(dtl.Rows[0]["Total_Value"]) + "</TotalValue><TotalOrderQty>" + Convert.ToString(dtl.Rows[0]["TotalOrderQty"]) + "</TotalOrderQty><WhId>" + Convert.ToString(dtl.Rows[0]["WHCompany"]) + "</WhId><BasketID>" + Convert.ToString(dtl.Rows[0]["BasketID"]) + "</BasketID>";
                                        var items = new StringBuilder();
                                        for (int i = 0; i < dtl.Rows.Count; i++)
                                        {
                                            items.Append("<ItemCode>" + Convert.ToString(dtl.Rows[i]["ProductCode"]) + "</ItemCode><Qty>" + Convert.ToString(dtl.Rows[i]["Quantity"]) + "</Qty><CasePrice>" + Convert.ToString(dtl.Rows[i]["CasePrice"]) + "</CasePrice>");
                                        }
                                        doc.LoadXml(("<Order type='regular' Section='B'>" + ords + items + "</Order>"));
                                        doc.Save(strFilePath);
                                        Console.WriteLine("1. Finish Writing File Header Details");
                                        dtl.Dispose();
                                    }
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    { }

                    // Convert Datatable to JSON string 
                    //if (Convert.ToString(dt.Rows[0]["ValidItemCount"]) == "0" && IsConfirmByErr == 1)
                    //{
                    //    DataTable dtl = new DataTable();
                    //    dtl.Columns.Add("SuccessMessage", typeof(string));
                    //    dtl.Columns.Add("Message", typeof(string));
                    //    dtl.Rows.Add("None Of The Item Is Valid.", "");
                    //    jsonString = dtToJson.convertDataTableToJson(dtl, "addOrderItemData", false);
                    //    dtl.Dispose();
                    //}
                    //else
                        jsonString = dtToJson.convertDataTableToJson(dt, "addOrderItemData", true);
                    return jsonString;
                }
                else
                {
                    jsonString = dtToJson.convertDataTableToJson(dt, "addOrderItemData", false);
                    return jsonString;
                }
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "addOrderItemData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
                crtOrder = null;
            }
            return null;
        }

        public string submiBypasstOrderData(OrderViewModel objOrderChat)
        {
            SqlDataAdapter da = null;
            DataTable dt = null, dtlOrder;
            String jsonString;
            String CustID = "", SalesID = String.Empty, CompID = String.Empty, On_Off = String.Empty, OrdVerify = string.Empty, TmpOrderNo = string.Empty, OrderDate = string.Empty, OrderCreateFlag = string.Empty;
            decimal Lat = 0, Long = 0;
            CreateOrderRepository crtOrder = new CreateOrderRepository();
            try
            {
                IEnumerable<PayLoadItem> ordItemLst = objOrderChat.payLoadItems;
                DataTable dtlOrd = new DataTable();
                dtlOrd.Columns.Add("ItemCode", typeof(string));
                dtlOrd.Columns.Add("ItemName", typeof(string));
                dtlOrd.Columns.Add("Qty", typeof(Int32));
                dtlOrd.Columns.Add("ItemPrice", typeof(decimal));
                dtlOrd.Columns.Add("ItemStatus", typeof(string));

                foreach (var itms in ordItemLst)
                {
                    dtlOrd.Rows.Add(itms.ItemCode, itms.ItemName, itms.Qty, itms.ItemPrice, itms.ItemStatus);
                    if (String.IsNullOrWhiteSpace(CustID))
                    {
                        CustID = itms.CustomerID;
                        SalesID = itms.SalesmanID;
                        CompID = itms.CompanyID;
                        On_Off = itms.OnOffline;
                        Lat = itms.Latitude;
                        Long = itms.Longitude;
                        OrdVerify = itms.OrderVerify;
                        TmpOrderNo = itms.OrderId;
                        OrderCreateFlag = itms.OrderCreateFlag;
                        OrderDate = itms.OrderDate;
                    }
                }

                OrderViewModel orderModel = new OrderViewModel();
                orderModel.payLoadItems = objOrderChat.payLoadItems;

                DataSet dsOrder = new DataSet();
                dsOrder.Tables.Add(dtlOrd);
                String OrderItms = dsOrder.GetXml();

                //// Saving Text file before creating the order
                //crtOrder.CreateTXTFileFromOrderItems(SalesID, CustID, CompID, dtlOrd);

                String success = string.Empty;
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcOMS_SubmitBypassOrderByAnyCustomer, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.CommandTimeout = 0;
                SqlCmd.Parameters.AddWithValue("@CustomerId", CustID);
                SqlCmd.Parameters.AddWithValue("@SalesmanId", SalesID);
                SqlCmd.Parameters.AddWithValue("@CompanyId", CompID);
                SqlCmd.Parameters.AddWithValue("@OnOffline", On_Off);
                SqlCmd.Parameters.AddWithValue("@Latitude", Lat);
                SqlCmd.Parameters.AddWithValue("@Longitude", Long);
                SqlCmd.Parameters.AddWithValue("@OrderVerify", OrdVerify);
                SqlCmd.Parameters.AddWithValue("@TmpOrderNo", TmpOrderNo);
                SqlCmd.Parameters.AddWithValue("@CreationFlag", OrderCreateFlag);
                SqlCmd.Parameters.AddWithValue("@OrderDate", OrderDate);
                SqlCmd.Parameters.AddWithValue("@XML_OrderDtl", OrderItms);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                if (dt.Rows.Count > 0)
                {
                    SqlCmd = null;
                    String POnum = Convert.ToString(dt.Rows[0]["Column1"]);
                    //String POnum = Convert.ToString(dt.Rows[0]["PONumber"]);
                    dbConnectionObj = new DBConnection();
                    SqlCmd = new SqlCommand(ApiConstant.SProcGetQuickOrderDetails, dbConnectionObj.ApiConnection);
                    SqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlCmd.Parameters.AddWithValue("@CompanyID", CompID);
                    SqlCmd.Parameters.AddWithValue("@POnumber", POnum);
                    da = new SqlDataAdapter(SqlCmd);
                    dt = new DataTable();
                    dbConnectionObj.ConnectionOpen();
                    da.Fill(dt);
                    dbConnectionObj.ConnectionClose();

                    //try
                    //{
                    //    if (dt != null)
                    //    {
                    //        if (dt.Rows.Count > 0)
                    //        {

                    //String StoredFileType = Convert.ToString(dt.Rows[0]["StoredFileType"]);

                    //if (StoredFileType == "EDI")
                    //{
                    //    Console.WriteLine("Start Creating EDI File For Order : " + strPONumber);
                    //    DateTime DeliveryDate = DateTime.Today.AddDays(7);
                    //    crtOrder.CreateEDIFileFromPurchaseOrder(strPONumber, "", "", DeliveryDate.ToShortDateString(), CustID);
                    //    Console.WriteLine("Finish Creating EDI File For Order : " + strPONumber);
                    //}
                    //if (StoredFileType == "XML")
                    //{
                    //    String strFilePath = String.Empty;
                    //    strFilePath = crtOrder.CreateXMLFile(strPONumber, Convert.ToString(objds.Tables[0].Rows[0]["CustomerName"]));
                    //    if (strFilePath != "")
                    //    {
                    //        Console.WriteLine("1. Start Writing File Header Details");
                    //        //objds.WriteXml(strFilePath);
                    //        List<OrderFiles> ordObj = new List<OrderFiles>();
                    //        DataTable dtl = new DataTable();
                    //        dtl = objds.Tables[0];                            //ordObj = DataTableToList(dtl);
                    //        XmlDocument doc = new XmlDocument();
                    //        String ords = string.Empty;
                    //        ords = "<CustomerId>" + Convert.ToString(dtl.Rows[0]["CustomerID"]) + "</CustomerId><BrokerId>" + Convert.ToString(dtl.Rows[0]["SalesmanID"]) + "</BrokerId><CompanyId>" + Convert.ToString(dtl.Rows[0]["CompanyID"]) + "</CompanyId><OrderNumber>" + Convert.ToString(dtl.Rows[0]["PONumber"]) + "</OrderNumber><OrderDate>" + Convert.ToString(dtl.Rows[0]["OrderDate"]) + "</OrderDate><DeliveryDate>" + Convert.ToString(dtl.Rows[0]["DeliveryDate"]) + "</DeliveryDate><CancelDate>" + Convert.ToString(dtl.Rows[0]["CancelDate"]) + "</CancelDate><CustomerName>" + Convert.ToString(dtl.Rows[0]["CustomerName"]).Trim() + "</CustomerName><TotalValue>" + Convert.ToString(dtl.Rows[0]["Total_Value"]) + "</TotalValue><TotalOrderQty>" + Convert.ToString(dtl.Rows[0]["TotalOrderQty"]) + "</TotalOrderQty><WhId>" + Convert.ToString(dtl.Rows[0]["WHCompany"]) + "</WhId><BasketID>" + Convert.ToString(dtl.Rows[0]["BasketID"]) + "</BasketID>";
                    //        var items = new StringBuilder();
                    //        for (int i = 0; i < dtl.Rows.Count; i++)
                    //        {
                    //            items.Append("<ItemCode>" + Convert.ToString(dtl.Rows[i]["ProductCode"]) + "</ItemCode><Qty>" + Convert.ToString(dtl.Rows[i]["Quantity"]) + "</Qty><CasePrice>" + Convert.ToString(dtl.Rows[i]["CasePrice"]) + "</CasePrice>");
                    //        }
                    //        doc.LoadXml(("<Order type='regular' Section='B'>" + ords + items + "</Order>"));
                    //        doc.Save(strFilePath);
                    //        Console.WriteLine("1. Finish Writing File Header Details");
                    //        dtl.Dispose();
                    //    }
                    //}

                    //FireBase_PushNitification fbPush_Notify = new FireBase_PushNitification();
                    //fbPush_Notify.FB_PushNotification("Order Submitted Successfully.", "Order No. : " + Convert.ToString(dt.Rows[0]["PONumber"]));

                    //        }
                    //    }

                    //}
                    //catch (Exception ex)
                    //{ }

                    // Convert Datatable to JSON string 
                    //if (Convert.ToString(dt.Rows[0]["ValidItemCount"]) == "0" && IsConfirmByErr == 1)
                    //{
                    //    DataTable dtl = new DataTable();
                    //    dtl.Columns.Add("SuccessMessage", typeof(string));
                    //    dtl.Columns.Add("Message", typeof(string));
                    //    dtl.Rows.Add("None Of The Item Is Valid.", "");
                    //    jsonString = dtToJson.convertDataTableToJson(dtl, "addOrderItemData", false);
                    //    dtl.Dispose();
                    //}
                    //else
                    jsonString = dtToJson.convertDataTableToJson(dt, "addOrderItemData", true);
                    return jsonString;
                }
                else
                {
                    jsonString = dtToJson.convertDataTableToJson(dt, "addOrderItemData", false);
                    return jsonString;
                }
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "addOrderItemData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
                crtOrder = null;
            }
            return null;
        }

        public string addOrderItemFData(OrderViewModel objOrderChat)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            String jsonString;
            String CustID = "", SalesID = String.Empty, CompID = String.Empty, On_Off = String.Empty;
            decimal Lat = 0, Long = 0;
            try
            {
                IEnumerable<PayLoadItem> ordItemLst = objOrderChat.payLoadItems;
                DataTable dtlOrd = new DataTable();
                dtlOrd.Columns.Add("ItemCode", typeof(string));
                dtlOrd.Columns.Add("ItemName", typeof(string));
                dtlOrd.Columns.Add("Qty", typeof(Int32));
                dtlOrd.Columns.Add("ItemPrice", typeof(decimal));
                dtlOrd.Columns.Add("ItemStatus", typeof(string));

                foreach (var itms in ordItemLst)
                {
                    dtlOrd.Rows.Add(itms.ItemCode, itms.ItemName, itms.Qty, itms.ItemPrice, itms.ItemStatus);
                    if (String.IsNullOrWhiteSpace(CustID))
                    {
                        CustID = itms.CustomerID;
                        SalesID = itms.SalesmanID;
                        CompID = itms.CompanyID;
                        On_Off = itms.OnOffline;
                        Lat = itms.Latitude;
                        Long = itms.Longitude;
                    }
                }

                OrderViewModel orderModel = new OrderViewModel();
                orderModel.payLoadItems = objOrderChat.payLoadItems;

                DataSet dsOrder = new DataSet();
                dsOrder.Tables.Add(dtlOrd);
                String OrderItms = dsOrder.GetXml();

                String success = string.Empty;
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcOMSSubmitQuickOrderByChatMsgFlsm, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CustomerId", CustID);
                SqlCmd.Parameters.AddWithValue("@SalesmanId", SalesID);
                SqlCmd.Parameters.AddWithValue("@CompanyId", CompID);
                SqlCmd.Parameters.AddWithValue("@OnOffline", On_Off);
                SqlCmd.Parameters.AddWithValue("@Latitude", Lat);
                SqlCmd.Parameters.AddWithValue("@Longitude", Long);
                SqlCmd.Parameters.AddWithValue("@XML_OrderDtl", OrderItms);
                //SqlCmd.Parameters.AddWithValue("@MESSAGE", 1);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                if (dt.Rows.Count > 0)
                {
                    SqlCmd = null;
                    String POnum = Convert.ToString(dt.Rows[0]["Column1"]);
                    dbConnectionObj = new DBConnection();
                    SqlCmd = new SqlCommand(ApiConstant.SProcGetQuickOrderDetails, dbConnectionObj.ApiConnection);
                    SqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlCmd.Parameters.AddWithValue("@CompanyID", CompID);
                    SqlCmd.Parameters.AddWithValue("@POnumber", POnum);
                    da = new SqlDataAdapter(SqlCmd);
                    dt = new DataTable();
                    dbConnectionObj.ConnectionOpen();
                    da.Fill(dt);
                    dbConnectionObj.ConnectionClose();
                }
                // Convert Datatable to JSON string 
                jsonString = dtToJson.convertDataTableToJson(dt, "addOrderItemData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "addOrderItemData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string getPriceListGoya(string companyId, string billingId, string languageId)
        {
            string jsonString;
            DataTable dtl = new DataTable();
            try
            {
                con = new DB2_Connection();
                var dtReturn = con.ExecDataSetProc(ApiConstant.SPGoya_PriceList, "@CompanyId", companyId, "@BillingCo", billingId, "@LanguageId", languageId);
                dtl = dtReturn.Tables[0];

                ////OleDbConnection con = new OleDbConnection(ApiConstant.apiConnectionStringDB2);
                ////OleDbCommand DB2cmd = new OleDbCommand(ApiConstant.SProcGetQuantityOnHand.ToString(), con);
                ////DB2cmd.Connection = con;
                //DB2cmd.Parameters.AddWithValue("@CompanyId", companyId);
                //DB2cmd.Parameters.AddWithValue("@LanguageId", languageId);
                //DB2cmd.Parameters.AddWithValue("@WarehouseId", warehouseId);

                ////con.Open();
                ////var dr = DB2cmd.ExecuteReader(CommandBehavior.CloseConnection);
                ////dt1.Load(dr);
                ////con.Close();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dtl, "getPriceListGoya", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertRegulartDataToJson(dtl);
            }
            finally
            {
                SqlCmd = null;
                con = null;
            }
            return null;
        }

        public string getPriceList(string companyId, String SalesmanID)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetPriceList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyID", companyId);
                SqlCmd.Parameters.AddWithValue("@SalesmanID", SalesmanID);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                //jsonString = dtToJson.convertRegulartDataToJson(dt);
                //jsonString = dtToJson.convertDataTableToJson(dt, "getPriceList   ", true);
                jsonString = convertPriceDataTableToJson(dt, "getPriceList   ", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertRegulartDataToJson(dt);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string getPrice_Data(string CompanyId, String SalesmanId)
        {
            string PrcData = string.Empty;
            SqlDataAdapter da = null;
            DataTable dtl = null;
            DataTable dt = new DataTable();
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetPriceList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);
                SqlCmd.Parameters.AddWithValue("@SalesmanID", SalesmanId);

                da = new SqlDataAdapter(SqlCmd);
                dtl = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dtl);
                dbConnectionObj.ConnectionClose();
                PrcData = DataTableToJSONWithJSONNet(dtl);

                StringBuilder sb = new StringBuilder();
                sb.Append("{");
                sb.Append("\"Domain\":" + "\"getPriceList\",");
                sb.Append("\"Payload\":");
                sb.Append(PrcData);
                //Replace("\"", "")
                sb.Append("}");
                dtl.Dispose();
                return sb.ToString();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dtl = null;
            }
            return null;
        }

        public string DataTableToJSONWithJSONNet(DataTable table)
        {
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(table);
            return JSONString;
        }  

        /// <summary>
        /// Get And Update Broker Details 
        /// </summary>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        public string GetUpdateBrokerData(BrokerDetails objBrokerData)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbAuthConnectionObj = new DBConnectionOAuth();
                SqlCmd = new SqlCommand(ApiConstant.SPGetUpdateBrokerDetails, dbAuthConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@BrokerId", objBrokerData.BrokerId);
                SqlCmd.Parameters.AddWithValue("@CompanyId", objBrokerData.CompanyId);
                SqlCmd.Parameters.AddWithValue("@BillingCO", objBrokerData.BillingCO);
                SqlCmd.Parameters.AddWithValue("@FullName", objBrokerData.FullName);
                SqlCmd.Parameters.AddWithValue("@Email", objBrokerData.Email);
                SqlCmd.Parameters.AddWithValue("@Phone", objBrokerData.Phone);
                SqlCmd.Parameters.AddWithValue("@Street", objBrokerData.Street);
                SqlCmd.Parameters.AddWithValue("@City", objBrokerData.City);
                SqlCmd.Parameters.AddWithValue("@State", objBrokerData.State);
                SqlCmd.Parameters.AddWithValue("@Zip", objBrokerData.Zip);
                SqlCmd.Parameters.AddWithValue("@Country", objBrokerData.Country);
                SqlCmd.Parameters.AddWithValue("@PhotoName", objBrokerData.PhotoName);
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbAuthConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbAuthConnectionObj.ConnectionClose();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        try
                        {   // Image Icon
                            System.Drawing.Image img;
                            String BrokerPhotoPath = ApiConstant.ImageData, BrkrCompPath = String.Empty;                                //String ImgPath = HttpContext.Current.Request.PhysicalApplicationPath + "ImgName\\icon\\";

                            BrkrCompPath = "Vikisha";
                            String ImgPath = BrokerPhotoPath + "\\Brokers\\" + BrkrCompPath+"\\";
                            if (!String.IsNullOrEmpty(objBrokerData.PhotoBinaryData) && !string.IsNullOrEmpty(objBrokerData.PhotoName) && objBrokerData.PhotoName != "~" && objBrokerData.PhotoBinaryData != "~")
                            {
                                string extension = Path.GetExtension(ImgPath + objBrokerData.PhotoName);
                                img = Base64ToImage(objBrokerData.PhotoBinaryData);
                                //if (extension == ".png" || extension == ".gif")
                                //    img = Base64ToImage(objImg.ImageBinaryData.Substring(22));
                                //else
                                //    img = Base64ToImage(objImg.ImageBinaryData.Substring(23));

                                img.Save(ImgPath + objBrokerData.PhotoName, System.Drawing.Imaging.ImageFormat.Png);
                            }

                        }
                        catch (Exception ex)
                        { }
                    }
                }
                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetUpdateBrokerData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetUpdateBrokerData", false);
            }
            finally
            {
                dbAuthConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public String convertPriceDataTableToJson(DataTable dt, string eventName, bool status)
        {

            JavaScriptSerializer jss = new JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            string jsonData;
            JsonData jsData = new JsonData();
            jsData.domain = eventName;  // 1
            jsData.events = eventName;  // 2
            StringBuilder sb = new StringBuilder();

            //foreach (DataRow dr in dt.Rows)
            //{

            //    row = new Dictionary<string, object>();
            //    foreach (DataColumn col in dt.Columns)
            //    {
            //        row.Add(col.ColumnName, dr[col]);
            //    }
            //    rows.Add(row);
            //}
            sb.Append("{");
            sb.Append("\"Domain\":" + "\"" + eventName + "\",");
            sb.Append("\"Event\":" + "\"" + eventName + "\",");
            sb.Append("\"ClientData\":" + "\"test2\",");
            sb.Append("\"Status\":" + "\"" + status + "\",");
            ///////////
            string[] jsonArray = new string[dt.Columns.Count];
            string headString = string.Empty;

            for (int i = 0; i < dt.Columns.Count; i++)
            {
                jsonArray[i] = dt.Columns[i].Caption; // Array for all columns
                headString += "\"" + jsonArray[i] + "\" : \"" + jsonArray[i] + i.ToString() + "%" + "\",";
            }

            if (headString.Length > 0)
                headString = headString.Substring(0, headString.Length - 1);

            sb.Append("\"Payload\":[");

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string tempString = headString;
                    sb.Append("{");

                    // To get each value from the datatable
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        tempString = tempString.Replace(dt.Columns[j] + j.ToString().Replace("\"", "") + "%", dt.Rows[i][j].ToString().Replace("\"", "")).Replace("\\", "/");//.Replace("'", "-")
                    }

                    sb.Append(tempString + "},");
                }
            }
            else
            {
                string tempString = headString;
                sb.Append("{");
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    tempString = tempString.Replace(dt.Columns[j] + j.ToString() + "%", "-");
                }

                sb.Append(tempString + "},");
            }

            sb = new StringBuilder(sb.ToString().Substring(0, sb.ToString().Length - 1));
            sb.Append("]}");
            ////////////////

            //string data = jss.Serialize(rows); //3
            jsData.payload = sb.ToString(); ;

            jsData.clientData = "";  // 4
            jsData.status = "Sucess";

            //  string jsonDataResult = jss.Serialize(jsData);
            // return jsonDataResult;

            //   JavaScriptSerializer json_serializer = new JavaScriptSerializer();


            //   var jObjectData = JObject.Parse(jsonDataResult);
            return sb.ToString();



            // jsonData = jss.Serialize(rows);
            // string finalJsonData = addHeaderToJson(jsonData);

            //return jss.Serialize(rows);                    

        }

        public string saveActivityLogData(ActivityLogViewModel objActivityLog)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            String jsonString;
            String CustID = "", CompID = String.Empty;
            CreateOrderRepository crtOrder = new CreateOrderRepository();
            try
            {
                IEnumerable<ActivityLogItem> ordItemLst = objActivityLog.activityLogItems;
                DataTable dtlAct = new DataTable();
                dtlAct.Columns.Add("UserLoginID", typeof(string));
                dtlAct.Columns.Add("ActivityDate", typeof(string));
                dtlAct.Columns.Add("ActivityDescription", typeof(string));
                dtlAct.Columns.Add("OrderId", typeof(string));
                dtlAct.Columns.Add("CustomerID", typeof(string));
                dtlAct.Columns.Add("ActivityLogType", typeof(string));

                foreach (var itms in ordItemLst)
                {
                    dtlAct.Rows.Add(itms.UserLoginID, itms.ActivityDate, itms.ActivityDescription, itms.OrderId, itms.CustomerID, itms.ActivityLogType);
                    if (String.IsNullOrWhiteSpace(CustID))
                    {
                        CompID = itms.CompanyID;
                    }
                }

                ActivityLogViewModel activityModel = new ActivityLogViewModel();
                activityModel.activityLogItems = objActivityLog.activityLogItems;

                DataSet dsActLog = new DataSet();
                dsActLog.Tables.Add(dtlAct);
                String ActivityItms = dsActLog.GetXml();

                String success = string.Empty;
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcOMS_SaveBrokerActivityLog, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.CommandTimeout = 0;

                SqlCmd.Parameters.AddWithValue("@CompanyID", CompID);
                SqlCmd.Parameters.AddWithValue("@XML_ActivityLogDtl", ActivityItms);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                jsonString = dtToJson.convertDataTableToJson(dt, "saveActivityLogData", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "saveActivityLogData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
                crtOrder = null;
            }
            return null;
        }

        /// <summary>
        /// Get Activity Details 
        /// </summary>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        public string GetActivityLogData(ActivityLog objActivityData)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SPGetBrokerActivityLog, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyID", objActivityData.CompanyId);
                SqlCmd.Parameters.AddWithValue("@ActivityLogTyp", objActivityData.ActivityLogType);
                SqlCmd.Parameters.AddWithValue("@BrokerID", objActivityData.BrokerId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetActivityLogData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetActivityLogData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public System.Drawing.Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
            return image;
        }


    }
}