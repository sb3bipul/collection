﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using OMSWebApi.Models;
using System.Xml.Serialization;
using Newtonsoft.Json.Linq;
using System.Data.OleDb;
using OMSWebApi.Repository;
using System.Xml;
using System.Text;
using System.Web.Script.Serialization;
using System.IO;

namespace OMSWebApi.Repository
{
    public class ServerDataRepository
    {
        DBConnection dbConnectionObj = null;
        SqlCommand SqlCmd = null;
        DataToJson dtToJson = new DataToJson();
        static DataSet objds = new DataSet();
        DBConnectionOAuth dbAuthConnectionObj = null;
        DB2Connection conn = null;
        DB2_Connection con = null;
        SqlCommand DB2cmd = null;

        public List<subCategory> getCategoryMasterList(string companyId, string languageId)
        {
            SqlDataAdapter da = null;
            DataTable dtl = null;
            DataTable dt = new DataTable();
            List<subCategory> catgLst;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetCategory, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", companyId);
                SqlCmd.Parameters.AddWithValue("@LanguageId", languageId);

                da = new SqlDataAdapter(SqlCmd);
                dtl = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dtl);
                dbConnectionObj.ConnectionClose();

                catgLst = new List<subCategory>();
                if (dtl != null)
                {
                    if (dtl.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtl.Rows.Count; i++)
                        {
                            catgLst.Add(new subCategory
                            {
                                CategoryID = Convert.ToString(dtl.Rows[i]["CatID"]),
                                CategoryName = Convert.ToString(dtl.Rows[i]["CategoryName"])
                            });
                        }
                    }
                }
                dtl.Dispose();
                //OMSWebApi.Models.CompanyModels.ListtoDataTable lsttodt = new OMSWebApi.Models.CompanyModels.ListtoDataTable();
                //dt = lsttodt.ToDataTable(mastrLst);
                //return dt;
                return catgLst;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dtl = null;
            }
            return null;
        }

        public List<subCategory> getSubCategoryMasterList(string companyId, string languageId)
        {
            SqlDataAdapter da = null;
            DataTable dtl = null;
            DataTable dt = new DataTable();
            List<subCategory> mastrLst;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetSubCategory, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", companyId);
                SqlCmd.Parameters.AddWithValue("@LanguageId", languageId);

                da = new SqlDataAdapter(SqlCmd);
                dtl = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dtl);
                dbConnectionObj.ConnectionClose();

                mastrLst = new List<subCategory>();
                if (dtl != null)
                {
                    if (dtl.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtl.Rows.Count; i++)
                        {
                            mastrLst.Add(new subCategory
                            {
                                CategoryID = Convert.ToString(dtl.Rows[i]["CatID"]),
                                SubCategoryID = Convert.ToString(dtl.Rows[i]["SubCatID"]),
                                SubCategoryName = Convert.ToString(dtl.Rows[i]["SubCategoryName"])
                            });
                        }
                    }
                }
                dtl.Dispose();
                return mastrLst;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dtl = null;
            }
            return null;
        }

        public List<products> getItemMasterList(string companyId, string languageId, string BillingCO)
        {
            SqlDataAdapter da = null;
            DataTable dtl = null;
            DataTable dt = new DataTable();
            List<products> productsLst;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetProductData, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", companyId);
                SqlCmd.Parameters.AddWithValue("@LanguageId", languageId);
                SqlCmd.Parameters.AddWithValue("@CatalogId", "0");
                SqlCmd.Parameters.AddWithValue("@WarehouseId", BillingCO);

                da = new SqlDataAdapter(SqlCmd);
                dtl = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dtl);
                dbConnectionObj.ConnectionClose();

                productsLst = new List<products>();
                if (dtl != null)
                {
                    if (dtl.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtl.Rows.Count; i++)
                        {
                            productsLst.Add(new products
                            {
                                CompanyId = companyId,
                                WHID = Convert.ToString(dtl.Rows[i]["WHID"]),
                                ProductID = Convert.ToString(dtl.Rows[i]["ProductID"]),
                                DisplayName = Convert.ToString(dtl.Rows[i]["DisplayName"]),
                                ProductDesc = Convert.ToString(dtl.Rows[i]["ProductDesc"]),
                                MFGCode = Convert.ToString(dtl.Rows[i]["MFGCode"]),
                                UPC = Convert.ToString(dtl.Rows[i]["UPC"]),
                                Brand = Convert.ToString(dtl.Rows[i]["Brand"]),
                                PackType = Convert.ToString(dtl.Rows[i]["PackType"]),
                                PackSize = Convert.ToString(dtl.Rows[i]["PackSize"]),
                                HEIGHT = Convert.ToString(dtl.Rows[i]["HEIGHT"]),
                                UNIT_WEIGHT = Convert.ToString(dtl.Rows[i]["UNIT_WEIGHT"]),
                                WIDTH = Convert.ToString(dtl.Rows[i]["WIDTH"]),
                                VENDOR = Convert.ToString(dtl.Rows[i]["VENDOR"]),
                                FreeQty = Convert.ToString(dtl.Rows[i]["FreeQty"]),
                                Cat1_ID = Convert.ToString(dtl.Rows[i]["Cat1_ID"]),
                                Cat2_ID = Convert.ToString(dtl.Rows[i]["Cat2_ID"]),
                                Image_Path = Convert.ToString(dtl.Rows[i]["Image_Path"]),
                                BImageFileName = Convert.ToString(dtl.Rows[i]["BImageFileName"]),
                                ImageFileName = Convert.ToString(dtl.Rows[i]["ImageFileName"]),
                                TaxId = Convert.ToString(dtl.Rows[i]["TaxId"]),
                                TaxableBy = Convert.ToString(dtl.Rows[i]["TaxableBy"]),
                                DiscountPercentage = Convert.ToString(dtl.Rows[i]["DiscountPercentage"]),
                                DiscuntAmount = Convert.ToString(dtl.Rows[i]["DiscuntAmount"])
                            });
                        }
                    }
                }
                dtl.Dispose();
                return productsLst;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dtl = null;
            }
            return null;
        }

        public List<CustomerLst1> getCustomerList(string CompanyId, String SalesmanId)
        {
            SqlDataAdapter da = null;
            DataTable dtl = null;
            DataTable dt = new DataTable();
            List<CustomerLst1> custLst;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcCustomerListByBroker, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);
                SqlCmd.Parameters.AddWithValue("@SalesmanId", SalesmanId);

                da = new SqlDataAdapter(SqlCmd);
                dtl = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dtl);
                dbConnectionObj.ConnectionClose();

                custLst = new List<CustomerLst1>();
                if (dtl != null)
                {
                    if (dtl.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtl.Rows.Count; i++)
                        {
                            custLst.Add(new CustomerLst1
                            {
                                CustomerId = Convert.ToString(dtl.Rows[i]["CustomerId"]),
                                Customer_Name = Convert.ToString(dtl.Rows[i]["Customer_Name"]),
                                CompanyID = Convert.ToString(dtl.Rows[i]["CompanyID"]),
                                BillingCo = Convert.ToString(dtl.Rows[i]["BillingCo"]),
                                BrokerID = Convert.ToString(dtl.Rows[i]["BrokerID"]),
                                ADDRESS = Convert.ToString(dtl.Rows[i]["ADDRESS"]),
                                STREET = Convert.ToString(dtl.Rows[i]["STREET"]),
                                CITY = Convert.ToString(dtl.Rows[i]["CITY"]),
                                ZIP = Convert.ToString(dtl.Rows[i]["ZIP"]),
                                STATE = Convert.ToString(dtl.Rows[i]["STATE"]),
                                COUNTRY = Convert.ToString(dtl.Rows[i]["COUNTRY"]),
                                CONTACTNO = Convert.ToString(dtl.Rows[i]["CONTACTNO"]),
                                EMAILID = Convert.ToString(dtl.Rows[i]["EMAILID"]),
                                STATUS = Convert.ToString(dtl.Rows[i]["STATUS"]),
                                WHSE_CODE = Convert.ToString(dtl.Rows[i]["WHSE_CODE"]),
                                CREDITSTATUS = Convert.ToString(dtl.Rows[i]["CREDITSTATUS"]),
                                BALANCE = Convert.ToString(dtl.Rows[i]["BALANCE"]),
                                CUSTOMERBALANCE = Convert.ToString(dtl.Rows[i]["CUSTOMERBALANCE"]),
                                CUSTOMERBALANCEOVER30 = Convert.ToString(dtl.Rows[i]["CUSTOMERBALANCEOVER30"]),
                                PromoAmount = Convert.ToString(dtl.Rows[i]["PromoAmount"]),
                                TaxGroup = Convert.ToString(dtl.Rows[i]["TaxGroup"]),
                                ContractNo = Convert.ToString(dtl.Rows[i]["ContractNo"]),
                                CreditDescription = Convert.ToString(dtl.Rows[i]["CreditDescription"]),
                                CreditLimit = Convert.ToString(dtl.Rows[i]["CreditLimit"])
                            });
                        }
                    }
                }
                dtl.Dispose();
                return custLst;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dtl = null;
            }
            return null;
        }

        public List<prices> getPriceMasterData(string CompanyId, String SalesmanId)
        {
            SqlDataAdapter da = null;
            DataTable dtl = null;
            DataTable dt = new DataTable();
            List<prices> prcLst;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetPriceList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);
                SqlCmd.Parameters.AddWithValue("@SalesmanID", SalesmanId);

                da = new SqlDataAdapter(SqlCmd);
                dtl = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dtl);
                dbConnectionObj.ConnectionClose();

                prcLst = new List<prices>();
                if (dtl != null)
                {
                    if (dtl.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtl.Rows.Count; i++)
                        {
                            prcLst.Add(new prices
                            {
                                CompanyID = Convert.ToString(dtl.Rows[i]["CompanyID"]),
                                ProductID = Convert.ToString(dtl.Rows[i]["ProductID"]),
                                CasePrice = Convert.ToString(dtl.Rows[i]["CasePrice"]),
                                SuggRetl = Convert.ToString(dtl.Rows[i]["SuggRetl"]),
                                UnitPrice = Convert.ToString(dtl.Rows[i]["UnitPrice"]),
                                UnitCost = Convert.ToString(dtl.Rows[i]["UnitCost"]),
                                By_Qty = Convert.ToString(dtl.Rows[i]["By_Qty"]),
                                PromoMinQty = Convert.ToString(dtl.Rows[i]["PromoMinQty"]),
                                IsPromo = Convert.ToString(dtl.Rows[i]["IsPromo"]),
                                CSBZN = Convert.ToString(dtl.Rows[i]["CSBZN"]),
                                PRSUNT = Convert.ToString(dtl.Rows[i]["PRSUNT"]),
                                PromoPrice = Convert.ToString(dtl.Rows[i]["PromoPrice"])
                            });
                        }
                    }
                }
                dtl.Dispose();
                return prcLst;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dtl = null;
            }
            return null;
        }

        public List<warehouses> getWarehouseMasterList(string companyId)
        {
            SqlDataAdapter da = null;
            DataTable dtl = null;
            DataTable dt = new DataTable();
            List<warehouses> whseLst;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcOMSWarehouseListByComp, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", companyId);

                da = new SqlDataAdapter(SqlCmd);
                dtl = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dtl);
                dbConnectionObj.ConnectionClose();

                whseLst = new List<warehouses>();
                if (dtl != null)
                {
                    if (dtl.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtl.Rows.Count; i++)
                        {
                            whseLst.Add(new warehouses
                            {
                                BILLING_CO = Convert.ToString(dtl.Rows[i]["BILLING_CO"]),
                                Whse_Code = Convert.ToString(dtl.Rows[i]["Whse_Code"]),
                                Whse_Name = Convert.ToString(dtl.Rows[i]["Whse_Name"]),
                                Cust_Whse_Class = Convert.ToString(dtl.Rows[i]["Cust_Whse_Class"]),
                                CompanyID = Convert.ToString(dtl.Rows[i]["CompanyID"])
                            });
                        }
                    }
                }
                dtl.Dispose();
                return whseLst;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dtl = null;
            }
            return null;
        }

        public List<stocks> getStockMasterList(string companyId)
        {
            DataTable dtl = null;
            List<stocks> stkLst;
            try
            {
                con = new DB2_Connection();
                var dtReturn = con.ExecDataSetProc(ApiConstant.SProcOMSProductSrockList, "@CompanyId", companyId);
                dtl = dtReturn.Tables[0];

                stkLst = new List<stocks>();
                if (dtl != null)
                {
                    if (dtl.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtl.Rows.Count; i++)
                        {
                            stkLst.Add(new stocks
                            {
                                CompanyID = Convert.ToString(dtl.Rows[i]["CompanyID"]),
                                BILLCO = Convert.ToString(dtl.Rows[i]["BILLCO"]),
                                ITEM = Convert.ToString(dtl.Rows[i]["ITEM"]),
                                CATEGORY_ID = Convert.ToString(dtl.Rows[i]["CATEGORY_ID"]),
                                SUBCATEGORY_ID = Convert.ToString(dtl.Rows[i]["SUBCATEGORY_ID"]),
                                WHSE_CODE = Convert.ToString(dtl.Rows[i]["WHSE_CODE"]),
                                QOH = Convert.ToString(dtl.Rows[i]["QOH"])
                            });
                        }
                    }
                }
                dtl.Dispose();
                return stkLst;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                con = null;
                dtl = null;
            }
            return null;
        }

        public List<brokers> getBrokerMasterList(string companyId)
        {
            DataTable dtl = null;
            List<brokers> brkrLst;
            try
            {
                con = new DB2_Connection();
                var dtReturn = con.ExecDataSetProc(ApiConstant.SProcOMSBrokerList, "@CompanyId", companyId);
                dtl = dtReturn.Tables[0];

                brkrLst = new List<brokers>();
                if (dtl != null)
                {
                    if (dtl.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtl.Rows.Count; i++)
                        {
                            brkrLst.Add(new brokers
                            {
                                BRROUT = Convert.ToString(dtl.Rows[i]["BRROUT"]),
                                USERNAME = Convert.ToString(dtl.Rows[i]["USERNAME"]),
                                BrokerDivisionCode = Convert.ToString(dtl.Rows[i]["BrokerDivisionCode"]),
                                COMPANY = Convert.ToString(dtl.Rows[i]["COMPANY"]),
                                BrokerGroupCode = Convert.ToString(dtl.Rows[i]["BrokerGroupCode"])
                            });
                        }
                    }
                }
                dtl.Dispose();
                return brkrLst;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                con = null;
                dtl = null;
            }
            return null;
        }







    }

}