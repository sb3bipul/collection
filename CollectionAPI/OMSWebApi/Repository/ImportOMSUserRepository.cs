﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace OMSWebApi.Repository
{
    public class ImportOMSUserRepository
    {
        DBConnectionOAuth dbConnectionObj = null;
        SqlCommand com = null;
        SqlDataAdapter da = null;
        DataTable dt = null;

        public bool ImportOMSUserToOAuthDB()
        {
            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocGetOMSUserForOAuthSync, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;

                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                string userID = string.Empty;
                string userName = string.Empty;


                foreach(DataRow dr in dt.Rows)
                {
                    userID = dr[0].ToString().Trim();
                    userName = dr[1].ToString().Trim();

                    AddUser(userID, userName);
                }

            }
            catch(Exception ex)
            {

            }
            finally
            {

            }

            return true;
        }

        public void AddUser(string userId, string userName)
        {
            string strPWD = "Pwd@"+userId;
            string email = userId + "@"+userId+".com";

            try
            {
                string data = string.Empty;
               // data = "UserName=" + txtUserId.Text.Trim() + "&Password=" + strOAuthPassword + "&Email=" + txtEmailId.Text.Trim();
                data = "UserName=" + userId + "&Password=" + strPWD + "&Email=" + email;
                string apiNewUserUri = ApiConstant.apiNewUserUri;  //WebConfigurationManager.AppSettings["apiNewUserUri"];

                System.Net.HttpWebRequest request;
                request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(apiNewUserUri);
                request.Method = "POST";

                byte[] byteArray = Encoding.UTF8.GetBytes(data);
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = byteArray.Length;

                Stream dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                //  dataStream.Close();
                

                System.Net.HttpWebResponse myHttpWebResponse = (System.Net.HttpWebResponse)request.GetResponse();
                if (myHttpWebResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {

                }
            }
            catch(Exception ex)
            {

            }
            finally
            {

            }

        }

    }
}