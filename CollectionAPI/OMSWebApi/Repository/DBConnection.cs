﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace OMSWebApi.Repository
{
    public class DBConnection
    {
        private SqlConnection _con;
        protected SqlTransaction _trans = null;
        public SqlTransaction Transaction { get { return _trans; } }
        /// <summary>
        /// Create instance of SqlConnection.
        /// </summary>
        public SqlConnection ApiConnection
        {
            get
            {
                _con = new SqlConnection(ApiConstant.apiConnectionString);
                return this._con;
            }
        }

        /// <summary>
        /// Open SQL Connection to connect to Database.
        /// </summary>
        public void ConnectionOpen()
        {
            if (_con.State != ConnectionState.Open)
            {
                _con.Close();
                _con.Open();
            }

        }
        //public DataSet ExecDataSetProc(string qry, params object[] args)
        //{
        //    using (SqlCommand cmd = CreateCommand(qry, CommandType.StoredProcedure, args))
        //    {
        //        SqlDataAdapter adapt = new SqlDataAdapter(cmd);
        //        DataSet ds = new DataSet();
        //        adapt.Fill(ds);
        //        return ds;
        //    }
        //}
        //public SqlCommand CreateCommand(string qry, CommandType type, params object[] args)
        //{
        //    SqlCommand cmd = new SqlCommand(qry, _con);

        //    // Associate with current transaction, if any
        //    if (_trans != null)
        //        cmd.Transaction = _trans;

        //    // Set command type
        //    cmd.CommandType = type;

        //    // Construct SQL parameters
        //    for (int i = 0; i < args.Length; i++)
        //    {
        //        if (args[i] is string && i < (args.Length - 1))
        //        {
        //            SqlParameter parm = new SqlParameter();
        //            parm.ParameterName = (string)args[i];
        //            parm.Value = args[++i];
        //            cmd.Parameters.Add(parm);
        //        }
        //        else if (args[i] is SqlParameter)
        //        {
        //            cmd.Parameters.Add((SqlParameter)args[i]);
        //        }
        //        else throw new ArgumentException("Invalid number or type of arguments supplied");
        //    }
        //    return cmd;
        //}

        /// <summary>
        /// Close SQL Connection from the Database.
        /// </summary>
        public void ConnectionClose()
        {
            if (_con.State == ConnectionState.Open)
            {
                _con.Close();
            }
        }

    }
}