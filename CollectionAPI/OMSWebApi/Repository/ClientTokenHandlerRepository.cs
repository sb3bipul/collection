﻿using Braintree;
using System.Web;

namespace OMSWebApi.Repository
{
    public class ClientTokenHandlerRepository
    {
        public string ProcessRequest(HttpContext context)
        {
            var gateway = new BraintreeGateway
            {
                Environment = Braintree.Environment.SANDBOX,
                MerchantId = "zhbjjhqb85wggwjt",
                PublicKey = "87p92br7g5nk3zp3",
                PrivateKey = "d6953b910908d9ab278acc7aef810356"
            };

            var clientToken = gateway.ClientToken.Generate();
            //var clientToken = gateway.ClientToken.Generate(
            //    new ClientTokenRequest
            //    {
            //        CustomerId = aCustomerId
            //    }
            //);
            return clientToken;
            //context.Response.Write(clientToken);
        }
    }
}