﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using OMSWebApi.Models;
using System.Xml.Serialization;
using Newtonsoft.Json.Linq;
using System.Data.OleDb;
using System.Web.Configuration;
using OMSWebApi.Controllers;
using OMSWebApi.Providers;
using OMSWebApi.Results;
using OMSWebApi.Repository;
using System.Net.Mail;
using System.Reflection;

namespace OMSWebApi.Repository
{
    public class CustomerRepository
    {
        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        DBConnection dbConnectionObj = null;
        DB2Connection con = null;
        SqlCommand SqlCmd = null;
        SqlCommand DB2cmd = null;
        DataToJson dtToJson = new DataToJson();

        /// <summary>
        /// Retunn list of all Customers Detail
        /// </summary>
        /// <returns></returns>
        public string GetCustomersDetailList(string brokerId)
        {
            List<CustomerDetail> custList = null;
            SqlDataAdapter da = null;
            DataTable dt = null;

            try
            {
                custList = new List<CustomerDetail>();
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SPocCustomersDetailList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@SalesmanId", brokerId);
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                string jsonString;
                jsonString = dtToJson.convertDataTableToJson(dt, "GetCustomersDetailList", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                log.Error("-> Method: GetCustomersDetailList(string brokerId) " + ex);
            }
            finally
            {
                dbConnectionObj = null;
                custList = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }


        /// <summary>
        /// Retunn list of all Customers Detail from DB2
        /// </summary>
        /// <returns></returns>
        public string GetCustomersDetailListDB2(string brokerId, string companyId)
        {
            DataTable dt1 = new DataTable();
            SqlDataAdapter da = null;
            try
            {
                con = new DB2Connection();
                DB2cmd = new SqlCommand(ApiConstant.SPocCustomersDetailListDB2, con.ApiConnection);
                ////OleDbConnection con = new OleDbConnection(ApiConstant.apiConnectionStringDB2);
                ////OleDbCommand DB2cmd = new OleDbCommand(ApiConstant.SPocCustomersDetailListDB2.ToString(), con);
                
                brokerId = brokerId.Substring(2, 4);

                DB2cmd.CommandType = CommandType.StoredProcedure;
                ////DB2cmd.Connection = con;

                DB2cmd.Parameters.AddWithValue("@COMPANYID", companyId);
                DB2cmd.Parameters.AddWithValue("@BROKERID", brokerId);

                ////con.Open();
                ////var dr = DB2cmd.ExecuteReader(CommandBehavior.CloseConnection);
                ////dt1.Load(dr);
                ////con.Close();

                // new by kous
                da = new SqlDataAdapter(DB2cmd);
                dt1 = new DataTable();
                con.ConnectionOpen();
                da.Fill(dt1);
                con.ConnectionClose();



                string jsonString;
                jsonString = dtToJson.convertDataTableToJson(dt1, "GetCustomersDetailListDB2", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                log.Error("-> Method: GetCustomersDetailList(string brokerId) " + ex);
            }
            finally
            {
                con = null;
                dt1 = null;
            }
            return null;
        }



        /// <summary>
        /// Get Customer Detail by CustomerId and CompanyId
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public string GetCustomersDetailByCustomerId(string customerId, int? companyId)
        {
            List<CustomerDetail> customerDetail = null;

            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                customerDetail = new List<CustomerDetail>();
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SPocCustomersDetailByCustomerId, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@CustomerID", customerId);
                SqlCmd.Parameters.AddWithValue("@CompanyId", companyId);
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetCustomersDetailByCustomerId", true);
                return jsonString;
            }


            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetCustomersDetailByCustomerId", false);
            }
            finally
            {
                dbConnectionObj = null;
                customerDetail = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;

        }

        /// <summary>
        /// Validate Customer by Customer ID and Company ID
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public string ValidateCustomerByID(string customerId, int companyId)
        {
            List<CustomerDetail> customerDetail = null;

            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                customerDetail = new List<CustomerDetail>();
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcValidateCustomerID, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@CustomerID", customerId);
                SqlCmd.Parameters.AddWithValue("@CompanyId", companyId);
                SqlCmd.Parameters.AddWithValue("@MESSAGE", "");

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "ValidateCustomerByID", true);
                return jsonString;
            }


            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "ValidateCustomerByID", false);
            }
            finally
            {
                dbConnectionObj = null;
                customerDetail = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;

        }




        public string GetCustomersDetailList1(string brokerId)
        {
            List<CustomerDetail> custList = null;
            SqlDataAdapter da = null;
            DataTable dt = null;

            try
            {
                custList = new List<CustomerDetail>();
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SPocCustomersDetailList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@SalesmanId", brokerId);
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                string jsonString;
                jsonString = dtToJson.convertRegulartDataToJson(dt);
                return jsonString;

            }
            catch (Exception ex)
            {
                log.Error("-> Method: GetCustomersDetailList1(string brokerId) " + ex);
            }
            finally
            {
                dbConnectionObj = null;
                custList = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        //  Get Brokers list By Adming Broker ID ending with 01
        public string GetBrokerListByAdminBroker(string brokerId)
        {
            List<CustomerDetail> custList = null;
            SqlDataAdapter da = null;
            DataTable dt = null;

            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetBrokerListByAdminBrokerID, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@BrokerID", brokerId);
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                string jsonString;
                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetBrokerListByAdminBroker", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                log.Error("-> Method: GetCustomersDetailList1(string brokerId) " + ex);
            }
            finally
            {
                dbConnectionObj = null;
                custList = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }


        // Get Customer Details by Customer ID
        public string GetCustomerDetailByCustomerID(string companyId, string customerId, string brokerId)
        {
            List<CustomerDetail> custList = null;
            SqlDataAdapter da = null;
            DataTable dt = null;

            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetCustomerDetailByCustomerID, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyID", companyId);
                SqlCmd.Parameters.AddWithValue("@CustomerId", customerId);
                SqlCmd.Parameters.AddWithValue("@BrokerID", brokerId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                string jsonString;
                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetBrokerListByAdminBroker", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                log.Error("-> Method: GetCustomersDetailList1(string brokerId) " + ex);
            }
            finally
            {
                dbConnectionObj = null;
                custList = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }


        // Get Broker Detail by Broker ID
        public string GetBrokerDetailByBrokerID(string companyId, string brokerId)
        {
            List<CustomerDetail> custList = null;
            SqlDataAdapter da = null;
            DataTable dt = null;

            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetBrokerDetailByBrokerID, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyID", companyId);
                SqlCmd.Parameters.AddWithValue("@BrokerID", brokerId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                string jsonString;
                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetBrokerDetailByBrokerID", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                log.Error("-> Method: GetBrokerDetailByBrokerID" + ex);
            }
            finally
            {
                dbConnectionObj = null;
                custList = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }


        // Get Customer list by Salesman ID
        public string GetCustomersListBySalesmanID(string salesmanID)
        {
            List<CustomerDetail> customerDetail = null;

            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                customerDetail = new List<CustomerDetail>();
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetCustomerListBySalesmanId, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@SalesmanId", salesmanID);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetCustomersListBySalesmanID", true);
                return jsonString;
            }


            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetCustomersListBySalesmanID", false);
            }
            finally
            {
                dbConnectionObj = null;
                customerDetail = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;

        }



        // Address Request Change by Customer
        public string AddressChangeRequest(string customerId, string userId, string streetAddress1, string streetAddress2, string city, string state, string zip, string phone)
        {
            List<CustomerDetail> customerDetail = null;

            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                customerDetail = new List<CustomerDetail>();
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcInsertAddressChangeRequest, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@UserID", userId);
                SqlCmd.Parameters.AddWithValue("@CustomerID", customerId);
                SqlCmd.Parameters.AddWithValue("@StreetAddress1", streetAddress1);
                SqlCmd.Parameters.AddWithValue("@StreetAddress2", streetAddress2);
                SqlCmd.Parameters.AddWithValue("@City", city);
                SqlCmd.Parameters.AddWithValue("@State", state);
                SqlCmd.Parameters.AddWithValue("@Zip", zip);
                SqlCmd.Parameters.AddWithValue("@Phone", phone);


                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                SendAddressChangeEmail(dt); // Email address change request
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "AddressChangeRequest", true);
                return jsonString;
            }



            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "AddressChangeRequest", false);
            }
            finally
            {
                dbConnectionObj = null;
                customerDetail = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;

        }

        // Send adddress change request email
        public void SendAddressChangeEmail(DataTable dt)
        {
            try
            {
                if (dt.Rows.Count > 0)
                {
                    UserAuthorizationRepository userRepObj = null;
                    bool ifEmailExist = false;
                    string regEmail = string.Empty;
                    var jsonResponseObj = "[{}]";
                    string body = string.Empty;

                    userRepObj = new UserAuthorizationRepository();
                    //    ifEmailExist = userRepObj.IfUserValidEmailOAuthDB(BrokerID, EmailTo);

                    // string from = WebConfigurationManager.AppSettings["SMTPServerEmail"];
                    string from = WebConfigurationManager.AppSettings["FromEmail"];
                    string to = "dixit@triveniit.com";
                    //   string cc = WebConfigurationManager.AppSettings["CCedEmail"];
                    string subject = "Address Change Request for Customer #:" + dt.Rows[0]["CustomerID"].ToString();
                    string bodyStr1 = "Hi,<br>";
                    bodyStr1 += dt.Rows[0]["SalesmanID"].ToString() + " has requested to update the address for Customer #" + dt.Rows[0]["CustomerID"].ToString() + "</b> to the following:<br><br>";
                    bodyStr1 += "<b>Street Address 1:</b>" + dt.Rows[0]["StreetAddress1"].ToString() + "<br>";
                    bodyStr1 += "<b>Street Address 2:</b>" + dt.Rows[0]["StreetAddress2"].ToString() + "<br>";
                    bodyStr1 += "<b>City:</b>" + dt.Rows[0]["City"].ToString() + "<br>";
                    bodyStr1 += "<b>State:</b>" + dt.Rows[0]["State"].ToString() + "<br>";
                    bodyStr1 += "<b>Zip:</b>" + dt.Rows[0]["Zip"].ToString() + "<br>";
                    bodyStr1 += "<b>Phone:</b>" + dt.Rows[0]["Phone"].ToString() + "<br><br>";
                    bodyStr1 += "Thank You";


                    //string bodyStr1 = "Please confirm your email address to set your password and redirect to ";

                    //  string redirectOMSurl = WebConfigurationManager.AppSettings["RedirectToOMSfromEmailURL"] + model.UserName + "&fogetPwdFlag=true";
                    string supportEmail = WebConfigurationManager.AppSettings["SupportEmail"];
                    string supportPhone = WebConfigurationManager.AppSettings["SupportPhone"];


                    if (true)
                    {
                        UserController userControllerObj = new UserController();
                        AccountController accountController = new AccountController();
                        LoginUser objUser = new LoginUser();
                        objUser.UserId = "FORGET_BROKER_TOKEN";
                        //    objUser.Password = "Pwd1@forget_broker_token";
                        //  var resStr = await userControllerObj.GetUserToken("FORGET_BROKER_TOKEN", "Pwd1@forget_broker_token");

                        //  string tokerStr = ((System.Web.Http.Results.OkNegotiatedContentResult<string>)(resStr)).Content;
                        // redirectOMSurl += "&access_token=" + tokerStr;

                        //  body = bodyStr1 + "<a href='" + redirectOMSurl + "'> link </a> </br>";
                        // body += "For assistance please email to " + supportEmail + " OR call to " + supportPhone;

                        bool isEmailSent = accountController.SendEmail(from, to, subject, bodyStr1, "");
                    }
                    else
                    {
                        userRepObj = null;
                        userRepObj = new UserAuthorizationRepository();


                        //  body = bodyStr1 + "<a href='" + redirectOMSurl + "'> link </a> </br>";
                        //  body += "For assistance please email to " + supportEmail + " OR call to " + supportPhone;

                        body = "Sorry, your email does not match with registered email, for assistance please email to " + supportEmail + " OR call to " + supportPhone;
                    }

                }
            }
            catch (Exception ex)
            {

            }
            finally
            {


            }



        }

        /// <summary>
        /// Get Customer Price List by Customer Id
        /// </summary>
        /// <param name="objCustomer"></param>
        /// <returns></returns>
        public string getPriceListFileByCustomerId(CustomerDetail objCustomer)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            DataTable dtDB2 = null;
            string jsonString;

            try
            {
                // Get Customer data from DB2 by Customer Id
                dtDB2 = getCustomerInfoByCustomerIdDB2(objCustomer.SalesmanId,objCustomer.CustomerId);

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetPriceListURL, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@Zone", dtDB2.Rows[0]["CSBZN"].ToString());
                SqlCmd.Parameters.AddWithValue("@CompanyId", dtDB2.Rows[0]["CSCO"].ToString());
                SqlCmd.Parameters.AddWithValue("@DocumentType","PriceList");
                

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();

                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "AddressChangeRequest", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "AddressChangeRequest", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// Update Customer address by Customer Id
        /// </summary>
        /// <param name="objCustomer"></param>
        /// <returns></returns>
        public string updateCustomerAddressDataByCustomerId(CustomerDetail objCustomer)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                // Get Customer data from DB2 by Customer Id

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetPriceListURL, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CustomerId", objCustomer.CustomerId);
                SqlCmd.Parameters.AddWithValue("@CompanyID", objCustomer.CompanyId);
                SqlCmd.Parameters.AddWithValue("@SalesmanId", objCustomer.SalesmanId);
                SqlCmd.Parameters.AddWithValue("@Street", objCustomer.Street);
                SqlCmd.Parameters.AddWithValue("@City", objCustomer.City);
                SqlCmd.Parameters.AddWithValue("@State", objCustomer.State);
                SqlCmd.Parameters.AddWithValue("@Zip", objCustomer.Zip);
                SqlCmd.Parameters.AddWithValue("@Country", "");
                SqlCmd.Parameters.AddWithValue("@Email", "");
                SqlCmd.Parameters.AddWithValue("@ContactNo", objCustomer.ContactNo);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();

                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "AddressChangeRequest", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "AddressChangeRequest", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public DataTable getCustomerInfoByCustomerIdDB2(string BrokerId,string CustomerId)
        {
            SqlDataAdapter da = null;
            DataTable dt = new DataTable();
            string jsonString;
            try
            {


                OleDbConnection con = new OleDbConnection(ApiConstant.apiConnectionStringDB2);
                OleDbCommand DB2cmd = new OleDbCommand(ApiConstant.SProcGetCustomerInfoByCustomerIdDB2.ToString(), con);

                DB2cmd.CommandType = CommandType.StoredProcedure;
                DB2cmd.Connection = con;

                DB2cmd.Parameters.AddWithValue("@SALESMANID", BrokerId.Substring(2, 4));
                DB2cmd.Parameters.AddWithValue("@CUSTOMERID", CustomerId);

                //  SqlCmd.Parameters.AddWithValue("@Action", action);


                con.Open();

                var dr = DB2cmd.ExecuteReader(CommandBehavior.CloseConnection);

                dt.Load(dr);
                string newColumnName;

                // Change column name coming from DB2 which have double quotes in all column name
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    newColumnName = dt.Columns[i].ColumnName.ToString();
                    dt.Columns[i].ColumnName = newColumnName.Replace("\"", "");
                    dt.AcceptChanges();
                }
                return dt;
            }
            catch (Exception ex)
            {
                log.Error("-> api/OrderHistory/GetOrderHistoryDB2", ex);
            }
            finally
            {

            }
            return null;

        }

        public List<CustomerList> GetCustomersDetailList_DB2(String BrokerId)
        {
            SqlDataAdapter da = null;
            DataTable dtl = null;
            List<CustomerList> custLst;
            try
            {
                con = new DB2Connection();
                DB2cmd = new SqlCommand(ApiConstant.SPocCustomersDetailListDB2, con.ApiConnection);
                ////OleDbConnection con = new OleDbConnection(ApiConstant.apiConnectionStringDB2);
                ////OleDbCommand DB2cmd = new OleDbCommand(ApiConstant.SPocCustomersDetailListDB2.ToString(), con);

                String brokerId = BrokerId.Substring(2, 4);

                DB2cmd.CommandType = CommandType.StoredProcedure;
                ////DB2cmd.Connection = con;

                DB2cmd.Parameters.AddWithValue("@COMPANYID", BrokerId.Substring(0, 2));
                DB2cmd.Parameters.AddWithValue("@BROKERID", brokerId);

                ////con.Open();
                ////var dr = DB2cmd.ExecuteReader(CommandBehavior.CloseConnection);
                ////dt1.Load(dr);
                ////con.Close();

                // new by kous
                da = new SqlDataAdapter(DB2cmd);
                dtl = new DataTable();
                con.ConnectionOpen();
                da.Fill(dtl);
                con.ConnectionClose();

                custLst = new List<CustomerList>();
                if (dtl != null)
                {
                    if (dtl.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtl.Rows.Count; i++)
                        {
                            custLst.Add(new CustomerList
                            {
                                UserID = Convert.ToString(dtl.Rows[i]["USERID"]),
                                Name = Convert.ToString(dtl.Rows[i]["NAME"]),
                                Address = Convert.ToString(dtl.Rows[i]["ADDRESS"]),
                                Region = Convert.ToString(dtl.Rows[i]["REGION"]),
                                CompanyID = Convert.ToString(dtl.Rows[i]["COMPANYID"]),
                                WHCompany = Convert.ToString(dtl.Rows[i]["WHCOMPANY"]),
                                CompanyName = Convert.ToString(dtl.Rows[i]["COMPANYNAME"]),
                                WHName = Convert.ToString(dtl.Rows[i]["WHNAME"]),
                                ContactNo = Convert.ToString(dtl.Rows[i]["CONTACTNO"]),
                                Street = Convert.ToString(dtl.Rows[i]["STREET"]),
                                City = Convert.ToString(dtl.Rows[i]["CITY"]),
                                State = Convert.ToString(dtl.Rows[i]["STATE"]),
                                Zip = Convert.ToString(dtl.Rows[i]["ZIP"]),
                                Status = Convert.ToString(dtl.Rows[i]["STATUS"]),
                                CreditStatus = Convert.ToString(dtl.Rows[i]["CREDITSTATUS"]),
                                CustomerBalance = Convert.ToString(dtl.Rows[i]["CUSTOMERBALANCE"]),
                                Zone = Convert.ToString(dtl.Rows[i]["ZONE"])
                            });
                        }
                    }
                }
                dtl.Dispose();
                return custLst;
            }
            catch (Exception ex)
            {

            }

             return null;
            
        }

        public String GetCustomersDetailListBySalesmanId(String BrokerId, Int32 CompanyId)
        {
            SqlDataAdapter da = null;
            DataTable dtl = null;
            //List<CustomerLst1> custLst;
            String jsonString = String.Empty;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcOMSGetCustomerListBySalesman, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@SalesmanId", BrokerId);
                SqlCmd.Parameters.AddWithValue("@CompanyID", Convert.ToString(CompanyId));

                // by koushik 
                da = new SqlDataAdapter(SqlCmd);
                dtl = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dtl);
                dbConnectionObj.ConnectionClose();

                jsonString = dtToJson.convertDataTableToJson(dtl, "GetCustomersDetailList", true);
                return jsonString;

                //custLst = new List<CustomerLst1>();
                //if (dtl != null)
                //{
                //    if (dtl.Rows.Count > 0)
                //    {
                //        for (int i = 0; i < dtl.Rows.Count; i++)
                //        {
                //            custLst.Add(new CustomerLst1
                //            {
                //                CustomerId = Convert.ToString(dtl.Rows[i]["USERID"]),
                //                Customer_Name = Convert.ToString(dtl.Rows[i]["NAME"]),
                //                ADDRESS = Convert.ToString(dtl.Rows[i]["ADDRESS"]),
                //                BrokerID = Convert.ToString(dtl.Rows[i]["BRROUT"]),
                //                CompanyID = Convert.ToString(dtl.Rows[i]["CompanyID"]),
                //                WHSE_CODE = Convert.ToString(dtl.Rows[i]["WHCOMPANY"]),
                //                ContractNo = Convert.ToString(dtl.Rows[i]["Zone"]),
                //                BillingCo = Convert.ToString(dtl.Rows[i]["BillingCO"]),
                //                CONTACTNO = Convert.ToString(dtl.Rows[i]["ContactNo"]),
                //                STREET = Convert.ToString(dtl.Rows[i]["STREET"]),
                //                CITY = Convert.ToString(dtl.Rows[i]["CITY"]),
                //                STATE = Convert.ToString(dtl.Rows[i]["STATE"]),
                //                ZIP = Convert.ToString(dtl.Rows[i]["ZIP"]),
                //                STATUS = Convert.ToString(dtl.Rows[i]["STATUS"]),
                //                CREDITSTATUS = Convert.ToString(dtl.Rows[i]["CREDITSTATUS"]),
                //                CUSTOMERBALANCE = Convert.ToString(dtl.Rows[i]["CUSTOMERBALANCE"]),
                //                COUNTRY = Convert.ToString(dtl.Rows[i]["COUNTRY"]),
                //                EMAILID = Convert.ToString(dtl.Rows[i]["EMAILID"]),
                //                BALANCE = Convert.ToString(dtl.Rows[i]["BALANCE"]),
                //                CUSTOMERBALANCEOVER30 = Convert.ToString(dtl.Rows[i]["CUSTOMERBALANCEOVER30"]),
                //                PromoAmount = Convert.ToString(dtl.Rows[i]["PromoAmount"]),
                //                TaxGroup = Convert.ToString(dtl.Rows[i]["TaxGroup"]),
                //                CreditDescription = Convert.ToString(dtl.Rows[i]["CreditDescription"]),
                //                CreditLimit = Convert.ToString(dtl.Rows[i]["CreditLimit"]),
                //                Latitude = Convert.ToString(dtl.Rows[i]["Latitude"]),
                //                Longitude = Convert.ToString(dtl.Rows[i]["Longitude"]),
                //                OrderQtyLength = Convert.ToString(dtl.Rows[i]["OrderQtyLength"])
                //            });
                //        }
                //    }
                //}
                //dtl.Dispose();
                //return custLst;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dtl, "GetCustomersDetailList", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dtl = null;
            }
            return null;
        }

        /// <summary>
        /// Update Customer address by Customer Id at Goya
        /// </summary>
        /// <param name="objCustomer"></param>
        /// <returns></returns>
        public string updateCustomerAddressData(CustomerDetail objCustomer)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                // Get Customer data from DB2 by Customer Id

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcUpdateCustomerAddressGoya, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CustomerId", objCustomer.CustomerId);
                SqlCmd.Parameters.AddWithValue("@CompanyID", objCustomer.CompanyId);
                SqlCmd.Parameters.AddWithValue("@SalesmanId", objCustomer.SalesmanId);
                SqlCmd.Parameters.AddWithValue("@COMPANYNAME", objCustomer.CompanyName);
                SqlCmd.Parameters.AddWithValue("@CUSTNAME", objCustomer.Name);
                SqlCmd.Parameters.AddWithValue("@CUSTADDESS", objCustomer.Address);
                SqlCmd.Parameters.AddWithValue("@CITY", objCustomer.City);
                SqlCmd.Parameters.AddWithValue("@STATE", objCustomer.State);
                SqlCmd.Parameters.AddWithValue("@ZIP", objCustomer.Zip);
                SqlCmd.Parameters.AddWithValue("@CONTACTNO", objCustomer.ContactNo);
                SqlCmd.Parameters.AddWithValue("@REGION", objCustomer.Region);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();

                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "CustomerAddressChangeRequest", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "CustomerAddressChangeRequest", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public DataTable GetCustomerFullDetails(string CompanyId, string CustomerId)
        {
            List<CustomerFullDetail> custFullDetObj = null;
            DataTable dt = new DataTable();
            try
            {
                custFullDetObj = new List<CustomerFullDetail>();
                AdoHelper adoHelper = new AdoHelper();
                dt = adoHelper.ExecDataTableProc(ApiConstant.SPCustomersFullDetailByCustomerId, new object[] { "@CustomerID", CustomerId, "@CompanyId", CompanyId });

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            custFullDetObj.Add(new CustomerFullDetail
                            {
                                Msg = "True",
                                CustomerID = Convert.ToString(dt.Rows[i]["UserID"]),
                                CustomerFullAddress = Convert.ToString(dt.Rows[i]["CustomerFullAddress"]),
                                CustomerImage = Convert.ToString(dt.Rows[i]["CustomerImage"]),
                                CustomerImagePath = Convert.ToString(dt.Rows[i]["CustomerImagePath"]),
                                Name = Convert.ToString(dt.Rows[i]["Name"]),
                                Street = Convert.ToString(dt.Rows[i]["Street"]),
                                City = Convert.ToString(dt.Rows[i]["City"]),
                                State = Convert.ToString(dt.Rows[i]["State"]),
                                Zip = Convert.ToString(dt.Rows[i]["Zip"]),
                                Country = Convert.ToString(dt.Rows[i]["Country"]),
                                ContactNo = Convert.ToString(dt.Rows[i]["ContactNo"]),
                                EmailID = Convert.ToString(dt.Rows[i]["EmailID"]),
                                Organization = "",
                                Website = Convert.ToString(dt.Rows[i]["Website"]),
                                CustomerBalance = Convert.ToDecimal(dt.Rows[i]["ARTOTAL"]),
                                Latitude = Convert.ToDecimal(dt.Rows[i]["Latitude"]),
                                Longitude = Convert.ToDecimal(dt.Rows[i]["Longitude"]),
                                Location = Convert.ToString(dt.Rows[i]["Location"]),
                                BillingAddress = Convert.ToString(dt.Rows[i]["BillingAddress"]),
                                ShippingAddress = Convert.ToString(dt.Rows[i]["ShippingAddress"])
                            });
                        }
                    }
                    else
                    {
                        custFullDetObj.Add(new CustomerFullDetail
                        {
                            Msg = "False"
                        });
                    }
                }
                else
                {
                    custFullDetObj.Add(new CustomerFullDetail
                    {
                        Msg = "False"
                    });
                }
                OMSWebApi.Models.CompanyModels.ListtoDataTable lsttodt = new OMSWebApi.Models.CompanyModels.ListtoDataTable();
                dt = lsttodt.ToDataTable(custFullDetObj);
                return dt;
            }
            catch (Exception ex)
            {
                string err = Convert.ToString(ex.Message);
            }
            finally
            {
                custFullDetObj = null;
                dt = null;
            }
            return null;
        }

        public DataTable GetCustomerStoreHours(string CompanyId, string CustomerId)
        {
            List<CustomerStoreHours> storeHoursObj = null;
            DataTable dt = new DataTable();
            try
            {
                storeHoursObj = new List<CustomerStoreHours>();
                AdoHelper adoHelper = new AdoHelper();
                dt = adoHelper.ExecDataTableProc(ApiConstant.SPGetDayHours_ByAuthCustomerId, new object[] { "@CustomerId", CustomerId, "@CompanyId", CompanyId });

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            storeHoursObj.Add(new CustomerStoreHours
                            {
                                Msg = "True",
                                CustomerID = Convert.ToString(dt.Rows[i]["CustomerID"]),
                                CompanyID = Convert.ToString(dt.Rows[i]["CompanyID"]),
                                DayHoursID = Convert.ToInt16(dt.Rows[i]["DayHoursID"]),
                                WeekDay = Convert.ToString(dt.Rows[i]["WeekDay"]),
                                CreatedDate = Convert.ToDateTime(dt.Rows[i]["CreatedDate"]),
                                TimeFrom = Convert.ToString(dt.Rows[i]["TimeFrom"]),
                                TimeTo = Convert.ToString(dt.Rows[i]["TimeTo"]),
                                SrlNo = Convert.ToString(dt.Rows[i]["SrlNo"])
                            });
                        }
                    }
                    else
                    {
                        storeHoursObj.Add(new CustomerStoreHours
                        {
                            Msg = "False"
                        });
                    }
                }
                else
                {
                    storeHoursObj.Add(new CustomerStoreHours
                    {
                        Msg = "False"
                    });
                }
                OMSWebApi.Models.CompanyModels.ListtoDataTable lsttodt = new OMSWebApi.Models.CompanyModels.ListtoDataTable();
                dt = lsttodt.ToDataTable(storeHoursObj);
                return dt;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                storeHoursObj = null;
                dt = null;
            }
            return null;
        }

        public DataTable GetCustomerReceivingHours(string CompanyId, string CustomerId)
        {
            List<CustomerReceivingeHours> receivingHoursObj = null;
            DataTable dt = new DataTable();
            try
            {
                receivingHoursObj = new List<CustomerReceivingeHours>();
                AdoHelper adoHelper = new AdoHelper();
                dt = adoHelper.ExecDataTableProc(ApiConstant.SPGetReceiveHours_ByAuthCustomerId, new object[] { "@CustomerId", CustomerId, "@CompanyId", CompanyId });

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            receivingHoursObj.Add(new CustomerReceivingeHours
                            {
                                Msg = "True",
                                CustomerID = Convert.ToString(dt.Rows[i]["CustomerID"]),
                                CompanyID = Convert.ToString(dt.Rows[i]["CompanyID"]),
                                DayHoursID = Convert.ToInt16(dt.Rows[i]["ReceiveHoursID"]),
                                WeekDay = Convert.ToString(dt.Rows[i]["WeekDay"]),
                                CreatedDate = Convert.ToDateTime(dt.Rows[i]["CreatedDate"]),
                                TimeFrom = Convert.ToString(dt.Rows[i]["TimeFrom"]),
                                TimeTo = Convert.ToString(dt.Rows[i]["TimeTo"]),
                                SrlNo = Convert.ToString(dt.Rows[i]["SrlNo"])
                            });
                        }
                    }
                    else
                    {
                        receivingHoursObj.Add(new CustomerReceivingeHours
                        {
                            Msg = "False"
                        });
                    }
                }
                else
                {
                    receivingHoursObj.Add(new CustomerReceivingeHours
                    {
                        Msg = "False"
                    });
                }
                OMSWebApi.Models.CompanyModels.ListtoDataTable lsttodt = new OMSWebApi.Models.CompanyModels.ListtoDataTable();
                dt = lsttodt.ToDataTable(receivingHoursObj);
                return dt;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                receivingHoursObj = null;
                dt = null;
            }
            return null;
        }

        public DataTable GetCustomerPayment(String CustomerBalance)
        {
            List<CustomerPayment> paymentObj = null;
            DataTable dt = new DataTable();
            try
            {
                paymentObj = new List<CustomerPayment>();

                if (!String.IsNullOrEmpty( CustomerBalance ))
                {
                    paymentObj.Add(new CustomerPayment
                    {
                        Msg = "True",
                        CustomerBalance = Convert.ToDecimal( CustomerBalance)
                    });
                        
                }
                else
                {
                    paymentObj.Add(new CustomerPayment
                    {
                        Msg = "False"
                    });
                }
                
                OMSWebApi.Models.CompanyModels.ListtoDataTable lsttodt = new OMSWebApi.Models.CompanyModels.ListtoDataTable();
                dt = lsttodt.ToDataTable(paymentObj);
                return dt;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                paymentObj = null;
                dt = null;
            }
            return null;
        }
        public DataTable GetCustomerLocation(String Latitude, String Longitude, String Location, String BillingAddress, String ShippingAddress)
        { 
            List<CustomerLocation> paymentObj = null;
            DataTable dt = new DataTable();
            try
            {
                paymentObj = new List<CustomerLocation>();

                if (!String.IsNullOrEmpty(Latitude))
                {
                    paymentObj.Add(new CustomerLocation
                    {
                        Msg = "True",
                        Latitude = Convert.ToDecimal(Latitude),
                        Longitude = Convert.ToDecimal(Longitude),
                        Location = Convert.ToString(Location),
                        BillingAddress = Convert.ToString(BillingAddress),
                        ShippingAddress = Convert.ToString(ShippingAddress)
                    });

                }
                else
                {
                    paymentObj.Add(new CustomerLocation
                    {
                        Msg = "False"
                    });
                }

                OMSWebApi.Models.CompanyModels.ListtoDataTable lsttodt = new OMSWebApi.Models.CompanyModels.ListtoDataTable();
                dt = lsttodt.ToDataTable(paymentObj);
                return dt;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                paymentObj = null;
                dt = null;
            }
            return null;
        }

    }
}