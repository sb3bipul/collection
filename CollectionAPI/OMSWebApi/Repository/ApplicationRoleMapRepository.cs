﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using OMSWebApi.Models;

namespace OMSWebApi.Repository
{
    public class ApplicationRoleMapRepository
    {
        DBConnectionOAuth dbConnectionObj = null;
        SqlCommand com = null;
        SqlDataAdapter da = null;
        DataTable dt = null;

        public RoleApplicationAccessMapping GetAppRoleMapListFromDb(int pApplicationId)
        {
            RoleApplicationAccessMapping returnObj = null;
            List<RoleApplicationAccessMapping> ListObj = null;
            Common common = new Common();

            try
            {
                returnObj = new RoleApplicationAccessMapping();
                ListObj = new List<RoleApplicationAccessMapping>();
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocGetApplicationRoleMappingList, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@ApplicationId", pApplicationId);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                //Bind ApplicationsModel generic list using LINQ 
                ListObj = (from DataRow dr in dt.Rows

                           select new RoleApplicationAccessMapping()
                           {
                               RoleId = Convert.ToInt32(dr["RoleId"]),
                               IsAccess = Convert.ToBoolean(dr["IsAccess"]),
                               RoleName = Convert.ToString(dr["RoleName"])
                               
                           }).ToList();

                returnObj.RoleAppAccessMapList = ListObj;
                returnObj.ApplicationList = common.GetGeneralLookupList(string.Empty, "Applications");
              //  returnObj.RoleList = GetRoleLookupListForAppId(pApplicationId);
            }
            catch (SqlException sqlex)
            {

            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbConnectionObj = null;
                ListObj = null;
                com = null;
                da = null;
                dt = null;
            }

            return returnObj;
        }

        public RoleApplicationAccessMapping GetRoleDropDownListFromDb(int pApplicationId)
        {
            RoleApplicationAccessMapping returnObj = null;
            //List<RoleApplicationAccessMapping> ListObj = null;
            Common common = new Common();

            try
            {
                returnObj = new RoleApplicationAccessMapping();
                //ListObj = new List<RoleApplicationAccessMapping>();
                //dbConnectionObj = new DBConnection();
                //com = new SqlCommand(ApiConstant.SPocGetApplicationRoleMappingList, dbConnectionObj.ApiConnection);
                //com.CommandType = CommandType.StoredProcedure;
                //com.Parameters.AddWithValue("@ApplicationId", pApplicationId);
                //da = new SqlDataAdapter(com);
                //dt = new DataTable();
                //dbConnectionObj.ConnectionOpen();
                //da.Fill(dt);
                //dbConnectionObj.ConnectionClose();

                //Bind ApplicationsModel generic list using LINQ 
                //ListObj = (from DataRow dr in dt.Rows

                //           select new RoleApplicationAccessMapping()
                //           {
                //               RoleId = Convert.ToInt32(dr["RoleId"]),
                //               IsAccess = Convert.ToBoolean(dr["IsAccess"]),
                //               RoleName = Convert.ToString(dr["RoleName"])
                               
                //           }).ToList();

                //returnObj.RoleAppAccessMapList = ListObj;
               // returnObj.ApplicationList = common.GetGeneralLookupList(string.Empty, "Applications");
                returnObj.RoleList = GetRoleLookupListForAppId(pApplicationId);
            }
            catch (SqlException sqlex)
            {

            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbConnectionObj = null;
               // ListObj = null;
                com = null;
                da = null;
                dt = null;
            }

            return returnObj;
        }

        /// <summary>
        /// Will be used to populate drop down list for ID 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="tblName"></param>
        /// <returns></returns>
        public List<LookupMappingTable> GetRoleLookupListForAppId(int applicationId)
        {
            List<LookupMappingTable> listObj = null;
            try
            {
                listObj = new List<LookupMappingTable>();
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocGetRoleLookupListForAppId, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@applicationId", applicationId);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                //Bind ApplicationsModel generic list using LINQ 
                listObj = (from DataRow dr in dt.Rows

                           select new LookupMappingTable()
                           {
                               CodeId = Convert.ToInt32(dr["RoleId"]),
                               Value1 = Convert.ToString(dr["RoleName"])
                           }).ToList();

                return listObj;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbConnectionObj = null;
                listObj = null;
                com = null;
                da = null;
                dt = null;
            }

            return listObj;
        }

        public bool UpdateAppRoleMapToDb(List<RoleApplicationAccessMapping> pObj)
        {
            bool success = false;
            SqlDataReader reader = null;
            try
            {
                dbConnectionObj = new DBConnectionOAuth();

                com = new SqlCommand(ApiConstant.SPocDeleteAppRoleMapByIds, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@ApplicationId", pObj[0].ApplicationId);

                dbConnectionObj.ConnectionOpen();
                reader = com.ExecuteReader();
                success = true;

                reader.Close();
                dbConnectionObj.ConnectionClose();


                foreach (var oItem in pObj)
                {
                    com = new SqlCommand(ApiConstant.SPocInsertAppRoleMap, dbConnectionObj.ApiConnection);
                    com.CommandType = CommandType.StoredProcedure;

                    com.Parameters.AddWithValue("@RoleId", oItem.RoleId);
                    com.Parameters.AddWithValue("@ApplicationId", oItem.ApplicationId);
                    com.Parameters.AddWithValue("@IsAccess", oItem.IsAccess);

                    dbConnectionObj.ConnectionOpen();
                    reader = com.ExecuteReader();
                    success = true;

                    reader.Close();
                    dbConnectionObj.ConnectionClose();
                }


                return success;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                reader = null;
                //  dbConnectionObj = null;
                com = null;
            }

            return success;
        }
    }
}