﻿using OMSWebApi.Models;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace OMSWebApi.Repository
{
    public class PayPanelRepository
    {
        DBConnection dbConnectionObj = null;
        SqlCommand SqlCmd = null;
        DataToJson dtToJson = new DataToJson();
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //static void RegisterRoutes(RouteCollection routes)
        //{
        //    routes.MapPageRoute("paypanelpayment", "Pages/{PageName}.aspx", "~/payment.aspx");
        //}
        public string InsertPayPanelTempdata(PayPanelModel PayPanel)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            Guid guid = Guid.NewGuid();
            DataTable DtFinel = null;
            DataTable DtError = null;
            string from, to, cc = "", subject, body;
            SMSService sms = new SMSService();
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_Insert_PayPanelData, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@ID", guid);
                SqlCmd.Parameters.AddWithValue("@CustomerName", string.IsNullOrEmpty(PayPanel.customer_name) ? DBNull.Value : (object)PayPanel.customer_name);
                SqlCmd.Parameters.AddWithValue("@Amount", string.IsNullOrEmpty(PayPanel.amount) ? DBNull.Value : (object)PayPanel.amount);
                SqlCmd.Parameters.AddWithValue("@Currency", string.IsNullOrEmpty(PayPanel.currency) ? DBNull.Value : (object)PayPanel.currency);
                SqlCmd.Parameters.AddWithValue("@EmailId", string.IsNullOrEmpty(PayPanel.email) ? DBNull.Value : (object)PayPanel.email);
                SqlCmd.Parameters.AddWithValue("@PhoneNo", string.IsNullOrEmpty(PayPanel.mobile) ? DBNull.Value : (object)PayPanel.mobile);
                SqlCmd.Parameters.AddWithValue("@OrderNo", string.IsNullOrEmpty(PayPanel.order_increment_id) ? DBNull.Value : (object)PayPanel.order_increment_id);


                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                if (Convert.ToString(dt.Rows[0]["ID"]) != null && Convert.ToString(dt.Rows[0]["ID"]) != "")
                {
                    byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(Convert.ToString(dt.Rows[0]["ID"]));
                    string encrypted = Convert.ToBase64String(b);
                    //return encrypted;
                    string url = ("http://3.221.58.176:91/PaymentWebForm/" + "CreditCard?id=" + encrypted);
                    from = WebConfigurationManager.AppSettings["FromEmail"];
                    to = Convert.ToString(dt.Rows[0]["EmailId"]);
                    cc = WebConfigurationManager.AppSettings["CCedEmail"] + ";" + cc;
                    subject = WebConfigurationManager.AppSettings["PaymentCreateSubject"];

                    body = "Dear User, " + "<br/>" + "Goya Foods Inc. has requested " + dt.Rows[0]["Amount"] + " " + dt.Rows[0]["Currency"] + " for order number #" + dt.Rows[0]["OrderNo"] +
                        ",<br/>" + "Please click on link below to complete the payment " + "<br/>"
                        + url + "<br/>" + " Please note that the link will expire in 30 minutes." + "<br/>" + "The PayPanel team";

                    SendEmail(from, to, subject, body, cc);

                    sms.send_to = Convert.ToString(dt.Rows[0]["PhoneNo"]);
                    sms.message = Convert.ToString("Goya Foods Inc. has requested " + dt.Rows[0]["Amount"] + " " + dt.Rows[0]["Currency"] + " for order number #" + dt.Rows[0]["OrderNo"] + ". Please click on link below to complete the payment " +
                     url + " Please note that the link will expire in 30 minutes.");

                    CallSMSServiceAPI(sms);

                    //var session = HttpContext.Current.Session;
                    //session[Convert.ToString(dt.Rows[0]["ID"])] = DateTime.Now.ToString("hh:mm:ss").ToString();

                    DtFinel = new DataTable();
                    DtFinel.Columns.Add("responsecode", typeof(String));
                    DtFinel.Columns.Add("url", typeof(String));
                    DtFinel.Columns.Add("responsemsg", typeof(String));

                    DataRow dr = DtFinel.NewRow();
                    dr[0] = "200";
                    dr[1] = url;
                    dr[2] = "Payment requested successfully";
                    DtFinel.Rows.Add(dr);
                    jsonString = dtToJson.convertDataTableToJson(DtFinel, "InsertPaymentInfo", true);
                }
                else
                {
                    DtError = new DataTable();
                    DtError.Columns.Add("responsecode", typeof(String));
                    DtError.Columns.Add("url", typeof(String));
                    DtError.Columns.Add("responsemsg", typeof(String));

                    DataRow dr = DtError.NewRow();
                    dr[0] = "200";
                    dr[1] = DBNull.Value;
                    dr[2] = "Order_increment_id already exits";
                    DtError.Rows.Add(dr);
                    jsonString = dtToJson.convertDataTableToJson(DtError, "InsertPaymentInfo", true);
                }
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertPaymentInfo", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
                DtError = null;
                DtFinel = null;
            }
            return null;
        }


        [AllowAnonymous]
        public static bool SendEmail(string from, string to, string subject, string body, string cc)
        {
            bool isEmailSent = false;

            try
            {
                MailMessage email = new MailMessage(from, to, subject, body);
                //System.Net.Mail.Attachment attachment;
                //attachment = new System.Net.Mail.Attachment(filePath);
                //email.Attachments.Add(attachment);
                if (cc != "")
                {
                    foreach (var address in cc.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        email.CC.Add(address);
                    }
                }
                email.IsBodyHtml = true;

                string server = WebConfigurationManager.AppSettings["SMTPServer"];
                SmtpClient smtp = new SmtpClient(server);

                //smtp.UseDefaultCredentials = true;
                smtp.UseDefaultCredentials = false;
                smtp.Port = 587;
                smtp.EnableSsl = true;
                string userEmail = WebConfigurationManager.AppSettings["SMTPServerEmail"].ToString();
                string password = WebConfigurationManager.AppSettings["SMTPServerPassword"].ToString();

                smtp.Credentials = new System.Net.NetworkCredential(userEmail, password);
                smtp.Send(email);
                isEmailSent = true;
            }
            catch (Exception ex)
            {
                isEmailSent = false;
            }
            return isEmailSent;
        }

        public string CallSMSServiceAPI(SMSService SMS)
        {
            string data = string.Empty;
            string SmsAPI = Convert.ToString(ConfigurationSettings.AppSettings["SingleSMSUrl"].ToString());
            string token = Convert.ToString(ConfigurationSettings.AppSettings["Token"].ToString());
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(SmsAPI);
                request.Timeout = 1000000;
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("Authorization", "AppToken " + token);
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (var sw = new StreamWriter(request.GetRequestStream()))
                {
                    string json = serializer.Serialize(SMS);
                    sw.Write(json);
                    sw.Flush();
                }
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    data = reader.ReadToEnd();
                }
                return data;
            }
            catch (Exception ex)
            {

            }
            return null;
        }       
    }
}