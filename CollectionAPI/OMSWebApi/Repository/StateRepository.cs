﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using OMSWebApi.Models;
using System.Xml.Serialization;
using Newtonsoft.Json.Linq;
using System.Data.OleDb;
using System.Web.Configuration;
using OMSWebApi.Controllers;
using OMSWebApi.Providers;
using OMSWebApi.Results;
using OMSWebApi.Repository;
using System.Net.Mail;

namespace OMSWebApi.Repository
{
    public class StateRepository
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        DBConnection dbConnectionObj = null;
        DataToJson dtToJson = new DataToJson();
        SqlCommand com = null;
        SqlDataAdapter da = null;
        DataTable dt = null;

        public string GetStateListFromDb(int? CountryCode)
        {
            List<StateModel> StateListObj = null;
            string jsonString;
            try
            {
                StateListObj = new List<StateModel>();
                dbConnectionObj = new DBConnection();
                com = new SqlCommand(ApiConstant.SProcGetStateList, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@CountryCode", CountryCode);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                ////Bind StateModel generic list using LINQ 
                //StateListObj = (from DataRow dr in dt.Rows
                //                  select new StateModel()
                //                  {
                //                      StateName = Convert.ToString(dr["StateName"]),
                //                      StateId = Convert.ToInt32(dr["StateCode"])
                //                  }).ToList();
                jsonString = dtToJson.convertDataTableToJson(dt, "GetStateListData", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertRegulartDataToJson(dt);
            }
            finally
            {
                dbConnectionObj = null;
                com = null;
                da = null;
                dt = null;
            }

            return null;
        }

    }
}