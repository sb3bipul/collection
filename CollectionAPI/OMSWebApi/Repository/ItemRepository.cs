﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using OMSWebApi.Models;
using System.Xml.Serialization;
using Newtonsoft.Json.Linq;
using System.Data.OleDb;

namespace OMSWebApi.Repository
{
    public class ItemRepository
    {
        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        DBConnection dbConnectionObj = null;
        SqlCommand SqlCmd = null;
        DataToJson dtToJson = new DataToJson();


        /// <summary>
        /// Get Item List
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="languageId"></param>
        /// <param name="searchText"></param>
        /// <param name="catalogId"></param>
        /// <param name="warehouseId"></param>
        /// <returns></returns>
        public string getItemList(int companyId, string languageId, string searchText, string catalogId,string warehouseId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetProductData, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
              
                SqlCmd.Parameters.AddWithValue("@CompanyId", companyId);
                SqlCmd.Parameters.AddWithValue("@LanguageId", languageId);
                SqlCmd.Parameters.AddWithValue("@CatalogId", catalogId);
                SqlCmd.Parameters.AddWithValue("@WarehouseId", warehouseId);


                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

              // Convert Datatable to JSON string
              //jsonString = dtToJson.convertRegulartDataToJson(dt);
                jsonString = dtToJson.convertDataTableToJson(dt, "SProcGetProductData   ", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertRegulartDataToJson(dt);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string addWishlistItemData(int companyId, string ProductCode)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcInsertItem_Wishlist, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyID", companyId);
                SqlCmd.Parameters.AddWithValue("@ProductCode", ProductCode);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                //jsonString = dtToJson.convertRegulartDataToJson(dt);
                jsonString = dtToJson.convertDataTableToJson(dt, "SProcGetProductData   ", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertRegulartDataToJson(dt);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        // added for Retailer portal popular product
        public string getPopularProcuct(ItemModel itemModel)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcPopularProduct, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@ItemId", (itemModel.ItemId <= 0) ? DBNull.Value : (object)itemModel.ItemId);
                SqlCmd.Parameters.AddWithValue("@ItemCode", (string.IsNullOrEmpty(itemModel.ItemCode)) ? DBNull.Value : (object)itemModel.ItemCode);
                SqlCmd.Parameters.AddWithValue("@ItemName", (string.IsNullOrEmpty(itemModel.ItemName)) ? DBNull.Value : (object)itemModel.ItemName);
                SqlCmd.Parameters.AddWithValue("@UPCNumber", (string.IsNullOrEmpty(itemModel.UPCNumber)) ? DBNull.Value : (object)itemModel.UPCNumber);
                SqlCmd.Parameters.AddWithValue("@CategoryId", (itemModel.ItemCategoryId <= 0) ? DBNull.Value : (object)itemModel.ItemCategoryId);
                SqlCmd.Parameters.AddWithValue("@SecondaryItemNumber", (string.IsNullOrEmpty(itemModel.SecondaryItemNumber)) ? DBNull.Value : (object)itemModel.SecondaryItemNumber);
                SqlCmd.Parameters.AddWithValue("@CompanyId", (itemModel.CompanyId <= 0) ? DBNull.Value : (object)itemModel.CompanyId);
                SqlCmd.Parameters.AddWithValue("@VendorId", (itemModel.VendorId <= 0) ? DBNull.Value : (object)itemModel.VendorId);
                SqlCmd.Parameters.AddWithValue("@IsActive", (itemModel.IsActive == null) ? DBNull.Value : (object)itemModel.IsActive);
                SqlCmd.Parameters.AddWithValue("@ZoneId", (itemModel.ZoneId <= 0) ? DBNull.Value : (object)itemModel.ZoneId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "getPopularProcuct", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "getPopularProcuct", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }


    }
}