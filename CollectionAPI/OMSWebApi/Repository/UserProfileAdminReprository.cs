﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using OMSWebApi.Models;

namespace OMSWebApi.Repository
{
    public class UserProfileAdminReprository
    {
        DBConnectionOAuth dbConnectionObj = null;
        SqlCommand com = null;
        SqlDataAdapter da = null;
        DataTable dt = null;

        public UserProfile GetUserSearchListFromDb(string pUserSearchStr)
        {
            UserProfile returnObj = null;
            List<UserProfile> ListObj = null;

            try
            {
                returnObj = new UserProfile();
                ListObj = new List<UserProfile>();

                returnObj.UserProfileList = GetUserSearchLst(pUserSearchStr, ApiConstant.SPocGetUserSearchList);
            }
            catch (SqlException sqlex)
            {

            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbConnectionObj = null;
                ListObj = null;
                com = null;
                da = null;
                dt = null;
            }

            return returnObj;
        }

        public List<UserProfile> GetUserSearchLst(string pUserSearchStr, string SPoc)
        {
            List<UserProfile> ListObj = null;
            pUserSearchStr = "%" + pUserSearchStr + "%";
            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(SPoc, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@UserSearchStr", pUserSearchStr);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                //Bind ApplicationsModel generic list using LINQ 
                ListObj = (from DataRow dr in dt.Rows

                           select new UserProfile()
                           {
                               UserName = Convert.ToString(dr["UserName"]),
                               UserFullName = Convert.ToString(dr["UserName"]) + ", " + Convert.ToString(dr["UserFullName"])
                           }).ToList();
                return ListObj;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbConnectionObj = null;
                ListObj = null;
                com = null;
                da = null;
                dt = null;
            }

            return ListObj;
        }

        public UserProfile GetUserProfileDetailsFromDb(string pUserName)
        {
            UserProfile returnObj = null;
          //  List<UserProfile> ListObj = null;

            try
            {
                returnObj = new UserProfile();
            //    ListObj = new List<UserProfile>();

                returnObj = GetUserProfileDetail(pUserName, ApiConstant.SPocGetUserMyProfile);
            }
            catch (SqlException sqlex)
            {

            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbConnectionObj = null;
              //  ListObj = null;
                com = null;
                da = null;
                dt = null;
            }

            return returnObj;
        }

        public UserProfile GetUserProfileDetail(string pUserName, string SPoc)
        {
            UserProfile userObj = null;
            try
            {
                userObj = new UserProfile();
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(SPoc, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@userId", pUserName);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                foreach (DataRow dr in dt.Rows)
                {
                    userObj.UserName = dr["UserName"].ToString();
                    userObj.UserFullName = dr["UserFullName"].ToString();
                    userObj.CompanyId = dr["CompanyId"].ToString();
                    userObj.CompanyName = dr["CompanyName"].ToString();
                    userObj.Role = dr["Role"].ToString();
                    userObj.Email = dr["Email"].ToString();
                    userObj.Phone = dr["PhoneNumber"].ToString();
                    userObj.Domain = dr["Domain"].ToString();
                    userObj.UserType = dr["UserType"].ToString();
                    userObj.OMSRole = dr["OMSRole"].ToString();   
                    userObj.RPTRole = dr["RPTRole"].ToString();
                }
                return userObj;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbConnectionObj = null;
                userObj = null;
                com = null;
                da = null;
                dt = null;
            }

            return userObj;
        }

        public bool UpdateUserProfileDetailToDb(UserProfile pObj)
        {
            bool success = false;
            SqlDataReader reader = null;
            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocUpdateUserProfileFromAdmin, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@userName", pObj.UserName);
                com.Parameters.AddWithValue("@userFulName", pObj.UserFullName);
                com.Parameters.AddWithValue("@email", pObj.Email);
                com.Parameters.AddWithValue("@phone", pObj.Phone);
                com.Parameters.AddWithValue("@updatedBy", pObj.UpdateBy);
                com.Parameters.AddWithValue("@comments", "Phone and Email got updated.");

                dbConnectionObj.ConnectionOpen();
                reader = com.ExecuteReader();
                success = true;

                reader.Close();
                dbConnectionObj.ConnectionClose();
                return success;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                reader = null;
                //  dbConnectionObj = null;
                com = null;
            }

            return success;
        }
    }
}