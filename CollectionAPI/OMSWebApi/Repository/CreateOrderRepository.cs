﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using OMSWebApi.Models;
using System.Xml.Serialization;
using Newtonsoft.Json.Linq;
using System.Data.OleDb;
using System.Configuration;
using System.IO;
using System.Text;
using System.Web.Configuration;
using System.Xml;

namespace OMSWebApi.Repository
{
    public class CreateOrderRepository
    {
        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        String ClientName = ApiConstant.getClientName.ToString();
        DBConnection dbConnectionObj = null;
        DB2Connection con = null;
        SqlCommand DB2cmd = null;
        SqlCommand SqlCmd = null;
        static DataSet objds = new DataSet();
        DataToJson dtToJson = new DataToJson();
        public static int FileIndex = 0;
        static string strFileIndex = string.Empty;
        static string strEDIFileType = string.Empty;
        static string strEDIFileName = string.Empty;
        String strPONumber = string.Empty;
        public static double totalAmount = 0;
        public static int noLineItem = 0;
        static string strEDIFileDirectory = ApiConstant.FileDirectory;// WebConfigurationManager.AppSettings["EDIFileDirectory"].ToString();

        /*
        -------------------------------------------------------------------------------------------------------------------
        |   Function Description    :   Create cart item,  Order and detail and then create a EDI file
        |   Date Created            :   12-17-2018
        |   Created By              :   Koushik Sarkar
        ---------------------------------------------------------------------------------------------------------------------
        */
        /// <summary>
        /// Get All Promo Codes
        /// </summary>
        /// <returns></returns>
        public string GetPromoCode()
        {
            List<PromoCode> promoCode = null;

            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                promoCode = new List<PromoCode>();
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetPromoCodes, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

               
                jsonString = dtToJson.convertDataTableToJson(dt, "GetPromoCode",true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetPromoCode",false);
            }
            finally
            {
                dbConnectionObj = null;
                promoCode = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string GetPromoCodeList()
        {
            List<PromoCode> promoCode = null;

            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                promoCode = new List<PromoCode>();
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetPromoCodeList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                jsonString = dtToJson.convertDataTableToJson(dt, "GetPromoCodeList", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetPromoCode", false);
            }
            finally
            {
                dbConnectionObj = null;
                promoCode = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// Get all Departments
        /// </summary>
        /// <returns></returns>
        public string GetDepartments()
        {
            List<Department> department = null;

            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                department = new List<Department>();
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetDepartments, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

               
                jsonString = dtToJson.convertDataTableToJson(dt, "GetDepartments",true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetDepartments", false);
            }
            finally
            {
                dbConnectionObj = null;
                department = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

     /// <summary>
     /// Get templates
     /// </summary>
     /// <param name="customerId">Not Used(Pass zero all the time)</param>
     /// <param name="companyId"></param>
     /// <param name="brokerId"></param>
     /// <param name="isPickup"></param>
     /// <returns></returns>
        public string getTemplatesByBrokerId(string customerId,int companyId,string brokerId,bool isPickup)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            { 
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetTemplates, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CustomerId", customerId);
                SqlCmd.Parameters.AddWithValue("@CompanyId", companyId);
                SqlCmd.Parameters.AddWithValue("@SalesmanID", brokerId);
                SqlCmd.Parameters.AddWithValue("@IsPickup", isPickup);


                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "getTemplatesByBrokerId",true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "getTemplatesByBrokerId", false);
            }
            finally
            {
                dbConnectionObj = null;
            
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// Get Order Item List
        /// </summary>       
        /// <returns></returns>
        public string getOrderItemList(int companyId, String department, String itemCode, int caseQuantity, int unitQuantity, int catalogId, String customerId, String brokerId, String languageId, String wharehouseId, bool isPickup, String comments, String action, String EOR, String result, DateTime deliveryDate, String clientPONumber, String promoCode, String day, String notDay,Decimal amountCollected,String goyaCompanyID,String orderMode)//, bool isRestricted,int QOH,decimal CasePrice,decimal RetailPrice,int UnitForCasePrice,decimal PromoAmount,bool IsPromoItem,int PromoMinQuantity,decimal OriginalCasePrice)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                // Get Item Price, QOH and Authorization details
                DataTable dtPriceQOH;

                bool isRestricted = true;
                int QOH = 0;
                decimal CasePrice = 0;
                decimal RetailPrice = 0;
                int UnitForCasePrice = 0;
                decimal PromoAmount = 0;
                bool IsPromoItem = false;
                int PromoMinQuantity = 0;
                decimal OriginalCasePrice = 0;

                dtPriceQOH = getItemQOHDatable(goyaCompanyID.ToString(), itemCode, customerId, caseQuantity); // Get Item Price, QOH and Authorization details
                if(dtPriceQOH.Rows.Count > 0)
                {
                    // Check Item authorization
                    //if(dtPriceQOH.Rows[0]["ISAUTHORIZED"].ToString()=="1")
                    //      isRestricted = false;

                    // Check Item is in Promotion or Not
                    if (dtPriceQOH.Rows[0]["ISPROMO"].ToString() == "X")
                          IsPromoItem = true;

                    QOH = Convert.ToInt32(dtPriceQOH.Rows[0]["ITEM_QOH"]);
                    CasePrice = Convert.ToDecimal(dtPriceQOH.Rows[0]["CASEPRICE"]);
                    RetailPrice = Convert.ToDecimal(dtPriceQOH.Rows[0]["RETAILERPRICE"]);
                    UnitForCasePrice = Convert.ToInt32(dtPriceQOH.Rows[0]["PRICEUNIT"]);
                    PromoAmount = Convert.ToDecimal(dtPriceQOH.Rows[0]["PROMOAMOUNT"]);

                    PromoMinQuantity = Convert.ToInt32(dtPriceQOH.Rows[0]["MINQUANTITY"]);
                    OriginalCasePrice = Convert.ToDecimal(dtPriceQOH.Rows[0]["ORIGINALCASEPRICE"]);

                }

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetItemOrderItemData, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CustomerId", customerId);
                SqlCmd.Parameters.AddWithValue("@SalesmanID", brokerId);
                SqlCmd.Parameters.AddWithValue("@CompanyID", companyId);
                SqlCmd.Parameters.AddWithValue("@Dept", department);
                SqlCmd.Parameters.AddWithValue("@ProductCode", itemCode);
                SqlCmd.Parameters.AddWithValue("@Cases", caseQuantity);
                SqlCmd.Parameters.AddWithValue("@Units", unitQuantity);
                SqlCmd.Parameters.AddWithValue("@LanguageID", languageId);
                SqlCmd.Parameters.AddWithValue("@CatalogId", catalogId);
                SqlCmd.Parameters.AddWithValue("@WHID", wharehouseId);
                SqlCmd.Parameters.AddWithValue("@Pickup", isPickup);
                SqlCmd.Parameters.AddWithValue("@Comments", comments);
                SqlCmd.Parameters.AddWithValue("@Action", action);
                SqlCmd.Parameters.AddWithValue("@EOR", EOR);
                SqlCmd.Parameters.AddWithValue("@DeliveryDate", deliveryDate);
                SqlCmd.Parameters.AddWithValue("@ClientPONumber", clientPONumber);
                SqlCmd.Parameters.AddWithValue("@PromoCode", promoCode);
                SqlCmd.Parameters.AddWithValue("@Day", day);
                SqlCmd.Parameters.AddWithValue("@AmountCollected",amountCollected);
                SqlCmd.Parameters.AddWithValue("@NotDay", notDay);
                SqlCmd.Parameters.AddWithValue("@IsRestricted",isRestricted);
                SqlCmd.Parameters.AddWithValue("@QOH", QOH);  
                SqlCmd.Parameters.AddWithValue("@CasePrice", CasePrice);
                SqlCmd.Parameters.AddWithValue("@RetailPrice", RetailPrice);
                SqlCmd.Parameters.AddWithValue("@UnitForCasePrice", UnitForCasePrice);

                SqlCmd.Parameters.AddWithValue("@PromoAmount", PromoAmount);
                SqlCmd.Parameters.AddWithValue("@IsPromoItem", IsPromoItem);
                SqlCmd.Parameters.AddWithValue("@PromoMinQuantity", PromoMinQuantity);
                SqlCmd.Parameters.AddWithValue("@OriginalCasePrice", OriginalCasePrice);
                SqlCmd.Parameters.AddWithValue("@OrderMode", orderMode);

                SqlCmd.Parameters.AddWithValue("@MESSAGE","");                
                 
              //  SqlCmd.Parameters.AddWithValue("@Comments", "");

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "getOrderItemList",true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "getOrderItemList",false);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// Add Order Item
        /// </summary>       
        /// <returns></returns>
        public string addOrderItem(int companyId, String itemCode, int caseQuantity, String customerId, String brokerId, DateTime deliveryDate, String promoCode, Decimal amountCollected, String EORno, String orderMode)//, bool isRestricted,int QOH,decimal CasePrice,decimal RetailPrice,int UnitForCasePrice,decimal PromoAmount,bool IsPromoItem,int PromoMinQuantity,decimal OriginalCasePrice)
        {
            SqlDataAdapter da = null; 
            DataTable dt = null;
            string jsonString;
            try
            {
                // Get Item Price, QOH and Authorization details
                DataTable dtPriceQOH;
                DataTable dtEOR;
                bool isRestricted = true;
                int QOH = 0;
                decimal CasePrice = 0;
                decimal RetailPrice = 0;
                int UnitForCasePrice = 0;
                decimal PromoAmount = 0;
                bool IsPromoItem = false;
                int PromoMinQuantity = 0;
                decimal OriginalCasePrice = 0;
                String goyaCompanyID = brokerId.Substring(0, 2);

                dtPriceQOH = getItemQOHDatable(companyId.ToString(), itemCode, customerId, caseQuantity); // Get Item Price, QOH and Authorization details
                if (dtPriceQOH.Rows.Count > 0)
                {
                    // Check Item authorization
                    //if (dtPriceQOH.Rows[0]["ISAUTHORIZED"].ToString() == "1")
                    isRestricted = false;

                    // Check Item is in Promotion or Not
                    if (dtPriceQOH.Rows[0]["ISPROMO"].ToString() == "X")
                        IsPromoItem = true;

                    QOH = Convert.ToInt32(dtPriceQOH.Rows[0]["ITEM_QOH"]);
                    CasePrice = Convert.ToDecimal(dtPriceQOH.Rows[0]["CASEPRICE"]);
                    RetailPrice = Convert.ToDecimal(dtPriceQOH.Rows[0]["RETAILERPRICE"]);
                    UnitForCasePrice = Convert.ToInt32(dtPriceQOH.Rows[0]["PRICEUNIT"]);
                    PromoAmount = Convert.ToDecimal(dtPriceQOH.Rows[0]["PROMOAMOUNT"]);

                    PromoMinQuantity = Convert.ToInt32(dtPriceQOH.Rows[0]["MINQUANTITY"]);
                    OriginalCasePrice = Convert.ToDecimal(dtPriceQOH.Rows[0]["ORIGINALCASEPRICE"]);

                }
                //if (EORno == "0")
                //{
                //    dtEOR = getDtlLastEOR(companyId, "ORDERNO", customerId);
                //    if (dtEOR.Rows.Count > 0)
                //    {
                //        EORno = Convert.ToString(dtEOR.Rows[0]["LastEOR"]);
                //    }
                //}
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetItemOrderItemMobile, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CustomerId", customerId);
                SqlCmd.Parameters.AddWithValue("@SalesmanID", brokerId);
                SqlCmd.Parameters.AddWithValue("@CompanyID", companyId);
                SqlCmd.Parameters.AddWithValue("@Dept", "1");
                SqlCmd.Parameters.AddWithValue("@ProductCode", itemCode);
                SqlCmd.Parameters.AddWithValue("@Cases", caseQuantity);
                SqlCmd.Parameters.AddWithValue("@Units", 0);
                SqlCmd.Parameters.AddWithValue("@LanguageID", "en-US");
                SqlCmd.Parameters.AddWithValue("@CatalogId", 1);
                SqlCmd.Parameters.AddWithValue("@WHID", "01");
                SqlCmd.Parameters.AddWithValue("@Pickup", 0);
                SqlCmd.Parameters.AddWithValue("@Comments", "");
                SqlCmd.Parameters.AddWithValue("@Action", "");
                SqlCmd.Parameters.AddWithValue("@EOR", EORno);
                SqlCmd.Parameters.AddWithValue("@DeliveryDate", deliveryDate);
                SqlCmd.Parameters.AddWithValue("@ClientPONumber", "");
                SqlCmd.Parameters.AddWithValue("@PromoCode", promoCode);
                SqlCmd.Parameters.AddWithValue("@Day", 0);
                SqlCmd.Parameters.AddWithValue("@AmountCollected", amountCollected);
                SqlCmd.Parameters.AddWithValue("@NotDay", 0);
                SqlCmd.Parameters.AddWithValue("@IsRestricted", isRestricted);
                SqlCmd.Parameters.AddWithValue("@QOH", QOH);
                SqlCmd.Parameters.AddWithValue("@CasePrice", CasePrice);
                SqlCmd.Parameters.AddWithValue("@RetailPrice", RetailPrice);
                SqlCmd.Parameters.AddWithValue("@UnitForCasePrice", UnitForCasePrice);

                SqlCmd.Parameters.AddWithValue("@PromoAmount", PromoAmount);
                SqlCmd.Parameters.AddWithValue("@IsPromoItem", IsPromoItem);
                SqlCmd.Parameters.AddWithValue("@PromoMinQuantity", PromoMinQuantity);
                SqlCmd.Parameters.AddWithValue("@OriginalCasePrice", OriginalCasePrice);
                SqlCmd.Parameters.AddWithValue("@OrderMode", orderMode);
                SqlCmd.Parameters.AddWithValue("@UserCompanyId", goyaCompanyID);
                SqlCmd.Parameters.AddWithValue("@MESSAGE", "");

                //  SqlCmd.Parameters.AddWithValue("@Comments", "");

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "getOrderItemList", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "getOrderItemList", false);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string submitOrder(string customerId, int companyId, string brokerId, int catalogId, string day, string notDay, string comments, bool isPickup,string EOR,decimal amountCollected,bool isActive,int totalCaseQuantity,decimal totalAmount,DateTime deliveryDate,string poNo,string productType,string templateName,int totalUnitQuantity,string clientPONumber,decimal Latitude,decimal Longitude,string Location)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcSubmitOrder, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                String basketId = String.Empty;
                basketId = brokerId.PadLeft(6, '0') + System.DateTime.Now.Year.ToString() +
                              System.DateTime.Now.Month.ToString().PadLeft(2, '0') + System.DateTime.Now.Day.ToString().PadLeft(2, '0') +
                              System.DateTime.Now.Hour.ToString().PadLeft(2, '0') + System.DateTime.Now.Minute.ToString().PadLeft(2, '0') +
                              System.DateTime.Now.Second.ToString().PadLeft(2, '0') + System.DateTime.Now.Millisecond.ToString().PadLeft(3, '0');


                SqlCmd.Parameters.AddWithValue("@BasketID", basketId);
                SqlCmd.Parameters.AddWithValue("@CustomerID", customerId);
                SqlCmd.Parameters.AddWithValue("@CompanyID", companyId);
                SqlCmd.Parameters.AddWithValue("@SalesmanID", brokerId);
                SqlCmd.Parameters.AddWithValue("@CatalogId", catalogId);               
                SqlCmd.Parameters.AddWithValue("@Day", day);
                SqlCmd.Parameters.AddWithValue("@NotDay", notDay);
                SqlCmd.Parameters.AddWithValue("@Comments", comments);
                SqlCmd.Parameters.AddWithValue("@Pickup", isPickup);
                SqlCmd.Parameters.AddWithValue("@PONumber", EOR);            
                SqlCmd.Parameters.AddWithValue("@AmountCollected", amountCollected);                
                SqlCmd.Parameters.AddWithValue("@IsActive", isActive);                           
                SqlCmd.Parameters.AddWithValue("@TotQty", totalCaseQuantity);
                SqlCmd.Parameters.AddWithValue("@TotAmount", totalAmount);
                SqlCmd.Parameters.AddWithValue("@DeliveryDate", deliveryDate);
                SqlCmd.Parameters.AddWithValue("@PONo", clientPONumber); // client PO Number      
                SqlCmd.Parameters.AddWithValue("@ProductType", productType);
                SqlCmd.Parameters.AddWithValue("@TemplateName", templateName);
                SqlCmd.Parameters.AddWithValue("@TotUnits", totalUnitQuantity);

                 SqlCmd.Parameters.AddWithValue("@Latitude", Latitude);
                 SqlCmd.Parameters.AddWithValue("@Longitude", Longitude);
                 SqlCmd.Parameters.AddWithValue("@Location", Location);

                SqlCmd.Parameters.AddWithValue("@MESSAGE", "");
                //  SqlCmd.Parameters.AddWithValue("@Comments", "");

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "getOrderItemList", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "getOrderItemList", false);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }
        public void CreateEDIFileFromPurchaseOrder(string strPONumber, string strStoreNumber, string strDealerMessage, string strShipDateFile, string strBCCustNo)
        {
            string strFilePath = string.Empty;

            try
            {
                strFilePath = CreateEDIFile(strPONumber, strStoreNumber, strShipDateFile, strBCCustNo);
                if (strFilePath != "")
                {
                    Console.WriteLine("1. Start Writing File Header");
                    WriteEDIHeaderSegmentToFile(strFilePath, strPONumber, strStoreNumber, strDealerMessage);
                    Console.WriteLine("1. Finish Writing File Header");
                    Console.WriteLine("");

                    Console.WriteLine("2. Start Writing Order Detail");
                    WriteEDIOrderDetailToFile(strFilePath, strPONumber, strStoreNumber, strDealerMessage);
                    Console.WriteLine("2. Finish Writing Order Detail");
                    Console.WriteLine("");
                }
            }
            catch (Exception ex)
            {
                //InsertErrorLogInfo(strSelectedFileName, "CreateEDIFileFromPurchaseOrder()", ex.Message, ex.StackTrace);
            }
        }
        public static void WriteEDIHeaderSegmentToFile(string strFilePath, string strPONumber, string strStoreNumber, string strDealerMessage)
        {
            int index;
            string strEDIStore = string.Empty;
            string strCustomerStoreNo = string.Empty;
            string strCustNo = string.Empty;
            string strCustName = string.Empty;
            string strAddress1 = string.Empty;
            string strSalesmanId = string.Empty;
            string strCity = string.Empty;
            string strState = string.Empty;
            string strZIP = string.Empty;

            string strOrderDate = string.Empty;
            string strShipDate = string.Empty;
            string strCancelDate = string.Empty;
            string strDealerComments = string.Empty;
            string strBasketID = string.Empty;
            string strOrderNumber = string.Empty;
            string strTotalAmount = string.Empty;

            StringBuilder strHeading = new StringBuilder();
            StreamWriter file = new StreamWriter(strFilePath, true);

            StreamWriter objWriter;
            StringBuilder strFileData;
            DataRow[] objdrHeader;
            DataRow[] objdrDefault;

            objdrHeader = null;
            objdrDefault = null;
            objdrHeader = objds.Tables[0].Select("PONumber = '" + strPONumber + "'");
            objdrDefault = objds.Tables[0].Select("PONumber ='" + strPONumber + "'");

            strCustomerStoreNo = Convert.ToString(objdrHeader[0]["WHID"]);
            strEDIStore = Convert.ToString(objdrHeader[0]["WHCompany"]);
            strCustNo = Convert.ToString(objdrHeader[0]["CustomerID"]);
            strCustName = Convert.ToString(objdrHeader[0]["CustomerName"]);
            strAddress1 = Convert.ToString(objdrHeader[0]["Street"]);
            strSalesmanId = Convert.ToString(objdrHeader[0]["SalesmanID"]);
            strCity = Convert.ToString(objdrHeader[0]["City"]);
            strState = Convert.ToString(objdrHeader[0]["State"]);
            strZIP = Convert.ToString(objdrHeader[0]["Zip"]);

            strOrderDate = Convert.ToString(objdrHeader[0]["OrderDate"]);
            strShipDate = Convert.ToString(objdrHeader[0]["DeliveryDate"]);
            strCancelDate = Convert.ToString(objdrHeader[0]["CancelDate"]);
            strDealerComments = Convert.ToString(objdrHeader[0]["Comments"]);
            strBasketID = Convert.ToString(objdrHeader[0]["BasketID"]);
            strOrderNumber = Convert.ToString(objdrHeader[0]["PONumber"]);
            strTotalAmount = Convert.ToString(objdrHeader[0]["ActualAmount"]);
            strTotalAmount = strTotalAmount.Replace(".", "");

            // Start creating Order Number Data
            strHeading.Append("Order*");
            if(objdrDefault.Length > 0 )
            {
                if (Convert.ToString(objdrDefault[0][0]) == strCustNo)
                {
                    strHeading.Append(Convert.ToString(objdrDefault[0][3]) + "~");    //Order Number
                }
                else
                {
                    //CreateAndSaveEDIFileIndexInfo(strPONumber, strStoreNumber, strEDIFileName);
                    strHeading.Append("*" + strFileIndex);
                }
            }
            file.WriteLine(strHeading.ToString());
            // Finish creating Order Number Data

            // Start creating GS Data
            strHeading.Remove(0, strHeading.Length);
            strHeading.Append("GS");
            objdrDefault[0][6] = "*" + strOrderDate + "~";
            if (objdrDefault.Length > 0) //for (index = 0; index < objdrDefault.Length; index++)
            {
                if (Convert.ToString(objdrDefault[0][0]) == strCustNo)
                {
                    strHeading.Append(Convert.ToString(objdrDefault[0][6]));    //Order Date
                }
                else
                {
                    strHeading.Append("*" + strFileIndex);
                }
            }
            file.WriteLine(strHeading.ToString());
            // Finish creating GS Data

            // Start creating ST Data
            strHeading.Remove(0, strHeading.Length);
            strHeading.Append("ST");            //objdrDefault = null;            //objdrDefault = objds.Tables[2].Select("SegmentID ='ST'");
            objdrDefault[0][1] = "*" + strSalesmanId + "~";
            if (objdrDefault.Length > 0) //for (index = 0; index < objdrDefault.Length; index++)
            {
                if (Convert.ToString(objdrDefault[0][0]) == strCustNo)
                {
                    strHeading.Append(Convert.ToString(objdrDefault[0][1]));    //Salesman ID
                }
                else
                {
                    strHeading.Append("*" + strFileIndex);
                }
            }
            file.WriteLine(strHeading.ToString());
            // Finish creating ST Data

            // Start creating BEG Data
            strHeading.Remove(0, strHeading.Length);
            strHeading.Append("BEG");
            objdrDefault[0][22] = "*" + strBasketID + "~";
            if (objdrDefault.Length > 0) //for (index = 0; index < objdrDefault.Length; index++)
            {
                strHeading.Append(Convert.ToString(objdrDefault[0][22]));   //Basket ID
            }
            file.WriteLine(strHeading.ToString());
            // Finish creating BEG Data

            // Start creating REF Data
            strHeading.Remove(0, strHeading.Length);
            strHeading.Append("REF");
            objdrDefault[0][5] = "*" + strCustNo + "~";
            if (objdrDefault.Length > 0) //for (index = 0; index < objdrDefault.Length; index++)
            {
                strHeading.Append(Convert.ToString(objdrDefault[0][5]));    //Customer ID
            }
            file.WriteLine(strHeading.ToString());
            // Start creating REF Data

            // Start creating DTM Data for Shipping date
            strHeading.Remove(0, strHeading.Length);
            strHeading.Append("DTM");
            objdrDefault[0][12] = "*" + strShipDate + "~";
            if (objdrDefault.Length > 0) //for (index = 0; index < objdrDefault.Length; index++)
            {
                strHeading.Append(Convert.ToString(objdrDefault[0][12]));   //Delivery Date
            }
            file.WriteLine(strHeading.ToString());

            strHeading.Remove(0, strHeading.Length);
            strHeading.Append("DTM");
            objdrDefault[0][20] = "*" + strCancelDate + "~";
            if (objdrDefault.Length > 0) //for (index = 0; index < objdrDefault.Length; index++)
            {
                strHeading.Append(Convert.ToString(objdrDefault[0][20]));   //Cancel Date
            }
            file.WriteLine(strHeading.ToString());
            // Finish creating DTM Data

            // Start creating N1 Data
            strHeading.Remove(0, strHeading.Length);
            strHeading.Append("N1");
            objdrDefault[0][25] = "*" + strCustName.Trim() + "*" + strEDIStore + "~";
            if (objdrDefault.Length > 0) //for (index = 0; index < objdrDefault.Length; index++)
            {
                strHeading.Append(Convert.ToString(objdrDefault[0][25]));   //Customer Name
            }
            file.WriteLine(strHeading.ToString());
            // Finish creating N1 Data

            file.Close();
        }
        
        public static void WriteEDIOrderDetailToFile(string strFilePath, string strPONumber, string strStoreNumber, string strDealerMessage)
        {
            int index;
            int LineNo;
            String strTotalOrderQty = String.Empty, str=String.Empty;
            DataRow[] objdr;
            DataRow[] objdrDefault;

            StringBuilder strFileData ;
            StreamWriter objWriter;

            objdr = objds.Tables[0].Select("PONumber = '" + strPONumber + "' ");
            objdrDefault = objds.Tables[0].Select("PONumber ='" + strPONumber + "'");
            if (objdr.Length > 0)
            {
                index = 0;
                LineNo = 0;
                noLineItem = objdr.Length;
                objWriter = new StreamWriter(strFilePath, true);
                for (index = 0; index < objdr.Length; index++)
                {
                    strFileData = new StringBuilder();
                    LineNo = LineNo + 1;
                    totalAmount = totalAmount + Convert.ToDouble(objdr[index]["Total_Value"].ToString());
                    String LineSrl, ProdCode, Qty, CasePrice, UnitForCP;
                    //objdrDefault[0][6] = "*" + objdr[index]["Line_No"].ToString();
                    LineSrl = "*" + Convert.ToString(LineNo);
                    Qty = "*" + objdr[index]["Quantity"].ToString();                    //Store in organisation
                    UnitForCP = "*" + objdr[index]["UnitForCasePrice"].ToString();      //Contact person name
                    CasePrice = "*" + objdr[index]["CasePrice"].ToString();             //Contact person email
                    ProdCode = "*" + objdr[index]["ProductCode"].ToString();            //warehouse code

                    strFileData.Append("Order1");
                    strFileData.Append(Qty);
                    strFileData.Append(UnitForCP);
                    strFileData.Append(CasePrice);
                    strFileData.Append(ProdCode);
                    
                    strFileData.Append("~");
                    objWriter.WriteLine(strFileData.ToString());
                }

                //for (index = 0; index < objdr.Length; index++)
                //{
                //    LineNo = LineNo + 1;
                //    totalAmount = totalAmount + Convert.ToDouble(objdr[index]["Total_Value"].ToString());
                //    //objdrDefault[0][6] = "*" + objdr[index]["Line_No"].ToString();
                //    objdrDefault[index][6] = "*" + Convert.ToString(LineNo);
                //    objdrDefault[index][34] = "*" + objdr[index]["Quantity"].ToString();            //Store in organisation
                //    objdrDefault[index][35] = "*" + objdr[index]["UnitForCasePrice"].ToString();    //Contact person name
                //    objdrDefault[index][36] = "*" + objdr[index]["CasePrice"].ToString();           //Contact person email
                //    objdrDefault[index][37] = "*" + objdr[index]["ProductCode"].ToString();         //wh code

                //    strFileData.Append("Order1");
                //    for (int i = 0; i < objdrDefault.Length; i++)
                //    {
                //        strFileData.Append(Convert.ToString(objdrDefault[i][34]));
                //        strFileData.Append(Convert.ToString(objdrDefault[i][35]));
                //        strFileData.Append(Convert.ToString(objdrDefault[i][36]));
                //        strFileData.Append(Convert.ToString(objdrDefault[i][37]));
                //    }
                //    strFileData.Append("~");
                //    objWriter.WriteLine(strFileData.ToString());
                //}

                // Start creating CTT Data
                strFileData = new StringBuilder();
                strFileData.Append("CTT");
                strTotalOrderQty = Convert.ToString(objdr[0]["TotalOrderQty"]);
                objdrDefault[0][34] = "*" + strTotalOrderQty;
                for (index = 0; index < objdrDefault.Length; index++)
                {
                    if (Convert.ToString(objdrDefault[index][3]) == strPONumber)
                    {
                        strFileData.Append(Convert.ToString(objdrDefault[index][34]));    //Total Order Qty
                    }
                    else
                    {
                        strFileData.Append("*" + strFileIndex);
                    }
                }
                strFileData.Append("~");
                objWriter.WriteLine(strFileData.ToString());
                // Finish creating CTT Data

                // Start creating AMT Data
                strFileData = new StringBuilder();
                strFileData.Append("AMT");
                str = Convert.ToString(objdr[0]["Total_Value"]);
                objdrDefault[0][35] = "*" + str;
                for (index = 0; index < objdrDefault.Length; index++)
                {
                    if (Convert.ToString(objdrDefault[index][3]) == strPONumber)
                    {
                        strFileData.Append(Convert.ToString(objdrDefault[index][35]));    //Total Amount value
                    }
                    else
                    {
                        strFileData.Append("*" + strFileIndex);
                    }
                }
                strFileData.Append("~");
                objWriter.WriteLine(strFileData.ToString());
                // Finish creating AMT Data

                // Start creating SE Data
                strFileData = new StringBuilder();
                strFileData.Append("SE");
                str = Convert.ToString(objdr[0]["WHCompany"]);
                objdrDefault[0][36] = "*" + str;
                for (index = 0; index < objdrDefault.Length; index++)
                {
                    if (Convert.ToString(objdrDefault[index][3]) == strPONumber)
                    {
                        strFileData.Append(Convert.ToString(objdrDefault[index][36]));    //Warehouse Company
                    }
                    else
                    {
                        strFileData.Append("*" + strFileIndex);
                    }
                }
                strFileData.Append("~");
                objWriter.WriteLine(strFileData.ToString());
                // Finish creating SE Data

                // Start creating GE Data
                strFileData = new StringBuilder();
                strFileData.Append("GE");
                str = Convert.ToString(objdr[0]["CompanyID"]);
                objdrDefault[0][37] = "*" + str;
                for (index = 0; index < objdrDefault.Length; index++)
                {
                    if (Convert.ToString(objdrDefault[index][3]) == strPONumber)
                    {
                        strFileData.Append(Convert.ToString(objdrDefault[index][37]));    // Company Id
                    }
                    else
                    {
                        strFileData.Append("*" + strFileIndex);
                    }
                }
                strFileData.Append("~");
                objWriter.WriteLine(strFileData.ToString());
                // Finish creating GE Data

                // Start creating IEA Data
                strFileData = new StringBuilder();
                strFileData.Append("IEA");
                str = Convert.ToString("1");
                objdrDefault[0][4] = "*" + str;
                for (index = 0; index < objdrDefault.Length; index++)
                {
                    if (Convert.ToString(objdrDefault[index][3]) == strPONumber)
                    {
                        strFileData.Append(Convert.ToString(objdrDefault[index][4]));    //Company Id
                    }
                    else
                    {
                        strFileData.Append("*" + strFileIndex);
                    }
                }
                strFileData.Append("~");
                objWriter.WriteLine(strFileData.ToString());
                // Finish creating IEA Data

                strFileData.Remove(0, strFileData.Length);
                objWriter.Close();


            }
        }

        public static string CreateEDIFile(string strPONumber, string strStoreNumber, string strShipDateFile, string strBCCustNo)
        {
            string strFilePath = string.Empty;
            string strFileName = string.Empty;

            try
            {
                FileIndex = FileIndex + 1;
                //strFileName = strPONumber + "-" + strStoreNumber + ".edi";
                strFileName = strPONumber + "_" + strBCCustNo.Trim() + "_" + strEDIFileType + "_" + CreateFileIndexValue(FileIndex) + ".edi";
                strEDIFileName = strFileName;
                strEDIFileDirectory = ApiConstant.FileDirectory;
                strFilePath = strEDIFileDirectory + "/EDI/" + strFileName;

                if (!Directory.Exists(strEDIFileDirectory))
                {
                    Directory.CreateDirectory(strEDIFileDirectory);
                }
                if (File.Exists(strFilePath))
                {
                    File.Delete(strFilePath);
                }
                StreamWriter file = new StreamWriter(strFilePath, true);
                file.Close();
            }
            catch (Exception ex)
            {
                strFilePath = "";
                Console.WriteLine("File Creation Error : " + ex.Message);
                //InsertErrorLogInfo(strSelectedFileName, "CreateEDIFile()", ex.Message, ex.StackTrace);
            }
            return strFilePath;
        }
        public string CreateXMLFile(string strPONumber, string strBCCustNo)
        {
            string strFilePath = string.Empty;
            string strFileName = string.Empty;

            try
            {
                FileIndex = FileIndex + 1;
                strFileName = strPONumber + "_" + strBCCustNo.Trim() + "_" + strEDIFileType + "_" + CreateFileIndexValue(FileIndex) + ".xml";
                strEDIFileName = strFileName;
                strEDIFileDirectory = ApiConstant.FileDirectory;
                strFilePath = strEDIFileDirectory + "/XML/" + strFileName;

                if (!Directory.Exists(strEDIFileDirectory))
                {
                    Directory.CreateDirectory(strEDIFileDirectory);
                }
                if (File.Exists(strFilePath))
                {
                    File.Delete(strFilePath);
                }
                StreamWriter file = new StreamWriter(strFilePath, true);
                file.Close();
            }
            catch (Exception ex)
            {
                strFilePath = "";
                Console.WriteLine("File Creation Error : " + ex.Message);
                //InsertErrorLogInfo(strSelectedFileName, "CreateEDIFile()", ex.Message, ex.StackTrace);
            }
            return strFilePath;
        }
        public static string CreateFileIndexValue(int FileIndex)
        {
            string strFileIndex = string.Empty;

            if (FileIndex > 0 && FileIndex < 10)
            {
                strFileIndex = "0000" + Convert.ToString(FileIndex);
            }
            else if (FileIndex >= 10 && FileIndex < 100)
            {
                strFileIndex = "000" + Convert.ToString(FileIndex);
            }
            else if (FileIndex >= 100 && FileIndex < 1000)
            {
                strFileIndex = "00" + Convert.ToString(FileIndex);
            }
            else if (FileIndex >= 1000 && FileIndex < 10000)
            {
                strFileIndex = "0" + Convert.ToString(FileIndex);
            }
            else
            {
                strFileIndex = Convert.ToString(FileIndex);
            }
            return strFileIndex;
        }
        public string submitQuickOrders(string customerId, int companyId, string brokerId, string EOR, decimal amountCollected, int totalCaseQuantity, decimal totalAmount, DateTime deliveryDate, string ClientPoNo, decimal Latitude, decimal Longitude, string Location)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcSubmitQuickOrder, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                String basketId = String.Empty;
                basketId = brokerId.PadLeft(6, '0') + System.DateTime.Now.Year.ToString() +
                              System.DateTime.Now.Month.ToString().PadLeft(2, '0') + System.DateTime.Now.Day.ToString().PadLeft(2, '0') +
                              System.DateTime.Now.Hour.ToString().PadLeft(2, '0') + System.DateTime.Now.Minute.ToString().PadLeft(2, '0') +
                              System.DateTime.Now.Second.ToString().PadLeft(2, '0') + System.DateTime.Now.Millisecond.ToString().PadLeft(3, '0');

                SqlCmd.Parameters.AddWithValue("@BasketID", basketId);
                SqlCmd.Parameters.AddWithValue("@CustomerID", customerId);
                SqlCmd.Parameters.AddWithValue("@CompanyID", companyId);
                SqlCmd.Parameters.AddWithValue("@SalesmanId", brokerId);
                SqlCmd.Parameters.AddWithValue("@CatalogId", 1);
                SqlCmd.Parameters.AddWithValue("@Day", "0");
                SqlCmd.Parameters.AddWithValue("@NotDay", "0");
                SqlCmd.Parameters.AddWithValue("@Comments", "");
                SqlCmd.Parameters.AddWithValue("@Pickup", 0);
                SqlCmd.Parameters.AddWithValue("@PONumber", EOR);
                SqlCmd.Parameters.AddWithValue("@AmountCollected", amountCollected);
                SqlCmd.Parameters.AddWithValue("@IsActive", 1);
                SqlCmd.Parameters.AddWithValue("@TotQty", totalCaseQuantity);
                SqlCmd.Parameters.AddWithValue("@TotAmount", totalAmount);
                SqlCmd.Parameters.AddWithValue("@DeliveryDate", deliveryDate);
                SqlCmd.Parameters.AddWithValue("@PONo", ClientPoNo); 
                SqlCmd.Parameters.AddWithValue("@ProductType", "111111");
                SqlCmd.Parameters.AddWithValue("@TemplateName", "");
                SqlCmd.Parameters.AddWithValue("@TotUnits", 0);

                SqlCmd.Parameters.AddWithValue("@Latitude", Latitude);
                SqlCmd.Parameters.AddWithValue("@Longitude", Longitude);
                SqlCmd.Parameters.AddWithValue("@Location", Location);
                SqlCmd.Parameters.Add("@MESSAGE", SqlDbType.VarChar, 50);
                SqlCmd.Parameters["@MESSAGE"].Direction = ParameterDirection.Output;
                //SqlParameter p = SqlCmd.Parameters.AddWithValue("@MESSAGE", SqlDbType.VarChar);
                //p.Direction = ParameterDirection.Output;

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                try
                {
                    if(dt != null)
                    {
                        if(dt.Rows.Count > 0)
                        {
                            String message = (string)SqlCmd.Parameters["@MESSAGE"].Value;
                            dbConnectionObj = new DBConnection();
                            SqlCmd = new SqlCommand(ApiConstant.SPGetOrdersByPO, dbConnectionObj.ApiConnection);
                            SqlCmd.CommandType = CommandType.StoredProcedure;
                            strPONumber = message;
                            SqlCmd.Parameters.AddWithValue("@PONumber", strPONumber);
                            SqlCmd.Parameters.AddWithValue("@CompanyId", companyId);
                            da = new SqlDataAdapter(SqlCmd);
                            dbConnectionObj.ConnectionOpen();
                            da.Fill(objds);
                            dbConnectionObj.ConnectionClose();
                            String StoredFileType = Convert.ToString(objds.Tables[0].Rows[0]["StoredFileType"]);
                            if (StoredFileType == "EDI")
                            {
                                Console.WriteLine("Start Creating EDI File For Order : " + strPONumber);
                                CreateEDIFileFromPurchaseOrder(strPONumber, "", "", deliveryDate.ToShortDateString(), customerId);
                                Console.WriteLine("Finish Creating EDI File For Order : " + strPONumber);
                            }
                            if (StoredFileType == "XML")
                            {
                                String strFilePath = String.Empty;
                                strFilePath = CreateXMLFile(strPONumber, Convert.ToString(objds.Tables[0].Rows[0]["CustomerName"]));
                                if (strFilePath != "")
                                {
                                    Console.WriteLine("1. Start Writing File Header Details");
                                    //objds.WriteXml(strFilePath);
                                    List<OrderFiles> ordObj = new List<OrderFiles>();
                                    DataTable dtl = new DataTable();
                                    dtl = objds.Tables[0];                            //ordObj = DataTableToList(dtl);
                                    XmlDocument doc = new XmlDocument();
                                    String ords = string.Empty;
                                    ords = "<CustomerId>" + Convert.ToString(dtl.Rows[0]["CustomerID"]) + "</CustomerId><BrokerId>" + Convert.ToString(dtl.Rows[0]["SalesmanID"]) + "</BrokerId><CompanyId>" + Convert.ToString(dtl.Rows[0]["CompanyID"]) + "</CompanyId><OrderNumber>" + Convert.ToString(dtl.Rows[0]["PONumber"]) + "</OrderNumber><OrderDate>" + Convert.ToString(dtl.Rows[0]["OrderDate"]) + "</OrderDate><DeliveryDate>" + Convert.ToString(dtl.Rows[0]["DeliveryDate"]) + "</DeliveryDate><CancelDate>" + Convert.ToString(dtl.Rows[0]["CancelDate"]) + "</CancelDate><CustomerName>" + Convert.ToString(dtl.Rows[0]["CustomerName"]).Trim() + "</CustomerName><TotalValue>" + Convert.ToString(dtl.Rows[0]["Total_Value"]) + "</TotalValue><TotalOrderQty>" + Convert.ToString(dtl.Rows[0]["TotalOrderQty"]) + "</TotalOrderQty><WhId>" + Convert.ToString(dtl.Rows[0]["WHCompany"]) + "</WhId><BasketID>" + Convert.ToString(dtl.Rows[0]["BasketID"]) + "</BasketID>";
                                    var items = new StringBuilder();
                                    for (int i = 0; i < dtl.Rows.Count; i++ )
                                    {
                                        items.Append("<ItemCode>" + Convert.ToString(dtl.Rows[i]["ProductCode"]) + "</ItemCode><Qty>" + Convert.ToString(dtl.Rows[i]["Quantity"]) + "</Qty><CasePrice>" + Convert.ToString(dtl.Rows[i]["CasePrice"]) + "</CasePrice>");
                                    }
                                    doc.LoadXml(("<Order type='regular' Section='B'>" + ords + items  + "</Order>"));   
                                    doc.Save(strFilePath);
                                    Console.WriteLine("1. Finish Writing File Header Details");
                                }
                            }
                        }
                    }

                }
                catch (Exception ex)
                { }

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "submitQuickOrder", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "submitQuickOrder", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        //public List<OrderFiles> DataTableToList(DataTable dt)
        //{
        //    List<OrderFiles> ordObj = new List<OrderFiles>();
        //    if (dt != null)
        //    {
        //        if (dt.Rows.Count > 0)
        //        {
        //            String Cust, po;
        //            for (int i = 0; i < dt.Rows.Count; i++)
        //            {
        //                Cust = Convert.ToString(dt.Rows[i]["CustomerID"]);
        //                po = Convert.ToString(dt.Rows[i]["PONumber"]);
        //                ordObj.Add(new OrderFiles
        //                {
        //                    CustomerId = Cust,
        //                    CompanyId = Convert.ToInt32(dt.Rows[i]["CompanyID"]),
        //                    BrokerId = Convert.ToString(dt.Rows[i]["SalesmanID"]),
        //                    PONumber = po,
        //                    OrderDate = Convert.ToDateTime(dt.Rows[i]["OrderDate"]),
        //                    DeliveryDate = Convert.ToDateTime(dt.Rows[i]["DeliveryDate"]),
        //                    CancelDate = Convert.ToDateTime(dt.Rows[i]["CancelDate"]),
        //                    CustomerName = Convert.ToString(dt.Rows[i]["CustomerName"]),
        //                    ItemCode = Convert.ToString(dt.Rows[i]["ProductCode"]),
        //                    CaseQuantity = Convert.ToInt32(dt.Rows[i]["Quantity"]),
        //                    CasePrice = Convert.ToDecimal(dt.Rows[i]["CasePrice"]),
        //                    totalAmount = Convert.ToDecimal(dt.Rows[i]["Total_Value"]),
        //                    totalCaseQuantity = Convert.ToInt32(dt.Rows[i]["TotalOrderQty"]),
        //                    WarehouseId = Convert.ToString(dt.Rows[i]["WHCompany"]),
        //                    BasketId = Convert.ToString(dt.Rows[i]["BasketID"])
        //                });
        //            }

        //            XmlTextWriter writer = new XmlTextWriter(ApiConstant.EDIFileDirectory + "\\" + Cust + po +".xml", null);
        //            writer.WriteStartDocument();
        //            writer.WriteStartElement("Order");
        //            int i = 0;
        //            foreach (OrderFiles ords in ordObj)
        //            {
        //                writer.WriteStartElement("Order");
        //                if (i == 0)
        //                {
        //                    writer.WriteElementString("PONumber", ords.PONumber.ToString());
        //                    writer.WriteElementString("OrderDate", ords.OrderDate.ToString());
        //                    writer.WriteElementString("SalesmanID", ords.BrokerId);
        //                    writer.WriteElementString("BasketID", ords.BasketId.ToString());
        //                    writer.WriteElementString("CustomerId", ords.CustomerId.ToString());
        //                    writer.WriteElementString("DeliveryDate", ords.DeliveryDate.ToString());
        //                    writer.WriteElementString("CancelDate", ords.CancelDate.ToString());
        //                    writer.WriteElementString("CustomerName", ords.CustomerName.ToString());
        //                    writer.WriteElementString("TotalCaseQuantity", ords.totalCaseQuantity.ToString());
        //                    writer.WriteElementString("TotalOrderAmount", ords.totalAmount.ToString());
        //                    writer.WriteElementString("Warehouse", ords.WarehouseId.ToString());
        //                    writer.WriteElementString("CompanyId", ords.CompanyId.ToString());
        //                }
        //                writer.WriteElementString("Quantity", ords.CaseQuantity.ToString());
        //                writer.WriteElementString("UnitForCasePrice", ords.UnitForCasePrice.ToString());
        //                writer.WriteElementString("CasePrice", ords.CasePrice.ToString());
        //                writer.WriteElementString("ProductCode", ords.ItemCode.ToString());
        //                i++;
        //                writer.WriteEndElement();
        //            }
        //            writer.WriteEndElement();
        //            writer.WriteEndDocument();

        //        }
        //    }

        //    return ordObj;
        //}

        /// <summary>
        /// Delete Order Item by EOR
        /// </summary>
        /// <returns></returns>
        public string deleteOrderItem(string itemCode, string customerId, string EOR)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcDeleteOrderItem, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@ItemCode", itemCode);
                SqlCmd.Parameters.AddWithValue("@CustomerId", customerId);
                SqlCmd.Parameters.AddWithValue("@EOR", EOR);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "deleteOrderItem",true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "deleteOrderItem",false);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// Delete All Items from Order
        /// </summary>
        /// <returns></returns>
        public string deleteAllOrderItems(string customerId, string EOR)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcDeleteAllOrderItems, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
              
                SqlCmd.Parameters.AddWithValue("@CustomerId", customerId);
                SqlCmd.Parameters.AddWithValue("@EOR", EOR);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "deleteAllOrderItems", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "deleteAllOrderItems", false);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

       /// <summary>
       /// update Item Case Quantity in Order
       /// </summary>
       /// <param name="customerId"></param>
       /// <param name="EOR"></param>
       /// <returns></returns>
        public string updateItemCaseQuantity(int caseQuantity,string itemCode,string customerId,string EOR)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcUpdateItemCaseQuantity, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CaseQuantity", caseQuantity);
                SqlCmd.Parameters.AddWithValue("@ProductCode", itemCode);
                SqlCmd.Parameters.AddWithValue("@CustomerId", customerId);
                SqlCmd.Parameters.AddWithValue("@EOR", EOR);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "updateItemCaseQuantity", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "updateItemCaseQuantity", false);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }
        public string updatePaymentTermCustomerPO(string PaymentTerm, Decimal AmountCollected, string ClientPONumber, string EOR, String CustomerId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SrocUpdateOrderPOPaymentTerm, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@EOR", EOR);
                SqlCmd.Parameters.AddWithValue("@PaymentTerm", PaymentTerm);
                SqlCmd.Parameters.AddWithValue("@AmountCollected", AmountCollected);
                SqlCmd.Parameters.AddWithValue("@ClientPONumber", ClientPONumber);
                SqlCmd.Parameters.AddWithValue("@CustomerId", CustomerId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "updatePaymentTermCustomerPO", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "updatePaymentTermCustomerPO", false);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// Update Item Unit Quantity
        /// </summary>
       
        /// <returns></returns>
        public string updateItemUnitQuantity(int unitQuantity, string itemCode, string customerId, string EOR)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcUpdateItemUnitQuantity, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@UnitQuantity", unitQuantity);
                SqlCmd.Parameters.AddWithValue("@ProductCode", itemCode);
                SqlCmd.Parameters.AddWithValue("@CustomerId", customerId);
                SqlCmd.Parameters.AddWithValue("@EOR", EOR);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "updateItemUnitQuantity", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "updateItemUnitQuantity", false);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// Create Order Using selected Template
        /// </summary>
       
        public string createOrderFromTemplate(string BasketId, int CompanyId, string CustomerId, string BrokerId, string LanguageId,string CurrentEOR,string EOR)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetOrderDataByTemplate, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@Basket_ID", BasketId);
                SqlCmd.Parameters.AddWithValue("@CompanyId", CompanyId);
                SqlCmd.Parameters.AddWithValue("@CustomerId", CustomerId);
                SqlCmd.Parameters.AddWithValue("@SalesmanID", BrokerId);                
                SqlCmd.Parameters.AddWithValue("@LanguageId", LanguageId);
                SqlCmd.Parameters.AddWithValue("@CurrentEOR", CurrentEOR);
                SqlCmd.Parameters.AddWithValue("@EOR", EOR);   
                SqlCmd.Parameters.AddWithValue("@Message", "");      

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "createOrderFromTemplate", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "createOrderFromTemplate", false);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }


        /// <summary>
        /// Get Order Header Detail
        /// </summary>
         
        public string getOrderHeaderDetails(string EOR, string customerId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetOrderHeaderDetail, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CustomerId", customerId);
                SqlCmd.Parameters.AddWithValue("@EOR", EOR);
                

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "getOrderHeaderDetails", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "getOrderHeaderDetails", false);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string getPendingOrderItemDetails(string SalesmanId, string customerId, int CompaniID)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetPendingOrderItemsList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CustomerId", customerId);
                SqlCmd.Parameters.AddWithValue("@SalesmanID", SalesmanId);
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompaniID);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "getPendingOrderItemDetails", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "getPendingOrderItemDetails", false);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string getPendingOrderItems(string SalesmanId, string customerId, int CompaniID)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetPendingOrderItems, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CustomerId", customerId);
                SqlCmd.Parameters.AddWithValue("@SalesmanID", SalesmanId);
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompaniID);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "getPendingOrderItemDetails", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "getPendingOrderItemDetails", false);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public DataTable checkPendingOrderItem(string SalesmanId, string customerId, int CompaniID)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetPendingOrderItemsList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CustomerId", customerId);
                SqlCmd.Parameters.AddWithValue("@SalesmanID", SalesmanId);
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompaniID);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                return dt;

            }
            catch (Exception ex)
            {
                
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
            }
            return null;
        }

        public DataTable checkPendingOrderItemLst(string SalesmanId, string customerId, int CompaniID)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetPendingOrderItems, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CustomerId", customerId);
                SqlCmd.Parameters.AddWithValue("@SalesmanID", SalesmanId);
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompaniID);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                return dt;

            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
            }
            return null;
        }

        // Get Pending Order Count by Customer Id
        public string getPendingOrderCountByCustomerId(string customerId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetPendingOrderCountByCustomerId, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CustomerId", customerId);
                


                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "getPendingOrderCountByCustomerId", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "getPendingOrderCountByCustomerId", false);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        // Get Last Order NO 
        public string getLastEOR(int CompanyId, string PageTitle, string BrokerId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetLastEOR, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);
                SqlCmd.Parameters.AddWithValue("@OrderTitle", PageTitle);
                SqlCmd.Parameters.AddWithValue("@SalesmanId", BrokerId);
                SqlCmd.Parameters.AddWithValue("@MESSAGE", "");



                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "getLastEOR", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "getLastEOR", false);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        // TEST IBM DB
        public string getItemQOH(string CompanyID, string ItemCode, string CustomerID,int CaseQuantity)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            DataTable dt1 = new DataTable();
            try
            {
                //if(ClientName == "Goya")
                //{
                    ////OleDbConnection con = new OleDbConnection(ApiConstant.apiConnectionStringDB2);
                    ////OleDbCommand DB2cmd = new OleDbCommand(ApiConstant.SProcGetQuantityOnHand.ToString(), con);

                    //con = new DB2Connection();
                    //DB2cmd = new SqlCommand(ApiConstant.SProcGetQuantityOnHand, con.ApiConnection);
                    //DB2cmd.CommandType = CommandType.StoredProcedure;

                    dbConnectionObj = new DBConnection();
                    SqlCmd = new SqlCommand(ApiConstant.SProcGetQuantityOnHand, dbConnectionObj.ApiConnection);
                    SqlCmd.CommandType = CommandType.StoredProcedure;

                    ////DB2cmd.Connection = con;
                    String GoyaCompId = CustomerID.Substring(0, 2);

                    SqlCmd.Parameters.AddWithValue("@COMPANYID", GoyaCompId);
                    SqlCmd.Parameters.AddWithValue("@ITEMCODE", ItemCode);
                    SqlCmd.Parameters.AddWithValue("@CUSTOMERID", CustomerID);
                    SqlCmd.Parameters.AddWithValue("@CASEQUANTITY", CaseQuantity.ToString());

                    ////con.Open();
                    ////var dr = DB2cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    ////dt1.Load(dr);
                    ////con.Close();

                    da = new SqlDataAdapter(SqlCmd);
                    dt1 = new DataTable();
                    dbConnectionObj.ConnectionOpen();
                    da.Fill(dt1);
                    dbConnectionObj.ConnectionClose();

                    // Convert Datatable to JSON string
                    jsonString = dtToJson.convertDataTableToJson(dt1, "getItemQOH", true);
                    return jsonString;
                //}
                //else
                //{
                //    dbConnectionObj = new DBConnection();
                //    SqlCmd = new SqlCommand(ApiConstant.SProcGetItemQuantityHand, dbConnectionObj.ApiConnection);

                //    SqlCmd.CommandType = CommandType.StoredProcedure;
                //    ////DB2cmd.Connection = con;

                //    SqlCmd.Parameters.AddWithValue("@COMPANYID", CompanyID);
                //    SqlCmd.Parameters.AddWithValue("@ITEMCODE", ItemCode);
                //    SqlCmd.Parameters.AddWithValue("@CUSTOMERID", CustomerID);
                //    SqlCmd.Parameters.AddWithValue("@CASEQUANTITY", CaseQuantity.ToString());

                //    ////con.Open();
                //    ////var dr = DB2cmd.ExecuteReader(CommandBehavior.CloseConnection);
                //    ////dt1.Load(dr);
                //    ////con.Close();

                //    da = new SqlDataAdapter(SqlCmd);
                //    dt1 = new DataTable();
                //    con.ConnectionOpen();
                //    da.Fill(dt1);
                //    con.ConnectionClose();

                //    // Convert Datatable to JSON string
                //    jsonString = dtToJson.convertDataTableToJson(dt1, "getItemQOH", true);
                //    return jsonString;
                //}
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "getItemQOH", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }
        public DataTable getDtl_ItemQOH(string CompanyID, string ItemCode, string CustomerID,int CaseQuantity)
        {
            SqlDataAdapter da = null;
            DataTable dt1 = new DataTable();
            try
            {
                //if(ClientName == "Goya")
                //{
                    ////OleDbConnection con = new OleDbConnection(ApiConstant.apiConnectionStringDB2);
                    ////OleDbCommand DB2cmd = new OleDbCommand(ApiConstant.SProcGetQuantityOnHand.ToString(), con);
                    dbConnectionObj = new DBConnection();
                    SqlCmd = new SqlCommand(ApiConstant.SProcGetQuantityOnHand, dbConnectionObj.ApiConnection);

                    SqlCmd.CommandType = CommandType.StoredProcedure;

                    //con = new DB2Connection();
                    //DB2cmd = new SqlCommand(ApiConstant.SProcGetQuantityOnHand, con.ApiConnection);
                    //DB2cmd.CommandType = CommandType.StoredProcedure;
                    ////DB2cmd.Connection = con;

                    SqlCmd.Parameters.AddWithValue("@COMPANYID", CompanyID);
                    SqlCmd.Parameters.AddWithValue("@ITEMCODE", ItemCode);
                    SqlCmd.Parameters.AddWithValue("@CUSTOMERID", CustomerID);
                    SqlCmd.Parameters.AddWithValue("@CASEQUANTITY", CaseQuantity.ToString());

                    ////con.Open();
                    ////var dr = DB2cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    ////dt1.Load(dr);
                    ////con.Close();

                    da = new SqlDataAdapter(SqlCmd);
                    dt1 = new DataTable();
                    dbConnectionObj.ConnectionOpen();
                    da.Fill(dt1);
                    dbConnectionObj.ConnectionClose();
                    
                //}
            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnectionObj = null;
                con = null;
                SqlCmd = null;
                da = null;
            }
            return dt1;
        }

        // Get Item QOH, Price and Authorize datatable
        public DataTable getItemQOHDatable(string CompanyID, string ItemCode, string CustomerID, int CaseQuantity)
        {
            SqlDataAdapter da = null;
            //DataTable dt = null;
            string jsonString;

            try
            {
                //con = new DB2Connection();
                //DB2cmd = new SqlCommand(ApiConstant.SProcGetQuantityOnHand, con.ApiConnection);
                //DB2cmd.CommandType = CommandType.StoredProcedure;
                ////OleDbConnection con = new OleDbConnection(ApiConstant.apiConnectionStringDB2);
                ////OleDbCommand DB2cmd = new OleDbCommand(ApiConstant.SProcGetQuantityOnHand.ToString(), con);

                //    SqlCmd = new SqlCommand(ApiConstant.SProcGetLastEOR, dbConnectionObj.ApiConnection);
                //  DB2cmd.CommandText = "select * from goya.csvitmprc where csco='" + csco + "' and csno='" + csNo + "' and item='" + itemNo +"'"; // Get Price by Item
                //DB2cmd.CommandText = "select * from goya.csvitmprc where csco='" + csco + "' and csno='" + csNo + "' ";   // Get Price All Item

                // DB2cmd.CommandText = "Select ITEM_QOH FROM goya.vitemqoh WHERE GOYACO='" + CompanyID + "' and ITEM='" + ItemCode + "' ";   // Get Stock
                ////DB2cmd.Connection = con;

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetQuantityOnHand, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@COMPANYID", CompanyID);
                SqlCmd.Parameters.AddWithValue("@ITEMCODE", ItemCode);
                SqlCmd.Parameters.AddWithValue("@CUSTOMERID", CustomerID);
                SqlCmd.Parameters.AddWithValue("@CASEQUANTITY", CaseQuantity.ToString());

                ////con.Open();
                dbConnectionObj.ConnectionOpen();
                var dr = SqlCmd.ExecuteReader(CommandBehavior.CloseConnection);
                DataTable dt1 = new DataTable();
                dt1.Load(dr);
                dbConnectionObj.ConnectionClose();
                ////con.Close();

                // Convert Datatable to JSON string
                //  jsonString = dtToJson.convertDataTableToJson(dt1, "getItemQOH", true);
                return dt1;

            }
            catch (Exception ex)
            {
                //jsonString = dtToJson.convertDataTableToJson(dt, "getItemQOH", false);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
            }
            return null;
        }
        public DataTable getDtlLastEOR(int CompanyId, string PageTitle, string BrokerId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetLastEOR, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);
                SqlCmd.Parameters.AddWithValue("@OrderTitle", PageTitle);
                SqlCmd.Parameters.AddWithValue("@SalesmanId", BrokerId);
                SqlCmd.Parameters.AddWithValue("@MESSAGE", "");



                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();
                
                return dt;

            }
            catch (Exception ex)
            {
                //jsonString = dtToJson.convertDataTableToJson(dt, "getLastEOR", false);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public void CreateTXTFileFromOrderItems(string strSalesmanId, string strCustomerNo, string strCompanyID, DataTable dtlOrderItems)
        {
            string strFilePath = string.Empty;
            try
            {
                strFilePath = CreateTXTFile(strSalesmanId, strCustomerNo, strCompanyID);
                if (strFilePath != "")
                {
                    //Saving the header part
                    WriteTXTHeaderSegmentToFile(strFilePath, dtlOrderItems, strCustomerNo, strSalesmanId);

                    //Saving the detail or item part
                    WriteTXTOrderDetailToFile(strFilePath, dtlOrderItems, strCustomerNo);
                }
            }
            catch (Exception ex)
            {
                //InsertErrorLogInfo(strSelectedFileName, "CreateTXTFileFromOrderItems()", ex.Message, ex.StackTrace);
            }
        }
        public static string CreateTXTFile(string strSalesmanId, string strCustomerNo, string strCompanyID)
        {
            string strFilePath = string.Empty;
            string strFileName = string.Empty, strFileDate = String.Empty;

            try
            {
                FileIndex = FileIndex + 1;
                strFileDate = Convert.ToString(System.DateTime.Today.ToString("mm-dd-yyyy"));
                strFileName = strCompanyID + strSalesmanId + "_" + strCustomerNo.Trim() + "_" + strFileDate + "_" + CreateFileIndexValue(FileIndex) + ".txt";
                strEDIFileName = strFileName;
                strEDIFileDirectory = ApiConstant.FileDirectory;
                strFilePath = strEDIFileDirectory + "/TXT/" + strFileName;

                if (!Directory.Exists(strEDIFileDirectory))
                {
                    Directory.CreateDirectory(strEDIFileDirectory);
                }
                if (File.Exists(strFilePath))
                {
                    File.Delete(strFilePath);
                }
                StreamWriter file = new StreamWriter(strFilePath, true);
                file.Close();
            }
            catch (Exception ex)
            {
                strFilePath = "";
                Console.WriteLine("File Creation Error : " + ex.Message);
                //InsertErrorLogInfo(strSelectedFileName, "CreateEDIFile()", ex.Message, ex.StackTrace);
            }
            return strFilePath;
        }
        public static void WriteTXTHeaderSegmentToFile(string strFilePath, DataTable OrderTable, String CustomerID, String strSalesmanId)
        {
            int index;
            string strEDIStore = string.Empty;
            string strCustomerStoreNo = string.Empty;
            string strCustNo = string.Empty;
            string strCustName = string.Empty;
            string strAddress1 = string.Empty;
            string strCity = string.Empty;
            string strState = string.Empty;
            string strZIP = string.Empty;

            string strOrderDate = string.Empty;
            string strShipDate = string.Empty;
            string strCancelDate = string.Empty;
            string strDealerComments = string.Empty;
            string strBasketID = string.Empty;
            string strOrderNumber = string.Empty;
            string strTotalAmount = string.Empty;

            StringBuilder strHeading = new StringBuilder();
            StreamWriter file = new StreamWriter(strFilePath, true);

            StreamWriter objWriter;
            StringBuilder strFileData;
            DataRow[] objdrHeader;
            DataRow[] objdrDefault;

            objdrHeader = null;
            objdrDefault = null;
            //objdrHeader = OrderTable.Select("Customer = '" + CustomerID + "' ");  //objds.Tables[0].Select("PONumber = '" + strPONumber + "'");
            //objdrDefault = OrderTable.Select("Customer = '" + CustomerID + "' "); //objds.Tables[0].Select("PONumber ='" + strPONumber + "'");

            //strCustomerStoreNo = Convert.ToString(objdrHeader[0]["WHID"]);
            //strEDIStore = Convert.ToString(objdrHeader[0]["WHCompany"]);
            strCustNo = CustomerID;// Convert.ToString(objdrHeader[0]["CustomerID"]);
            //strCustName = Convert.ToString(objdrHeader[0]["CustomerName"]);
            //strAddress1 = Convert.ToString(objdrHeader[0]["Street"]);
            //strSalesmanId = Convert.ToString(objdrHeader[0]["SalesmanID"]);
            //strCity = Convert.ToString(objdrHeader[0]["City"]);
            //strState = Convert.ToString(objdrHeader[0]["State"]);
            //strZIP = Convert.ToString(objdrHeader[0]["Zip"]);

            //strOrderDate = Convert.ToString(objdrHeader[0]["OrderDate"]);
            //strShipDate = Convert.ToString(objdrHeader[0]["DeliveryDate"]);
            //strCancelDate = Convert.ToString(objdrHeader[0]["CancelDate"]);
            //strDealerComments = Convert.ToString(objdrHeader[0]["Comments"]);
            //strBasketID = Convert.ToString(objdrHeader[0]["BasketID"]);
            //strOrderNumber = Convert.ToString(objdrHeader[0]["PONumber"]);
            //strTotalAmount = Convert.ToString(objdrHeader[0]["ActualAmount"]);
            Decimal TotalAmt = 0;
            int TotalQty = 0;
            for (int i = 0; i < OrderTable.Rows.Count; i++)
            {
                TotalAmt = TotalAmt + Convert.ToDecimal(OrderTable.Rows[i]["ItemPrice"]);
                TotalQty = TotalQty + Convert.ToInt32(OrderTable.Rows[i]["Qty"]);
            }

            strTotalAmount = strTotalAmount.Replace(".", "");

            // Start creating Order Number Data
            strHeading.Append("OrderTotal*");
            if (OrderTable.Rows.Count > 0)
            {
                if (CustomerID == strCustNo)
                {
                    //strHeading.Append(Convert.ToString(objdrDefault[0][3]) + "~");    //Order Number
                    strHeading.Append(Convert.ToString(TotalAmt) + "~" );
                }
                else
                {
                    //CreateAndSaveEDIFileIndexInfo(strPONumber, strStoreNumber, strEDIFileName);
                    strHeading.Append("*" + strFileIndex);
                }
            }
            file.WriteLine(strHeading.ToString());
            // Finish creating Order Number Data

            // Start creating GS Data
            strHeading.Remove(0, strHeading.Length);
            strHeading.Append("OrderDate");
            //objdrDefault[0][6] = "*" + strOrderDate + "~";
            if (OrderTable.Rows.Count > 0) //for (index = 0; index < objdrDefault.Length; index++)
            {
                if (CustomerID == strCustNo) //if (Convert.ToString(objdrDefault[0][0]) == strCustNo)
                {
                    //strHeading.Append(Convert.ToString(objdrDefault[0][6]));    //Order Date
                    strHeading.Append("*" + Convert.ToString(System.DateTime.Today.ToShortDateString()) + "~");
                }
                else
                {
                    strHeading.Append("*" + strFileIndex);
                }
            }
            file.WriteLine(strHeading.ToString());
            // Finish creating GS Data

            // Start creating ST Data
            strHeading.Remove(0, strHeading.Length);
            strHeading.Append("SalesmanID");            //objdrDefault = null;            //objdrDefault = objds.Tables[2].Select("SegmentID ='ST'");
            //objdrDefault[0][1] = "*" + strSalesmanId + "~";
            if (OrderTable.Rows.Count > 0) //for (index = 0; index < objdrDefault.Length; index++)
            {
                if (CustomerID == strCustNo) //if (Convert.ToString(objdrDefault[0][0]) == strCustNo)
                {
                    //strHeading.Append(Convert.ToString(objdrDefault[0][1]));    //Salesman ID
                    strHeading.Append("*" + strSalesmanId + "~");
                }
                else
                {
                    strHeading.Append("*" + strFileIndex);
                }
            }
            file.WriteLine(strHeading.ToString());
            // Finish creating ST Data

            // Start creating BEG Data
            strHeading.Remove(0, strHeading.Length);
            strHeading.Append("TotalQty");
            //objdrDefault[0][22] = "*" + strBasketID + "~";
            if (OrderTable.Rows.Count > 0) //for (index = 0; index < objdrDefault.Length; index++)
            {
                //strHeading.Append(Convert.ToString(objdrDefault[0][22]));   //Basket ID
                strHeading.Append("*" + Convert.ToString(TotalQty) + "~");
            }
            file.WriteLine(strHeading.ToString());
            // Finish creating BEG Data

            //// Start creating REF Data
            //strHeading.Remove(0, strHeading.Length);
            //strHeading.Append("REF");
            //objdrDefault[0][5] = "*" + strCustNo + "~";
            //if (objdrDefault.Length > 0) //for (index = 0; index < objdrDefault.Length; index++)
            //{
            //    strHeading.Append(Convert.ToString(objdrDefault[0][5]));    //Customer ID
            //}
            //file.WriteLine(strHeading.ToString());
            //// Start creating REF Data

            //// Start creating DTM Data for Shipping date
            //strHeading.Remove(0, strHeading.Length);
            //strHeading.Append("DTM");
            //objdrDefault[0][12] = "*" + strShipDate + "~";
            //if (objdrDefault.Length > 0) //for (index = 0; index < objdrDefault.Length; index++)
            //{
            //    strHeading.Append(Convert.ToString(objdrDefault[0][12]));   //Delivery Date
            //}
            //file.WriteLine(strHeading.ToString());

            //strHeading.Remove(0, strHeading.Length);
            //strHeading.Append("DTM");
            //objdrDefault[0][20] = "*" + strCancelDate + "~";
            //if (objdrDefault.Length > 0) //for (index = 0; index < objdrDefault.Length; index++)
            //{
            //    strHeading.Append(Convert.ToString(objdrDefault[0][20]));   //Cancel Date
            //}
            //file.WriteLine(strHeading.ToString());
            //// Finish creating DTM Data

            // Start creating N1 Data
            strHeading.Remove(0, strHeading.Length);
            strHeading.Append("N1");
            //objdrDefault[0][25] = "*" + strCustName.Trim() + "*" + strEDIStore + "~";
            if (OrderTable.Rows.Count > 0) //for (index = 0; index < objdrDefault.Length; index++)
            {
                //strHeading.Append(Convert.ToString(objdrDefault[0][25]));   //Customer Name
                strHeading.Append("*" + CustomerID + "~");
            }
            file.WriteLine(strHeading.ToString());
            // Finish creating N1 Data

            file.Close();
        }

        public static void WriteTXTOrderDetailToFile(string strFilePath, DataTable dtlOrderItems, string strCustomerID)
        {
            int index;
            int LineNo;
            String strTotalOrderQty = String.Empty, str = String.Empty;
            Int32 strTotalQty=0;
            Double strPriceTotal=0;
            //DataRow[] objdr;
            //DataRow[] objdrDefault;

            StringBuilder strFileData;
            StreamWriter objWriter;

            //objdr = dtlOrderItems;// dtlOrderItems.Select("Customer = '" + strCustomerID + "' ");
            //objdrDefault = dtlOrderItems.Select("Customer = '" + strCustomerID + "' ");
            if (dtlOrderItems.Rows.Count > 0)
            {
                index = 0;
                LineNo = 0;
                noLineItem = dtlOrderItems.Rows.Count;
                objWriter = new StreamWriter(strFilePath, true);
                for (index = 0; index < dtlOrderItems.Rows.Count; index++)
                {
                    strFileData = new StringBuilder();
                    LineNo = LineNo + 1;
                    totalAmount = totalAmount + Convert.ToDouble(dtlOrderItems.Rows[index]["ItemPrice"].ToString()) * Convert.ToInt32(dtlOrderItems.Rows[index]["Qty"].ToString());
                    strPriceTotal = strPriceTotal + Convert.ToDouble(dtlOrderItems.Rows[index]["ItemPrice"].ToString());
                    strTotalQty = strTotalQty + Convert.ToInt32(dtlOrderItems.Rows[index]["Qty"].ToString());
                    String LineSrl, ProdCode, Qty, CasePrice, ItemCode;
                    //objdrDefault[0][6] = "*" + objdr[index]["Line_No"].ToString();
                    LineSrl = "*" + Convert.ToString(LineNo);
                    Qty = "*" + dtlOrderItems.Rows[index]["Qty"].ToString();                    //Store in organisation
                    ItemCode = "*" + dtlOrderItems.Rows[index]["ItemCode"].ToString();         //Contact person name
                    CasePrice = "*" + dtlOrderItems.Rows[index]["ItemPrice"].ToString();             //Contact person email
                    ProdCode = "*" + dtlOrderItems.Rows[index]["ItemName"].ToString();            //warehouse code

                    strFileData.Append("Order1");
                    strFileData.Append(Qty);
                    strFileData.Append(ItemCode);
                    strFileData.Append(CasePrice);
                    strFileData.Append(ProdCode);

                    strFileData.Append("~");
                    objWriter.WriteLine(strFileData.ToString());
                }

                //for (index = 0; index < objdr.Length; index++)
                //{
                //    LineNo = LineNo + 1;
                //    totalAmount = totalAmount + Convert.ToDouble(objdr[index]["Total_Value"].ToString());
                //    //objdrDefault[0][6] = "*" + objdr[index]["Line_No"].ToString();
                //    objdrDefault[index][6] = "*" + Convert.ToString(LineNo);
                //    objdrDefault[index][34] = "*" + objdr[index]["Quantity"].ToString();            //Store in organisation
                //    objdrDefault[index][35] = "*" + objdr[index]["UnitForCasePrice"].ToString();    //Contact person name
                //    objdrDefault[index][36] = "*" + objdr[index]["CasePrice"].ToString();           //Contact person email
                //    objdrDefault[index][37] = "*" + objdr[index]["ProductCode"].ToString();         //wh code

                //    strFileData.Append("Order1");
                //    for (int i = 0; i < objdrDefault.Length; i++)
                //    {
                //        strFileData.Append(Convert.ToString(objdrDefault[i][34]));
                //        strFileData.Append(Convert.ToString(objdrDefault[i][35]));
                //        strFileData.Append(Convert.ToString(objdrDefault[i][36]));
                //        strFileData.Append(Convert.ToString(objdrDefault[i][37]));
                //    }
                //    strFileData.Append("~");
                //    objWriter.WriteLine(strFileData.ToString());
                //}

                // Start creating CTT Data
                strFileData = new StringBuilder();
                strFileData.Append("TotalQty");
                strTotalOrderQty = Convert.ToString(strTotalQty);//objdr[0]["TotalOrderQty"]);
                //objdrDefault[0][34] = "*" + strTotalOrderQty;
                ////for (index = 0; index < dtlOrderItems.Length; index++)
                ////{
                    if (strCustomerID != "")
                    {
                        //strFileData.Append(Convert.ToString(objdrDefault[index][34]));    //Total Order Qty
                        strFileData.Append( "*" + strTotalOrderQty);
                    }
                    else
                    {
                        strFileData.Append("*" + strFileIndex);
                    }
                //}
                strFileData.Append("~");
                objWriter.WriteLine(strFileData.ToString());
                // Finish creating CTT Data

                // Start creating AMT Data
                strFileData = new StringBuilder();
                strFileData.Append("PriceTotal");
                //str = Convert.ToString(objdr[0]["CaseTotal"]);
                //objdrDefault[0][35] = "*" + str;
                //for (index = 0; index < objdrDefault.Length; index++)
                //{
                if (strCustomerID != "") //if (Convert.ToString(objdrDefault[index][3]) == strCustomerID)
                    {
                        //strFileData.Append(Convert.ToString(objdrDefault[index][35]));    //Total Amount value
                        strFileData.Append("*" + Convert.ToString(strPriceTotal));
                    }
                    else
                    {
                        strFileData.Append("*" + strFileIndex);
                    }
                //}
                strFileData.Append("~");
                objWriter.WriteLine(strFileData.ToString());
                // Finish creating AMT Data

                // Start creating WHSE Data
                strFileData = new StringBuilder();
                strFileData.Append("OrderTotal");
                //str = Convert.ToString(objdr[0]["WHCompany"]);
                //objdrDefault[0][36] = "*" + str;
                
                    if (strCustomerID != "")
                    {
                        //strFileData.Append(Convert.ToString(objdrDefault[index][36]));    //Warehouse Company
                        strFileData.Append("*" +  Convert.ToString(totalAmount));
                    }
                    else
                    {
                        strFileData.Append("*" + strFileIndex);
                    }
                
                strFileData.Append("~");
                objWriter.WriteLine(strFileData.ToString());
                // Finish creating WHSE Data

                strFileData.Remove(0, strFileData.Length);
                objWriter.Close();


            }
        }

        public DataTable GetOrderDataByCustomerID(String CustID, String SalesID, String CompID, String PONumber, String OnOff, String OrderVerify, String XMLOrderItems)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SPGetOrdersDetailByPOCustomer, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@OrderNumber", PONumber);
                SqlCmd.Parameters.AddWithValue("@CustomerID", CustID);
                SqlCmd.Parameters.AddWithValue("@SalesmanID", SalesID);
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompID);
                SqlCmd.Parameters.AddWithValue("@OnOffline", OnOff);
                SqlCmd.Parameters.AddWithValue("@OrderVerify", OrderVerify);
                SqlCmd.Parameters.AddWithValue("@XML_ItemDtl", XMLOrderItems);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                return dt;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

    }
}