﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using OMSWebApi.Models;
using System.Xml.Serialization;
using Newtonsoft.Json.Linq;
using System.Data.OleDb;
using System.Web.Configuration;
using OMSWebApi.Controllers;
using OMSWebApi.Providers;
using OMSWebApi.Results;
using OMSWebApi.Repository;
using System.Net.Mail;

namespace OMSWebApi.Repository
{
    public class CountryRepository
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        DBConnection dbConnectionObj = null;
        DataToJson dtToJson = new DataToJson();
        SqlCommand com = null;
        SqlDataAdapter da = null;
        DataTable dt = null;

        public List<CountryModel> GetCountryListFromDb()
        {
            List<CountryModel> CountryListObj = null;
            try
            {
                CountryListObj = new List<CountryModel>();
                dbConnectionObj = new DBConnection();
                com = new SqlCommand(ApiConstant.SProcGetCountryList, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                ////Bind CountryModel generic list using LINQ 
                CountryListObj = (from DataRow dr in dt.Rows
                                  select new CountryModel()
                                  {
                                      CountryName = Convert.ToString(dr["CountryName"]),
                                      CountryCode = Convert.ToInt32(dr["CountryCode"])
                                  }).ToList();


            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbConnectionObj = null;
                com = null;
                da = null;
                dt = null;
            }

            return CountryListObj;
        }
    }
}