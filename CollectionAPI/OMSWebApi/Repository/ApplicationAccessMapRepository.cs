﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using OMSWebApi.Models;

namespace OMSWebApi.Repository
{
    public class ApplicationAccessMapRepository
    {
        DBConnectionOAuth dbConnectionObj = null;
        SqlCommand com = null;
        SqlDataAdapter da = null;
        DataTable dt = null;


        #region Get Menu on load event 
        public ApplicationAccessMapping GetAppAccessMapListFromDb(int appId)
        {
            ApplicationAccessMapping listForViewObj = null;
            List<ApplicationAccessMapping> AppAccessMapListObj = null;
            Common common = new Common();
           // List<LookupMappingTable> AccessTypeListObj = null;
           // List<LookupMappingTable> AccessControlListObj = null;

            try
            {
                listForViewObj = new ApplicationAccessMapping();
                AppAccessMapListObj = new List<ApplicationAccessMapping>();
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocGetApplicationAccessMappingList, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@pAppId", appId);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                //Bind ApplicationsModel generic list using LINQ 
                AppAccessMapListObj = (from DataRow dr in dt.Rows

                           select new ApplicationAccessMapping()
                           {
                               AccessId = Convert.ToInt32(dr["AccessId"]),
                               ApplicationId = Convert.ToInt32(dr["ApplicationId"]),
                               ApplicationName = Convert.ToString(dr["ApplicationName"]),
                               ParentAccessId = Convert.ToInt32(dr["ParentAccessId"]),
                               AccessName = Convert.ToString(dr["AccessName"]),
                               Description = Convert.ToString(dr["Description"]),
                               SortOrder = Convert.ToInt32(dr["SortOrder"]),
                               AccessType = Convert.ToInt32(dr["AccessType"]),
                               AccessTypeName = Convert.ToString(dr["AccessTypeName"]),
                               AccessControl = Convert.ToInt32(dr["AccessControl"]),
                               AccessControlName = Convert.ToString(dr["AccessControlName"]),
                               PageURL = Convert.ToString(dr["PageURL"]),           // RK
                               ScreenURL = Convert.ToString(dr["ScreenURL"]),       // RK
                               ReportName = Convert.ToString(dr["ReportName"]),     // RK
                               IsActive = Convert.ToBoolean(dr["IsActive"]),

                               ApplicationList = common.GetGeneralLookupList(Convert.ToString(dr["ApplicationName"]), "Applications"),
                               AccessTypeList = common.GetLookupMappingTableList("AccessType"),
                               AccessControlList = common.GetLookupMappingTableList("AccessControl")

                           }).ToList();

                listForViewObj.AppAccessMapList = AppAccessMapListObj;
                listForViewObj.AccessTypeList = common.GetLookupMappingTableList("AccessType");
                listForViewObj.AccessControlList = common.GetLookupMappingTableList("AccessControl");
                listForViewObj.ApplicationList = common.GetGeneralLookupList(string.Empty, "Applications");
                
            }
            catch (Exception ex)
            {
                
            }
            finally
            {
                dbConnectionObj = null;
                AppAccessMapListObj = null;
                com = null;
                da = null;
                dt = null;
            }

            return listForViewObj;
        }

        //private List<LookupMappingTable> GetLookupMappingTableList(string pName)
        //{
        //    List<LookupMappingTable> listObj = null;
        //    try
        //    {
        //        listObj = new List<LookupMappingTable>();
        //        dbConnectionObj = new DBConnection();
        //        com = new SqlCommand(ApiConstant.SPocGetLookupMappingTableList, dbConnectionObj.ApiConnection);
        //        com.CommandType = CommandType.StoredProcedure;
        //        com.Parameters.AddWithValue("@pName", pName);
        //        da = new SqlDataAdapter(com);
        //        dt = new DataTable();
        //        dbConnectionObj.ConnectionOpen();
        //        da.Fill(dt);
        //        dbConnectionObj.ConnectionClose();

        //        //Bind ApplicationsModel generic list using LINQ 
        //        listObj = (from DataRow dr in dt.Rows

        //                   select new LookupMappingTable()
        //                   {
        //                       CodeId = Convert.ToInt32(dr["CodeId"]),
        //                       Value1 = Convert.ToString(dr["Value"])
        //                   }).ToList();

        //        return listObj; 
        //    }
        //    catch(Exception ex)
        //    {

        //    }
        //    finally
        //    {
        //        dbConnectionObj = null;
        //        listObj = null;
        //        com = null;
        //        da = null;
        //        dt = null;
        //    }

        //    return listObj;
        //}

        //private List<LookupMappingTable> GetApplicationLookupList(string selectedValue)
        //{
        //    List<LookupMappingTable> listObj = null;
        //    try
        //    {
        //        listObj = new List<LookupMappingTable>();
        //        dbConnectionObj = new DBConnection();
        //        com = new SqlCommand(ApiConstant.SPocGetApplicationLookupList, dbConnectionObj.ApiConnection);
        //        com.CommandType = CommandType.StoredProcedure;
        //        da = new SqlDataAdapter(com);
        //        dt = new DataTable();
        //        dbConnectionObj.ConnectionOpen();
        //        da.Fill(dt);
        //        dbConnectionObj.ConnectionClose();

        //        //Bind ApplicationsModel generic list using LINQ 
        //        listObj = (from DataRow dr in dt.Rows

        //                   select new LookupMappingTable()
        //                   {
        //                       CodeId = Convert.ToInt32(dr["ApplicationId"]),
        //                       Value1 = Convert.ToString(dr["ApplicationName"]),
        //                       SelectedValue = selectedValue
        //                   }).ToList();

        //        return listObj;
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    finally
        //    {
        //        dbConnectionObj = null;
        //        listObj = null;
        //        com = null;
        //        da = null;
        //        dt = null;
        //    }

        //    return listObj;
        //}

        #endregion Get Menu on load event

        /// <summary>
        /// Update roles for roleId
        /// </summary>
        /// <param name="pObj"></param>
        /// <returns></returns>
        public bool UpdateMenuToDb(ApplicationAccessMapping pObj)
        {
            bool success = false;
            SqlDataReader reader = null;
            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocUpdateMenuById, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@AccessId", pObj.AccessId);
                com.Parameters.AddWithValue("@ApplicationId", pObj.ApplicationId);
                com.Parameters.AddWithValue("@AccessName", pObj.AccessName);
                com.Parameters.AddWithValue("@Description", pObj.Description);
                com.Parameters.AddWithValue("@PageURL", pObj.PageURL);
                com.Parameters.AddWithValue("@ScreenURL", pObj.ScreenURL);
                com.Parameters.AddWithValue("@ReportName", pObj.ReportName);
                com.Parameters.AddWithValue("@SortOrder", pObj.SortOrder);
                com.Parameters.AddWithValue("@AccessType", pObj.AccessType);
                com.Parameters.AddWithValue("@AccessControl", pObj.AccessControl);
                com.Parameters.AddWithValue("@IsActive", pObj.IsActive);

                dbConnectionObj.ConnectionOpen();
                reader = com.ExecuteReader();
                success = true;

                reader.Close();
                dbConnectionObj.ConnectionClose();
                return success;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                reader = null;
                //  dbConnectionObj = null;
                com = null;
            }

            return success;
        }


        public bool AddNewMenuToDb(ApplicationAccessMapping pObj)
        {
            bool success = false;
            SqlDataReader reader = null;
            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocInsertAppAccessMap, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
              //  com.Parameters.AddWithValue("@AccessId", pObj.AccessId);
                com.Parameters.AddWithValue("@ApplicationId", pObj.ApplicationId);
                com.Parameters.AddWithValue("@AccessName", pObj.AccessName);
                com.Parameters.AddWithValue("@Description", pObj.Description);
                com.Parameters.AddWithValue("@PageURL", pObj.PageURL);
                com.Parameters.AddWithValue("@ScreenURL", pObj.ScreenURL);
                com.Parameters.AddWithValue("@ReportName", pObj.ReportName);
                com.Parameters.AddWithValue("@SortOrder", pObj.SortOrder);
                com.Parameters.AddWithValue("@AccessType", pObj.AccessType);
                com.Parameters.AddWithValue("@AccessControl", pObj.AccessControl);
                com.Parameters.AddWithValue("@CreateDate", pObj.CreateDate);
                com.Parameters.AddWithValue("@IsActive", pObj.IsActive);

                dbConnectionObj.ConnectionOpen();
                reader = com.ExecuteReader();
                success = true;

                reader.Close();
                dbConnectionObj.ConnectionClose();
                return success;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                reader = null;
                //  dbConnectionObj = null;
                com = null;
            }

            return success;
        }


        public bool DeleteMenuFromDb(ApplicationAccessMapping pObj)
        {
            bool success = false;
            SqlDataReader reader = null;
            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocDeleteAppAccessMapById, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@AccessId", pObj.AccessId);

                dbConnectionObj.ConnectionOpen();
                reader = com.ExecuteReader();
                success = true;

                reader.Close();
                dbConnectionObj.ConnectionClose();
                return success;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                reader = null;
                //  dbConnectionObj = null;
                com = null;
            }

            return success;
        }

    }
}