﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using OMSWebApi.Models;
using System.Xml.Serialization;
using Newtonsoft.Json.Linq;
using System.Data.OleDb;
using System.Web.Configuration;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;



namespace OMSWebApi.Repository
{
    public class GuidedSearchRepository
    {
        DataAccess objData = new DataAccess();
        DBConnection dbConnectionObj = null;
        SqlCommand SqlCmd = null;
        DataToJson dtToJson = new DataToJson();

        string strSPName = string.Empty;
        string strJsonString = string.Empty;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // Get Warehouse List
        public string GetWarehouseList()
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetWarehouseList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                string jsonString;
                jsonString = dtToJson.convertDataTableToJson(dt, "GetWarehouseList", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                log.Error("-> Method: GetWarehouseList() " + ex);
            }
            return null;
        }

        // Get Category List
        public string GetCategoryList()
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetProductClass, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                string jsonString;
                jsonString = dtToJson.convertDataTableToJson(dt, "GetCategoryList", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                log.Error("-> Method: GetCategoryList(string brokerId) " + ex);
            }
            return null;
        }


        // Get Sub Category List by Category Id
        public string GetSubCategoryList()
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SprocGetSubCategoryByCategory, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                //SqlCmd.Parameters.AddWithValue("@VendorId", vendorId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                string jsonString;
                jsonString = dtToJson.convertDataTableToJson(dt, "GetSubCategoryList", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                log.Error("-> Method: GetSubCategoryList(string brokerId) " + ex);
            }
            return null;
        }

        // Get Vendor List
        public string GetVendorList()
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetVendorList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                string jsonString;
                jsonString = dtToJson.convertDataTableToJson(dt, "GetCategoryList", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                log.Error("-> Method: GetVendorList() " + ex);
            }
            return null;
        }


        //Get Vendor Group by Vendor ID
        public string GetVendorGroup(string vendorId)
        {

            SqlDataAdapter da = null;
            DataTable dt = null;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SprocGetVendorGroupByVendorID, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@VendorId", vendorId);
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                string jsonString;
                jsonString = dtToJson.convertDataTableToJson(dt, "GetVendorGroup", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                log.Error("-> Method: GetVendorGroup() " + ex);
            }
            return null;
        }

        // Get Product Class (Category) by Vendor ID
        public string GetCategoryByVendorChanged(string vendorId)
        {

            SqlDataAdapter da = null;
            DataTable dt = null;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SprocGetCategoryByVendorID, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@VendorId", vendorId);
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                string jsonString;
                jsonString = dtToJson.convertDataTableToJson(dt, "GetCategoryByVendorChanged", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                log.Error("-> Method: GetCategoryByVendorChanged() " + ex);
            }
            return null;
        }

        // Get Vendor by Product Class (Category Change)
        public string GetVendorByCategoryChanged(string categoryId)
        {

            SqlDataAdapter da = null;
            DataTable dt = null;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SprocGetVendorByCategoryID, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@CategoryId", categoryId);
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                string jsonString;
                jsonString = dtToJson.convertDataTableToJson(dt, "GetVendorByCategoryChanged", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                log.Error("-> Method: GetVendorByCategoryChanged() " + ex);
            }
            return null;
        }


        // Get Sub Category by Product Class (Category Change)
        public string GetSubCategoryByCategoryId(string categoryId)
        {

            SqlDataAdapter da = null;
            DataTable dt = null;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SprocGetSubCategoryByCategoryId, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@CategoryId", categoryId);
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                string jsonString;
                jsonString = dtToJson.convertDataTableToJson(dt, "GetSubCategoryByCategoryId", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                log.Error("-> Method: GetSubCategoryByCategoryId() " + ex);
            }
            return null;
        }



        // Get Vendor Group by  SubCategory Id
        public string GetVendorGroupBySubCategoryId(string vendorId, string categoryId, string subCategoryId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SprocGetVendorGroupBySubCategoryId, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@VendorId", vendorId);
                SqlCmd.Parameters.AddWithValue("@CategoryId", categoryId);
                SqlCmd.Parameters.AddWithValue("@SubCategoryId", subCategoryId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                string jsonString;
                jsonString = dtToJson.convertDataTableToJson(dt, "GetVendorGroupBySubCategoryId", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                log.Error("-> Method: GetVendorGroupBySubCategoryId() " + ex);
            }
            return null;
        }


        // Item Search by Filters
        public string SearchItems(string vendorId, string categoryId, string customerId, string warehouseId, string groupId, string searchText, string mfgUpc, string searchFor, string subCategoryId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetItemsBySearch, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CustomerId", customerId);
                SqlCmd.Parameters.AddWithValue("@VendorId", vendorId);
                SqlCmd.Parameters.AddWithValue("@CategoryId", categoryId);
                SqlCmd.Parameters.AddWithValue("@SubCategoryId", subCategoryId);
                SqlCmd.Parameters.AddWithValue("@WarehouseId", warehouseId);
                SqlCmd.Parameters.AddWithValue("@GroupId", groupId);
                SqlCmd.Parameters.AddWithValue("@SearchText", searchText);
                SqlCmd.Parameters.AddWithValue("@MfgUpc", mfgUpc);
                SqlCmd.Parameters.AddWithValue("@SearchFor", searchFor);


                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                string jsonString;
                jsonString = dtToJson.convertDataTableToJson(dt, "SearchItems", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                log.Error("-> Method: SearchItems() " + ex);
            }
            return null;
        }


        // Add to Cart Item
        public string AddToCart(string customerId, string salesmanId, string companyId, string productId, string UPC1, string warehouseId, int lineNo, int OrdQty, decimal ListPrice, string promoCode, bool isRestricted, decimal retailPrice, decimal promoAmount, bool isPromoItem, int promoMinQuantity, string poNumber, string clientPONumber, string comments, DateTime shippingDate, DateTime orderDate, DateTime cancelDate, string orderMode)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcAddToCart, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CustomerID", customerId);
                SqlCmd.Parameters.AddWithValue("@SalesmanID", salesmanId);
                SqlCmd.Parameters.AddWithValue("@CompanyID", companyId);
                SqlCmd.Parameters.AddWithValue("@ProductId", productId);
                SqlCmd.Parameters.AddWithValue("@UPC1", UPC1);
                SqlCmd.Parameters.AddWithValue("@WarehouseId", warehouseId);
                SqlCmd.Parameters.AddWithValue("@LineNo", lineNo);
                SqlCmd.Parameters.AddWithValue("@OrdQty", OrdQty);
                SqlCmd.Parameters.AddWithValue("@ListPrice", ListPrice);
                SqlCmd.Parameters.AddWithValue("@PromoCode", promoCode);
                SqlCmd.Parameters.AddWithValue("@IsRestricted", isRestricted);
                SqlCmd.Parameters.AddWithValue("@RetailPrice", retailPrice);
                SqlCmd.Parameters.AddWithValue("@PromoAmount", promoAmount);
                SqlCmd.Parameters.AddWithValue("@IsPromoItem", isPromoItem);
                SqlCmd.Parameters.AddWithValue("@PromoMinQuantity", promoMinQuantity);
                SqlCmd.Parameters.AddWithValue("@PONumber", poNumber);
                SqlCmd.Parameters.AddWithValue("@ClientPONumber", clientPONumber);
                SqlCmd.Parameters.AddWithValue("@Comments", comments);
                SqlCmd.Parameters.AddWithValue("@ShippingDate", shippingDate);
                SqlCmd.Parameters.AddWithValue("@OrderDate", orderDate);
                SqlCmd.Parameters.AddWithValue("@CancelDate", cancelDate);
                SqlCmd.Parameters.AddWithValue("@OrderMod", orderMode);
                SqlCmd.Parameters.AddWithValue("@Message", "");

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                string jsonString;
                jsonString = dtToJson.convertDataTableToJson(dt, "SearchItems", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                log.Error("-> Method: SearchItems() " + ex);
            }
            return null;
        }


        // Get Order Data by Order ID
        public string GetOrderDetailsByOrderId(string orderID)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetOrderDetailsByOrderId, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@OrderId", orderID);


                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                string jsonString;
                jsonString = dtToJson.convertDataTableToJson(dt, "GetOrderDetailsByOrderId", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                log.Error("-> Method: GetOrderDetailsByOrderId() " + ex);
            }
            return null;
        }


        // Submit Order
        public string SubmitOrder(string poNumber, string clientPONumber, string salesmanId, string customerId, DateTime orderDate, DateTime shippingDate, string comments,DateTime cancelDate,bool sbTag)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcSubmitOrderFO, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                int sbTagInt = 0;

                if (sbTag == false)
                    sbTagInt = 0;
                else
                    sbTagInt = 1;


                SqlCmd.Parameters.AddWithValue("@PONumber", poNumber);
                SqlCmd.Parameters.AddWithValue("@ClientPONumber", clientPONumber);
                SqlCmd.Parameters.AddWithValue("@SalesmanId", salesmanId);
                SqlCmd.Parameters.AddWithValue("@CustomerID", customerId);
                SqlCmd.Parameters.AddWithValue("@OrderDate", orderDate);
                SqlCmd.Parameters.AddWithValue("@Shippingdate", shippingDate);
                SqlCmd.Parameters.AddWithValue("@Comments", comments);
                SqlCmd.Parameters.AddWithValue("@SBtag", sbTagInt);
                //SqlCmd.Parameters.AddWithValue("@CancelDate", cancelDate);


                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                string jsonString;
                jsonString = dtToJson.convertDataTableToJson(dt, "GetOrderDetailsByOrderId", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                log.Error("-> Method: GetOrderDetailsByOrderId() " + ex);
            }
            return null;
        }


        // Update Order Item Qty
        public string UpdateItemQty(string poNumber, string orderId, int qty)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcUpdateItemQty, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@OrderId", poNumber);
                SqlCmd.Parameters.AddWithValue("@ItemCode", orderId);
                SqlCmd.Parameters.AddWithValue("@OrderQuantity", qty);



                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                string jsonString;
                jsonString = dtToJson.convertDataTableToJson(dt, "UpdateItemQty", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                log.Error("-> Method: UpdateItemQty() " + ex);
            }
            return null;
        }


        // Delete Item from Order
        public string DeleteItem(string poNumber, string itemCode)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcDeleteItem, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@OrderId", poNumber);
                SqlCmd.Parameters.AddWithValue("@ItemCode", itemCode);


                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                string jsonString;
                jsonString = dtToJson.convertDataTableToJson(dt, "DeleteItem", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                log.Error("-> Method: DeleteItem() " + ex);
            }
            return null;
        }

     

        // Order History
        public string OrderHistory(string companyId, string salesmanId, string customerId, DateTime fromDate, DateTime toDate, string status)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcOrderHistoryData, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyCode", Convert.ToInt32(companyId));
                SqlCmd.Parameters.AddWithValue("@SalesmanID", salesmanId);
                SqlCmd.Parameters.AddWithValue("@CustomerID", customerId);
                SqlCmd.Parameters.AddWithValue("@FromDate", fromDate);
                SqlCmd.Parameters.AddWithValue("@ToDate", toDate);
                SqlCmd.Parameters.AddWithValue("@Status", status);


                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                string jsonString;
                jsonString = dtToJson.convertDataTableToJson(dt, "DeleteItem", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                log.Error("-> Method: DeleteItem() " + ex);
            }
            return null;
        }




        // Delete Order
        public string DeleteOrder(string poNumber, string salesmanId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcDeleteOrder, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@OrderId", poNumber);
                SqlCmd.Parameters.AddWithValue("@SalesmanId", salesmanId);


                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                string jsonString;
                jsonString = dtToJson.convertDataTableToJson(dt, "DeleteOrder", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                log.Error("-> Method: DeleteOrder() " + ex);
            }
            return null;
        }

        // Add Item to Order
        public string AddItemToOrder(string customerId, string salesmanId, string companyId, string productId, int OrdQty, string warehouseId, string poNumber, string clientPONumber, string comments)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcAddItemToOrder, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CustomerId", customerId);
                SqlCmd.Parameters.AddWithValue("@SalesmanID", salesmanId);
                SqlCmd.Parameters.AddWithValue("@CompanyID", Convert.ToInt32(companyId));
                SqlCmd.Parameters.AddWithValue("@ProductId", productId);
                SqlCmd.Parameters.AddWithValue("@Quanitity", OrdQty);
                SqlCmd.Parameters.AddWithValue("@WarehouseId", warehouseId);
                SqlCmd.Parameters.AddWithValue("@OrderNo", poNumber);
                SqlCmd.Parameters.AddWithValue("@ClientPONumber", clientPONumber);
                SqlCmd.Parameters.AddWithValue("@Comments", comments);


                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                string jsonString;
                jsonString = dtToJson.convertDataTableToJson(dt, "AddItemToOrder", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                log.Error("-> Method: AddItemToOrder() " + ex);
            }
            return null;
        }

        // Updated Order file to Server
        public string UploadOrderFile(string fileName, string fileData, string fileType, DateTime sentDate, string customerId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                string OrderData = WebConfigurationManager.AppSettings["UploadFileData"];
                string base64BinaryStr = fileData;
                byte[] bytes = Convert.FromBase64String(base64BinaryStr);

                System.IO.FileStream stream = new FileStream(OrderData +customerId+"@"+fileName, FileMode.Append);
                System.IO.BinaryWriter writer =
                new BinaryWriter(stream);
                writer.Write(bytes, 0, bytes.Length);
                writer.Close();

                //DeserailizeByteArrayToDataSet
              //  csvToDatable(OrderData + fileName);

                return "";
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "UploadOrderFile", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        // Convert .csv Order file to Data Table
        private void csvToDatable(string filePath)
        {
            StreamReader sr = new StreamReader(filePath);
            string[] headers = sr.ReadLine().Split(',');
            DataTable dt = new DataTable();
            foreach (string header in headers)
            {
                dt.Columns.Add(header);
            }
            while (!sr.EndOfStream)
            {
                string[] rows = Regex.Split(sr.ReadLine(), ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                DataRow dr = dt.NewRow();
                for (int i = 0; i < headers.Length; i++)
                {
                    dr[i] = rows[i];
                }
                dt.Rows.Add(dr);
            }
          //  return dt;
            
        }

        //// Parse .csv ORDER file and Put in DB
        //private DataSet DeserailizeByteArrayToDataSet(byte[] byteArrayData)
        //{
        //    DataSet tempDataSet = new DataSet();
        //    DataTable dt;
        //    // Deserializing into datatable    
        //    using (MemoryStream stream = new MemoryStream(byteArrayData))
        //    {
        //        BinaryFormatter bformatter = new BinaryFormatter();
        //        dt = (DataTable)bformatter.Deserialize(stream);
        //        if (dt != null)
        //        {
        //            foreach (DataRow row in dt.Rows)
        //            {
        //                Console.WriteLine("----------------------");
        //                Console.WriteLine("PostalCode:" + row["POSTAL_CODE"]);
        //                Console.WriteLine("CountryCode:" + row["COUNTRY_CODE"]);
        //                Console.WriteLine("ProcessNumber:" + row["PROCESS_NUMBER"]);
        //                Console.WriteLine("ProcessStatus:" + row["PROCESS_STATUS"]);
        //                Console.WriteLine("TimeStamp:" + row["TIMESTAMP"]);
        //                Console.WriteLine("----------------------");
        //            }
        //        }
        //    }
        //    // Adding DataTable into DataSet    
        //    tempDataSet.Tables.Add(dt);
        //    return tempDataSet;
        //}
    }
}