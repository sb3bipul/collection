﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OMSWebApi.Models;
using System.Data;
using System.Data.SqlClient;
using OMSWebApi.Controllers;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using System.Data.OleDb;


namespace OMSWebApi.Repository
{
    public class SearchRepository
    {
        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        DBConnection dbConnectionObj = null;
        SqlCommand SqlCmd = null;
        DataToJson dtToJson = new DataToJson();

        public string LoadProduct(string CustomerId)
        {
            List<SearchLoad> Searchload = null;
            SqlDataAdapter da = null;
            DataTable dt = null;

            try
            {
                Searchload = new List<SearchLoad>();
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcProductListByCatalogId, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@CustomerId", CustomerId);
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                string jsonString;
                jsonString = dtToJson.convertDataTableToJson(dt, "LoadProduct",true);
                return jsonString;
            }
            catch (Exception ex)
            {
                log.Error("-> LoadProduct(string CustomerId)", ex);
            }
            finally
            {
                dbConnectionObj = null;
                Searchload = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;

        }

        public string FreeTextSearch(string CustomerId, string UserSearch)
        {
            List<SearchLoad> TextSearch = null;
            SqlDataAdapter da = null;
            DataTable dt = null;

            try
            {
                TextSearch = new List<SearchLoad>();
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcProductListByCatalogId, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@CustomerId", CustomerId);
                SqlCmd.Parameters.AddWithValue("@UserSearch", UserSearch);
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                string jsonString;
                jsonString = dtToJson.convertDataTableToJson(dt, "FreeTextSearch",true);
                return jsonString;
            }
            catch (Exception ex)
            {
                log.Error("-> FreeTextSearch(string CustomerId, string UserSearch)", ex);
            }
            finally
            {
                dbConnectionObj = null;
                TextSearch  = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;

        }

        public string SProcGetOutOfStockItemList(int companyId, DateTime fromDate, DateTime toDate)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetOutOfStockItemList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", companyId);
                SqlCmd.Parameters.AddWithValue("@FromDate", fromDate);
                SqlCmd.Parameters.AddWithValue("@ToDate", toDate);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                //  jsonString = dtToJson.convertRegulartDataToJson(dt);
                jsonString = dtToJson.convertDataTableToJson(dt, "SProcGetOutOfStockItemList", true);

                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertRegulartDataToJson(dt);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }


        /// <summary>
        /// Get Item list from DB2 using company and zone
        /// </summary>
        /// <returns></returns>
        public string GetItemListDB2(string GoyaCompanyID, string BillingCompany, string Zone, string CustomerID,int Limit, int Offset, string SearchText,string IsPromo)
        {
            DataTable dt1 = new DataTable();
            try
            {
                OleDbConnection con = new OleDbConnection(ApiConstant.apiConnectionStringDB2);
                OleDbCommand DB2cmd = new OleDbCommand(ApiConstant.SProcGetProductDataDB2.ToString(), con);

                DB2cmd.CommandType = CommandType.StoredProcedure;
                DB2cmd.Connection = con;

                DB2cmd.Parameters.AddWithValue("@COMPANYID", GoyaCompanyID);
                DB2cmd.Parameters.AddWithValue("@BILLCO", BillingCompany);
                DB2cmd.Parameters.AddWithValue("@ZONE", Zone);
                DB2cmd.Parameters.AddWithValue("@CUSTOMERID", CustomerID);
                DB2cmd.Parameters.AddWithValue("@ROWLIMIT", Limit);
                DB2cmd.Parameters.AddWithValue("@ROWOFFSET", Offset);
                DB2cmd.Parameters.AddWithValue("@SEARCHTEXT", "%"+SearchText+"%");

                if (IsPromo == "" || IsPromo == null)
                    IsPromo = "%";
                

                DB2cmd.Parameters.AddWithValue("@ISPROMO", IsPromo);


                con.Open();

                var dr = DB2cmd.ExecuteReader(CommandBehavior.CloseConnection);

                dt1.Load(dr);
                con.Close();

                string jsonString;
                jsonString = dtToJson.convertDataTableToJson(dt1, "GetItemListDB2", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                log.Error("-> Method: GetItemListDB2  " + ex);
            }
            finally
            {
                dbConnectionObj = null;
                dt1 = null;
            }
            return null;
        }
    }
}