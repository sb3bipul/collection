﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using OMSWebApi.Models;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Web.Configuration;
using System.Transactions;
using System.IO;
using System.Reflection;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using System.Configuration;
using System.Globalization;

namespace OMSWebApi.Repository
{
    public class UIConfigRepository
    {
        DBConnection dbConnectionObj = null;
        DBConnectionConfig dbConnectionConfigObj = null;
        SqlCommand SqlCmd = null;
        DataToJson dtToJson = new DataToJson();
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Insert email config data
        /// </summary>
        public string InsertEmailConfigData(UIConfigModel UiConfig)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_InsertEmailConfigData, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@IsEmailToCompany", UiConfig.IsEmailCompany);
                SqlCmd.Parameters.AddWithValue("@IsEmailToBroker", UiConfig.IsEmailBroker);
                SqlCmd.Parameters.AddWithValue("@IsEmailToCP", UiConfig.IsEmailCP);
                SqlCmd.Parameters.AddWithValue("@IsEmailToCustomer", UiConfig.IsEmailCustomer);
                SqlCmd.Parameters.AddWithValue("@CreatedBy", string.IsNullOrEmpty(UiConfig.CreatedBy) ? DBNull.Value : (object)UiConfig.CreatedBy);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertEmailConfigData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertEmailConfigData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// get data for who can get the payment receipt by email
        /// </summary>
        public string getEmailConfigDatabyCompanyId(SearchCinfigDt SearchConfig)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_getemailconfigByCompanyId, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", (SearchConfig.CompanyId <= 0) ? DBNull.Value : (object)SearchConfig.CompanyId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "getEmailConfigDatabyCompanyId", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "getEmailConfigDatabyCompanyId", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// insert company data from super admin side
        /// </summary>
        public string InsertCompanyData(CompanyMaster Company)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            string Logo = string.Empty;
            string banner = string.Empty;
            string Logoimgpath = string.Empty;
            string Bannerimgpath = string.Empty;
            var saveImageName = "";
            try
            {
                var random = new Random(System.DateTime.Now.Millisecond);
                Logo = Convert.ToString(random.Next(1, 999999));
                banner = Convert.ToString(random.Next(2, 999));

                dbConnectionConfigObj = new DBConnectionConfig();
                SqlCmd = new SqlCommand(ApiConstant.SProc_InsertCompanyData, dbConnectionConfigObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyName", string.IsNullOrEmpty(Company.CompanyName) ? DBNull.Value : (object)Company.CompanyName);
                SqlCmd.Parameters.AddWithValue("@ContactNo", string.IsNullOrEmpty(Company.ContactNo) ? DBNull.Value : (object)Company.ContactNo);
                SqlCmd.Parameters.AddWithValue("@EmailId", string.IsNullOrEmpty(Company.EmailId) ? DBNull.Value : (object)Company.EmailId);
                SqlCmd.Parameters.AddWithValue("@Fax", string.IsNullOrEmpty(Company.Fax) ? DBNull.Value : (object)Company.Fax);
                SqlCmd.Parameters.AddWithValue("@ContactPerson", string.IsNullOrEmpty(Company.ContactPerson) ? DBNull.Value : (object)Company.ContactPerson);
                SqlCmd.Parameters.AddWithValue("@ContactPersonEmail", string.IsNullOrEmpty(Company.ContactPersonEmail) ? DBNull.Value : (object)Company.ContactPersonEmail);
                SqlCmd.Parameters.AddWithValue("@Street", string.IsNullOrEmpty(Company.Street) ? DBNull.Value : (object)Company.Street);
                SqlCmd.Parameters.AddWithValue("@City", string.IsNullOrEmpty(Company.City) ? DBNull.Value : (object)Company.City);
                SqlCmd.Parameters.AddWithValue("@StateId", string.IsNullOrEmpty(Company.StateId) ? DBNull.Value : (object)Company.StateId);
                SqlCmd.Parameters.AddWithValue("@CountryId", string.IsNullOrEmpty(Company.CountryId) ? DBNull.Value : (object)Company.CountryId);
                SqlCmd.Parameters.AddWithValue("@Zip", string.IsNullOrEmpty(Company.Zip) ? DBNull.Value : (object)Company.Zip);
                SqlCmd.Parameters.AddWithValue("@CompanyURL", string.IsNullOrEmpty(Company.CompanyURL) ? DBNull.Value : (object)Company.CompanyURL);

                if (Company.CompanyLogo != null || Company.CompanyLogo != "")
                {
                    System.Drawing.Image img;
                    String StoreImgPath = System.Web.Hosting.HostingEnvironment.MapPath("~/PaymentImage");
                    string imageSavePath = string.Empty;
                    var base64Image = Company.Base64logo;
                    var imageName = Company.CompanyLogo;
                    if (!String.IsNullOrEmpty(base64Image))
                    {
                        saveImageName = Logo + Path.GetExtension(imageName);
                        imageSavePath = StoreImgPath + "\\" + saveImageName;
                        if (!String.IsNullOrEmpty(base64Image))
                        {
                            img = Base64ToImage(Convert.ToString(base64Image));
                            img.Save(imageSavePath);
                        }
                    }

                    Logoimgpath = ApiConstant.PaymentImage;
                    SqlCmd.Parameters.AddWithValue("@CompanyLogo", saveImageName);
                    SqlCmd.Parameters.AddWithValue("@LogoPath", Logoimgpath);
                    saveImageName = "";
                }
                else
                {
                    SqlCmd.Parameters.AddWithValue("@CompanyLogo", DBNull.Value);
                    SqlCmd.Parameters.AddWithValue("@LogoPath", Logoimgpath);
                }

                if (Company.CompanyBanner != null || Company.CompanyBanner != "")
                {
                    System.Drawing.Image img;
                    String StoreImgPath = System.Web.Hosting.HostingEnvironment.MapPath("~/PaymentImage");
                    string imageSavePath = string.Empty;
                    var base64Image = Company.Base64Banner;
                    var imageName = Company.CompanyBanner;
                    if (!String.IsNullOrEmpty(base64Image))
                    {
                        saveImageName = banner + Path.GetExtension(imageName);
                        imageSavePath = StoreImgPath + "\\" + saveImageName;
                        if (!String.IsNullOrEmpty(base64Image))
                        {
                            img = Base64ToImage(Convert.ToString(base64Image));
                            img.Save(imageSavePath);
                        }
                    }

                    Bannerimgpath = ApiConstant.PaymentImage;
                    SqlCmd.Parameters.AddWithValue("@CompanyBanner", saveImageName);
                    SqlCmd.Parameters.AddWithValue("@BannerPath", Bannerimgpath);
                }
                else
                {
                    SqlCmd.Parameters.AddWithValue("@CompanyBanner", DBNull.Value);
                    SqlCmd.Parameters.AddWithValue("@BannerPath", Logoimgpath);
                }

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionConfigObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionConfigObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertCompanyData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertCompanyData", false);
            }
            finally
            {
                dbConnectionConfigObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// get all company data
        /// </summary>
        public string GetCompanyDetails(SearchCompanyList search)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                //dbConnectionObj = new DBConnection();
                dbConnectionConfigObj = new DBConnectionConfig();
                SqlCmd = new SqlCommand(ApiConstant.SProc_Collection_GetAllCompanyDetails, dbConnectionConfigObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyName", string.IsNullOrEmpty(search.CompanyName) ? DBNull.Value : (object)search.CompanyName);
                SqlCmd.Parameters.AddWithValue("@FromDt", search.FromDate == DateTime.MinValue ? DBNull.Value : (object)search.FromDate);
                SqlCmd.Parameters.AddWithValue("@ToDate", search.ToDate == DateTime.MinValue ? DBNull.Value : (object)search.ToDate);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionConfigObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionConfigObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetCompanyDetails", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetCompanyDetails", false);
            }
            finally
            {
                dbConnectionConfigObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// Insert UI Config Data
        /// </summary>
        public string InsertUiConfigData(UIConfig UiCobfig)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                //dbConnectionObj = new DBConnection();
                dbConnectionConfigObj = new DBConnectionConfig();
                SqlCmd = new SqlCommand(ApiConstant.SProc_InsertUIConfigData, dbConnectionConfigObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@ClientName", string.IsNullOrEmpty(UiCobfig.ClientName) ? DBNull.Value : (object)UiCobfig.ClientName);
                SqlCmd.Parameters.AddWithValue("@ClientID", string.IsNullOrEmpty(UiCobfig.ClientID) ? DBNull.Value : (object)UiCobfig.ClientID);
                SqlCmd.Parameters.AddWithValue("@IsERP", UiCobfig.IsERP);
                SqlCmd.Parameters.AddWithValue("@IsOthers", UiCobfig.IsOthers);
                SqlCmd.Parameters.AddWithValue("@CreatedBy", string.IsNullOrEmpty(UiCobfig.CreatedBy) ? DBNull.Value : (object)UiCobfig.CreatedBy);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionConfigObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionConfigObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertUiConfigData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertUiConfigData", false);
            }
            finally
            {
                dbConnectionConfigObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// Insert Collection temp customer data
        /// </summary>
        public string InsertCollectionTempCust(CollectionTempCust tempCust)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_InsertCollectionTempCustomer, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CustName", string.IsNullOrEmpty(tempCust.CustName) ? DBNull.Value : (object)tempCust.CustName);
                SqlCmd.Parameters.AddWithValue("@EmailId", string.IsNullOrEmpty(tempCust.EmailId) ? DBNull.Value : (object)tempCust.EmailId);
                SqlCmd.Parameters.AddWithValue("@ContactNo", string.IsNullOrEmpty(tempCust.ContactNo) ? DBNull.Value : (object)tempCust.ContactNo);
                SqlCmd.Parameters.AddWithValue("@Address", string.IsNullOrEmpty(tempCust.Address) ? DBNull.Value : (object)tempCust.Address);
                SqlCmd.Parameters.AddWithValue("@Status", string.IsNullOrEmpty(tempCust.Status) ? DBNull.Value : (object)tempCust.Status);
                SqlCmd.Parameters.AddWithValue("@CompanyId", (tempCust.CompanyId <= 0) ? DBNull.Value : (object)tempCust.CompanyId);
                SqlCmd.Parameters.AddWithValue("@CreatedBy", string.IsNullOrEmpty(tempCust.CreatedBy) ? DBNull.Value : (object)tempCust.CreatedBy);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertCollectionTempCust", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertCollectionTempCust", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// base 64 string to image convaretion
        /// </summary>
        /// <param name="base64String"></param>
        /// <returns></returns>
        public System.Drawing.Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
            return image;
        }

        /// <summary>
        /// Get Customer Config Data By Client Name
        /// </summary>
        /// <param name="searchCustClient"></param>
        /// <returns></returns>
        public string GetCustConfigDataByClientName(SearchCustConfig searchCustClient)
        {
            SqlDataAdapter da = null;
            DataSet ds = null;
            string jsonString;
            try
            {

                //dbConnectionObj = new DBConnection();
                dbConnectionConfigObj = new DBConnectionConfig();
                SqlCmd = new SqlCommand(ApiConstant.SProc_Collection_GetCustConfigData, dbConnectionConfigObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@ClientName", string.IsNullOrEmpty(searchCustClient.ClientName) ? DBNull.Value : (object)searchCustClient.ClientName);

                da = new SqlDataAdapter(SqlCmd);
                ds = new DataSet();
                dbConnectionConfigObj.ConnectionOpen();
                da.Fill(ds);
                dbConnectionConfigObj.ConnectionClose();
                ds.Tables[0].TableName = "CustConfig";
                ds.Tables[1].TableName = "PaymentMethodConfig";
                jsonString = dtToJson.convertDataSetToJson(ds, "GetCustConfigDataByClientName", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(ds.Tables[0], "GetCustConfigDataByClientName", false);
            }
            finally
            {
                dbConnectionConfigObj = null;
                SqlCmd = null;
                da = null;
                ds = null;
            }
            return null;
        }

        /// <summary>
        /// update company details
        /// </summary>
        /// <param name="updateComp"></param>
        /// <returns></returns>
        public string UpdateCompanyDetails(UpdateCompany updateComp)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                //dbConnectionObj = new DBConnection();
                dbConnectionConfigObj = new DBConnectionConfig();
                SqlCmd = new SqlCommand(ApiConstant.SProc_UpdateCompanyDetails, dbConnectionConfigObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyName", string.IsNullOrEmpty(updateComp.CompanyName) ? DBNull.Value : (object)updateComp.CompanyName);
                SqlCmd.Parameters.AddWithValue("@ContactNo", string.IsNullOrEmpty(updateComp.ContactNo) ? DBNull.Value : (object)updateComp.ContactNo);
                SqlCmd.Parameters.AddWithValue("@Emailid", string.IsNullOrEmpty(updateComp.Emailid) ? DBNull.Value : (object)updateComp.Emailid);
                SqlCmd.Parameters.AddWithValue("@Fax", string.IsNullOrEmpty(updateComp.Fax) ? DBNull.Value : (object)updateComp.Fax);
                SqlCmd.Parameters.AddWithValue("@ContactPerson", string.IsNullOrEmpty(updateComp.ContactPerson) ? DBNull.Value : (object)updateComp.ContactPerson);
                SqlCmd.Parameters.AddWithValue("@ContactPersonEmail", string.IsNullOrEmpty(updateComp.ContactPersonEmail) ? DBNull.Value : (object)updateComp.ContactPersonEmail);
                SqlCmd.Parameters.AddWithValue("@CompanyURL", string.IsNullOrEmpty(updateComp.CompanyURL) ? DBNull.Value : (object)updateComp.CompanyURL);
                SqlCmd.Parameters.AddWithValue("@Street", string.IsNullOrEmpty(updateComp.Street) ? DBNull.Value : (object)updateComp.Street);
                SqlCmd.Parameters.AddWithValue("@City", string.IsNullOrEmpty(updateComp.City) ? DBNull.Value : (object)updateComp.City);
                SqlCmd.Parameters.AddWithValue("@StateId", string.IsNullOrEmpty(updateComp.StateId) ? DBNull.Value : (object)updateComp.StateId);
                SqlCmd.Parameters.AddWithValue("@CountryId", string.IsNullOrEmpty(updateComp.CountryId) ? DBNull.Value : (object)updateComp.CountryId);
                SqlCmd.Parameters.AddWithValue("@Zip", string.IsNullOrEmpty(updateComp.Zip) ? DBNull.Value : (object)updateComp.Zip);
                SqlCmd.Parameters.AddWithValue("@CompanyId", (updateComp.CompanyId <= 0) ? DBNull.Value : (object)updateComp.CompanyId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionConfigObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionConfigObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "UpdateCompanyDetails", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "UpdateCompanyDetails", false);
            }
            finally
            {
                dbConnectionConfigObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// Change Company Status
        /// </summary>
        /// <param name="CompanyStatus"></param>
        /// <returns></returns>
        public string ChangeCompanyStatus(ChangeCompanyStatus CompanyStatus)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                //dbConnectionObj = new DBConnection();
                dbConnectionConfigObj = new DBConnectionConfig();
                SqlCmd = new SqlCommand(ApiConstant.SProc_ChangeCompanyStatus, dbConnectionConfigObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", (CompanyStatus.CompanyId <= 0) ? DBNull.Value : (object)CompanyStatus.CompanyId);
                SqlCmd.Parameters.AddWithValue("@Status", CompanyStatus.Status);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionConfigObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionConfigObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "ChangeCompanyStatus", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "ChangeCompanyStatus", false);
            }
            finally
            {
                dbConnectionConfigObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// Delete Company Details
        /// </summary>
        /// <param name="DelCompany"></param>
        /// <returns></returns>
        public string DeleteCompanyDetails(DeleteCompany DelCompany)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                //dbConnectionObj = new DBConnection();
                dbConnectionConfigObj = new DBConnectionConfig();
                SqlCmd = new SqlCommand(ApiConstant.SProc_DeleteCompanyDetails, dbConnectionConfigObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", (DelCompany.CompanyId <= 0) ? DBNull.Value : (object)DelCompany.CompanyId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionConfigObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionConfigObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "DeleteCompanyDetails", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "DeleteCompanyDetails", false);
            }
            finally
            {
                dbConnectionConfigObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// Get Company List For UI Config
        /// </summary>
        /// <returns></returns>
        public string GetCompanyListForUIConfig()
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                //dbConnectionObj = new DBConnection();
                dbConnectionConfigObj = new DBConnectionConfig();
                SqlCmd = new SqlCommand(ApiConstant.SProc_GetCompanyListForUIConfig, dbConnectionConfigObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionConfigObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionConfigObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetCompanyListForUIConfig", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetCompanyListForUIConfig", false);
            }
            finally
            {
                dbConnectionConfigObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// Insert App UI Config Data
        /// </summary>
        /// <param name="AppUiConfig"></param>
        /// <returns></returns>
        public string InsertAppUIConfigData(AppUIConfig AppUiConfig)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionConfigObj = new DBConnectionConfig();
                //dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_InsertCollectionCustomUX, dbConnectionConfigObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@ClientName", string.IsNullOrEmpty(AppUiConfig.ClientName) ? DBNull.Value : (object)AppUiConfig.ClientName);
                SqlCmd.Parameters.AddWithValue("@ThemeColor", string.IsNullOrEmpty(AppUiConfig.Themecolor) ? DBNull.Value : (object)AppUiConfig.Themecolor);
                SqlCmd.Parameters.AddWithValue("@Currency", string.IsNullOrEmpty(AppUiConfig.Currency) ? DBNull.Value : (object)AppUiConfig.Currency);
                SqlCmd.Parameters.AddWithValue("@IsDisplayInvoice", AppUiConfig.IsDisplayInvoice);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionConfigObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionConfigObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertAppUIConfigData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertAppUIConfigData", false);
            }
            finally
            {
                dbConnectionConfigObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// Get App UI Config Data By Client Name
        /// </summary>
        /// <param name="AppUiConfig"></param>
        /// <returns></returns>
        public string GetAppUIConfigDataByClientName(AppUIConfig AppUiConfig)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionConfigObj = new DBConnectionConfig();
                //dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_GetAppUIConfigData_ByClientName, dbConnectionConfigObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@ClientName", string.IsNullOrEmpty(AppUiConfig.ClientName) ? DBNull.Value : (object)AppUiConfig.ClientName);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionConfigObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionConfigObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetAppUIConfigDataByClientName", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetAppUIConfigDataByClientName", false);
            }
            finally
            {
                dbConnectionConfigObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// Insert Payment method config
        /// </summary>
        /// <param name="PaymentConfig"></param>
        /// <returns></returns>
        public string InsertPaymentmethodconfig(PaymentMethodConfig PaymentConfig)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionConfigObj = new DBConnectionConfig();
                //dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_InsertPaymentMethodConfigdata, dbConnectionConfigObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@ClientName", string.IsNullOrEmpty(PaymentConfig.ClientName) ? DBNull.Value : (object)PaymentConfig.ClientName);
                SqlCmd.Parameters.AddWithValue("@PaymentMethod", string.IsNullOrEmpty(PaymentConfig.PaymentMethod) ? DBNull.Value : (object)PaymentConfig.PaymentMethod);
                SqlCmd.Parameters.AddWithValue("@Name", string.IsNullOrEmpty(PaymentConfig.Name) ? DBNull.Value : (object)PaymentConfig.Name);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionConfigObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionConfigObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertPaymentmethodconfig", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertPaymentmethodconfig", false);
            }
            finally
            {
                dbConnectionConfigObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }


        public string GetPaymentMethodByClientName(PaymentMethodConfig PmConfig)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                SqlConnection Sqlcon = new SqlConnection(ApiConstant.apiConnectionStringConfig);
                Sqlcon.Open();
                String Sqlquery = @"select PaymentMethod as id, Name as name from tbl_Collection_PaymentMethodConfig where ClientName=@ClientName";
                SqlCommand Sqlcommand = new SqlCommand(Sqlquery, Sqlcon);
                Sqlcommand.Parameters.Add("@ClientName", PmConfig.ClientName);
                Sqlcommand.ExecuteNonQuery();
                da = new SqlDataAdapter(Sqlcommand);
                dt = new DataTable();
                da.Fill(dt);
                Sqlcon.Close();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetPaymentMethodByClientName", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetPaymentMethodByClientName", false);
            }
            finally
            {
                da = null;
                dt = null;
            }
            return null;
        }

        public string deletepaymentmethodbyclientname(PaymentMethodConfig PmConfig)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                SqlConnection Sqlcon = new SqlConnection(ApiConstant.apiConnectionStringConfig);
                Sqlcon.Open();
                String Sqlquery = @"DELETE FROM tbl_Collection_PaymentMethodConfig WHERE ClientName=@ClientName";
                SqlCommand Sqlcommand = new SqlCommand(Sqlquery, Sqlcon);
                Sqlcommand.Parameters.Add("@ClientName", PmConfig.ClientName);
                Sqlcommand.ExecuteNonQuery();
                da = new SqlDataAdapter(Sqlcommand);
                dt = new DataTable();
                da.Fill(dt);
                Sqlcon.Close();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "deletepaymentmethodbyclientname", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "deletepaymentmethodbyclientname", false);
            }
            finally
            {
                da = null;
                dt = null;
            }
            return null;
        }
    }
}