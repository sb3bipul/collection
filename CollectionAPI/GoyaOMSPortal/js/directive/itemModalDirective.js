define([], function(app){
    'use strict';

function itemModal($rootScope){
	return{
		restrict: 'C',
		link: function($scope,element,attribute){
		element.bind('click',function ($event) {
			//console.log($($event.target).closest('.orderCode').length);
			if($($event.target).closest('.mb-lahd1').length == 1 || $($event.target).closest('.right_bord').length == 1 || $($event.target).closest('.newfldd').length ==1 || $($event.target).closest('.orderCode').length ==1){
				$('#modalShowItem').css('display','block');
				$('#modalShowOrderDetails').css('display','block');
				if($('.net').height()>=750){
					if($($event.target).offset().top+$('#modalShowItem').height()>$('.net').height()){
						$('#modalShowItem').css({left:$($event.target).offset().left+70,top:$($event.target).offset().top-350});
						$('#modalShowOrderDetails').css({left:$($event.target).offset().left+70,top:$($event.target).offset().top-350});
					}else{
			           $('#modalShowItem').css({left:$($event.target).offset().left+70,top:$($event.target).offset().top-100});
			           $('#modalShowOrderDetails').css({left:$($event.target).offset().left+70,top:$($event.target).offset().top-100});
	
					}

				}else{
				 $('#modalShowItem').css({left:$($event.target).offset().left+70,top:$($event.target).offset().top-190+$('.net').height()-$($event.target).offset().top});
				 $('#modalShowOrderDetails').css({left:$($event.target).offset().left+120,top:$($event.target).offset().top});
				}
			   
			}
			$event.stopPropagation();
		})
		$(document).ready(function () {
			$(document).click(function (e) {
				e.stopImmediatePropagation();
				if($(e.target).closest('.indexSet').length == 1 && $(e.target).closest('.disj').length != 1 && $(e.target).closest('.gh').length != 1){

				}
				if($(e.target).closest('.disj').length == 1 || $(e.target).closest('.gh').length == 1){
					$('#modalShowItem').css('display','none');
					$('#modalShowOrderDetails').css('display','none');
				}
				
			})
			 $('.box-itmessnw').hover(function() {
			        $(".zoomT").addClass('transition');
			    
			    }, function() {
			        $(".zoomT").removeClass('transition');
			    });
			// body...
		})		
			
		}
	}
}
itemModal.$inject = ['$rootScope'];
return itemModal;
});
