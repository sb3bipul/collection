define([], function(app) {
    'use strict';

    function loginController($scope,$rootScope,requestManager, $state,setUser,checkAuthorization,FactoryIndexedDBLoad,labelSet,setHeaderItem,messageSending) {
        var vm = this;
          vm.checkMsz=true;
        function initRootScope() {
          localStorage.setItem('lang','English');
          if(!localStorage['access_token']){
             $rootScope.login = false;
          }

          if(messageSending.setMessage()!= undefined){
            vm.mailMesssage = messageSending.setMessage();

            console.log(vm.mailMesssage);
          }
          
           
            vm.authMsg = false;

        }
        function checkUserAuth(){
            if(localStorage['access_token']){             
                $state.go('home');
            }
            setHeaderItem.setItems();
        }
        checkUserAuth();
        initRootScope();

        function goBack() {
            localStorage.setItem('path','/login');
        }
        goBack();
         function getLanguage() {
              var req = {
                ApplicationName:"Goya OMS",
                ObjectName:"%%",
                LanguageName:"%%"
              }
             requestManager.Language(req).then(function (res) {
               console.log('set lan'+JSON.stringify(res.Payload));
               localStorage.setItem('Labels',JSON.stringify(res.Payload));
               setLan();
               
             })
            }
            getLanguage();
        
        vm.submitRequest = {
            grant_type: 'password',
            userId: '',
            password: ''
        }
        function setLan() {
          var lan = 'English';
         labelSet.getLabel(lan,"Login");
         vm.langgs = labelSet.setLabels();
        }
        $scope.$on('setlang',function (evt,data) {
         labelSet.getLabel(data,"Login");
         vm.langgs = labelSet.setLabels();
        });
     //$rootScope.isLoading = true;
        vm.checkLogin = function(req) {

           /* var auth = {
                user_id:req.userId
            }*/
            //console.log(JSON.stringify(req))
            localStorage.setItem('user_id',req.userId);
            $rootScope.isLoading = true;  
            requestManager.loginRequest(req).then(function(result) {
                console.log('ress'+JSON.stringify(result));
                $rootScope.isLoading = false;  
               if(result.Payload[0].Authorization == 'Success'){
                 vm.permission();
                    vm.text = result.Payload[0].token_type;
               $rootScope.isLoading = false;
                vm.userName = result.Payload[0].userFullName;
               // console.log(vm.text);

                vm.Token_Type = vm.text.substring(0, 1).toUpperCase() + vm.text.substring(1);
                localStorage.setItem("access_token", result.Payload[0].access_token);
                localStorage.setItem("companyName", result.Payload[0].companyName);
                localStorage.setItem("role", result.Payload[0].role);
                localStorage.setItem("email", result.Payload[0].email);
                localStorage.setItem("phone", result.Payload[0].phone);
                localStorage.setItem("token_type", vm.Token_Type);
                localStorage.setItem("userName",vm.userName);
                localStorage.setItem("goyaCompanyId",result.Payload[0].companyId);

                var splitName = vm.userName.split(" ");
                if(splitName.length == 2){
                  var first_letter = splitName[0].substring(0,1);
                  var last_letter = splitName[1].substring(0,1);
                   console.log(first_letter +""+last_letter)
                }
                if(splitName.length == 3){
                  var first_letter = splitName[0].substring(0,1);
                  var last_letter = splitName[2].substring(0,1);
                }
                if(splitName.length == 1){
                  var first_letter = vm.userName.substring(0,1);
                  var last_letter = vm.userName.substring(1,2);
                }
                var s = result.Payload[0].userName.substring(2, result.Payload[0].userName.length);
                //var firt_name_letter = result.Payload[0].userFullName.splice(0,1);
                var setCashObj = {
                  "token":result.Payload[0].access_token,
                  "userName":result.Payload[0].userName,
                  "brokerid":s,
                  "fullname":result.Payload[0].userFullName,
                  "fullnameinitials":first_letter+last_letter,
                  "billingco":result.Payload[0].companyId,
                  "userroles":[{"type":"http://schemas.microsoft.com/ws/2008/06/identity/claims/role","value":result.Payload[0].role}]
                  
                }
                console.log(setCashObj)
                localStorage.setItem("ls.goyabrokerportalauthdata",JSON.stringify(setCashObj));
                
                vm.authMsg = false;
                vm.getEORnumber();
                setUser.checkUser(vm.userName);
                vm.customerIndexDbData();
              /*  auth['access_token'] = result.Payload[0].access_token;
                auth['token_type'] = vm.Token_Type;
                localStorage.setItem('auth',JSON.stringify(auth));*/
                //localStorage.setItem("user_id", '013030');

                vm.getProfile();
               }else{
                 vm.mailMesssage = '';
                vm.authMsg = true;
               }
                
            });
            
            vm.customerIndexDbData = function () {
                $rootScope.isLoading = true; 

                  var req = {
                BrokerId: localStorage['user_id'],
                GoyaCompanyId:localStorage['goyaCompanyId']
            }

                requestManager.customerDetailPost(req).then(function(result) {
                    $rootScope.isLoading = false;
                    vm.custDet = result;
                    vm.searchItemData();
                })
            }
            vm.searchItemData = function () {
                var req = {
                CustomerId: '816625'
            }
            $rootScope.isLoading = true;
                requestManager.ItemList(req).then(function(result) {
                     $rootScope.isLoading = false;
                     vm.searchItemData = result;
                     vm.getDepartmentDetails();
                     
                })
            }
            vm.getDepartmentDetails = function() 
           {
          $rootScope.isLoading = true;
            requestManager.GetDepartmentsPost().then(function(result) {
                $rootScope.isLoading = false;  
                if (result.Payload) {
                    vm.Departments = result;  
                    //console.log('customerDetail Payload: ' + JSON.stringify(vm.Departments));
                     setDataIndex(vm.custDet,vm.searchItemData,vm.check_per,vm.Departments);                 
                     addOrder();

                }
            });
        }
            function addOrder() {
            FactoryIndexedDBLoad.addOrderIndexdb();

            }
            function setDataIndex(custData,searchData,per,dept){
               console.log('enterrr');
                //debugger;
                 var permissionJSON = {"Domain":"GetToken","Event":"GetToken","ClientData":"test2","Status":"True","Payload":[{"AccessId":"6","AccessName":"Home","Description":"Home Page","AccessType":"Common","AccessControl":"Menu","IsAccess":"True"},{"AccessId":"7","AccessName":"About Us","Description":"About Us Page","AccessType":"Common","AccessControl":"Menu","IsAccess":"True"},{"AccessId":"8","AccessName":"Contact Us","Description":"Contact Us Page","AccessType":"Common","AccessControl":"Menu","IsAccess":"True"},{"AccessId":"9","AccessName":"Login","Description":"Login page  Authentication","AccessType":"Common","AccessControl":"Menu","IsAccess":"True"},{"AccessId":"10","AccessName":"Sign Up","Description":"Sign up page  new user","AccessType":"Common","AccessControl":"Menu","IsAccess":"True"},{"AccessId":"11","AccessName":"Login","Description":"Login Button","AccessType":"Common","AccessControl":"Button","IsAccess":"True"},{"AccessId":"12","AccessName":"Forgot Password?","Description":"Forgot Password link","AccessType":"Common","AccessControl":"Link","IsAccess":"True"},{"AccessId":"13","AccessName":"Customers","Description":"Customer Page","AccessType":"Required_Access","AccessControl":"Menu","IsAccess":"True"},{"AccessId":"14","AccessName":"Create Order","Description":"Quick Order Entry Page","AccessType":"Required_Access","AccessControl":"Menu","IsAccess":"True"},{"AccessId":"15","AccessName":"Order History","Description":"Order History Page","AccessType":"Required_Access","AccessControl":"Menu","IsAccess":"True"},{"AccessId":"16","AccessName":"Search Items","Description":"Search Items Page","AccessType":"Required_Access","AccessControl":"Menu","IsAccess":"True"},{"AccessId":"17","AccessName":"Logout","Description":"Logout Link","AccessType":"Common","AccessControl":"Link","IsAccess":"True"},{"AccessId":"21","AccessName":"Test Button","Description":"Test Button","AccessType":"Required_Access","AccessControl":"Button","IsAccess":"False"},{"AccessId":"45","AccessName":"Sales Communicator","Description":"Sales Communicator","AccessType":"Required_Access","AccessControl":"Menu","IsAccess":"True"}]}
                 //FactoryIndexedDBLoad.createIndexDB(custData.Payload,searchData.Payload,per.Payload,dept.Payload);
                 FactoryIndexedDBLoad.createIndexDB(custData.Payload,searchData.Payload,permissionJSON.Payload,dept.Payload);
            }
            vm.permission = function () {

                var req = {
                     userId: localStorage['user_id'],
                     appId: '23'
                }
                //console.log('show ')
                requestManager.permissionRequestPost(req).then(function (result) {
                    console.log('show res***'+JSON.stringify(result));
                  //  debugger;
                    if(result){
                      vm.permit = {
                        home:'True',//result.Payload[0].IsAccess,
                        customer:'True',//result.Payload[7].IsAccess,
                        orderEntry:'True',//result.Payload[8].IsAccess,
                        orderHistory:'True',//result.Payload[9].IsAccess,
                        searchItem:'True',//result.Payload[10].IsAccess
                         }
                         vm.check_per = result;
                       
                        checkAuthorization.permissionAdmin(vm.permit);  
                    }
                    
                    //console.log('show res'+JSON.stringify(vm.permit));
                })
            }
               vm.getEORnumber = function () {
                var req = {
                     BrokerId: localStorage['user_id'],
                     pageTitle: 'ORDERNO',
                     CompanyId: 2
                }
                //console.log('showReqObj* '+JSON.stringify(req));
                requestManager.getEOR(req).then(function (result) {
                    console.log('show res*'+JSON.stringify(result));
                    if(result){
                        var lasteor=parseInt(result.Payload[0].LastEOR);
                      localStorage.setItem('EORKEY',lasteor);
                    }
                })
            }
            

        vm.getProfile = function() {
                $rootScope.login = true;
                //$location.url('/order-entry');
                $state.go("home");
        }


        }
    }

    loginController.$inject = ['$scope','$rootScope', 'requestManager', '$state','setUser','checkAuthorization','FactoryIndexedDBLoad','labelSet','setHeaderItem','messageSending'];

    return loginController;
});