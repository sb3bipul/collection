
define([], function(app){
    'use strict';

    function countByBrokerController($rootScope,requestManager,$state,$filter,checkAuthorization){
        var vm = this;

        function initRootScope() {
            //after login set login variable in root scope
            $rootScope.login = true;
            checkAuthorization.Authentication();
            vm.fromDate = $filter('date')(new Date(), 'MM/dd/yyyy');
            vm.toDate = $filter('date')(new Date(), 'MM/dd/yyyy');
            vm.BrokeFromDate = $filter('date')(new Date(), 'MM/dd/yyyy');
            vm.BrokeToDate = $filter('date')(new Date(), 'MM/dd/yyyy');
            vm.sortType = ''; 
            vm.sortReverse = false;
            vm.brokerData = [];
            
            countByBroker();
        }
        initRootScope();

        function goBack() {
            localStorage.setItem('path','/countByBroker');
        }
        goBack();

        function countByBroker(){
            var req = {
                FromDate:vm.fromDate,
                ToDate:vm.toDate,
                GoyaCompanyID:'01'    
            }
            vm.brokerData = [];
            $rootScope.isLoading = true;
            requestManager.countByBroker(req).then(function(result) {
                $rootScope.isLoading = false;
                for(var i = 0;i<result.Payload.length;i++){
                    if(result.Payload[i].Web == '-'){
                        vm.brokerData.push({UserNo:result.Payload[i].UserNo,UserName:result.Payload[i].UserName,Handheld:result.Payload[i].Handheld,EDI:result.Payload[i].EDI,Web:result.Payload[i].Web})
    
                    }else{
                        var total = parseInt(result.Payload[i].Handheld)+parseInt(result.Payload[i].EDI)+parseInt(result.Payload[i].Web)
                        vm.brokerData.push({UserNo:result.Payload[i].UserNo,UserName:result.Payload[i].UserName,Handheld:parseInt(result.Payload[i].Handheld),EDI:parseInt(result.Payload[i].EDI),Web:parseInt(result.Payload[i].Web),Total_Count:total})

                    }
                }
                
                        //console.log('countByBroker response: ' + JSON.stringify(result.Payload));
            });
        }
        vm.filterBrokerData = function () {
            if(vm.BrokeFromDate !='' && vm.BrokeToDate !=''){
                vm.fromDate = vm.BrokeFromDate;
                vm.toDate = vm.BrokeToDate;
                countByBroker();
            }
            
        }
    }

    countByBrokerController.$inject=['$rootScope', 'requestManager','$state','$filter','checkAuthorization'];

    return countByBrokerController;
});
