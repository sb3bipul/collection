define([], function(app) {
    'use strict';

    function SearchItemController($scope,$rootScope,$timeout, requestManager, checkAuthorization,labelSet, $state,$location,FactoryIndexedDBLoad,setHeaderItem,checkBrowserDetails) {
        var vm = this;
        vm.itemsPerPage = 100;
        vm.currentPage = 0;
        vm.addCartItem = [];
        vm.count = 0;
        vm.sumQuantity = 0;
        vm.quantity = 1;
        vm.filtItem = [];
        vm.filtPromo = false;
        vm.searchItem = [];
        vm.limit = 50;
        vm.offset = 0;
        vm.duplCart = false;
        vm.custId = localStorage['Cust_id'];
        vm.custName = localStorage['Cust_Name'];
         vm.caseTotal = 0;
         vm.itemScr = true;
		vm.orderItemData = [];
	    vm.EOR = '';
        vm.ItemCode='';
        vm.CaseQuantity='';
        vm.Departments='';
        vm.SelectedDepartment='';
        
     $rootScope.headerUrl = 'includes/footer1.html';
        function initRootScope() {
            //after login set login variable in root scope
            if(localStorage['access_token']){
              labelSet.getLabel(localStorage['lang'],'Item');
              vm.labes = labelSet.setLabels();
              //console.log(vm.labes);
              $rootScope.login = true;
            }
            if (!localStorage['Cust_id']) {
                $state.go('customerdetails');
            }
            
            checkAuthorization.Authentication();
            //goSearchItemList();
            if($rootScope.online == true){
               getDepartmentDetails();
            vm.selectedDepartmentValue = "1";
            vm.SelectedDepartment="1";
            var queryStringObject = $location.search();

           if(queryStringObject.EOR > 0)
           {
              vm.EOR = queryStringObject.EOR;
              addItem('','0');  // Load Item List

           } 
            }
            

        }
        initRootScope();
       
        function goBack() {
            localStorage.setItem('path', '/search-item');
            setHeaderItem.setItems();
        }
        goBack();

         // Get Deaprtment Details
          function getDepartmentDetails() 
           {
          //$rootScope.isLoading = true;
            requestManager.GetDepartmentsPost().then(function(result) {
               // $rootScope.isLoading = false;  
                if (result.Payload) {
                    vm.Departments = result.Payload;                   
                }
            });
        }
vm.searchItemList = '';
        vm.goSearchItemList = function(text,ispromo) {

        // vm.offset =0;
          if(vm.filtPromo == true)
          {
            vm.filtPromoCode= 'X';
          }else
          {
            vm.filtPromoCode= '';
          }
           var req = {
               
                GoyaCompanyId:'01',
                BillingCompanyId : '01',
                Zone : 1,
                CustomerId: localStorage['Cust_id'],
                SearchText:vm.searchItemList,
                IsPromo:vm.filtPromoCode,
                Limit:vm.limit,
                Offset:vm.offset
            }
            console.log('json' + JSON.stringify(req))
            $rootScope.isLoading = true;
            if($rootScope.online == true){
            requestManager.ItemListDB2(req).then(function(result) {
                  
                //vm.filtItem = angular.copy(result.Payload);
                //vm.searchItem = result.Payload;
                //console.log(JSON.stringify(result.Payload));
                
                 vm.pushNewElement(result.Payload);
               
                
                
               // alert(vm.searchItem.length);


                $rootScope.isLoading = false;
               // console.log('SearchItemList response: ' + JSON.stringify(result.Payload));
                vm.range = function() {
                    var rangeSize = 5;
                    var ps = [];


                    var start = vm.currentPage;
                    //  console.log(start)
                    //  console.log(vm.pageCount(),vm.currentPage)
                    if (start > vm.pageCount() - rangeSize) {
                        start = vm.pageCount() - rangeSize + 1;
                    }

                    for (var i = start; i < start + rangeSize; i++) {
                        if (i >= 0)
                            ps.push(i);
                    }
                   // console.log(ps)
                    return ps;
                };

                vm.prevPage = function() {
                  //$('body').scrollTop(0);
                  vm.itemScr = true;
                    if (vm.currentPage > 0) {
                        vm.currentPage--;

                    }
                };

                vm.DisablePrevPage = function() {
                    return vm.currentPage === 0 ? "disabled" : "";
                };

                vm.pageCount = function() {
                    return Math.ceil(vm.searchItem.length / vm.itemsPerPage) - 1;
                };

                vm.nextPage = function() {
                  //$('body').scrollTop(0);
                  vm.itemScr = true;
                    if (vm.currentPage < vm.pageCount()) {
                        vm.currentPage++;

                    }

                };

                vm.DisableNextPage = function() {
                  console.log(vm.currentPage+" "+vm.pageCount());
                 // $('body').scrollTop(0);
                  //vm.itemScr = true;
                    return vm.currentPage === vm.pageCount() ? "disabled" : "";
                };

                vm.setPage = function(n) {
                  vm.itemScr = true;
                    vm.currentPage = n;
                };

            });
          }
        }
        vm.goSearchItemList();
        vm.goSearchItemLists = function(text,ispromo) {
           vm.searchItem = [];
          vm.offset =0;
          if(ispromo == true){
            ispromo= 'X';
          }else{
            ispromo= '';
          }

            var req = {
               
                GoyaCompanyId:'01',
                BillingCompanyId : '01',
                Zone : 1,
                CustomerId: localStorage['Cust_id'],
                SEARCHTEXT:text,
                ISPROMO:ispromo,
                Limit:vm.limit,
                Offset:vm.offset
            }
            console.log('json' + JSON.stringify(req))
            $rootScope.isLoading = true;
            if($rootScope.online == true){
            requestManager.ItemListDB2(req).then(function(result) {
                $rootScope.isLoading = false;
                //vm.filtItem = angular.copy(result.Payload);
                vm.pushNewElement(result.Payload);
                //console.log(JSON.stringify(vm.searchItem));

            });
          }
        }
//vm.goSearchItemLists();
        $scope.$on('setItem',function (evt,data) {
          vm.offset++;
          vm.goSearchItemList();
        });
        vm.getBarCode = function (upc,index) {
            $("#"+upc).barcode(upc,"code128",{barWidth:1, barHeight:40,moduleSize: 2});
        }
        
        vm.pushNewElement = function (data) {
         
          console.log(JSON.stringify(data));
          for(var i=0;i<data.length;i++){
            if(data[i].PRODUCTCODE!='-'){
              vm.searchItem.push(data[i]);
            }
            
          }
          console.log(JSON.stringify())
          //vm.searchItemsList = angular.copy(vm.searchItem);

          //console.log(JSON.stringify(vm.searchItem));
        }
        /*vm.setFilterItem = function (val) {
          if(val == true){
            vm.searchItem = _.filter(vm.searchItems,function (item) {
              return item.ISPROMO == 'X';
            })
          }else{
            vm.searchItem = vm.searchItemsList;
          }
        }
        vm.filterItems = function ($event,string) {
           //$event.stopImmediatePropagation();
           vm.searchItem = [];
           console.log($event)
          var reg = new RegExp(string,'i');
            if(string !=''){
              vm.searchItem = _.filter(vm.searchItems,function (item) {              
               return item.PRODUCTCODE.match(reg) ||  item.DISPLAYNAME.match(reg);
               
           })
            }else{
              vm.searchItem = vm.searchItemsList;
            }
         
          
         
        }*/
        vm.setScroll = function () {
        var $target = $('html,body'); 
        if(vm.itemScr == true){
           vm.itemScr = false;
         //  alert('ghdhd')
          $target.animate({scrollTop: $target.height()}, 1000);
         
        }else{
          vm.itemScr = true;
         // alert('ghdhd')
          $target.animate({scrollTop: 0}, 1000);
          
        }
        

        }
        vm.setItemDetails = function (item_details) {
          //console.log(JSON.stringify(item_details));
          vm.countIm = 1;
          vm.setDetails = item_details;           
            vm.setUnitIm = 'https://portal.goya.com/goyaimages/web/'+item_details.PRODUCTCODE+'_A1C1.jpg';
            vm.setCaseIm = 'https://portal.goya.com/goyaimages/web/'+item_details.PRODUCTCODE+'_A1L1.png';
            vm.setNutritionIm = 'https://portal.goya.com/goyaimages/'+item_details.PRODUCTCODE+'_NFP.jpg';
            vm.setSpecificationIm = 'https://portal.goya.com/goyaimages/'+item_details.PRODUCTCODE+'_SPEC.jpg';
            vm.Procode = item_details.PRODUCTCODE;
            vm.Proname = item_details.DISPLAYNAME;
            vm.setUpdateIm =  'https://portal.goya.com/goyaimages/web/'+item_details.PRODUCTCODE+'_A1C1.jpg';
           

           
             
        }
        vm.setUpdateimg = function (Img,event) {
         // alert($(event.target).offset().top)
          //$('body').scrollTop($(event.target).offset().top);
          //$('#modalShowItem').css({left:$(event.target).offset().left+70,top:$(event.target).offset().top-100});

          vm.setUpdateIm = Img;
        }
        
        vm.addCart = function(item, index) 
		{
            vm.ItemCode = item.PRODUCTCODE;
			vm.newCartItem = item;
            $('.cartModal').modal('show');
            vm.custId = localStorage['Cust_id'];
        }
        vm.plusCount = function() {
            //console.log('hii');
            vm.quantity = vm.quantity + 1;

        }
        vm.minusCount = function() {
           // console.log('hii');
            vm.quantity = vm.quantity - 1;

        }
        vm.resetCart = function(index) {
           // console.log('hello');
          //  vm.addCartItem.splice(index, 1);
            /// vm.newCartItem.splice(vm.indexItem,1);
           // vm.count = vm.count - 1;
           // vm.quantity = 1;
        }
        vm.confirmCart = function() {
            $('.cartModal').modal('hide');
          /*  if (vm.addCartItem.indexOf(vm.newCartItem) == -1) 
            {
                vm.addCartItem.push(vm.newCartItem);

                // vm.indexItem = vm.addCartItem.length-1;
               // vm.count = vm.count + 1;
                for (var i = 0; i < vm.addCartItem.length; i++) {
                    if (vm.addCartItem[i].ProductCode == vm.newCartItem.ProductCode) {
                        vm.addCartItem[i]['quantity'] = vm.quantity;
                    }
                }
                vm.reOrdering(vm.newCartItem);

            } else 
            {
                // vm.indexItem = vm.addCartItem.length-1;
                for (var i = 0; i < vm.addCartItem.length; i++) {
                    if (vm.addCartItem[i].ProductCode == vm.newCartItem.ProductCode) {
                        vm.addCartItem[i]['quantity'] = vm.quantity;
                    }
                }
                vm.reOrdering(vm.newCartItem);
            } */
            vm.sumQuantity = 0;
            vm.totalQuantity();
            //vm.quantity = 1;
			
			// Add Item to Pending Order
			addItem(vm.ItemCode,vm.quantity);
          
        }
        vm.reOrdering = function () {
          /*  for(var i=0;i<vm.addCartItem.length;i++)
            {
                if(vm.addCartItem[i].ProductCode == vm.newCartItem.ProductCode){
                    vm.addCartItem.splice(i,1);
                    vm.addCartItem.splice(0,0,vm.newCartItem);
                }
            } */
        }
        vm.totalQuantity = function () {
          //  _.forEach(vm.addCartItem,function (item) {
          //      vm.sumQuantity = vm.sumQuantity + item.quantity;
          //  })          
        }
        vm.countIm = 1;
        vm.inc = function () {
          vm.countIm++;
          // body...
        }
        vm.dec = function () {
          vm.countIm--;
          // body...
        }
        vm.addModCart = function () {
          var itemcode = vm.setDetails.PRODUCTCODE;
          //alert(itemcode+vm.countIm)
          $('#modalShowItem').css('display','none');
          vm.ItemCode = itemcode;
          addItem(itemcode,vm.countIm);
         
        }
		
		               // Item Item 
        function addItem(ItemCode,CaseQuantity)
        {
               // Check EOR 
                 if(vm.EOR=='undefined' || vm.EOR==null || vm.EOR =='')
                       vm.EOR = ''; 
                 
                // check Item Code or UPC
                var Action;
                if(ItemCode.length >4)
                      Action = 'UPC';
                  else
                       Action = '';

                   if(vm.SelectedDepartment=='')
                        vm.SelectedDepartment="1";
          //   alert($scope.SelectedCustomer);
        var QOH,restrictedCheck,casePrice,retailPrice,unitForCasePrice,promoAmount,promoMinQuantity,originalCasePrice,isPromoItem;
          // Check Item QOH, Promotion, Restriction
               var req1 = 
                      {                        
                         CustomerId:localStorage['Cust_id'],
                         ItemCode:ItemCode,
                         GoyaCompanyId:localStorage['goyaCompanyId'],
                         CaseQuantity:CaseQuantity
              };              
             
                requestManager.checkRestrictedStockItem(req1).then(function(result1) 
                {  
                    
                    QOH = parseInt(result1.Payload[0].ITEM_QOH);
                    restrictedCheck = result1.Payload[0].ISAUTHORIZED;
                    casePrice = result1.Payload[0].CASEPRICE;
                    retailPrice = result1.Payload[0].RETAILERPRICE;
                    unitForCasePrice = result1.Payload[0].PRICEUNIT;

                    promoAmount = result1.Payload[0].PROMOAMOUNT;
                 
                    promoMinQuantity = parseInt(result1.Payload[0].MINQUANTITY);
                    originalCasePrice = result1.Payload[0].ORIGINALCASEPRICE;

                  
                    isPromoItem=false;
                   vm.isPromoItem= false;

                  // Check if Item is on Promotion
                    if(result1.Payload[0].ISPROMO=='X')
                    {
                      isPromoItem = true;
                      vm.isPromoItem= true;
                    }

                  // Check Item is restricted 
                     if(restrictedCheck=='1')
                      {
                          vm.IsRestricted = false;
                      }
                      else
                      {
                          vm.IsRestricted = true;
                      }   
             
                    
                      var req = 
                      {
                         
                         CustomerId:vm.custId,
                         EOR: vm.EOR,
                         CaseQuantity: CaseQuantity,
                         ItemCode:vm.ItemCode, 
                         BrokerId: localStorage['user_id'],
                         OrderNumber:'',
                         Status: "0",                         
                         CompanyId: 2,
                         UnitQuantity: '',
                         LanguageId: "en-US",
                         CatalogId: "1",
                         SearchText: "%%",
                         WarehouseId: "01",
                         BasketId: '',
                         Message: '',
                         Dept:vm.SelectedDepartment,
                         SalesManID: localStorage['user_id'],
                         WHID: "01",
                         chkPickup: false,
                         clientPONumber: '',
                         day:'',                       
                         notDay:'',    
                         deliveryDate:'2016-05-18',  
                         promoCode:'', 
                         Action:'',   
                         strCases:'1',   
                         Comments:'', 
                         Result:'',   
                         Units:'1',
                         key:12,
                         AmountCollected:0,
                         isRestricted:vm.IsRestricted,
                         QOH:QOH,
                         CasePrice:casePrice,
                         RetailPrice:retailPrice,
                         UnitForCasePrice:unitForCasePrice,
                         PromoAmount:promoAmount,
                         IsPromoItem:isPromoItem,
                         PromoMinQuantity:promoMinQuantity,
                         OriginalCasePrice:originalCasePrice

                     }
                     
                requestManager.AddItemToOrder(req).then(function(result) 
                {
                   
                 if (result.Payload)
                 { 
                   vm.orderItemData =  result.Payload;
                   vm.addCartItem1 = result.Payload;
                    vm.quantity = 1;
                  if(vm.orderItemData.length>0)
                  {                    
                      // Get generated EOR                 
                      vm.EOR = vm.orderItemData[0].PONumber;
                      // Reset Values 
                    
                      vm.ItemCode = '';
                       vm.caseTotal = 0;

                       for(var i = 0; i<vm.orderItemData.length;i++)
                      {
                          vm.caseTotal += parseInt(vm.orderItemData[i].Quantity);
                       }
                      //   vm.CaseQty = '';
                 }
                          

                }
            });
          });

        }

          // Delete Item       
         vm.deleteItem = function(productCode) 
         {    
            var req = 
                      {
                         ItemCode:productCode,
                         CustomerId:vm.custId,
                         EOR:vm.EOR
                      } 
             requestManager.DeleteOrderItem(req).then(function(result) 
             {  
                  vm.DeletedItem =  productCode;                  
                  addItem('','0')

             });
          }
          function indexDbSearchItem(){
            if($rootScope.online == false){
                $rootScope.isLoading = true;
                   FactoryIndexedDBLoad.getItemsData().then(function (result) {
                    vm.searchItem = result;
                    vm.dept();
                    
                   
                })  
            }
           
          }
          indexDbSearchItem()
          vm.dept = function () {
              FactoryIndexedDBLoad.getDeptData().then(function (result) {
                  vm.Departments = result;
                  vm.SelectedDepartment = result[0].DepartmentCode;
                  $rootScope.isLoading = false;
              })
          }
        
        }

    SearchItemController.$inject = ['$scope','$rootScope','$timeout', 'requestManager', 'checkAuthorization','labelSet', '$state','$location','FactoryIndexedDBLoad','setHeaderItem','checkBrowserDetails'];

    return SearchItemController;
});