﻿using Microsoft.Owin;
using Owin;

//[assembly: OwinStartupAttribute(typeof(OMSAdminPortal.Startup))]
namespace OMSAdminPortal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
