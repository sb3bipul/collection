﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="payment.aspx.cs" Inherits="OMSWebApi.PaymentWebForm.payment" %>


<!doctype html>
<html lang="en">
<%--<meta http-equiv="content-type" content="text/html;charset=UTF-8" />--%>
<head>
    <script src="https://js.stripe.com/v3/"></script>
    <script>
        function onChange() {
            debugger;
            var paymenttype = document.getElementsByName("customRadio");
            var cardfn = document.getElementById("card");
            var bankfn = document.getElementById("bank");
            var payPal = document.getElementById("payPal");

            for (var i = 0, length = paymenttype.length; i < length; i++) {
                if (paymenttype[i].checked) {
                    if (paymenttype[i].value === "CreditCard") {
                        cardfn.style.display = 'block'; // Display
                        bankfn.style.display = 'none'; // Hide
                        payPal.style.display = 'none'; // Hide
                    }
                    else if (paymenttype[i].value === "BankTransfer") {
                        cardfn.style.display = 'none'; // Hide
                        bankfn.style.display = 'block'; // Display
                        payPal.style.display = 'none'; // Hide
                    }
                    else {
                        cardfn.style.display = 'none'; // Hide
                        bankfn.style.display = 'none'; // Hide
                        payPal.style.display = 'block'; // Display
                    }
                    break;
                }
            }

        };

        document.addEventListener("DOMContentLoaded", function () {
            var stripe = Stripe("pk_test_51ITpT7HoTn5ARElrJHr9ihINaWBotTmpUHZ0YHSRIk0TXJVl1tBX3Esf4DEaHU16kVb6bKkOzS2TYHYeqMhaZvoc00uJAfIsCG");
            var elements = stripe.elements();
            var cardElement = elements.create('card');
            cardElement.mount('#card-element');

            var form = document.getElementById("paymentForm");
            form.addEventListener("submit", function (e) {
                e.preventDefault();
                stripe.confirmCardPayment(
                    "<%= clientSecret %>",
                    {
                        payment_method: {
                            card: cardElement,
                            billing_details: {
                                name: "<%= CustName %>"
                            }
                        }
                    }
                ).then(function (result) {
                    if (result.error) {
                        alert(result.error.message);
                    }
                    else {
                        form.submit();
                        alert("Payment Successfully done!");
                    }
                })
            });
        });




    </script>
    <link rel="stylesheet" href="../Content/bootstrap.css" />
    <link rel="stylesheet" href="../Content/style.css" />
    <title>PayPanel</title>
</head>
<body>
    <section class="payment-method">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-8 m-auto">
                    <div class="payment-contnt">
                        <div class="payment-contnt-1">
                            <img src="../Content/images/logo.png" style="width: 80%; height: auto; margin-bottom: 20px;" />
                            <h5>Invoice from Goya Foods Inc</h5>
                            <p>Billed to <%= CustName %></p>
                        </div>


                        <div class="content-box">
                            <div class="payment-contnt-4">
                                <h5>$ <%= Amount %> due on <%= DueDate %></h5>
                                <p>Choose how would you like to pay</p>

                                <div class="radio-container mt-4">
                                    <div class="custom-control custom-radio mb-2">
                                        <input type="radio" id="customRadio1" name="customRadio" value="CreditCard" class="custom-control-input" onchange="onChange();" />
                                        <label class="custom-control-label" for="customRadio1">
                                            <span class="img-box">
                                                <img src="../Content/images/credit-card.png" style="width: 25px;" /></span> Credit Card</label>
                                    </div>

                                    <div class="custom-control custom-radio mb-2">
                                        <input type="radio" id="customRadio2" name="customRadio" value="BankTransfer" class="custom-control-input" onchange="onChange();" />
                                        <label class="custom-control-label" for="customRadio2">
                                            <span class="img-box">
                                                <img src="../Content/images/bank.png" style="width: 20px;" /></span> Bank Transfer</label>
                                    </div>

                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="customRadio3" name="customRadio" value="Paypal" class="custom-control-input" onchange="onChange();" />
                                        <label class="custom-control-label" for="customRadio3">
                                            <span class="img-box">
                                                <img src="../Content/images/paypal.png" style="width: 25px;" /></span> Paypal</label>
                                    </div>
                                </div>

                                <div id="card" style="display: none;">
                                    <%--<span><%= clientSecret %></span>--%>
                                    <form id="paymentForm" runat="server" method="post">
                                        <div id="card-element"></div>
                                        <button type="submit" class="btn btn-block btn-info mt-3">Pay</button>
                                    </form>
                                </div>

                                <div id="bank" style="display: none;">
                                    <button type="submit" class="btn btn-block btn-info mt-3">Pay</button>
                                </div>

                                <div id="payPal" style="display: none;">
                                    <button type="submit" class="btn btn-block btn-info mt-3">Pay</button>
                                </div>


                                <%--<table class="card-details mt-5">
                                    <tr>
                                        <td>
                                            <div id="card-element"></div>
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <input type="text" class="form-control border-0" placeholder="MM" />
                                                <input type="text" class="form-control border-0" placeholder="YY" />
                                                <input type="text" class="form-control border-0" placeholder="CVC" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="footer">
        <center>
                <p>Powered By Goya Foods Inc</p>
        </center>
    </section>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="../Content/js/jquery-3.5.1.slim.min.js"></script>
    <script src="../Content/js/popper.min.js"></script>
    <script src="../Content/js/bootstrap.min.js"></script>

</body>

</html>
