
define([], function(app){
    'use strict';

    function backRoomController($rootScope,requestManager,$state,checkAuthorization,setHeaderItem,$scope,$filter,labelSet){
    	var vm = this;
        vm.SelectedCustomer = {
                "Name": "",
                "id": ""
            }
            vm.ItemCode = '';
        vm.custData = [];
        vm.orderItemData = [];
        vm.isScan = false
        vm.SelectedDepartment = '1';
        vm.selectedDepartmentValue = '1';
        vm.BrokerId = localStorage['user_id'];
        function initRootScope() {
            //after login set login variable in root scope
            if(localStorage['access_token']){
              $rootScope.login = true;
        	 $rootScope.isLoading = false;
           document.getElementById("txtItemCodeMobile").focus();
           document.getElementById("txtItemCode").focus();
           labelSet.getLabel(localStorage['lang'],'Create Order');
              vm.labes = labelSet.setLabels();
            checkAuthorization.Authentication();
            if(localStorage['Cust_Name']!=undefined){
               vm.selectedCustomerValue = localStorage['Cust_id'];
            vm.SelectedCustomer = localStorage['Cust_id']+'-'+localStorage['Cust_Name'];
            // console.log('show custo'+vm.SelectedCustomer);
            vm.CustomerName = localStorage['Cust_id']+'-'+localStorage['Cust_Name']; 
            }
            
        }
            
            //goOrderDetail();
        }
        initRootScope();

        function goBack() {
            localStorage.setItem('path','/backRoomEntry');
        }
        goBack();

        function goOrderDetail(){
            var req = {
                    
            }
            /*requestManager.OrderList(req).then(function(result) {
                        console.log('OrderList response: ' + JSON.stringify(result));
            });*/
        }
        vm.customerDetails = function(callback) {
           if($rootScope.online == true){

                   var req = {
                BrokerId: localStorage['user_id'],
                GoyaCompanyId:localStorage['goyaCompanyId']
            }
                requestManager.customerDetailPost(req).then(function(result) {
                    vm.customerData = result.Payload;
                   // console.log(JSON.stringify(vm.customerData));
                    for (var i = 0; i < vm.customerData.length; i++) {
                      if(vm.customerData[i].STATUS == 'A'){
                        vm.custData.push({
                            UserID: vm.customerData[i].USERID,
                            Name: vm.customerData[i].USERID + "-" + vm.customerData[i].NAME
                        })
                      }                       
                    }
                   // console.log('show customerList'+JSON.stringify(vm.customerData));
                    callback(vm.custData);
                    
                });
           }
          }
          vm.onCustomerChanged = function(item)
        {
          //console.log('selected item'+JSON.stringify(item));
                // localStorage['Cust_id'] =  vm.selectedCustomerValue; drpCustomer
                if(item!=undefined) {
                  localStorage.setItem('Cust_id',item.UserID);
            var name='';
           for (var i=0;i<vm.customerData.length;i++)
           {    
                if(vm.customerData[i].USERID == localStorage['Cust_id'] )
                 {

                      name = vm.customerData[i].NAME;
                      vm.CustomerBalance = vm.customerData[i].CUSTOMERBALANCE;
                      localStorage['bal']= vm.customerData[i].CUSTOMERBALANCE;
                      break;
                 }
           }

           var reqItem = {
                    eor:'',
                    case:0
                   }
            setHeaderItem.setItems(reqItem);
            localStorage.setItem('Cust_Name',name);
            vm.CustomerName = localStorage['Cust_id']+'-'+localStorage['Cust_Name'];
            //
              resetOrderScreen();
              //getPendingOrderCount();
                }          
           
        }
        function getDepartmentDetails()  {
        if($rootScope.online == true){
          $rootScope.isLoading = true;
            requestManager.GetDepartmentsPost().then(function(result) {
                $rootScope.isLoading = false;  
                if (result.Payload) {
                    vm.Departments = result.Payload;                   
                }
            });
          }else{
              FactoryIndexedDBLoad.getDeptData().then(function (result) {
               $rootScope.isLoading = false;
               vm.Departments = result;    
                });
          }
        }
        getDepartmentDetails();
        function resetOrderScreen()
        {
           // vm.EOR = '';
            vm.orderItemData = '';
            //vm.isPickup = false;
            vm.CaseQty = '';
            vm.UnitQty = 0;       
           // vm.SelectedPromotion = '';
           // vm.SelectedDay = 0;
           // vm.SelectedNotDay=0;
            //vm.ClientPONumber='';
           // vm.SelectedDay='0';
           // vm.SelectedNotDay='0';
            //vm.Message='';        
            //vm.ExtraInfo = true;
            //vm.AmountCollected='';
           // vm.Comment='';
           // vm.TemplateName='';
           // $scope.EOR='';
            //vm.IsRestricted=false;
            vm.selectedDepartmentValue = '1';
            vm.SelectedDepartment = '1';
            
           // dateTimeCheck();
            //changeBackgroundColorForPickup(); 
        }
        $scope.addItem = function(keyEvent,check) 
        {  
          
              if (keyEvent.which === 13)
              {    // Add Item on ENTER click
                  console.log("callled from here:--"+vm.tempItemCode);

                  vm.tempItemCode = vm.ItemCode;
                  vm.ItemCode = '';
                 
                      
                  if(check=='m')
                  {
                     document.getElementById("txtItemCodeMobile").focus();
                     vm.IsMobile = true;
                  }
                  else
                  {
                    document.getElementById("txtItemCode").focus();
                    vm.IsMobile=false;
                  }
                   
                   // Check for Quantity Validation
                   var check = checkQuantityValidation();
                      if (check && vm.tempItemCode!="" && vm.tempItemCode!=null)    
                      {
                          console.log("add item called");
                          addItem();                   
                        }
               }
               else
               {
               
                 // Move focus to Case Quantity if Item code lenght  > 4        
                 var ItemCodeLength = vm.ItemCode.length;

             // if(vm.iOS==true)         
             //     ItemCodeLength = ItemCodeLength - 1;      


                  if(vm.isScan == false && ItemCodeLength==4)
                  {    
                     if(check=='m')
                     {      
                         // document.getElementById("txtCaseQtyMobile").select();

                         document.getElementById("txtCaseQtyMobile").focus();
                            $('input[type="number"]').on('focus',function(){
                                 document.getElementById("txtCaseQtyMobile").selectionStart = 0;
                                 document.getElementById("txtCaseQtyMobile").selectionEnd = 9999;                                 
                           })
                      
                     }
                     else
                     {
                      // document.getElementById("txtCaseQtyDesktop").select();

                       document.getElementById("txtCaseQtyDesktop").focus();
                         $('input[type="number"]').on('focus',function(){
                                 
                                  document.getElementById("txtCaseQtyDesktop").selectionStart = 0;
                                 document.getElementById("txtCaseQtyDesktop").selectionEnd = 9999;
                          })
                           //   $('txtCaseQtyDesktop').get(0).selectionStart=0;
                           //      $('txtCaseQtyDesktop').get(0).selectionEnd=9999;
                     }

                  }
               }
           }
           $scope.btnAddItem = function(keyEvent)
         {
                 vm.tempItemCode = vm.ItemCode;
                 vm.ItemCode = '';
                   // Check for Quantity Validation
                  var check = checkQuantityValidation();
                
                  if(keyEvent=='m')
                  {
                     document.getElementById("txtItemCodeMobile").focus();
                     vm.IsMobile = true;
                  }
                  else
                  {
                    document.getElementById("txtItemCode").focus();
                    vm.IsMobile = false;
                  }

                  if (check)                     
                      addItem();                   
         }
         function checkQuantityValidation()
        { 
          if(vm.isPickup == false)  // Regular Order
          {
              if(vm.CaseQty < 1 && vm.CaseQty !=0)
              {
                 alert('Please enter valid quantity.');
                 return false;
              }
         }
         else
         {
             if(vm.CaseQty < 1  && vm.UnitQty < 1 && vm.CaseQty!=0 && vm.UnitQty !=0 )  // Pickup Order. Check for both Case and Unit Quantity
             {
                  alert('Please enter valid quantity.');
                 return false;
             }
             else if(vm.CaseQty > 0  && vm.UnitQty > 0)
              {
                 alert('Either Case Or Unit can be entered.');
                 return false;
              }
         }
          return true;
        }

           function addItem()
        {    
        
               if(localStorage['Cust_id']=='undefined' || localStorage['Cust_id']==null || localStorage['Cust_id'] =='')
               {
                     alert('Please select customer.');
                     return;
               } 
            
               // Check for ZERO quantity to Delete Item
               if(vm.CaseQty == 0 && vm.UnitQty == 0 && vm.ItemCode > 0)
               {
                    deleteItem(vm.BrokerId,vm.ItemCode); // delete Item
                    vm.ItemCode = '';
               }
               $('.div-clr').css('height','0px');
               vm.ExtraInfo = false;  // Hide Extra Information

               var EOR = $scope.EOR;
 
                 // Check EOR 
                 if(vm.EOR=='undefined' || vm.EOR==null || vm.EOR =='')
                       vm.EOR = '';
                 else
                       vm.EOR = $scope.EOR;
                 
                 // Make Default Case Quanitity to 1
                   if((vm.CaseQty=='undefined' || vm.CaseQty==null || vm.CaseQty =='') && vm.isPickup == false)
                       vm.CaseQty = 1;
                   else if(!(vm.CaseQty % 1 === 0))
                       vm.CaseQty = 1;                   

                // For Pick up if Both Case and Unit zero then default will be 1 case
                   if(vm.isPickup== true && vm.CaseQty<1 && (vm.UnitQty ==0|| vm.UnitQty=='undefined' || vm.UnitQty==null || vm.UnitQty =='' ))
                         vm.CaseQty = 1;
                   //   vm.caseQty=0;

                   if(vm.UnitQty=='undefined' || vm.UnitQty==null || vm.UnitQty =='')
                      vm.UnitQty = 0;
                 
                   var PromoCodeValue;
                  /* if(vm.SelectedPromotion.PromoCode=='undefined' || vm.SelectedPromotion.PromoCode==null || vm.SelectedPromotion.PromoCode =='')
                      PromoCodeValue = '';
                    else
                      PromoCodeValue = vm.SelectedPromotion.PromoCode;
*/


                var Action;
                
               var ItemCodeLength = ''+vm.tempItemCode;

                if(ItemCodeLength.length > 4)
                      Action = 'UPC';
                else
                      Action = ''; 
              
               
         
                 
               if($rootScope.online == true)
               {
                //console.log('req'+JSON.stringify(req));
                var checkIteCode = vm.tempItemCode;
                if(vm.tempItemCode=='')
                  checkIteCode = '0';

                
                 
              
                // Check Item QOH, Promotion, Restriction
                  

                      var req = 
                      {
                         BrokerId: vm.BrokerId,
                         CustomerId:localStorage['Cust_id'],
                         OrderNumber:'',
                         Status: "0",
                         FromDate: "4/1/2015",
                         ToDate: "4/30/2016",
                         CompanyId: 2,
                         EOR: "",
                         CaseQuantity: vm.CaseQty,
                         UnitQuantity: vm.UnitQty,
                         LanguageId: "en-US",
                         CatalogId: "1",
                         SearchText: "%%",
                         WarehouseId: "01",
                         BasketId: '',
                         Message: '',
                         Dept: vm.SelectedDepartment,
                         SalesManID: vm.BrokerId,
                         WHID: "01",
                         chkPickup: false,
                         isPickUp:false,
                         clientPONumber:"",  
                         day: "0",                       
                         notDay: "0",    
                         deliveryDate:$filter('date')(new Date(), 'MM/dd/yyyy'),  
                         promoCode:'', 
                         Action:Action,   
                         strCases:'1',   
                         Comments:'', 
                         ItemCode:checkIteCode, 
                         Result:'',   
                         Units:'1',
                         key:12,
                         AmountCollected:"0",
                         isRestricted:false,
                         QOH:1,
                         CasePrice:"0",
                         RetailPrice:"0",
                         UnitForCasePrice:"0",
                         PromoAmount:"0",
                         IsPromoItem:false,
                         PromoMinQuantity:"0",
                         OriginalCasePrice:"0"
                }  
                console.log(JSON.stringify(req))                
                // debugger;
                requestManager.backRoomData(req).then(function(result) 
                {
                 //debugger;
                      if (result.Payload)
                      {                   
                         vm.orderItemData =  result.Payload;
                         vm.addCartItem1 = result.Payload;

                       
                        if(vm.orderItemData.length>0)
                             $scope.ctDisabled = true;
                         
                         // Get generated EOR 
                         $scope.EOR =  vm.orderItemData[0].PONumber;
                         var reqItem = {
                          eor:$scope.EOR,
                          case:$scope.getCaseQtyTotal()
                         }
                         setHeaderItem.setItems(reqItem);
                         localStorage['EORKEY'] = $scope.EOR;
                         vm.EOR = vm.orderItemData[0].PONumber;
                         
                         //vm.AmountCollected = vm.orderItemData[0].AmountCollected;

                         vm.Message = vm.orderItemData[0].SuccessMessage;
                         $('#lblMessage').css("color", "green");
                         $('#lblMessage1').css("color", "green"); // Top Order Entry

                        // Unauthorized items and Item Stock check message in RED
                         
                         // When Last Item is Deleted
                         if(vm.orderItemData[0].ProductCode=='-')
                         {
                           vm.orderItemData ='';
                           vm.Message = '';
                           vm.isDeleted = false;
                         }

                         if( vm.orderItemData.length>0)
                         {
                           if( vm.orderItemData[0].ISVALID=='NOT' && checkIteCode!='0')
                           {
                              vm.Message = "Item Not Found."
                              $('#lblMessage').css("color", "red");
                              $('#lblMessage1').css("color", "red"); // Top Order Entry
                               playSound('2');

                           }
                           else if (checkIteCode!='0')
                           {
                                  playSound('1');
                           }
                         }

                         if(vm.isDeleted == true)
                         {
                             $('#lblMessage').css("color", "red");
                             $('#lblMessage1').css("color", "red"); // Top Order Entry
                             vm.Message = "Item #" + vm.DeletedItem +" Deleted.";
                         }

                         // Reset Values 
                         vm.ItemCode = '';
                         vm.CaseQty = '';
                         vm.UnitQty = 0;
                         vm.isDeleted = false;
                         vm.IsRestricted = false;

                         //focus('ItemNo');    
                        vm.setItemCodeFocus = true;   

                        if(vm.IsMobile)
                        {
                           document.getElementById("txtItemCodeMobile").focus();                     
                        }
                        else
                        {
                          document.getElementById("txtItemCode").focus();                   
                        }

                      }
                   });
                
           }
           else
           {
                vm.dataa=req;
                checkItemDetails(req.ItemCode)
               
           }
        }
        function playSound(val)
       {
         
        // Play Sound when Regular Item added
         if($rootScope.mute == true)
         {
          if(val=='1')
                var audio = new Audio('images/beep-03.wav');
          else if(val=='2')
               var audio = new Audio('images/beep-09.wav');

          audio.play();

          }     
       }
        $scope.getCaseQtyTotal = function ()
           {
              var caseTotal = 0;
              for(var i = 0; i<vm.orderItemData.length;i++)
              {
                 caseTotal += parseInt(vm.orderItemData[i].Quantity);
              }

             
              return caseTotal;
           }
           $scope.updateCaseQuantity = function(keyEvent,itemCode,caseQty,unitQty)
        {     
          console.log(keyEvent)
              
              if(unitQty<1)
              {
                vm.CaseQty = caseQty;
              if(keyEvent == '1' ){
                caseQty = parseInt(caseQty) + 1;
                 vm.CaseQty = caseQty;
              }       
              else if(keyEvent == '-1')
              {
                if(caseQty==1)
                {
                     var confirmDelete = confirm('Do you want to Delete Item?');
                     if(confirmDelete == true)
                         deleteItem(vm.BrokerId,itemCode);                       
                }
                else
                {
                  caseQty = parseInt(caseQty)- 1;
                  vm.CaseQty = caseQty;
                }
              }
             
               if ((keyEvent.which >47 && keyEvent.which <58)|| keyEvent.which === 13 || keyEvent.which === 0 || keyEvent=='-1' || keyEvent =='1') // On Key Press ENTER or On Blur
               {   
                  updateCaseQuantity(itemCode,caseQty);
               }
             }
              else
              {
                alert('Either Case Or Unit can be entered.');
              }
           // var target = $event.target;
           // target.blur();
        }

        // Update Case quantity
        function updateCaseQuantity(itemCode,caseQty)
        {    

            var req = 
                      {
                         caseQuantity:caseQty,
                         ItemCode:itemCode,
                         CustomerId:localStorage['Cust_id'],
                         BrokerId:vm.BrokerId
                      } 
                      console.log(req)  
             requestManager.updateItemCaseQuantity(req).then(function(result) 
             {
             
                       addItem();
                 
             });
        }
        vm.deleteItem = function(BrokerId,productCode)
         { 
           var deleteConfirm;
           deleteConfirm = confirm("Sure want to delete Item?")   
           if(deleteConfirm)
             deleteItem(BrokerId,productCode); 
        }
         function deleteItem(BrokerId,productCode)
        {   
            var req = 
                      {
                         ItemCode:productCode,
                         CustomerId:localStorage['Cust_id'],
                         BrokerId:vm.BrokerId
                      } 
                      console.log(req)
             requestManager.deleteBackRoomInventoryItem(req).then(function(result) 
             {    
                   
                  vm.isDeleted = true;   
                  //vm.DeletedItem =  productCode;  
                   

                  if(vm.IsMobile)
                  {
                     document.getElementById("txtItemCodeMobile").focus();                     
                  }
                  else
                  {
                    document.getElementById("txtItemCode").focus();                   
                  }
                  addItem(); 
                  
                  //vm.tempItemCode='';
             });
        }
        $scope.clearAllData = function () {
          var del = confirm('Are you sure want to delete all items');
          if(del == true){
            clearAllInventory();
          }
          
        }
        function clearAllInventory(){
          var req = {
                ItemCode:'',
                CustomerId:localStorage['Cust_id'],
                BrokerId:vm.BrokerId
          }
          requestManager.clearRoomInventoryItem(req).then(function (result) {
            debugger;
            console.log(result);
            vm.tempItemCode = '';
            addItem(); 
          })
        }
        $scope.updateCaseQuantityBox = function(updateQty)
        {
          var qtyCheck = 0;
          qtyCheck =parseInt(vm.CaseQty) + parseInt(updateQty);
         
           if(qtyCheck>=0)
           {              
              vm.CaseQty = parseInt(vm.CaseQty) + parseInt(updateQty);
           }

        }
    }

    backRoomController.$inject=['$rootScope', 'requestManager','$state','checkAuthorization','setHeaderItem','$scope','$filter','labelSet'];

    return backRoomController;
});
