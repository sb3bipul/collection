
define([], function(app){
    'use strict';

    function ForgetPasswordController($rootScope,requestManager,checkAuthorization,$state,$location,messageSending){
    	var vm = this;
        
        initRootScope();
        function initRootScope() {
        	//after login set login variable in root scope
        	//$rootScope.login = true;
            
            //checkAuthorization.Authentication();
            //goForgetPassword(); 
            vm.submitReqUser = {
                "userId": "",
                "emailId": ""
             }
             vm.flagCheck = false;
             vm.submitReqChangePass = {
                UserName:$location.search().username,
                NewPassword:''
             }
             localStorage.path = '/forget-password';
             messageSending.recieveMessage('');
             if($location.search().fogetPwdFlag == 'true'){
                vm.flagCheck = true;
                var credDetails = {
                    access_token:$location.search().access_token,
                    token_type:'Bearer'
                }
             }
             messageSending.setCredintialsEmailDetails(credDetails);
             console.log($location.search().username);
             console.log($location.search().access_token);
             console.log(typeof $location.search().fogetPwdFlag);
        }

        function goForgetPassword(){
        	
        }
        vm.forgotPassword = function (req) {
              console.log(req);
              var req = {
                  "UserName": req.userId,
                  "Email": req.emailId   
             }
             $rootScope.isLoading = true;
            requestManager.forgetPassword(req).then(function(result) {
                $rootScope.isLoading = false;
                 messageSending.recieveMessage('Please Check Your Mail');
                 $state.go('login');
                        console.log('forgetPassword response: ' + JSON.stringify(result));
            });
              
            }
     vm.changePassword = function (req) {
        $rootScope.isLoading = true;
         requestManager.ForgetPasswordEmail(req).then(function (result) {
            $rootScope.isLoading = false;
            messageSending.recieveMessage('Password Changed');
            $state.go('login');
             console.log('got it');
         })
     }
    }

    ForgetPasswordController.$inject=['$rootScope','requestManager','checkAuthorization','$state','$location','messageSending'];

    return ForgetPasswordController;
});
