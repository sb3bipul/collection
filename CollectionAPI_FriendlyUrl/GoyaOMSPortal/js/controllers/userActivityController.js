
define([], function(app){
    'use strict';

    function userActivityController($scope,$rootScope,requestManager,$state,checkAuthorization){
    	var vm = this;

        function initRootScope() {
            //after login set login variable in root scope
        	  $rootScope.login = true;
            checkAuthorization.Authentication();
            vm.setGraphDetail = {type:'',data:''};
            if(localStorage['access_token']){
                localStorage['path'] = '/userActivity';
               // console.log('hii'+JSON.stringify(vm.per) )
            }
            //goOrderDetail();
        }
        initRootScope();
        vm.showGraphData = function (type) {
            if(type == 'pie'){
             vm.mapByBrowser(type);
            }
            if(type == 'bar'){
              vm.mapByDevice(type);
            }
            if(type == 'column'){
              vm.mapByScreen(type);
            }
            
        }
        vm.getUserActivityData = function () {

           var req = {
                   UserId:'0',
                   FromDate:'12/1/2016',
                   ToDate:'12/31/2016'
           }
          $rootScope.isLoading = true;

           requestManager.getUserActivityData(req).then(function(result) 
             {
                 $rootScope.isLoading = false;
                 console.log(result)
                 vm.templateResult =  result.Payload;
                 vm.dataUsageFilter(vm.templateResult);
                // console.log(JSON.stringify(vm.templateResult));
             });
        }
        vm.getUserActivityData();
        vm.setActivityFilterData = function (brokerName,fromdate,todate) {
          
        }
        vm.dataUsageFilter = function (usageData) {
          vm.Bybrowser = _.groupBy(usageData,'BrowserName');
          vm.Bydevice = _.groupBy(usageData,'DeviceType');
          vm.Byscreen = _.groupBy(usageData,'PageName');
          vm.mapByBrowser('pie');
        }
        vm.mapByBrowser = function (type) {
         var browdata =  _.map(vm.Bybrowser,function (value,key) {
            var BybrowserMapData = {name:key,y:value.length}
            return BybrowserMapData
          })
          vm.setGraphData(type,browdata,'Browser','Browser Data Usage');
         // console.log(JSON.stringify(vm.setGraphDetail));
        };
        vm.mapByDevice = function (type) {
          var devicedata =  _.map(vm.Bydevice,function (value,key) {
            var BydeviceMapData = {name:key,y:value.length}
            return BydeviceMapData
          })
          vm.setGraphData(type,devicedata,'Device','Device Data Usage');
        }
        vm.mapByScreen = function (type) {
          var screendata =  _.map(vm.Byscreen,function (value,key) {
            var ByscreenMapData = {name:key,y:value.length}
            return ByscreenMapData
          });
          vm.setGraphData(type,screendata,'Screen','Screen Data Usage');
        }
        vm.setGraphData = function (type,data,type_data,text) {
          vm.setGraphDetail = {
            type:type,
            data:data,
            name:type_data,
            text:text
          }
        }
        $scope.$on('setGraphEvent',function (event,res) {
          vm.graphFilterData(res);
        });
        vm.graphFilterData = function (data) {
          console.log(data);
          /*if(data.type == 'pie'){
           $scope.userData =  _.filter(vm.setGraphDetail.data,function (item) {
               return item.
            })
          }*/
        }

    }

    userActivityController.$inject=['$scope','$rootScope', 'requestManager','$state','checkAuthorization'];

    return userActivityController;
});
