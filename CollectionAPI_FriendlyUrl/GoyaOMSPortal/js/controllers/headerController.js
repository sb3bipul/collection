
define([], function(app){
    'use strict';

    function headerController($scope,$rootScope,$location, $state,requestManager,FactoryIndexedDBLoad,labelSet,$window,checkAuthorization,checkBrowserDetails,messageSending){
        var vm = this;
        var perrr = [];
         console.log('insi');   
        function initRootScope() {
            //after login set login variable in root scope
             vm.showList = [];
             vm.showList1 = [];
             $rootScope.locationSetter = false;
             vm.reqChangePass = {
              confPassword:'',
              oldPassword:'',
              newPassword:''
             }
             vm.lang = 'English';
             $rootScope.mute = true;
             vm.UserProfile = {
              userName: localStorage['user_id'],
              fullName: localStorage['userName'],
              company: localStorage['companyName'],
              role: localStorage['role'],
              email: localStorage['email'],
              phone: localStorage['phone']
             }
             vm.notShowableList = [];
            if(!localStorage['access_token']){
              $rootScope.login = false;
            }
            vm.case = 0;
            vm.username = localStorage['user_id']+"-"+localStorage['userName'];
            vm.userId = localStorage['user_id'];
            if(localStorage['access_token']){
            permission();

            }
            vm.setHeaderCol = true;
            
        }
        initRootScope();
        $scope.$on('setItems',function (evt,res) {
          if(localStorage['path'] == '/order-entry'){
               vm.EOR = res.eor;
               vm.case = res.case;
               vm.setHeaderCol = true;
            }else{
              vm.setHeaderCol = false;
            }
           
        })
         vm.lang = "English";
        vm.setLan = function (lan) {
          //console.log(lan);
          localStorage.setItem('lang',lan);
          var langs = labelSet.getLabel(lan,"Login");
          $rootScope.$broadcast('setlang',lan);
        }
        vm.sendTrackData = function () {
        var systemStats = eval(localStorage['userTrack']);
       
        var req1 = 
        {
             systemStat:  JSON.stringify(systemStats)                   
        }

          requestManager.sendUserTackingData(req1).then(function (result) 
          {
             localStorage.removeItem('userTrack');
        }) 
       }
       vm.sendTrackData();
       $scope.$on('userTacking',function (evt) {
         vm.sendTrackData();
       })
   $scope.$on('userClickingData',function (evt,data,event) {
    
     checkBrowserDetails.setTrackerDetails(data.target,event,$scope.longitude,$scope.latitude,$scope.locationAddress)
   })

   var options = {
          enableHighAccuracy: true,
          timeout: 5000,
          maximumAge: 0
        };
            function success(pos)
             {
              var crd = pos.coords;
              $rootScope.locationSetter = true;
              console.log('go ins');
            //  alert('Current Position:' +'Latitude : ' + crd.latitude + 'Longitude: ' + crd.longitude + 'More or less ' + crd.accuracy + ' meters.');
              GetAddress(crd.latitude,crd.longitude);
            };
            function error(err) {
              $rootScope.locationSetter = false;
              console.warn('ERROR(' + err.code + '): ' + err.message);
            };
            function GetAddress(latitude,longitude) {
                        var lat = parseFloat(latitude);
                        var lng = parseFloat(longitude);
                        $scope.longitude = lng;
                        $scope.latitude = lat;
                        var latlng = new google.maps.LatLng(lat, lng);
                        var geocoder = geocoder = new google.maps.Geocoder();
                        geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                if (results[1]) {
                                  $scope.locationAddress = results[1].formatted_address;
                                   // alert("Location: " + results[1].formatted_address);
                                }
                            }
                        });
                    }

                        vm.getLocation = function (argument) {   

                        if (navigator.geolocation) {
                            
                            navigator.geolocation.getCurrentPosition(success, error, options);
                            
                          }
                        }
        vm.getLocation();
         function permission() {
            if($rootScope.online == true){
                var req = {
                     userId: localStorage['user_id'],
                     appId: '23'
                }
                 $rootScope.isLoading = true;

                requestManager.permissionRequestPost(req).then(function (result) {  
                //console.log('show res***'+JSON.stringify(result));                    
                     if(!localStorage['access_token']){
                     $rootScope.isLoading = false;
                
            }
           // vm.sendTrackData()
             var perList = checkAuthorization.getPermissionMenu(result);
             for(var i =0 ;i<perList.length;i++){
              if(perList[i].AccessName == "ezorder" || perList[i].AccessName == "reports" || perList[i].AccessName == "cashmanagement" || perList[i].AccessName == "adminmanagement"){
                        vm.notShowableList.push(perList[i]);
                        
                      }else{
                        vm.showList.push(perList[i]);
                      }
             }
           //  console.log("show"+JSON.stringify(vm.showList));
                    vm.per = vm.showList;
                    var lists = vm.notShowableList;
                    vm.otherPer = lists; 
                  //  console.log("show"+JSON.stringify(vm.otherPer));        
                })
             }
            }
            
        /*function getPermisssionList(){
                if($rootScope.online == false){
                    $rootScope.stopCall = true;
                     $rootScope.isLoading = true;
            FactoryIndexedDBLoad.getPermissionIndexDb().then(function(result) {
                if(result){
                    $rootScope.isLoading = false;
                       var perList1 = checkAuthorization.getPermissionMenu(result);
                     for(var i =0 ;i<perList1.length;i++){
                      if(perList[i].AccessName == "ezorder" || perList[i].AccessName == "reports"){
                         vm.notShowableList.push(perList1[i]);
                      }else{
                        vm.showList1.push(perList1[i]);
                      }
                     }
              
                   //  console.log("show"+JSON.stringify(showList));
                    vm.per = vm.showList1;
                     var lists = vm.notShowableList;
                    vm.otherPer = {
                        reports:lists[1].IsAccess,
                        ezorder:lists[0].IsAccess
                      }
                    }
                })
            }
        }

   getPermisssionList(); */ 
   vm.userClickDetails = [];
   vm.confPass = true;
        $scope.$on('usersend',function  (event,res) {
           initRootScope();
        })
        $scope.$on('permit',function (event,data) {
            //vm.per = data;
            //console.log('reeeee'+JSON.stringify(vm.per))
        })
        vm.setMute = function (bool) {
          $rootScope.mute = bool;
          console.log($rootScope.mute);
        }
        vm.passwordConfirm = function (req,type) {
        // console.log(req);
         var reqObject = {
          "UserName":localStorage['user_id'],
          "OldPassword":req.oldPassword,
          "NewPassword":req.newPassword,
          "ConfirmPassword":req.confPassword
         }
         vm.confPass = checkAuthorization.confirmPassword(reqObject);
        // $rootScope.isLoading = true;
         if(vm.confPass == true){
            requestManager.changePassword(reqObject).then(function (response) {
          //  console.log(JSON.stringify(response));
           // $rootScope.isLoading = false;
           
            if(type!='Profile'){
              localStorage.removeItem('access_token');
              localStorage.removeItem('token_type');
              localStorage.removeItem('user_Id');
              localStorage.removeItem('user_id');
              localStorage.removeItem('Cust_id');
              localStorage.removeItem('Cust_Name');
              localStorage.removeItem('userName');
              localStorage.removeItem('menuPer');
              $('#passwordChange').modal('hide');
              messageSending.recieveMessage('Password Change Please Login Again.')
              $state.go('login'); 
            }else{
              messageSending.recieveMessage('Password Change');
              vm.changePassMes = messageSending.setMessage();
             // console.log(vm.changePassMes)
            }
            
             // body...
           })
         }
         
        }
        vm.Profile = function (req) {
         
          var req = {
            UserName: req.userName,
            Email: req.email,
            Phone: req.phone
          }
          $rootScope.isLoading = true;
          requestManager.updateProfile(req).then(function (result) {
           // $('#myProfile').modal('hide');

            $rootScope.isLoading = false;

            localStorage.setItem("email",req.Email);
            localStorage.setItem("phone", req.Phone);

            messageSending.recieveMessage('Profile Updated!');
            vm.changeProfileMes = messageSending.setMessage();
           // console.log(result);
          })
        }
        vm.close = function () {
          vm.reqChangePass = {
              confPassword:'',
              oldPassword:'',
              newPassword:'',
              password:''
             }
             vm.confPass = true;
             $('#passwordChange').modal('hide');
        }
        vm.closeProfile = function () {
          vm.UserProfile = {
              userName: localStorage['user_id'],
              fullName: localStorage['userName'],
              company: localStorage['companyName'],
              role: localStorage['role'],
              email: localStorage['email'],
              phone: localStorage['phone']
             }
             vm.changeProfileMes = '';
             messageSending.recieveMessage('');
        }
        vm.logout = function () {
             $rootScope.isLoading=false;
            localStorage.removeItem('access_token');
            localStorage.removeItem('token_type');
            localStorage.removeItem('user_Id');
            localStorage.removeItem('user_id');
            localStorage.removeItem('Cust_id');
            localStorage.removeItem('Cust_Name');
            localStorage.removeItem('userName');
            localStorage.removeItem('menuPer');
            localStorage.removeItem('phone');
            localStorage.removeItem('email');
            localStorage.removeItem('role');
            localStorage.removeItem('companyName');
            localStorage.removeItem('userTrack');
            localStorage.setItem('lang','English');
              var indexedDB = $window.indexedDB;
                var request =  indexedDB.open("GOYAOMS",4);
                indexedDB =  indexedDB ||  mozIndexedDB || webkitIndexedDB ||  msIndexedDB;
                //prefixes of window.IDB objects
                window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
                window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange

                if (!indexedDB) {
                    //console.log("Your browser doesn't support a stable version of IndexedDB.")
                }

                var DBDeleteRequest = indexedDB.deleteDatabase("GOYAOMS");
                DBDeleteRequest.onerror = function(event) {
                // console.log("Error deleting database.");
                };
                 
                DBDeleteRequest.onsuccess = function(event) {
                 // console.log("Database deleted successfully");
                    
                };     
            $state.go("login");


            /*requestManager.logOut().then(function(result) {
                    console.log('logOut response: ' + JSON.stringify(result));
                    //$rootScope.login = true;
                    //$location.url('/order-entry');
                    //$state.go("login");
            });*/
        }
        vm.getOrderDataFromIndexDB = function(){
          if($rootScope.online == true){
            var con = confirm('Are you sure want to Syncronize these Orders?');
          console.log(con)
          if(con==true){
            FactoryIndexedDBLoad.getOrderData().then(function (result) {
            var indexdbOrderData = result[0];
                vm.dbOrderData=result;
            console.log('orderData****'+JSON.stringify(vm.dbOrderData));
              if(indexdbOrderData!=undefined){
                indexDBAddItemOrder(vm.dbOrderData);
              }
              
                });  
            }  
          }      
        }
       
        

        function indexDBAddItemOrder(data){
          var reqObject=data;
          console.log('Order Data****'+JSON.stringify(reqObject));
            $rootScope.isLoading = true;
            for(var i=0;i<data.length;i++){
               requestManager.AddItemToOrder(reqObject[i]).then(function(result) 
               { 
                     if(i == data.length){
                        location.reload();
                        $rootScope.isLoading = false;
                     }                         
                 }); 
            }
            
         
        }

         
    }

    headerController.$inject=['$scope','$rootScope','$location', '$state','requestManager','FactoryIndexedDBLoad','labelSet','$window','checkAuthorization','checkBrowserDetails','messageSending'];

    return headerController;
});
