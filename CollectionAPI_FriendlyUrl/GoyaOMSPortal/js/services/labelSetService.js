define([], function(app) {
    'use strict';

    function labelSet($q, $http,$state,$rootScope) {
    	return {
    		getLabel: function (lan,page) {
    			this.obj = {};
                var saveLabels = {} ;
                var pagePath = localStorage['path'];
                var page = page;
                var Language ;
                if(lan == 'English'){
                    Language = 'EN-US';
                }else{
                   Language = 'EN-MX'; 
                }
                var labels = JSON.parse(localStorage['Labels']);
                console.log(labels);
                for(var i =0;i<labels.length;i++){
                    if(labels[i].Language == Language && labels[i].objectname1 == page){
                        saveLabels[labels[i].ControlKey] = labels[i].ControlValue;
                    }                    
                }
                this.obj = saveLabels;
                console.log(this.obj)
                
    		},
            setLabels: function () {
                var langLabel = {
                    lan: this.obj
                }
                return langLabel;
            }
            
    	  }
    	}

    labelSet.$inject = ['$q', '$http','$state','$rootScope'];

    return labelSet;
});