define([], function(app) {
    'use strict';

    function geoLocationService($q) {	
        this.options = {
              enableHighAccuracy: true,
              timeout: 5000,
              maximumAge: 0
            };
            var defer = $q.defer();
    		this.success = function(pos) {
              var crd = pos.coords;
             // alert('Current Position:' +'Latitude : ' + crd.latitude + 'Longitude: ' + crd.longitude + 'More or less ' + crd.accuracy + ' meters.');
              this.cord = {
                longitude: crd.latitude,
                latitude: crd.longitude
              }
            }
            this.error = function(err) {
              console.warn('ERROR(' + err.code + '): ' + err.message);
            }
            this.GetAddress = function(latitude,longitude) {
            var lat = parseFloat(latitude);
            var lng = parseFloat(longitude);
            var latlng = new google.maps.LatLng(lat, lng);
            var geocoder  = new google.maps.Geocoder();
            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        defer.resolve(results[1].formatted_address);
                        //alert("Location: " + results[1].formatted_address);
                    }
                }
            }); 
            return defer.promise;
        }
            this.getLocation = function () {
                alert(this.cord)
                 return  this.cord;             
            }
                this.setLocation = function () {
                     if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(this.success,this.error,this.options);
                   // console.log(navigator.geolocation);
                    
                  }
                }
    		
    

    }
    geoLocationService.$inject = ['$q'];

    return geoLocationService;
})