define([], function(app){
    'use strict';
  function OrderPreviewController(requestManager,$rootScope)
   {
      
       return {
        restrict: 'E',
        replace: true,
        scope: {
            mobDetails:"="
        },
     
        templateUrl: 'html/pages/customer-info.html',
        link: function ($scope) 
        {
            $scope.brokerName = localStorage['userName'];             
            $scope.Action = '';
            $scope.PONumber='';
            $scope.TotalOrderQty = 0;
            $scope.isPickUp = false;
            $scope.DeliveryDate='';
            $scope.totalCasePrice = 0;
            $scope.OrderPreview='';
            var options = {
          enableHighAccuracy: true,
          timeout: 5000,
          maximumAge: 0
        };
            function success(pos)
             {
              var crd = pos.coords;

            //  alert('Current Position:' +'Latitude : ' + crd.latitude + 'Longitude: ' + crd.longitude + 'More or less ' + crd.accuracy + ' meters.');
              GetAddress(crd.latitude,crd.longitude);
            };
            function error(err) {
              console.warn('ERROR(' + err.code + '): ' + err.message);
            };
            function GetAddress(latitude,longitude) {
                        var lat = parseFloat(latitude);
                        var lng = parseFloat(longitude);
                        $scope.longitude = lng;
                        $scope.latitude = lat;
                        var latlng = new google.maps.LatLng(lat, lng);
                        var geocoder = geocoder = new google.maps.Geocoder();
                        geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                if (results[1]) {
                                  $scope.locationAddress = results[1].formatted_address;
                                   // alert("Location: " + results[1].formatted_address);
                                }
                            }
                        });
                    }

                        $scope.getLocation = function (argument) {                         
                        if (navigator.geolocation) {
                           // console.log(navigator.geolocation);
                            navigator.geolocation.getCurrentPosition(success, error, options);
                            
                          }
                        }
                     $scope.getLocation();
             $scope.setPreview = function () {              
               var modal_height = $('.item-dtlnew').height()*$scope.tot*3+500;
               var printStyle  = $('#print');
               var style = '@media print { body{ visibility: hidden !important; page-break-after:always; width:100%} .amm{ visibility: hidden !important ; }.overview-div{width:100% !important}.visible-area { visibility: visible !important; width:120% !important;position:relative;left:-50px !important; } .modal{ position: absolute !important; top: -20px !important; height:'+modal_height+'px !important; } .new-modal{ margin-top:0px !important; padding-top: 1px; } .over-nww{ width: 50% !important; } .print_r{ display: none; }.cart-fxt{margin-top:0px !important;} .newli1 .over-left{width:28%;} .newli1 .over-left1{width:70%}   .newli2 .over-left{width:38% !important;height:30px} .newli2 .over-left1{width:62% !important;height:30px}.overview-div  .over-ul li{padding-right:0 !important;} }}}';
               printStyle.text(style);             
               window.print();
             } 
             $scope.formatTelNumber = function(number) {
              if(number!=undefined){
                return number.match(/\d/g)
                    .join('')
                    .replace(
                        /(\d{3})(\d{3})(\d{4})/,
                       '($1) $2-$3'
                    );
              }
            }
       
            // Submit Order Data
            $scope.submitOrder = function()
            {
              var submitConfirm;
              submitConfirm = confirm("Are you sure want to Submit order?")   
              if(submitConfirm)
                   submitOrderData();
            }

        // Submit Order Detail
        function submitOrderData()
        {            
       // debugger;
       if($scope.latitude==undefined || $scope.latitude =='' || $scope.latitude ==null)
                     $scope.latitude = 0;

                   if($scope.longitude==undefined || $scope.longitude =='' ||$scope.longitude==null)
                     $scope.longitude = 0;

                   if($scope.locationAddress==undefined || $scope.locationAddress =='' ||  $scope.locationAddress==null)
                    $scope.locationAddress = '';
               var req = 
                      {                        
                         CustomerId:$scope.OrderPreview[0].CustomerID,
                         CompanyId:'2',
                         BrokerId:$scope.OrderPreview[0].SalesmanID,
                         CatalogId:"1",
                         day: $scope.OrderPreview[0].OrderDay,                       
                         notDay: $scope.OrderPreview[0].NotDay,  
                         Comments:$scope.OrderPreview[0].Comments,
                         isPickUp:$scope.isPickUp,
                         EOR:$scope.OrderPreview[0].PONumber,
                         AmountCollected:$scope.OrderPreview[0].ActualAmount,
                         isActive:'1',                        
                         Latitude: $scope.latitude,
                         Longitude: $scope.longitude,
                         Location: $scope.locationAddress,
                         totalCaseQuantity:$scope.TotalOrderQty,
                         totalAmount:'3',
                         DeliveryDate:$scope.OrderPreview[0].DeliveryDate,
                         ClientPONumber:$scope.OrderPreview[0].ClientPONumber,                      
                         productType: '',// need to fixed .... $scope.OrderPreview[0].'',
                         templateName:'',
                         totalUnitQuantity:$scope.TotalUnitsQty,

                      } ;
                      console.log(req)

             requestManager.SubmitOrder(req).then(function(result) 
             {           
                            $scope.mobDet.StatusText = "Submitted";
                           alert('Order ' + $scope.OrderPreview[0].PONumber+' Submitted Successfully!'); 
                         

                           //resetOrderScreen();     
             });

        }

           $scope.$watch('mobDetails',function () 
           {
               $scope.OrderPreview='';
            if($scope.mobDetails){
              console.log('show mob details'+JSON.stringify($scope.mobDetails));
              if($scope.mobDetails.Pickup == "true")
              {
                $scope.mobDet = $scope.mobDetails;
                $scope.pickup = 'Pickup';
                $scope.isPickUp = true;

              }else
              {
                $scope.mobDet = $scope.mobDetails;
                $scope.pickup = 'Regular';
                $scope.isPickUp = false;
              } 
              console.log('customerDetail Payload: ' + JSON.stringify($scope.mobDet));

              if($scope.mobDet.BasketID !='')
              {
                 $scope.PONumber = $scope.mobDet.BasketID;
                 $scope.Action='B';
              }
              else
              {
                 $scope.PONumber = $scope.mobDet.PONumber;
                 $scope.Action='P';
              }
              var req='';
              
              if($scope.mobDetails.PONumber.length>4)
              {
               req = 
               {
                    BasketId:$scope.PONumber,
                    CompanyId:'2',                                     
                    Action:$scope.Action
                } 
              }
              else  // Order Preview from AS400 DB
              {
                req = 
               {
                    BasketId:$scope.mobDetails.OrderNo,
                    GoyaCompanyId:'01',                                     
                    Action:$scope.Action
                } 

              }

           
           $rootScope.$broadcast('sendMsg');
           $scope.totalCasePrice = 0;
        
          if($scope.mobDetails.PONumber.length>4)
          {
             requestManager.GetOrderPreview(req).then(function(result)  // Order Preview from OMS
            {      
            $rootScope.$broadcast('disconsendMsg')                              
                $rootScope.isLoading = false;  
                if (result.Payload) 
                {     
                    $scope.tot = result.Payload.length; 
                          
                   $scope.OrderPreview = result.Payload;

                    console.log(JSON.stringify($scope.OrderPreview)); 
                   // Order Header Details console.log($scope.tot)  
                   $scope.TotalOrderQty = result.Payload[0].TotalOrderQty;
                   for(var i = 0;i<$scope.OrderPreview.length;i++){
                    $scope.totalCasePrice = $scope.totalCasePrice+parseFloat(result.Payload[i].CasePrice);

                   }
                   if($scope.isPickUp)
                      $scope.TotalUnitsQty = result.Payload[0].TotalUnitsQty;
                    else
                      $scope.TotalUnitsQty = '';
                    
                   $scope.Address = result.Payload[0].Street +","+result.Payload[0].City+","+result.Payload[0].State+","+result.Payload[0].Zip;
                   $scope.PhoneNo = result.Payload[0].ContactNo;
                   $scope.Comments = result.Payload[0].Comments;
                   $scope.DeliveryDate = result.Payload[0].DeliveryDate;
                   $scope.ClientPONumber = result.Payload[0].ClientPONumber;
                   $scope.PromoName = result.Payload[0].PromoName;
                   $scope.OrderDay = result.Payload[0].OrderDay;
                   $scope.NotDay = result.Payload[0].NotDay;
                   $scope.PromoName = result.Payload[0].PromoName;
                   $scope.ActualAmount = result.Payload[0].ActualAmount;

                   if($scope.ActualAmount=='')
                        $scope.ActualAmount='0';


                    
                   //  console.log('customerDetail Payload: ' + JSON.stringify($scope.OrderPreview));
                }
            });
           }
          else 
          {
            requestManager.GetOrderPreviewDB2(req).then(function(result)   // Order Preview from AS400 DB
            {                                    
                $rootScope.isLoading = false;  
                if (result.Payload) 
                {     
                    $scope.tot = result.Payload.length; 
                          
                   $scope.OrderPreview = result.Payload;

                    console.log(JSON.stringify($scope.OrderPreview)); 
                   // Order Header Details console.log($scope.tot)  
                   $scope.TotalOrderQty = result.Payload[0].TotalOrderQty;
                   for(var i = 0;i<$scope.OrderPreview.length;i++){
                    $scope.totalCasePrice = $scope.totalCasePrice+parseFloat(result.Payload[i].CasePrice);

                   }
                   if($scope.isPickUp)
                      $scope.TotalUnitsQty = result.Payload[0].TotalUnitsQty;
                    else
                      $scope.TotalUnitsQty = '';
                    
                   $scope.Address = result.Payload[0].Street +","+result.Payload[0].City+","+result.Payload[0].State+","+result.Payload[0].Zip;
                   $scope.PhoneNo = result.Payload[0].ContactNo;
                   $scope.Comments = result.Payload[0].Comments;
                   $scope.DeliveryDate = result.Payload[0].DeliveryDate;
                   $scope.ClientPONumber = result.Payload[0].ClientPONumber;
                   $scope.PromoName = result.Payload[0].PromoName;
                   $scope.OrderDay = result.Payload[0].OrderDay;
                   $scope.NotDay = result.Payload[0].NotDay;
                   $scope.PromoName = result.Payload[0].PromoName;
                   $scope.ActualAmount = result.Payload[0].ActualAmount;

                   if($scope.ActualAmount=='')
                        $scope.ActualAmount='0';


                    
                   //  console.log('customerDetail Payload: ' + JSON.stringify($scope.OrderPreview));
                }
            });
          }
            /////////////////////////////


            }
           })
             

           
      }
      
            
        }

      };
     


    OrderPreviewController.$inject=['requestManager','$rootScope'];

    return OrderPreviewController;
 
});


