define([], function(app){
    'use strict';

function orderStatus($rootScope,$compile){
	return{
		restrict:'EA',
		templateUrl:'html/templates/orderStatus.html',
		scope:{
			status:'@'
		},
		link: function($scope,element,attribute){
	     $scope.EORStatus = '';
	     $scope.eorLen = $scope.status.length.toString();
		 if($scope.status.length > '1' && $scope.status.length < '5')
		 {
		 	$scope.EORStatus = '(H)';
		 }
		 if($scope.status.length >'4')
		 {
		 	$scope.EORStatus = '(W)';
		 }
		 if($scope.status.length == '1')
		 {
		 	$scope.EORStatus = '(E)';
		 }	
		 
		}

	}
}
orderStatus.$inject = ['$rootScope','$compile'];
return orderStatus;
});