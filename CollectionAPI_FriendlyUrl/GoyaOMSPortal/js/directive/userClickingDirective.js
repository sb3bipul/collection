define([], function(app){
    'use strict';

function userclick($rootScope){
	return{
		restrict:'A',
		link: function($scope,element,attribute){	
		//console.log('inside me')		
			element.on('click',function ($event) {
				var userData = $event;
				var userEventDetails = 'click';			 		
			 	if($($event.target).is("input") || $($event.target).is("a") || $($event.target).is("li") || $($event.target).is("button")){
			 		//console.log($event);
			 		$rootScope.$broadcast('userClickingData',userData,userEventDetails);
			 	}			
							
			})
			element.on('change',function ($event) {
				var userData = $event;
				var userEventDetails = 'select';			 		
			 	if($($event.target).is("select")){
			 		//console.log('select');
			 		//console.log($event);
			 		$rootScope.$broadcast('userClickingData',userData,userEventDetails);
			 	}			
							
			})
			$(document).ready(function () {
				$(document).click(function (e) {
					 if($('.wrapper').hasClass('toggled') && !$(e.target).hasClass('navbar-toggle')){
					 	$('.wrapper').removeClass('toggled');
					 }
					 if($(e.target).closest('.mb-lahd1').length == 0 && $(e.target).closest('.newfldd').length == 0 && $(e.target).closest('.indexSet').length != 1){
					 	console.log('go in none');
					 	$('#modalShowItem').css('display','none');
					 	$('#modalShowOrderDetails').css('display','none');
					 }
			    })
			})
			
		}
	}
}
userclick.$inject = ['$rootScope'];
return userclick;
});