define([], function(app){
    'use strict';

function excelConvert($rootScope,$filter){
	return{
		restrict:'EA',
		scope:{
			excelData:"="
		},
		link: function($scope,element,attribute){			
			element.bind('click',function ($event) {
				//console.log('hii')
				var arrData = typeof $scope.excelData != 'object' ? JSON.parse($scope.excelData) : $scope.excelData;				    
				    var CSV = '';  
				    var ReportTitle = $('#headerRep').text();  
				    var date = $filter('date')(new Date(), 'MM/dd/yyyy');
				    var ShowLabel = true;    
				    CSV += ','+ReportTitle + '\r\n\n';
				    CSV += ',Date:'+date + '\r\n\n';
				    if (ShowLabel) {
				        var row = "";
				        for (var index in arrData[0]) {
				           	row += index+',';
				            //console.log(row);
				        }
				        row = row.slice(0, -1);
				        CSV += row + '\r\n'; 
				        CSV = CSV.replace(',$$hashKey',"");
				        //console.log(CSV)
				    }

				    for (var i = 0; i < arrData.length; i++) {
				        var row = "";
				      
				        for (var index in arrData[i]) {
				            delete arrData[i]['$$hashKey'];
				        	
				        		row += '"' + arrData[i][index] + '",';
		            
				        }
				        row.slice(0, row.length - 1);				        				      
				        CSV += row + '\r\n';
				        CSV = CSV.replace(',$$hashKey',"");
				       // console.log(CSV)
				    }
				    if (CSV == '') {        
				        alert("Invalid data");
				        return;
				    }   
	
				    var fileName = "Goya_Report_";
				   
				    fileName += ReportTitle.replace(/ /g,"_");   
		
				    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
				    
			
				    var link = document.createElement("a");    
				    link.href = uri;
				   
				    link.style = "visibility:hidden";
				    link.download = fileName + ".csv";
			
				    document.body.appendChild(link);
				    link.click();
				    document.body.removeChild(link);
			})				
				
		}
	}
}
excelConvert.$inject = ['$rootScope','$filter'];
return excelConvert;
});