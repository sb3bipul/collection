define([], function(app){
    'use strict';

function graphDetails($rootScope,$timeout){
    return{
        restrict:'EA',
        scope:{
            data: '='
        },
        template:'<div id="container" style="width:600px; height: 400px; margin: 0 auto;"></div>',
        link: function($scope,element,attribute){
            console.log('go inside');
                $scope.$watch('data',function (val) {
                    if($scope.data !={}){
                       var chart = new Highcharts.Chart({
                    chart: {
                        renderTo: 'container',
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    },
                    title: {
                      // must change to dynamic
                        text: $scope.data.text
                        //scope.title
                    },
                     exporting: {
                        enabled: true
                    },
                    tooltip: {
                             pointFormat: '{series.name}: <b>{point.percentage}% - {point.y}/{point.total}  </b>',
                        percentageDecimals: 1
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                color: '#000000',
                                connectorColor: '#000000',
                                percentageDecimals:1,
                                formatter: function () {
                                    //return '<b>' + this.point.name + '</b>: ' + this.percentage + ' %';
                                   //return '<b>' + this.point.name + '</b>: '  +Math.round(this.percentage)+ ' %';
                                    return '<b>' + this.point.name + '</b>: '  +  this.percentage.toFixed(1)+ ' %';

                                }
                            }
                        }
                    },
            series: [{
                name: $scope.data.name,
                point: {
                        events: {
                            click: function (e) {
                                console.log(this.name);
                                console.log(this.y);
                                var req = {
                                    name:this.name,
                                    value:this.y,
                                    type: $scope.data.type
                                }
                                $rootScope.$broadcast('setGraphEvent',req);

                            }
                        }
                    },
                type: $scope.data.type,
                data: $scope.data.data
            }]
                }); 
                    }
                console.log($scope.data);

                    
               
               
            },true) 
          
                   
        
         }
        }
}
graphDetails.$inject = ['$rootScope','$timeout'];
return graphDetails;
});




