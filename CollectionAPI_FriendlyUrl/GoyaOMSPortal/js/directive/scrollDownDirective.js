

define([], function(app){
    'use strict';
    function scrollDown($rootScope){
         return {
          restrict: 'A',
          link: function (scope, element, attrs) {
          var fn = scope.$eval(attrs.execOnScrollToBottom),
          clientHeight = element[0].clientHeight;
          
          element.on('scroll', function (e) {
            var el = e.target;
            //console.log('goooooooo')
            if ((el.scrollHeight - el.scrollTop) === clientHeight) { // fully scrolled
              console.log("scrolled to bottom...");
              $rootScope.$broadcast('setItem');
            }
          });
        }

     };
    }

    scrollDown.$inject=['$rootScope'];

    return scrollDown;

});



