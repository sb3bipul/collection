define([], function(app){
    'use strict';

function focusItem($rootScope,$timeout){
	return{
		restrict:'A',
		link: function($scope,element,attribute){	
		//console.log('inside me')		
			element.bind('click',function ($event) {
				$event.stopPropagation();
				$timeout(function () {
					var el = $('#txtItemCode');
					var elM = $('#txtItemCodeMobile');
					elM .focus();
				    el.focus();
				},500)
							
							
			})
		}
	}
}
focusItem.$inject = ['$rootScope','$timeout'];
return focusItem;
});