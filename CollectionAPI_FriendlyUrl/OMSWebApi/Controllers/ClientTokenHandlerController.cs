﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using OMSWebApi.Repository;
using System.Threading.Tasks;
using Braintree;

namespace OMSWebApi.Controllers
{
    public class ClientTokenHandlerController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        ClientTokenHandlerRepository clienttoken = null;
        /// <summary>
        /// get client token
        /// </summary>
        [HttpPost]
        [Route("api/clienttoken/getClientToken")]
        public HttpResponseMessage getClientToken(HttpContext context)
        {
            try
            {
                clienttoken = new ClientTokenHandlerRepository();
                StringContent sc = new StringContent(clienttoken.ProcessRequest(context));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/clienttoken/getClientToken", ex);
            }
            finally
            {
                clienttoken = null;
            }
            return null;
        }

        //[HttpPost]
        //[AllowAnonymous]
        //[Route("api/clienttoken/CreateTransection")]
        //public async Task<IHttpActionResult>CreateTransection(Microsoft.Owin.FormCollection collection)
        //{
        //    var gateway = new BraintreeGateway
        //    {
        //        Environment = Braintree.Environment.SANDBOX,
        //        MerchantId = "zhbjjhqb85wggwjt",
        //        PublicKey = "87p92br7g5nk3zp3",
        //        PrivateKey = "d6953b910908d9ab278acc7aef810356"
        //    };

        //    string nonceFromTheClient = collection["payment_method_nonce"];
        //    var request = new TransactionRequest
        //    {
        //        Amount = 10.00M,
        //        PaymentMethodNonce = nonceFromTheClient,
        //        DeviceData = deviceDataFromTheClient,
        //        Options = new TransactionOptionsRequest
        //        {
        //            SubmitForSettlement = true
        //        }
        //    };

        //    Result<Transaction> result = gateway.Transaction.Sale(request);
        //    return null;
        //}
    }
}
