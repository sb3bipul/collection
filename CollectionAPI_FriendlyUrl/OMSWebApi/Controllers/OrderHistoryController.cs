﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;
using OMSWebApi.Models;
using OMSWebApi.Repository;
using System.Data.Entity;


namespace OMSWebApi.Controllers
{
    public class OrderHistoryController : ApiController
    {
        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            
        OrderHistoryRepository orderHistory = null;

        /// <summary>
        /// Get NON Pending Order History
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="customerId"></param>
        /// <param name="orderNumber"></param>
        /// <param name="status"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        //[Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/OrderHistory/GetOrderHistoryTest")]
        public HttpResponseMessage GetOrderHistory(Order objOrderHistory)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                //// Check if the client request have header with Authorization key
                //if (headers.Contains("Authorization"))
                //{
                //    // Read token received from client's request in header
                //    token = headers.GetValues("Authorization").First();
                //    userId = "013030";//headers.GetValues("UserId").First();
                //}

                //// Check if token received from client is valid, if validated then send customer list otherwise unauthorized note.
                //if (userController.IsValidToken(token, userId))
                //{
                    orderHistory = new OrderHistoryRepository();
                    StringContent sc = new StringContent(orderHistory.GetOrderHistory(objOrderHistory.BrokerId, objOrderHistory.CustomerId, objOrderHistory.OrderNumber, objOrderHistory.Status, objOrderHistory.FromDate, objOrderHistory.ToDate, objOrderHistory.CompanyId));
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;
                    return data;
                //}
                //else
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");

            }
            catch (Exception ex)
            {
                log.Error("-> api/OrderHistory/GetOrderHistory", ex);
            }
            finally
            {
                orderHistory = null;
            }
            return null;
        }

        /// <summary>
        /// Get Pending Order History
        /// </summary>
        /// <param name="objOrderHistory"></param>
        /// <returns></returns>
      //  [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/OrderHistory/GetPendingOrderHistory")]
        public HttpResponseMessage GetPendingOrderHistory(Order objOrderHistory)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                //// Check if the client request have header with Authorization key
                //if (headers.Contains("Authorization"))
                //{
                //    // Read token received from client's request in header
                //    token = headers.GetValues("Authorization").First();
                //    userId = "013030";//headers.GetValues("UserId").First();
                //}

                //// Check if token received from client is valid, if validated then send customer list otherwise unauthorized note.
                //if (userController.IsValidToken(token, userId))
                //{
                    orderHistory = new OrderHistoryRepository();
                    StringContent sc = new StringContent(orderHistory.GetPendingOrderHistoryByCustomer(objOrderHistory.BrokerId, objOrderHistory.CustomerId, objOrderHistory.FromDate, objOrderHistory.ToDate, objOrderHistory.CompanyId));
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;
                    return data;
                //}
                //else
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");

            }
            catch (Exception ex)
            {
                log.Error("-> api/OrderHistory/GetPendingOrderHistory", ex);
            }
            finally
            {
                orderHistory = null;
            }
            return null;
        }


        /// <summary>
        /// Get All Order History
        /// </summary>
        /// <param name="objOrderHistory"></param>
        /// <returns></returns>

     //   [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/OrderHistory/GetAllOrderStatusPost")]
        public HttpResponseMessage GetAllOrderStatusPost(Order objOrderHistory)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                //// Check if the client request have header with Authorization key
                //if (headers.Contains("Authorization"))
                //{
                //    // Read token received from client's request in header
                //    token = headers.GetValues("Authorization").First();
                //    userId = "013030";//headers.GetValues("UserId").First();
                //}

                //// Check if token received from client is valid, if validated then send customer list otherwise unauthorized note.
                //if (userController.IsValidToken(token, userId))
                //{
                    orderHistory = new OrderHistoryRepository();
                    StringContent sc = new StringContent(orderHistory.GetAllOrderStatus());
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;
                    return data;
                //}
                //else
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");

            }
            catch (Exception ex)
            {
                log.Error("-> api/OrderHistory/GetAllOrderStatusPost", ex);
            }
            finally
            {
                orderHistory = null;
            }
            return null;
        }

        /// <summary>
        /// Delete Order from Order History
        /// </summary>
        /// <param name="objOrderHistory"></param>
        /// <returns></returns>
      //  [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/OrderHistory/DeleteOrder")]
        public HttpResponseMessage DeleteOrder(Order objOrderHistory)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                //// Check if the client request have header with Authorization key
                //if (headers.Contains("Authorization"))
                //{
                //    // Read token received from client's request in header
                //    token = headers.GetValues("Authorization").First();
                //    userId = "013030";//headers.GetValues("UserId").First();
                //}

                //// Check if token received from client is valid, if validated then send customer list otherwise unauthorized note.
                //if (userController.IsValidToken(token, userId))
                //{
                    orderHistory = new OrderHistoryRepository();
                    StringContent sc = new StringContent(orderHistory.DeleteOrder(objOrderHistory.BasketId, objOrderHistory.OrderNumber, objOrderHistory.BrokerId, objOrderHistory.Message));
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;
                    return data;
                //}
                //else
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");

            }
            catch (Exception ex)
            {
                log.Error("-> api/OrderHistory/GetAllOrderStatusPost", ex);
            }
            finally
            {
                orderHistory = null;
            }
            return null;
        }


     //   [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/OrderHistory/GetOrderPreview")]
        public HttpResponseMessage GetOrderPreview(Order objOrderHistory)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                //// Check if the client request have header with Authorization key
                //if (headers.Contains("Authorization"))
                //{
                //    // Read token received from client's request in header
                //    token = headers.GetValues("Authorization").First();
                //    userId = "013030";//headers.GetValues("UserId").First();
                //}

                //// Check if token received from client is valid, if validated then send customer list otherwise unauthorized note.
                //if (userController.IsValidToken(token, userId))
                //{
                    orderHistory = new OrderHistoryRepository();
                    StringContent sc = new StringContent(orderHistory.GetOrderPreview(objOrderHistory.BasketId, objOrderHistory.CompanyId, objOrderHistory.Action));
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;
                    return data;
                //}
                //else
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");

            }
            catch (Exception ex)
            {
                log.Error("-> api/OrderHistory/GetAllOrderStatusPost", ex);
            }
            finally
            {
                orderHistory = null;
            }
            return null;
        }

     //   [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/OrderHistory/GetOrderPreviewDB2")]
        public HttpResponseMessage GetOrderPreviewDB2(Order objOrderHistory)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;
 
                orderHistory = new OrderHistoryRepository();
                StringContent sc = new StringContent(orderHistory.GetOrderPreviewDB2(objOrderHistory.BasketId, objOrderHistory.GoyaCompanyId, objOrderHistory.Action));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
               
            }
            catch (Exception ex)
            {
                log.Error("-> api/OrderHistory/GetOrderPreviewDB2", ex);
            }
            finally
            {
                orderHistory = null;
            }
            return null;
        }


        /// <summary>
        /// Get Order Count by Broker
        /// </summary>
        /// <param name="objOrderHistory"></param>
        /// <returns></returns>
     //   [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/OrderHistory/GetOrderCountByBroker")]
        public HttpResponseMessage GetOrderCountByBroker(Order objOrderHistory)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;
 
                orderHistory = new OrderHistoryRepository();
                StringContent sc = new StringContent(orderHistory.GetOrderCountByBroker(objOrderHistory.FromDate, objOrderHistory.ToDate, objOrderHistory.GoyaCompanyId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
               
            }
            catch (Exception ex)
            {
                log.Error("-> api/OrderHistory/GetOrderCountByBroker", ex);
            }
            finally
            {
                orderHistory = null;
            }
            return null;
        }



        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/OrderHistory/SendOrderByEmail")]
        public HttpResponseMessage SendOrderByEmail(Order objOrderHistory)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;
                              
                orderHistory = new OrderHistoryRepository();
                StringContent sc = new StringContent(orderHistory.SendOrderByEmail(objOrderHistory.BrokerId,objOrderHistory.EOR, objOrderHistory.EmailTo, objOrderHistory.EmailBody, objOrderHistory.AttachmentPath, objOrderHistory.FileType));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
                //}
                //else
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");

            }
            catch (Exception ex)
            {
                log.Error("-> api/OrderHistory/GetOrderCountByBroker", ex);
            }
            finally
            {
                orderHistory = null;
            }
            return null;
        }


        // Get Order History From AS400 db
       // [Authorize(Roles = "Broker")]
       // [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/OrderHistory/GetOrderHistory")]
        public HttpResponseMessage GetOrderHistoryDB2(Order objOrderHistory)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                orderHistory = new OrderHistoryRepository();
                StringContent sc = new StringContent(orderHistory.GetOrderHistoryDB2(objOrderHistory));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/OrderHistory/GetOrderHistoryDB2", ex);
            }
            finally
            {
                orderHistory = null;
            }
            return null;
        }

        //[Authorize(Roles = CustomRoles.BrkRole)]
        //[HttpPost]
        //[Route("api/OrderHistory/GetSubmittedOrder")]
        //public HttpResponseMessage GetSubmittedOrder(Order objOrderHistory)
        //{
        //    try
        //    {
        //        orderHistory = new OrderHistoryRepository();
        //        StringContent sc = new StringContent(orderHistory.GetSubmittedOrderData(objOrderHistory.BrokerId, objOrderHistory.CompanyId));
        //        sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
        //        HttpResponseMessage data = new HttpResponseMessage();
        //        data.Content = sc;
        //        return data;

        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("-> api/OrderHistory/GetSubmittedOrder", ex);
        //    }
        //    finally
        //    {
        //        orderHistory = null;
        //    }
        //    return null;
        //}

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/OrderHistory/GetSavedCartOrder")]
        public HttpResponseMessage GetSavedCartOrder(Order objOrderHistory)
        {
            // UserController declaration for Token Varification 
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                orderHistory = new OrderHistoryRepository();
                StringContent sc = new StringContent(orderHistory.GetSavedCartOrderData(objOrderHistory.BrokerId, objOrderHistory.CompanyId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/OrderHistory/GetSavedCartOrder", ex);
            }
            finally
            {
                orderHistory = null;
            }
            return null;
        }

        //public DbSet<OrderHistory> OrderHistory { get; set; }
        //[Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/OrderHistory/DeleteCartOrder")]
        public HttpResponseMessage DeleteCartOrder(OrderItemDetailsViewModel objOrderHistory)
        { 
            // UserController declaration for Token Varification 
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty, ret = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;
                //dynamic cartDetails = objOrderHistory;
                //JArray itemDetailsJson = cartDetails.OrderHistory;

                string Salesman_ID = objOrderHistory.SalesmanId;
                var svOrd = objOrderHistory.itemDetails;
                foreach (var item in svOrd)
                {
                    orderHistory = new OrderHistoryRepository();
                    ret = orderHistory.deleteOpenOrderData(item.CustomerID, item.PONumber, Salesman_ID, item.CompanyID);
                    //lstSavedDetails.Add(item.ToObject<OrderHistory>());
                }  

                orderHistory = new OrderHistoryRepository();
                StringContent sc = new StringContent(ret);
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/OrderHistory/GetSavedCartOrder", ex);
            }
            finally
            {
                orderHistory = null;
            }
            return null;
        }

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/OrderHistory/GetSubmittedOrderDetails")]
        public HttpResponseMessage GetSubmittedOrderDetails(Order objOrderHistory)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                orderHistory = new OrderHistoryRepository();
                StringContent sc = new StringContent(orderHistory.GetSubmittedOrderDetailData(objOrderHistory.OrderNumber, objOrderHistory.CompanyId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/OrderHistory/GetSubmittedOrderDetails", ex);
            }
            finally
            {
                orderHistory = null;
            }
            return null;
        }

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/OrderHistory/GetSavedCartOrderDetails")]
        public HttpResponseMessage GetSavedCartOrderDetails(Order objOrderHistory)
        {
            // UserController declaration for Token Varification 
            UserController userController = null;
            try
            {
                userController = new UserController();
                String token = String.Empty;
                var request = Request;
                var headers = request.Headers;
                String userId = String.Empty;

                orderHistory = new OrderHistoryRepository();
                StringContent sc = new StringContent(orderHistory.GetSavedCartOrderDetailData(objOrderHistory.PONumber, objOrderHistory.CompanyId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/OrderHistory/GetSubmittedOrderDetails", ex);
            }
            finally
            {
                orderHistory = null;
            }
            return null;
        }


        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/OrderHistory/GetOrderByStatus")]
        public HttpResponseMessage GetOrderByStatus(Order objOrderHistory)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                orderHistory = new OrderHistoryRepository();
                StringContent sc = new StringContent(orderHistory.GetOrderByStatusData(objOrderHistory.BrokerId, objOrderHistory.CompanyId, objOrderHistory.OrderMode));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/OrderHistory/GetOrderByStatus", ex);
            }
            finally
            {
                orderHistory = null;
            }
            return null;
        }

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/OrderHistory/GetOrderDetailByStatus")]
        public HttpResponseMessage GetOrderDetailByStatus(Order objOrderHistory)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                orderHistory = new OrderHistoryRepository();
                StringContent sc = new StringContent(orderHistory.GetOrderDetailByStatusData(objOrderHistory.OrderNumber, objOrderHistory.CompanyId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/OrderHistory/GetOrderDetailByStatus", ex);
            }
            finally
            {
                orderHistory = null;
            }
            return null;
        }

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/OrderHistory/GetHighCreditCustomer")]
        public HttpResponseMessage GetHighCreditCustomer(Order objOrderHistory)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                orderHistory = new OrderHistoryRepository();
                StringContent sc = new StringContent(orderHistory.GetCreditCustomerData(objOrderHistory.BrokerId, Convert.ToString(objOrderHistory.CompanyId), objOrderHistory.Action));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/OrderHistory/GetHighCreditCustomer", ex);
            }
            finally
            {
                orderHistory = null;
            }
            return null;
        }

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/OrderHistory/GetTopSellingCustomer")]
        public HttpResponseMessage GetTopSellingCustomer(Order objOrderHistory)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                orderHistory = new OrderHistoryRepository();
                StringContent sc = new StringContent(orderHistory.GetTopSellingCustomerData(objOrderHistory.BrokerId, Convert.ToString(objOrderHistory.CompanyId)));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/OrderHistory/GetTopSellingCustomer", ex);
            }
            finally
            {
                orderHistory = null;
            }
            return null;
        }

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/OrderHistory/GetOrderHeaderByStatus")]
        public HttpResponseMessage GetOrderHeader(Order objOrderHistory)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                orderHistory = new OrderHistoryRepository();
                StringContent sc = new StringContent(orderHistory.GetOrderHeaderDataByStatus(objOrderHistory.BrokerId, Convert.ToString(objOrderHistory.CompanyId), objOrderHistory.Status));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/OrderHistory/GetOrderHeaderDataByStatus", ex);
            }
            finally
            {
                orderHistory = null;
            }
            return null;
        }

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/OrderHistory/GetCustomerToVisit")]
        public HttpResponseMessage GetCustomerToVisit(OrderParam objOrderHistory)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                orderHistory = new OrderHistoryRepository();
                StringContent sc = new StringContent(orderHistory.GetCustomerToVisitData(objOrderHistory.CompanyID, objOrderHistory.BrokerId, objOrderHistory.BillingCO));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/OrderHistory/GetCustomerToVisit", ex);
            }
            finally
            {
                orderHistory = null;
            }
            return null;
        }

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/OrderHistory/GetNotification")]
        public HttpResponseMessage GetNotification(Order objOrderHistory)
        {
            try
            {
                orderHistory = new OrderHistoryRepository();
                StringContent sc = new StringContent(orderHistory.GetNotificationData(objOrderHistory.BrokerId, objOrderHistory.CompanyId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/OrderHistory/GetNotification", ex);
            }
            finally
            {
                orderHistory = null;
            }
            return null;
        }

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/OrderHistory/UpdateNotificationGetOrderDetails")]
        public HttpResponseMessage UpdateNotificationGetOrderDetails(Order objOrderHistory)
        {
            try
            {
                orderHistory = new OrderHistoryRepository();
                StringContent sc = new StringContent(orderHistory.UpdateNotificationGetDetailData(objOrderHistory.OrderNumber, Convert.ToString(objOrderHistory.CompanyId)));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/OrderHistory/UpdateNotificationGetOrderDetails", ex);
            }
            finally
            {
                orderHistory = null;
            }
            return null;
        }

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/OrderHistory/DeleteNotification")]
        public HttpResponseMessage DeleteNotification(NotificationModel objNotificationModel)
        {
            try
            {
                orderHistory = new OrderHistoryRepository();
                StringContent sc = new StringContent(orderHistory.DeleteNotificationData(objNotificationModel));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/OrderHistory/DeleteNotification", ex);
            }
            finally
            {
                orderHistory = null;
            }
            return null;
        }

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/OrderHistory/GetAllOrders")]
        public HttpResponseMessage GetAllOrders(Order objOrderHistory)
        {
            try
            {
                orderHistory = new OrderHistoryRepository();
                StringContent sc = new StringContent(orderHistory.GetSubmittedOrderData(objOrderHistory.BrokerId, objOrderHistory.CompanyId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/OrderHistory/GetAllOrders", ex);
            }
            finally
            {
                orderHistory = null;
            }
            return null;
        }

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/OrderHistory/GetErrorOrder")]
        public HttpResponseMessage GetErrorOrder(Order objOrderHistory)
        {
            try
            {
                orderHistory = new OrderHistoryRepository();
                StringContent sc = new StringContent(orderHistory.GetErrorOrderData(objOrderHistory.BrokerId, Convert.ToString(objOrderHistory.CompanyId)));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/OrderHistory/GetErrorOrder", ex);
            }
            finally
            {
                orderHistory = null;
            }
            return null;
        }

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/OrderHistory/GetErrorOrderDetails")]
        public HttpResponseMessage GetErrorOrderDetails(Order objOrderHistory)
        {
            try
            {
                orderHistory = new OrderHistoryRepository();
                StringContent sc = new StringContent(orderHistory.GetErrorOrderDetails(Convert.ToString(objOrderHistory.CompanyId), objOrderHistory.OrderNumber));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/OrderHistory/GetErrorOrderDetails", ex);
            }
            finally
            {
                orderHistory = null;
            }
            return null;
        }

    }
}
