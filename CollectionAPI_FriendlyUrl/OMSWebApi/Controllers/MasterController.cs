﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Filters;
using OMSWebApi.Models;
using OMSWebApi.Repository;
using System.Data;

namespace OMSWebApi.Controllers
{
    public class MasterController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        MasterRepository masterRepository = null;
        int x = Globals.GlobalInt;
        Int32 compId = Globals.SetGlobalInt(0);
        String ClientName = ApiConstant.getClientName.ToString();
        /// <summary>
        /// Get Product List
        /// </summary>
        /// <param name="objOrderTemplate"></param>
        /// <returns></returns>
        //  [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Master/getProductData")]
        public HttpResponseMessage getProductData(master objMstr)
        {
            try
            {
                masterRepository = new MasterRepository();
                StringContent sc = new StringContent(masterRepository.getItemList(objMstr.CompanyId, objMstr.LanguageId, objMstr.BillingId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/Master/getProductData", ex);
            }
            finally
            {
                masterRepository = null;
            }
            return null;
        }


        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Master/getCategory")]
        public HttpResponseMessage getCategory(master objMstr)
        {
            try
            {
                masterRepository = new MasterRepository();

                StringContent sc = new StringContent(masterRepository.getCategoryList(objMstr.CompanyId, objMstr.LanguageId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/Master/getCategory", ex);
            }
            finally
            {
                masterRepository = null;
            }
            return null;
        }


        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Master/getSubCategory")]
        public HttpResponseMessage getSubCategory(master objMstr)
        {
            try
            {
                masterRepository = new MasterRepository();

                StringContent sc = new StringContent(masterRepository.getSubCategoryList(objMstr.CompanyId, objMstr.LanguageId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/Master/getSubCategory", ex);
            }
            finally
            {
                masterRepository = null;
            }
            return null;
        }


        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Master/getCompanyByID")]
        public HttpResponseMessage getCompanyByID(master objMstr)
        {
            try
            { 
                    
                masterRepository = new MasterRepository();
                StringContent sc = new StringContent(masterRepository.getCompanyByIDData(objMstr.CompanyId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/Master/getCompanyByID", ex);
            }
            finally
            {
                masterRepository = null;
            }
            return null;
        }


        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Master/BrokerList")]
        public HttpResponseMessage BrokerList(master objMstr)
        {
            try
            {
                //switch (ClientName)
                //{
                    //case "Goya":
                    //    {
                    //        masterRepository = new MasterRepository();
                    //        StringContent sc = new StringContent(masterRepository.GetBrokerList_DB2(objMstr.CompanyId));
                    //        sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    //        HttpResponseMessage data = new HttpResponseMessage();
                    //        data.Content = sc;

                    //        return data;
                    //    }
                    //default:
                    //    {
                masterRepository = new MasterRepository();
                StringContent sc = new StringContent(masterRepository.getBrokerListByComp(objMstr.CompanyId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
                    //}
                //}
            }
            catch (Exception ex)
            {
                log.Error("-> api/Master/BrokerList", ex);
            }
            finally
            {
                masterRepository = null;
            }
            return null;
        }


        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Master/CustomerList")]
        public HttpResponseMessage CustomerList(Order objOrder)
        {
            try
            {
                masterRepository = new MasterRepository();
                //switch (ClientName)
                //{
                //    case "Goya":
                //        {
                //            StringContent sc = new StringContent(masterRepository.GetCustomersDetailList_DB2(objOrder.BrokerId));
                //            sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                //            HttpResponseMessage data = new HttpResponseMessage();
                //            data.Content = sc;
                //            return data;

                //            //masterRepository = new MasterRepository();
                //            //return masterRepository.GetCustomersDetailList_DB2(objOrder.BrokerId).AsEnumerable();
                //        }
                //    default:
                //        {
                StringContent sc = new StringContent(masterRepository.getCustomerBySalesmanId(Convert.ToString(objOrder.CompanyId), objOrder.BrokerId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
                //        }
                //}
            }
            catch (Exception ex)
            {
                log.Error("-> api/Customer/CustomerList ", ex);
                throw ex;
            }
            finally
            {
                masterRepository = null;
            }
               return null;
        }


        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Master/WarehouseList")]
        public HttpResponseMessage WarehouseList(master objMstr)
        {
            try
            {
                masterRepository = new MasterRepository();
                StringContent sc = new StringContent(masterRepository.getWarehouseByComp(objMstr.CompanyId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/Master/WarehouseList", ex);
            }
            finally
            {
                masterRepository = null;
            }
            return null;
        }


        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Master/BrandVendorGrpList")]
        public HttpResponseMessage BrandVendorGrpList(master objMstr)
        {
            DataSet dsData = new DataSet();
            try
            {
                masterRepository = new MasterRepository();

                DataTable dtBrandVendor = new DataTable();
                dtBrandVendor = masterRepository.getVendorListData(objMstr.CompanyId);
                dsData.Tables.Add(dtBrandVendor);

                DataTable dtVendorGrp = new DataTable();
                dtVendorGrp = masterRepository.getVendorGroupListData(objMstr.CompanyId);
                dsData.Tables.Add(dtVendorGrp);

                //StringContent sc = new StringContent(masterRepository.getVendorListData(objMstr.CompanyId));
                //sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                //HttpResponseMessage data = new HttpResponseMessage();
                //data.Content = sc;
                //return data;

                return Request.CreateResponse(HttpStatusCode.OK, dsData);
            }
            catch (Exception ex)
            {
                log.Error("-> api/Master/BrandVendorGrpList", ex);
            }
            finally
            {
                masterRepository = null;
            }
            return null;
        }
        

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Master/CountryList")]
        public HttpResponseMessage CountryList(CustProfile objCustProf)
        {
            try
            {
                masterRepository = new MasterRepository();

                StringContent sc = new StringContent(masterRepository.getCountryData(objCustProf.CompanyID));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/Master/CountryList", ex);
            }
            finally
            {
                masterRepository = null;
            }
            return null;
        }


        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Master/BrandList")]
        public HttpResponseMessage BrandList()
        {
            try
            {
                masterRepository = new MasterRepository();

                StringContent sc = new StringContent(masterRepository.getBrandData());
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/Master/BrandList", ex);
            }
            finally
            {
                masterRepository = null;
            }
            return null;
        }


        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Master/OrderStatusList")]
        public HttpResponseMessage OrderStatusList(Item objItem)
        {
            try
            {
                masterRepository = new MasterRepository();
                StringContent sc = new StringContent(masterRepository.getOrderStatusData());
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/Master/OrderStatusList", ex);
            }
            finally
            {
                masterRepository = null;
            }
            return null;
        }


        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Master/StockList")]
        public HttpResponseMessage StockList(master objMstr)
        {
            try
            {
                masterRepository = new MasterRepository();
                StringContent sc = new StringContent(masterRepository.getStockDataByComp(objMstr.CompanyId, objMstr.LanguageId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/Master/StockList", ex);
            }
            finally
            {
                masterRepository = null;
            }
            return null;
        }


        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Master/ProductPromoPrice")]
        public HttpResponseMessage ProductPromoPrice(master objMstr)
        {
            try
            {
                masterRepository = new MasterRepository();

                StringContent sc = new StringContent(masterRepository.getProductPromoPriceData(objMstr.CompanyId, objMstr.SalesmanId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/Master/ProductPromoPrice", ex);
            }
            finally
            {
                masterRepository = null;
            }
            return null;
        }


        //[Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Master/SubmitOrderByChatMsg")]
        public HttpResponseMessage SubmitOrderByChatMsg(OrderViewModel objOrderChat)
        {
            // UserController declaration for Token Varification 
            UserController userController = null;
            try
            {
                //switch (ClientName)
                //{
                //    case "Goya":
                //        {
                userController = new UserController();
                String token = String.Empty, ret = String.Empty;
                var request = Request;
                var headers = request.Headers;
                String userId = String.Empty;

                masterRepository = new MasterRepository();
                StringContent sc = new StringContent(masterRepository.addOrderItemData(objOrderChat));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
                            //string Salesman_ID = objOrderChat.SalesmanID;
                            //var svOrd = objOrderChat.orderitemDetails;
                            //foreach (var item in svOrd)
                            //{
                            //    masterRepository = new MasterRepository();
                            //    ret = masterRepository.deleteOpenOrderData(item.CustomerID, item.PONumber, Salesman_ID, item.CompanyID);
                            //    //lstSavedDetails.Add(item.ToObject<OrderHistory>());
                            //}
                    //    }
                    //case "Flsm":
                    //    {
                    //        userController = new UserController();
                    //        String token = String.Empty, ret = String.Empty;
                    //        var request = Request;
                    //        var headers = request.Headers;
                    //        String userId = String.Empty;

                    //        masterRepository = new MasterRepository();
                    //        StringContent sc = new StringContent(masterRepository.addOrderItemFData(objOrderChat));
                    //        sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    //        HttpResponseMessage data = new HttpResponseMessage();
                    //        data.Content = sc;
                    //        return data;
                    //    }
                    //}
            }
            catch (Exception ex)
            {
                log.Error("-> api/OrderHistory/SubmitOrderByChatMsg", ex);
            }
            finally
            {
                masterRepository = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/Master/SubmitBypassOrderByAnyCustomer")]
        public HttpResponseMessage SubmitBypassOrderByAnyCustomer(OrderViewModel objBypassOrder)
        {
            // UserController declaration for Token Varification 
            UserController userController = null;
            try
            {
                userController = new UserController();
                String token = String.Empty, ret = String.Empty;
                var request = Request;
                var headers = request.Headers;
                String userId = String.Empty;

                masterRepository = new MasterRepository();
                StringContent sc = new StringContent(masterRepository.submiBypasstOrderData(objBypassOrder));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/Master/SubmitBypassOrderByAnyCustomer", ex);
            }
            finally
            {
                masterRepository = null;
            }
            return null;
        }

        /// <summary>
        /// Get Product List
        /// </summary>
        /// <param name="objOrderTemplate"></param>
        /// <returns></returns>
        //  [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Master/getPriceData")]
        public HttpResponseMessage getPriceData(master objMstr)
        {
            try
            {
                masterRepository = new MasterRepository();
                StringContent sc = new StringContent(masterRepository.getPriceList(objMstr.CompanyId, objMstr.SalesmanId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/Master/getPriceData", ex);
            }
            finally
            {
                masterRepository = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/Master/getPrice")]
        public HttpResponseMessage getPrice(prices objPrice)
        {
            try
            {
                masterRepository = new MasterRepository();
                StringContent sc = new StringContent(masterRepository.getPrice_Data(objPrice.CompanyID, objPrice.SalesmanId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/Master/getPrice", ex);
            }
            finally
            {
                masterRepository = null;
            }
            return null;
        }

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Master/GetUpdateBrokerDetails")]
        public HttpResponseMessage GetUpdateBrokerDetails(BrokerDetails objBrokerData)
        {
            try
            {
                masterRepository = new MasterRepository();
                StringContent sc = new StringContent(masterRepository.GetUpdateBrokerData(objBrokerData));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/Master/GetUpdateBrokerDetails", ex);
            }
            finally
            {
                masterRepository = null;
            }
            return null;
        }

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Master/SaveActivityLog")]
        public HttpResponseMessage SaveActivityLog(ActivityLogViewModel objActivityLog)
        {
            try
            {
                masterRepository = new MasterRepository();
                StringContent sc = new StringContent(masterRepository.saveActivityLogData(objActivityLog));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/Master/SaveActivityLog", ex);
            }
            finally
            {
                masterRepository = null;
            }
            return null;
        }

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Master/GetActivityLog")]
        public HttpResponseMessage GetActivityLog(ActivityLog objActivityData)
        {
            try
            {
                masterRepository = new MasterRepository();
                StringContent sc = new StringContent(masterRepository.GetActivityLogData(objActivityData));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/Master/GetActivityLog", ex);
            }
            finally
            {
                masterRepository = null;
            }
            return null;
        }
        
    }
}
