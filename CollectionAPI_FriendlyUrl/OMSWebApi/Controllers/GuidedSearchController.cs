﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http.Cors;
using System.Web.Http.Filters;
using OMSWebApi.Models;
using OMSWebApi.Repository;
using Newtonsoft.Json;

namespace OMSWebApi.Controllers
{
    public class GuidedSearchController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        GuidedSearchRepository guidedSearch;

        //Get Warehouse List
        // [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/GuidedSearch/GetWarehouseList")]
        public HttpResponseMessage GetWarehouseList()
        {
            guidedSearch = new GuidedSearchRepository();
            try
            {
                log.Debug(" Method: GetWarehouseList() [api/GuidedSearch/GetWarehouseList] -> Token Validation Success");

                StringContent sc = new StringContent(guidedSearch.GetWarehouseList());
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error(" Method: GetWarehouseList() [api/GuidedSearch/GetWarehouseList]" + ex);
                throw ex;
            }
            finally
            {
                guidedSearch = null;
            }
        }


        // Get Item Category List
        // [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/GuidedSearch/GetCategoryList")]
        public HttpResponseMessage GetCategoryList()
        {
            guidedSearch = new GuidedSearchRepository();
            try
            {
                log.Debug(" Method: GetCategoryList() [api/GuidedSearch/GetCategoryList] -> Token Validation Success");

                StringContent sc = new StringContent(guidedSearch.GetCategoryList());
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error(" Method: GetCategoryList() [api/GuidedSearch/GetCategoryList]" + ex);
                throw ex;
            }
            finally
            {
                guidedSearch = null;
            }
        }

        // Get Item Category List
        // [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/GuidedSearch/GetSubCategoryList")]
        public HttpResponseMessage GetSubCategoryList()
        {
            guidedSearch = new GuidedSearchRepository();
            try
            {
                log.Debug(" Method: GetCategoryList() [api/GuidedSearch/GetCategoryList] -> Token Validation Success");

                StringContent sc = new StringContent(guidedSearch.GetSubCategoryList());
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error(" Method: GetCategoryList() [api/GuidedSearch/GetSubCategoryList]" + ex);
                throw ex;
            }
            finally
            {
                guidedSearch = null;
            }
        }

        // Get Vendor List
        //[Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/GuidedSearch/GetVendorList")]
        public HttpResponseMessage GetVendorList()
        {
            guidedSearch = new GuidedSearchRepository();
            try
            {
                log.Debug(" Method: GetVendorList() [api/GuidedSearch/GetVendorList] -> Token Validation Success");
                StringContent sc = new StringContent(guidedSearch.GetVendorList());
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error(" Method: GetVendorList() [api/GuidedSearch/GetVendorList]" + ex);
                throw ex;
            }
            finally
            {
                guidedSearch = null;
            }
        }

        // Get Vendor Group by Vendoer ID
        //[Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/GuidedSearch/GetVendorGroup")]
        public HttpResponseMessage GetVendorGroup(GuidedSearch objGuidedSearch)
        {
            guidedSearch = new GuidedSearchRepository();
            try
            {
                log.Debug(" Method: GetVendorGroup(string vendorId) [api/GuidedSearch/GetVendorGroup(string vendorId)] -> Token Validation Success");

                StringContent sc = new StringContent(guidedSearch.GetVendorGroup(objGuidedSearch.vendorId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error(" Method: GetVendorGroup(string vendorId) [api/GuidedSearch/GetVendorGroup/{vendorId}]" + ex);
                throw ex;
            }
            finally
            {
                guidedSearch = null;
            }
        }

        // Get Category List by Vendor ID
        //[Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/GuidedSearch/GetCategoryByVendorChanged")]
        public HttpResponseMessage GetCategoryByVendorChanged(GuidedSearch objGuidedSearch)
        {
            guidedSearch = new GuidedSearchRepository();
            try
            {
                log.Debug(" Method: GetCategoryByVendorChanged(string vendorId) [api/GuidedSearch/GetCategoryByVendorChanged(string vendorId)] -> Token Validation Success");

                StringContent sc = new StringContent(guidedSearch.GetCategoryByVendorChanged(objGuidedSearch.vendorId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error(" Method: GetCategoryByVendorChanged(string vendorId) [api/GuidedSearch/GetCategoryByVendorChanged/{vendorId}]" + ex);
                throw ex;
            }
            finally
            {
                guidedSearch = null;
            }
        }

        // Get Vendor list by Category
        //[Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/GuidedSearch/GetVendorByCategoryChanged")]
        public HttpResponseMessage GetVendorByCategoryChanged(GuidedSearch objGuidedSearch)
        {
            guidedSearch = new GuidedSearchRepository();
            try
            {
                log.Debug(" Method: GetVendorByCategoryChanged(string categoryId) [api/GuidedSearch/GetVendorByCategoryChanged(string categoryId)] -> Token Validation Success");

                StringContent sc = new StringContent(guidedSearch.GetVendorByCategoryChanged(objGuidedSearch.categoryId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error(" Method: GetVendorByCategoryChanged(string categoryId) [api/GuidedSearch/GetVendorByCategoryChanged/{categoryId}]" + ex);
                throw ex;
            }
            finally
            {
                guidedSearch = null;
            }
        }


        // Get Sub Category By Category 
        //[Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/GuidedSearch/GetSubCategoryByCategoryId")]
        public HttpResponseMessage GetSubCategoryByCategoryId(GuidedSearch objGuidedSearch)
        {
            guidedSearch = new GuidedSearchRepository();
            try
            {
                log.Debug(" Method: GetVendorByCategoryChanged(string categoryId) [api/GuidedSearch/GetSubCategoryByCategoryId(string categoryId)] -> Token Validation Success");

                StringContent sc = new StringContent(guidedSearch.GetSubCategoryByCategoryId(objGuidedSearch.categoryId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error(" Method: GetSubCategoryByCategoryId(string categoryId) [api/GuidedSearch/GetSubCategoryByCategoryId/{categoryId}]" + ex);
                throw ex;
            }
            finally
            {
                guidedSearch = null;
            }
        }


        // Get Vendor Group by  Sub Category Id
        //[Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/GuidedSearch/GetVendorGroupBySubCategoryId")]
        public HttpResponseMessage GetVendorGroupBySubCategoryId(GuidedSearch objGuidedSearch)
        {
            guidedSearch = new GuidedSearchRepository();
            try
            {
                log.Debug(" Method: GetVendorByCategoryChanged(string categoryId) [api/GuidedSearch/GetVendorGroupBySubCategoryId(string categoryId)] -> Token Validation Success");

                StringContent sc = new StringContent(guidedSearch.GetVendorGroupBySubCategoryId(objGuidedSearch.vendorId, objGuidedSearch.categoryId,objGuidedSearch.subCategoryId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error(" Method: GetSubCategoryByCategoryId(string categoryId) [api/GuidedSearch/GetVendorGroupBySubCategoryId/{categoryId}]" + ex);
                throw ex;
            }
            finally
            {
                guidedSearch = null;
            }
        }

        // Get Items By Search
        //[Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/GuidedSearch/SearchItems")]
        public HttpResponseMessage SearchItems(GuidedSearch objGuidedSearch)
        {
            guidedSearch = new GuidedSearchRepository();
            try
            {
                log.Debug(" Method: SearchItems [api/GuidedSearch/SearchItems ] -> Token Validation Success");

                StringContent sc = new StringContent(guidedSearch.SearchItems(objGuidedSearch.vendorId, objGuidedSearch.categoryId, objGuidedSearch.customerId, objGuidedSearch.warehouseId, objGuidedSearch.groupId, objGuidedSearch.searchText, objGuidedSearch.mfgUpc, objGuidedSearch.searchFor, objGuidedSearch.subCategoryId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error(" Method: GetVendorByCategoryChanged(string categoryId) [api/GuidedSearch/SearchItems" + ex);
                throw ex;
            }
            finally
            {
                guidedSearch = null;
            }
        }

        // Add to Cart
        //[Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/GuidedSearch/AddToCart")]
        public HttpResponseMessage AddToCart(GuidedSearch objGuidedSearch)
        {
            guidedSearch = new GuidedSearchRepository();
            HttpResponseMessage data = new HttpResponseMessage();
            var CartResult = JsonConvert.DeserializeObject<List<GuidedSearch>>(objGuidedSearch.CartItems);

            try
            {
                log.Debug(" Method: SearchItems [api/GuidedSearch/AddToCart] -> Token Validation Success");

                // Parse User Activity Data to Store in DB0
                if (CartResult != null)
                {
                    for (int i = 0; i < CartResult.Count; i++)
                    {
                        StringContent sc = new StringContent(guidedSearch.AddToCart(CartResult[i].customerId, CartResult[i].salesmanId, CartResult[i].companyId, CartResult[i].productId, CartResult[i].UPC1, CartResult[i].warehouseId, CartResult[i].lineNo, CartResult[i].OrdQty, CartResult[i].ListPrice, CartResult[i].promoCode, CartResult[i].isRestricted, CartResult[i].retailPrice, CartResult[i].promoAmount, CartResult[i].isPromoItem, CartResult[i].promoMinQuantity, CartResult[i].poNumber, CartResult[i].clientPONumber, CartResult[i].comments, CartResult[i].shippingDate, CartResult[i].orderDate, CartResult[i].cancelDate, CartResult[i].orderMode));
                        sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        data.Content = sc;
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                log.Error(" Method: GetVendorByCategoryChanged(string categoryId) [api/GuidedSearch/SearchItems" + ex);
                throw ex;
            }
            finally
            {
                guidedSearch = null;
            }
        }



        // Get Items By Search
        //[Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/GuidedSearch/GetOrderDetailsByOrderId")]
        public HttpResponseMessage GetOrderDetailsByOrderId(GuidedSearch objGuidedSearch)
        {
            guidedSearch = new GuidedSearchRepository();
            try
            {
                log.Debug(" Method: SearchItems [api/GuidedSearch/GetOrderDetailsByOrderId ] -> Token Validation Success");

                StringContent sc = new StringContent(guidedSearch.GetOrderDetailsByOrderId(objGuidedSearch.poNumber));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error(" Method: GetVendorByCategoryChanged(string categoryId) [api/GuidedSearch/GetOrderDetailsByOrderId" + ex);
                throw ex;
            }
            finally
            {
                guidedSearch = null;
            }
        }


        // Submit Order
        //[Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/GuidedSearch/SubmitOrder")]
        public HttpResponseMessage SubmitOrder(GuidedSearch objGuidedSearch)
        {
            guidedSearch = new GuidedSearchRepository();
            try
            {
                log.Debug(" Method: SearchItems [api/GuidedSearch/SubmitOrder ] -> Token Validation Success");
  
                StringContent sc = new StringContent(guidedSearch.SubmitOrder(objGuidedSearch.poNumber,objGuidedSearch.clientPONumber,objGuidedSearch.salesmanId,objGuidedSearch.customerId,objGuidedSearch.orderDate,objGuidedSearch.shippingDate,objGuidedSearch.comments,objGuidedSearch.cancelDate,objGuidedSearch.SBTag));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error(" Method: GetVendorByCategoryChanged(string categoryId) [api/GuidedSearch/GetOrderDetailsByOrderId" + ex);
                throw ex;
            }
            finally
            {
                guidedSearch = null;
            }
        }

        // Update Item Qty by Order Id
        //[Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/GuidedSearch/UpdateItemQty")]
        public HttpResponseMessage UpdateItemQty(GuidedSearch objGuidedSearch)
        {
            guidedSearch = new GuidedSearchRepository();
            try
            {
                log.Debug(" Method: SearchItems [api/GuidedSearch/UpdateItemQty ] -> Token Validation Success");

                StringContent sc = new StringContent(guidedSearch.UpdateItemQty(objGuidedSearch.poNumber, objGuidedSearch.productId, objGuidedSearch.OrdQty));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error(" Method: GetVendorByCategoryChanged(string categoryId) [api/GuidedSearch/UpdateItemQty" + ex);
                throw ex;
            }
            finally
            {
                guidedSearch = null;
            }
        }

        // Delete Item by Order Id
        //[Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/GuidedSearch/DeleteItem")]
        public HttpResponseMessage DeleteItem(GuidedSearch objGuidedSearch)
        {
            guidedSearch = new GuidedSearchRepository();
            try
            {
                log.Debug(" Method: SearchItems [api/GuidedSearch/UpdateItemQty ] -> Token Validation Success");

                StringContent sc = new StringContent(guidedSearch.DeleteItem(objGuidedSearch.poNumber, objGuidedSearch.productId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error(" Method: GetVendorByCategoryChanged(string categoryId) [api/GuidedSearch/DeleteItem" + ex);
                throw ex;
            }
            finally
            {
                guidedSearch = null;
            }
        }



        // Upload Order file
        //[Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/GuidedSearch/UploadOrderFile")]
        public HttpResponseMessage UploadOrderFile(GuidedSearch objGuidedSearch)
        {
            guidedSearch = new GuidedSearchRepository();
            try
            {
                log.Debug(" Method: SearchItems [api/GuidedSearch/UploadOrderFile ] -> Token Validation Success");

                StringContent sc = new StringContent(guidedSearch.UploadOrderFile(objGuidedSearch.fileName, objGuidedSearch.fileData, objGuidedSearch.fileType, objGuidedSearch.sentDate,objGuidedSearch.customerId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error(" Method: GetVendorByCategoryChanged(string categoryId) [api/GuidedSearch/UploadOrderFile" + ex);
                throw ex;
            }
            finally
            {
                guidedSearch = null;
            }
        }


        // Get Order History
        //[Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/GuidedSearch/OrderHistory")]
        public HttpResponseMessage OrderHistory(GuidedSearch objGuidedSearch)
        {
            guidedSearch = new GuidedSearchRepository();
            try
            {
                log.Debug(" Method: SearchItems [api/GuidedSearch/OrderHistory ] -> Token Validation Success");

                StringContent sc = new StringContent(guidedSearch.OrderHistory(objGuidedSearch.companyId, objGuidedSearch.salesmanId, objGuidedSearch.customerId, objGuidedSearch.fromDate,objGuidedSearch.toDate,objGuidedSearch.status));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error(" Method: GetVendorByCategoryChanged(string categoryId) [api/GuidedSearch/OrderHistory" + ex);
                throw ex;
            }
            finally
            {
                guidedSearch = null;
            }
        }


        // Delete Order by order id
        //[Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/GuidedSearch/DeleteOrder")]
        public HttpResponseMessage DeleteOrder(GuidedSearch objGuidedSearch)
        {
            guidedSearch = new GuidedSearchRepository();
            try
            {
                log.Debug(" Method: SearchItems [api/GuidedSearch/DeleteOrder ] -> Token Validation Success");

                StringContent sc = new StringContent(guidedSearch.DeleteOrder(objGuidedSearch.poNumber, objGuidedSearch.salesmanId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error(" Method: [api/GuidedSearch/DeleteOrder" + ex);
                throw ex;
            }
            finally
            {
                guidedSearch = null;
            }
        }


        // Add One Item to Order
        //[Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/GuidedSearch/AddItemToOrder")]
        public HttpResponseMessage AddItemToOrder(GuidedSearch objGuidedSearch)
        {
            guidedSearch = new GuidedSearchRepository();
            try
            {
                log.Debug(" Method: SearchItems [api/GuidedSearch/AddItemToOrder ] -> Token Validation Success");

                StringContent sc = new StringContent(guidedSearch.AddItemToOrder(objGuidedSearch.customerId, objGuidedSearch.salesmanId, objGuidedSearch.companyId, objGuidedSearch.productId, objGuidedSearch.OrdQty, objGuidedSearch.warehouseId, objGuidedSearch.poNumber, objGuidedSearch.clientPONumber, objGuidedSearch.comments));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error(" Method: [api/GuidedSearch/AddItemToOrder" + ex);
                throw ex;
            }
            finally
            {
                guidedSearch = null;
            }
        }


    }
}