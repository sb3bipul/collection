﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;
using OMSWebApi.Models;
using OMSWebApi.Repository;
using System.Web.Http.Cors;
using Newtonsoft.Json;


namespace OMSWebApi.Controllers
{
    public class LanguageController : ApiController
    {
        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        LanguageRepsitory languageRepository = null;

        /// <summary>
        /// Get Application data using Language
        /// </summary>
        /// <param name="objLanguage"></param>
        /// <returns></returns>
         
        [HttpPost]
        [Route("api/Language/GetApplicationDataByLanguage")]
        public HttpResponseMessage GetApplicationDataByLanguage(Language objLanguage)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try 
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;
 
                languageRepository = new LanguageRepsitory();
                StringContent sc = new StringContent(languageRepository.GetApplicationDataByLanguage(objLanguage.ApplicationName, objLanguage.ObjectName, objLanguage.LanguageName));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;              

            }
            catch (Exception ex)
            {
                log.Error("-> api/Language/GetApplicationData", ex);
            }
            finally
            {
                //orderHistory = null;
            }
            return null;
        }

    }
}
