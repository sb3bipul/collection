﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using OMSWebApi.Models;
using OMSWebApi.Repository;

namespace OMSWebApi.Controllers
{
    public class PayPanelController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        PayPanelRepository paypanel = null;

        /// <summary>
        /// insert pay panel temp data
        /// </summary>
        /// <param name="pay"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/paypanel/InsertPayPanelTempdata")]
        public HttpResponseMessage InsertPayPanelTempdata(PayPanelModel pay)
        {
            try
            {
                paypanel = new PayPanelRepository();
                StringContent sc = new StringContent(paypanel.InsertPayPanelTempdata(pay));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/paypanel/InsertPayPanelTempdata", ex);
            }
            finally
            {
                paypanel = null;
            }
            return null;
        }
    }
}
