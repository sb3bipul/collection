﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;
using OMSWebApi.Models;
using OMSWebApi.Repository;
using System.Data.Entity;

namespace OMSWebApi.Controllers
{
    public class LocationController : ApiController
    {
        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        LocationRepository locationHistory = null;

        [HttpPost]
        [Route("api/Location/GetLocationHistory")]
        public HttpResponseMessage GetLocationHistory(Location loctnHistory)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                locationHistory = new LocationRepository();
                StringContent sc = new StringContent(locationHistory.GetTrackLocationHistory(loctnHistory.UserId, loctnHistory.FromDate, loctnHistory.ToDate));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/Location/GetLocationHistory", ex);
            }
            finally
            {
                locationHistory = null;
            }
            return null;
        }

        [HttpPost]
        [Route("api/Location/InsertLocationToTrack")]
        public HttpResponseMessage InsertLocation(Location loctnInsert)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                locationHistory = new LocationRepository();
                StringContent sc = new StringContent(locationHistory.InsertTrackLocation(loctnInsert.UserId, loctnInsert.UserName, loctnInsert.CreatedDate, loctnInsert.Latitude, loctnInsert.Longitude, loctnInsert.LocationName));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/Location/InsertLocationToTrack", ex);
            }
            finally
            {
                locationHistory = null;
            }
            return null;
        }


    }
}