﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Filters;
using OMSWebApi.Models;
using OMSWebApi.Repository;

namespace OMSWebApi.Controllers
{
    public class ItemController : ApiController
    {
        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        ItemRepository itemRepository = null;

        /// <summary>
        /// Get Item List
        /// </summary>
        /// <param name="objOrderTemplate"></param>
        /// <returns></returns>
      //  [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Item/getItemList")]
        public HttpResponseMessage getItemList(Item objItem)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                // Check if the client request have header with Authorization key
                //if (headers.Contains("Authorization"))
                //{
                //    // Read token received from client's request in header
                //    token = headers.GetValues("Authorization").First();
                //    userId = "013030";//headers.GetValues("UserId").First();
                //}

               //  Check if token received from client is valid, if validated then send customer list otherwise unauthorized note.
                //if (userController.IsValidToken(token, userId))
                //{
                    itemRepository = new ItemRepository();
                    StringContent sc = new StringContent(itemRepository.getItemList(objItem.CompanyId,objItem.LanguageId,objItem.SearchText,objItem.CatalogId,objItem.WarehouseId));
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;

                    return data;
            //    }
             //   else
               //     return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");

            }
            catch (Exception ex)
            {
                log.Error("-> api/Item/getItemList", ex);
            }
            finally
            {
                itemRepository = null;
            }
            return null;
        }

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Item/addWishlistItem")]
        public HttpResponseMessage addWishlistItem(Item objItem)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                itemRepository = new ItemRepository();
                StringContent sc = new StringContent(itemRepository.addWishlistItemData(objItem.CompanyId, objItem.ProductCode));
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;

                    return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/Item/getItemList", ex);
            }
            finally
            {
                itemRepository = null;
            }
            return null;
        }

        /// <summary>
        /// Get last 6 months popular product list for all zone, can filter by zoneid, companyid, category name, vendor, itemcode and itemid/code
        /// </summary>
        /// <param name="itemModel"></param>
        /// <returns></returns>
        //[Authorize(Roles = CustomRoles.BrkRole)]
        [Authorize]
        [HttpPost]
        [Route("api/Item/GetPopularProductList")]
        public HttpResponseMessage GetPopularProduct(ItemModel itemModel)
        {
            try
            {
                itemRepository = new ItemRepository();
                StringContent sc = new StringContent(itemRepository.getPopularProcuct(itemModel));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/Item/getItems", ex);
            }
            finally
            {
                itemRepository = null;
            }
            return null;
        }
      
    }
}
