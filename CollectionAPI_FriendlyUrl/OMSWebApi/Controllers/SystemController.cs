﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;
using OMSWebApi.Models;
using OMSWebApi.Repository;
using System.Web.Http.Cors;
using Newtonsoft.Json;


namespace OMSWebApi.Controllers
{
    public class SystemController : ApiController
    {
        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        SystemStatsRepository systemRepository = null;


        /// <summary>
        /// Insert Stats for the Users activity
        /// </summary>
        /// <returns></returns>
  
       [AllowAnonymous]
       [HttpPost]
       [Route("api/UserStats/insertUserActivity")]
       public HttpResponseMessage insertUserActivity(SystemStats  systemStatNew)
        { 

            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;               
                           
                systemRepository = new SystemStatsRepository();
                HttpResponseMessage data = new HttpResponseMessage();

               
              var StatResult = JsonConvert.DeserializeObject<List<SystemStats>>(systemStatNew.systemStat);

                // Parse User Activity Data to Store in DB
                if (StatResult != null)
                {
                    for (int i = 0; i < StatResult.Count; i++)
                    {


                        StringContent sc = new StringContent(systemRepository.InsertSystemStats(StatResult[i].UserID, StatResult[i].ControlType,
                        StatResult[i].ActionName, StatResult[i].ActionDate, StatResult[i].DeviceType, StatResult[i].Value, StatResult[i].PageURL,
                        StatResult[i].PageName, StatResult[i].ControlID, StatResult[i].ControlName, StatResult[i].BrowserName, StatResult[i].BrowserVersion));

                        sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        data.Content = sc;
                    }
                }
                
               
                
                return data;
                
            }
            catch (Exception ex)
            {
                log.Error("-> api/Order/insertUserActivity", ex);
            }
            finally
            {
                systemRepository = null;
            }
            return null;
        }

     //  [Authorize(Roles = "Broker")]
       [Authorize(Roles = CustomRoles.BrkRole)]
       [HttpPost]
       [Route("api/UserStats/getUserActivity")]
       public HttpResponseMessage GetUserActivity(SystemStats objSystemStat)
       {
           // UserController declaration for Token Varification
           UserController userController = null;
           try
           {
               userController = new UserController();
               string token = string.Empty;
               var request = Request;
               var headers = request.Headers;
               string userId = string.Empty;
 
               systemRepository = new SystemStatsRepository();
               StringContent sc = new StringContent(systemRepository.GetSystemStats(objSystemStat.UserID, objSystemStat.FromDate, objSystemStat.ToDate));
               sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
               HttpResponseMessage data = new HttpResponseMessage();
               data.Content = sc;
               return data;
             

           }
           catch (Exception ex)
           {
               log.Error("-> api/OrderHistory/getUserActivity", ex);
           }
           finally
           {
             
           }
           return null;
       }



       //  Get Release Updates By Version
       [Authorize(Roles = CustomRoles.BrkRole)]
       [HttpPost]
       [Route("api/UserStats/GetReleaseUpdatesByVersion")]
       public HttpResponseMessage GetReleaseUpdatesByVersion(SystemStats objSystemStat)
       {
           // UserController declaration for Token Varification
           UserController userController = null;
           try
           {
               userController = new UserController();
               string token = string.Empty;
               var request = Request;
               var headers = request.Headers;
               string userId = string.Empty;

               systemRepository = new SystemStatsRepository();
               StringContent sc = new StringContent(systemRepository.GetReleaseUpdatesByVersion(objSystemStat.ReleaseVersion, objSystemStat.ApplicationName));
               sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
               HttpResponseMessage data = new HttpResponseMessage();
               data.Content = sc;
               return data;
          
           }
           catch (Exception ex)
           {
               log.Error("-> api/OrderHistory/GetOrderHistory", ex);
           }
           finally
           {
             
           }
           return null;
       }
    }
}
