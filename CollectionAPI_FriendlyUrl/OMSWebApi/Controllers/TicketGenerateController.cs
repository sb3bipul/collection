﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using System.Threading.Tasks;
using System.Web;
//using OMSWebApi.UploadRepo;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OMSWebApi.Models;
using OMSWebApi.Repository;
using System.Net.Http.Headers;
using System.Data;
namespace OMSWebApi.Controllers
{
    public class TicketGenerateController : ApiController
    {
        [Mime]
        public async Task<FileUploadDetails> Post()
        {
            // file path
            var fileuploadPath = HttpContext.Current.Server.MapPath("~/UploadedFiles");
            // 
            var multiFormDataStreamProvider = new MultiFileUploadProvider(fileuploadPath);

            // Read the MIME multipart asynchronously 
            await Request.Content.ReadAsMultipartAsync(multiFormDataStreamProvider);

            String uploadingFileName = multiFormDataStreamProvider
                .FileData.Select(x => x.LocalFileName).FirstOrDefault();

            String uploadingFormData = multiFormDataStreamProvider.FormData.GetValues(0).FirstOrDefault();//.FirstOrDefault();
            uploadingFormData = uploadingFormData.Replace("\\", "");

            dynamic frmData = JsonConvert.DeserializeObject(uploadingFormData);
            String CustId, CompId, TicketImgName, TicketImgPath, TicketReason, OtherReason, CustomerEmail;
            CustId = frmData.CustomerID;
            CompId = frmData.CompanyID;
            TicketReason = frmData.TicketReason;
            OtherReason = frmData.OtherReason;
            TicketImgName = frmData.UploadFileName;
            TicketImgPath = frmData.UploadFilePath;
            CustomerEmail = frmData.CustomerEmail;
            DataTable dtl = new DataTable();
            CustProfileRepository custProfile = new CustProfileRepository();
            dtl = custProfile.SendEmailByTicket(CustId, CompId, CustomerEmail, OtherReason, TicketReason, TicketImgName, TicketImgPath);

            //CustProfileRepository custProfile = null;
            //custProfile = new CustProfileRepository();
            //StringContent sc = new StringContent(custProfile.SendEmailByTicket(CustId, CompId, CustomerEmail, OtherReason, TicketReason, TicketImgName, TicketImgPath));
            //sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            //HttpResponseMessage data = new HttpResponseMessage();
            //data.Content = sc;
            
            // Create response
            return new FileUploadDetails
            {
                FilePath = uploadingFileName,

                FileName = "Message: " + Convert.ToString(dtl.Rows[0]["Msg"]) + ", TicketId: " + Convert.ToString(dtl.Rows[0]["TicketId"]) + "",

                FileLength = new FileInfo(uploadingFileName).Length,

                FileCreatedTime = DateTime.Now.ToLongDateString()
            };
        }
    }
}
