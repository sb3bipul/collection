﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;
using OMSWebApi.Models;
using OMSWebApi.Repository;
using System.Web.Http.Cors;


namespace OMSWebApi.Controllers
{
    public class BackRoomInventoryController : ApiController
    {
        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        BackRoomInventoryRepository backRoomInventoryRepository = null;

        /// <summary>
        /// Insert Item and Get item list from Back Room Inventory 
        /// </summary>
        /// <param name="objOrderTemplate"></param>
        /// <returns></returns>
      //  [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/BackRoomInventory/getBackRoomInventoryItemList")]
        public HttpResponseMessage getOrderItemList(Order objOrderTemplate)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                 backRoomInventoryRepository = new BackRoomInventoryRepository();
                 StringContent sc = new StringContent(backRoomInventoryRepository.getBackRoomInventoryList(objOrderTemplate.CompanyId, objOrderTemplate.Dept, objOrderTemplate.ItemCode, objOrderTemplate.CaseQuantity,
                                             objOrderTemplate.UnitQuantity, objOrderTemplate.CatalogId, objOrderTemplate.CustomerId, objOrderTemplate.BrokerId,
                                             objOrderTemplate.LanguageId, objOrderTemplate.WarehouseId, objOrderTemplate.isPickUp, objOrderTemplate.Comments,
                                             objOrderTemplate.Action, objOrderTemplate.EOR, objOrderTemplate.Result, objOrderTemplate.DeliveryDate,
                                             objOrderTemplate.ClientPONumber, objOrderTemplate.PromoCode, objOrderTemplate.Day, objOrderTemplate.NotDay, objOrderTemplate.AmountCollected,
                                             objOrderTemplate.isRestricted, objOrderTemplate.QOH, objOrderTemplate.CasePrice, objOrderTemplate.RetailPrice,
                                             objOrderTemplate.UnitForCasePrice, objOrderTemplate.PromoAmount, objOrderTemplate.IsPromoItem, objOrderTemplate.PromoMinQuantity, objOrderTemplate.OriginalCasePrice));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
                //}
                //else
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");

            }
            catch (Exception ex)
            {
                log.Error("-> api/Order/getOrderItemList", ex);
            }
            finally
            {
                backRoomInventoryRepository = null;
            }
            return null;
        }


        // Delete Back room Inventory Item
      //  [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/BackRoomInventory/deleteBackRoomInventoryItem")]
        public HttpResponseMessage deleteOrderItem(Order objOrderTemplate)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                backRoomInventoryRepository = new BackRoomInventoryRepository();
                StringContent sc = new StringContent(backRoomInventoryRepository.deleteBackRoomInventoryItem(objOrderTemplate.ItemCode, objOrderTemplate.CustomerId, objOrderTemplate.BrokerId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;              

            }
            catch (Exception ex)
            {
                log.Error("-> api/Order/deleteBackRoomInventoryItem", ex);
            }
            finally
            {
                backRoomInventoryRepository = null;
            }
            return null;
        }


        /// <summary>
        /// Delete All Items from Back Room Inventory by Customer
        /// </summary>
        /// <param name="objOrderTemplate"></param>
        /// <returns></returns>
      //  [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/BackRoomInventory/deleteAllBackRoomItems")]
        public HttpResponseMessage DeleteAllOrderItems(Order objOrderTemplate)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                backRoomInventoryRepository = new BackRoomInventoryRepository();
                StringContent sc = new StringContent(backRoomInventoryRepository.deleteAllBackRoomItems(objOrderTemplate.CustomerId, objOrderTemplate.BrokerId));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
                //}
                //else
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");

            }
            catch (Exception ex)
            {
                log.Error("-> api/Order/deleteAllBackRoomItems", ex);
            }
            finally
            {
                backRoomInventoryRepository = null;
            }
            return null;
        }


        // Update Backroom Item quantity
     //   [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/BackRoomInventory/updateItemCaseQuantity")]
        public HttpResponseMessage UpdateItemCaseQuantity(Order objOrderTemplate)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                // Check if the client request have header with Authorization key
                if (headers.Contains("Authorization"))
                {
                    // Read token received from client's request in header
                    token = headers.GetValues("Authorization").First();
                    userId = "013030";//headers.GetValues("UserId").First();
                }

                // Check if token received from client is valid, if validated then send customer list otherwise unauthorized note.
                if (userController.IsValidToken(token, userId))
                {
                    backRoomInventoryRepository = new BackRoomInventoryRepository();
                    StringContent sc = new StringContent(backRoomInventoryRepository.updateItemCaseQuantity(objOrderTemplate.CaseQuantity, objOrderTemplate.ItemCode, objOrderTemplate.CustomerId, objOrderTemplate.BrokerId));
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    HttpResponseMessage data = new HttpResponseMessage();
                    data.Content = sc;

                    return data;
                }
                else
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Authorization has been denied for this request.");

            }
            catch (Exception ex)
            {
                log.Error("-> api/Order/updateItemCaseQuantity", ex);
            }
            finally
            {
                backRoomInventoryRepository = null;
            }
            return null;
        }

    }
}
