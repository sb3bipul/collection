﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using OMSWebApi.Models;
using OMSWebApi.Repository;

namespace OMSWebApi.Controllers
{
   
  //  [Authorize(Roles = CustomRoles.BrkRole)]
    [Authorize(Roles = "Admin")]
    public class ApplicationController : ApiController
    {
        ApplicationRepository appRepository = null;
        
        // GET: api/Application
        [HttpGet]
        [Route("api/Application/GetAllApplicationList")]
        public IEnumerable<Applications> Get()
        {
            appRepository = new ApplicationRepository();
            return (appRepository.GetApplicationListFromDb());
        }

        [HttpPost]
        [Route("api/Application/UpdateApplicationById")]
        public bool UpdateApplicationById(Applications appObj)
        {
            appRepository = new ApplicationRepository();
            return appRepository.UpdateApplicationToDb(appObj);
        }

        [HttpPost]
        [Route("api/Application/AddNewApplication")]
        public bool AddNewApplication(Applications appObj) 
        {
            appRepository = new ApplicationRepository();
            return appRepository.AddNewApplicationToDb(appObj);
        }


        [HttpPost]
        [Route("api/Application/DeleteApplication")]
        public bool DeleteApplication(Applications appObj)
        {
            appRepository = new ApplicationRepository();
            return appRepository.DeleteApplicationFromDb(appObj);
        }


        // GET: api/Application/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Application
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Application/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Application/5
        public void Delete(int id)
        {
        }
    }
}
