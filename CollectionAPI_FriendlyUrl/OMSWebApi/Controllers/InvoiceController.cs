﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Filters;
using OMSWebApi.Models;
using OMSWebApi.Repository;

namespace OMSWebApi.Controllers
{
    public class InvoiceController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        InvoiceRepository invoiceRepository = null;

        //  [Authorize(Roles = "Broker")]
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Invoice/getOrderNoByOrderDate")]
        public HttpResponseMessage getOrderDetailByNo(Invoice objInv)
        {
            try
            {
                invoiceRepository = new InvoiceRepository();

                StringContent sc = new StringContent(invoiceRepository.getOrderNoByOrderDateData(objInv.OrderDateFrom, objInv.OrderDateTo));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/Invoice/getOrderNoByOrderDate", ex);
            }
            finally
            {
                invoiceRepository = null;
            }
            return null;
        }

        // To get the order detail by order number drop down list               
        //  ON CLICK "NEXT" BUTTON     api/OrderHistory/GetSubmittedOrderDetails   

        // To save the invoice 
        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Order/submitInvoice")]
        public HttpResponseMessage submitQuickOrder(InvoiceDetails objInv)
        {
            // UserController declaration for Token Varification
            UserController userController = null;
            try
            {
                userController = new UserController();
                string token = string.Empty;
                var request = Request;
                var headers = request.Headers;
                string userId = string.Empty;

                invoiceRepository = new InvoiceRepository();
                //StringContent sc = new StringContent("");
                StringContent sc = new StringContent(invoiceRepository.submitInvoiceOrders(objInv.CustomerId, objInv.CompanyId, objInv.BrokerId,
                    objInv.OrderNo, objInv.InvoiceDate, objInv.CustBalanceDue, objInv.NetAmount, objInv.OutshandingBalance, objInv.ShippingCharges,
                    objInv.NetAmountToBePaid, objInv.PaymentMode, objInv.CustomerNotes, objInv.InvoiceTotalQty));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/Order/submitInvoice", ex);
            }
            finally
            {
                invoiceRepository = null;
            }
            return null;
        }

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Invoice/getInvoiceListByDate")]
        public HttpResponseMessage getInvoiceListByDate(Invoice objInv)
        {
            try
            {
                invoiceRepository = new InvoiceRepository();

                StringContent sc = new StringContent(invoiceRepository.getInvoiceDataByDate(objInv.CompanyID, objInv.SalesmanID, objInv.CustomerID, objInv.OrderNo, objInv.OrderDateFrom, objInv.OrderDateTo));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;

                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/Invoice/getInvoiceListByDate", ex);
            }
            finally
            {
                invoiceRepository = null;
            }
            return null;
        }

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Invoice/GetInvoiceDetailsByNo")]
        public HttpResponseMessage GetInvoiceDetailsByNo(Invoice objInv)
        {
            try
            {
                invoiceRepository = new InvoiceRepository();
                StringContent sc = new StringContent(invoiceRepository.GetInvoiceDetailsDataByNo(objInv.InvoiceNo, objInv.CompanyID));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/Invoice/GetInvoiceDetailsByNo", ex);
            }
            finally
            {
                invoiceRepository = null;
            }
            return null;
        }

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Invoice/GetPaymentTransaction")]
        public HttpResponseMessage GetPaymentTransaction(Invoice objInv)
        {
            try
            {
                invoiceRepository = new InvoiceRepository();
                StringContent sc = new StringContent(invoiceRepository.GetPaymentTransaction(objInv.CustomerID, objInv.CompanyID, objInv.SalesmanID));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/Invoice/GetPaymentTransaction", ex);
            }
            finally
            {
                invoiceRepository = null;
            }
            return null;
        }

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Invoice/GetReceiptDetailsByInvoice")]
        public HttpResponseMessage GetReceiptDetailsByInvoice(Invoice objInv)
        {
            try
            {
                invoiceRepository = new InvoiceRepository();
                StringContent sc = new StringContent(invoiceRepository.GetReceiptDetailsDataByInvoice(objInv.CompanyID, objInv.InvoiceNo));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/Invoice/GetReceiptDetailsByInvoice", ex);
            }
            finally
            {
                invoiceRepository = null;
            }
            return null;
        }

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Invoice/GetReceiptList")]
        public HttpResponseMessage GetReceiptList(Invoice objInv)
        {
            try
            {
                invoiceRepository = new InvoiceRepository();
                StringContent sc = new StringContent(invoiceRepository.GetReceiptListBySalesman(objInv.CompanyID, objInv.SalesmanID));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;
            }
            catch (Exception ex)
            {
                log.Error("-> api/Invoice/GetReceiptList", ex);
            }
            finally
            {
                invoiceRepository = null;
            }
            return null;
        }

        [Authorize(Roles = CustomRoles.BrkRole)]
        [HttpPost]
        [Route("api/Invoice/GetPaymentList")]
        public HttpResponseMessage GetPaymentList(Invoice objInv)
        {
            try
            {
                invoiceRepository = new InvoiceRepository();
                StringContent sc = new StringContent(invoiceRepository.GetPaymentDataList(objInv.CompanyID, objInv.SalesmanID));
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage data = new HttpResponseMessage();
                data.Content = sc;
                return data;

            }
            catch (Exception ex)
            {
                log.Error("-> api/Invoice/GetPaymentList", ex);
            }
            finally
            {
                invoiceRepository = null;
            }
            return null;
        }
        
    }
}
