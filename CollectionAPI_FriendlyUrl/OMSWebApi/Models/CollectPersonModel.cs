﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMSWebApi.Models
{
    public class CollectPersonModel
    {
        public string CPCODE { get; set; }
        public string CPNAME { get; set; }
        public string EmailId { get; set; }
        public string ContactNo { get; set; }
        public int CompanyId { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Zone { get; set; }
        public string StateId { get; set; }
        public int CountryId { get; set; }
        public string ZipCode { get; set; }
        public string Password { get; set; }
        public string CPassword { get; set; }
        public string CPImage { get; set; }
        public string ImageBase64String { get; set; }
    }

    public class CollectPersonList
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string CPName { get; set; }
        public string EmailId { get; set; }
    }

    public class StateList
    {
        public int CountryCode { get; set; }
    }

    public class CompanyList
    {
        public int CompanyId { get; set; }
    }

    public class CustomerModel
    {
        public int CompanyId { get; set; }
        public string CustCode { get; set; }
        public string CustName { get; set; }
        public int Zone { get; set; }
        public string BrokerId { get; set; }
    }

    public class AssignCustomer
    {
        public string CPCode { get; set; }
        public string CustCode { get; set; }
        public int CompanyId { get; set; }
    }

    public class SearchAssignCust
    {
        public string CustCode { get; set; }
        public string CPName { get; set; }
        public string CustName { get; set; }
    }

    public class Userlogin
    {
        public string UserId { get; set; }
        public string Password { get; set; }
        public string CompanyId { get; set; }
    }

    public class CustomerListCP
    {
        public string CPCode { get; set; }
        public string CompanyId { get; set; }
    }

    public class UpdateCp
    {
        public string CPName { get; set; }
        public string EmailId { get; set; }
        public string ContactNo { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Zone { get; set; }
        public string StateId { get;set;}
        public int CountryId { get; set; }
        public string Zip { get; set; }
        public string CPCode { get; set; }
    }
    public class CPStatus
    {
        public Boolean Status { get; set; }
        public int CompanyId { get; set; }
        public string CPCode { get; set; }
    }

    public class BrokerList
    {
        public string CompanyId { get; set; }
    }

    public class DelCPCustomer
    {
        public string CPCode { get; set; }
        public string CustCode { get; set; }
        public string CompanyId { get; set; }
    }

    public class CPCustomer
    {
        public string CPCode { get; set; }
        public string CompantId { get; set; }
    }
    public class UPdateCPCustomer
    {
        public string CPCode {get;set;}
		public string CustomerCode {get;set;}
        public int CompanyId { get; set; }
    }
}