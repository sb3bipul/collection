﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMSWebApi.Models
{
    public class StateModel
    {
        public string StateName { get; set; }
        public int StateId { get; set; }
    }

}