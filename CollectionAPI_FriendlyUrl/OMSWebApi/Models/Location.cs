﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMSWebApi.Models
{
    public class Location
    {
        public String UserId { get; set; }
        public String UserName { get; set; }
        public DateTime CreatedDate { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public String LocationName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}