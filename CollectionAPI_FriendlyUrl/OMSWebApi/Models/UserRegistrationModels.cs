﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMSWebApi.Models
{
    public class UserRegistrationModels
    {
        public string UserID { get; set; }
        public int CompanyID { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string ContactNo { get; set; }
        public string EmailID { get; set; }
        public string Password { get; set; }
        public int UserType { get; set; }
        public string IMEI { get; set; }
    }
}