﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMSWebApi.Models
{
    public class Invoice
    {
        public string OrderDateFrom { get; set; }
        public string OrderDateTo { get; set; }
        public string OrderNo { get; set; }
        public string CompanyID { get; set; }
        public string SalesmanID { get; set; }
        public string CustomerID { get; set; }
        public string InvoiceNo { get; set; }
    }
    public class InvoiceDetails
    {
        public string CustomerId { get; set; }
        public string CompanyId { get; set; }
        public string BrokerId { get; set; }
        public string OrderNo { get; set; }
        public DateTime InvoiceDate { get; set; }
        public decimal CustBalanceDue { get; set; }
        public decimal NetAmount { get; set; }
        public decimal OutshandingBalance { get; set; }
        public decimal ShippingCharges { get; set; }
        public decimal NetAmountToBePaid { get; set; }
        public string PaymentMode { get; set; }
        public string CustomerNotes { get; set; }
        public Int32 InvoiceTotalQty { get; set; }


    }
}