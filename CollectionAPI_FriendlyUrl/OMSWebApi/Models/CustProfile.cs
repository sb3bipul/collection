﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMSWebApi.Models
{
    public class CustProfile
    {
        public string SalesmanId { get; set; }
        public string SearchTxt { get; set; }
        public string CompanyID { get; set; }
        public Int32 CountryID { get; set; }
    }

    public class CustProfileViewModel
    {
        public string CustomerID { get; set; }
        public string CompanyID { get; set; }
        public string SalesmanId { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string EmailID { get; set; }
        public string ContactNo { get; set; }
        public string Organization { get; set; }
        public Decimal Latitude { get; set; }
        public Decimal Longitude { get; set; }
        public string StoreEntranceLocation { get; set; }
        public Decimal StoreEntranceLatitude { get; set; }
        public Decimal StoreEntranceLongitude { get; set; }
        public string ReceivingEntranceLocation { get; set; }
        public Decimal ReceivingEntranceLatitude { get; set; } 
        public Decimal ReceivingEntranceLongitude { get; set; }
        public string StoreLogoImgName { get; set; }
        public string StoreLogoImgBinaryData { get; set; }
        public string StoreLogoImgPath { get; set; }
        public string LocalCustomerID { get; set; }
        public string Zone { get; set; }
        public IEnumerable<DayHourItem> dayHourItems { get; set; }
        public IEnumerable<ReceivingHourItem> receivingHourItems { get; set; }
        //public OrderItems[] orderitemDetails { get; set; }
    }
    public class DayHourItem
    {
        public string CustomerID { get; set; }
        public string CompanyID { get; set; }
        public int DayHoursID { get; set; }
        public string WeekDay { get; set; }
        public DateTime CreatedDate { get; set; }
        public string TimeFrom { get; set; }
        public string TimeTo { get; set; }
        public Int64 SrlNo { get; set; }
    }
    public class ReceivingHourItem
    {
        public string CustomerID { get; set; }
        public string CompanyID { get; set; }
        public int ReceiveHoursID { get; set; }
        public string WeekDay { get; set; }
        public DateTime CreatedDate { get; set; }
        public string TimeFrom { get; set; }
        public string TimeTo { get; set; }
        public Int64 SrlNo { get; set; }
    }
    //public class DayHours
    //{
    //    public string CustomerID { get; set; }
    //    public string CompanyID { get; set; }
    //    public int DayHoursID { get; set; }
    //    public string WeekDay { get; set; }
    //    public DateTime CreatedDate { get; set; }
    //    public string TimeFrom { get; set; }
    //    public string TimeTo { get; set; }
    //}
    public class SendEmail
    {
        public string CustomerId { get; set; }
        public string CompanyId { get; set; }
        public string RequestId { get; set; }
        public string CustomerEmail { get; set; }
        public string Action { get; set; }
        public string Comments { get; set; }
        public string UserEmpID { get; set; }

    }
    public class SectionImg
    {
        public string CustomerID { get; set; }
        public string CompanyID { get; set; }
        public int SectionID { get; set; }
        public string UploadShopName { get; set; }
        public string UploadShopImagePath { get; set; }
        public string UploadShopImageName { get; set; }
        public string SrlNo { get; set; }
    }

    public class ShopOutsideImg
    {
        public string CustomerID { get; set; }
        public string CompanyID { get; set; }
        public int OutsideID { get; set; }
        public string UploadShopName { get; set; }
        public string UploadShopImagePath { get; set; }
        public string UploadShopImageName { get; set; }
        public string SrlNo { get; set; }
    }

    public class ShopInsideImg
    {
        public string CustomerID { get; set; }
        public string CompanyID { get; set; }
        public int InsideID { get; set; }
        public string UploadShopName { get; set; }
        public string UploadShopImagePath { get; set; }
        public string UploadShopImageName { get; set; }
        public string SrlNo { get; set; }
    }

    public class ImgReuslt
    {
        public string Msg { get; set; }
        public string CustomerId { get; set; }
    }
    public class ImageResult
    {
        public string Status { get; set; }
        public string ImageId { get; set; }
        public string ImageURL { get; set; }
        public string LocalCustomerID { get; set; }
    }
    public class ImageData
    {
        public string CustomerId { get; set; }
        public String CompanyId { get; set; }
        public int Srl { get; set; }
        public string ImageName { get; set; }
        public string ImageBinaryData { get; set; }
        public string Action { get; set; }
        public string LocalCustomerID { get; set; }
    }
    public class SaveTicket
    {
        public string CustomerId { get; set; }
        public string UserEmpId { get; set; }
        public string CompanyId { get; set; }
        public string CustomerEmail { get; set; }
        public string OtherReason { get; set; }
        public string TicketReason { get; set; }
        public string fileName { get; set; }
        public string FilePath { get; set; }
        public string TicketId { get; set; }
        
    }

    public class UpdateTicket
    {
        public string CompanyId { get; set; }
        public string TicketID { get; set; }
        public string Status { get; set; }
        public string UserId { get; set; }
        public string CustomerEmail {get; set;} 
        public string OtherReason {get; set; }
        public string TicketReason { get; set; }
    }

    public class SaveContactUs
    {
        public string CompanyID { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public Int32 ContactSubject { get; set; }
        public string ContactMessage { get; set; }
        public string ContactPhone { get; set; }
        public string ContactCompany { get; set; }
        public string ContactPerson { get; set; }
    }


    public class ImageViewModel
    {
        public String CustomerId { get; set; } 
        public String CompanyId { get; set; }
        public IEnumerable<SectionImage> sectionImgs { get; set; }
        public IEnumerable<OutsideImg> outsideImgs { get; set; }
        public IEnumerable<InsideImg> insideImgs { get; set; }
        //public OrderItems[] orderitemDetails { get; set; }
    }

    public class SectionImage
    {
        public int SectionSrl { get; set; }
        public string SectionName { get; set; }
    }

    public class OutsideImg
    {
        public int OutsideSrl { get; set; }
        public string OutsideName { get; set; }
    }

    public class InsideImg
    {
        public int InsideSrl { get; set; }
        public string InsideName { get; set; }
    }

    public class TicketDet
    {
        public string TicketReason { get; set; }
        public string UploadFileName { get; set; }
        public string UploadFilePath { get; set; }
        public string OtherReason { get; set; }
        public string CustomerID { get; set; }
        public string CompanyID { get; set; } 
        public string CustomerEmail { get; set; }
        public string UserEmpId { get; set; }
        public string Status { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }
    public class TicketStatusDetail
    {
        public string TicketStatus { get; set; }
    }
    
}