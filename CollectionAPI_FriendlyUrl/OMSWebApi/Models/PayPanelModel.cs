﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMSWebApi.Models
{
    public class PayPanelModel
    {
        public string amount { get; set; }
        public string customer_name { get; set; }
        public string email { get; set; }
        public string mobile { get; set; }
        public string order_increment_id { get; set; }
        public string currency { get; set; }
    }

    public class PaymentDue
    {
        public long Amount { get; set; }
        public string Customer_Name { get; set; }
        public string Currency { get; set; }

    }
}