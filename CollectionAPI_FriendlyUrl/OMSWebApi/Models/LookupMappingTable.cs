﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMSWebApi.Models
{
    public class LookupMappingTable
    {
        public int CodeId { get; set; }
        public string Name { get; set; }
        public string Value1 { get; set; }
        public string Description { get; set; }
        public int ParentCodeId { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public bool IsActive { get; set; }
        public string SelectedValue { get; set; }
    }
}