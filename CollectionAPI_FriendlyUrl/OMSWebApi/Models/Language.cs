﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMSWebApi.Models
{
    public class Language
    {         
        public string ApplicationName { get; set; }
        public string ObjectName { get; set; }
        public string LanguageName { get; set; }        
    }
}