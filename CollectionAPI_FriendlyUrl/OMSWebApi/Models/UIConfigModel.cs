﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMSWebApi.Models
{
    public class UIConfigModel
    {
        public Boolean IsEmailCustomer { get; set; }
        public Boolean IsEmailCompany { get; set; }
        public Boolean IsEmailBroker { get; set; }
        public Boolean IsEmailCP { get; set; }
        public string CreatedBy { get; set; }
    }

    public class SearchCinfigDt
    {
        public int CompanyId { get; set; }
    }

    public class CompanyMaster
    {
        public string CompanyName { get; set; }
        public string ContactNo { get; set; }
        public string EmailId { get; set; }
        public string Fax { get; set; }
        public string ContactPerson { get; set; }
        public string ContactPersonEmail { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string StateId { get; set; }
        public string CountryId { get; set; }
        public string Zip { get; set; }
        public string CompanyURL { get; set; }
        public string CompanyLogo { get; set; }
        public string CompanyBanner { get; set; }
        //public string LogoPath { get; set; }
        //public string BannerPath { get; set; }
        //public string LogoName { get; set; }
        //public string BannerName { get; set; }
        public string Base64logo { get; set; }
        public string Base64Banner { get; set; }
    }

    public class SearchCompanyList
    {
        public string CompanyName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }

    public class UIConfig
    {
        public string ClientName { get; set; }
        public string ClientID { get; set; }
        public Boolean IsERP { get; set; }
        public Boolean IsOthers { get; set; }
        public string CreatedBy { get; set; }
    }

    public class CollectionTempCust
    {
        public string CustName { get; set; }
        public string EmailId { get; set; }
        public string ContactNo { get; set; }
        public string Address { get; set; }
        public string Status { get; set; }
        public int CompanyId { get; set; }
        public string CreatedBy { get; set; }
    }

    public class SearchCustConfig
    {
        public string ClientName { get; set; }
    }

    public class UpdateCompany
    {
        public string CompanyName { get; set; }
        public string ContactNo { get; set; }
        public string Emailid { get; set; }
        public string Fax { get; set; }
        public string ContactPerson { get; set; }
        public string ContactPersonEmail { get; set; }
        public string CompanyURL { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string StateId { get; set; }
        public string CountryId { get; set; }
        public string Zip { get; set; }
        public int CompanyId { get; set; }
    }

    public class DeleteCompany
    {
        public int CompanyId { get; set; }
    }

    public class ChangeCompanyStatus
    {
        public int CompanyId { get; set; }
        public Boolean Status { get; set; }
    }

    public class AppUIConfig
    {
        public string ClientName { get; set; }
        public string Themecolor { get; set; }
        public string Currency { get; set; }
        public bool IsDisplayInvoice { get; set; }
    }

    public class PaymentMethodConfig
    {
        public string ClientName { get; set; }
        public string PaymentMethod { get; set; }
        public string Name { get; set; }
    }
}