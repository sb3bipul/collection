﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using OMSWebApi.Models;
using OMSWebApi.Repository;

namespace OMSWebApi.Models
{
    public class CompanyModels
    {
        DBConnection dbConnectionObj = null;
        SqlCommand com = null;
        SqlDataAdapter da = null;
        DataTable dt = null;

        #region Get LoginImg on load event
        public List<ScreenData> GetLoginImgListFromDb(string ScreenName, int CompanyId)
        {
            Common common = new Common();
            List<ScreenData> LoginImgListObj = null;
            try
            {
                LoginImgListObj = new List<ScreenData>();
                dbConnectionObj = new DBConnection();
                com = new SqlCommand(ApiConstant.SProcGetScreenImageList, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@ScreenName", ScreenName);
                com.Parameters.AddWithValue("@CompanyID", CompanyId);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                ////Bind LoginImgModel generic list using LINQ 
                LoginImgListObj = (from DataRow dr in dt.Rows
                                   select new ScreenData()
                                       {
                                           ScreenName = Convert.ToString(dr["ScreenName"]),
                                           ImageFullPath = Convert.ToString(dr["ImageFullPath"]),
                                           FolderName = Convert.ToString(dr["FolderName"]),
                                           FolderHeaderImage = Convert.ToString(dr["FolderHeaderImage"]),
                                           HeaderImageName = Convert.ToString(dr["HeaderImageName"]),
                                           ImageName = Convert.ToString(dr["ImageName"]),
                                           MenuAciveImage = Convert.ToString(dr["MenuAciveImage"]),
                                           MenuText = Convert.ToString(dr["MenuText"]),
                                           DashboardTopImage = Convert.ToString(dr["DashboardTopImage"]),
                                           DashboardTopText = Convert.ToString(dr["DashboardTopText"]),
                                           FooterActiveImage = Convert.ToString(dr["FooterActiveImage"]),
                                           FooterInactiveImage = Convert.ToString(dr["FooterInactiveImage"]),
                                           FooterActiveFontColor = Convert.ToString(dr["FooterActiveFontColor"]),
                                           FooterInactiveFontColor = Convert.ToString(dr["FooterInactiveFontColor"]),
                                           FooterText = Convert.ToString(dr["FooterText"]),
                                           DashboardMiddleImage = Convert.ToString(dr["DashboardMiddleImage"]),
                                           DashboardMiddleText = Convert.ToString(dr["DashboardMiddleText"]),
                                           DashboardBottomImage = Convert.ToString(dr["DashboardBottomImage"])
                                       }).ToList();
                
                //if (dt != null)
                //{
                //    if (dt.Rows.Count > 0)
                //    {
                //        for (int i = 0; i < dt.Rows.Count; i++)
                //        {
                //            LoginImgListObj.Add(new LoginImgData
                //            {
                //                ScreenName = Convert.ToString(dt.Rows[i]["ScreenName"]),
                //                ImageFullPath = Convert.ToString(dt.Rows[i]["ImageFullPath"]),
                //                FolderName = Convert.ToString(dt.Rows[i]["FolderName"]),
                //                HeaderImageName = Convert.ToString(dt.Rows[i]["HeaderImageName"]),
                //                ImageName = Convert.ToString(dt.Rows[i]["ImageName"])
                //            });
                //        }
                //    }
                //}

            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbConnectionObj = null;
                com = null;
                da = null;
                dt = null;
            }

            return LoginImgListObj;
        }

        public class ListtoDataTable
        {
            public DataTable ToDataTable<T>(List<T> items)
            {
                DataTable dataTable = new DataTable(typeof(T).Name);
                //Get all the properties by using reflection   
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names  
                    dataTable.Columns.Add(prop.Name);
                }
                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {

                        values[i] = Props[i].GetValue(item, null);
                    }
                    dataTable.Rows.Add(values);
                }

                return dataTable;
            }
        }

        public DataTable GetMenuFromDb(string ScreenName, int CompanyId)
        {
            Common common = new Common();
            List<MenuDetails> MenuDetailsListObj = null;
            DataTable dtl = null; 
            try
            {
                dtl = new DataTable();
                MenuDetailsListObj = new List<MenuDetails>();
                dbConnectionObj = new DBConnection();
                com = new SqlCommand(ApiConstant.SProcGetScreenImageList, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@ScreenName", ScreenName);
                com.Parameters.AddWithValue("@CompanyID", CompanyId);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                ////Bind LoginImgModel generic list using LINQ  
                MenuDetailsListObj = (from DataRow dr in dt.Rows
                                   select new MenuDetails()
                                   {
                                       ScreenName = Convert.ToString(dr["ScreenName"]),
                                       ImageFullPath = Convert.ToString(dr["ImageFullPath"]),
                                       FolderName = Convert.ToString(dr["FolderName"]),
                                       FolderHeaderImage = Convert.ToString(dr["FolderHeaderImage"]),
                                       HeaderImageName = Convert.ToString(dr["HeaderImageName"]),
                                       ImageName = Convert.ToString(dr["ImageName"]),
                                       MenuAciveImage = Convert.ToString(dr["MenuAciveImage"]),
                                       MenuText = Convert.ToString(dr["MenuText"]),
                                       
                                   }).ToList();
                ListtoDataTable lsttodt = new ListtoDataTable();
                dtl = lsttodt.ToDataTable(MenuDetailsListObj);
                
            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbConnectionObj = null;
                com = null;
                da = null;
                dt = null;
                MenuDetailsListObj = null;
            }

            return dtl;
        }
        public DataTable GetFooterFromDb(string ScreenName, int CompanyId)
        {
            Common common = new Common();
            List<FooterDetails> FooterDetailsListObj = null;
            DataTable dtl = null;
            try
            {
                dtl = new DataTable();
                FooterDetailsListObj = new List<FooterDetails>();
                dbConnectionObj = new DBConnection();
                com = new SqlCommand(ApiConstant.SProcGetScreenImageList, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@ScreenName", ScreenName);
                com.Parameters.AddWithValue("@CompanyID", CompanyId);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                ////Bind LoginImgModel generic list using LINQ 
                FooterDetailsListObj = (from DataRow dr in dt.Rows
                                        select new FooterDetails()
                                   {
                                       ScreenName = Convert.ToString(dr["ScreenName"]),
                                       ImageFullPath = Convert.ToString(dr["ImageFullPath"]),
                                       FolderName = Convert.ToString(dr["FolderName"]),
                                       FolderHeaderImage = Convert.ToString(dr["FolderHeaderImage"]),
                                       HeaderImageName = Convert.ToString(dr["HeaderImageName"]),
                                       ImageName = Convert.ToString(dr["ImageName"]),
                                       FooterActiveImage = Convert.ToString(dr["FooterActiveImage"]),
                                       FooterInactiveImage = Convert.ToString(dr["FooterInactiveImage"]),
                                       FooterActiveFontColor = Convert.ToString(dr["FooterActiveFontColor"]),
                                       FooterInactiveFontColor = Convert.ToString(dr["FooterInactiveFontColor"]),
                                       FooterText = Convert.ToString(dr["FooterText"])
                                   }).ToList();
                ListtoDataTable lsttodt = new ListtoDataTable();
                dtl = lsttodt.ToDataTable(FooterDetailsListObj);

            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbConnectionObj = null;
                com = null;
                da = null;
                dt = null;
                FooterDetailsListObj = null;
            }

            return dtl;
        }

        public DataTable GetDashboardTopListFromDb(int CompanyId)
        {
            Common common = new Common();
            List<DashBoardTop> DashTopListObj = null;
            DataTable dtl = new DataTable();
            try
            {
                DashTopListObj = new List<DashBoardTop>();
                dbConnectionObj = new DBConnection();
                com = new SqlCommand(ApiConstant.SProcGetDashboardFullList, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("ScreenName", "Dashboard");
                com.Parameters.AddWithValue("@CompanyID", CompanyId);
                com.Parameters.AddWithValue("@Action", "Top");
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            DashTopListObj.Add(new DashBoardTop
                            {
                                ScreenName = Convert.ToString(dt.Rows[i]["ScreenName"]),
                                ImageFullPath = Convert.ToString(dt.Rows[i]["ImageFullPath"]),
                                DashboardTopImage = Convert.ToString(dt.Rows[i]["DashboardTopImage"]),
                                TextCaption = Convert.ToString(dt.Rows[i]["DashboardTopText"]),
                                TopValue = "0"
                            });
                        }
                    }
                }
                ListtoDataTable lsttodt = new ListtoDataTable();
                dtl = lsttodt.ToDataTable(DashTopListObj);
                
            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnectionObj = null;
                DashTopListObj = null;
                com = null;
                da = null;
                dt = null;
            }

            return dtl;
        }

        public DataTable GetDashboardMiddleListFromDb(int CompanyId, String SalesmanId)
        {
            Common common = new Common();
            List<DashBoardMiddle> DboardMiddleListObj = null;
            DataTable dtl = null;
            try
            {
                dtl = new DataTable();
                DboardMiddleListObj = new List<DashBoardMiddle>();
                dbConnectionObj = new DBConnection();
                com = new SqlCommand(ApiConstant.SProcGetDashboardFullList, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("ScreenName", "Dashboard");
                com.Parameters.AddWithValue("@CompanyID", CompanyId);
                com.Parameters.AddWithValue("@Action", "Middle");
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                String TotalCustomers = string.Empty, CustomerVisited = string.Empty, MiddleValue="0";
                AdoHelper adoHelperItem = new AdoHelper();
                DataTable dtCountList = adoHelperItem.ExecDataTableProc(ApiConstant.SPCustomersOrderCountBySalesman, "@SalesmanId", SalesmanId, "@CompanyId", CompanyId);
                adoHelperItem.Dispose();
                if (dtCountList != null)
                {
                    if (dtCountList.Rows.Count > 0)
                    {
                        TotalCustomers = Convert.ToString(dtCountList.Rows[0]["TotalCustomers"]);
                        CustomerVisited = Convert.ToString(dtCountList.Rows[1]["CustomerVisited"]);
                    }
                }
                ////Bind LoginImgModel generic list using LINQ 
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (Convert.ToString(dt.Rows[i]["RowNo"]) == "1")
                                MiddleValue = CustomerVisited;
                            else if (Convert.ToString(dt.Rows[i]["RowNo"]) == "2")
                                MiddleValue = "0";
                            else if (Convert.ToString(dt.Rows[i]["RowNo"]) == "3")
                                MiddleValue = "0";
                            else if (Convert.ToString(dt.Rows[i]["RowNo"]) == "4")
                                MiddleValue = TotalCustomers;
                            else
                                MiddleValue = "0";

                            DboardMiddleListObj.Add(new DashBoardMiddle
                            {
                                ScreenName = Convert.ToString(dt.Rows[i]["ScreenName"]),
                                ImageFullPath = Convert.ToString(dt.Rows[i]["ImageFullPath"]),
                                DashboardMiddleImage = Convert.ToString(dt.Rows[i]["DashboardMiddleImage"]),
                                TextCaption = Convert.ToString(dt.Rows[i]["DashboardMiddleText"]),
                                MiddleValue = MiddleValue
                            });
                        }
                    }
                }

                //DboardMiddleListObj = (from DataRow dr in dt.Rows
                //                        select new DashBoardMiddle()
                //                        {
                //                            ScreenName = Convert.ToString(dr["ScreenName"]),
                //                            ImageFullPath = Convert.ToString(dr["ImageFullPath"]),
                //                            DashboardMiddleImage = Convert.ToString(dr["DashboardMiddleImage"]),
                //                            //TextName = Convert.ToString(dr["TextName"])
                //                            TotalCustomers = TotalCustomers,
                //                            CustomerVisited = CustomerVisited
                //                        }).ToList();
                ListtoDataTable lsttodt = new ListtoDataTable();
                dtl = lsttodt.ToDataTable(DboardMiddleListObj);

            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbConnectionObj = null;
                com = null;
                da = null;
                dt = null;
                DboardMiddleListObj = null;
            }

            return dtl;
        }

        public DataTable GetDashboardBottomListFromDb( int CompanyId)
        {
            Common common = new Common();
            List<DashBoardBottom> DboardBottomListObj = null;
            DataTable dtl = null;
            try
            {
                dtl = new DataTable();
                DboardBottomListObj = new List<DashBoardBottom>();
                dbConnectionObj = new DBConnection();
                com = new SqlCommand(ApiConstant.SProcGetDashboardFullList, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("ScreenName", "Dashboard");
                com.Parameters.AddWithValue("@CompanyID", CompanyId);
                com.Parameters.AddWithValue("@Action", "Bottom");
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                ////Bind LoginImgModel generic list using LINQ 
                DboardBottomListObj = (from DataRow dr in dt.Rows
                                       select new DashBoardBottom()
                                        {
                                            ScreenName = Convert.ToString(dr["ScreenName"]),
                                            ImageFullPath = Convert.ToString(dr["ImageFullPath"]),
                                            DashboardBottomImage = Convert.ToString(dr["DashboardBottomImage"]),
                                            DashboardBottomText = Convert.ToString(dr["DashboardBottomText"]),
                                            ProductName  = Convert.ToString(dr["DisplayName"]),
                                            OriginalCasePrice = Convert.ToDecimal(dr["ListPrice"])
                                        }).ToList();
                ListtoDataTable lsttodt = new ListtoDataTable();
                dtl = lsttodt.ToDataTable(DboardBottomListObj);

            }
            catch (Exception ex)
            {
                String exerror = ex.Message.ToString();
            }
            finally
            {
                dbConnectionObj = null;
                com = null;
                da = null;
                dt = null;
                DboardBottomListObj = null;
            }

            return dtl;
        }

        #endregion Get LoginImg on load event
    }

    public class ScreenData
    {
        public string ScreenName { get; set; }
        public string ImageFullPath { get; set; }
        public string FolderName { get; set; }
        public string FolderHeaderImage { get; set; }
        public string HeaderImageName { get; set; }
        public string ImageName { get; set; }
        public string MenuAciveImage { get; set; }
        public string MenuText { get; set; }
        public string DashboardTopImage { get; set; }
        public string DashboardTopText { get; set; }
        public string FooterActiveImage { get; set; }
        public string FooterInactiveImage { get; set; }
        public string FooterActiveFontColor { get; set; }
        public string FooterInactiveFontColor { get; set; }
        public string FooterText { get; set; }
        public string DashboardMiddleImage { get; set; }
        public string DashboardMiddleText { get; set; }
        public string DashboardBottomImage { get; set; }
    }
    public class MenuDetails
    {
        public string ScreenName { get; set; }
        public string ImageFullPath { get; set; }
        public string FolderName { get; set; }
        public string FolderHeaderImage { get; set; }
        public string HeaderImageName { get; set; }
        public string ImageName { get; set; }
        public string MenuAciveImage { get; set; }
        public string MenuText { get; set; }
    }
    public class CustomerListLabels
    {
        public string ScreenName { get; set; }
        public string ScreenLabel { get; set; }
        public string ScreenImageName { get; set; }
        public string ScreenImagePath { get; set; }
    }
    public class CustomerEntryLabels
    {
        public string ScreenName { get; set; }
        public string ScreenLabel { get; set; }
        public string ScreenImageName { get; set; }
        public string ScreenImagePath { get; set; }
    }
    public class GeneratedCustomerNo
    {
        public string LastCustomerNo { get; set; }
    }
    public class FooterDetails
    {
        public string ScreenName { get; set; }
        public string ImageFullPath { get; set; }
        public string FolderName { get; set; }
        public string FolderHeaderImage { get; set; }
        public string HeaderImageName { get; set; }
        public string ImageName { get; set; }
        public string FooterActiveImage { get; set; }
        public string FooterInactiveImage { get; set; }
        public string FooterActiveFontColor { get; set; }
        public string FooterInactiveFontColor { get; set; }
        public string FooterText { get; set; }
    }
     
    public class CompanyImgData
    {
        public int ImageID { get; set; }
        public string ImageFullPath { get; set; }
        public int CompanyID { get; set; }
        public string cmBGImageName { get; set; }
        public string cmLogoName { get; set; }
        public string cmIconName { get; set; }
        public string HeaderColor { get; set; }
        public string CompanyName { get; set; }
        public string AllPagesBGColor { get; set; }
        public string MiddleSectionBGcolor { get; set; }
        public string FooterBGcolor { get; set; }
        public string OrderVerify { get; set; }
    }
    public class DashBoardTop
    {
        public string ScreenName { get; set; }
        public string ImageFullPath { get; set; }
        public string DashboardTopImage { get; set; }
        public string TextCaption { get; set; }
        public string TopValue { get; set; }
    }
    public class DashBoardMiddle
    {
        public string ScreenName { get; set; }
        public string ImageFullPath { get; set; }
        public string DashboardMiddleImage { get; set; }
        public string TextCaption { get; set; }
        public string MiddleValue { get; set; }
    }
    public class DashBoardBottom
    {
        public string ScreenName { get; set; }
        public string ImageFullPath { get; set; }
        public string DashboardBottomImage { get; set; }
        public string ProductName { get; set; }
        public decimal OriginalCasePrice { get; set; }
        public string DashboardBottomText { get; set; }
    }

    public class CompanyModelsProperty
    { 
        public string CompanyName { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string ContactNo { get; set; }
        public string EmailId { get; set; }
        public string CompanyUrl { get; set; }
        public string ContactPerson { get; set; }
        public string CompanyLogo { get; set; }
        public string LogoBinarydData { get; set; }
        public string CompanyIcon { get; set; }
        public string IconBinaryData { get; set; }
        public string Comments { get; set; }
    }

    
}