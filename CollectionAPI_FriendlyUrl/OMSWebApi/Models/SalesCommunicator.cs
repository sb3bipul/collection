﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMSWebApi.Models
{
    public class SalesCommunicator
    {
        public int WeekNo { get; set; }
        public string Year {get;set;}
        public int Srl { get; set; }
        public DateTime StartDate{get;set;}
        public DateTime EndDate {get;set;}
        public string PdfFilePath {get;set;}
        public string FileName {get;set;}
        public string UserID{get;set;}
        public bool IsActive {get;set;}
        public string Comments {get;set;}
        public string FileData {get;set;}
        public string FileType {get;set;}
        public DateTime SentDate {get;set;} 
    }
}