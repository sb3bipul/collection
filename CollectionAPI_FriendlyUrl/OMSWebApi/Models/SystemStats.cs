﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace OMSWebApi.Models
{
    public class SystemStats
    {
        public int TrackID { get; set; }
        public string UserID { get; set; }
        public string ControlType { get; set; }
        public string ActionName { get; set; }
        public DateTime ActionDate { get; set; }
        public string DeviceType { get; set; }
        public string Value { get; set; }
        public string PageURL { get; set; }
        public string PageName { get; set; }
        public string ControlID { get; set; }
        public string ControlName { get; set; }
        public string BrowserName { get; set; }
        public string BrowserVersion { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

        public string ApplicationName { get; set; }
        public string ReleaseVersion { get; set; }

      //public List<SystemStats> systemStat { get; set; }
        public String systemStat { get; set; }
    }
    
    public class RootObject
    {
        public List<SystemStats> stats { get; set; }
    }
}

