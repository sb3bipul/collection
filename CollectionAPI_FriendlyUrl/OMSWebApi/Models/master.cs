﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMSWebApi.Models
{
    public class master
    {
        public string CompanyId { get; set; }
        public string LanguageId { get; set; }
        public string WarehouseId { get; set; }
        public string CatalogId { get; set; }
        public string SearchText { get; set; }
        public string SalesmanId { get; set; }
        public string BillingId { get; set; }

    }
    public class CustomerLst
    {
        public string UserID { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public string ContactNo { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Status { get; set; }
        public string CreditStatus { get; set; }
        public string CustomerBalance { get; set; }
        public string CUSTOMERBALANCEOVER30 { get; set; }
        public string BrokerID { get; set; }
        public string BillingCo { get; set; }
        public string WarehouseID { get; set; }
        public string EmailId { get; set; }
        public string Country { get; set; }
        public string TaxGroup { get; set; }
        public string ContractNo { get; set; }
    }
    public class BrokerDetails
    {
        public string CompanyId { get; set; }
        public string BrokerId { get; set; }
        public string BillingCO { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string PhotoName { get; set; }
        public string PhotoBinaryData { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
    }
    public class Brand
    {
        public string VendorCode { get; set; }
        public string Name { get; set; }
        public string ImageName { get; set; }
        public string CompanyID { get; set; }
    }

    public class VendorGroup
    {
        public string VendorId { get; set; }
        public string VendorGroupId { get; set; }
        public string GroupDescription { get; set; }
        public string CompanyID { get; set; }

    }
    public class ActivityLog
    {
        public string CompanyId { get; set; }
        public string BrokerId { get; set; }

        public string ActivityLogType { get; set; }
    }
    
}