﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using OMSWebApi.Providers;
using OMSWebApi.Models;

[assembly: OwinStartup(typeof(OMSWebApi.Startup))]

namespace OMSWebApi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
           try
           {
               ConfigureOAuth(app);
               ///
               HttpConfiguration config = new HttpConfiguration();
               WebApiConfig.Register(config);
               app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
               app.UseWebApi(config);
               ///
           }
           catch(Exception ex)
           { 
           
           }
            

        }

        /// rks
        public void ConfigureOAuth(IAppBuilder app)
        {
            /// rk 
            // Configure the db context and user manager to use a single instance per request
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            ///
            /*********************************************************************/
            //The Oauth provider that is defined is set here to allow the token to be passed in the query string.  
            /*********************************************************************/
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions()
            {
                Provider = new QueryStringOAuthBearerProvider("access_token")
            });


            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(365),
                Provider = new SimpleAuthorizationServerProvider()
            };
            // Token Generation
            app.UseOAuthAuthorizationServer(OAuthServerOptions);


        }
        ///
    }
}
