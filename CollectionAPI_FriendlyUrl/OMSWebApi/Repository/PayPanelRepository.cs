﻿using OMSWebApi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Web;
using System.Web.Routing;

namespace OMSWebApi.Repository
{
    public class PayPanelRepository
    {
        DBConnection dbConnectionObj = null;
        SqlCommand SqlCmd = null;
        DataToJson dtToJson = new DataToJson();
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //static void RegisterRoutes(RouteCollection routes)
        //{
        //    routes.MapPageRoute("paypanelpayment", "Pages/{PageName}.aspx", "~/payment.aspx");
        //}
        public string InsertPayPanelTempdata(PayPanelModel PayPanel)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            Guid guid = Guid.NewGuid();
            DataTable DtFinel = null;
            DataTable DtError = null;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_Insert_PayPanelData, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@ID", guid);
                SqlCmd.Parameters.AddWithValue("@CustomerName", string.IsNullOrEmpty(PayPanel.customer_name) ? DBNull.Value : (object)PayPanel.customer_name);
                SqlCmd.Parameters.AddWithValue("@Amount", string.IsNullOrEmpty(PayPanel.amount) ? DBNull.Value : (object)PayPanel.amount);
                SqlCmd.Parameters.AddWithValue("@Currency", string.IsNullOrEmpty(PayPanel.currency) ? DBNull.Value : (object)PayPanel.currency);
                SqlCmd.Parameters.AddWithValue("@EmailId", string.IsNullOrEmpty(PayPanel.email) ? DBNull.Value : (object)PayPanel.email);
                SqlCmd.Parameters.AddWithValue("@PhoneNo", string.IsNullOrEmpty(PayPanel.mobile) ? DBNull.Value : (object)PayPanel.mobile);
                SqlCmd.Parameters.AddWithValue("@OrderNo", string.IsNullOrEmpty(PayPanel.order_increment_id) ? DBNull.Value : (object)PayPanel.order_increment_id);
                

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                if (Convert.ToString(dt.Rows[0]["ID"]) != null && Convert.ToString(dt.Rows[0]["ID"]) !="")
                {
                    byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(Convert.ToString(dt.Rows[0]["ID"]));
                    string encrypted = Convert.ToBase64String(b);
                    //return encrypted;
                    string url = ("http://localhost:50313/PaymentWebForm/" + "payment?id=" + encrypted);
                    DtFinel = new DataTable();
                    DtFinel.Columns.Add("responsecode", typeof(String));
                    DtFinel.Columns.Add("url", typeof(String));
                    DtFinel.Columns.Add("responsemsg", typeof(String));

                    DataRow dr = DtFinel.NewRow();
                    dr[0] = "200";
                    dr[1] = url;
                    dr[2] = "Payment requested successfully";
                    DtFinel.Rows.Add(dr);
                    jsonString = dtToJson.convertDataTableToJson(DtFinel, "InsertPaymentInfo", true);
                }
                else
                {
                    DtError = new DataTable();
                    DtError.Columns.Add("responsecode", typeof(String));
                    DtError.Columns.Add("url", typeof(String));
                    DtError.Columns.Add("responsemsg", typeof(String));

                    DataRow dr = DtError.NewRow();
                    dr[0] = "200";
                    dr[1] = DBNull.Value;
                    dr[2] = "Order_increment_id already exits";
                    DtError.Rows.Add(dr);
                    jsonString = dtToJson.convertDataTableToJson(DtError, "InsertPaymentInfo", true);
                }
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertPaymentInfo", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
                DtError = null;
                DtFinel = null;
            }
            return null;
        }

        //public DataTable CustomFields()
        //{
        //    DataTable dt = new DataTable();
        //    dt.Columns.Add("FieldName", typeof(String));
        //    dt.Columns.Add("FieldType", typeof(String));
        //    dt.Columns.Add("FieldValue", typeof(String));

        //    dt.Rows.Add()
        //    return dt;
        //}
    }
}