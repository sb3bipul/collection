﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using OMSWebApi.Models;
using System.Xml.Serialization;
using Newtonsoft.Json.Linq;
using System.Data.OleDb;
using System.Configuration;

namespace OMSWebApi.Repository
{
    public class BackRoomInventoryRepository
    {

        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        DBConnection dbConnectionObj = null;

        SqlCommand SqlCmd = null;
        DataToJson dtToJson = new DataToJson();


        /// <summary>
        /// Get/Insert Back Room Inventory list
        /// </summary>       
        /// <returns></returns>
        public string getBackRoomInventoryList(int companyId, String department, String itemCode, int caseQuantity, int unitQuantity, int catalogId, String customerId, String brokerId, String languageId, String wharehouseId, bool isPickup, String comments, String action, String EOR, String result, DateTime deliveryDate, String clientPONumber, String promoCode, String day, String notDay, Decimal amountCollected, bool isRestricted, int QOH, decimal CasePrice, decimal RetailPrice, int UnitForCasePrice, decimal PromoAmount, bool IsPromoItem, int PromoMinQuantity, decimal OriginalCasePrice)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcBackRoomInventoryList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CustomerId", customerId);
                SqlCmd.Parameters.AddWithValue("@SalesmanID", brokerId);
                SqlCmd.Parameters.AddWithValue("@CompanyID", companyId);
                SqlCmd.Parameters.AddWithValue("@Dept", department);
                SqlCmd.Parameters.AddWithValue("@ProductCode", itemCode);
                SqlCmd.Parameters.AddWithValue("@Cases", caseQuantity);
                SqlCmd.Parameters.AddWithValue("@Units", unitQuantity);
                SqlCmd.Parameters.AddWithValue("@LanguageID", languageId);
                SqlCmd.Parameters.AddWithValue("@CatalogId", catalogId);
                SqlCmd.Parameters.AddWithValue("@WHID", wharehouseId);
                SqlCmd.Parameters.AddWithValue("@Pickup", isPickup);
                SqlCmd.Parameters.AddWithValue("@Comments", comments);
                SqlCmd.Parameters.AddWithValue("@Action", action);
                SqlCmd.Parameters.AddWithValue("@EOR", EOR);
                SqlCmd.Parameters.AddWithValue("@DeliveryDate", deliveryDate);
                SqlCmd.Parameters.AddWithValue("@ClientPONumber", clientPONumber);
                SqlCmd.Parameters.AddWithValue("@PromoCode", promoCode);
                SqlCmd.Parameters.AddWithValue("@Day", day);
                SqlCmd.Parameters.AddWithValue("@AmountCollected", amountCollected);
                SqlCmd.Parameters.AddWithValue("@NotDay", notDay);
                SqlCmd.Parameters.AddWithValue("@IsRestricted", isRestricted);
                SqlCmd.Parameters.AddWithValue("@QOH", QOH);
                SqlCmd.Parameters.AddWithValue("@CasePrice", CasePrice);
                SqlCmd.Parameters.AddWithValue("@RetailPrice", RetailPrice);
                SqlCmd.Parameters.AddWithValue("@UnitForCasePrice", UnitForCasePrice);

                SqlCmd.Parameters.AddWithValue("@PromoAmount", PromoAmount);
                SqlCmd.Parameters.AddWithValue("@IsPromoItem", IsPromoItem);
                SqlCmd.Parameters.AddWithValue("@PromoMinQuantity", PromoMinQuantity);
                SqlCmd.Parameters.AddWithValue("@OriginalCasePrice", OriginalCasePrice);

                SqlCmd.Parameters.AddWithValue("@MESSAGE", "");

                //  SqlCmd.Parameters.AddWithValue("@Comments", "");

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "getBackRoomInventoryList", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "getBackRoomInventoryList", false);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// Delete Back room Inventory Item
        /// </summary>
        /// <returns></returns>
        public string deleteBackRoomInventoryItem(string itemCode, string customerId, string brokerId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcDeleteBarckRoomItem, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@ItemCode", itemCode);
                SqlCmd.Parameters.AddWithValue("@CustomerId", customerId);
                SqlCmd.Parameters.AddWithValue("@BrokerId", brokerId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "deleteBackRoomInventoryItem", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "deleteBackRoomInventoryItem", false);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }



        /// <summary>
        /// Delete All Back Room Inventory by Customer ID
        /// </summary>
        /// <returns></returns>
        public string deleteAllBackRoomItems(string customerId, string brokerId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcDeleteAllBackRoomItems, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CustomerId", customerId);
                SqlCmd.Parameters.AddWithValue("@BrokerId", brokerId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "deleteAllBackRoomItems", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "deleteAllBackRoomItems", false);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }


        /// <summary>
        /// update Item Case Quantity for Back room inventory
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="EOR"></param>
        /// <returns></returns>
        public string updateItemCaseQuantity(int caseQuantity, string itemCode, string customerId, string brokerId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcUpdateItemCaseBackRoomQuantity, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CaseQuantity", caseQuantity);
                SqlCmd.Parameters.AddWithValue("@ProductCode", itemCode);
                SqlCmd.Parameters.AddWithValue("@CustomerId", customerId);
                SqlCmd.Parameters.AddWithValue("@BrokerId", brokerId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "updateItemCaseQuantity", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "updateItemCaseQuantity", false);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        } 
    }
}