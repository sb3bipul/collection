﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using OMSWebApi.Models;
using System.Xml.Serialization;
using Newtonsoft.Json.Linq;

namespace OMSWebApi.Repository
{
    public class SystemStatsRepository
    {
        DBConnection dbConnectionObj = null;
        SqlCommand SqlCmd = null;
        DataToJson dtToJson = new DataToJson();

        public string InsertSystemStats(string UserID, string ControlType, string ActionName, DateTime ActionDate, string DeviceType, string Value, string PageURL, string PageName, string ControlID, string ControlName, string BrowserName, string BrowserVersion)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcInsertUserActivity, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                if (UserID == null)
                    UserID = "0";
                if (Value == null)
                    Value = "";

                SqlCmd.Parameters.AddWithValue("@UserID", UserID);
                SqlCmd.Parameters.AddWithValue("@ControlType", ControlType);
                SqlCmd.Parameters.AddWithValue("@ActionName", ActionName);
                SqlCmd.Parameters.AddWithValue("@ActionDate", ActionDate);
                SqlCmd.Parameters.AddWithValue("@DeviceType", DeviceType);
                SqlCmd.Parameters.AddWithValue("@Value", Value);
                SqlCmd.Parameters.AddWithValue("@PageURL", PageURL);
                SqlCmd.Parameters.AddWithValue("@PageName", PageName);
                SqlCmd.Parameters.AddWithValue("@ControlID", ControlID);
                SqlCmd.Parameters.AddWithValue("@ControlName", ControlName);
                SqlCmd.Parameters.AddWithValue("@BrowserName", BrowserName);
                SqlCmd.Parameters.AddWithValue("@BrowserVersion", BrowserVersion);
 
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertSystemStats", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertSystemStats", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        // Get System Stats for all the user
        public string GetSystemStats(string UserID, DateTime FromDate, DateTime ToDate)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetSystemStats, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                if (UserID == null || UserID == "0")
                    UserID = "%%";

                SqlCmd.Parameters.AddWithValue("@UserID", UserID);
                SqlCmd.Parameters.AddWithValue("@FromDate", FromDate);
                SqlCmd.Parameters.AddWithValue("@ToDate", ToDate);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetSystemStats", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetSystemStats", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }


        //  Get Release Updates By Version
        public string GetReleaseUpdatesByVersion(string ReleaseVersion, string ApplicationName)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetReleaseUpdatesByVersion, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@releaseVersion", ReleaseVersion);
                SqlCmd.Parameters.AddWithValue("@applicationName", ApplicationName);
       

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetReleaseUpdatesByVersion", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetReleaseUpdatesByVersion", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

    }
}