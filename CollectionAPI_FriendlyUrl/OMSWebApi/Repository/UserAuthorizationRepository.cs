﻿using OMSWebApi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace OMSWebApi.Repository
{
    public class UserAuthorizationRepository
    {
        //Here is the once-per-class call to initialize the log object
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        
        DBConnectionOAuth dbConnectionObj = null;
        SqlCommand com = null;
        SqlDataAdapter da = null;
        DataTable dt = null;

        public int GetRoleIdFromUserId(string userId)
        {
            DataTable dt = null;
            int roleId = 0;

            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocGetRoleIdFromUserId, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@userId", userId);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                foreach (DataRow dr in dt.Rows)
                {
                    roleId = Convert.ToInt32(dr["RoleId"]);
                }

                log.Debug("Debug logging: AuthorizationWebApi.Repository -> UserAuthorizationRepository -> GetRoleIdFromUserId");

                return roleId; 
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                dbConnectionObj = null;
                // custList = null;
                com = null;
                da = null;
                dt = null;
            }

            return roleId;
        }

        public string GetUserFullNameFromUserId(string userId)
        {
            DataTable dt = null;
            string userFName = string.Empty;

            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocGetUserFullName, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@userId", userId);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                foreach (DataRow dr in dt.Rows)
                {
                    userFName = dr["UserFullName"].ToString();
                }

                log.Debug("Debug logging: AuthorizationWebApi.Repository -> UserAuthorizationRepository -> GetRoleIdFromUserId");

                return userFName;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                dbConnectionObj = null;
                // custList = null;
                com = null;
                da = null;
                dt = null;
            }

            return userFName;
        }

        public UserProfile GetUserProfile(string userId, string CompanyID)
        {
            UserProfile userObj = null;

            try
            {
                userObj = new UserProfile();
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocGetUserMyProfile, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@userId", userId);
                com.Parameters.AddWithValue("@CompanyID", CompanyID);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                foreach (DataRow dr in dt.Rows)
                {
                    userObj.UserName = dr["UserName"].ToString();
                    userObj.UserFullName = dr["UserFullName"].ToString();
                    userObj.CompanyId = dr["CompanyId"].ToString();
                    userObj.CompanyName = dr["CompanyName"].ToString();
                    userObj.Role = dr["Role"].ToString();
                    userObj.Email = dr["Email"].ToString();
                    userObj.Phone = dr["PhoneNumber"].ToString();
                    userObj.BrokerPicName = dr["BrokerPicName"].ToString();
                    userObj.UserType = dr["UserType"].ToString();
                    if (dr["IsNewReleaseFlag"] is DBNull)
                        userObj.IsNewReleaseFlag = false;
                    else
                        userObj.IsNewReleaseFlag = Convert.ToBoolean(dr["IsNewReleaseFlag"]);
                    userObj.LoginCompanyId = dr["LoginCompanyId"].ToString();
                }

                log.Debug("Debug logging: AuthorizationWebApi.Repository -> UserAuthorizationRepository -> GetRoleIdFromUserId");

                return userObj;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                dbConnectionObj = null;
                // custList = null;
                com = null;
                da = null;
                dt = null;
            }

            return userObj;
        }

        public bool UpdateUserProfile(UserProfile userObj)
        {
            bool success = false;
            SqlDataReader reader = null;

            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocUpdateMyProfile, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@UserName", userObj.UserName);
                com.Parameters.AddWithValue("@Email", userObj.Email);
                com.Parameters.AddWithValue("@Phone", userObj.Phone);
                com.Parameters.AddWithValue("@Comments", "Phone and Email got updated.");

                dbConnectionObj.ConnectionOpen();
                reader = com.ExecuteReader();
                success = true;

                reader.Close();
                dbConnectionObj.ConnectionClose();
            }
            catch (Exception ex)
            {
                success = false;
                log.Error("Error: " + ex);
                throw ex;
            }
            finally
            {
                dbConnectionObj = null;
                com = null;
                da = null;
                dt = null;
            }

            return success;
        }

        public bool UpdateNewUser(CreateNewUserBindingModel userObj)
        {
            bool success = false;
            SqlDataReader reader = null;

            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocUpdateUser, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@UserName", userObj.UserName.Trim());
                com.Parameters.AddWithValue("@UserFullName", userObj.UserFullName.Trim());
                com.Parameters.AddWithValue("@Domain", userObj.Domain.Trim());
                com.Parameters.AddWithValue("@UserTypeCode", userObj.UserTypeCode.Trim());

                dbConnectionObj.ConnectionOpen();
                reader = com.ExecuteReader();
                success = true;

                reader.Close();
                dbConnectionObj.ConnectionClose();
            }
            catch (Exception ex)
            {
                success = false;
                log.Error("Error: " + ex);
                throw ex;
            }
            finally
            {
                dbConnectionObj = null;
                com = null;
                da = null;
                dt = null;
            }

            return success;
        }

        public bool UpdateUpdatedBy(string userName)
        {
            bool success = false;
            SqlDataReader reader = null;
            string comments = "Password is been reset.";

            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocUpdateUpdatedBy, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@userName", userName);
                com.Parameters.AddWithValue("@comments", comments);

                dbConnectionObj.ConnectionOpen();
                reader = com.ExecuteReader();
                success = true;

                reader.Close();
                dbConnectionObj.ConnectionClose();
            }
            catch (Exception ex)
            {
                success = false;
                log.Error("Error: " + ex);
                throw ex;
            }
            finally
            {
                dbConnectionObj = null;
                com = null;
                da = null;
                dt = null;
            }

            return success;
        }


        public string GetUserIDFromUserName(string userName)
        {
            DataTable dt = null;
            string userId = string.Empty;

            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocGetUserIdFromUserName, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@userName", userName);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                foreach (DataRow dr in dt.Rows)
                {
                    userId = dr["Id"].ToString();
                }

                log.Debug("Debug logging: AuthorizationWebApi.Repository -> UserAuthorizationRepository -> GetUserIDFromUserName");

                return userId;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                dbConnectionObj = null;
                // custList = null;
                com = null;
                da = null;
                dt = null;
            }

            return userId;
        }

        public bool IfUserExistInOAuthDB(string userName, string email)
        {
            DataTable dt = null;
            bool isExist = false;

            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocGetIfUserOrEmailExist, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@userName", userName);
                com.Parameters.AddWithValue("@email", email);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                foreach (DataRow dr in dt.Rows)
                {
                    if (Convert.ToInt32(dr[0]) == 1)
                        isExist = true;
                }

                log.Debug("Method -> IfUserExistInOAuthDB");

                return isExist;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                dbConnectionObj = null;
                // custList = null;
                com = null;
                da = null;
                dt = null;
            }

            return isExist;
        }

        public bool IfUserValidEmailOAuthDB(string userName, string email)
        {
            DataTable dt = null;
            bool isExist = false;

            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocCheckIfValidEmailExist, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@userName", userName);
                com.Parameters.AddWithValue("@email", email);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                foreach (DataRow dr in dt.Rows)
                {
                    if (Convert.ToInt32(dr[0]) == 1)
                        isExist = true;
                }

                log.Debug("Method -> IfUserExistInOAuthDB");

                return isExist;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                dbConnectionObj = null;
                // custList = null;
                com = null;
                da = null;
                dt = null;
            }

            return isExist;
        }

        //  Reset Release Update flag
        public string ResetReleaseUpdateFlag(string userId)
        {
            DataTable dt = null;
            int roleId = 0;

            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocResetReleaseUpdateFlag, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@userId", userId);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose(); 

                log.Debug("Debug logging: AuthorizationWebApi.Repository -> UserAuthorizationRepository -> ResetReleaseUpdateFlag");

                return "Updated";
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                dbConnectionObj = null;
                // custList = null;
                com = null;
                da = null;
                dt = null;
            }

            return "Failed";
        }

        public UserLoginByEmail GetUserIdByEmail(string emailId)
        {
            UserLoginByEmail userObj = null;

            try
            {
                userObj = new UserLoginByEmail();
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SProcGetUserIdByEmail, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@EmailId", emailId);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                foreach (DataRow dr in dt.Rows)
                {
                    userObj.UserId = dr["UserName"].ToString();
                    userObj.UserName = dr["UserFullName"].ToString();
                    userObj.UserPassword = dr["PasswordHash"].ToString();
                    userObj.CompanyId = dr["CompanyID"].ToString();
                }

                log.Debug("Debug logging: AuthorizationWebApi.Repository -> UserAuthorizationRepository -> GetUserIdByEmail");

                return userObj;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                dbConnectionObj = null;
                com = null;
                da = null;
                dt = null;
            }

            return userObj;
        }

    }
}