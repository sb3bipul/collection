﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using OMSWebApi.Models;
using System.Xml.Serialization;
using Newtonsoft.Json.Linq;
using System.Data.OleDb;
using System.Web.Configuration;
using OMSWebApi.Controllers;
using OMSWebApi.Providers;
using OMSWebApi.Results;
using OMSWebApi.Repository;
using System.Net.Mail;
using System.Globalization;
using System.Text;
using System.Web.Script.Serialization;

namespace OMSWebApi.Repository
{
    public class CustProfileRepository
    {
        DBConnection dbConnectionObj = null;
        SqlCommand SqlCmd = null;
        DataToJson dtToJson = new DataToJson();
        SqlDataAdapter da = null;
        String CustomerPortalImages = ApiConstant.CustomerPortalImage;
        /// <summary>
        /// Get Cust Profile List
        /// </summary>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        public DataTable GetCustProfileData(string SalesmanId, String SearchTxt, String CompanyID)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            //string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SPGetCustomerProfileList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@SalesmanId", SalesmanId);
                SqlCmd.Parameters.AddWithValue("@SearchTxt", SearchTxt);
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyID);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                // = dtToJson.convertDataTableToJson(dt, "GetCustProfileData", true);
                return dt;

            }
            catch (Exception ex)
            {
                //jsonString = dtToJson.convertDataTableToJson(dt, "GetCustProfileData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                //dt = null;
            }
            return null;
        }

        public DataTable SaveCustomerProfileData(CustProfileViewModel objCustProfVM)
        { 
            SqlDataAdapter da = null;
            DataTable dt = null;
            //String jsonString, ret = String.Empty;
            String CustID = "", CompID = String.Empty, WeekDay = String.Empty, TimeFrom=String.Empty, TimeTo=String.Empty;
            int DayHrID = 0, ReceiveHrID =0;
            DateTime CreatedDate;
            try 
            {
                //    Day hour item data
                IEnumerable<DayHourItem> dayHourLst = objCustProfVM.dayHourItems;
                DataTable dtlCustProf = new DataTable();
                dtlCustProf.Columns.Add("CustomerID", typeof(string));
                dtlCustProf.Columns.Add("CompanyID", typeof(string));
                dtlCustProf.Columns.Add("DayHoursID", typeof(Int32));
                dtlCustProf.Columns.Add("Week_Day", typeof(string));
                dtlCustProf.Columns.Add("CreatedDate", typeof(DateTime));
                dtlCustProf.Columns.Add("TimeFrom", typeof(string));
                dtlCustProf.Columns.Add("TimeTo", typeof(string));
                foreach (var itms in dayHourLst)
                {
                    dtlCustProf.Rows.Add(objCustProfVM.CustomerID, objCustProfVM.CompanyID, itms.DayHoursID, itms.WeekDay, itms.CreatedDate == null || itms.CreatedDate == Convert.ToDateTime("1/1/0001 12:00AM") ? DateTime.Now : itms.CreatedDate, itms.TimeFrom, itms.TimeTo);
                    if (String.IsNullOrWhiteSpace(CustID))
                    {
                        CustID = objCustProfVM.CustomerID;
                        CompID = objCustProfVM.CompanyID;
                        DayHrID = itms.DayHoursID;
                        WeekDay = itms.WeekDay;
                        CreatedDate = itms.CreatedDate == null || itms.CreatedDate == Convert.ToDateTime("1/1/0001 12:00AM") ? DateTime.Now : itms.CreatedDate;//itms.CreatedDate;
                        TimeFrom = itms.TimeFrom;
                        TimeTo = itms.TimeTo;
                    }
                }
                CustProfileViewModel cstProVMdl = new CustProfileViewModel();
                cstProVMdl.dayHourItems = objCustProfVM.dayHourItems;
                DataSet dsCustProf = new DataSet();
                dsCustProf.Tables.Add(dtlCustProf);
                String dayHourItems = dsCustProf.GetXml();

                //    Receving hour item data
                IEnumerable<ReceivingHourItem> receivingHourLst = objCustProfVM.receivingHourItems;
                DataTable dtlCustProfReceiveHr = new DataTable();
                dtlCustProfReceiveHr.Columns.Add("CustomerID", typeof(string));
                dtlCustProfReceiveHr.Columns.Add("CompanyID", typeof(string));
                dtlCustProfReceiveHr.Columns.Add("ReceiveHoursID", typeof(Int32));
                dtlCustProfReceiveHr.Columns.Add("Week_Day", typeof(string));
                dtlCustProfReceiveHr.Columns.Add("CreatedDate", typeof(DateTime));
                dtlCustProfReceiveHr.Columns.Add("TimeFrom", typeof(string));
                dtlCustProfReceiveHr.Columns.Add("TimeTo", typeof(string));
                foreach (var itms in receivingHourLst)
                {
                    dtlCustProfReceiveHr.Rows.Add(objCustProfVM.CustomerID, objCustProfVM.CompanyID, itms.ReceiveHoursID, itms.WeekDay, itms.CreatedDate == null || itms.CreatedDate == Convert.ToDateTime("1/1/0001 12:00AM") ? DateTime.Now : itms.CreatedDate, itms.TimeFrom, itms.TimeTo);
                    if (String.IsNullOrWhiteSpace(CustID))
                    {
                        CustID = objCustProfVM.CustomerID;
                        CompID = objCustProfVM.CompanyID;
                        ReceiveHrID = itms.ReceiveHoursID;
                        WeekDay = itms.WeekDay;
                        CreatedDate = itms.CreatedDate == null || itms.CreatedDate == Convert.ToDateTime("1/1/0001 12:00AM") ? DateTime.Now : itms.CreatedDate;//itms.CreatedDate;
                        TimeFrom = itms.TimeFrom;
                        TimeTo = itms.TimeTo;
                    }
                }
                CustProfileViewModel cstProRecHrVMdl = new CustProfileViewModel();
                cstProRecHrVMdl.receivingHourItems = objCustProfVM.receivingHourItems;
                DataSet dslCustProfReceiveHr = new DataSet();
                dslCustProfReceiveHr.Tables.Add(dtlCustProfReceiveHr);
                String receivingHourItems = dslCustProfReceiveHr.GetXml();

                String success = string.Empty;
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SPUpdateAddCustomerPortalM, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CustomerId", CustID);
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompID);
                SqlCmd.Parameters.AddWithValue("@SalesmanId", objCustProfVM.SalesmanId);
                SqlCmd.Parameters.AddWithValue("@Name", objCustProfVM.Name);
                SqlCmd.Parameters.AddWithValue("@Street", objCustProfVM.Street);
                SqlCmd.Parameters.AddWithValue("@City", objCustProfVM.City);
                SqlCmd.Parameters.AddWithValue("@State", objCustProfVM.State);
                SqlCmd.Parameters.AddWithValue("@Zip", objCustProfVM.Zip);
                SqlCmd.Parameters.AddWithValue("@Country", objCustProfVM.Country);
                SqlCmd.Parameters.AddWithValue("@Email", objCustProfVM.EmailID);
                SqlCmd.Parameters.AddWithValue("@ContactNo", objCustProfVM.ContactNo);
                SqlCmd.Parameters.AddWithValue("@Website", objCustProfVM.Organization);
                SqlCmd.Parameters.AddWithValue("@Latitude", objCustProfVM.Latitude);
                SqlCmd.Parameters.AddWithValue("@Longitude", objCustProfVM.Longitude);
                SqlCmd.Parameters.AddWithValue("@Zone", objCustProfVM.Zone);
                SqlCmd.Parameters.AddWithValue("@StoreEntranceLocation", objCustProfVM.StoreEntranceLocation);
                SqlCmd.Parameters.AddWithValue("@StoreEntranceLatitude", objCustProfVM.StoreEntranceLatitude);
                SqlCmd.Parameters.AddWithValue("@StoreEntranceLongitude", objCustProfVM.StoreEntranceLongitude);
                SqlCmd.Parameters.AddWithValue("@ReceivingEntranceLocation", objCustProfVM.ReceivingEntranceLocation);
                SqlCmd.Parameters.AddWithValue("@ReceivingEntranceLatitude", objCustProfVM.ReceivingEntranceLatitude);
                SqlCmd.Parameters.AddWithValue("@ReceivingEntranceLongitude", objCustProfVM.ReceivingEntranceLongitude);
                SqlCmd.Parameters.AddWithValue("@StoreLogoImgName", objCustProfVM.StoreLogoImgName);
                SqlCmd.Parameters.AddWithValue("@StoreLogoImgPath", objCustProfVM.StoreLogoImgPath);
                SqlCmd.Parameters.AddWithValue("@LocalCustomerID", objCustProfVM.LocalCustomerID);
                SqlCmd.Parameters.AddWithValue("@XML_DayHours", dayHourItems);
                SqlCmd.Parameters.AddWithValue("@XML_ReceivingHours", receivingHourItems);
                //SqlCmd.Parameters.AddWithValue("@MESSAGE", 1);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                //jsonString = dtToJson.convertDataTableToJson(dt, "GetCustProfileData", true);
                //return jsonString;

            }
            catch (Exception ex)
            {
                //jsonString = dtToJson.convertDataTableToJson(dt, "GetCustProfileData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                //dt = null;
            }
            return dt;
        }

        public string GetStateListData(String CompanyIDs)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SPgetStateList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetStateListData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetStateListData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public DataTable GetSectionImgList()
        {
            Common common = new Common();
            DataTable dt = null;
            List<SectionImg> secImgObj = null;
            DataTable dtl = new DataTable();
            try
            {
                secImgObj = new List<SectionImg>();
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SPGetSectionImgList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@CompanyID", 2);
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            secImgObj.Add(new SectionImg
                            {
                                CustomerID = Convert.ToString(dt.Rows[i]["CustomerID"]),
                                CompanyID = Convert.ToString(dt.Rows[i]["CompanyID"]),
                                SectionID = Convert.ToInt32(dt.Rows[i]["SectionID"]),
                                UploadShopName = Convert.ToString(dt.Rows[i]["UploadShopName"]),
                                UploadShopImagePath = Convert.ToString(dt.Rows[i]["UploadShopImagePath"]),
                                UploadShopImageName = Convert.ToString(dt.Rows[i]["UploadShopImageName"]),
                                SrlNo = Convert.ToString(dt.Rows[i]["SrlNo"])
                            });
                        }
                    }
                }
                OMSWebApi.Models.CompanyModels.ListtoDataTable lsttodt = new OMSWebApi.Models.CompanyModels.ListtoDataTable();
                dtl = lsttodt.ToDataTable(secImgObj);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnectionObj = null;
                secImgObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return dtl;
        }

        public DataTable GetShopOutsideImgList()
        {
            Common common = new Common();
            DataTable dt = null;
            List<ShopOutsideImg> outsideImgObj = null;
            DataTable dtl = new DataTable();
            try
            {
                outsideImgObj = new List<ShopOutsideImg>();
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SPGetShopOutsideImgList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@CompanyID", 2);
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            outsideImgObj.Add(new ShopOutsideImg
                            {
                                CustomerID = Convert.ToString(dt.Rows[i]["CustomerID"]),
                                CompanyID = Convert.ToString(dt.Rows[i]["CompanyID"]),
                                OutsideID = Convert.ToInt32(dt.Rows[i]["OutsideID"]),
                                UploadShopName = Convert.ToString(dt.Rows[i]["UploadShopName"]),
                                UploadShopImagePath = Convert.ToString(dt.Rows[i]["UploadShopImagePath"]),
                                UploadShopImageName = Convert.ToString(dt.Rows[i]["UploadShopImageName"]),
                                SrlNo = Convert.ToString(dt.Rows[i]["SrlNo"])
                            });
                        }
                    }
                }
                OMSWebApi.Models.CompanyModels.ListtoDataTable lsttodt = new OMSWebApi.Models.CompanyModels.ListtoDataTable();
                dtl = lsttodt.ToDataTable(outsideImgObj);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnectionObj = null;
                outsideImgObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return dtl;
        }

        public DataTable GetShopInsideImgList()
        {
            Common common = new Common();
            DataTable dt = null;
            List<ShopInsideImg> insideImgObj = null;
            DataTable dtl = new DataTable();
            try
            {
                insideImgObj = new List<ShopInsideImg>();
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SPGetShopInsideImgList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@CompanyID", 2);
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            insideImgObj.Add(new ShopInsideImg
                            {
                                CustomerID = Convert.ToString(dt.Rows[i]["CustomerID"]),
                                CompanyID = Convert.ToString(dt.Rows[i]["CompanyID"]),
                                InsideID = Convert.ToInt32(dt.Rows[i]["InsideID"]),
                                UploadShopName = Convert.ToString(dt.Rows[i]["UploadShopName"]),
                                UploadShopImagePath = Convert.ToString(dt.Rows[i]["UploadShopImagePath"]),
                                UploadShopImageName = Convert.ToString(dt.Rows[i]["UploadShopImageName"]),
                                SrlNo = Convert.ToString(dt.Rows[i]["SrlNo"])
                            });
                        }
                    }
                }
                OMSWebApi.Models.CompanyModels.ListtoDataTable lsttodt = new OMSWebApi.Models.CompanyModels.ListtoDataTable();
                dtl = lsttodt.ToDataTable(insideImgObj);
            }
            catch (Exception ex)
            { 
            }
            finally
            {
                dbConnectionObj = null;
                insideImgObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return dtl;
        }

        public DataTable GetCustomerDayHoursList(String CompanyID)
        {
            Common common = new Common();
            DataTable dt = null;
            List<DayHourItem> dayHoursObj = null;
            DataTable dtl = new DataTable();
            try
            {
                dayHoursObj = new List<DayHourItem>();
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SPGetCustomer_DayHoursList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@CompanyId", CompanyID);
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            dayHoursObj.Add(new DayHourItem
                            {
                                CustomerID = Convert.ToString(dt.Rows[i]["CustomerID"]),
                                CompanyID = Convert.ToString(dt.Rows[i]["CompanyID"]),
                                DayHoursID = Convert.ToInt32(dt.Rows[i]["DayHoursID"]),
                                WeekDay = Convert.ToString(dt.Rows[i]["WeekDay"]),
                                CreatedDate = Convert.ToDateTime(dt.Rows[i]["CreatedDate"]),
                                TimeFrom = Convert.ToString(dt.Rows[i]["TimeFrom"]),
                                TimeTo = Convert.ToString(dt.Rows[i]["TimeTo"]),
                                SrlNo = Convert.ToInt64(dt.Rows[i]["SrlNo"])
                            });
                        }
                    }
                }
                OMSWebApi.Models.CompanyModels.ListtoDataTable lsttodt = new OMSWebApi.Models.CompanyModels.ListtoDataTable();
                dtl = lsttodt.ToDataTable(dayHoursObj);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnectionObj = null;
                dayHoursObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return dtl;
        }

        public DataTable GetCustomerReceivingHoursList(String CompanyID)
        {
            Common common = new Common();
            DataTable dt = null;
            List<ReceivingHourItem> receivingHoursObj = null;
            DataTable dtl = new DataTable();
            try
            {
                receivingHoursObj = new List<ReceivingHourItem>();
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SPGetCustomerReceivingHoursList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@CompanyId", CompanyID);
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            receivingHoursObj.Add(new ReceivingHourItem
                            {
                                CustomerID = Convert.ToString(dt.Rows[i]["CustomerID"]),
                                CompanyID = Convert.ToString(dt.Rows[i]["CompanyID"]),
                                ReceiveHoursID = Convert.ToInt32(dt.Rows[i]["ReceiveHoursID"]),
                                WeekDay = Convert.ToString(dt.Rows[i]["WeekDay"]),
                                CreatedDate = Convert.ToDateTime(dt.Rows[i]["CreatedDate"]),
                                TimeFrom = Convert.ToString(dt.Rows[i]["TimeFrom"]),
                                TimeTo = Convert.ToString(dt.Rows[i]["TimeTo"]),
                                SrlNo = Convert.ToInt64(dt.Rows[i]["SrlNo"])
                            });
                        }
                    }
                }
                OMSWebApi.Models.CompanyModels.ListtoDataTable lsttodt = new OMSWebApi.Models.CompanyModels.ListtoDataTable();
                dtl = lsttodt.ToDataTable(receivingHoursObj);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnectionObj = null;
                receivingHoursObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return dtl;
        }

        public DataTable GetDayHoursByCustomerId(String CustomerId, String CompanyId)
        {
            Common common = new Common();
            DataTable dtl = new DataTable();
            DataTable dt = new DataTable();
            List<DayHourItem> dayHoursObj = null;
            try
            {
                dayHoursObj = new List<DayHourItem>();
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SPGetDayHours_ByCustomerId, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@CustomerId", CustomerId);
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);
                da = new SqlDataAdapter(SqlCmd);
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            dayHoursObj.Add(new DayHourItem
                            {
                                CustomerID = Convert.ToString(dt.Rows[i]["CustomerID"]),
                                CompanyID = Convert.ToString(dt.Rows[i]["CompanyID"]),
                                DayHoursID = Convert.ToInt32(dt.Rows[i]["DayHoursID"]),
                                WeekDay = Convert.ToString(dt.Rows[i]["WeekDay"]),
                                CreatedDate = Convert.ToDateTime(dt.Rows[i]["CreatedDate"]),
                                TimeFrom = Convert.ToString(dt.Rows[i]["TimeFrom"]),
                                TimeTo = Convert.ToString(dt.Rows[i]["TimeTo"]),
                                SrlNo = Convert.ToInt64(dt.Rows[i]["SrlNo"])
                            });
                        }
                    }
                }
                OMSWebApi.Models.CompanyModels.ListtoDataTable lsttodt = new OMSWebApi.Models.CompanyModels.ListtoDataTable();
                dtl = lsttodt.ToDataTable(dayHoursObj);
                //return dtl;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnectionObj = null;
                dayHoursObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return dtl;
        }

        public DataTable GetReceiveHoursByCustomerId(String CustomerId, String CompanyId)
        {
            Common common = new Common();
            DataTable dtl = new DataTable();
            DataTable dt = new DataTable();
            List<ReceivingHourItem> receiveHoursObj = null;
            try
            {
                receiveHoursObj = new List<ReceivingHourItem>();
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SPGetReceiveHours_ByCustomerId, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@CustomerId", CustomerId);
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);
                da = new SqlDataAdapter(SqlCmd);
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            receiveHoursObj.Add(new ReceivingHourItem
                            {
                                CustomerID = Convert.ToString(dt.Rows[i]["CustomerID"]),
                                CompanyID = Convert.ToString(dt.Rows[i]["CompanyID"]),
                                ReceiveHoursID = Convert.ToInt32(dt.Rows[i]["ReceiveHoursID"]),
                                WeekDay = Convert.ToString(dt.Rows[i]["WeekDay"]),
                                CreatedDate = Convert.ToDateTime(dt.Rows[i]["CreatedDate"]),
                                TimeFrom = Convert.ToString(dt.Rows[i]["TimeFrom"]),
                                TimeTo = Convert.ToString(dt.Rows[i]["TimeTo"]),
                                SrlNo = Convert.ToInt64(dt.Rows[i]["SrlNo"])
                            });
                        }
                    }
                }
                OMSWebApi.Models.CompanyModels.ListtoDataTable lsttodt = new OMSWebApi.Models.CompanyModels.ListtoDataTable();
                dtl = lsttodt.ToDataTable(receiveHoursObj);
                //return dtl;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnectionObj = null;
                receiveHoursObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return dtl;
        }

        public string SendEmail(String CustomerId, String CompanyId, String CustomerEmail, String Action, String Comments)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                clsEmail objMail;
                objMail = new clsEmail();
                objMail.MailFrom = Convert.ToString(ApiConstant.strSenderEmailId);
                objMail.MailTo = Convert.ToString(CustomerEmail);
                objMail.MailSubject = Action == "R" ? CustomerId + " Has Been Rejected. " : CustomerId + " Has Been Succeeded. ";
                objMail.MailBody = objMail.GenerateMailBody(Convert.ToString(CustomerId), Comments);
                int status = objMail.SendGeneralizedMail(Convert.ToString(ApiConstant.SMTPServer), Convert.ToString(ApiConstant.strSenderEmailId), Convert.ToString(ApiConstant.smtppasswd));
                dt = new DataTable();
                dt.Columns.Add("Result", typeof(string));
                if (status > 0)
                    dt.Rows.Add("Ok");
                else
                    dt.Rows.Add("Not Ok");
                //dbConnectionObj = new DBConnection();
                //SqlCmd = new SqlCommand(ApiConstant.SPGetCustomerProfileList, dbConnectionObj.ApiConnection);
                //SqlCmd.CommandType = CommandType.StoredProcedure;
                //SqlCmd.Parameters.AddWithValue("@SalesmanId", SalesmanId);
                //SqlCmd.Parameters.AddWithValue("@SearchTxt", SearchTxt);
                //da = new SqlDataAdapter(SqlCmd);
                //dt = new DataTable();
                //dbConnectionObj.ConnectionOpen();
                //da.Fill(dt);
                //dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "SendEmailData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "SendEmailData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }
        public DataTable SendEmailByTicket(String CustomerId, String CompanyId, String CustomerEmail, String OtherReason, String TicketReason,String fileName, String FilePath)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            int ret = 0;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcInsert_TicketDetails, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@CompanyID", 2);
                SqlCmd.Parameters.AddWithValue("@TicketReason", TicketReason);
                SqlCmd.Parameters.AddWithValue("@UploadFileName", fileName);
                SqlCmd.Parameters.AddWithValue("@UploadFilePath", FilePath);
                SqlCmd.Parameters.AddWithValue("@OtherReason", OtherReason);
                SqlCmd.Parameters.AddWithValue("@UserId", CustomerId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        clsEmail objMail;
                        objMail = new clsEmail();
                        objMail.MailFrom = Convert.ToString(ApiConstant.strSenderEmailId);
                        objMail.MailTo = Convert.ToString(CustomerEmail);
                        objMail.MailSubject = Convert.ToString(dt.Rows[0]["TicketId"]) + ", Ticket Has Been Generated By The " + CustomerId;
                        objMail.MailBody = objMail.GenerateMailBody(Convert.ToString(CustomerId), "TicketId : "+ Convert.ToString(dt.Rows[0]["TicketId"]) + ". Reason : "  + TicketReason);
                        ret = objMail.SendGeneralizedMail(Convert.ToString(ApiConstant.SMTPServer), Convert.ToString(ApiConstant.strSenderEmailId), Convert.ToString(ApiConstant.smtppasswd));
                        //dt.Columns.Add("Result", typeof(string));
                    }
                }
                // Convert Datatable to JSON string
                //jsonString = dtToJson.convertDataTableToJson(dt, "SendEmailByTicket", true);
                return dt;
            }
            catch (Exception ex)
            {
                //jsonString = dtToJson.convertDataTableToJson(dt, "SendEmailByTicket", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public DataTable SaveImageListAll(String CustId, String CompId, dynamic frmData )// String[] SectionSrl, String[] SectionNm, String[] OutsideSrl, String[] OutsideNm, String[] InsideSrl, String[] InsideNm)
        {
            Common common = new Common();
            DataTable dt = null;
            List<ImgReuslt> ImgObj = null;
            DataTable dtl = new DataTable();
            try
            {
                ImgObj = new List<ImgReuslt>();
                
                //     Outside
                foreach (var ot in frmData.outsideDet)
                {
                    String OutsideImgName = ot.OutsideImgName;
                    if (!String.IsNullOrEmpty(OutsideImgName))
                    {
                        dbConnectionObj = new DBConnection();
                        SqlCmd = new SqlCommand(ApiConstant.SPInsertUploadShopOutside, dbConnectionObj.ApiConnection);
                        SqlCmd.CommandType = CommandType.StoredProcedure;
                        SqlCmd.Parameters.AddWithValue("@CustomerId", CustId);
                        SqlCmd.Parameters.AddWithValue("@CompanyID", CompId);
                        SqlCmd.Parameters.AddWithValue("@UploadShopName", OutsideImgName);
                        SqlCmd.Parameters.AddWithValue("@UploadShopImagePath", CustomerPortalImages);
                        SqlCmd.Parameters.AddWithValue("@UploadShopImageName", OutsideImgName);
                        SqlCmd.Parameters.AddWithValue("@action", 'I');

                        da = new SqlDataAdapter(SqlCmd);
                        dt = new DataTable();
                        dbConnectionObj.ConnectionOpen();
                        da.Fill(dt);
                        dbConnectionObj.ConnectionClose();
                    }
                }
                //     Section
                foreach (var sec in frmData.sectionDet)
                {
                    String SectionImgName = sec.SectionImgName;
                    if (!String.IsNullOrEmpty(SectionImgName))
                    {
                        dbConnectionObj = new DBConnection();
                        SqlCmd = new SqlCommand(ApiConstant.SPInsertUploadShopInside, dbConnectionObj.ApiConnection);
                        SqlCmd.CommandType = CommandType.StoredProcedure;
                        SqlCmd.Parameters.AddWithValue("@CustomerId", CustId);
                        SqlCmd.Parameters.AddWithValue("@CompanyID", CompId);
                        SqlCmd.Parameters.AddWithValue("@UploadShopName", SectionImgName);
                        SqlCmd.Parameters.AddWithValue("@UploadShopImagePath", CustomerPortalImages);
                        SqlCmd.Parameters.AddWithValue("@UploadShopImageName", SectionImgName);
                        SqlCmd.Parameters.AddWithValue("@action", 'I');

                        da = new SqlDataAdapter(SqlCmd);
                        dt = new DataTable();
                        dbConnectionObj.ConnectionOpen();
                        da.Fill(dt);
                        dbConnectionObj.ConnectionClose();
                    }
                }
                //     Inside
                foreach (var ins in frmData.insideDet)
                {
                    String InsideImgName = ins.InsideImgName;
                    if (!String.IsNullOrEmpty(InsideImgName))
                    {
                        dbConnectionObj = new DBConnection();
                        SqlCmd = new SqlCommand(ApiConstant.SPInsertUploadShopInside, dbConnectionObj.ApiConnection);
                        SqlCmd.CommandType = CommandType.StoredProcedure;
                        SqlCmd.Parameters.AddWithValue("@CustomerId", CustId);
                        SqlCmd.Parameters.AddWithValue("@CompanyID", CompId);
                        SqlCmd.Parameters.AddWithValue("@UploadShopName", InsideImgName);
                        SqlCmd.Parameters.AddWithValue("@UploadShopImagePath", CustomerPortalImages);
                        SqlCmd.Parameters.AddWithValue("@UploadShopImageName", InsideImgName);
                        SqlCmd.Parameters.AddWithValue("@action", 'I');

                        da = new SqlDataAdapter(SqlCmd);
                        dt = new DataTable();
                        dbConnectionObj.ConnectionOpen();
                        da.Fill(dt);
                        dbConnectionObj.ConnectionClose();
                    }
                }
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        ImgObj.Add(new ImgReuslt {
                            Msg = Convert.ToString(dt.Rows[0]["Msg"]),
                            CustomerId = CustId});
                    }
                }
                
                OMSWebApi.Models.CompanyModels.ListtoDataTable lsttodt = new OMSWebApi.Models.CompanyModels.ListtoDataTable();
                dtl = lsttodt.ToDataTable(ImgObj);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnectionObj = null;
                ImgObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return dtl;
        }

        public string GetTicketListData(String CustId, String UserEmpID, String CompanyId, String Status, String FromDate, String ToDate)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SPGetTicketDetails, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@UserEmpID", UserEmpID);
                SqlCmd.Parameters.AddWithValue("@CustomerId", CustId);
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);
                SqlCmd.Parameters.AddWithValue("@Status", Status);
                SqlCmd.Parameters.AddWithValue("@FromDate", FromDate);
                SqlCmd.Parameters.AddWithValue("@ToDate", ToDate);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();
                if (dt.Rows.Count >= 1)
                    jsonString = dtToJson.convertDataTableToJson(dt, "GetTicketListData", true);
                else
                {
                    jsonString = convertDataTableToJsonWithoutPayload(dt, "GetTicketListData", false);
                }
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetTicketListData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }
        public String convertDataTableToJsonWithoutPayload(DataTable dt, string eventName, bool status)
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            string jsonData;
            JsonData jsData = new JsonData();
            jsData.domain = eventName;  // 1
            jsData.events = eventName;  // 2
            StringBuilder sb = new StringBuilder();

            sb.Append("{");
            sb.Append("\"Domain\":" + "\"" + eventName + "\",");
            sb.Append("\"Event\":" + "\"" + eventName + "\",");
            sb.Append("\"ClientData\":" + "\"test2\",");
            sb.Append("\"Status\":" + "\"" + status + "\",");
            ///////////
            string[] jsonArray = new string[dt.Columns.Count];
            string headString = string.Empty;

            for (int i = 0; i < dt.Columns.Count; i++)
            {
                jsonArray[i] = dt.Columns[i].Caption; // Array for all columns
                headString += "\"" + jsonArray[i] + "\" : \"" + jsonArray[i] + i.ToString() + "%" + "\",";
            }

            if (headString.Length > 0)
                headString = headString.Substring(0, headString.Length - 1);

            ////sb.Append("\"Payload\":[");

            ////if (dt.Rows.Count > 0)
            ////{
            ////    for (int i = 0; i < dt.Rows.Count; i++)
            ////    {
            ////        string tempString = headString;
            ////        sb.Append("{");

            ////        // To get each value from the datatable
            ////        for (int j = 0; j < dt.Columns.Count; j++)
            ////        {
            ////            tempString = tempString.Replace(dt.Columns[j] + j.ToString().Replace("\"", "") + "%", dt.Rows[i][j].ToString().Replace("\"", "")).Replace("\\", "/").Replace("'", "-");
            ////        }

            ////        sb.Append(tempString + "},");
            ////    }
            ////}
            ////else
            ////{
            ////    string tempString = headString;
            ////    sb.Append("{");
            ////    for (int j = 0; j < dt.Columns.Count; j++)
            ////    {
            ////        tempString = tempString.Replace(dt.Columns[j] + j.ToString() + "%", "-");
            ////    }

            ////    sb.Append(tempString + "},");
            ////}

            sb = new StringBuilder(sb.ToString().Substring(0, sb.ToString().Length - 1));
            sb.Append("}");
            ////////////////

            //string data = jss.Serialize(rows); //3
            jsData.payload = sb.ToString(); ;

            jsData.clientData = "";  // 4
            jsData.status = "Sucess";
            return sb.ToString();
        }

        public DataTable SaveImageList(String CustomerId, String CompanyId, String ImageName, String Action, String LocalCustomerID)// String[] SectionSrl, String[] SectionNm, String[] OutsideSrl, String[] OutsideNm, String[] InsideSrl, String[] InsideNm)
        {
            Common common = new Common();
            DataTable dt = null;
            List<ImageResult> ImgObj = null;
            DataTable dtl = new DataTable();
            try
            {
                ImgObj = new List<ImageResult>();

                //     Outside
                if (Action == "Outside")
                {
                    if (!String.IsNullOrEmpty(CustomerId) && !String.IsNullOrEmpty(ImageName))
                    {
                        dbConnectionObj = new DBConnection();
                        SqlCmd = new SqlCommand(ApiConstant.SPInsertUploadShopOutside, dbConnectionObj.ApiConnection);
                        SqlCmd.CommandType = CommandType.StoredProcedure;
                        SqlCmd.Parameters.AddWithValue("@CustomerId", CustomerId);
                        SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);
                        SqlCmd.Parameters.AddWithValue("@UploadShopName", ImageName);
                        SqlCmd.Parameters.AddWithValue("@UploadShopImagePath", ApiConstant.CustPortalImage);
                        SqlCmd.Parameters.AddWithValue("@UploadShopImageName", ImageName);
                        SqlCmd.Parameters.AddWithValue("@action", 'I');

                        da = new SqlDataAdapter(SqlCmd);
                        dt = new DataTable();
                        dbConnectionObj.ConnectionOpen();
                        da.Fill(dt);
                        dbConnectionObj.ConnectionClose();
                    }
                }

                //     Section
                if (Action == "Section")
                {
                    if (!String.IsNullOrEmpty(CustomerId) && !String.IsNullOrEmpty(ImageName))
                    {
                        dbConnectionObj = new DBConnection();
                        SqlCmd = new SqlCommand(ApiConstant.SPInsertUploadSection, dbConnectionObj.ApiConnection);
                        SqlCmd.CommandType = CommandType.StoredProcedure;
                        SqlCmd.Parameters.AddWithValue("@CustomerId", CustomerId);
                        SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);
                        SqlCmd.Parameters.AddWithValue("@SectionName", ImageName);
                        SqlCmd.Parameters.AddWithValue("@SectionImagePath", ApiConstant.CustPortalImage);
                        SqlCmd.Parameters.AddWithValue("@SectionImageName", ImageName);
                        SqlCmd.Parameters.AddWithValue("@action", 'I');

                        da = new SqlDataAdapter(SqlCmd);
                        dt = new DataTable();
                        dbConnectionObj.ConnectionOpen();
                        da.Fill(dt);
                        dbConnectionObj.ConnectionClose();
                    }
                }

                //     Inside
                if (Action == "Inside")
                {
                    if (!String.IsNullOrEmpty(CustomerId) && !String.IsNullOrEmpty(ImageName))
                    {
                        dbConnectionObj = new DBConnection();
                        SqlCmd = new SqlCommand(ApiConstant.SPInsertUploadShopInside, dbConnectionObj.ApiConnection);
                        SqlCmd.CommandType = CommandType.StoredProcedure;
                        SqlCmd.Parameters.AddWithValue("@CustomerId", CustomerId);
                        SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);
                        SqlCmd.Parameters.AddWithValue("@UploadShopName", ImageName);
                        SqlCmd.Parameters.AddWithValue("@UploadShopImagePath", ApiConstant.CustPortalImage);
                        SqlCmd.Parameters.AddWithValue("@UploadShopImageName", ImageName);
                        SqlCmd.Parameters.AddWithValue("@action", 'I');

                        da = new SqlDataAdapter(SqlCmd);
                        dt = new DataTable();
                        dbConnectionObj.ConnectionOpen();
                        da.Fill(dt);
                        dbConnectionObj.ConnectionClose();
                    }
                }
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        ImgObj.Add(new ImageResult
                        {
                            Status = Convert.ToString(dt.Rows[0]["Msg"]),
                            ImageId = Convert.ToString(dt.Rows[0]["SrlNo"]),
                            ImageURL = ApiConstant.CustPortalImage + ImageName,
                            LocalCustomerID = LocalCustomerID
                        });
                    }
                }

                OMSWebApi.Models.CompanyModels.ListtoDataTable lsttodt = new OMSWebApi.Models.CompanyModels.ListtoDataTable();
                dtl = lsttodt.ToDataTable(ImgObj);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnectionObj = null;
                ImgObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return dtl;
        }

        public string UpdateTicketData( String CompanyId, String TicketId, String Status, String UserId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            int ret = 0;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcUpdate_TicketDetails, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);
                SqlCmd.Parameters.AddWithValue("@TicketID", TicketId);
                SqlCmd.Parameters.AddWithValue("@Status", Status);
                SqlCmd.Parameters.AddWithValue("@UserId", UserId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        String CustomerEmailId = Convert.ToString(dt.Rows[0]["EmailID"]);
                        if (!String.IsNullOrEmpty(CustomerEmailId))
                        {
                            clsEmail objMail;
                            objMail = new clsEmail();
                            objMail.MailFrom = Convert.ToString(ApiConstant.strSenderEmailId);
                            objMail.MailTo = Convert.ToString(CustomerEmailId);
                            objMail.MailSubject = Convert.ToString(dt.Rows[0]["TicketID"]) + ", Ticket Status Has Been Updated. ";
                            objMail.MailBody = objMail.GenerateMailBody(Convert.ToString(dt.Rows[0]["Name"]), "For TicketId : " + Convert.ToString(dt.Rows[0]["TicketID"]) + ", Status Has Been Updated As : " + Status);
                            ret = objMail.SendGeneralizedMail(Convert.ToString(ApiConstant.SMTPServer), Convert.ToString(ApiConstant.strSenderEmailId), Convert.ToString(ApiConstant.smtppasswd));
                            //dt.Columns.Add("Result", typeof(string));
                        }
                    }
                }
                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "UpdateTicketData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "UpdateTicketData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string SaveTicketData(String CustomerId, String UserEmpID, String CompanyId, String CustomerEmail, String OtherReason, String TicketReason, String fileName, String FilePath, string TicketId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            int ret = 0;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcInsert_TicketDetails, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);
                SqlCmd.Parameters.AddWithValue("@UserEmpID", UserEmpID);
                SqlCmd.Parameters.AddWithValue("@TicketReason", TicketReason);
                SqlCmd.Parameters.AddWithValue("@UploadFileName", fileName);
                SqlCmd.Parameters.AddWithValue("@UploadFilePath", FilePath);
                SqlCmd.Parameters.AddWithValue("@OtherReason", OtherReason);
                SqlCmd.Parameters.AddWithValue("@UserId", CustomerId);
                SqlCmd.Parameters.AddWithValue("@TicketId", TicketId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        String maiStatus = TicketId == "0" ? "Generated" : "Updated";
                        clsEmail objMail;
                        objMail = new clsEmail();
                        objMail.MailFrom = Convert.ToString(ApiConstant.strSenderEmailId);
                        objMail.MailTo = Convert.ToString(CustomerEmail);
                        objMail.MailSubject = Convert.ToString(dt.Rows[0]["TicketId"]) + ", Ticket Has Been "+maiStatus+" By The " + UserEmpID + " ( " +  CustomerId + " ). ";
                        objMail.MailBody = objMail.GenerateMailBody(Convert.ToString(UserEmpID + " ( " + CustomerId + " ), "), "TicketId : " + Convert.ToString(dt.Rows[0]["TicketId"]) + ". Reason : " + TicketReason);
                        ret = objMail.SendGeneralizedMail(Convert.ToString(ApiConstant.SMTPServer), Convert.ToString(ApiConstant.strSenderEmailId), Convert.ToString(ApiConstant.smtppasswd));
                        //dt.Columns.Add("Result", typeof(string));
                    }
                }
                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "SaveTicketData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "SaveTicketData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string SaveContactUs( String CompanyId, String ContactName, String ContactEmail, Int32 ContSubject, String ContMessage, String Phone, String CompanyName, String ContPerson)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            int ret = 0;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcSaveContactUsData, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);
                SqlCmd.Parameters.AddWithValue("@ContactName", ContactName);
                SqlCmd.Parameters.AddWithValue("@ContactEmail", ContactEmail);
                SqlCmd.Parameters.AddWithValue("@ContactSubject", ContSubject);
                SqlCmd.Parameters.AddWithValue("@ContactMessage", ContMessage);
                SqlCmd.Parameters.AddWithValue("@ContactPhone", Phone);
                SqlCmd.Parameters.AddWithValue("@ContactCompany", CompanyName);
                SqlCmd.Parameters.AddWithValue("@ContactPerson", ContPerson);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        clsEmail objMail;
                        objMail = new clsEmail();
                        objMail.MailFrom = Convert.ToString(ApiConstant.strSenderEmailId);
                        objMail.MailTo = Convert.ToString(ContactEmail);
                        objMail.MailSubject = "Contact Us Mail";
                        objMail.MailBody = objMail.GenerateMailBody(Convert.ToString(ContactName), "Thanks For Contacting Us, Admin Will Contact You Soon.");
                        ret = objMail.SendGeneralizedMail(Convert.ToString(ApiConstant.SMTPServer), Convert.ToString(ApiConstant.strSenderEmailId), Convert.ToString(ApiConstant.smtppasswd));
                    }
                }
                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "SaveContactUs", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "SaveContactUs", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string GetOpenTicketData(String CustId, String UserEmpID, String CompanyId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SPGetTicketForOpen, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@UserEmpID", UserEmpID);
                SqlCmd.Parameters.AddWithValue("@CustomerId", CustId);
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();
                if (dt.Rows.Count >= 1)
                    jsonString = dtToJson.convertDataTableToJson(dt, "GetOpenTicketData", true);
                else
                {
                    jsonString = convertDataTableToJsonWithoutPayload(dt, "GetOpenTicketData", false);
                }
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetOpenTicketData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        
        public DataTable GetGeneratedCustomeIDFromDB(string CompanyId, String BillingCO)
        {
            Common common = new Common();
            List<GeneratedCustomerNo> GenCustomerNoObj = null;
            DataTable dtl = null;
            try
            {
                dtl = new DataTable();
                GenCustomerNoObj = new List<GeneratedCustomerNo>();
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcOMSGetLastCustomerNo, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);
                SqlCmd.Parameters.AddWithValue("@OrderTitle", "CUSTOMER" );
                SqlCmd.Parameters.AddWithValue("@BillingCO", BillingCO);
                SqlCmd.Parameters.Add("@MESSAGE", SqlDbType.VarChar, 50);
                SqlCmd.Parameters["@MESSAGE"].Direction = ParameterDirection.Output;
                da = new SqlDataAdapter(SqlCmd);
                dbConnectionObj.ConnectionOpen();
                SqlCmd.ExecuteNonQuery();
                dbConnectionObj.ConnectionClose();
                string LastCustomerNo = Convert.ToString(SqlCmd.Parameters["@MESSAGE"].Value);

                ////Bind LastCustomerNo generic list using LINQ  
                GenCustomerNoObj.Add(new GeneratedCustomerNo
                            {
                                LastCustomerNo = LastCustomerNo
                            });

                OMSWebApi.Models.CompanyModels.ListtoDataTable lsttodt = new OMSWebApi.Models.CompanyModels.ListtoDataTable();
                dtl = lsttodt.ToDataTable(GenCustomerNoObj);

            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                GenCustomerNoObj = null;
            }
            return dtl;
        }

        public DataTable DeleteOpenTicketFromDB(string CompanyId, String TicketId)
        {
            Common common = new Common();
            DataTable dt = null;
            SqlDataAdapter da = null;
            int ret = 0;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcDeleteOpenTicket, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);
                SqlCmd.Parameters.AddWithValue("@TicketId", TicketId);
                SqlCmd.Parameters.Add("@MESSAGE", SqlDbType.VarChar, 50);
                SqlCmd.Parameters["@MESSAGE"].Direction = ParameterDirection.Output;
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();
                String retStatus = Convert.ToString(SqlCmd.Parameters["@MESSAGE"].Value);

                if (dt.Rows.Count>0)
                {
                    String UserEmpID = Convert.ToString(dt.Rows[0]["UserEmpId"]);
                    String CustomerID = Convert.ToString(dt.Rows[0]["CustomerID"]);
                    String Ticket_Id = Convert.ToString(dt.Rows[0]["TicketId"]);
                    clsEmail objMail;
                    objMail = new clsEmail();
                    objMail.MailFrom = Convert.ToString(ApiConstant.strSenderEmailId);
                    objMail.MailTo = Convert.ToString(dt.Rows[0]["CustomerEmailID"]);
                    objMail.MailSubject = Convert.ToString(dt.Rows[0]["TicketId"]) + ", Ticket Has Been Deleted By The " + UserEmpID + " ( " + CustomerID + " ). ";
                    objMail.MailBody = objMail.GenerateMailBody(UserEmpID + " ( " + CustomerID + " )", "TicketId : " + Ticket_Id + ". Status : Deleted" );
                    ret = objMail.SendGeneralizedMail(Convert.ToString(ApiConstant.SMTPServer), Convert.ToString(ApiConstant.strSenderEmailId), Convert.ToString(ApiConstant.smtppasswd));
                }
                return dt;

            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
            }
            return dt;
        }

        public string EmailToNewUser(String CustomerId, String UserEmpID, String CompanyId, String CustomerEmail, String Action)
        {
            DataTable dt = null;
            string jsonString, Comments=string.Empty;
            try
            {
                if (Action == "I")
                    Comments = "Welcome To The Customer Portal. Your User Id : " + UserEmpID + " And Password Is : 11111111";
                else if (Action == "U")
                    Comments = "Thanks For Updating The Profile Data.";
                clsEmail objMail;
                objMail = new clsEmail();
                objMail.MailFrom = Convert.ToString(ApiConstant.strSenderEmailId);
                objMail.MailTo = Convert.ToString(CustomerEmail);
                objMail.MailSubject = Action == "I" ? UserEmpID + " Has Been Created Under CustomerID " + CustomerId : UserEmpID + " Has Been Updated Under CustomerID " + CustomerId;
                objMail.MailBody = objMail.GenerateMailBody(Convert.ToString(UserEmpID), Comments);
                int status = objMail.SendGeneralizedMail(Convert.ToString(ApiConstant.SMTPServer), Convert.ToString(ApiConstant.strSenderEmailId), Convert.ToString(ApiConstant.smtppasswd));
                dt = new DataTable();
                dt.Columns.Add("Result", typeof(string));
                if (status > 0)
                    dt.Rows.Add("Ok");
                else
                    dt.Rows.Add("Not Ok");

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "EmailToNewUser", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "EmailToNewUser", false);
            }
            finally
            {
                dt = null;
            }
            return null;
        }

        public string GetZoneListData(String CompanyId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SPGetZoneList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetZoneListData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetZoneListData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        

    }
}