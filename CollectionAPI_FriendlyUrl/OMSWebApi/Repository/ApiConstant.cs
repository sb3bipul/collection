﻿using System.Configuration;
using System.Web.Configuration;

namespace OMSWebApi.Repository
{
    public class ApiConstant
    {
        /// <summary>
        /// Conncetion String to connect to database
        /// </summary>
        public static string apiConnectionString = ConfigurationManager.ConnectionStrings["conStr"].ToString();
        public static string apiConnectionStringDB2 = ConfigurationManager.ConnectionStrings["conStrDB2"].ToString();
        public static string apiConnectionStringConfig = ConfigurationManager.ConnectionStrings["conConfig"].ToString();
        public static string apiConnectionStringOAuth = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        /// <summary>
        /// User Authentication and if success response with Token
        /// </summary> 
        public static string userTokenApiUri = WebConfigurationManager.AppSettings["UserTokenApiUri"];
        public static string ldapAuthenticationApiUri = WebConfigurationManager.AppSettings["LDAPAuthenticationApiUri"];
        public static string userVerifyApiUri = WebConfigurationManager.AppSettings["UserVerifyApiUri"];
        public static string userAuthorizationApiUri = WebConfigurationManager.AppSettings["UserAuthorizationApiUri"];
        public static string getUserFullNameApiUri = WebConfigurationManager.AppSettings["GetUserFullNameApiUri"];
        public static string getClientName = WebConfigurationManager.AppSettings["ClientName"];
        public static string getClientFolsom = WebConfigurationManager.AppSettings["ClientFlsm"];
        public static string getClientOther = WebConfigurationManager.AppSettings["ClientOther"];
        public static string FileDirectory = WebConfigurationManager.AppSettings["ImageData"] + "\\Files"; // "E:\\OMSmobile_API\\OMSmobilePublished\\Files";    // WebConfigurationManager.AppSettings["FileDirectoryPath"];   E:\\OMSmobile_API\\OMSmobilePublished\\Files    D:\Koushik\OMSmobile_API\OMSmobileImg\Files\EDI
        public static string SectionOutsideInside_ImagePath = WebConfigurationManager.AppSettings["ImageData"] + "\\Customer\\StoreImage"; // "E:\\OMSmobile_API\\OMSmobilePublished\\UploadedFiles";   //E:\OMSmobile_API\OMSmobilePublished\UploadedFiles
        public static string CustPortalImage = WebConfigurationManager.AppSettings["CustPortalImage"]; // "http://localhost/CustomerPortalImages/";   //WebConfigurationManager.AppSettings["CustPortalImagePath"]; 
        public static string CustomerPortalImage = WebConfigurationManager.AppSettings["CustomerPortalImage"];
        public static string CategoryAPI = WebConfigurationManager.AppSettings["CategoryAPI"];
        public static string StorePhotoPath = WebConfigurationManager.AppSettings["ImageData"] + "\\Customer\\ProfileImage";
        public static string ImageData = WebConfigurationManager.AppSettings["ImageData"];
        public static string PaymentImage = WebConfigurationManager.AppSettings["PaymentImage"];
        /// <summary>
        /// Token verification 
        /// </summary>
        public static string userVerifyTokenApiUri = WebConfigurationManager.AppSettings["UserVerifyTokenApiUri"];

        /// <summary>
        /// Change Password
        /// </summary>
        public static string changePasswordApiUri = WebConfigurationManager.AppSettings["ChangePasswordApiUri"];

        public static string brokerPicUri = WebConfigurationManager.AppSettings["BrokerPicUri"];

        public static string DefaultBrokerPwdPrefix = WebConfigurationManager.AppSettings["DefaultBrokerPwdPrefix"];

        /// <summary> 
        /// Stored Procedure to get list of all customer for salesmanID from DB2
        /// </summary>
        public static string SPocCustomersDetailListDB2 = "USP_GET_CUST_BY_BROKER";
        public static string SPocCustomersListDB2 = "USP_CUSTOMER_LIST_BY_BROKER";

        // Get Quantity on Hand
        public static string SProcGetQuantityOnHand = "USP_GET_ITEM_QOH";
        public static string SProcGetItemQuantityHand = "SP_GET_ITEM_QOH";
        /// <summary> 
        /// Stored Procedure to get list of all customer for salesmanID
        /// </summary>
        public static string SPocCustomersDetailList = "SP_CustomersDetailList";

        /// <summary>
        /// Get Customer Detail by CustomerId and CompnayId
        /// </summary>
        public static string SPocCustomersDetailByCustomerId = "SP_CustomersDetailByCustomerId";

        /// <summary>
        /// Get all Promo Codes
        /// </summary>
        public static string SProcGetPromoCodes = "SP_getPromoCode";
        public static string SProcGetPromoCodeList = "SP_getPromoCodeList";
        /// <summary>
        /// Get All Departments
        /// </summary>
        public static string SProcGetDepartments = "SP_getDepartment";

        /// <summary>
        /// Get templates data by BrokerID,CompanyId  get
        /// </summary>
        public static string SProcGetTemplates = "SProc_OMS_ViewTempBasketData_Template";

        /// <summary>
        /// Get Order History by BrokerId  
        /// </summary>
        public static string SProcOrderHistory = "SProc_GetOrdersByFilters";
        public static string SProcGetSubmittedOrdersDetail = "SP_GetSubmittedOrdersDetail";
        public static string SProcSubmittedOrder = "SProc_GetSubmittedOrder";

        public static string SProcSavedCartOrder = "SProc_GetSavedCartOrder";
        public static string SProcGetSavedCartOrdersDetail = "SP_GetSavedCartOrdersDetail";
        public static string SPGetOrdersDetailByPOCustomer = "SP_GetOrdersDetailByPOCustomer";
        public static string SProcGetOrderHeaderByStatus = "SProc_GetOrderHeaderByStatus";
        public static string SProcGetErrorOrder = "SProc_GetErrorOrder";
        public static string SProcGetErrorOrderDetail = "SP_GetErrorOrdersDetail";
        /// <summary>
        /// Get Pending Order History 
        /// </summary>
        public static string SProcPendingOrderHistoryByCustomer = "SP_GetPendingOrderHistory";

        /// <summary>
        /// Get All Order Status
        /// </summary>
        public static string SProcGetAllOrderStatus = "SP_GetAllOrderStatus";

        /// <summary>
        /// Delete Order from Order History Screen
        /// </summary>
        public static string SProcDeleteOrders = "SProc_DeleteOrders";
        public static string SProcDeleteOpenOrders = "SProc_DeleteOpenOrders";

        /// <summary>
        /// Get Item Data for Create Order
        /// </summary>
        public static string SProcGetItemOrderItemData = "SProc_OMS_Get_InsertTempBasketOrder";
        public static string SProcGetItemOrderItemMobile = "SProc_OMS_Get_InsertTempBasketOrderM";
        /// <summary>
        /// Delete Order Item
        /// </summary>
        public static string SProcDeleteOrderItem = "SP_DeleteOrderItem";

        /// <summary>
        /// Delete all order items from Order
        /// </summary>
        public static string SProcDeleteAllOrderItems = "SP_DeleteAllOrderItems";

        /// <summary>
        /// Update Item Case Quantity
        /// </summary>
        public static string SProcUpdateItemCaseQuantity = "SP_UpdateItemCaseQuantity";
        public static string SrocUpdateOrderPOPaymentTerm = "SP_UpdateOrderPOPaymentTerm";
        /// <summary>
        /// Update Item Unit Quantity
        /// </summary>
        public static string SProcUpdateItemUnitQuantity = "SP_UpdateItemUnitQuantity";


        //  public static string SProcGetProductData = "SProc_OMS_ProductListFromCatalogId";
        public static string SProcGetProductData = "SP_ProudctList";
        public static string SProcGoyaGetProductData = "SP_Goya_ProudctList";

        // Get Item list from DB2
        public static string SProcGetProductDataDB2 = "USP_GET_ITEM_LIST";

        //   Send Email 
        public static string strSenderEmailId = "koushik@sb3inc.com";//WebConfigurationManager.AppSettings["strSenderAddress"]; 
        public static string smtppasswd = "welcome123#";//WebConfigurationManager.AppSettings["smtppasswd"];
        public static string SMTPServer = "smtp.1and1.com";

        public static string SProcProductListByCatalogId = "SP_ProductbyCustomerId";

        public static string SProcProductListByUserInput = "SP_ProductbySearch";


        public static string SProcSubmitOrder = "SProc_OMS_SaveBasket_HeaderDtl_Ins";
        public static string SProcSubmitQuickOrder = "SProc_OMS_Submit_QuickOrder";

        public static string SProcGetUserDetails = "sp_GetUserDetails";

        /// <summary>
        /// Get template data and create New Order
        /// </summary>
        public static string SProcGetOrderDataByTemplate = "SProc_OMS_InsertTempBasket_TemplateDetails";

        public static string SProcGetOrderHeaderDetail = "SP_GetOrderHeaderDetail";
        public static string SProcGetPendingOrderItemsList = "SP_GetPendingOrderItemsList";

        public static string SProcGetPendingOrderItems = "SP_GetPendingOrderItems";

        public static string SProcGetPendingOrderCountByCustomerId = "SP_GetPendingOrderByCustomerId";

        /// <summary>
        ///  Get Order Preview
        /// </summary>
        public static string SProcGetOrderPreview = "SP_GetOrdersByBasketID";

        /// <summary>
        /// Get Order Count By Broker
        /// </summary>
        public static string SProcGetOrderCountByBroker = "SP_OrderCountByBroker";

        /// <summary>
        ///  Get Order Preview from DB2
        /// </summary>
        public static string SProcGetOrderPreviewDB2 = "USP_GET_ORDER_DETAIL_BY_ORDERID";

        public static string SProcGetSalesCommunicatorData = "SProc_OMS_ViewSalesCommunicator";

        public static string SProcGetLastEOR = "SProc_OMS_GetLastEOR";

        // Validate Customer by ID

        public static string SProcValidateCustomerID = "SP_CheckCustomerByCustomerId";

        // Insert Sales Communicator
        public static string SProcInsertSalesCommunicator = "Sproc_OMS_InsertSalesCommunicator";

        // Edit Sales Communicator Data
        public static string SProcEditSalesCommunicator = "Sproc_OMS_UpdateSalesCommunicator";

        // Delete Sales Communicator (Soft Delete)
        public static string SProcDeleteSalesCommunicator = "Sproc_OMS_DeleteSalesCommunicator";

        // Get Out of Stock Items list by Date Range
        public static string SProcGetOutOfStockItemList = "SP_GetOutOfStockItemList";

        // Get Broker list By Admin Broker ID
        public static string SProcGetBrokerListByAdminBrokerID = "SP_GetBrokerListByAdminBroker";

        // Insert User Activity
        public static string SProcInsertUserActivity = "SP_InsertUserActivity";

        // Get System Stats
        public static string SProcGetSystemStats = "SP_GetSystemStat";


        // Get all controls name using langauge
        public static string SProcGetApplicationDataByLanguage = "SP_GetApplicationDataByLanguage";

        // Get Customer Details by Customer ID
        public static string SProcGetCustomerDetailByCustomerID = "SP_GetCustomerDetail";

        // Get Broker Detail by BrokerID
        public static string SProcGetBrokerDetailByBrokerID = "SP_GetBrokerDetail";

        public static string SPocGetUserFullName = "sp_GetUserFullName";

        public static string SPocGetUserIdFromUserName = "sp_GetUserIdFromUserName";

        public static string SPocGetLookupMappingTableList = "sp_GetLookupMappingTableList";

        public static string SPocGetGeneralLookupList = "sp_GetGeneralLookupList";

        public static string SPocGetRoleApplicationAccessMappingList = "sp_GetRoleApplicationAccessMappingList";

        public static string SPocGetUserAppRoleAssignedList = "sp_GetUserAppRoleAssignedList";
        public static string SPocGetUserAppRoleNotAssignedList = "sp_GetUserAppRoleNotAssignedList";
        public static string SPocDeleteRoleAppAccessMapByIds = "sp_DeleteRoleAppAccessMapByIds";
        public static string SPocInsertUserAppRoleMap = "sp_InsertUserAppRoleMap";
        public static string SPocDeleteUserAppRoleMapByIds = "sp_DeleteUserAppRoleMapByIds";
        public static string SPocInsertRoleAppAccessMap = "sp_InsertRoleAppAccessMap";
        public static string SPocGetIfUserExist = "sp_GetIfUserExist";
        public static string SPocGetIfUserOrEmailExist = "sp_GetIfUserOrEmailExist";
        public static string SPocCheckIfValidEmailExist = "sp_CheckIfValidEmailExist";
        public static string SPocGetRoleIdFromUserId = "sp_GetRoleIdFromUserId";

        // Reset Update Release Flag 
        public static string SPocResetReleaseUpdateFlag = "sp_ResetReleaseUpdateFlag";

        public static string DefaultPwdPrefix = WebConfigurationManager.AppSettings["DefaultPwdPrefix"];
        public static string apiNewUserUri = WebConfigurationManager.AppSettings["apiNewUserUri"];
        public static string apiChangePassword = WebConfigurationManager.AppSettings["apiChangePassword"];

        public static string SPocGetUserMyProfile = "sp_GetUserMyProfile";
        public static string SPocUpdateMyProfile = "sp_UpdateMyProfile";

        public static string SPocGetOMSUserForOAuthSync = "sp_GetOMSUserForOAuthSync";
        public static string SPocGetRolesList = "sp_GetRolesList";
        public static string SPocGetApplicationAccessMappingList = "sp_GetApplicationAccessMappingList";
        public static string SPocInsertApplication = "sp_InsertApplication";
        public static string SPocGetApplicationRoleMappingList = "sp_GetApplicationRoleMappingList";
        public static string SPocUpdateRoleById = "sp_UpdateRolesById";
        public static string SPocDeleteApplicationById = "sp_DeleteApplicationById";
        public static string SPocGetApplicationsList = "sp_GetApplicationsList";
        public static string SPocUpdateMenuById = "sp_UpdateMenuById";
        public static string SPocInsertAppAccessMap = "sp_InsertAppAccessMap";
        public static string SPocDeleteAppAccessMapById = "sp_DeleteAppAccessMapById";
        public static string SPocUpdateApplicationById = "sp_UpdateApplicationById";
        public static string SPocInsertRole = "sp_InsertRole";
        public static string SPocGetRoleLookupListForAppId = "sp_GetRoleLookupListForAppId";
        public static string SPocDeleteAppRoleMapByIds = "sp_DeleteAppRoleMapByIds";
        public static string SPocDeleteRoleById = "sp_DeleteRoleById";
        public static string SPocInsertAppRoleMap = "sp_InsertAppRoleMap";
        public static string SPocUpdateUser = "sp_UpdateUser";
        public static string SPocGetUserSearchList = "sp_GetUserSearchList";
        public static string SPocUpdateUserProfileFromAdmin = "sp_UpdateUserProfileFromAdmin";
        public static string SPocUpdateUpdatedBy = "sp_UpdateUpdatedBy";


        /* Back Room Inventory List  */

        // Insert / Get Back room Inventory List
        public static string SProcBackRoomInventoryList = "SP_GetInsertBackRoomInventory";

        // Delete Item from Back Room inventory
        public static string SProcDeleteBarckRoomItem = "SP_DeleteBackRoomItem";

        // Delete Item from Back Room inventory
        public static string SProcDeleteAllBackRoomItems = "SP_DeleteAllBackRoomItems";


        // Update Item Case Quantity       
        public static string SProcUpdateItemCaseBackRoomQuantity = "SP_UpdateItemCaseBackRoomQuantity";


        // Insert Address Request for Customer by Broker
        public static string SProcInsertAddressChangeRequest = "SP_InsertAddressChangeRequest";

        // Get Release Updates by Version
        public static string SProcGetReleaseUpdatesByVersion = "sp_GetReleaseUpdatesByVersion";

        // Get Order History from AS400 DB2
        // public static string SProcGetOrderHistoryDB2 = "USP_GET_ORDER_HISTORY";
        public static string SProcGetOrderHistoryDB2 = "USP_ORDERHISTORY_TEST";

        // Get Customer information by Customer Id from DB2
        public static string SProcGetCustomerInfoByCustomerIdDB2 = "USP_GET_CUSTOMER_INFO_CUSTOMERID";

        // Get Price List URL 
        public static string SProcGetPriceListURL = "Sproc_OMS_GetPriceListFile";


        // FO - Get Warehouse List
        public static string SProcGetWarehouseList = "SProc_OMS_GetWarehouseList";

        // FO - Get Vendor List
        public static string SProcGetVendorList = "SProc_OMS_GetVendorList";

        // FO - Get Product Class list
        public static string SProcGetProductClass = "SProc_OMS_GetCategoryList";

        // FO - Get Vendor Group by Vendor ID
        public static string SprocGetVendorGroupByVendorID = "SProc_OMS_GetVendorGroup";

        // FO - Get Product Class (Category) by Vendor ID
        public static string SprocGetCategoryByVendorID = "SProc_OMS_GetCategoryByVendorChanged";

        // FO - Get Vendor by Category ID
        public static string SprocGetVendorByCategoryID = "SProc_OMS_GetVendorByCategoryChanged";

        // FO - Get Customer list by Salesman ID
        public static string SProcGetCustomerListBySalesmanId = "SProc_OMS_GetCustomerListBySalesmanID";

        // FO - Get Items By Search
        public static string SProcGetItemsBySearch = "SProc_OMS_ProductSearch";

        // FO - Add to Cart
        public static string SProcAddToCart = "SProc_OMS_AddToCartGuided";

        // FO - Get Order Details by Order ID
        public static string SProcGetOrderDetailsByOrderId = "SProc_OMS_GetOrderDetailsByOrderId";

        // FO - Get Order Details by Order ID
        public static string SProcSubmitOrderFO = "SProc_CreatePurchaseOrder";

        // FO - Update Order Item Qty
        public static string SProcUpdateItemQty = "SP_UpdateOrderItemQty";

        // FO - Delete Item
        public static string SProcDeleteItem = "SP_DeleteOrderItemQty";

        // FO - Order History
        public static string SProcOrderHistoryData = "SProc_PopulateOrderHistoryData";

        // FO - Delete Order
        public static string SProcDeleteOrder = "SP_DeleteOrder";

        // FO - Add Item to Order
        public static string SProcAddItemToOrder = "SProc_OMS_AddItemToOrder";

        // FO - Get Sub Cateogry List
        public static string SprocGetSubCategoryByCategory = "SProc_OMS_GetSubcategoryList";

        // FO - Get Sub Category by CategoryId
        public static string SprocGetSubCategoryByCategoryId = "SProc_OMS_GetSubcategoryByCategoryChanged";

        // FO - Get Vendor Group by Sub CategoryId
        public static string SprocGetVendorGroupBySubCategoryId = "SProc_OMS_GetVendorGroupBySubCategory";

        // Screen Image for Login
        public static string SProcGetScreenImageList = "SProc_GetScreenImageList";

        // Compoany Registration
        public static string SProcInsertCompany = "SProc_InsertCompany";

        // Company Image like BG, Icon, Logo
        public static string SProcGetCompanyImageList = "SProc_GetCompanyImageList";

        // User Registration
        public static string SprocInsertUserRegistration = "Sproc_OMS_InsertUserRegistration";
        // User Id by Email 
        public static string SProcGetUserIdByEmail = "SProc_GetUserIdByEmail";
        public static string SProcGetDashboardFullList = "SProc_GetDashboardList";
        public static string SPCustomersOrderCountBySalesman = "SP_CustomersOrderCountBySalesman";
        public static string SProcInsertItem_Wishlist = "SProc_InsertItem_Wishlist";

        public static string SProcGetCategory = "SProc_OMS_GetCategory";
        public static string SProcGetCategoryGoya = "SProc_OMS_GetCategoryGoya";

        public static string SProcGetSubCategory = "SProc_OMS_GetSubCategory";
        public static string SProcGetSubCategoryGoya = "SProc_OMS_GetSubCategoryGoya";

        public static string SProcGetCompanyByID = "SProc_OMS_GetCompanyByIDSPgetStateList";


        public static string SProcUpdateCustomerAddressGoya = "SP_UpdateCustomerAddressGoya";

        public static string SProcGetAllOrderByStatus = "SProc_GetAllOrderByStatus";
        public static string SProcGetOrdersDetailByStatus = "SP_GetOrdersDetailByStatus";

        public static string SProcGetOrderNoByOrderDate = "SP_GetOrderNoByOrderDate";
        public static string SProcOMS_Submit_Invoice = "SProc_OMS_Submit_Invoice";
        public static string SProcOMSGetCustomerListBySalesman = "SProc_OMS_GetCustomerListBySalesman";
        public static string SProcGetCustomerToVisit = "SProc_GetCustomerToVisit";
        public static string SProcGetNotification = "SProc_GetNotification";
        public static string SPUpdateNotificationGetDetail = "SP_UpdateNotificationGetDetail";
        public static string SProcOMS_DeleteNotification = "SProc_OMS_DeleteNotification";
        public static string SProcGetAllOrders = "SProc_GetAllOrders";
        public static string SPCustomersFullDetailByCustomerId = "SP_CustomersFullDetailByCustomerId";
        public static string SPGetDayHours_ByAuthCustomerId = "SP_GetDayHours_ByAuthCustomerId";
        public static string SPGetReceiveHours_ByAuthCustomerId = "SP_GetReceiveHours_ByAuthCustomerId";

        //          Master Data
        public static string SProcOMSBrokerList = "SProc_OMS_BrokerList";
        public static string SProcOMSBrokerListByComp = "SProc_OMS_BrokerListByComp";
        public static string SProcCustomerListByBroker = "SP_CustomerList_By_Broker";
        public static string SProcOMSWarehouseList = "SProc_OMS_WarehouseList";
        public static string SProcOMSWarehouseListByComp = "SProc_OMS_WarehouseListByComp";
        public static string SProcOMSVendorList = "SProc_OMS_VendorList";
        public static string SProcOMSBrandList = "SProc_OMS_BrandList";
        public static string SProcOMSOrderStatusist = "SProc_OMS_OrderStatusList";
        public static string SProcOMSProductSrockList = "SProc_OMS_ProductSrockList";
        public static string SProcOMSProductSrockListByComp = "SProcOMSProductSrockListByComp";
        public static string SProc_OMS_GetProductPromoPriceGoya = "SProc_OMS_GetProductPromoPriceGoya";
        public static string SProcOMSSubmitQuickOrderByChatMsg = "SProc_OMS_Submit_QuickOrderByChatMsg";
        public static string SProcOMS_SubmitBypassOrderByAnyCustomer = "SProc_OMS_SubmitBypassOrderByAnyCustomer";
        public static string SProcOMSSubmitQuickOrderByChatMsgFlsm = "SProc_OMS_Submit_QuickOrderByChatMsgFlsm";
        public static string SProcGetQuickOrderDetails = "SProc_GetQuickOrderDetails";
        public static string SPGoya_PriceList = "SP_Goya_PriceList";
        public static string SProcGetPriceList = "SProc_GetPriceList";
        public static string SProcGetCreditCustomer = "SProc_GetCreditCustomer";
        public static string SProcGetTopSellingCustomer = "SProc_GetTopSellingCustomer";
        public static string SPGetReceiptDetailByInvoiceNo = "SP_GetReceiptDetailByInvoiceNo";
        public static string SPGetReceiptList = "SP_GetReceiptList";
        public static string SPGetUpdateBrokerDetails = "sp_GetUpdateBrokerDetails";
        public static string SProcInsertCategory = "SProc_InsertCategory";
        public static string SProc_GetMergedCategory = "SProc_OMS_GetMergedCategory";
        public static string SProc_OMS_VendorGroupList = "SProc_OMS_VendorGroupList";

        //   Track Location  
        public static string SProcGetTrackLocationHistory = "SProc_GetTrackLocationHistory";
        public static string SProcInsertTrackLocation = "SProc_InsertTrackLocation";


        //   Customer Profile Creation
        public static string SPGetCustomerProfileList = "SP_GetCustomerProfileList";
        public static string SPUpdateAddCustomerPortalM = "SP_UpdateAddCustomerPortalM";
        public static string SPgetStateList = "SP_getStateList";
        public static string SPGetSectionImgList = "SP_GetSectionImgList";
        public static string SPGetShopOutsideImgList = "SP_GetShopOutsideImgList";
        public static string SPGetShopInsideImgList = "SP_GetShopInsideImgList";
        public static string SPGetCustomer_DayHoursList = "SP_GetCustomer_DayHoursList";
        public static string SProcInsert_TicketDetails = "SProc_Insert_TicketDetails";
        public static string SPInsertUploadSection = "SP_InsertUploadSection";
        public static string SPInsertUploadShopInside = "SP_InsertUploadShopInside";
        public static string SPInsertUploadShopOutside = "SP_InsertUploadShopOutside";
        public static string SPGetTicketDetails = "SP_GetTicketDetails";
        public static string SPGetTicketForOpen = "SP_GetTicketForOpen";
        public static string SProcDeleteOpenTicket = "SProc_OMS_DeleteOpenTicket";

        public static string SPGetDayHours_ByCustomerId = "SP_GetDayHours_ByCustomerId";
        public static string SPGetReceiveHours_ByCustomerId = "SP_GetReceiveHours_ByCustomerId";
        public static string SPGetOrdersByPO = "SP_GetOrdersByPO";

        public static string SProcSaveContactUsData = "SProc_SaveContactUsData";
        public static string SProcUpdate_TicketDetails = "SProc_Update_TicketDetails";
        public static string SPGetZoneList = "SP_GetZoneList";

        // Smart Order
        public static string SProcGetItemsListByCategories = "SProc_GetItemsListByCategories";          // Get Item List by Category
        public static string SProcGetItemSubCategory = "sp_GetItemSubCategory";                         // Get Item Sub Category
        public static string SProcGetItemsListByTagName = "SProc_GetItemsListByTag";                    // Get Item List by Tag Name
        public static string SProcGetItemSalesCategory = "SP_GetItemSalesCategory";                     // Get Items Sales Category
        public static string SProcGetItemSalesSubCategory = "SP_GetItemSalesSubCategory";               // Get Items Sales Sub Category
        public static string SProcGetItemsListBySalesCategories = "SProc_GetItemsListBySalesCategories";// Istem list by Sales Category
        public static string SProcGetTop50ItemsByCustomer = "SProc_GetItemsTop50ByCustomer";           // Top 50 Item by Customer
        public static string SProcGetItemListByCustomerHistory = "SProcGetItemListByCustomerHistory";   // Item list by Customer History
        public static string SProcGetItemListByCustomerLastOrder = "SProcGetItemListByCustomerLastOrder";// Last order by customer
        public static string SProcGetPromoItemsList = "SProc_GetPromoItemsList";                        // Item list by promo
        public static string SProcGetItemListByNotOrderedLastMonth = "SProc_GetItemListByNotOrderedLastMonth"; // Not Ordered Last Month
        public static string SProcGetOrderImageList = "SProc_GetOrderImageList";

        // Invoice
        public static string SProcGetInvoiceOrderByDate = "SProc_GetInvoiceOrderByDate";
        public static string SPGetInvoiceDetailByNo = "SP_GetInvoiceDetailByNo";
        public static string SProcGetPaymentTransaction = "SProc_GetPaymentTransaction";
        public static string SProcGetPaymentList = "SProc_GetPaymentList";

        // Custoemr Profile 
        public static string SProcOMSGetLastCustomerNo = "SProc_OMS_GetLastCustomerNo";
        public static string SPGetCustomerReceivingHoursList = "SP_GetCustomer_ReceivingHoursList";

        //Activity Log
        public static string SProcOMS_SaveBrokerActivityLog = "SProc_OMS_SaveBrokerActivityLog";
        public static string SPGetBrokerActivityLog = "SP_GetBrokerActivityLog";

        //Goya Popular product
        public static string SProcPopularProduct = "SP_OMS_GetPopularProduct";// added for Goya popular product list

        //Collection API
        public static string Sproc_SavePaymentInfo = "Sproc_SavePaymentInfo"; // Save payment Information
        public static string SProc_SavePaymentImage = "SProc_SavePaymentImage"; //save payment image
        public static string SProc_GetPaymentHistory = "SProc_GetPaymentHistory"; // get payment history
        public static string SProc_SavePaymentPDF = "SProc_SavePaymentPDF"; //insert payment receipt pdf
        public static string SProc_getDataforCollectionAdmin = "SProc_getDataforCollectionAdmin";
        public static string SProc_getPaymentimage = "SProc_getPaymentimage";
        public static string SProc_getCollectionDashbordData = "SProc_getCollectionDashbordData"; //get Collection Dashbord Data
        public static string SProc_GetAllPendingPaymentDetails = "SProc_GetAllPendingPaymentDetails"; // get all pending and failure payment details
        public static string SProc_GetPaymentStatusByReceiptNo = "SProc_GetPaymentStatusByReceiptNo"; // get paymentstatus by receiptno
        public static string SProc_UpdatePaymentStatus = "SProc_UpdatePaymentStatus"; // update payment status


        // Collection Admin Portal API
        public static string SProc_InsertCollectPersonalData = "SProc_InsertCollectPersonalData"; // Insert collection profile data
        public static string SProc_GetCollectionPersonProfileData = "SProc_GetCollectionPersonProfileData"; //Get Collection Person Profile Data
        // Country list
        public static string SProcGetCountryList = "SProc_GetCountryList";
        // State list by country 
        public static string SProcGetStateList = "SProc_GetStateList";
        public static string SProc_GetActiveCompanyList = "SProc_GetActiveCompanyList"; // get all active company list
        public static string SProc_GetCustomerList = "SProc_GetCustomerList"; // get all active customer list for collection admin
        public static string SProc_GetZoneList = "SProc_GetZoneList"; // get all customer zone data
        public static string SProc_GetCollectPersonList = "SProc_GetCollectPersonList"; // get collect personal list for assign customer module
        public static string SProc_InsertCollectPersonCustomer = "SProc_InsertCollectPersonCustomerRelation"; // insert collect person and customer relation
        public static string SProc_GetCustCollectPersonRelation = "SProc_GetCustCollectPersonRelation"; // Get Cust Collect Person Relation


        // Collect Person login
        public static string SProc_getUserProfileForLogin = "SProc_getUserProfileForLogin"; // get user profile for user login
        public static string getCustomerListByCpCode = "GetCustomerListByCpCode"; // get customer list as per collect person login
        public static string SProc_ChangeCPStatus = "SProc_ChangeCPStatus"; // change cp status
        public static string SProc_DeleteCollectPersonData = "SProc_DeleteCollectPersonData"; // soft delete collect person data
        public static string SProc_updateCollectPersonData = "SProc_updateCollectPersonData"; // update collect person data
        public static string SProc_DeleteFailedPayment = "SProc_DeleteFailedPayment"; // remove pending and failed payment (optional)   
        public static string SProc_GetBrokerList = "SProc_GetBrokerList"; // get broker list by company id for collection admin
        public static string SProc_DeleteCPCustomer = "SProc_DeleteCPCustomer"; // delete cp customer
        public static string SProc_GetCustomerListForUpdate = "SProc_GetCustomerListForUpdate";
        public static string SProc_GetCPCustomerByCPCode = "SProc_GetCPCustomerByCPCode"; // get cp customer list by cp code
        public static string SProc_GetCustomerByBrokerId = "SProc_GetCustomerByBrokerId";// get customer by broker
        public static string SProc_UpdateCPCustomerRelation = "SProc_UpdateCPCustomerRelation"; // update cp customer
        public static string SProc_DeleteCPCustRelation = "SProc_DeleteCPCustRelation"; // Delete CP Cust Relation

        public static string SProc_GetLastSendEmailAndContact = "SProc_GetLastSendEmailAndContact"; // get last email and contact customer got payment receipt
        public static string SProc_InsertEmailConfigData = "SProc_InsertEmailConfigData"; // manage email configuration for get payment receipt
        public static string SProc_getemailconfigByCompanyId = "SProc_getemailconfigByCompanyId"; //get data for who can get the payment receipt by email
        public static string SProc_GetCustomerContactInfo = "SProc_GetCustomerContactInfo";

        public static string SProc_Collection_GetAllCompanyDetails = "SProc_Collection_GetAllCompanyDetails";
        public static string SProc_InsertCompanyData = "SProc_InsertCompanyData";
        public static string SProc_InsertUIConfigData = "SProc_InsertUIConfigData"; // insert UI customization data
        public static string SProc_InsertCollectionTempCustomer = "SProc_InsertCollectionTempCustomer"; // insert collection temp customer
        public static string SProc_Collection_GetCustConfigData = "SProc_Collection_GetCustConfigData"; // get cust config data by client name
        public static string OutSideERPCustomerListByCpCode = "OutSideERPCustomerListByCpCode"; // get out side erp customer list for collection app
        public static string SProc_UpdateCompanyDetails = "SProc_UpdateCompanyDetails"; // UPDATE company details
        public static string SProc_ChangeCompanyStatus = "SProc_ChangeCompanyStatus"; // change company status
        public static string SProc_DeleteCompanyDetails = "SProc_DeleteCompanyDetails"; // delete company details by companyid
        public static string SProc_GetCompanyListForUIConfig = "SProc_GetCompanyListForUIConfig";
        public static string SProc_InsertCollectionCustomUX = "SProc_InsertCollectionCustomUX"; // insert collection app ui config data
        public static string SProc_GetAppUIConfigData_ByClientName = "SProc_GetAppUIConfigData_ByClientName"; // get app ui config data by client name
        public static string SProc_InsertPaymentMethodConfigdata = "SProc_InsertPaymentMethodConfigdata"; // Insert payment methods config data

        //Pay Panel Sp
        public static string SProc_Insert_PayPanelData = "SP_Insert_Temp_PayPanelData";// Insert pay panel temp data 
        public static string SProc_Get_PayPanelData = "SP_Get_PayPanelDataById"; // Get pay panel data by Id
    }
}