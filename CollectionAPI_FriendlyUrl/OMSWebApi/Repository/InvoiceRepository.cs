﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using OMSWebApi.Models;
using System.Xml.Serialization;
using Newtonsoft.Json.Linq;
using System.Data.OleDb;
using OMSWebApi.Repository;

namespace OMSWebApi.Repository
{
    public class InvoiceRepository
    {

        DBConnection dbConnectionObj = null;
        SqlCommand SqlCmd = null;
        DataToJson dtToJson = new DataToJson();


        DB2Connection con = null;
        SqlCommand DB2cmd = null;


        public string getOrderNoByOrderDateData(string OrderDateFrom, string OrderDateTo)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetOrderNoByOrderDate, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@OrderDateFrom", OrderDateFrom);
                SqlCmd.Parameters.AddWithValue("@OrderDateTo", OrderDateTo);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                //jsonString = dtToJson.convertRegulartDataToJson(dt);
                jsonString = dtToJson.convertDataTableToJson(dt, "getOrderNoByOrderDateData   ", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertRegulartDataToJson(dt);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string submitInvoiceOrders(string customerId, string companyId, string brokerId, string OrderNo, DateTime InvoiceDate, decimal CustBalanceDue, decimal NetAmount, decimal OutshandingBalance, decimal ShippingCharges, decimal NetAmountToBePaid, string PaymentMode, string CustomerNotes, Int32 InvoiceTotalQty)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcOMS_Submit_Invoice, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                String invoiceID = String.Empty;
                invoiceID = brokerId.PadLeft(6, '0') + System.DateTime.Now.Year.ToString() +
                              System.DateTime.Now.Month.ToString().PadLeft(2, '0') + System.DateTime.Now.Day.ToString().PadLeft(2, '0') +
                              System.DateTime.Now.Hour.ToString().PadLeft(2, '0') + System.DateTime.Now.Minute.ToString().PadLeft(2, '0') +
                              System.DateTime.Now.Second.ToString().PadLeft(2, '0') + System.DateTime.Now.Millisecond.ToString().PadLeft(3, '0');

                SqlCmd.Parameters.AddWithValue("@InvoiceID", invoiceID);
                SqlCmd.Parameters.AddWithValue("@CustomerID", customerId);
                SqlCmd.Parameters.AddWithValue("@CompanyID", companyId);
                SqlCmd.Parameters.AddWithValue("@SalesmanId", brokerId);
                SqlCmd.Parameters.AddWithValue("@CustomerAddress", NetAmount);
                SqlCmd.Parameters.AddWithValue("@CustomeNotes", CustomerNotes);
                SqlCmd.Parameters.AddWithValue("@PONumber", OrderNo);
                SqlCmd.Parameters.AddWithValue("@InvoiceBalanceDue", CustBalanceDue);

                SqlCmd.Parameters.AddWithValue("@InvoiceDate", InvoiceDate);
                SqlCmd.Parameters.AddWithValue("@InvoiceTotQty", InvoiceTotalQty);
                SqlCmd.Parameters.AddWithValue("@PaymentMode", PaymentMode);
                SqlCmd.Parameters.AddWithValue("@OutstandingBalance", OutshandingBalance);
                SqlCmd.Parameters.AddWithValue("@ShippingCharges", ShippingCharges);
                SqlCmd.Parameters.AddWithValue("@NetAmountToBePaid", NetAmountToBePaid);
                SqlCmd.Parameters.AddWithValue("@MESSAGE", "");

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "submitInvoiceOrders", true);
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "submitInvoiceOrders", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string getInvoiceDataByDate(string CompanyID, string SalesmanID, string CustomerID, string OrderNo, string OrderDateFrom, string OrderDateTo)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetInvoiceOrderByDate, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@SalesmanID", SalesmanID);
                SqlCmd.Parameters.AddWithValue("@CustomerID", CustomerID);
                SqlCmd.Parameters.AddWithValue("@OrderNo", OrderNo);
                SqlCmd.Parameters.AddWithValue("@DateFrom", OrderDateFrom);
                SqlCmd.Parameters.AddWithValue("@DateTo", OrderDateTo);
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyID);
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                //jsonString = dtToJson.convertRegulartDataToJson(dt);
                if(dt.Rows.Count > 0)
                {
                    jsonString = dtToJson.convertDataTableToJson(dt, "getInvoiceDataByDate   ", true);
                    return jsonString;
                }
                else
                {
                    dt = new DataTable();
                    //dt.Columns.Add("SuccessMessage", typeof(string));
                    dt.Columns.Add("Message", typeof(string));
                    dt.Rows.Add("No Record Found For The Selected Combination.");
                    jsonString = dtToJson.convertDataTableToJson(dt, "getInvoiceDataByDate   ", false);
                    return jsonString;
                }
                
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertRegulartDataToJson(dt);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string GetInvoiceDetailsDataByNo(string InvoiceNo, string CompanyId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SPGetInvoiceDetailByNo, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@InvoiceNumber", InvoiceNo);
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetInvoiceDetailsDataByNo", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetInvoiceDetailsDataByNo", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string GetPaymentTransaction(string CustomerID, string CompanyId, String SalesmanId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetPaymentTransaction, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CustomerID", CustomerID);
                SqlCmd.Parameters.AddWithValue("@CompanyId", CompanyId);
                SqlCmd.Parameters.AddWithValue("@SalesmanId", SalesmanId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                if(dt.Rows.Count > 0 )
                    jsonString = dtToJson.convertDataTableToJson(dt, "GetPaymentTransaction", true);
                else
                {
                    DataTable dtl = new DataTable();
                    dtl.Columns.Add("SuccessMessage", typeof(string));
                    dtl.Columns.Add("Message", typeof(string));
                    dtl.Rows.Add("Data Does Not Exists", "");
                    jsonString = dtToJson.convertDataTableToJson(dtl, "GetReceiptDetailsByInvoice", false);
                    dtl = null;
                }
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetPaymentTransaction", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string GetReceiptDetailsDataByInvoice( string CompanyId, string InvoiceNo)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SPGetReceiptDetailByInvoiceNo, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);
                SqlCmd.Parameters.AddWithValue("@InvoiceNo", InvoiceNo);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                if(dt.Rows.Count > 0)
                    jsonString = dtToJson.convertDataTableToJson(dt, "GetReceiptDetailsByInvoice", true);
                else
                {
                    DataTable dtl = new DataTable();
                    dtl.Columns.Add("SuccessMessage", typeof(string));
                    dtl.Columns.Add("Message", typeof(string));
                    dtl.Rows.Add("Data Does Not Exists", "");
                    jsonString = dtToJson.convertDataTableToJson(dtl, "GetReceiptDetailsByInvoice", false);
                    dtl = null;
                }
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetReceiptDetailsByInvoice", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }
        public string GetReceiptListBySalesman(string CompanyId, string SalesmanID)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SPGetReceiptList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);
                SqlCmd.Parameters.AddWithValue("@SalesmanID", SalesmanID);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                //if(dt.Rows.Count > 0)
                jsonString = dtToJson.convertDataTableToJson(dt, "GetReceiptList", true);
                //else
                //{
                //    DataTable dtl = new DataTable();
                //    dtl.Columns.Add("SuccessMessage", typeof(string));
                //    dtl.Columns.Add("Message", typeof(string));
                //    dtl.Rows.Add("Receipt Data Does Not Exists", "");
                //    jsonString = dtToJson.convertDataTableToJson(dtl, "GetReceiptList", false);
                //    dtl = null;
                //}
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetReceiptList", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }
        public string GetPaymentDataList(string CompanyId, String SalesmanId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetPaymentList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", CompanyId);
                SqlCmd.Parameters.AddWithValue("@SalesmanId", SalesmanId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                if(dt.Rows.Count > 0 )
                    jsonString = dtToJson.convertDataTableToJson(dt, "GetPaymentDataList", true);
                 else
                {
                    DataTable dtl = new DataTable();
                    dtl.Columns.Add("SuccessMessage", typeof(string));
                    dtl.Columns.Add("Message", typeof(string));
                    dtl.Rows.Add("Payment Data Does Not Exists", "");
                    jsonString = dtToJson.convertDataTableToJson(dtl, "GetPaymentDataList", false);
                    dtl = null;
                }
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetPaymentDataList", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }
        

    }
}