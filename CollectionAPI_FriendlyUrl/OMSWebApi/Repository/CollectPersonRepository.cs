﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using OMSWebApi.Models;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Web.Configuration;
using System.Transactions;
using System.IO;
using System.Reflection;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using System.Configuration;
using System.Globalization;

namespace OMSWebApi.Repository
{
    public class CollectPersonRepository
    {
        DBConnection dbConnectionObj = null;
        SqlCommand SqlCmd = null;
        DataToJson dtToJson = new DataToJson();
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string InsertCollectPersonData(CollectPersonModel CollectPersonal)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            string CPCode = string.Empty;
            string imgpath = string.Empty;
            try
            {
                //CPCode = Guid.NewGuid().ToString("N");
                //Random rnd = new Random();
                //CPCode = Convert.ToString(rnd.Next(100000, 999999));

                var random = new Random(System.DateTime.Now.Millisecond);
                CPCode = Convert.ToString(random.Next(1, 999999));

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_InsertCollectPersonalData, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CPCODE", string.IsNullOrEmpty(CPCode) ? DBNull.Value : (object)CPCode);
                SqlCmd.Parameters.AddWithValue("@CPNAME", string.IsNullOrEmpty(CollectPersonal.CPNAME) ? DBNull.Value : (object)CollectPersonal.CPNAME);
                SqlCmd.Parameters.AddWithValue("@EmailId", string.IsNullOrEmpty(CollectPersonal.EmailId) ? DBNull.Value : (object)CollectPersonal.EmailId);
                SqlCmd.Parameters.AddWithValue("@ContactNo", string.IsNullOrEmpty(CollectPersonal.ContactNo) ? DBNull.Value : (object)CollectPersonal.ContactNo);
                SqlCmd.Parameters.AddWithValue("@CompanyId", (CollectPersonal.CompanyId <= 0) ? DBNull.Value : (object)CollectPersonal.CompanyId);
                SqlCmd.Parameters.AddWithValue("@Address", string.IsNullOrEmpty(CollectPersonal.Address) ? DBNull.Value : (object)CollectPersonal.Address);
                SqlCmd.Parameters.AddWithValue("@City", string.IsNullOrEmpty(CollectPersonal.City) ? DBNull.Value : (object)CollectPersonal.City);
                SqlCmd.Parameters.AddWithValue("@Zone", string.IsNullOrEmpty(CollectPersonal.Zone) ? DBNull.Value : (object)CollectPersonal.Zone);
                SqlCmd.Parameters.AddWithValue("@StateId", string.IsNullOrEmpty(CollectPersonal.StateId) ? DBNull.Value : (object)CollectPersonal.StateId);
                SqlCmd.Parameters.AddWithValue("@CountryId", (CollectPersonal.CountryId <= 0) ? DBNull.Value : (object)CollectPersonal.CountryId);
                SqlCmd.Parameters.AddWithValue("@ZipCode", string.IsNullOrEmpty(CollectPersonal.ZipCode) ? DBNull.Value : (object)CollectPersonal.ZipCode);
                SqlCmd.Parameters.AddWithValue("@Password", string.IsNullOrEmpty(CollectPersonal.Password) ? DBNull.Value : (object)CollectPersonal.Password);
                SqlCmd.Parameters.AddWithValue("@CPassword", string.IsNullOrEmpty(CollectPersonal.CPassword) ? DBNull.Value : (object)CollectPersonal.CPassword);

                var saveImageName = "";
                System.Drawing.Image img;
                String StoreImgPath = System.Web.Hosting.HostingEnvironment.MapPath("~/PaymentImage");
                string imageSavePath = string.Empty;
                var base64Image = CollectPersonal.ImageBase64String;
                var imageName = CollectPersonal.CPImage;
                if (!String.IsNullOrEmpty(base64Image))
                {
                    saveImageName = CPCode + Path.GetExtension(imageName);
                    imageSavePath = StoreImgPath + "\\" + saveImageName;
                    if (!String.IsNullOrEmpty(base64Image))
                    {
                        img = Base64ToImage(Convert.ToString(base64Image));
                        img.Save(imageSavePath);
                    }
                }

                SqlCmd.Parameters.AddWithValue("@CPImage", string.IsNullOrEmpty(saveImageName) ? DBNull.Value : (object)saveImageName);
                if (saveImageName == "" || saveImageName == null)
                {
                    imgpath = null;
                }
                else
                {
                    imgpath = ApiConstant.PaymentImage;
                }
                SqlCmd.Parameters.AddWithValue("@ImagePath", imgpath);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertCollectPersonData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertCollectPersonData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string GetCollectPersonData(CollectPersonList CPList)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_GetCollectionPersonProfileData, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@FromDate", CPList.FromDate == DateTime.MinValue ? DBNull.Value : (object)CPList.FromDate);
                SqlCmd.Parameters.AddWithValue("@ToDate", CPList.ToDate == DateTime.MinValue ? DBNull.Value : (object)CPList.ToDate);
                SqlCmd.Parameters.AddWithValue("@CPName", string.IsNullOrEmpty(CPList.CPName) ? DBNull.Value : (object)CPList.CPName);
                SqlCmd.Parameters.AddWithValue("@EmailId", string.IsNullOrEmpty(CPList.EmailId) ? DBNull.Value : (object)CPList.EmailId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetCollectPersonData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetCollectPersonData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string getCountryList()
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetCountryList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "getCountryList", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "getCountryList", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string GetStateListByCountryId(StateList state)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetStateList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CountryCode", (state.CountryCode <= 0) ? DBNull.Value : (object)state.CountryCode);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetStateListByCountryId", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetStateListByCountryId", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string GetCompanylist(CompanyList Company)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                DBConnectionConfig dbconfig = new DBConnectionConfig();
                SqlCmd = new SqlCommand(ApiConstant.SProc_GetActiveCompanyList, dbconfig.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", (Company.CompanyId <= 0) ? DBNull.Value : (object)Company.CompanyId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbconfig.ConnectionOpen();
                da.Fill(dt);
                dbconfig.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetCompanylist", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetCompanylist", false);
            }
            finally
            {
                //dbconfig = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string GetCustomerlist(CustomerModel Customer)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_GetCustomerList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", (Customer.CompanyId <= 0) ? DBNull.Value : (object)Customer.CompanyId);
                SqlCmd.Parameters.AddWithValue("@CustomerCode", string.IsNullOrEmpty(Customer.CustCode) ? DBNull.Value : (object)Customer.CustCode);
                SqlCmd.Parameters.AddWithValue("@CustomerName", string.IsNullOrEmpty(Customer.CustName) ? DBNull.Value : (object)Customer.CustName);
                SqlCmd.Parameters.AddWithValue("@Zone", (Customer.Zone <= 0) ? DBNull.Value : (object)Customer.Zone);
                SqlCmd.Parameters.AddWithValue("@BrokerId", string.IsNullOrEmpty(Customer.BrokerId) ? DBNull.Value : (object)Customer.BrokerId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetCustomerlist", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetCustomerlist", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }


        public string getAllZone(CustomerModel Customer)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_GetZoneList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", (Customer.CompanyId <= 0) ? DBNull.Value : (object)Customer.CompanyId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "getAllZone", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "getAllZone", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string GetAllCollectPersonList(CustomerModel Customer)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_GetCollectPersonList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", (Customer.CompanyId <= 0) ? DBNull.Value : (object)Customer.CompanyId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetAllCollectPersonList", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetAllCollectPersonList", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string SaveAssignCustomerdata(AssignCustomer assignCust)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_InsertCollectPersonCustomer, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CPCode", string.IsNullOrEmpty(assignCust.CPCode) ? DBNull.Value : (object)assignCust.CPCode);
                SqlCmd.Parameters.AddWithValue("@CustomerCode", string.IsNullOrEmpty(assignCust.CustCode) ? DBNull.Value : (object)assignCust.CustCode);
                SqlCmd.Parameters.AddWithValue("@CompanyId", (assignCust.CompanyId <= 0) ? DBNull.Value : (object)assignCust.CompanyId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "SaveAssignCustomerdata", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "SaveAssignCustomerdata", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string getCustCollectPersonRelation(SearchAssignCust getAssin)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_GetCustCollectPersonRelation, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CustCode", string.IsNullOrEmpty(getAssin.CustCode) ? DBNull.Value : (object)getAssin.CustCode);
                SqlCmd.Parameters.AddWithValue("@CPName", string.IsNullOrEmpty(getAssin.CPName) ? DBNull.Value : (object)getAssin.CPName);
                SqlCmd.Parameters.AddWithValue("@CustName", string.IsNullOrEmpty(getAssin.CustName) ? DBNull.Value : (object)getAssin.CustName);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "getCustCollectPersonRelation", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "getCustCollectPersonRelation", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string getUserProfileforLogin(Userlogin User)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            DataTable dtLogin = null;
            string jsonString;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_getUserProfileForLogin, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@UserId", string.IsNullOrEmpty(User.UserId) ? DBNull.Value : (object)User.UserId);
                SqlCmd.Parameters.AddWithValue("@Password", string.IsNullOrEmpty(User.Password) ? DBNull.Value : (object)User.Password);
                SqlCmd.Parameters.AddWithValue("@CompanyId", string.IsNullOrEmpty(User.CompanyId) ? DBNull.Value : (object)User.CompanyId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                if (dt.Rows.Count > 0)
                {
                    // Convert Datatable to JSON string
                    SqlConnection con = new SqlConnection(ApiConstant.apiConnectionStringConfig);
                    con.Open();
                    String query = @"SELECT CompanyName, LEFT(CompanyName, CASE WHEN charindex(' ', CompanyName) = 0 THEN LEN(CompanyName) ELSE charindex(' ', CompanyName) - 1 END) AS ClientName
                                    FROM tbl_collection_CompanyMaster WHERE companyid=@CompanyId";
                    SqlCommand command = new SqlCommand(query, con);
                    command.Parameters.Add("@CompanyId", Convert.ToString(dt.Rows[0]["LoginCompanyId"]));
                    command.ExecuteNonQuery();
                    da = new SqlDataAdapter(command);
                    DataSet ds = new DataSet();
                    dtLogin = new DataTable();
                    da.Fill(dtLogin);
                    con.Close();

                    //dt.Columns.Add("companyName");
                    //dt.Columns.Add("ClientName");

                    System.Data.DataColumn Column = new System.Data.DataColumn("companyName", typeof(System.String));
                    System.Data.DataColumn Column1 = new System.Data.DataColumn("ClientName", typeof(System.String));
                    Column.DefaultValue = Convert.ToString(dtLogin.Rows[0]["CompanyName"]);
                    Column1.DefaultValue = Convert.ToString(dtLogin.Rows[0]["ClientName"]);
                    dt.Columns.Add(Column);
                    dt.Columns.Add(Column1);


                    //DataColumn dtRowlogin = dt.Columns();
                    //dtRowlogin["companyName"] = Convert.ToString(dtLogin.Rows[0]["CompanyName"]);
                    //dtRowlogin["ClientName"] = Convert.ToString(dtLogin.Rows[0]["ClientName"]);
                    //dt.Rows.Add(dtRowlogin);

                    jsonString = dtToJson.convertDataTableToJson(dt, "getUserProfileforLogin", true);
                }
                else
                {
                    DataTable DtError = new DataTable();
                    DtError.Columns.Add("Authorization");
                    DataRow dtrow = DtError.NewRow();
                    dtrow["Authorization"] = "Failed";
                    DtError.Rows.Add(dtrow);
                    jsonString = dtToJson.convertDataTableToJson(DtError, "getUserProfileforLogin", true);
                }
                return jsonString;
            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "getUserProfileforLogin", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string getCustomerListByCP(CustomerListCP CPCust)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.getCustomerListByCpCode, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CPCode", string.IsNullOrEmpty(CPCust.CPCode) ? DBNull.Value : (object)CPCust.CPCode);
                SqlCmd.Parameters.AddWithValue("@CompanyId", string.IsNullOrEmpty(CPCust.CompanyId) ? DBNull.Value : (object)CPCust.CompanyId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "getCustomerListByCP", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "getCustomerListByCP", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string updateCollectPersonData(UpdateCp CPUpdate)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_updateCollectPersonData, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CPName", string.IsNullOrEmpty(CPUpdate.CPName) ? DBNull.Value : (object)CPUpdate.CPName);
                SqlCmd.Parameters.AddWithValue("@EmailId", string.IsNullOrEmpty(CPUpdate.EmailId) ? DBNull.Value : (object)CPUpdate.EmailId);
                SqlCmd.Parameters.AddWithValue("@ContactNo", string.IsNullOrEmpty(CPUpdate.ContactNo) ? DBNull.Value : (object)CPUpdate.ContactNo);
                SqlCmd.Parameters.AddWithValue("@Address", string.IsNullOrEmpty(CPUpdate.Address) ? DBNull.Value : (object)CPUpdate.Address);
                SqlCmd.Parameters.AddWithValue("@City", string.IsNullOrEmpty(CPUpdate.City) ? DBNull.Value : (object)CPUpdate.City);
                SqlCmd.Parameters.AddWithValue("@Zone", string.IsNullOrEmpty(CPUpdate.Zone) ? DBNull.Value : (object)CPUpdate.Zone);
                SqlCmd.Parameters.AddWithValue("@StateId", string.IsNullOrEmpty(CPUpdate.StateId) ? DBNull.Value : (object)CPUpdate.StateId);
                SqlCmd.Parameters.AddWithValue("@CountryId", (CPUpdate.CountryId <= 0) ? DBNull.Value : (object)CPUpdate.CountryId);
                SqlCmd.Parameters.AddWithValue("@Zip", string.IsNullOrEmpty(CPUpdate.Zip) ? DBNull.Value : (object)CPUpdate.Zip);

                SqlCmd.Parameters.AddWithValue("@CPCode", string.IsNullOrEmpty(CPUpdate.CPCode) ? DBNull.Value : (object)CPUpdate.CPCode);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "updateCollectPersonData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "updateCollectPersonData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string ChangeCPStatus(CPStatus CPstatus)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_ChangeCPStatus, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@Status", Convert.ToBoolean(CPstatus.Status));
                SqlCmd.Parameters.AddWithValue("@CompanyId", (CPstatus.CompanyId <= 0) ? DBNull.Value : (object)CPstatus.CompanyId);
                SqlCmd.Parameters.AddWithValue("@CPCode", string.IsNullOrEmpty(CPstatus.CPCode) ? DBNull.Value : (object)CPstatus.CPCode);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "ChangeCPStatus", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "ChangeCPStatus", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string DeleteCPData(CPStatus CPstatus)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_DeleteCollectPersonData, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", (CPstatus.CompanyId <= 0) ? DBNull.Value : (object)CPstatus.CompanyId);
                SqlCmd.Parameters.AddWithValue("@CPCode", string.IsNullOrEmpty(CPstatus.CPCode) ? DBNull.Value : (object)CPstatus.CPCode);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "DeleteCPData", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "DeleteCPData", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string GetBrokerListByCompanyId(BrokerList broker)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_GetBrokerList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", string.IsNullOrEmpty(broker.CompanyId) ? DBNull.Value : (object)broker.CompanyId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetBrokerListByCompanyId", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetBrokerListByCompanyId", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string DeleteCPCustomer(DelCPCustomer DelCPCust)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_DeleteCPCustomer, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CPCode", string.IsNullOrEmpty(DelCPCust.CPCode) ? DBNull.Value : (object)DelCPCust.CPCode);
                SqlCmd.Parameters.AddWithValue("@CustomerCode", string.IsNullOrEmpty(DelCPCust.CustCode) ? DBNull.Value : (object)DelCPCust.CustCode);
                SqlCmd.Parameters.AddWithValue("@CompanyId", string.IsNullOrEmpty(DelCPCust.CompanyId) ? DBNull.Value : (object)DelCPCust.CompanyId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "DeleteCPCustomer", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "DeleteCPCustomer", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }
        public System.Drawing.Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
            return image;
        }

        public string GetCustomerlistForCPUpdate(CustomerModel Customer)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_GetCustomerListForUpdate, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", (Customer.CompanyId <= 0) ? DBNull.Value : (object)Customer.CompanyId);
                SqlCmd.Parameters.AddWithValue("@CustomerCode", string.IsNullOrEmpty(Customer.CustCode) ? DBNull.Value : (object)Customer.CustCode);
                SqlCmd.Parameters.AddWithValue("@CustomerName", string.IsNullOrEmpty(Customer.CustName) ? DBNull.Value : (object)Customer.CustName);
                SqlCmd.Parameters.AddWithValue("@Zone", (Customer.Zone <= 0) ? DBNull.Value : (object)Customer.Zone);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetCustomerlistForCPUpdate", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetCustomerlistForCPUpdate", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string getCPCustomerListByCPCode(CPCustomer CPCustomer)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_GetCPCustomerByCPCode, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CPCode", string.IsNullOrEmpty(CPCustomer.CPCode) ? DBNull.Value : (object)CPCustomer.CPCode);
                SqlCmd.Parameters.AddWithValue("@CompanyId", string.IsNullOrEmpty(CPCustomer.CompantId) ? DBNull.Value : (object)CPCustomer.CompantId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "getCPCustomerListByCPCode", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "getCPCustomerListByCPCode", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string GetCustomerlistByBrokerId(CustomerModel Customer)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_GetCustomerByBrokerId, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", (Customer.CompanyId <= 0) ? DBNull.Value : (object)Customer.CompanyId);
                SqlCmd.Parameters.AddWithValue("@CustomerCode", string.IsNullOrEmpty(Customer.CustCode) ? DBNull.Value : (object)Customer.CustCode);
                SqlCmd.Parameters.AddWithValue("@CustomerName", string.IsNullOrEmpty(Customer.CustName) ? DBNull.Value : (object)Customer.CustName);
                SqlCmd.Parameters.AddWithValue("@BrokerId", string.IsNullOrEmpty(Customer.BrokerId) ? DBNull.Value : (object)Customer.BrokerId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetCustomerlistByBrokerId", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetCustomerlistByBrokerId", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string UpdateCPCutomer(UPdateCPCustomer UpdateCP)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_UpdateCPCustomerRelation, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;


                SqlCmd.Parameters.AddWithValue("@CPCode", string.IsNullOrEmpty(UpdateCP.CPCode) ? DBNull.Value : (object)UpdateCP.CPCode);
                SqlCmd.Parameters.AddWithValue("@CustomerCode", string.IsNullOrEmpty(UpdateCP.CustomerCode) ? DBNull.Value : (object)UpdateCP.CustomerCode);
                SqlCmd.Parameters.AddWithValue("@CompanyId", (UpdateCP.CompanyId <= 0) ? DBNull.Value : (object)UpdateCP.CompanyId);


                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "UpdateCPCutomer", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "UpdateCPCutomer", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string DeleteCPRelation(DelCPCustomer DelCPCust)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_DeleteCPCustRelation, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CPCode", string.IsNullOrEmpty(DelCPCust.CPCode) ? DBNull.Value : (object)DelCPCust.CPCode);
                SqlCmd.Parameters.AddWithValue("@CompanyId", string.IsNullOrEmpty(DelCPCust.CompanyId) ? DBNull.Value : (object)DelCPCust.CompanyId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "DeleteCPRelation", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "DeleteCPRelation", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }
        
        public string GetLastEmailAndContact(string CustomerId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_GetLastSendEmailAndContact, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CustomerId", string.IsNullOrEmpty(CustomerId) ? DBNull.Value : (object)CustomerId);                

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetLastEmailAndContact", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetLastEmailAndContact", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string GetOutSideERPCustomerlist(CustomerListCP outSideCustomer)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.OutSideERPCustomerListByCpCode, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CPCode", string.IsNullOrEmpty(outSideCustomer.CPCode) ? DBNull.Value : (object)outSideCustomer.CPCode);
                SqlCmd.Parameters.AddWithValue("@CompanyId", string.IsNullOrEmpty(outSideCustomer.CompanyId) ? DBNull.Value : (object)outSideCustomer.CompanyId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetOutSideERPCustomerlist", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetOutSideERPCustomerlist", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }
    }
}