﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using OMSWebApi.Models;

namespace OMSWebApi.Repository
{
    public class RoleRepository
    {
        DBConnectionOAuth dbConnectionObj = null;
        SqlCommand com = null;

        /// <summary>
        /// Retunn list of all Roles from database
        /// </summary>
        /// <returns>List<Roles></returns>
        public List<Roles> GetRoleListFromDb()
        {
            List<Roles> listObj = null;

            SqlDataAdapter da = null;
            DataTable dt = null;
            try
            {
                listObj = new List<Roles>();
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocGetRolesList, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                //Bind ApplicationsModel generic list using LINQ 
                listObj = (from DataRow dr in dt.Rows

                            select new Roles()
                           {
                               Id = Convert.ToString(dr["Id"]),
                               Name = Convert.ToString(dr["Name"]),
                               IsActive = Convert.ToBoolean(dr["IsActive"])
                           }).ToList();

                return listObj;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                dbConnectionObj = null;
                listObj = null;
                com = null;
                da = null;
                dt = null;
            }
        }

        /// <summary>
        /// Update roles for roleId
        /// </summary>
        /// <param name="pObj"></param>
        /// <returns></returns>
        public bool UpdateRoleToDb(Roles pObj)
        {
            bool success = false;
            SqlDataReader reader = null;
            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocUpdateRoleById, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@RoleId", pObj.Id);
                com.Parameters.AddWithValue("@RoleName", pObj.Name);
                com.Parameters.AddWithValue("@IsActive", pObj.IsActive);

                dbConnectionObj.ConnectionOpen();
                reader = com.ExecuteReader();
                success = true;

                reader.Close();
                dbConnectionObj.ConnectionClose();
                return success;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                reader = null;
                //  dbConnectionObj = null;
                com = null;
            }

            return success; 
        }

        /// <summary>
        /// Add new role to database
        /// </summary>
        /// <param name="pObj"></param>
        /// <returns></returns>
        public bool AddNewRoleToDb(Roles pObj)
        {
            bool success = false;
            SqlDataReader reader = null;
            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocInsertRole, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@RoleName", pObj.Name);
                com.Parameters.AddWithValue("@Description", pObj.Description);
                com.Parameters.AddWithValue("@IsActive", pObj.IsActive);

                dbConnectionObj.ConnectionOpen();
                reader = com.ExecuteReader();
                success = true;

                reader.Close();
                dbConnectionObj.ConnectionClose();
                return success;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                reader = null;
                //  dbConnectionObj = null;
                com = null;
            }

            return success;
        }

        /// <summary>
        /// Delete role from database
        /// </summary>
        /// <param name="pObj"></param>
        /// <returns></returns>
        public bool DeleteRoleFromDb(Roles pObj)
        {
            bool success = false;
            SqlDataReader reader = null;
            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocDeleteRoleById, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@RoleId", pObj.Id);

                dbConnectionObj.ConnectionOpen();
                reader = com.ExecuteReader();
                success = true;

                reader.Close();
                dbConnectionObj.ConnectionClose();
                return success;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                reader = null;
                //  dbConnectionObj = null;
                com = null;
            }

            return success;
        }
    }
}