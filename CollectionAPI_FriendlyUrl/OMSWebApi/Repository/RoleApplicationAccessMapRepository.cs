﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using OMSWebApi.Models;

namespace OMSWebApi.Repository
{
    public class RoleApplicationAccessMapRepository
    {
        DBConnectionOAuth dbConnectionObj = null;
        SqlCommand com = null;
        SqlDataAdapter da = null;
        DataTable dt = null;

        public RoleApplicationAccessMapping GetRoleAppAccessMapListFromDb(int pApplicationId, int pRoleId, string userId)
        {
            RoleApplicationAccessMapping returnObj = null;
            List<RoleApplicationAccessMapping> ListObj = null;
            Common common = new Common();

            try
            {
                returnObj = new RoleApplicationAccessMapping();
                ListObj = new List<RoleApplicationAccessMapping>();
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(ApiConstant.SPocGetRoleApplicationAccessMappingList, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@ApplicationId", pApplicationId);
                com.Parameters.AddWithValue("@RoleId", pRoleId);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                //Bind ApplicationsModel generic list using LINQ 
                ListObj = (from DataRow dr in dt.Rows

                                       select new RoleApplicationAccessMapping()
                                       {
                                         //  ApplicationId = Convert.ToInt32(dr["ApplicationId"]),
                                         //  RoleId = Convert.ToInt32(dr["RoleId"]),
                                           AccessId = Convert.ToInt32(dr["AccessId"]),
                                           AccessName = Convert.ToString(dr["AccessName"]),
                                           Description = Convert.ToString(dr["Description"]),
                                           AccessTypeName = Convert.ToString(dr["AccessTypeName"]),
                                           AccessControlName = Convert.ToString(dr["AccessControlName"]),
                                           PageURL = Convert.ToString(dr["PageURL"]),
                                           IsAccess = Convert.ToBoolean(dr["IsAccess"])
                                       }).ToList();

                // Append userId at the end for report url

                if (ListObj.Count > 0)
                {
                    for(int i=0; i <= ListObj.Count-1; i++)
                    {
                        if (ListObj[i].AccessName == "reports")
                            ListObj[i].PageURL = ListObj[i].PageURL + userId;
                    }
                }

                returnObj.RoleAppAccessMapList = ListObj;
                returnObj.ApplicationList = common.GetGeneralLookupList(string.Empty, "Applications");
                returnObj.RoleList = common.GetGeneralLookupList(string.Empty, "Roles");

                return returnObj;
            }
            catch(SqlException sqlex)
            {

            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbConnectionObj = null;
                returnObj = null;
                ListObj = null;
                com = null;
                da = null;
                dt = null;
            }

            return returnObj;
        }

       

        /////

        public RoleApplicationAccessMapping GetUserAppRoleAssignedListFromDb(int pApplicationId, int pRoleId)
        {
            RoleApplicationAccessMapping returnObj = null;
            List<RoleApplicationAccessMapping> ListObj = null;
            Common common = new Common();

            try
            {
                returnObj = new RoleApplicationAccessMapping();
                ListObj = new List<RoleApplicationAccessMapping>();

                returnObj.UserAppRoleAssignedList = GetUserAssignedList(pApplicationId, pRoleId, ApiConstant.SPocGetUserAppRoleAssignedList);
                returnObj.UserAppRoleNotAssignedList = GetUserAssignedList(pApplicationId, pRoleId, ApiConstant.SPocGetUserAppRoleNotAssignedList);
                returnObj.ApplicationList = common.GetGeneralLookupList(string.Empty, "Applications");
                returnObj.RoleList = common.GetGeneralLookupList(string.Empty, "Roles");
            }
            catch (SqlException sqlex)
            {

            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbConnectionObj = null;
                ListObj = null;
                com = null;
                da = null;
                dt = null;
            }

            return returnObj;
        }

        public List<RoleApplicationAccessMapping> GetUserAssignedList(int pApplicationId, int pRoleId, string SPoc)
        {
            List<RoleApplicationAccessMapping> ListObj = null;
            try
            {
                dbConnectionObj = new DBConnectionOAuth();
                com = new SqlCommand(SPoc, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@ApplicationId", pApplicationId);
                com.Parameters.AddWithValue("@RoleId", pRoleId);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                //Bind ApplicationsModel generic list using LINQ 
                ListObj = (from DataRow dr in dt.Rows

                           select new RoleApplicationAccessMapping()
                           {
                               UserName = Convert.ToString(dr["UserName"]),
                               UserFullName = Convert.ToString(dr["UserName"]) + ", " + Convert.ToString(dr["UserFullName"])
                           }).ToList();
                return ListObj;
            }
            catch(Exception ex)
            {

            }
            finally
            {
                dbConnectionObj = null;
                ListObj = null;
                com = null;
                da = null;
                dt = null;
            }

            return ListObj;
        }

        //////

        public bool UpdateRoleAppAccessMapToDb(List<RoleApplicationAccessMapping> pObj)
        {
            bool success = false;
            SqlDataReader reader = null;
            try
            {
                
/*
                using (SqlConnection oConnection = new SqlConnection(ApiConstant.apiConnectionString))
                {
                    oConnection.Open();
                  //  using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                   // {
                        using (SqlCommand oCommand = new SqlCommand(ApiConstant.SPocDeleteRoleAppAccessMapByIds, oConnection))
                        {
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@RoleId", pObj[0].RoleId);
                            oCommand.Parameters.AddWithValue("@ApplicationId", pObj[0].ApplicationId);

                            reader = com.ExecuteReader();
                            success = true;

                            //if (oCommand.ExecuteNonQuery() != 1)
                            //{
                            //    // this snippet will throw an exception to force a rollback
                            //    throw new InvalidProgramException();
                            //}
                        }
                       // oTransaction.Commit();
                        
                  //  }
                }

              

                using (SqlConnection oConnection = new SqlConnection(ApiConstant.apiConnectionString))
                {
                    oConnection.Open();
                   // using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                    //{
                        using (SqlCommand oCommand = new SqlCommand(ApiConstant.SPocInsertRoleAppAccessMap, oConnection))
                        {
                            foreach (var oItem in pObj)
                            {
                                oCommand.CommandType = CommandType.StoredProcedure;
                                oCommand.Parameters.AddWithValue("@RoleId", oItem.RoleId);
                                oCommand.Parameters.AddWithValue("@ApplicationId", oItem.ApplicationId);
                                oCommand.Parameters.AddWithValue("@AccessId", oItem.AccessId);
                                oCommand.Parameters.AddWithValue("@IsAccess", oItem.IsAccess);

                                reader = com.ExecuteReader();
                                success = true;

                                ////if (oCommand.ExecuteNonQuery() != 1)
                                ////{
                                ////    // this snippet will throw an exception to force a rollback
                                ////    throw new InvalidProgramException();
                                ////}
                            }
                        }
                      //  oTransaction.Commit();
                      //  success = true;
                   // }
                }
*/
                dbConnectionObj = new DBConnectionOAuth();


                com = new SqlCommand(ApiConstant.SPocDeleteRoleAppAccessMapByIds, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@RoleId", pObj[0].RoleId);
                com.Parameters.AddWithValue("@ApplicationId", pObj[0].ApplicationId);

                dbConnectionObj.ConnectionOpen();
                reader = com.ExecuteReader();
                success = true;

                reader.Close();
                dbConnectionObj.ConnectionClose();
                

                foreach (var oItem in pObj)
                {
                    com = new SqlCommand(ApiConstant.SPocInsertRoleAppAccessMap, dbConnectionObj.ApiConnection);
                    com.CommandType = CommandType.StoredProcedure;
                    
                    com.Parameters.AddWithValue("@RoleId", oItem.RoleId);
                    com.Parameters.AddWithValue("@ApplicationId", oItem.ApplicationId);
                    com.Parameters.AddWithValue("@AccessId", oItem.AccessId);
                    com.Parameters.AddWithValue("@IsAccess", oItem.IsAccess);

                    dbConnectionObj.ConnectionOpen();
                    reader = com.ExecuteReader();
                    success = true;

                    reader.Close();
                    dbConnectionObj.ConnectionClose();
                }

               
                return success;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                reader = null;
                //  dbConnectionObj = null;
                com = null;
            }

            return success;
        }




        public bool UpdateUserAppRoleMapToDb(List<RoleApplicationAccessMapping> pObj)
        {
            bool success = false;
            SqlDataReader reader = null;
            try
            {

                dbConnectionObj = new DBConnectionOAuth();


                com = new SqlCommand(ApiConstant.SPocDeleteUserAppRoleMapByIds, dbConnectionObj.ApiConnection);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@RoleId", pObj[0].RoleId);
                com.Parameters.AddWithValue("@ApplicationId", pObj[0].ApplicationId);

                dbConnectionObj.ConnectionOpen();
                reader = com.ExecuteReader();
                success = true;

                reader.Close();
                dbConnectionObj.ConnectionClose();


                foreach (var oItem in pObj)
                {
                    com = new SqlCommand(ApiConstant.SPocInsertUserAppRoleMap, dbConnectionObj.ApiConnection);
                    com.CommandType = CommandType.StoredProcedure;

                    com.Parameters.AddWithValue("@RoleId", oItem.RoleId);
                    com.Parameters.AddWithValue("@ApplicationId", oItem.ApplicationId);
                    com.Parameters.AddWithValue("@UserName", oItem.UserName);

                    dbConnectionObj.ConnectionOpen();
                    reader = com.ExecuteReader();
                    success = true;

                    reader.Close();
                    dbConnectionObj.ConnectionClose();
                }


                return success;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                reader = null;
                //  dbConnectionObj = null;
                com = null;
            }

            return success;
        }

    }
}