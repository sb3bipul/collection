﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using OMSWebApi.Models;
using System.Xml.Serialization;
using Newtonsoft.Json.Linq;
using System.Data.OleDb;
using System.Web.Configuration;
using OMSWebApi.Controllers;
using OMSWebApi.Providers;
using OMSWebApi.Results;
using OMSWebApi.Repository;
using System.Net.Mail;
using System.Globalization;

namespace OMSWebApi.Repository
{
    public class LocationRepository
    {
        DBConnection dbConnectionObj = null;
        SqlCommand SqlCmd = null;
        DataToJson dtToJson = new DataToJson();

        /// <summary>
        /// Get Track Location History 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string GetTrackLocationHistory(string UserId,  DateTime fromDate, DateTime toDate)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetTrackLocationHistory, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@UserId", UserId);
                SqlCmd.Parameters.AddWithValue("@FROMDATE", fromDate);
                SqlCmd.Parameters.AddWithValue("@TODATE", toDate);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetTrackLocationHistory", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "GetTrackLocationHistory", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// Save Track Location  
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string InsertTrackLocation(string UserId, string UserName,  DateTime createDate, decimal Latitude, decimal Longitude, String Locatn)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcInsertTrackLocation, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@UserId", UserId);
                SqlCmd.Parameters.AddWithValue("@UserName", UserName);
                SqlCmd.Parameters.AddWithValue("@CreatedDate", createDate);
                SqlCmd.Parameters.AddWithValue("@Latitude", Latitude);
                SqlCmd.Parameters.AddWithValue("@Longitude", Longitude);
                SqlCmd.Parameters.AddWithValue("@Location", Locatn);


                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertTrackLocation", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "InsertTrackLocation", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }


    }
}