﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using OMSWebApi.Models;
using System.Xml.Serialization;
using Newtonsoft.Json.Linq;
using System.Data.OleDb;
using System.Configuration;
using System.Web.Configuration;
using System.Net.Mail;
using System.IO;
using System.Net;
using System.Globalization;
using System.Net.Http;
using System.Net.Http.Headers;

namespace OMSWebApi.Repository
{
    public class PriceListRepository
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        string URLpath = ApiConstant.CategoryAPI;
        private string urlParameters = "", FolderName = "Category";
        DBConnection dbConnectionObj = null;
        SqlCommand SqlCmd = null;
        DataToJson dtToJson = new DataToJson();

        /// <summary>
        ///  Get Category for Item Price List
        /// </summary>
        /// <returns></returns>
        public string GetItemCategory(PriceList objPriceList)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            String jsonString;
            try
            {
                String Image_Name = String.Empty, Cat_ID = String.Empty;
                //HttpClient client = new HttpClient();
                //client.BaseAddress = new Uri(URL);
                //// Add an Accept header for JSON format.
                //client.DefaultRequestHeaders.Accept.Add(
                //new MediaTypeWithQualityHeaderValue("application/json"));
                //// List data response.
                //HttpResponseMessage response = client.GetAsync(urlParameters).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.
                //if (response.IsSuccessStatusCode)
                //{
                //    // Parse the response body.
                //    var dataObjects = response.Content.ReadAsAsync<IEnumerable<Categorylst[]>>().Result;  //Make sure to add a reference to System.Net.Http.Formatting.dll
                //    foreach (var d in dataObjects)
                //    {
                //        ImageName = d[0].ToString();
                //    }
                //}
                DataTable dtCat = new DataTable("Catg");
                DataColumn dc = new DataColumn("ImageName", typeof(String));
                dtCat.Columns.Add(dc);
                dc = new DataColumn("CatID", typeof(String));
                dtCat.Columns.Add(dc);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(URLpath);
                    //HTTP GET
                    var responseTask = client.GetAsync("api/Company/GetCategoryImage/" + objPriceList.CompanyId);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<Categorylst[]>();
                        readTask.Wait();

                        var categories = readTask.Result;
                        //ImageName = categories[0].CategoryImage;
                        foreach (var catg in categories)
                        {
                            Image_Name = catg.CategoryImage.ToString();
                            Cat_ID = catg.CatID.ToString();
                            dtCat.Rows.Add(new Object[] { Image_Name, Cat_ID });
                        }
                    }
                }
                DataSet dsCatg = new DataSet();
                dsCatg.Tables.Add(dtCat);
                String TxtCategories = dsCatg.GetXml();
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcInsertCategory, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", objPriceList.CompanyId);
                SqlCmd.Parameters.AddWithValue("@FolderName", FolderName);
                SqlCmd.Parameters.AddWithValue("@TxtCategories", TxtCategories);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProc_GetMergedCategory, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", objPriceList.CompanyId);
                SqlCmd.Parameters.AddWithValue("@LanguageId", "en-US");

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();
                //dt.Merge(dtCat);

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "Category", true);
                log.Info("Final Result:::::::::::::" + jsonString);

                return jsonString;
            }
            catch (Exception ex)
            {
                log.Error("-> api/PriceList/GetItemCategory" + "param:" + objPriceList.CompanyId, ex);
                jsonString = dtToJson.convertDataTableToJson(dt, "Category", false);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        ///  Get Items List by Sub Category
        /// </summary>
        /// <returns></returns>
        public string GetItemsListByCategories(PriceList objPriceList)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            String jsonString;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetItemsListByCategories, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", objPriceList.CompanyId);
                SqlCmd.Parameters.AddWithValue("@CategoryCode", objPriceList.CategoryCode);
                SqlCmd.Parameters.AddWithValue("@SubCategoryCode", objPriceList.SubCategoryCode);
                SqlCmd.Parameters.AddWithValue("@CustomerID", objPriceList.CustomerCode);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                if (dt.Rows.Count > 0)
                    jsonString = dtToJson.convertDataTableToJson(dt, "GetItemsListByCategories", true);
                else
                {
                    DataTable dtl = new DataTable();
                    dtl.Columns.Add("SuccessMessage", typeof(string));
                    dtl.Columns.Add("Message", typeof(string));
                    dtl.Rows.Add("Item Does Not Exists", "");
                    jsonString = dtToJson.convertDataTableToJson(dtl, "GetItemsListByCategories", false);
                    dtl = null;
                }
                log.Info("Final Result:::::::::::::" + jsonString);

                return jsonString;

            }
            catch (Exception ex)
            {
                log.Error("-> api/PriceList/GetItemsListByCategories" + "param:" + objPriceList.CompanyId, ex);
                jsonString = dtToJson.convertDataTableToJson(dt, "GetItemsListByCategories", false);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        ///  Get Items List by Saless Category
        /// </summary>
        /// <returns></returns>
        public string GetItemsListBySalesCategories(PriceList objPriceList)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetItemsListBySalesCategories, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyID", objPriceList.CompanyId);
                SqlCmd.Parameters.AddWithValue("@SalesCategoryCode", objPriceList.SalesCategoryCode);
                SqlCmd.Parameters.AddWithValue("@SalesSubCategoryCode", objPriceList.SalesSubCategoryCode);
                SqlCmd.Parameters.AddWithValue("@CustomerID", objPriceList.CustomerCode);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "GetItemsListBySalesCategories", true);
                log.Info("Final Result:::::::::::::" + jsonString);

                return jsonString;

            }
            catch (Exception ex)
            {
                log.Error("-> api/PriceList/GetItemsListBySalesCategories" + "param:" + objPriceList.CompanyId, ex);
                jsonString = dtToJson.convertDataTableToJson(dt, "GetItemsListBySalesCategories", false);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        ///  Get Items List by Tag Name
        /// </summary>
        /// <returns></returns>
        public string GetItemsListByTagName(PriceList objPriceList)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetItemsListByTagName, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", objPriceList.CompanyId);
                SqlCmd.Parameters.AddWithValue("@BillingCO", objPriceList.BillingCO);
                SqlCmd.Parameters.AddWithValue("@CustomerID", objPriceList.CustomerCode);
                SqlCmd.Parameters.AddWithValue("@SearchTxt", objPriceList.ItemName);
                SqlCmd.Parameters.AddWithValue("@TagName", objPriceList.TagName);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                if (dt.Rows.Count > 0)
                    jsonString = dtToJson.convertDataTableToJson(dt, "GetItemsListByTagName", true);
                else
                {
                    DataTable dtl = new DataTable();
                    dtl.Columns.Add("SuccessMessage", typeof(string));
                    dtl.Columns.Add("Message", typeof(string));
                    dtl.Rows.Add("Item Does Not Exists", "");
                    jsonString = dtToJson.convertDataTableToJson(dtl, "GetItemsListByTagName", false);
                    dtl = null;
                }
                log.Info("Tag Name Result :: " + jsonString);

                return jsonString;

            }
            catch (Exception ex)
            {
                log.Error("-> api/PriceList/GetItemsListByTagName" + "param:" + objPriceList.CompanyId, ex);
                jsonString = dtToJson.convertDataTableToJson(dt, "GetItemsListByTagName", false);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public string GetItemSubCategory(PriceList objPriceList)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            String jsonString;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetItemSubCategory, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", objPriceList.CompanyId);
                SqlCmd.Parameters.AddWithValue("@CategoryId", objPriceList.CategoryId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "Sub Category", true);
                return jsonString;

            }
            catch (Exception ex)
            {
                jsonString = dtToJson.convertDataTableToJson(dt, "Sub Category", false);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }


        //public string GetItemSubCategory2(PriceList objPriceList)
        //{
        //    SqlDataAdapter da = null;
        //    DataTable dt = null;
        //    string jsonString;

        //    try
        //    {
        //        dbConnectionObj = new DBConnection();
        //        SqlCmd = new SqlCommand(ApiConstant.SProcGetItemSubCategory2, dbConnectionObj.ApiConnectionPriceList);
        //        SqlCmd.CommandType = CommandType.StoredProcedure;
        //        SqlCmd.Parameters.AddWithValue("@CompanyId", objPriceList.CompanyId);
        //        SqlCmd.Parameters.AddWithValue("@CategoryId", objPriceList.CategoryId);
        //        da = new SqlDataAdapter(SqlCmd);
        //        dt = new DataTable();
        //        dbConnectionObj.ConnectionOpen();
        //        da.Fill(dt);
        //        dbConnectionObj.ConnectionClose();

        //        // Convert Datatable to JSON string
        //        jsonString = dtToJson.convertDataTableToJson(dt, "GetItemSubCategory2", true);
        //        return jsonString;

        //    }
        //    catch (Exception ex)
        //    {
        //        jsonString = dtToJson.convertDataTableToJson(dt, "GetItemSubCategory2", false);
        //    }
        //    finally
        //    {
        //        dbConnectionObj = null;

        //        SqlCmd = null;
        //        da = null;
        //        dt = null;
        //    }
        //    return null;
        //}

        // Send Email with attached price list to Customer
        //public string EmailPriceList(PriceList objPriceList)
        //{
        //    SqlDataAdapter da = null;
        //    DataTable dt = null;
        //    DataTable dtDB2 = null;
        //    string jsonString;

        //    try
        //    {
        //        //dbConnectionObj = new DBConnection();
        //        //SqlCmd = new SqlCommand(ApiConstant.SProcGetItemCategory, dbConnectionObj.ApiConnectionPriceList);
        //        //SqlCmd.CommandType = CommandType.StoredProcedure;

        //        //SqlCmd.Parameters.AddWithValue("@CompanyId", objPriceList.CompanyId);

        //        //Get Customer data from DB2 by Customer Id
        //        dtDB2 = getCustomerInfoByCustomerIdDB2(objPriceList.BrokerId, objPriceList.CustomerCode);

        //        dbConnectionObj = new DBConnection();
        //        SqlCmd = new SqlCommand(ApiConstant.SProcGetPriceListURL, dbConnectionObj.ApiConnection);
        //        SqlCmd.CommandType = CommandType.StoredProcedure;

        //        SqlCmd.Parameters.AddWithValue("@Zone", dtDB2.Rows[0]["CSBZN"].ToString());
        //        SqlCmd.Parameters.AddWithValue("@CompanyId", dtDB2.Rows[0]["CSCO"].ToString());
        //        SqlCmd.Parameters.AddWithValue("@DocumentType", "PriceList");

        //        da = new SqlDataAdapter(SqlCmd);
        //        dt = new DataTable();

        //        dbConnectionObj.ConnectionOpen();
        //        da.Fill(dt);
        //        dbConnectionObj.ConnectionClose();

        //        ///if(dt.Rows.Count>0)
        //        SendEmailWithAttachment("pricelist@goya.com", objPriceList.Email, "Goya Price List", "", "", dt.Rows[0]["FileURL"].ToString());
        //        // Convert Datatable to JSON string
        //        jsonString = dtToJson.convertDataTableToJson(dt, "EmailPriceList", true);
        //        //log.Info("Final Result:::::::::::::" + jsonString);

        //        return jsonString;

        //    }
        //    catch (Exception ex)
        //    {
        //        // log.Error("-> api/PriceList/EmailPriceList" + "param:" + objPriceList.CompanyId, ex);
        //        jsonString = dtToJson.convertDataTableToJson(dt, "EmailPriceList", false);
        //    }
        //    finally
        //    {
        //        dbConnectionObj = null;

        //        SqlCmd = null;
        //        da = null;
        //        dt = null;
        //    }
        //    return null;
        //}

        public bool SendEmailWithAttachment(string from, string to, string subject, string body, string cc, string filePath)
        {
            bool isEmailSent = false;

            try
            {
                MailMessage email = new MailMessage(from, to, subject, "<p>Hi</p></br></br> <p>Please find the attached Goya Price list<p></br></br><p>Thank You</p>");

                // Get File stream from URL
                var stream = new WebClient().OpenRead("https://portal.goya.com/PriceList/01/Zone_1.pdf?v=05012018");
                Attachment attachment = new Attachment(stream, "");
                attachment.Name = "GoyaPriceList.pdf";

                email.Attachments.Add(attachment); // attach file


                if (cc != "")
                {
                    email.CC.Add(cc);
                }
                email.IsBodyHtml = true;

                string server = WebConfigurationManager.AppSettings["SMTPServer"];
                SmtpClient smtp = new SmtpClient(server);

                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = true;

                smtp.Send(email);
                isEmailSent = true;
            }
            catch (Exception ex)
            {
                isEmailSent = false;
            }
            return isEmailSent;
        }

        // Get Customer data from DB2 by Customer Id
        public DataTable getCustomerInfoByCustomerIdDB2(string BrokerId, string CustomerId)
        {
            SqlDataAdapter da = null;
            DataTable dt = new DataTable();
            string jsonString;
            try
            {


                OleDbConnection con = new OleDbConnection(ApiConstant.apiConnectionStringDB2);
                OleDbCommand DB2cmd = new OleDbCommand(ApiConstant.SProcGetCustomerInfoByCustomerIdDB2.ToString(), con);

                DB2cmd.CommandType = CommandType.StoredProcedure;
                DB2cmd.Connection = con;

                DB2cmd.Parameters.AddWithValue("@SALESMANID", BrokerId.Substring(2, 4));
                DB2cmd.Parameters.AddWithValue("@CUSTOMERID", CustomerId);

                //  SqlCmd.Parameters.AddWithValue("@Action", action);


                con.Open();

                var dr = DB2cmd.ExecuteReader(CommandBehavior.CloseConnection);

                dt.Load(dr);
                string newColumnName;

                // Change column name coming from DB2 which have double quotes in all column name
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    newColumnName = dt.Columns[i].ColumnName.ToString();
                    dt.Columns[i].ColumnName = newColumnName.Replace("\"", "");
                    dt.AcceptChanges();
                }
                return dt;
            }
            catch (Exception ex)
            {
                log.Error("-> api/customer/getCustomerInfoByCustomerIdDB2", ex);
            }
            finally
            {

            }
            return null;

        }


        // Get Item Sales Category
        public string GetItemSalesCategory(PriceList objPriceList)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetItemSalesCategory, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", objPriceList.CompanyId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "Sales Category", true);


                return jsonString;

            }
            catch (Exception ex)
            {
                log.Error("-> api/PriceList/GetItemSalesCategory", ex);
                jsonString = dtToJson.convertDataTableToJson(dt, "Sales Category", false);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        // Get Item Sales Category
        public string GetItemSalesSubCategory(PriceList objPriceList)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            string jsonString;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetItemSalesSubCategory, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@SalesCategoryId", objPriceList.SalesCategoryId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                jsonString = dtToJson.convertDataTableToJson(dt, "Sales Sub Category", true);


                return jsonString;

            }
            catch (Exception ex)
            {
                log.Error("-> api/PriceList/GetItemSalesSubCategory", ex);
                jsonString = dtToJson.convertDataTableToJson(dt, "Sales Sub Category", false);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        /// Get Item list from DB2 using company and zone
        /// </summary>
        /// <returns></returns>
        //public string GetItemListDB2(string GoyaCompanyID, string BillingCompany, string Zone, string CustomerID, int Limit, int Offset, string SearchText, string IsPromo)
        //{
        //    DataTable dt1 = new DataTable();
        //    DataTable dt2 = new DataTable();
        //    try
        //    {
        //        OleDbConnection con = new OleDbConnection(ApiConstant.apiConnectionStringDB2);
        //        OleDbCommand DB2cmd = new OleDbCommand(ApiConstant.SProcGetProductDataDB2.ToString(), con);

        //        DB2cmd.CommandType = CommandType.StoredProcedure;
        //        DB2cmd.Connection = con;

        //        DB2cmd.Parameters.AddWithValue("@COMPANYID", GoyaCompanyID);
        //        DB2cmd.Parameters.AddWithValue("@BILLCO", BillingCompany);
        //        DB2cmd.Parameters.AddWithValue("@ZONE", Zone);
        //        DB2cmd.Parameters.AddWithValue("@CUSTOMERID", CustomerID);
        //        DB2cmd.Parameters.AddWithValue("@ROWLIMIT", Limit);
        //        DB2cmd.Parameters.AddWithValue("@ROWOFFSET", Offset);
        //        DB2cmd.Parameters.AddWithValue("@SEARCHTEXT", "%" + SearchText + "%");

        //        if (IsPromo == "" || IsPromo == null)
        //            IsPromo = "%";


        //        DB2cmd.Parameters.AddWithValue("@ISPROMO", IsPromo);
        //        con.Open();

        //        var dr = DB2cmd.ExecuteReader(CommandBehavior.CloseConnection);

        //        dt1.Load(dr);
        //        dt2 = getItemListWithSalesCategory(); // Get Item Sales Category

        //        // Merge two data tables to get Sales Category,Sales Sub Cateogry
        //        dt1.Columns.Add("SalesCategoryId");
        //        dt1.Columns.Add("SalesCategoryDes");
        //        dt1.Columns.Add("SalesSubCategoryId");
        //        dt1.Columns.Add("SalesSubCategoryDes");
        //        dt1.Columns.Add("Count");

        //        for (int i = 0; i < dt1.Rows.Count; i++)
        //        {
        //            log.Debug("Product Code = " + dt1.Rows[i]["PRODUCTCODE"].ToString().Trim());
        //            dt1.Rows[i]["Count"] = "1";
        //            for (int j = 0; j < dt2.Rows.Count; j++)
        //            {
        //                if (dt1.Rows[i]["PRODUCTCODE"].ToString().Trim() == dt2.Rows[j]["ProductCode"].ToString().Trim())
        //                {
        //                    log.Debug("Product Code = " + dt1.Rows[i]["PRODUCTCODE"].ToString().Trim());
        //                    dt1.Rows[i]["SalesCategoryId"] = dt2.Rows[j]["SalesCategoryID"];
        //                    dt1.Rows[i]["SalesCategoryDes"] = dt2.Rows[j]["SalesCategoryDes"];
        //                    dt1.Rows[i]["SalesSubCategoryId"] = dt2.Rows[j]["SalesSubCategoryID"];
        //                    dt1.Rows[i]["SalesSubCategoryDes"] = dt2.Rows[j]["SalesSubCategoryDes"];
        //                    break;
        //                }
        //            }
        //        }

        //        con.Close();

        //        string jsonString;
        //        jsonString = dtToJson.convertDataTableToJson(dt1, "GetItemListDB2", true);
        //        return jsonString;

        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("-> Method: GetItemListDB2  " + ex);
        //    }
        //    finally
        //    {
        //        dbConnectionObj = null;
        //        dt1 = null;
        //    }
        //    return null;
        //}



        //public DataTable getItemListWithSalesCategory()
        //{
        //    SqlDataAdapter da = null;
        //    DataTable dt = null;
        //    string jsonString;
        //    try
        //    {
        //        dbConnectionObj = new DBConnection();
        //        SqlCmd = new SqlCommand(ApiConstant.SProcGetItemsSalesCategory, dbConnectionObj.ApiConnectionPriceList);
        //        SqlCmd.CommandType = CommandType.StoredProcedure;

        //        da = new SqlDataAdapter(SqlCmd);
        //        dt = new DataTable();
        //        dbConnectionObj.ConnectionOpen();
        //        da.Fill(dt);
        //        dbConnectionObj.ConnectionClose();



        //        return dt;

        //    }
        //    catch (Exception ex)
        //    {
        //        return dt;
        //    }
        //    finally
        //    {
        //        dbConnectionObj = null;

        //        SqlCmd = null;
        //        da = null;
        //        dt = null;
        //    }

        //}


        /// <summary>
        /// Get Top 50 Items by Customer
        /// </summary>
        /// <returns></returns>
        public string getTop50ItemsByCustomer(Item objItem)
        {
            DataTable dt = new DataTable();
            SqlDataAdapter da = null;
            String jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetTop50ItemsByCustomer, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyID", objItem.CompanyId);
                SqlCmd.Parameters.AddWithValue("@CustomerID", objItem.CustomerId);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                //string FromDate = objItem.FromDate.ToString("yyyy-MM-dd");
                //string ToDate = objItem.ToDate.ToString("yyyy-MM-dd");
                //DB2cmd.Parameters.AddWithValue("@COMPANYID", objItem.GoyaCompanyId);
                //DB2cmd.Parameters.AddWithValue("@ZONE", objItem.Zone);
                //DB2cmd.Parameters.AddWithValue("@CUSTOMERID", objItem.CustomerId);
                //if (objItem.IsPromo == "" || objItem.IsPromo == null)
                //    objItem.IsPromo = "%";
                //DB2cmd.Parameters.AddWithValue("@ISPROMO", objItem.IsPromo);
                //DB2cmd.Parameters.AddWithValue("@BILLCO", objItem.BillingCompanyId);
                //DB2cmd.Parameters.AddWithValue("@FROMDATE", FromDate);
                //DB2cmd.Parameters.AddWithValue("@TODATE", ToDate);
                //con.Open();
                //var dr = DB2cmd.ExecuteReader(CommandBehavior.CloseConnection);
                //dt1.Load(dr);
                //con.Close();

                // Convert Datatable to JSON string
                if (dt.Rows.Count > 0)
                    jsonString = dtToJson.convertDataTableToJson(dt, "SProc_GetItemsTop50ByCustomer", true);
                else
                {
                    DataTable dtl = new DataTable();
                    dtl.Columns.Add("SuccessMessage", typeof(string));
                    dtl.Columns.Add("Message", typeof(string));
                    dtl.Rows.Add("Item Does Not Exists", "");
                    jsonString = dtToJson.convertDataTableToJson(dtl, "SProc_GetItemsTop50ByCustomer", false);
                    dtl = null;
                }
                
                return jsonString;
            }
            catch (Exception ex)
            {
                log.Error("-> Method: getTop50ItemsByCustomer  " + ex);
            }
            finally
            {
                dbConnectionObj = null;
                dt = null;
                SqlCmd = null;
                da = null;
            }
            return null;
        }


        /// <summary>
        ///   Get Item List by Customer History
        /// </summary>
        /// <returns></returns>
        public string getItemListByCustomerHistory(Item objItem)
        {
            DataTable dt = new DataTable();
            SqlDataAdapter da = null;
            string jsonString;
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetItemListByCustomerHistory, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                string FromDate = objItem.FromDate.ToString("yyyy-MM-dd");
                string ToDate = objItem.ToDate.ToString("yyyy-MM-dd");

                SqlCmd.Parameters.AddWithValue("@CompanyID", objItem.CompanyId);
                SqlCmd.Parameters.AddWithValue("@CustomerID", objItem.CustomerId);
                SqlCmd.Parameters.AddWithValue("@FromDate", FromDate);
                SqlCmd.Parameters.AddWithValue("@ToDate", ToDate);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                //OleDbConnection con = new OleDbConnection(ApiConstant.apiConnectionStringDB2);
                //OleDbCommand DB2cmd = new OleDbCommand(ApiConstant.SProcGetItemListByCustomerHistory.ToString(), con);
                //DB2cmd.CommandType = CommandType.StoredProcedure;
                //DB2cmd.Connection = con;
                //string FromDate = objItem.FromDate.ToString("yyyy-MM-dd");
                //string ToDate = objItem.ToDate.ToString("yyyy-MM-dd");
                //DB2cmd.Parameters.AddWithValue("@COMPANYID", objItem.GoyaCompanyId);
                //DB2cmd.Parameters.AddWithValue("@ZONE", objItem.Zone);
                //DB2cmd.Parameters.AddWithValue("@CUSTOMERID", objItem.CustomerId);
                //if (objItem.IsPromo == "" || objItem.IsPromo == null)
                //    objItem.IsPromo = "%";
                //DB2cmd.Parameters.AddWithValue("@ISPROMO", objItem.IsPromo);
                //DB2cmd.Parameters.AddWithValue("@BILLCO", objItem.BillingCompanyId);
                //DB2cmd.Parameters.AddWithValue("@FROMDATE", FromDate);
                //DB2cmd.Parameters.AddWithValue("@TODATE", ToDate);
                //con.Open();
                //var dr = DB2cmd.ExecuteReader(CommandBehavior.CloseConnection);
                //dt1.Load(dr);
                //con.Close();
                if (dt.Rows.Count > 0)
                    jsonString = dtToJson.convertDataTableToJson(dt, "getItemListByCustomerHistory", true);
                else
                {
                    DataTable dtl = new DataTable();
                    dtl.Columns.Add("SuccessMessage", typeof(string));
                    dtl.Columns.Add("Message", typeof(string));
                    dtl.Rows.Add("Item Does Not Exists", "");
                    jsonString = dtToJson.convertDataTableToJson(dtl, "getItemListByCustomerHistory", false);
                    dtl = null;
                }
                return jsonString;

            }
            catch (Exception ex)
            {
                log.Error("-> Method: getItemListByCustomerHistory  " + ex);
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        /// <summary>
        ///   Get Item List by Customer History
        /// </summary>
        /// <returns></returns>
        public string getItemListByCustomerLastOrder(Item objItem)
        {
            DataTable dt = new DataTable();
            SqlDataAdapter da = null;
            DataTable dt2 = new DataTable();
            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetItemListByCustomerLastOrder, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyID", objItem.CompanyId);
                SqlCmd.Parameters.AddWithValue("@CustomerID", objItem.CustomerId);
                SqlCmd.Parameters.AddWithValue("@SalesmanID", objItem.SalesmanId);
                SqlCmd.Parameters.AddWithValue("@Action", objItem.Limit);
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                string jsonString;
                if (dt.Rows.Count > 0)
                    jsonString = dtToJson.convertDataTableToJson(dt, "getItemListByCustomerLastOrder", true);
                else
                {
                    DataTable dtl = new DataTable();
                    dtl.Columns.Add("SuccessMessage", typeof(string));
                    dtl.Columns.Add("Message", typeof(string));
                    dtl.Rows.Add("Item Does Not Exists", "");
                    jsonString = dtToJson.convertDataTableToJson(dtl, "getItemListByCustomerLastOrder", false);
                    dtl = null;
                }
               
                return jsonString;

            }
            catch (Exception ex)
            {
                log.Error("-> Method: getItemListByCustomerLastOrder  " + ex);
            }
            finally
            {
                dbConnectionObj = null;
                dt = null;
            }
            return null;
        }

        public string getItemListNotOrderedLastMonth(Item objItem)
        {
            DataTable dt = new DataTable();
            SqlDataAdapter da = null;
            DataTable dt2 = new DataTable();
            try
            {
                DateTime firstDay = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                DateTime firstDate = firstDay.AddMonths(-1);
                DateTime lastDay = firstDay.AddMonths(0).AddDays(-1);
                objItem.FromDate = Convert.ToDateTime(firstDate.ToString("MM/dd/yyyy"));
                objItem.ToDate = Convert.ToDateTime(lastDay.ToString("MM/dd/yyyy"));

                string FromDate = objItem.FromDate.ToString("yyyy-MM-dd");
                string ToDate = objItem.ToDate.ToString("yyyy-MM-dd");

                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetItemListByNotOrderedLastMonth, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyID", objItem.CompanyId);
                SqlCmd.Parameters.AddWithValue("@CustomerID", objItem.CustomerId);
                SqlCmd.Parameters.AddWithValue("@SalesmanID", objItem.SalesmanId);
                SqlCmd.Parameters.AddWithValue("@LastMonthFrom", FromDate);
                SqlCmd.Parameters.AddWithValue("@LastMonthTo", ToDate);
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                string jsonString;
                if (dt.Rows.Count > 0)
                    jsonString = dtToJson.convertDataTableToJson(dt, "getItemListNotOrderedLastMonth", true);
                else
                {
                    DataTable dtl = new DataTable();
                    dtl.Columns.Add("SuccessMessage", typeof(string));
                    dtl.Columns.Add("Message", typeof(string));
                    dtl.Rows.Add("Item Does Not Exists", "");
                    jsonString = dtToJson.convertDataTableToJson(dtl, "getItemListNotOrderedLastMonth", false);
                    dtl = null;
                }
                
                return jsonString;

            }
            catch (Exception ex)
            {
                log.Error("-> Method: getItemListByCustomerLastOrder  " + ex);
            }
            finally
            {
                dbConnectionObj = null;
                dt = null;
            }
            return null;
        }



        ///// <summary>

        //// Get Price Catalog PDF
        ///// </summary>
        ///// <returns></returns>
        //public string getPriceCatalogPDF(Item objItem)
        //{
        //    SqlDataAdapter da = null;
        //    DataTable dt = null;
        //    string jsonString;

        //    try
        //    {
        //        //   String retValue = objConn.insertSmartOrderCatalog("SProc_SmartOrderCatalogInsert", "@CompanyId", smrtCatalogOrd.CompanyID, "@CatalogId", Convert.ToString(smrtCatalogOrd.CatalogId), "@Week", smrtCatalogOrd.Week, "@Zone", smrtCatalogOrd.Zone, "@ItemNos", smrtCatalogOrd.ItemNos);
        //        String smartOrderID = System.DateTime.Now.Year.ToString() + System.DateTime.Now.Month.ToString().PadLeft(2, '0') + System.DateTime.Now.Day.ToString().PadLeft(2, '0') + "_" +
        //      System.DateTime.Now.Hour.ToString().PadLeft(2, '0') + System.DateTime.Now.Minute.ToString().PadLeft(2, '0') +
        //      System.DateTime.Now.Second.ToString().PadLeft(2, '0') + System.DateTime.Now.Millisecond.ToString().PadLeft(3, '0');

        //        dbConnectionObj = new DBConnection();
        //        SqlCmd = new SqlCommand(ApiConstant.SProcGetPriceCatalogPDF, dbConnectionObj.ApiConnectionPriceList);
        //        SqlCmd.CommandType = CommandType.StoredProcedure;


        //        SqlCmd.Parameters.AddWithValue("@CompanyId", objItem.GoyaCompanyId);
        //        // SqlCmd.Parameters.AddWithValue("@CatalogId", "1");
        //        SqlCmd.Parameters.AddWithValue("@Week", objItem.Week);
        //        SqlCmd.Parameters.AddWithValue("@Zone", objItem.Zone);
        //        SqlCmd.Parameters.AddWithValue("@ItemNos", objItem.ItemNos);
        //        SqlCmd.Parameters.AddWithValue("@SmartOrderID", smartOrderID);



        //        da = new SqlDataAdapter(SqlCmd);
        //        dt = new DataTable();
        //        dbConnectionObj.ConnectionOpen();
        //        da.Fill(dt);
        //        dbConnectionObj.ConnectionClose();




        //        var strDtTm = DateTime.Now.ToString(@"yyyy/MM/dd hh:mm:ss", new CultureInfo("en-US"));
        //        strDtTm = strDtTm.Replace(":", "_");
        //        strDtTm = strDtTm.Replace("/", "_");

        //        var PDF_path = Convert.ToString(ConfigurationManager.AppSettings["PDF_downloadPath"]);
        //        var ReportURL = Convert.ToString(ConfigurationManager.AppSettings["ReportUrl"]);
        //        var filename = Convert.ToString(ConfigurationManager.AppSettings["FileName"]) + smartOrderID + ".PDF";


        //        String paramVal = "CompanyID=" + Convert.ToString(objItem.GoyaCompanyId) + "&CatalogId=" + Convert.ToString(objItem.CatalogId) + "&SmartOrderID=" + smartOrderID; ;
        //        var RptURL = ReportURL + "/Pages/ReportViewer.aspx?%2fSales+Report+UAT%2frptSmartOrderCatalogDetails&rs:Command=Render&" + paramVal + "&rs:ClearSession=true&rs:Format=PDF";
        //        WebClient Client = new WebClient();
        //        Client.UseDefaultCredentials = true;

        //        byte[] myDataBuffer = Client.DownloadData(RptURL);

        //        var fileLocation = PDF_path + "\\" + filename;

        //        dt.Columns.Add("FilePath");



        //        if (System.IO.File.Exists(fileLocation) == true)
        //        {
        //            //lblMessage.Text = "PDF File already exists!";
        //        }
        //        else
        //        {
        //            //SAVE FILE HERE
        //            System.IO.File.WriteAllBytes(fileLocation, myDataBuffer);

        //            // Send PDF images
        //            MemoryStream ms = new MemoryStream(myDataBuffer, 0, myDataBuffer.Length);
        //            ms.Write(myDataBuffer, 0, myDataBuffer.Length);

        //            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
        //            result.Content = new ByteArrayContent(ms.ToArray());
        //            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");

        //            return "";

        //        }

        //        if (dt.Rows.Count > 0)
        //            dt.Rows[0]["FilePath"] = fileLocation;

        //        // Convert Datatable to JSON string
        //        jsonString = dtToJson.convertDataTableToJson(dt, "SProcGetRouteOrderSummary", true);
        //        return jsonString;



        //    }
        //    catch (Exception ex)
        //    {
        //        //throw ex.ToString();
        //    }
        //    return "";

        //}


        ////  // Get Customer Order Item Avegrage Count by Last 30 days, Last 4 Orders
        //public string getCustomerOrderItemsAvegrage(PriceList objPriceList)
        //{
        //    SqlDataAdapter da = null;
        //    DataTable dt = null;
        //    string jsonString;

        //    try
        //    {
        //        dbConnectionObj = new DBConnection();
        //        SqlCmd = new SqlCommand(ApiConstant.SProcGetCustomerOrderItemsAvegrage, dbConnectionObj.ApiConnection);
        //        SqlCmd.CommandType = CommandType.StoredProcedure;

        //        SqlCmd.Parameters.AddWithValue("@BrokerId", objPriceList.BrokerId);
        //        SqlCmd.Parameters.AddWithValue("@CustomerId", objPriceList.CustomerCode);
        //        SqlCmd.Parameters.AddWithValue("@Days", objPriceList.Days);

        //        da = new SqlDataAdapter(SqlCmd);
        //        dt = new DataTable();
        //        dbConnectionObj.ConnectionOpen();
        //        da.Fill(dt);
        //        dbConnectionObj.ConnectionClose();

        //        // Convert Datatable to JSON string
        //        jsonString = dtToJson.convertDataTableToJson(dt, "getCustomerOrderItemsAvegrage", true);


        //        return jsonString;

        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("-> api/PriceList/getCustomerOrderItemsAvegrage", ex);
        //        jsonString = dtToJson.convertDataTableToJson(dt, "getCustomerOrderItemsAvegrage", false);
        //    }
        //    finally
        //    {
        //        dbConnectionObj = null;

        //        SqlCmd = null;
        //        da = null;
        //        dt = null;
        //    }
        //    return null;
        //}


        public DataTable GetCustomerMenuFromDb(String ScreenName, String CompanyId)
        {
            Common common = new Common();
            List<Customer> MenuDetailsListObj = null;
            DataTable dt = null;
            SqlDataAdapter da = null;
            try
            {
                dt = new DataTable();
                MenuDetailsListObj = new List<Customer>();
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetScreenImageList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@ScreenName", ScreenName);
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                ////Bind LoginImgModel generic list using LINQ   
                MenuDetailsListObj = (from DataRow dr in dt.Rows
                                      select new Customer()
                                      {
                                          ScreenName = Convert.ToString(dr["ScreenName"]),
                                          ImageFullPath = Convert.ToString(dr["ImageFullPath"]),
                                          ImageName = Convert.ToString(dr["ImageName"]),
                                          MenuAciveImage = Convert.ToString(dr["MenuAciveImage"]),
                                          MenuText = Convert.ToString(dr["MenuText"]),

                                      }).ToList();
                OMSWebApi.Models.CompanyModels.ListtoDataTable lsttodt = new OMSWebApi.Models.CompanyModels.ListtoDataTable();
                dt = lsttodt.ToDataTable(MenuDetailsListObj);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                MenuDetailsListObj = null;
            }
            return dt;
        }
        public DataTable GetOrderMenuFromDb(String ScreenName, String CompanyId)
        {
            Common common = new Common();
            List<Orders> MenuDetailsListObj = null;
            DataTable dt = null;
            SqlDataAdapter da = null;
            try
            {
                dt = new DataTable();
                MenuDetailsListObj = new List<Orders>();
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetScreenImageList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@ScreenName", ScreenName);
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                ////Bind LoginImgModel generic list using LINQ   
                MenuDetailsListObj = (from DataRow dr in dt.Rows
                                      select new Orders()
                                      {
                                          ScreenName = Convert.ToString(dr["ScreenName"]),
                                          ImageFullPath = Convert.ToString(dr["ImageFullPath"]),
                                          ImageName = Convert.ToString(dr["ImageName"]),
                                          MenuAciveImage = Convert.ToString(dr["MenuAciveImage"]),
                                          MenuText = Convert.ToString(dr["MenuText"]),

                                      }).ToList();
                OMSWebApi.Models.CompanyModels.ListtoDataTable lsttodt = new OMSWebApi.Models.CompanyModels.ListtoDataTable();
                dt = lsttodt.ToDataTable(MenuDetailsListObj);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                MenuDetailsListObj = null;
            }
            return dt;
        }
        public DataTable GetDemographicMenuFromDb(String ScreenName, String CompanyId)
        {
            Common common = new Common();
            List<Demographic> MenuDetailsListObj = null;
            DataTable dt = null;
            SqlDataAdapter da = null;
            try
            {
                dt = new DataTable();
                MenuDetailsListObj = new List<Demographic>();
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetScreenImageList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@ScreenName", ScreenName);
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                ////Bind LoginImgModel generic list using LINQ   
                MenuDetailsListObj = (from DataRow dr in dt.Rows
                                      select new Demographic()
                                      {
                                          ScreenName = Convert.ToString(dr["ScreenName"]),
                                          ImageFullPath = Convert.ToString(dr["ImageFullPath"]),
                                          ImageName = Convert.ToString(dr["ImageName"]),
                                          MenuAciveImage = Convert.ToString(dr["MenuAciveImage"]),
                                          MenuText = Convert.ToString(dr["MenuText"]),

                                      }).ToList();
                OMSWebApi.Models.CompanyModels.ListtoDataTable lsttodt = new OMSWebApi.Models.CompanyModels.ListtoDataTable();
                dt = lsttodt.ToDataTable(MenuDetailsListObj);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                MenuDetailsListObj = null;
            }
            return dt;
        }
        public DataTable GetTagMenuFromDb(String ScreenName, String CompanyId)
        {
            Common common = new Common();
            List<Tag> MenuDetailsListObj = null;
            DataTable dt = null;
            SqlDataAdapter da = null;
            try
            {
                dt = new DataTable();
                MenuDetailsListObj = new List<Tag>();
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetScreenImageList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@ScreenName", ScreenName);
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                ////Bind LoginImgModel generic list using LINQ   
                MenuDetailsListObj = (from DataRow dr in dt.Rows
                                      select new Tag()
                                      {
                                          ScreenName = Convert.ToString(dr["ScreenName"]),
                                          ImageFullPath = Convert.ToString(dr["ImageFullPath"]),
                                          ImageName = Convert.ToString(dr["ImageName"]),
                                          MenuAciveImage = Convert.ToString(dr["MenuAciveImage"]),
                                          MenuText = Convert.ToString(dr["MenuText"]),

                                      }).ToList();
                OMSWebApi.Models.CompanyModels.ListtoDataTable lsttodt = new OMSWebApi.Models.CompanyModels.ListtoDataTable();
                dt = lsttodt.ToDataTable(MenuDetailsListObj);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnectionObj = null;
                SqlCmd = null;
                da = null;
                MenuDetailsListObj = null;
            }
            return dt;
        }
        /// <summary>
        ///  Get Items List by Promotion
        /// </summary>
        /// <returns></returns>
        public string GetPromoItemsList(PriceList objPriceList)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            String jsonString;

            try
            {
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetPromoItemsList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@CompanyId", objPriceList.CompanyId);
                SqlCmd.Parameters.AddWithValue("@BillingCO", objPriceList.BillingCO);
                SqlCmd.Parameters.AddWithValue("@CustomerID", objPriceList.CustomerCode);
                SqlCmd.Parameters.AddWithValue("@SearchTxt", objPriceList.ItemName);

                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                // Convert Datatable to JSON string
                if(dt.Rows.Count > 0)
                    jsonString = dtToJson.convertDataTableToJson(dt, "GetPromoItemsList", true);
                else
                {
                    DataTable dtl = new DataTable();
                    dtl.Columns.Add("SuccessMessage", typeof(string));
                    dtl.Columns.Add("Message", typeof(string));
                    dtl.Rows.Add("Receipt Does Not Exists", "");
                    jsonString = dtToJson.convertDataTableToJson(dtl, "GetPromoItemsList", false);
                    dtl = null;
                }
                log.Info("GetPromoItemsList :: " + jsonString);

                return jsonString;

            }
            catch (Exception ex)
            {
                log.Error("-> api/PriceList/GetPromoItemsList" + "param:" + objPriceList.CompanyId, ex);
                jsonString = dtToJson.convertDataTableToJson(dt, "GetPromoItemsList", false);
            }
            finally
            {
                dbConnectionObj = null;

                SqlCmd = null;
                da = null;
                dt = null;
            }
            return null;
        }

        public DataTable GetCategoryImagesFromDb(string CompanyId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            Common common = new Common();
            List<Category> CategoryImageObj = null;
            DataTable dtl = new DataTable();
            try
            {
                CategoryImageObj = new List<Category>();
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetOrderImageList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("ScreenName", "Category");
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);
                SqlCmd.Parameters.AddWithValue("@Action", "Category");
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            CategoryImageObj.Add(new Category
                            {
                                ScreenName = Convert.ToString(dt.Rows[i]["ScreenName"]),
                                ImageFullPath = Convert.ToString(dt.Rows[i]["ImageFullPath"]),
                                CategoryID = Convert.ToString(dt.Rows[i]["CategoryID"]),
                                CategoryImageName = Convert.ToString(dt.Rows[i]["CategoryImageName"])
                            });
                        }
                    }
                }
                OMSWebApi.Models.CompanyModels.ListtoDataTable lsttodt = new OMSWebApi.Models.CompanyModels.ListtoDataTable();
                dtl = lsttodt.ToDataTable(CategoryImageObj);

            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnectionObj = null;
                CategoryImageObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }

            return dtl;
        }

        public DataTable GetBannerImagesFromDb(string CompanyId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            Common common = new Common();
            List<Banner> BannerImageObj = null;
            DataTable dtl = new DataTable();
            try
            {
                BannerImageObj = new List<Banner>();
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetOrderImageList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("ScreenName", "Banner");
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);
                SqlCmd.Parameters.AddWithValue("@Action", "Banner");
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            BannerImageObj.Add(new Banner
                            {
                                ScreenName = Convert.ToString(dt.Rows[i]["ScreenName"]),
                                ImageFullPath = Convert.ToString(dt.Rows[i]["ImageFullPath"]),
                                BannerName = Convert.ToString(dt.Rows[i]["BannerName"]),
                                BannerImageName = Convert.ToString(dt.Rows[i]["BannerImageName"])
                            });
                        }
                    }
                }
                OMSWebApi.Models.CompanyModels.ListtoDataTable lsttodt = new OMSWebApi.Models.CompanyModels.ListtoDataTable();
                dtl = lsttodt.ToDataTable(BannerImageObj);

            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnectionObj = null;
                BannerImageObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }

            return dtl;
        }

        public DataTable GetSalesCategoryImagesFromDb(string CompanyId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            Common common = new Common();
            List<SalesCategory> SalesCategoryObj = null;
            DataTable dtl = new DataTable();
            try
            {
                SalesCategoryObj = new List<SalesCategory>();
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetOrderImageList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("ScreenName", "Sales Category");
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);
                SqlCmd.Parameters.AddWithValue("@Action", "SalesCat");
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            SalesCategoryObj.Add(new SalesCategory
                            {
                                ScreenName = Convert.ToString(dt.Rows[i]["ScreenName"]),
                                ImageFullPath = Convert.ToString(dt.Rows[i]["ImageFullPath"]),
                                SalesCategoryID = Convert.ToString(dt.Rows[i]["SalesCategoryID"]),
                                SalesCategoryImageName = Convert.ToString(dt.Rows[i]["SalesCategoryImageName"])
                            });
                        }
                    }
                }
                OMSWebApi.Models.CompanyModels.ListtoDataTable lsttodt = new OMSWebApi.Models.CompanyModels.ListtoDataTable();
                dtl = lsttodt.ToDataTable(SalesCategoryObj);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnectionObj = null;
                SalesCategoryObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return dtl;
        }

        public DataTable GetItemDefaultImageFromDb(string CompanyId)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            Common common = new Common();
            List<ItemdefaultImage> ItemDefaultImageObj = null;
            DataTable dtl = new DataTable();
            try
            {
                ItemDefaultImageObj = new List<ItemdefaultImage>();
                dbConnectionObj = new DBConnection();
                SqlCmd = new SqlCommand(ApiConstant.SProcGetOrderImageList, dbConnectionObj.ApiConnection);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("ScreenName", "ItemImage");
                SqlCmd.Parameters.AddWithValue("@CompanyID", CompanyId);
                SqlCmd.Parameters.AddWithValue("@Action", "ItemImage");
                da = new SqlDataAdapter(SqlCmd);
                dt = new DataTable();
                dbConnectionObj.ConnectionOpen();
                da.Fill(dt);
                dbConnectionObj.ConnectionClose();

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            ItemDefaultImageObj.Add(new ItemdefaultImage
                            {
                                ImageFullPath = Convert.ToString(dt.Rows[i]["ImageFullPath"]),
                                ItemDefaultImage = Convert.ToString(dt.Rows[i]["ItemDefaultImage"])
                            });
                        }
                    }
                }
                OMSWebApi.Models.CompanyModels.ListtoDataTable lsttodt = new OMSWebApi.Models.CompanyModels.ListtoDataTable();
                dtl = lsttodt.ToDataTable(ItemDefaultImageObj);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbConnectionObj = null;
                ItemDefaultImageObj = null;
                SqlCmd = null;
                da = null;
                dt = null;
            }
            return dtl;
        }




    }

    public class Customer
    {
        public string ScreenName { get; set; }
        public string ImageFullPath { get; set; }
        public string ImageName { get; set; }
        public string MenuAciveImage { get; set; }
        public string MenuText { get; set; }
    }
    public class Orders
    {
        public string ScreenName { get; set; }
        public string ImageFullPath { get; set; }
        public string ImageName { get; set; }
        public string MenuAciveImage { get; set; }
        public string MenuText { get; set; }
    }
    public class Demographic
    {
        public string ScreenName { get; set; }
        public string ImageFullPath { get; set; }
        public string ImageName { get; set; }
        public string MenuAciveImage { get; set; }
        public string MenuText { get; set; }
    }
    public class Tag
    {
        public string ScreenName { get; set; }
        public string ImageFullPath { get; set; }
        public string ImageName { get; set; }
        public string MenuAciveImage { get; set; }
        public string MenuText { get; set; }
    }

    public class Category
    {
        public string ScreenName { get; set; }
        public string ImageFullPath { get; set; }
        public string CategoryID { get; set; }
        public string CategoryImageName { get; set; }
    }
    public class Banner
    {
        public string ScreenName { get; set; }
        public string ImageFullPath { get; set; }
        public string BannerName { get; set; }
        public string BannerImageName { get; set; }
    }
    public class SalesCategory
    {
        public string ScreenName { get; set; }
        public string ImageFullPath { get; set; }
        public string SalesCategoryID { get; set; }
        public string SalesCategoryImageName { get; set; }
    }
    public class ItemdefaultImage
    {
        public string ImageFullPath { get; set; }
        public string ItemDefaultImage { get; set; }
    }

    public class Categorylst
    {
        //public string ConfigID { get; set; }
        //public string ScreenName { get; set; }
        //public string ImageFullPath { get; set; }
        public string CategoryImage { get; set; }
        //public string MenuAciveImage { get; set; }
        public string CatID { get; set; }
        //public string SortOrder { get; set; }
        
        
    }
    public class Payload
    {
        //public IEnumerable<Payload> Payload { get; set; }
    }
}