﻿C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.5\System.Web.Extensions.dll

Dashboard
http://107.21.119.157/OMSmobile/api/Company/GetScreenDetails/2/2	(New)-- Dashboard single
http://107.21.119.157/OMSmobile/api/Company/GetDashboardAllData/2/013030  (New)-- Dashboard Top, middle and Bottom
Company
http://107.21.119.157/OMSmobile/api/Company/GetCompanyImgDetails	(New)-- Company Image Details
http://107.21.119.157/OMSmobile/api/Company/AddCompanyRegistration	(New)-- POST   Add Company by registration   
Footer and Menu
http://107.21.119.157/OMSmobile/api/Company/GetMenuFooter/2			(New)-- Footer and Menu 		

Customer
http://107.21.119.157/OMSmobile/api/Customer/CustomersDetailList1	 {"BrokerId":"013030"}   -- POST		All Customers by SalesmanID
http://107.21.119.157/OMSmobile/api/Customer/CustomersDetailByCustomerIdPost  {"CustomerId":"000008","CompanyId":2}  -- POST Single Customer

Login
http://107.21.119.157/OMSmobile/api/Company/GetScreenDetails/Login1/2	(New)-- Login Screen data
http://107.21.119.157/OMSmobile/api/User/LoginOtherUser				(New) -- POST Miscrosoft Login
http://107.21.119.157/OMSmobile/api/User	{ "UserId":"015937","Password":"Pwd@015937" }  -- POST User Login 
http://107.21.119.157/OMSmobile/api/User/AddUserRegistration		-- POST User Registration

1> Customer

http://107.21.119.157/OMSmobile/api/Customer/CustomersDetailListPost  Parameters: { "BrokerId":"015937", "CompanyId":"2" } Binding Fields : value : USERID , Text: NAME

2> Promo Code	http://107.21.119.157/OMSmobile/api/order/getPromoCodePost	Parameters: No parameters ( Post API )	Binding Fields : value :  PromoConc, Text: PromoConc

3> Verify Item/Product	http://107.21.119.157/OMSmobile/api/Order/getItemQOH	Parameters: { "GoyaCompanyId":"2", "ItemCode":"11023", "CustomerId":"016969", "CaseQuantity":1 } ( Post API )	Binding Fields : Item

4> Template: http://107.21.119.157/OMSmobile/api/Order/loadTemplate	Parameters: {  "CustomerId":"0", "BrokerId":"015937", "CompanyId":"2", "isPickup":0 } ( Post API )	Binding Fields : Text: TemplateName , value : BasketID

5> Department :	http://107.21.119.157/OMSmobile/api/order/getDepartmentsPost	Parameters: No parameters ( Post API )	Binding Fields : value :  DepartmentCode , Text: DepartmentCodeNm

Create Order
1> Add Item http://107.21.119.157/OMSmobile/api/Order/addOrderItem	(New) Parameters : {  "CompanyId":2,"ItemCode":"1103","CaseQuantity":3,"CustomerId":"006969","BrokerId":"015937","DeliveryDate":"2018-9-11","PromoCode":"","AmountCollected":0,"OrderMode":"Add"}

2> Remove Item	http://107.21.119.157/OMSmobile/api/Order/deleteOrderItem	(New) Parameters : { "ItemCode":"1103", "CustomerId":"006969", "EOR":"350955711 }

3> After inputting payment term and client PO, call this API and you will get the list to bind the next screen http://localhost:50313/api/Order/updatePOPaymentTerm		(New) Parameters : { "EOR":"1594055723","PaymentTerm":"Cash","AmountCollected":12,"ClientPONumber":"CPO-1234" }

4> To get the pending list of add to cart items		http://107.21.119.157/OMSmobile/api/Order/getPendingOrderItems	(New) Parameters : { "BrokerId":"015937","CustomerId":"006969","CompanyId":2 }






1> saved order details  Link:  
107.21.119.157/OMSmobile/api/OrderHistory/GetSavedCartOrderDetails

Parameters : { "PONumber":"303355701", "CompanyId":2 }

2> Edit / update Customer Address 
http://107.21.119.157/OMSmobile/api/Customer/UpdateCustomerAddress

Parameters : { "CustomerId":"712450","CompanyId":"2","SalesmanId":"5937","CompanyName":"SHOP RITE 130","Name":"SHOP RITE 130","Address":"RECEIVES 6:30 AM-1 PM DSD REQUIRED,3115 KENNEDY BLVD,NORTH BERGEN,NJ,07047,NORTH BERGEN,NJ,07047", "City":"NORTH BERGEN", "State":"NJ", "Zip":"07047", "ContactNo":"2015535700", "Region":"test" }

3> To get the details of submitted order by seledcted order number

http://107.21.119.157/OMSmobile/api/OrderHistory/GetSubmittedOrderDetails
Parameters : { "OrderNumber":"303355701", "CompanyId":2 }

MAIL CONTENT :

Created the API for deleting saved cart order and adding Wish list item / product.

1> To delete saved cart order

http://107.21.119.157/OMSmobile/api/OrderHistory/DeleteCartOrder

Parameters: "SalesmanId":"015937",itemDetails:[{"CustomerID":"003925", "CompanyID":2,"PONumber":"392855777"}]



2> Add Wish list item / product.

http://localhost:50313/api/Item/addWishlistItem

Parameters: { "CompanyId":2, "ProductCode":"1103"}

Thanks,

Koushik

On September 13, 2018 at 1:05 PM Koushik Sarkar <koushik@sb3inc.com> wrote: 

Hi Arjoo,

Created the API for Saved Order and Submitted Order :


Submitted Order

http://107.21.119.157/OMSmobile/api/OrderHistory/GetSubmittedOrder

Parameters : { "BrokerId":"015937", "CompanyId":2 }


Saved Order:

http://107.21.119.157/OMSmobile/api/OrderHistory/GetSavedCartOrder

Parameters : { "BrokerId":"015937", "CompanyId":2 }

Thanks,

Koushik

On September 12, 2018 at 2:00 PM Koushik Sarkar <koushik@sb3inc.com> wrote: 

Hi,

Created the API for the following :

1> To submit the Order header and details

http://localhost:50313/api/Order/submitQuickOrder

Parameters : { "CustomerId":"006969","CompanyId":2,"BrokerId":"015937","EOR":"1594055723","AmountCollected":0,"totalCaseQuantity":1,"totalAmount":1234,"DeliveryDate":"2018-09-09","ClientPONumber":"testing PO","Latitude":28.6125,"Longitude":77.3777,"Location":"Sector 64, New Jersey, USA"}



2>  To get the Customer Balance 

http://107.21.119.157/OMSmobile/api/Customer/CustomersDetailList1

Parameter : { "BrokerId":"015937" }



3> To update the Customer address by Customer id

http://localhost:50313/api/Customer/UpdateCustomerAddressByCustomerId

Parameters : CustomerId, CompanyID ,SalesmanId ,Street,City,State ,Zip ,Country ,Email ,ContactNo 

Thanks,

Koushik

On September 7, 2018 at 1:14 PM Koushik Sarkar <koushik@sb3inc.com> wrote: 

Hi Arjoo,

Created the API for updating the cart item quantity 

http://107.21.119.157/OMSmobile/api/Order/updateItemCaseQuantity

Parameters : {  "CaseQuantity":3, "ItemCode":"1105", "CustomerId":"006969", "EOR":"1594055723" }

Thanks,

Koushik

On September 6, 2018 at 2:04 PM Koushik Sarkar <koushik@sb3inc.com> wrote: 

Hi Arjoo,

Created OMS mobile API today for the following:



1> After inputting payment term and client PO, call this API and you will get the list to bind the next screen

http://localhost:50313/api/Order/updatePOPaymentTerm

Parameters : { "EOR":"1594055723","PaymentTerm":"Cash","AmountCollected":12,"ClientPONumber":"CPO-1234" }



2> To get the pending list of add to cart items

http://107.21.119.157/OMSmobile/api/Order/getPendingOrderItems

Parameters : { "BrokerId":"015937","CustomerId":"006969","CompanyId":2 }

@Shekhar Da/Madam: I have created some of the new API for not matching Goya OMS logic flow and updated or created view for in our local database due to not availability of IBM database.

Please let me know, if you have any query.

Thanks,

Koushik

On September 4, 2018 at 1:39 PM Koushik Sarkar <koushik@sb3inc.com> wrote: 

Hi,

As per the Create Order screen, listed the API for adding item and removing item:

1> Add Item

http://107.21.119.157/OMSmobile/api/Order/addOrderItem

Parameters : {  "CompanyId":2,"ItemCode":"1103","CaseQuantity":3,"CustomerId":"006969","BrokerId":"015937","DeliveryDate":"2018-9-11","PromoCode":"","AmountCollected":0,"OrderMode":"Add"}



2> Remove Item

http://107.21.119.157/OMSmobile/api/Order/deleteOrderItem

Parameters : { "ItemCode":"1103", "CustomerId":"006969", "EOR":"350955711 }

Thanks,

Koushik

On September 3, 2018 at 7:50 PM Koushik Sarkar <koushik@sb3inc.com> wrote: 

Hi Arjoo,

As per the attached screen of Create Order, you need to bind the drop down list for  Customer, Promo and Department , Template, using following APIs:

1> Customer

http://107.21.119.157/OMSmobile/api/Customer/CustomersDetailListPost

Parameters: { "BrokerId":"015937", "CompanyId":"2" }

Binding Fields : value : USERID , Text: NAME



2> Promo Code

http://107.21.119.157/OMSmobile/api/order/getPromoCodePost

Parameters: No parameters ( Post API )

Binding Fields : value :  PromoConc, Text: PromoConc



3> Verify Item/Product

http://107.21.119.157/OMSmobile/api/Order/getItemQOH

Parameters: { "GoyaCompanyId":"2", "ItemCode":"11023", "CustomerId":"016969", "CaseQuantity":1 } ( Post API )

Binding Fields : Item



4> Template: 

http://107.21.119.157/OMSmobile/api/Order/loadTemplate

Parameters: {  "CustomerId":"0", "BrokerId":"015937", "CompanyId":"2", "isPickup":0 } ( Post API )

Binding Fields : Text: TemplateName , value : BasketID

5> Department :

http://107.21.119.157/OMSmobile/api/order/getDepartmentsPost

Parameters: No parameters ( Post API )

Binding Fields : value :  DepartmentCode , Text: DepartmentCodeNm

6> Day 

Create an array field and bind by these values.

{{Day : "--", Value : "0"},{Day : "Mon", Value : "1"},{Day : "Tue", Value : "2"},{Day : "Wed", Value : "3"},{Day : "Thu", Value : "4"},
{Day : "Fri", Value : "5"},{Day : "Sat", Value : "6"},{Day : "Sun", Value : "7"} }

Binding Fields : Text: Day , value : Value 

7> Not Day : Same as Day drop  down list



 

If there there are any queries, let me know.

Thanks,

Koushik

On August 28, 2018 at 7:17 PM Koushik Sarkar <koushik@sb3inc.com> wrote: 

Hi Arjoo,

As we per the discussion that I have arranged the columns TextCaption and Value in the existing Api of Dashboard.  

In the Customer list screen, when user will click on the button 'Select ' then you need to store the CustomerID and name in your local device. When user will go to create the order then this customer will be selected on the order screen. In the Goya system, we do the same. I have attached the screen shots for the reference. Let me know, if you have any query.

Thanks,

Koushik

On August 27, 2018 at 12:58 PM Koushik Sarkar <koushik@sb3inc.com> wrote: 
Dashboard, Menu and Footer screen update 
Hi,

I have completed the changes for the following:

1> I have counted Total Customer Count and Customer Visited count for Dashboard in the existing API.

2> As designer given new menu image, I have updated new menu image in the existing API .

3> Added the caption for Dashboard Text, Footer Text and Menu text in the existing API.

4> Also added Language ID in the Company and Screen setting to configure the language.

5> Created OMS Admin app PPT.

6> As designer created the image for Home icon, so I have added Home icon for Footer and also added Dashboard bottom icons.

Thanks,

Koushik

On August 23, 2018 at 7:49 AM Koushik Sarkar <koushik@sb3inc.com> wrote: 
Re: Login with other system ( Microsoft user login )
Hi,
Created the Microsoft login Web API for signing in to OMS system by Microsoft user email for OMS mobile app:

http://107.21.119.157/OMSmobile/api/User/LoginOtherUser

Parameters: { "EmailID":"015937@015937.com","UserPassword":"Pwd@015937"}

Thanks,

Koushik

On August 22, 2018 at 7:42 PM Koushik Sarkar <koushik@sb3inc.com> wrote: 

Hi,

I have reduced some columns to look better which are not essential for the time being in the Dashboard API 'GetDashboardAllData' and reference is given by the attache file.

Thanks,

Koushik

On August 21, 2018 at 8:36 PM Koushik Sarkar <koushik@sb3inc.com> wrote: 

Hi Arjoo,

As we discussed that I have created the 'GetDashboardAllData' Web API to combine altogether dashboard top section, middle section and bottom section for running the performance faster for OMS mobile app and have mentioned the array and fields in the attached files for the reference.

Link : http://107.21.119.157/OMSmobile/api/Company/GetDashboardAllData/2

Parameter : CompanyId   

Thanks,

Koushik

On August 20, 2018 at 7:40 PM Koushik Sarkar <koushik@sb3inc.com> wrote: 

Hi Arjoo,

As we discussed that I have created the 'GetMenuFooter' Web API for joining menu and footer to make the performance faster for OMS mobile app and have mentioned the array and fields in the attached files for the reference.

Link : http://107.21.119.157/OMSmobile/api/Company/GetMenuFooter/2

Parameter : CompanyId   

Thanks,

Koushik

On August 16, 2018 at 5:13 AM Koushik Sarkar <koushik@sb3inc.com> wrote: 

Hi Arjoo,

The below API link for Customer list for showing all the customers by Broker/Salesman, you have to show the Customer list first in the Customer screen then you have to show the Customer details on click of Customer name.

Customer List API: ( POST method )

1> http://107.21.119.157/OMSmobile/api/Customer/CustomersDetailList1

parameters: { "BrokerId":"013030"  }

headers : 





Customer details by Customer Id :( POST method )

2> http://107.21.119.157/OMSmobile/api/Customer/CustomersDetailByCustomerIdPost

parameters:{ "CustomerId":"000008", "CompanyId":2  }      

Pass/Send the selected Customer Id

headers : header is same as above.

Thanks,

Koushik

On August 10, 2018 at 6:47 PM Koushik Sarkar <koushik@sb3inc.com> wrote: 

Hi Arjoo,

I have added the column of Dashboard and menu in the Web API ( GetScreenDetails ) for the following:

Dashboard : 

1> http://107.21.119.157/OMSmobile/api/Company/GetScreenDetails/2/2

["DashboardImageCategory":"Dashboard/approved-order.png","DashboardTopImage":"Dashboard/approved-order.png","DashboardMiddleImage":"Dashboard/customers.png","DashboardBottomImage":"Dashboard/bans_grains.png"}]

Use this columns for dashboard ( DashboardImageCategory, DashboardMiddleImage , DashboardBottomImage )

Menu :

2> http://107.21.119.157/OMSmobile/api/Company/GetScreenDetails/4/2

[{"ScreenName":"Menu","ImageFullPath":"http://107.21.119.157/OMSmobileImg/","FolderName":"Menu","FolderHeaderImage":"","HeaderImageName":"","ImageName":"","MenuAciveImage":"Menu/customer_management_active.png","MenuInactiveImage":"Menu/customer_management.png","DashboardImageCategory":"","DashboardTopImage":"","FooterActiveImage":"","FooterInactiveImage":"","FooterActiveFontColor":"","FooterInactiveFontColor":"","DashboardMiddleImage":"","DashboardBottomImage":""},



Use this columns for Menu( MenuAciveImage, MenuInactiveImage )

Thanks,

Koushik

On August 8, 2018 at 7:46 PM Koushik Sarkar <koushik@sb3inc.com> wrote: 

Hi,

I have created the Web API for registering the new user for OMS mobile app:

http://107.21.119.157/OMSmobile/api/User/AddUserRegistration

Parameters to be passed by this API : public string UserID { get; set; } public int CompanyID { get; set; } public string UserGUID { get; set; } public string Name { get; set; } public string Street { get; set; } public string City { get; set; } public string Zip { get; set; }
public string Country { get; set; } public string State { get; set; } public string ContactNo { get; set; } public string EmailID { get; set; }
public string Password { get; set; } public int UserType { get; set; } public string IMEI { get; set; }

Thanks,

Koushik

On August 7, 2018 at 8:07 PM Koushik Sarkar <koushik@sb3inc.com> wrote: 

Hi Arjoo,

I have added the column in the existing API for the following:

1> http://107.21.119.157/OMSmobile/api/Company/GetScreenDetails/3/2

[{"ScreenName":"Footer","ImageFullPath":"http://107.21.119.157/OMSmobileImg/","FolderName":"Footer","FolderHeaderImage":"","HeaderImageName":"","ImageName":"","DashboardImageCategory":"","DashboardImageName":"","FooterImage":"account_icon_active.png"}}

Use this column for footer : FooterImage : Footer/account_icon_active.png



2> http://107.21.119.157/OMSmobile/api/Company/GetCompanyImgDetails

[{"ImageID":1,"ImageFullPath":"http://107.21.119.157/OMSCompanyImg/","CompanyID":2,"cmBGImageName":"bg/bg-2.png","cmLogoName":"logo/GoyaLogo.png","cmIconName":"icon/goya_icon.png","HeaderColor":"#23337e","CompanyName":"Goya Foods, Inc.","AllPagesBGColor":"#edeeef","MiddleSectionBGcolor":"#fff","FooterBGcolor":"#fff"}]

Use this column for Company background image:  cmBGImageName : bg/bg-2.png 

@Saheli, Can you create the menu icon, so then we can add those icons in our API.

Thanks,

Koushik

On August 2, 2018 at 6:36 PM Koushik Sarkar <koushik@sb3inc.com> wrote: 

Hi Arjoo,

I have created the Web API for displaying the Country and State list for OMS mobile app for the following:



1> To get the Country list 

http://107.21.119.157/OMSmobile/api/Country/GetCountryList



2> To get the state list by Country code ( USA country code = 243 )

http://107.21.119.157/OMSmobile/api/State/GetStateListByCountry/243



3> Added company name field in Company Image details API

http://107.21.119.157/OMSmobile/api/Company/GetCompanyImgDetails

( Fields : "CompanyName":"Goya Foods, Inc." AND "CompanyName":"Folsom Corporation" )



If you have any query, let me know.

Thanks,

Koushik

On August 1, 2018 at 7:55 PM Koushik Sarkar <koushik@sb3inc.com> wrote: 

Hi Arjoo,

I have created the Web API for displaying the company icon list for OMS mobile app.

http://107.21.119.157/OMSmobile/api/Company/GetCompanyImgDetails

Output: 

[{"ImageID":1,"ImageFullPath":"http://107.21.119.157/OMSCompanyImg/","CompanyID":2,"cmBGImageName":"bg/Goya_bg.png","cmLogoName":"logo/GoyaLogo.png","cmIconName":"icon/goya_icon.png","ColorCode":"#f9f9f9"},{"ImageID":2,"ImageFullPath":"http://107.21.119.157/OMSCompanyImg/","CompanyID":1,"cmBGImageName":"bg/Folsom_bg.png","cmLogoName":"logo/FolsomLogo.png","cmIconName":"icon/Folsom_icon.png","ColorCode":"#f2f2f2"}]

Company Icon : By this API you will get these icons.





I will provide you the country and state API today. 

Thanks,

Koushik

On July 31, 2018 at 8:01 PM Koushik Sarkar <koushik@sb3inc.com> wrote: 

Hi Arjoo,

I have created the API for Company Registration screen ( for making new Company ).

Company Registration API to save the new company
http://107.21.119.157/OMSmobile/api/Company/AddCompanyRegistration

You need to pass the parameters in this way:

{

                "CompanyName":"NPatel" ,     "Street":"123 Row Street",     "City":"Jerset City",

     "Country":"USA",      "State":"NJ",     "ZipCode":"20018",     "ContactNo":"12345",

     "EmailId":"npatel@npatel.com",     "CompanyUrl":"npatel.com",     "ContactPerson":"koushik",

     "CompanyLogo":"NPatelSample.png",     "LogoBinarydData":"",     "CompanyIcon":"NPatelIcon.png",

“IconBinaryData":"",     "Comments":"Testing”

}

For any query, let us know.

Thanks,

Koushik

On July 30, 2018 at 7:01 PM Koushik Sarkar <koushik@sb3inc.com> wrote: 

Hi All,

As per the discussion that I have specified the fields and validation for Company Registration screen by the attached file.

Please download the attachment and if there is any query, let us know.

Thanks,

Koushik

On July 27, 2018 at 7:36 PM Koushik Sarkar <koushik@sb3inc.com> wrote: 

Hi,

As per our understanding that Login, Menu and Dashboard ( including logo, menu item, dashboard images ) will be configurable by database. If admin change logo or image for different company then those images or logos will be reflected in the application.

I have created the login Web API for OMS mobile app for the following:

1> User login API to get the OAuth token key ( Post Method ):

http://107.21.119.157/OMSmobile/api/User

@Arjoo : Steps to send the request by the following way:

Header =             [{"key":"Content-Type","value":"application/json","description":""}]



Token has generated by the above picture and this token has to pass every time when you are sending the next request.



2> To get the Company picture ( Goya ) for Login screen : ( Get Method )

http://107.21.119.157/OMSmobile/api/Company/GetLoginDetails/1/2

For any query, please let me know your feedback.


To get the order detail by selected Order Number 
http://107.21.119.157/OMSmobile/api/OrderHistory/GetOrderDetailByStatus				Parameters : { "OrderNumber":"392855768",  "CompanyId":2 }



-----------------------------   Master Data Sync	-------------------------------------------------
1> Custoemr List ( MCustomer )
http://107.21.119.157/OMSmobile/api/Master/CustomerList		{  "BrokerId":"015937", "CompanyId":2 }

2> Product List ( MProduct )
http://107.21.119.157/OMSmobile/api/Master/getProductData	{ "CompanyId":2, "LanguageId":"en-US", "WarehouseId":"01"}

3> To get the Category list ( Category1 )
http://107.21.119.157/OMSmobile/api/Master/getCategory		{ "CompanyId":2,  "LanguageId":"en-US"}

4> To get the sub category list ( Category2 )
http://107.21.119.157/OMSmobile/api/Master/getSubCategory	{ "CompanyId":2,  "LanguageId":"en-US"}

5> To get the list of Company 
http://107.21.119.157/OMSmobile/api/Master/getCompanyByID	{ "CompanyId":2 }

6> To get the list of Company ( MBroker )
http://107.21.119.157/OMSmobile/api/Master/BrokerList		{ "CompanyId":"2" }

7> To get the warehouse list
http://107.21.119.157/OMSmobile/api/Master/WarehouseList				{"CompanyId":"2"}

8> Get Vendor list
http://107.21.119.157/OMSmobile/api/Master/VendorList				{	"CompanyId":"2" }

9> Country List 
http://107.21.119.157/OMSmobile/api/Master/CountryList				{}

10> Brand list
http://107.21.119.157/OMSmobile/api/Master/BrandList				{}

11> Order Status List	
http://107.21.119.157/OMSmobile/api/Master/OrderStatusList			{}

12> Order Status List	
http://107.21.119.157/OMSmobile/api/Master/StockList				{	"CompanyId":"2" }

-----------------------------------------------------------------------------------------------------
13> SAVE The OFFLINE Order 
http://107.21.119.157/OMSmobile/api/Master/SubmitOrderByChatMsg {    "payLoadItems":[{
  "CustomerID":"006969",  "SalesmanID":"015937",  "CompanyID":"2",  "FullPartial":"P",  "Latitude":0.00,  "Longitude":0.00,
  "ItemCode":"1111","ItemName":"","Qty":5,"ItemPrice":0,"ItemStatus":""
  },
  {  "CustomerID":"006969",  "SalesmanID":"015937",  "CompanyID":"2",  "FullPartial":"P",  "Latitude":0.00,  "Longitude":0.00,
  "ItemCode":"8888","ItemName":"","Qty":3,"ItemPrice":0,"ItemStatus":""
  }]
}

OUTPUT : 
{
    "Domain": "addOrderItemData",
    "Event": "addOrderItemData",
    "ClientData": "test2",
    "Status": "True",
    "Payload": [        {            "ID": "1",            "ItemCode": "1111",            "ItemName": "EXTRA VIRGIN OLIVE OIL 50.7 OZ",            "Qty": "5",
            "ItemPrice": "87.10",            "ItemStatus": "Ok",            "CustomerID": "006969",            "SalesmanID": "015937",            "CompanyID": "2",
            "OrderDate": "11/14/2018",            "DeliveryDate": "11/15/2018",            "Latitude": "0.000000000",            "Longitude": "0.000000000",
            "FullPartial": "P",            "PONumber": "697255825",            "CustomerName": "DOLLAR GENERAL #8427               "
        },
        {
            "ID": "2",            "ItemCode": "8888",            "ItemName": "",            "Qty": "3",            "ItemPrice": "0.00",            "ItemStatus": "Invalid",
            "CustomerID": "006969",            "SalesmanID": "015937",            "CompanyID": "2",            "OrderDate": "11/14/2018",           "DeliveryDate": "11/15/2018",
            "Latitude": "0.000000000",            "Longitude": "0.000000000",            "FullPartial": "P",            "PONumber": "697255825",            "CustomerName": "DOLLAR GENERAL #8427               "
        }
    ]
}
---------------------------------------------------------------------------------
14> Customer list ( Customer Profile Arindam )
http://107.21.119.157/OMSmobile/api/CustProfile/GetCustomerProfileList  {   "SalesmanId":"015937",    "SearchTxt":""}
15> Customer profile save( Customer Profile Arindam )
api/CustProfile/SaveCustomerProfile 


http://airstine.com/OMSmobile_Folsom/api/Master/BrandVendorGrpList
{"CompanyId":"1"}
To get the Brand list and Vendor Group list altogether by this API