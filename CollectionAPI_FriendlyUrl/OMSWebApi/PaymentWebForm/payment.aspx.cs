﻿using OMSWebApi.Repository;
using Stripe;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace OMSWebApi.PaymentWebForm
{
    public partial class payment : System.Web.UI.Page
    {
        public string stripePublishableKey = WebConfigurationManager.AppSettings["StripePublishableKey"];
        public string clientSecret = "";
        public string CustName = "";
        public string Amount = "";
        public string DueDate = "";
        DBConnection dbConnectionObj = null;
        SqlCommand SqlCmd = null;
        DataToJson dtToJson = new DataToJson();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string guid = Request.QueryString["id"];
                byte[] b;
                string decrypted;
                b = Convert.FromBase64String(guid);
                decrypted = System.Text.ASCIIEncoding.ASCII.GetString(b);
                this.BindControls(decrypted);

                var option = new PaymentIntentCreateOptions
                {
                    Amount = 100,
                    Currency = "USD",
                    Description = "Software development services",
                    Shipping = new ChargeShippingOptions
                    {
                        Name = "Jenny Rosen",
                        Address = new AddressOptions
                        {
                            Line1 = "510 Townsend St",
                            PostalCode = "98140",
                            City = "San Francisco",
                            State = "CA",
                            Country = "US",
                        },
                    },

                    PaymentMethodTypes = new List<string>
                    {
                        "card",
                    },
                };
                var service = new PaymentIntentService();
                var paymentIntent = service.Create(option);
                clientSecret = paymentIntent.ClientSecret;
            }
            else
            {

            }
        }

        public string BindControls(string guid)
        {
            SqlDataAdapter da = null;
            DataTable dt = null;
            DateTime date = new DateTime();
            dbConnectionObj = new DBConnection();
            SqlCmd = new SqlCommand(ApiConstant.SProc_Get_PayPanelData, dbConnectionObj.ApiConnection);
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@ID", guid);
            da = new SqlDataAdapter(SqlCmd);
            dt = new DataTable();
            dbConnectionObj.ConnectionOpen();
            da.Fill(dt);
            dbConnectionObj.ConnectionClose();

            CustName = Convert.ToString(dt.Rows[0]["CustomerName"]);
            Amount = Convert.ToString(dt.Rows[0]["Amount"]);
            DueDate = String.Format("{0:ddd, MMM d, yyyy}", date);    // "Sun, Mar 9, 2008"
            return null;
        }
        //public string BindForm()
        //{
        //    //< form id = "paymentForm" runat = "server" method = "post" >

        //    //                                < div id = "card-element" ></ div >

        //    //                                 < button type = "submit" class="btn btn-block btn-info mt-3">Pay</button>
        //    //                       </form>
        //    StringBuilder str = new StringBuilder();
        //    str.Append("<form id = 'paymentForm' runat = 'server' method='post'>");
        //    str.Append()
        //    str.Append("<button type = 'submit' class='btn btn - block btn - info mt - 3'>Pay</button>");
        //    str.Append("</form>");
        //}



        //public string PaymentDue(PaymentDue payment)
        //{
        //    var option = new PaymentIntentCreateOptions
        //    {
        //        Amount = payment.Amount,
        //        Currency = Convert.ToString(payment.Currency),
        //        Description = "Software development services",
        //        Shipping = new ChargeShippingOptions
        //        {
        //            Name = Convert.ToString(payment.Customer_Name),
        //            Address = new AddressOptions
        //            {
        //                Line1 = "510 Townsend St",
        //                PostalCode = "98140",
        //                City = "San Francisco",
        //                State = "CA",
        //                Country = "US",
        //            },
        //        },

        //        PaymentMethodTypes = new List<string>
        //            {
        //                "card",
        //            },
        //    };
        //    var service = new PaymentIntentService();
        //    var paymentIntent = service.Create(option);
        //    clientSecret = paymentIntent.ClientSecret;
        //    return null;
        //}
    }
}