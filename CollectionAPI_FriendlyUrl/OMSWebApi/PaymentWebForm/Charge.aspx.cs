﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stripe;

namespace OMSWebApi.PaymentWebForm
{
    public partial class Charge : System.Web.UI.Page
    {
        public string clientSecret = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var option = new PaymentIntentCreateOptions
                {
                    Amount = 7361,
                    Currency = "USD",
                    PaymentMethodTypes = new List<string>
                    {
                        "card",
                    },
                };
                var service = new PaymentIntentService();
                var paymentIntent = service.Create(option);
                clientSecret=paymentIntent.ClientSecret;
            }
            else
            {

            }
        }
    }
}